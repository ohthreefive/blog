+++
date = "2019-08-17T15:46:26.232Z"
title = "2012 movies"
description = "Films I saw that year: ranked"
tags = [ "movie" ]

+++

I found some lists in Evernote, so here goes:

1.  The Avengers

    - Nipped in ahead of The Raid on the strength of sheer entertainment value.

2.  The Raid

    - The best and most surprising action movie since the Matrix; I could not
      believe what I was seeing.

3.  The Dark Knight Rises

    - There are a lot of disappointments, especially given how the hype was
      raised by TDK, but this remains very satisfying.

4.  Mission Impossible: Ghost Protocol

    - Burj Khalifa scene on IMAX - oh yes! However, the film couldn't recover
      from the impact of this set piece.

5.  Argo

    - Fantastic, tense film-making from Batfleck.

6.  Looper

    - Enjoyable timey-wimey caper.

8.  Moonrise Kingdom

    - Upper class Anderson - funny and charming

9.  Silver Linings Playbook

    - My favourite De Niro performance in years.

10. The Hunger Games

    - Looking back seven years later, I can't recall whether the first (and
      biggest and best) of the new wave of YA was anything special.

11. Brave

    - Not the best Pixar by a long shot.

12. Skyfall

    - I don't think Daniel Craig has had a Bond film worthy of him since Casino
      Royale. One excellent, tense scene, leading to the first attempted
      assassination of M, but not much else and a stupid finale.

13. Delicacy

    - Was this the Audrey Tatou one? If so, enjoyable time-passer with my wife.
      If not, ???

14. Friends with Kids

    - Pretty much a nothing of a movie

15. The Hobbit: An Unexpected Journey

    - Am I the only one who thought the long slow start to the movie was the
      only part of the whole trilogy that honoured the books the way the LotR
      trilogy  did? Poor Martin Freeman - he is perfect.

16. Prometheus

    - Swing and a miss.

17. Total Recall

    - Headache-inducing crap

[Home](/blog "ohthreefive")
