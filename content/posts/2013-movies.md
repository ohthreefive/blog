+++
date = "2019-08-17T16:07:44.041Z"
title = "2013 movies"
description = "Movies I saw in 2013: ranked!"
tags = [ "movie" ]

+++

Looking back, not a vintage year!

1.  Iron Man 3

    - Remains near the type of my MCU list!

2.  Zero Dark Thirty

    - Terrific but long and complicated. 'Complicated' would benefit from repeat
      viewings; 'long' makes that less likely!

3.  Alan Partridge: Alpha Papa

    - A deserved big screen outing for Alan

4.  Star Trek Into Darkness

    - Initially received well with an near-instant backlash, I can't help but
      still enjoy it.

5.  Gravity

    - **Made** for IMAX. Pretty much unrelenting and the correct length.

6.  The World's End

    - I've re-watched it and it remains the weakest of the Cornetto trilogy, but
      Nick Frost's action scene is a masterpiece.

7.  Oblivion

    - I *know* it's not very good but I still like it!

8.  Trance

    - Twisted and bizarre in a good way

9.  Django Unchained

    - I don't remember seeing this in the cinema but perhaps I did. I've never
      re-visited it and can't remember much from it.

10. Monsters University

    - Occasional flashes of genius but way lower than where Pixar should be.

11. Only God Forgives

    - I bought in to it while I was in the cinema, but it's a *hard* watch and
      I'd be hard-pressed to recommend it.

12. Elysium

    - I was hoping for more after District 9; it was ok.

13. Man of Steel

    - Let down by the action; the character moments are good.

14. Sunshine on Leith

    - I just didn't have that much fun with it.

15. Pacific Rim

    - One of the stupidest films I've ever seen

16. The Hobbit: The Desolation of Smaug

    - I think they nailed Smaug, but the fight between him and the dwarves was
      the worst example of CGI excess in the trilogy. Well, until part three...

[Home](/blog "ohthreefive")