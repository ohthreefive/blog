+++
         
date = "2019-08-17T16:21:53.755Z"
title = "2014 movies"
description = "Movies I saw that year: ranked!"
tags = [ "movie" ]

+++

Parenthood strikes! (Struck?)

1.  Interstellar

    - A phenomenal IMAX experience

2.  Edge of Tomorrow

    - Glad I sought this out. Hidden gem.

3.  The Lego Movie (home)

    - Might have topped the list if I'd seen it in the cinema. Great fun.

4.  Captain America: the Winter Soldier (home)

    - Everything but the finale is just great.

5.  X-men: Days of Future Past (home)

    - Best X since X2

6.  22 Jump Street

    - Very, very funny

7.  Guardians of the Galaxy (home)

    - I still haven't hugely got on board with the Guardians

8.  Maleficent (home)

    - Revisionist fantasy? That's new to me! Fun.

9.  Godzilla

    - Where was he?

10. The Hobbit: The Battle of the Five Armies

    - Just fuck off

[Home](/blog "ohthreefive")