+++
date = "2019-08-17T16:37:52.032Z"
title = "2015 movies"
description = "Movies I saw that year: ranked"
tags = [ "movie" ]

+++

Ooft another curtailed year!

1.  Star Wars: The Force Awakens (IMAX)

    - Star Wars is BACK. And good. Rey catching the light sabre *tingles*

2.  The Martian (IMAX)

    - I found it really funny and really solid

3.  Avengers: Age of Ultron (home)

    - The only Avengers I've not seen at the cinema. Improved with age.

4.  Kingsman: the Secret Service (cinema)

    - The church scene was just wonderful, but the initial Colin Firth bar fight
      is over stylised and try hard. Kick-Ass nailed everything, this nailed a
      few things.

5.  Mission: Impossible - Rogue Nation (home)

    - I've re-visited it after Fallout, but still can't put it above good-not-
      great. Also, the car crash definitely killed Ethan and Benji. So it lost
      me after that.

6.  John Wick (home)

    - I haven't seen Parabellum yet, but I am not 100% on board with this and
      Chapter 2 the way other people are. They're good, but they're not genre
      re-defining.

7.  The Good Dinosaur (cinema)

    - I know, it's nowhere near Pixar's best, but I saw it with my firstborn,
      she loved it, I loved her loving it and love re-watching it with her
      (and child 2). It's *nice.*

8.  Mad Max: Fury Road (cinema)

    - Yup, I'm definitely in the minority, but I just didn't get on board with
      it.

9.  Spectre (probably IMAX)

    - Suffered from the same "you haven't earned Blofeld" that Into Darkness did
      with Khan. Felt more poorly written than Quantum, which is saying a lot!

[Home](/blog "ohthreefive")