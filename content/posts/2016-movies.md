+++
date = "2019-08-17T16:52:33.929Z"
title = "2016 movies"
description = "Movies I watched that year: ranked!"
tags = [ "movie" ]

+++

A mixed year!

1.  Arrival (cinema)

    - Captivating and interesting

2.  Captain America: Civil War (home)

    - The most emotionally engaging MCU movie. It's *Cap* **fighting** *Tony*!

3.  Moana (cinema)

    - A fab cinema trip with the daughter

4.  Rogue One: A Star Wars Story (IMAX)

    - Perfect for the IMAX, and the Vader final scene was amazing.

5.  Jason Bourne (cinema)

    - I found Bourne had become too unkillable, but really enjoyed it at the
      time. Haven't been back.

7.  Deadpool (home)

    - Fun dialogue; meh action

6.  Doctor Strange (IMAX)

    - Wonderful visuals and I thought Cumberbatch was perfect.

7.  Finding Dory (cinema)

    - Nemo is still my favourite Pixar cinema experience, so while this was
      good, the love for the first unfairly soured my opinion of this sequel.

8.  Batman v Superman: Dawn of Justice (home)

    - I quite like it!

9.  Fantastic Beasts and Where To Find Them (home)

    - Crap

[Home](/blog "ohthreefive")