+++

date = "2017-12-31T19:50:08Z"
title = "2017 movies"
description = "Films I've seen this year: ranked."
tags = [ "movie" ]

+++

1. Logan (cinema)
	* No film this year has left me thinking about it for so long. I loved
	it so much I almost don’t want to watch it again in case it doesn’t meet
	my expectations.
2. Dunkirk (IMAX)
	* A breathtaking IMAX experience. Watching at home will never match it,
	but at least I’ll never forget what I saw.
3. Star Wars: The Last Jedi (IMAX)
	* I’m desperate to see this again. It’s my favourite Star Wars. I wish
	Rey had taken Ren’s hand and it had gone down some bizarre, light/dark
	precarious alliance alleyway, but it seems even Rian Johnson wasn’t
	willing to piss the fans off that much!
4. Blade Runner 2049 (cinema) 
	* More than just stunning, but boy was it beautiful!
5. Spider-Man: Homecoming (home)
	* Tom Holland nails it again as Spidey, and while it was the year’s most
	conventional Marvel, it was the best.
6. Thor Ragnarok (cinema)
	* Hilarious but thin. 
7. John Wick Chapter 2 (home)
	* The original trod the line between brilliant and stupid perfectly;
	this sequel put a few steps wrong.
8. The Villainess (home)
	* Wonderfully mental.
9. Wonder Woman (home)
	* A third act car crash detracts from a spectacular origins story.
10. Paddington 2 (cinema)
	* Funnier than the first.
11. Captain Underpants (cinema)
	* Ruined by child 2; heartily enjoyed up until then.
12. Baby Driver (cinema)
	* I’m such a fan of Edgar Wright that I think my opinion of this film
	must be some sort of mistake. But it just didn’t work for me.
13. Guardians of the Galaxy vol. 2 (home)
	* Two movies in and it seems I’m just not a Guardians fan. Chris Pratt
	is great. I just don’t *care* about any of the characters.
14. King: Skull Island (home)
	* Passable.

[Home](/blog "ohthreefive")
