
+++

date = "2019-04-21T20:36:32Z"
title = "2018: My Yoga Year"
description = "Placeholder"
tags = [ "random" ]

+++

# My yoga year

Around the end of 2017, I was getting fed up. It was four years since I'd moved
house and started having children (I stopped after two.) I was getting fed up
about exercise; exercise is very important to me. So, after about sixteen years
of getting my exercise in similar ways, I made a big change.

Skip the following autobiography if you just want to hear about the yoga!

### Where I came from

I am a rather short chap: 5' 6" (1.65 m) on a good day. Despite this, rugby was
my chosen sport. I wasn't terribly good at it until I was 14. Back then I
started playing for a club as well as my school and also started playing
football for a club. For the first time in my life, I became aerobically fit.
With this fitness came an unknown: I was fast. A good friend (and great rugby
player) always said that in rugby, there's no substitute for speed. Easy for him
to say as he's about 6' 2" and at least fast as I was!

I felt I needed a little something else. I started with weights machines aged 16
and moved on to free weights at 19. For years, that was it: free weights and
rugby. At around 25 I was introduced to CrossFit and was instantly hooked. At 27
I ruptured and had repaired my ACL and spent the next couple of years building
back up to where I was before. I proved to myself that I could get back on the
rugby pitch and then promptly retired: too old and too small!

At 29 I had my first child and moved out of the city. For the first year I was
back to just weights, then I discovered hill running (I *loathe* road running)
and finally a local CrossFit opened. I joined it around my 31st birthday.

### [CrossFit Forth Valley][1]

This is a really friendly and competitive 'box' and I highly recommend it to
anyone interested. I had just under two fantastic years there. It's about ten
minutes from my house and evening classes are five, six and seven o'clock. That
was the issue. My children went to bed around seven and I rarely got home from
work before half six. It was a bit of a rush to arrive in, disrupt a bedtime
routine and leave two still-awake children and a wife who had been working hard
all day.

We made this work for some time, during which I got my strength and fitness up
in fits and starts. Three sessions a week is the minimum I needed to stay on top
of things and it was not happening consistently.

### [The Wee Yoga Room][2]

My wife had started going to yoga about eighteen months before she convinced me
to give it a try. The yoga studio is really close to my house and the classes
start at half seven: ideal!

Rowena practices and teaches Ashtanga yoga, which became the second type of yoga
I'd heard of after Andy Murray and his Bikram yoga. Ashtanga is awesome, active
and very very difficult.

I had reasonable hamstring flexibility, but my hips and shoulders are appalling
in that regard. I was strong and had a decent core. After a year with no weights
and a decent amount of yoga, my maximum strength has definitely decreased but my
body weight strength has probably increased. My core strength is ridiculously
improved. My hamstrings are markedly better and my hips and shoulders are
beginning to move, though they are taking their sweet time. My balance and
breathing are much improved.

Yoga has become my only form of exercise. I gave up hill running and mountain
biking late last summer to give my joints and muscles the best chance to relax
and stretch. I've lost about 8 kg: mostly muscle bulk but also some fat as I've
been eating less without the high intensity CrossFit sessions.

There still is a long way to go, and I fully intend to be back on the bike and
on the hills at some point, but I'm delighted with where I've got to.

[1]: http://www.crossfitforthvalley.com/ "CrossFit Forth Valley home page"

[2]: https://www.theweeyogaroom.com/ "Wee Yoga Room website"

[Home](/blog "ohthreefive")

