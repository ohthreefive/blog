+++

date = "2019-01-01T21:08:37Z"
title = "2018 movies"
description = "Films I've seen this year: ranked"
tags = [ "movie" ]

+++

1. Mission: Impossible - Fallout (IMAX)
    * Easily the best action movie I've seen in the cinema since the
      original Raid. Edge of my seat. Jaw on the floor.
2. Avengers: Infinity War (IMAX)
    * I still have some issues about permanence and consequences, but
      perhaps I should just trust in Marvel to see it through. Highly
      re-watchable.
3. Mary Poppins Returns
    * Practically perfect in every way.
4. Coco (cinema)
    * UK release was 2018. Another Pixar tear-jerker. It's a shame there
      hasn't been a truly hilarious Pixar film since Finding Nemo.
5. Spider-Man: Into The Spider-Verse (cinema)
    * Visually unique. Great entertainment.
6. Black Panther (cinema)
    * Really disappointing finale to an otherwise cracking movie
7. Ant Man and the Wasp (cinema)
    * Relatively _light_ next to Marvel's other two this year but good fun
8. Annihilation (home)
    * Good mix of horror and science fiction; wonderfully weird
9. Ready Player One (IMAX)
    * Disappointing

[Home](/blog "ohthreefive")
