+++
date = "2020-01-02T21:01:35.671Z"
title = "2019 movies"
description = "Films I’ve seen this year: ranked"
tags = [ "movie" ]

+++

* Avengers: Endgame (IMAX)
    * Flawless? No. Consequences? Yes. The MCU will (hopefully) never be the
      same again
* Shaun the Sheep: Farmageddon
    * Hilarious. Universal. Near perfect.
* Once Upon a Time in Hollywood (IMAX)
    * I'm not dying to see it again, but I enjoyed my hours in these people's
company.
* Us (home)
    * Tremendously enjoyable follow up to Get Out; made with such confidence.
* Joker
    * I have some major quibbles, but overall really enjoyed how weird and
      different this seemed. Gutted that a sequel seems inevitable.
* Captain Marvel (IMAX... i think)
    * Really soared in the third act; a confident debut for a difficult (in that
      she's too powerful) hero.
* How to Train Your Dragon 3: The Hidden World
    * An excellent conclusion to the story. Except I'm sure they'll be back.
* Toy Story 4
    * In retrospect, it's relatively forgettable for Pixar, and that's not good
      enough.
* Frozen II
    * I love the soundtrack, and overall think it's better than Frozen.
* Star Wars Episode IX: The Rise of Skywalker (IMAX)
    * A significant step in the wrong direction after The Last Jedi, and a
      disappointing conclusion.
* Spider-Man: Far From Home
    * An enjoyable watch but films like these are, I think, part of Scorsese's
      argument. It's a fun ride while your on it but you do get the feeling it's
      all just part of a big theme park.

[Home](/blog "ohthreefive")