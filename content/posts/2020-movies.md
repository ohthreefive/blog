+++
date = "2021-01-11T17:10:42.727Z"
title = "2020 movies"
description = "Movies I saw in 2020: ranked"
tags = [ "movie" ]

+++

* Parasite (cinema, pre-pandemic)
    * It feels like so long ago since I saw this, but it's a masterpiece
* Tenet (cinema, mid-pandemic)
    * I can't wait to see it again: I understood little but enjoyed a lot of
      what I saw
* Wolfwalkers (home, start of 'second' wave)
    * Cartoon saloon do it again - magical
* Uncut Gems (home, pre-pandemic)
    * A heart attack of a movie, in a good way
* Soul (home, lockdown two)
    * Pixar's best in years
* Fireball
    * Herzog on fine form

[Home](/blog "ohthreefive")