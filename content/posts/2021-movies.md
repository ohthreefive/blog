+++
date = "2022-05-04T07:16:45.248Z"
title = "2021 movies"
description = "Movies I saw in 2021: ranked"
tags = [ "movie" ]

+++

An even more lean list than 2020?

* Dune: Part One (cinema)
    * Similar relief to when they didn't ruin LotR and similar ecstasy that it
      was actually superb.
* Spider-Man: No Way Home (cinema)
    * A joyful cinema experience. Not sure it will elicit the same response at
      home, but it was the right movie at the right time.
* Ron's Gone Wrong (cinema)
    * An unexpected joy. A gentle movie with a nice message. The children loved
      it.
* No Time To Die (cinema)
    * Too long and yet two superfluous scenes were among my favourites.
* The Matrix Resurrections (home)
    * First act _WOW_; third act _YAWN_.
* Shang Chi and the Legend of the Ten Rings (cinema)
    * Third act _YAWN_.

[Home](/blog "ohthreefive")