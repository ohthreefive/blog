+++
date = "2023-01-31T17:28:05.928Z"
title = "2022 movies"
description = "Movies I saw in 2022: ranked!"
tags = [ "movie" ]

+++

The list of movies remains small, but the are some real **movie** movies in
here. Cinema is back

- Top Gun: Maverick (IMAX)
    - The best IMAX experience I've had since Fallout. Cruise is ON FORM.
- The Banshees of Inisherrin (home)
    - Shortly after it turned up on Disney+, I devoured this. So strange, so
      compelling, so funny, so tragic. I just thought it was wonderfully weird.
- Everything Everywhere All At Once (cinema)
    - A near-complete joy, but it was overlong, especially in the third act.
- Avatar: The Way of Water (IMAX)
    - Visually breathtaking, narratively conservative. I think the returning
      villain is a mistake, and the water clan just forgetting to take part in
      the last showdown was poor writing.
- The Unbearable Weight of Massive Talent (home)
    - I don't know which I preferred, Nicholas Cage having the time of his life
      or Pedro Pascal being adorable.
- Prey (home)
    - A simple reminder of how economical a good chase movie can be. Wish I'd
      had a chance to see it in the cinema.
- My Father's Dragon (home)
    - Not up there with Wolkwalkers or Song of the Sea, this was nevertheless
      joyfully strange in a world of same-y animation (see 'Luck').
- Dr Strange and the Multiverse of Madness (home)
    - Suprisingly bland, and I loathed the drop in Illuminati scene.
- Thor: Love and Thunder (home)
    - All the worst traits of Ragnarok with little of the good bits.
- Luck (home)
    - Really, really, completely, average.
- The Gray Man (home)
    - Laughable. As Simon Mayo put it, "So derivative." And it looked
      *terrible*.

[Home](/blog "ohthreefive")