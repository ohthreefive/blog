+++
date = "2024-02-20T18:10:51Z"
title = "2023 movies"
description = "Movies I saw in 2023 - ranked!"
tags = ["movie"]

+++

## A good year for quality, an average year for quantity

- Jurassic Park
    - 30 years ago I thought they had made a movie just for me. Now I was able to take my children to see it. Neither are dinosaur fans. Both loved the film. It was interesting to see where the visual effects looked ropey. It was very interesting to see where they held up. And it was unsurprising to see all the practical effects had held up gloriously.
- Mission: Impossible - Dead Reckoning, part one (IMAX)
    - On first viewing, my main thought was, “Not as good as fallout,” which is unfair - very little is. I managed not to have seen the bike stunt before seeing it on IMAX. It is the current pinnacle of stunts as far as I’m concerned. He recorded that dialogue in free fall. Wow.
- Spider-Man: Across the Spider-Verse (IMAX)
    - I failed in my role as a father and we missed the first 10 minutes. Like M:I 7, this was on first viewing less satisfying than ‘Into’. Having re-watched it, I stand by that. After the train chase, the whole film has to reset and re-build momentum to its conclusion.
- Oppenheimer (IMAX)
    - Yes, deserving of all the plaudits, but the three above it beat it for entertainment.
- Wonka
   - I *liked* it in the cinema. I now *love* the soundtrack and look forward to seeing it again.
- Puss in Boots: The Last Wish
   - Proof that animation studios have learnt from Spider-Verse. From the opening scene my jaw dropped at the look of it. Just terrific.
- Teenage Mutant Ninja Turtles: Mutant Mayhem
    - Again, an interesting visual style and a genuinely fun film.
- The Creator
    - Oh dear to the plot. Plenty of ideas. Nice visuals.
- The Super Mario Bros. Movie
    - Just so boring in looks and in content.

## Honorable mentions

- RRR (home)
    - I was unsubscribing from Netflix but thought I should see this first. Just wow!
- Barbie (plane)
    - Really fun and funny.
-  The Holdovers (plane)
    - Really heartwarming. I hope Paul Giamatti gets an Oscar.

[Home](/blog "ohthreefive")