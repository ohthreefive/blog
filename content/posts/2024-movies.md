+++
date = "2025-01-01T20:55:57Z"
title = "2024 movies"
description = "Movies I saw in 2024, ranked!"
tags = ["movie"]

+++

## 2024 movies

### Back to the Future

The only film better than Dune this year is one of the greatest films ever. The IMAX were showing this so I took the opportunity to introduce the children to it. I had a huge fondness for it but having not seen it in years (decades?) and never in the cinema, I didn't realise how good it was. A packed screening of parents taking their children for the same reasons only made it better. Just wonderful.

### Dune: Part 2

An outstanding second part to a difficult novel. I've re-watched it numerous times. The soundtrack, especially 'Kiss the Ring' has also been on repeat. Well done!

### The Boy and the Heron

My first Ghibli cinema experience didn't let me down. Another soundtrack masterpiece too.

### Wallace and Gromit: Vengeance Most Fowl

My 4th film of the year that reaches near perfection, and the only film on this list watched at home for obvious reasons. Even at 'feature length' it's perfectly short and tight. Well done Aardman. Again.

### The Wild Robot

The first half of this movie would have been on the top tier of the list, with humour that the kids loved (a lot of slapstick) and the parents appreciated for other reasons too (the truth of raising a child). It drops off a little after that. Another piece of beauty as far as the animation goes. DreamWorks really can mix it with the best.

### Furiosa

I enjoyed it and felt it passed in a flash, but barely remember it now.

### Wicked

Fuck me it's long. Looked beautiful, great tunes, but why so long?

### Moana 2

Didn't work for me at all.

### Kung Fu Panda 4

Really really poor. Including the animation, which made it look almost old fashioned compared to for example Puss in Boots or The Wild Robot from the same studio.

[Home](/blog "ohthreefive")