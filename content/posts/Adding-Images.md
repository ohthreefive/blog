+++

date        = "2017-02-11T15:58:25Z"
title       = "Adding Images"
description = "Adding images to Hugo posts"
tags  = [ "meta" ]

+++

### PICCIES!

Adding images to posts isn't something I plan to do much of but I want to be
able to do it when I need/want.

It wasn't exactly self explanatory but the [top result of a search][1] told me
to add images to the `/static/` directory. I decided to put them in a sub
directory `/static/images`.

When I come to do the markdown link it's as simple as `/images/filename.jpeg/`.

On my iPad, I've sorted a quick [Workflow][2] which will take a local image,
re-size it to 480 px wide (height is automatic), then show the Open In dialogue
so I can send it to [Working Copy][3]. The Working Copy dialogue is nice: it
lets you pick a directory (***essential***) and rename the file (*desirable*.)

Next thing I'll try is to do the same workflow but with the clipboard as the
input rather than the photo picker (saves me downloading images I don't need on
my iPad) and I'll add a step to set clipboard. this means I can quickly paste my
desired file name in to Working Copy and again paste it in [Editorial][4].

Let's test it out with a picture of the Hugo icon:

![Hugo logo][pic1]

[1]: https://discuss.gohugo.io/t/solved-how-to-insert-image-in-my-post/1473 "Hugo forums"

[2]: https://workflow.is/ "Workflow website"

[3]: https://workingcopyapp.com/ "Working Copy website"

[4]: http://omz-software.com/editorial/ "Editorial website"

[pic1]: https://ohthreefive.gitlab.io/blog/images/Hugo.png "Hugo logo"

[Home](/blog "ohthreefive")
