+++

date        = "2017-02-22T12:24:08Z"
title       = "Android Password Manager"
description = "How to set up Password Manager from F-droid for Android devices"
tags  = [ "fdroid" , "android" ]

+++

### PASSWORD STORE - AWESOME PASSWORD MANAGER FOR ANDROID

This is a re-post of a post I put on tumblr almost exactly a year ago. It was
very long-winded so I've edited it and changed anything that's now changed. I've
recently repeated the whole setup for my Nexus 5X and CopperheadOS.

Here it is:

### SETTING UP PASSWORD STORE FOR ANDROID

#### WHY?

I’ve largely moved from iOS ([1Password][1] did the job there) and my iPad as a
mobile daily driver to a Raspberry Pi/Ubuntu Mate laptop combination.
I was looking for an open source password alternative and found "[The Standard
Unix Password Manager][2].”

#### BACKGROUND

I set up `pass` on my home machine and that was the first I'd played with `gpg`.
I regret not writing down the steps I'd followed when setting it up then and
subsequently the first time I'd set it up on Android so I thought I'd document
it this time.

Password Store for Android is available from the Play Store or f-droid
(recommended for the most up to date packages.) [Here is a link][3] to download
the package (.apk) directly but the best way is to download the f-droid package
manager and let it keep Password Store up to date.

The steps I’ve documented here are largely from the [Read Me][4] page for
Password Store on GitHub. Not mentioned on the f-droid page or GitHub read me is
that you’ll also need the app [OpenKeychain][5] to handle the encryption-y [^1]
side of things.

#### SSH KEY FOR COMMUNICATION WITH REPOSITORY

First we get the splash screen:

![Password Store splash screen](/images/PassSplash.png "look top right!")

Jump straight in to settings:

![Settings screen](/images/PassSettings.png "lots of words I barely know the meaning of")

Select to generate SSH key pair. Leave length at 2048 and chose whether to add a
passphrase and comment. I use both and add a comment because I forget to jog my
memory in future! For example, the one on my Nexus 6 says "SSH key generated in
Android Password Store for syncing with gitlab,” which is pretty descriptive!
Once you have entered the details hit `GENERATE`. This shouldn't take long.

The [instructions][6] then say "Press Copy to copy the public key and add it to
your ssh server.” For me, this means put my public SSH key on to my GitLab
repository. (I use GitLab rather than GitHub for this; you use what you want.)
Simply copy-paste it in to GitLab’s SSH keys section. On GitLab this is
[here][7]. The text of the key starts with `ssh-rsa`, ends with the comment we
typed; the key is in between.

#### GPG KEY FOR ENCRYPTION

I already have `pass` running on my home machine, which means I already have my
encrytpion key and I need this same key on my phone (via `gpg`-fu [^2].) There
are instructions for Password Store which say:

> ### Export your gpg private key
> 
> *   Get your pass script gpg id(s) ie: `cat ~/.password-store/.gpg-id`
> *   You can also get a full ids list using `gpg -k`
> *   Export your private key with
> 
> `gpg --export-secret-key [the_id] > keys.asc`
> 
> Import it in OpenKeychain

I also think OpenKeychain can generate one for you.

I followed the above instructions at the terminal on my home Ubunut machine. I
used the awesome [Syncthing][8] to transfer `keys.asc` to my phone. Do it
however you want.

Once the gpg key is on our device’s memory we need to get it in to OpenKeychain.
The OpenKeychain splash screen is friendly and the import option is obvious.

![OpenKeychain splash screen](/images/OpenKeySplash.png "big ol' robot!")

Navigate to wherever `keys.asc` is kept in our file system and import! This step
is surprisingly easy.

#### RE-CAP

A quick re-cap on what we’ve done:

1.  Already had pass running on a home Linux machine (pushing to a Gitlab
    repository in my case)
2.  Installed f-droid, Password Store and OpenKeychain on an android phone
3.  Dived in to Password Store’s settings, created an SSH key, copied the
    **public** key to Gitlab
4.  Gone back to my Linux machine and followed instructions to export the
    ***private*** `gpg` key with which all my passwords are encrypted to a file
    called `keys.asc`
5.  Imported this file to the OpenKeychain app

**WE’RE NEARLY THERE!**

#### COMMUNICATING BETWEEN PASSWORD STORE AND GITLAB

Next go back to the Password Store splash screen and select ‘clone existing.’
It will ask whether to store the local repository on 'SD-card’ or 'hidden
(preferred.)’ We’ll choose hidden. Because it’s preferred <sup>3(#foot3)</sup>.

*I can't remember if this option appears anymore*

The next screen was daunting when I first saw it:

![Git server settings](/images/GitRepo.png "YIKES!")

If we fill in 'Username', 'Server URL' and 'Repo Path', 'Resulting URL' starts
to fill in. This was *really* helpful, because I didn’t know what to enter in
'Resulting URL'. The syntax of 'Resulting URL' looked familiar. Back on
gitlab.com, if I click on the 'project’ for `pass`, it shows an URL for the
project with the same syntax, so that would be another place to look! In my
case: `git@gitlab.com:[myusername]/my-passwords.git`

If I put this in 'Resulting URL', the first three fields auto-complete as we
fill 'Resulting URL'.

#### GET THOSE PASSWORDS!

Now hit 'CLONE!’. . . And get a scary message! "Directory already exist” and "do
you want to delete?” I remember coming across this when I did all this for my
Nexus 6\. I’m sure I remember reading somewhere that it’s something the dev
wants to take out and you should just hit delete. So press delete.

*this might be out of the app by now*

My SSH key has a passphrase, so I entered it, hit OK and a command ran
(presumably a `git pull`.) In less than 5 seconds my passwords were on my phone.

#### DECRPYT THOSE PASSWORDS

Password Store needs to access the `gpg` key to decrypt the passwords (and
encrypt new/edited passwords.) Go to settings and scroll to the Crypto section.
The first option is Select OpenPGP Provider. Only OpenKeychain can be selected.
Select it. Then under Select key you should see a list of our keys (which might
be just one.) Select it. That should be it. Now we can decrypt the passwords!

---

[^1]: "encryption-y" - I’m soooo a hacker!

[^2]: Pretty sure I mentioned I’m a awesome hacker

<a name="foot3">3</a>: Natch!

[1]: https://itunes.apple.com/gb/app/1password-password-manager/id568903335?mt=8 "superb password manager (UK App Store link)"

[2]: https://www.passwordstore.org/ "pass website"

[3]: https://f-droid.org/repository/browse/?fdfilter=password+store&fdid=com.zeapo.pwdstore "you can also download f-droid from this link"

[4]: https://github.com/zeapo/Android-Password-Store "Password Store on GitHub"

[5]: https://f-droid.org/repository/browse/?fdfilter=password+store&fdid=org.sufficientlysecure.keychain "f-droid web link - better to download from f-droid app"

[6]: https://github.com/zeapo/Android-Password-Store/blob/HEAD/README.md "Password Store on GitHub"

[7]: https://gitlab.com/profile/keys "assuming you're logged in to GitLab..."

[8]: https://syncthing.net/ "more open source goodness"

[Home](/blog "ohthreefive")
