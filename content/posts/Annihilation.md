+++

date = "2018-03-15T08:38:30Z"
title = "Annihilation"
description = "Brief movie reviews #20"
tags = [ "movie" ]

+++

**one minor spoiler ahead**

‘Theatrically’ released on Netflix in the EU - I’m unsure if this decision was made before or after it[only earned $27m  in US on $40m budget][1] - Annihilation in this regard was a first for me. Duncan Jones’ Mute would have had this privilege but I avoided it after the very poor reviews. Shortly before its Netflix release, I listened to the audiobook, though the plot of the movie is only very loosely related to the book.

There were many subtly explored ideas in Annihilation, and these were the highlights. There were also some surprising clunkers, for example when Lena’s work colleague touched her elbow early in the movie, I thought it was an oddly intimate interaction. I wondered if an affair was being implied. Later, it’s spelled out in capitals. Omit one of the two scenes/moments, and you leave the audience thinking. Do it this way, you leave them expecting everything to be answered.

Annihilation does not answer everything, however, and it’s much better when it doesn’t.

3/5 

[Home](/blog "ohthreefive")

[1]: http://www.boxofficemojo.com/movies/?id=annihilation.htm "Annihilation on Box Offic Mojo"
