+++

date = "2018-08-10T09:03:45Z"
title = "Ant Man and the Wasp"
description = "Brief movie reviews #24"
tags = [ "movie" ]

+++

Marvel’s first since Infinity War is fun but thin. Interested to find out what
happens with Janet van Dyne and whatever powers she might have. It also left me
wanted more Wasp. I guess it’s a positive thing for a movie to leave you wanting
more! Overall, however, since Ragnarok, I’m having serious trouble with the lack
of definitive endings to Marvel’s movies, given they all exist in this never
ending cinematic universe.

3/5

[Home](/blog "ohthreefive")

