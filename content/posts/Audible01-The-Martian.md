+++

date        = "2017-02-11T14:17:21Z"
title       = "The Martian"
description = "Audible audiobook listens #1"
tags  = [ "Audible" ]

+++

### THE MARTIAN

I'm going to document my audible listens and (*briefly*) my opinions here.

I've been an audible member since late 2015 so I'm first going to go back
through my archive.

Audible is a sponsor of the podcast [TWiT][1] and the host talks very highly of
both it and, for the months around my decision to subscribe to audible, this
book.

I thought it was excellent. Highly entertaining. Fun and gripping. It's
occasionally over-technical but that's my only gripe.

Ok, let's do star ratings: 4/5

![The Martian - book cover][pic1]

[1]: https://twit.tv/shows/this-week-in-tech "TWiT on the web"

[pic1]: https://ohthreefive.gitlab.io/blog/images/TheMartian.jpeg "The Martian - book cover"

[Home](/blog "ohthreefive")
