+++

date        = "2017-02-12T09:57:43Z"
title       = "Dune"
description = "Audible audiobook listens #2"
tags  = [ "Audible" ]

+++

### DUNE

Frank Herbert's sci fi classic is getting [another big screen treatment][1] by
Denis Villeneuve. It's a dense and wonderful novel. Listeners should bear in
mind that most chapters are about an hour so you can't squeeze them in on a
quick dog walk or short drive!

I must re-visit Dune again soon; it's wonderful. 5/5

![Dune book cover][pic1]

### The re-read (September 26th, 2021)

So much better the second time around and no, the chapters aren't as long as I
had thought! Lies within lies within lies.

### The three-read (April 18th, 2024)

I am currently obsessed with the movies, so re-visited this again. It was as much improved by a third read than it was by a second. I was struck this time by the speed of the conclusion. But I was able to keep track of all threads and all characters throughout this time, even Fenring.

[1]: http://www.empireonline.com/movies/dune/denis-villeneuve-confirmed-director-dune-remake/ "Empire news story"

[pic1]: https://ohthreefive.gitlab.io/blog/images/Dune.jpg "Giant spice worm!"

[Home](/blog "ohthreefive")
