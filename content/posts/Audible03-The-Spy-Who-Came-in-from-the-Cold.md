+++

date        = "2017-02-12T14:20:02Z"
title       = "The Spy Who Came in from the Cold"
description = "Audible audiobook listens #3"
tags  = [ "Audible" ]

+++

### THE SPY WHO CAME IN FROM THE COLD

A little bit of Cold War action from le Carré and it didn't disappoint. Double
agents, double crossing and double quick time - this novel doesn't hang about.

Oozes quality; thoroughly gripping. 4/5

![The Spy Who Came In From the Cold cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/SpyCold.jpg "book cover"

[Home](/blog "ohthreefive")
