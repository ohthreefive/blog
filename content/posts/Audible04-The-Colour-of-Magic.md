+++

date        = "2017-02-12T17:34:56Z"
title       = "The Colour of Magic"
description = "Audible audiobook listens #4"
tags  = [ "Audible" ]

+++

### THE COLOUR OF MAGIC

I'd always thought (and been told) that I'd love the Discworld series. Sadly I
didn't. To an extent I wonder if the dense any chaotic writing style doesn't
lend itself to an audiobook. I haven't re-visited the Discworld but maybe I'll
give it another try one day. Bonus point for the constant hilarity that is the
luggage though!

2/5

### The re-read (August 13th, 2021)

No doubt improved on second read, I still found myself losing interest in the
ever-changing adventures of our two protagonists before the end.

![Colour of Magic book cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/ColourofMagic.jpg "Colour of Magic book cover"

[Home](/blog "ohthreefive")
