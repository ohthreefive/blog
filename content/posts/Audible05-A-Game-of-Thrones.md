+++

date        = "2017-02-12T17:41:54Z"
title       = "A Game of Thrones"
description = "Audible audiobook listens #5"
tags        = [ "Audible" ]

+++

The Song of Ice and Fire caught me completely off guard. I thought it would be
fan fiction quality fluff but at least entertaining. These thoughts were
entirely based on ignorance!

It's dense, alive and wonderfully vicious.

5/5

### The re-read (April 22nd, 2018)

GoT became my first audible re-read. I love the books, but they’re so dense
(richly so) that surely a second read would be rewarding?

Correct! I was able to understand and enjoy the book so much more second time.

Interesting tidbit: Lady Stark foreshadows Lady Stoneheart at one point, saying
something like, "I have turned my heart to stone," when grieving about Ned and
her daughters’ plights.

![Game of Thrones cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/GoT.jpg "Game of Thrones cover"

[Home](/blog "ohthreefive")
