+++

date        = "2017-02-12T17:57:11Z"
title       = "The Looking Glass War"
description = "Audible audiobook listens #6"
tags  = [ "Audible" ]

+++

I didn't enjoy this le Carré half as much as Spy. It felt more... predictable.

2/5

![Looking Glass War book cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/LookingGlassWar.jpg "Looking Glass War book cover"

[Home](/blog "ohthreefive")
