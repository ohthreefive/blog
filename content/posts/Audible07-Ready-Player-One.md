+++

date        = "2017-02-12T17:48:30Z"
title       = "Ready Player One"
description = "Audible audiobook listens #7"
tags  = [ "Audible" ]

+++

This was the first book I downloaded due to a recommendation in the Audible app.
It's a really entertaining bit of fluff, though most of the gaming references
went over my head!

3/5

![Ready Player One book cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/ReadyPlayerOne.jpg "Ready Player One book cover"

[Home](/blog "ohthreefive")
