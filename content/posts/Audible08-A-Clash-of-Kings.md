+++

date        = "2017-03-09T20:30:46Z"
title       = "A Clash of Kings"
description = "Audible audiobook listens #8"
tags  = [ "Audible" ]

+++

### RETURN TO WESTEROS!

Since loving GoT, I'd had a minor le Carré disappointment and a pretty enjoyable
Ready Player One. But GoT was the last listen I'd **really** loved, so I went
back to the series and wasn't disappointed.

Dialling the drama up a notch and making Tyrion such a focus makes this probably
the most satisfying of the series so far. Highly recommended!

5/5

### The re-read (July 4th, 2018)

Filling the void of no new GoT on TV or paper, I continued re-reading the series
with CoK. Another fantastic re-read. A superb novel. One little thing that stood
out: the description of Storm’s End as an impenetrable fortress. Scene for a
last stand against the dead?

![Clash of Kings cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/ClashOfKings.jpg "Clash of Kings cover"

[Home](/blog "ohthreefive")
