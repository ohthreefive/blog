+++

date        = "2017-03-09T20:58:42Z"
title       = "A Storm of Swords"
description = "Audible audiobook listens #9"
tags  = [ "Audible" ]

+++

### THINGS KEEP DIALLING UP A NOTCH!

Westeros continues apace with another cracking novel. But MY WORD is it long!

The perma-put-upon Starks get even more put upon. Tyrion goes from hero to zero.
Danaerys has a go at being a ruler. Brienne arrives! Yup, another cracker.

5/5

![Storm of Swords cover][pic1]

### The re-read (31st May, 2021)

The most eventful of the Thrones novels so far: Catelyn, Robb, Tywin, Joffrey, Balon, Jamie's hand, Arya leaves, Sansa the pawn, The Kingslayer Brothers. 

[pic1]: https://ohthreefive.gitlab.io/blog/images/StormOfSwords.jpg "Storm of Swords cover"

[Home](/blog "ohthreefive")
