+++
date = "2023-02-06T21:31:22.045Z"
title = "A Dance of Dragons"
description = "Audible audiobook listens #10.5"
tags = [ "Audible" ]

+++

It looks like I never left a review for the last (latest?) GoT novel! But now
it's time for...

### The Re-read (31st Jan, 2023)

Wow. It's taken about 4 years to re-read the series! ADOD has truly the most
action packed final chapters of any book I've read for some time. Quentin
Martell -  oh no! Varys vs Pycelle & Kevin! Tyrion tries to win the Second Sons.
Danaerys meets a new Khalasar. Cersei meets Robert Strong! Griff & Egg! The end
of the Bartheons? The end of Snow? Wow!

5/5

[Home](/blog "ohthreefive")