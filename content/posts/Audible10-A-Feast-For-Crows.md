+++

date        = "2017-03-14T21:56:21Z"
title       = "A Feast For Crows"
tags  = ["Audible"]
description = "Audible audiobooks listens #10"

+++

I ploughed on through the Song of Ice and Fire series and stumbled blindly in to
Feast for Crows without knowing that it was one half of a narrative with the
fifth book in the series. Top characters Jon, Tyrion and Danare.., Dineerez..,
eh, Dany are absent in this 'half.'

It's a more navel-gazing and reflective novel, using Brienne's travels to
demonstrate the impact of the war on Westeros.

The comparative lack of action does detract from the excitement but it was still
a gripping read.

4/5

![Feast for Crows cover][pic1]

### The re-read (16th March, 2022)

The split narrative made FFC the most challenging ASOIAF novel first time round,
but it was highly satisfying on second read, now that I know the characters better.

[pic1]: https://ohthreefive.gitlab.io/blog/images/Feast_for_Crows.jpg "A Feast for Crows cover"

[Home](/blog "ohthreefive")
