+++
date = "2022-02-03T08:29:20.401Z"
title = "A Memory of Light"
description = "Audible audiobook listens #101"
tags = [ "Audible" ]

+++

A 41 hour audiobook consumed in 20 days? Yes, please. An outstanding end to the
epic series. I will now read about this series extensively and continue to
lament Amazon's disappointing first season.

5/5

[Home](/blog "ohthreefive")