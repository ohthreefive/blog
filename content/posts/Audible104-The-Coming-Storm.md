+++
date = "2022-04-23T07:10:09.043Z"
title = "The Coming Storm"
description = "Audible audiobook listens #104"
tags = [ "Audible" ]

+++

Much of the content of this has come up, either in in his podcast, "Against the
Rules," or his book, "The Fifth Risk," but Michael Lewis' ongoing love affair
with unsung heroes usually in government roles continues to be fascinating and
was well worth a couple of hours.

4/5

[Home](/blog "ohthreefive")