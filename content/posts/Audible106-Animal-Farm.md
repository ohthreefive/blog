+++
date = "2022-05-15T09:07:59.470Z"
title = "Animal Farm"
description = "Audible audiobook listens #106"
tags = [ "Audible" ]

+++

My first read of this since being a child, at which point communism was entirely
unknown to me. It is utterly concise and brutal. Really heartbreaking stuff.

[Home](/blog "ohthreefive")