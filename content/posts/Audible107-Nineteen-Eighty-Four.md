+++
date = "2022-06-02T21:44:25.755Z"
title = "Nineteen Eighty-Four"
description = "Audible audiobook listens #107"
tags = [ "audible" ]

+++

Lean and mean. In 2022, it suffers from a lack of freshness - its ideas have
been used so often - but that is due to its success.

[Home](/blog "ohthreefive")