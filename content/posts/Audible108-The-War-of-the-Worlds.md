+++

title       = "The War of the Worlds"
description = "Audible audiobook listens #108"
date        = "2022-06-28"
tags        = [ "Audible" ]

+++

Staggering to think how long ago this was written. It's horrific, fast-paced and
lean.

[Home](/blog "ohthreefive")
