+++
date = "2022-07-24T13:36:04.404Z"
title = "The First Men in the Moon"
description = "Audible audiobook listens #109"
tags = [ "Audible" ]

+++

It's astonishing that this is over 120 years old. Modern knowledge makes some
ideas preposterous - of course the moon has no atmosphere or surface life! - but
to criticise the novel for that is churlish. Enjoyable, weird stuff!

[Home](/blog "ohthreefive")