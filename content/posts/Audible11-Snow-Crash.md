+++

date = "2017-03-25T18:00:28Z"
title = "Snow Crash"
description = "Audible audiobook listens #11"
tags = [ "Audible" ]

+++

I'd been introduced to Neal Stephenson with [Quicksilver][1]. It was a gift
from my sister who had been recommended it by a friend whom she thought had
similar tastes to me.

I found it impossibly dense and struggled to engage with it.

I heard about Snow Crash, one of his earlier novels, and decided to give it a
go. It was a fantastic read, other than its historical/investigative sections,
which dragged compared to the forward thrust of the story.

It'll definitely take another listen and I'll also be more than happy to take on
more Neal Stephenson after this.

4/5

### The re-read (February 2nd, 2024)

Interesting re-read. Excellent opening chapter. Wonderful character names (‘Hiro Protagonist’). But a bit childish/leery in places. As a father of a now seven-years-older-than-she-was-when-I-first-read-it daughter, I found YT’s casual sexuality (she’s 15 in the novel) somewhat unacceptable. Maybe I’m a prude. I was losing interest towards the end. I’d probably demote this to 3/5 on re-read.

![Snow Crash book cover][pic1]

[1]: https://www.theguardian.com/books/2003/oct/25/featuresreviews.guardianreview20 "Quicksilver - Guardian book review"

[pic1]: https://ohthreefive.gitlab.io/blog/images/SnowCrash.jpg "Snow Crash book cover"
