+++
date = "2022-07-28T10:18:55.888Z"
title = "The Time Machine"
description = "Audible audiobook listens #110"
tags = [ "Audible" ]

+++

Despite a couple of instances of jarring racism and sexism, this centuries old
novel, little more than a short story, remains a thing of wonder. Wells' musings
on the future (and descent) of humanity are fascinating and his brief foray
towards the death of the Sun is horrific.

Where did the time traveller go in the end?

5/5

[Home](/blog "ohthreefive")