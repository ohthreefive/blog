+++
date = "2022-08-25T06:13:00.561Z"
title = "The Invisible Man"
description = "Audible audiobook listens #111"
tags = [ "audible" ]

+++

The penultimate of the Wells collection on Audible is a fascinating tale of a
man's obsession with his work leading to the corruption of his soul. He arrives
on the page on the run and elicits some sympathy. His behaviour quickly becomes
unacceptable and by the time we get to his re-telling of his backstory, he's
already the villain.

4/5

[Home](/blog "ohthreefive")