+++
date = "2022-09-05T05:17:59.247Z"
title = "The Island of Doctor Moreau"
description = "Audible audiobook listens #112"
tags = [ "audible" ]

+++

The last of the HG Wells collection from Audible. I had neither read it before
nor seen the movie (though I had heard about the movie - who hasn't?)

Another simple and lean tale. A brilliant scientist goes too far. Interesting
ideas are discussed. Many concepts still feel modern. Wells truly was a
visionary.

3/5

[Home](/blog "ohthreefive")