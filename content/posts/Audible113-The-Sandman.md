+++

title       = "The Sandman: Act I"
description = "Audible audiobook listens #113"
date        = "2022-09-25"
tags        = [ "audible" ]

+++

A truly excellent production by Audible, and my introduction to Morpheus.

I wasn't sure what to expect, and I am yet to become a major Gaiman fan. The
opening few chapters were excellent and captivating, as Dream is captured,
escapes and re-claims his tools.

Thereafter there is little of a driving narrative. Some of the individual
stories hit on their own, others are less interesting.

Overall the lack of an obvious driving narrative was too much of a downside for
me, and I won't visit Act II in a hurry.

3/5

[Home](/blog "ohthreefive")
