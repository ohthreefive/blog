+++
date = "2022-10-10T11:04:02.351Z"
title = "Cloud Cuckoo Land"
description = "Audible audiobook listens #114"
tags = [ "Audible" ]

+++

I think I heard about this novel on the [TWiT podcast] [1] and I thought I would
go for it - something out of the blue.

It's an effort and a half to get into it, but satisfying come the end. Not earth
shattering, but a nice self-contained novel.

3/5

[Home](/blog “ohthreefive”)

[1]: https://twit.tv/ “This Week in Tech podcast network homepage”