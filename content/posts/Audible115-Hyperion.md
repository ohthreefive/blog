+++
date = "2022-10-24T18:40:38.693Z"
title = "Hyperion"
description = "Audible audiobook listens #115"
tags = [ "Audible" ]

+++

Maybe it's being a father, but from Part Four: "The River Lethe's Taste Is
Bitter", I was just hooked. Such a heartbreaking tale. It doesn't really provide
a satisfying narrative on its own, but I enjoyed it sufficiently that I'll
definitely need to explore the Fall of Hyperion soon.

[Home](/blog "ohthreefive")