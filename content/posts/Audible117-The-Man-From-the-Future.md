+++
date = "2022-11-21T17:18:26.795Z"
title = "The Man From the Future: The Visionary Life of John von Neumann"
description = "Audible  audiobook listens #117"
tags = [ "Audible" ]

+++

I was really quite excited about this one. The biography however is ponderous
and much more work-centric than person-centric. The main areas to which von
Neumann made incredible contributions are discussed in depth, but as much is
made of everyone else's contributions as is our subject's. A disappointment.

2/5

[Home](/blog "ohthreefive")