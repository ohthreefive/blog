+++
date = "2022-12-10T20:39:37.915Z"
title = "The Seven Moons of Maali Almeida"
description = "Audible audiobook listens #118"
tags = [ "Audible" ]

+++

For the second Booker winner in a row I've been somewhat put off by an
unconventional narrative style. I preferred this to The Promise, but neither
have been up there with Shuggie Bain, despite the horrors therein. Still, much
to enjoy.

3/5

[Home](/blog "ohthreefive")