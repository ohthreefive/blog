+++
date = "2023-01-02T14:56:54.355Z"
title = "Northern Lights"
description = "Audible audiobook listens #119"
tags = [ "Audible" ]

+++

My interest in the _His Dark Materials_ trilogy was re-kindled by the television
series then a desire to re-read the first two _Book of Dust_ novels in
anticipation of the third.

In stepped the children, however, who wanted to listen to this book. I think I
originally read them some time in my late teenage years, and I'd completely
forgotten how simple and direct the narrative is. That made Northern Lights a
fantastically pacy and captivating read.

Better second time round than first? Definitely.

5/5

[Home](/blog "ohthreefive")