+++

description = "Audible audiobook listens #12"
date = "2017-04-03T13:32:41+01:00"
title = "Sapiens"
tags = ["Audible"]

+++

I can't remember exactly where I heard about this novel but it was a wonderful
journey through the history of humankind. It managed to be superficial enough to
remain a fast-paced read but deep enough to achieve profundity on many
occasions.

4/5

![Sapiens - book cover][pic1]

[pic1]: https://ohthreefive.gitlab.io/blog/images/Sapiens.jpg "Sapiens - book cover"

[Home](/blog "ohthreefive")
