+++
         
date = "2023-01-21T20:47:33.402Z"
title = "The Fall of Hyperion"
description = "Audible audiobook listens #120"
tags = [ "Audible" ]

+++

The original Hyperion got me in the end - I loved it.

The sequel was a vastly different novel, but equally compelling.

Stories came together and concluded beautifully. Top sci-fi.

4/5

[Home](/blog "ohthreefive")