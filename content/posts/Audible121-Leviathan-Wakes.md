+++
date = "2023-02-06T21:38:53.790Z"
title = "Leviathan Wakes"
description = "Audible audiobook listens #121"
tags = [ "Audible" ]

+++

I'm on a sci-fi binge right now and, having watched the excellent series on
Amazon, this wasn't an adventurous pick. However, it's a well-written narrative
(faithfully adapted too) and the thought that has gone into existence among the
planets is awesome.

I've done straight into book 2

4/5

[Home](/blog "ohthreefive")