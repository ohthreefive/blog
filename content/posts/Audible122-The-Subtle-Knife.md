+++
date = "2023-02-26T10:26:31.090Z"
title = "The Subtle Knife"
description = "Audible audiobook listens #122"
tags = [ "Audible" ]

+++

Back when I originally read this trilogy, most of my friends preferred this to
Northern Lights, while I preferred the first book. Having re-listened, I'm more
affectionate towards Subtle Knife, but still prefer Northern Lights. Subtle
Knife is more uncertain - characters are doing things they think they should but
they're not sure why. No one knows what the endgame is. And come the conclusion,
it's very 'middle part of a trilogy'. Still, I'm struck with how efficient
Pullman's writing is. Maybe it's years of reading tomes like GoT and other dense
stories, this just feels so lean and focused. It's pacing is similar to the
Hobbit - one thing after another *happens* but despite the speed and development
of the narrative, it doesn't feel rushed.

On to the Amber Spyglass!

4/5

[Home](/blog "ohthreefive")