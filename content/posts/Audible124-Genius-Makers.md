+++
date = "2023-03-21T08:04:33.163Z"
title = "Genius Makers: The Mavericks Who Brought AI to Google, Facebook, and the World"
description = "Audible audiobook listens #124"
tags = [ "Audible" ]

+++

I immediately added this to my wish list when it was released (about a year ago,
I think.)

It languished there, frequently overlooked, and I removed it about two months
ago.

Then ChatGPT and all that happened and the [Next Big Idea podcast] [1] re-
published its interview with the author.

My interest was piqued and I dove in. It's a fast-paced and engaging read.
Somewhat paradoxically, given that recent developments are what made me
interested, my attention wandered more towards the end of the narrative. Given
how short it is, I highly recommend it to anyone looking for an academic
background to how we got here.

[Home](/blog "ohthreefive")

[1]: https://nextbigideaclub.com/podcast/ “link to website for the Next Big Idea podcast”