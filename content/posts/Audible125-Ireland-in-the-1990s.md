+++
date = "2023-03-28T16:21:52.581Z"
title = "Ireland in the 1990s: The Path to Peace"
description = "Audible audiobook listens #125"
tags = [ "Audible" ]

+++

I've wanted to know more history for years, and particularly history close to
home. Hearing an American accent was a little off-putting, but this is a whistle
stop tour through the troubles, and has informed me that Senator George Mitchell
and John Major were more important figures in the Good Friday agreement than
Tony Blair or President Clinton.

The threat level in Northern Ireland was raised to 'severe' today for the first
time in twelve years. It's just horrendous.

4/5

[Home](/blog "ohthreefive")