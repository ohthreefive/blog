+++
date = "2023-04-13T17:34:05.247Z"
title = "The Amber Spyglass"
description = "Audible audiobook listens #126"
tags = [ "Audible" ]

+++

Wow I'd forgotten how openly anti-religious this book was!

The sprawling nature of this narrative strains against the short, lean writing
style - you have to go with it more in this while Lights and Knife swept you
along.

A marvellous trilogy.

4/5

[Home](/blog "ohthreefive")