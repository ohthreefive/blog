+++
date = "2023-04-13T17:36:34.909Z"
title = "Abaddon’s Gate"
description = "Audible audiobook listens #127"
tags = [ "Audible" ]

+++

Three books in and I found myself bored for the first time by the Expanse.

I think I found this series of the TV show a bit dull too - the Slow Zone just
not as exciting as protomolecules?

Or maybe it's because now I fear [Camina Drummer][1] /Cara Gee might be a show-
only character!

Or maybe because of how much they changed Ashford from the superb David
Strathairn version.

Anyway, time to have an Expanse break.

2/5

[Home](/blog "ohthreefive")

[1]: https://expanse.fandom.com/wiki/Camina_Drummer_(TV) "The Expanse fandom page about Drummer"