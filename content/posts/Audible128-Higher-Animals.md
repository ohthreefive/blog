+++

date = "2023-05-20T11:40:37"
title = "Higher Animals: Vaccines, Synthetic Biology, and the Future of Life"
description = "Audible audiobook listens #128"
tags = ["Audible"]

+++

A bit of a cheat - I’ve been intrigued by Pushkin’s audiobooks and so
bought directly from them, so it’s not technically an Audible listen.
The process and easy integration with my podcast app of choice was
seamless.

Specter is an efficient writer and good narrator. This is something of
a primer, clearly built out of a huge respect and awe of the production
of the COVID vaccine. A quick read for anyone, scientific background or
not. Highly recommended.

4/5

[Home](/blog "ohthreefive")