+++
date = "2023-06-01T11:29:04"
title = "Mythos: The Greek Myths Retold"
description = "Audible audiobook listens #129"
tags = ["Audible"]

+++

I’d seen this advertised by Audible and it had piqued my interested, but recently my children have been reading Percy Jackson and they were really keen for more Greek legend stories.

It’s a great re-telling and good narration from Stephen Fry. My interest waned towards the end, but not hugely. Highly recommended.

4/5

[Home](/blog "ohthreefive")