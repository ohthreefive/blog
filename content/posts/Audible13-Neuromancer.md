+++

date = "2017-05-06T21:36:53Z"
title = "Neuromancer"
description = "Audible audiobook listens #13"
tags = [ "Audible" ]

+++

William Gibson's Neuromancer was a book I'd long heard good things about and
which sounded completely to my tastes. In fact, it had been a toss up to decide
which of this and [Snow Crash][1] I read first.


I found Neuromancer slightly less accessible and a little slower (other than in
the parts in which Snow Crash went deep in to Ancient Sumerian culture.)

Overall though, this is a satisfying slice of cyber fiction.

4/5

### The re-read (December 19th, 2023)

I had to come back to Neuromancer, having enjoyed but not *loved* it first time
around. Unfortunately, while I enjoyed it, I didn’t feel I had a particularly
stronger grip on it than I had last time around. I’m going straight on to a
Snow Crash re-read for comparison!

![Neuromancer book cover][pic1]

[1]: /post/Audible11-Snow-Crash "my mini snow crash review"

[Home](/blog "ohthreefive")

[pic1]: https://ohthreefive.gitlab.io/blog/images/Neuromancer.jpg "Neuromancer book cover"
