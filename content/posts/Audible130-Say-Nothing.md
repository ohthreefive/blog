+++
date = "2023-07-04T17:48:36"
title = "Say Nothing: A True Story of Murder and Memory in Northern Ireland"
description = "Audible audiobook listens #130"
tags = ["Audible"]

+++

This highly recommended book about the troubles justified the recommendations. It’s a fairly unrelenting anti-Gerry Adams book.

There is something of an irony in the fact that I found the American accent narrating Audible’s ‘Ireland in the 1990s’ to be off-putting; inauthentic.

The Irish narration here is not the best but I assumed it was the author himself - then it turned out the author was an American!

5/5

[Home](/blog "ohthreefive")