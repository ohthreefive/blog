+++
title = "Heroes: Mortals and Monsters. Quests and Adventures"
date = 2023-07-07T14:08:54+01:00
description = "Audible audiobook listens #131"
tags = ["Audible"]
+++

Just like Mythos, Stephen Fry has put tremendous effort into crafting a
compelling narrative out of these myths.

It's also a very long book but the author is on hand with advice from beginning:
don't try to absorb every fact, every timeline, every family tree. Instead, just
enjoy the stories, and remember the bits you want to remember.

The kids loved it - Troy next!

4/5