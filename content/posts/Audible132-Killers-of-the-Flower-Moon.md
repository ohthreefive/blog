+++
date = "2023-08-01T21:22:29+02:00"
title = "Killers of the Flower Moon: The Osage Murders and the Birth of the FBI"
description = "Audible audiobook listens #132"
tags = ["Audible"]

+++

The subject matter is incredible - a campaign of greed-based murder.

I struggled a bit with the book, though. So many names, families etc. I found it difficult to keep up. In the end I elected to relax, give up trying to follow specifics and instead follow the thread of the narrative.

Can’t wait to see what Scorsese has done with it.

3/5

[Home](/blog "ohthreefive")