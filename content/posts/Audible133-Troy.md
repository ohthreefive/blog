+++

title       = "Troy: Our Greatest Story Retold"
description = "Audible audiobook listens #133"
date        = "2023-08-20"
tags        = [ "Audible" ]

+++

The third in Stephen Fry's thusfar trilogy of Greek mythology is much like the
first two in terms of quality, though narrower in scope. Entertainingly done.

4/5

[Home](/blog "ohthreefive")
