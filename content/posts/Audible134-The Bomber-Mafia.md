+++
date = "2023-09-10T21:17:42"
title = "The Bomber Mafia"
description = "Audible audiobook listens #134"
tags = ["Audible"]

+++

A second Pushkin audiobook, expanding on the three-parter from the [Revisionist History][1] podcast. Fast-paced and enthralling. A real fun listen.

4/5

[1]: https://www.pushkin.fm/podcasts/revisionist-history “Revisionist History podcast homepage”

[Home](/blog "ohthreefive")