+++
date = "2023-09-24T12:06:49"
title = "Nova"
description = "Audible audiobook listens #135"
tags = ["Audible"]

+++

I only learned about author Samuel Delaney when he died earlier this year. Audible offered this book of his and I gave it a try. The dense character-based discussion did not lend itself terribly well to an audiobook but the story certainly moved along nicely and swiftly.

3/5

[Home](/blog "ohthreefive")