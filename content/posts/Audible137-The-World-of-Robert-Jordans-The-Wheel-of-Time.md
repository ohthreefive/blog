+++
date = "2023-10-12T12:32:38"
title = "The World of Robert Jordan’s The Wheel of Time"
description = "Audible audiobook listens #137"
tags = ["Audible"]

+++

A reference book, so not exactly a page-turner, but everything about the Age of Legends was fascinating.

2/5

[Home](/blog "ohthreefive")