+++
date = "2023-11-04T17:17:59Z"
title = "Going Infinite: The Rise and Fall of a New Tycoon"
description = "Audible audiobook listens #138"
tags = ["Audible"]

+++

I finished the audiobook before SBF’s trial had concluded. I had heard a lot of criticism that Michael Lewis had taken it too easy on Sam. But between his ‘on background’ podcast interviews and his discussions during the ‘judging Sam’ podcast, I think Lewis is clearly just laying out the story as he saw it and letting the reader judge. Lewis clearly thinks Sam is a criminal, but he thinks that because of what he saw, so that’s what he shows (not tells) us.

4/5

[Home](/blog "ohthreefive")