+++
date = "2023-11-20T20:49:03Z"
title = "Cibola Burn"
description = "Audible audiobook listens #139"
tags = ["Audible"]

+++

Returning to the Expanse after a break was a good idea. It’s amazing how good and how faithful the TV series was. Murtry is even more of an objectionable psychopath. The short chapters are really good for working through it, but the plot still really takes its time.

4/5

[Home](/blog "ohthreefive")