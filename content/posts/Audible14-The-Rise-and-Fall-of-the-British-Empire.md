+++

date = "2017-05-07T19:56:59Z"
title = "The Rise and Fall of the British Empire"
description = "Audible audiobook listens #14"
tags = [ "Audible" ]

+++

I regret not studying history further at school and was intruiged by Audible's
'The Great Courses' series, so what a way to combine these feelings!

Listening to lectures is significantly harder work than most novels, with facts
flying by at a rate of knots. However, the lectures are short (almost exactly
half my commute) so listening twice in a row to a chapter is no chore.

Wonderful 'narration' and a unique subject matter.

5/5

[Home](/blog "ohthreefive")
