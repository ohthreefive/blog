+++

title       = "Your Face Belongs to Us: A Secretive Startup's Quest to End Privacy as We Know It"
description = "Audible audiobook listens #140"
date        = "2023-12-10"
tags        = [ "Audible" ]

+++

A well written but in places overlong tour from script kiddie to (presumable)
multimillionaire by exploiting the public's willingness to give photos of them to
various social platforms. The flip-side to this naivety being those platforms
offering little in the way of protection, warning or way out.

Clearview AI is a terrifying company but its product will soon be a commodity -
everyone will have this technology. The worry is what authoritarian or simply
lazy governments will do, or are already doing, with it.

3/5

[Home](/blog "ohthreefive")
