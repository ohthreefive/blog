+++
date = "2024-03-23T12:06:17Z"
title = "Prophet Song"
description = "Audible audiobook listens #141"
tags = ["Audible"]

+++

My annual promise to have at least one of my listens chosen for me: the
Booker Prize winner.

Prophet song was as accessible as Shuggie Bain, which is to its credit.

My habit of diving in without much pre-reading about what I’m about to
start did bite me here though. I didn’t know it was dystopian fiction until
something completely strange struck me in the narrative. I’d assumed it
was ‘Troubles’ set fiction or even non-fiction.

Heartbreakingly close to real life.

4/5

[Home](/blog "ohthreefive")