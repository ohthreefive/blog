+++
date = "2024-05-04T09:37:48"
title = "Temeraire"
description = "Audible audiobook listens #142"
tags = ["Audible"]

+++

OH!

SO!

FORMAL!

I loved the straight-faced tone. It made Temeraire’s casual disregard for it all the more funny. A really enjoyable light read. I am officially **on baord** with this series.

[Home](/blog "ohthreefive")