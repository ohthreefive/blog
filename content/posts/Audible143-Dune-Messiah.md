+++
date = "2024-05-19T19:43:39"
title = "Dune Messiah"
description = "Audible audiobook listens #143"
tags = ["Audible"]

+++

I have an undoubted Dune obsession right now and thought I would dip into book two for the first time.

Chani was hard done by, especially given the impact she’d just had in the part 2 movie.

Paul seemed somewhat aloof and cold - probably Herbert’s exact plan.

Alia held the most interest for me - she seemed to be both concerned about and enjoying who she was and what she was becoming.

4/5

[Home](/blog "ohthreefive")