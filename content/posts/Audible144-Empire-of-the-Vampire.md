+++

title       = "Empire of the Vampire"
description = "Audible audiobook listens #144"
date        = "2024-06-18"
tags        = [ "Audible" ]

+++

There was more than a little Kvothe about the unwilling narrator/child prodigy about this one. Additionally, 20+ hours for only part of a story means it did stretch in places. But I still tore through it for the most part.

3/5

(Half-assed theory - the redeemer is described as bringing people back from the dead... like a vampire!?)

[Home](/blog "ohthreefive")
