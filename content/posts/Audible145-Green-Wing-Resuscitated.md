+++

title       = "Green Wing: Resuscitated"
description = "Audible audiobook listens #144"
date        = "2024-06-18"
tags        = [ "Audible" ]

+++

Well it was nice to spend time with these characters again, but if it didn't come free with an Audible subscription, I wouldn't say it was worth a credit.

2/5

[Home](/blog "ohthreefive")
