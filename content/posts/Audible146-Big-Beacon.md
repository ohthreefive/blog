+++
date = "2024-07-20T08:40:51"
title = "Alan Partridge: Big Beacon"
description = "Audible audiobook listens #146"
tags = ["Audible"]

+++

Between this and the ‘From the Oasthouse’ podcast series, I think audio really has become the perfect medium for Partridge. It’s a hilarious listen with the highlight being, I’m ashamed to say, a Partridge-narrated sex scene.

4/5

[Home](/blog "ohthreefive")