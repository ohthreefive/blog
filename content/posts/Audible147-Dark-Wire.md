+++

title       = "Dark Wire"
description = "Audible audiobook listens #147"
date        = "2024-08-20"
tags        = [ "Audible" ]

+++

Frankly sensational account of the FBI setting up a cell network to spy on drug traffickers using 'encrypted phones'.

4/5

[Home](/blog "ohthreefive")
