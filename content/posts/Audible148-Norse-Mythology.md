+++

title       = "Norse Mythology"
description = "Audible audiobooks listen #148"
date        = "2024-08-27"
tags        = [ "Audible" ]

+++

The children love their mythology and have devoured Stephen Fry's Greek
offerings. Owing to a great book, they have at least a passing interest in the
Norse lore too. I've found Gaiman's novels difficult to crack and was
trepidatious but needn't have been - this is lively, short and enjoyable.

I must admit though, there is a ~1 hour gap in my listening as I was a very
asleep passenger on a long car journey at the time!

4/5

### The re-read (February 4th, 2025)

Finally got round to finishing this short collection without snoozes! Again,
it's an enjoyable collection of short stories, but, oh no, Neil Gaiman, [what have
you been up to?](https://www.theguardian.com/commentisfree/2025/jan/17/neil-gaiman-allegations-sexual-assault "Link to Guardian commentary about allegations against Neil Gaiman")

[Home](/blog "ohthreefive")
