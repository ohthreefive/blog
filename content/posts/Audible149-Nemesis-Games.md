+++
date = "2024-09-22T12:31:17"
title = "Nemesis Games"
description = "Audible audiobook listens #149"
tags = ["Audible"]

+++

A return to the Expanse saga after a break. Absence definitely made the heart grow fonder. This was not my favourite series of the TV show but I enjoyed my time with the characters here. Still, I prefer the crew together.

4/5

[Home](/blog "ohthreefive")