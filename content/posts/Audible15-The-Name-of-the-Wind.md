+++

date = "2017-07-27T22:10:05Z"
title = "The Name of the Wind"
description = "Audible audiobook listens #15"
tags = [ "Audible" ]

+++

The first book in the Kingkiller Chronicle came highly recommended by a cousin.

This wonderful fantasy tale hits all the right notes. Demystifying a legend is
often unfulfilling (Star Wars prequels, Silmarillion) but this novel didn't
suffer for it, perhaps because it wasn't a prequel and we actually learn more
about Kvothe as he develops than Kvothe the legend.

I found frustration in the same place I found pleasure in Name of the Wind.
Kvothe's dragon-slaying turns out to be the poisoning of a (albeit gigantic)
herbivorous lizard. I love the demystification - stories are always
embellishments - but was disappointed that this proved to be the climax, at
least as far as action was concerned. (Been a while since I read this, actually,
so I may be mis-remembering.)

Anyway, I'm delighted to welcome this series to my collection of favourite
fantasy series! 

5/5

### The re-read (June 26th, 2021)

This remains an absolute charm, though Kvothe is a bit of an arrogant Mary Sue!

[Home](/blog "ohthreefive")