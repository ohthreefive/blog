+++

title       = "War"
description = "Audible audiobook listens #151"
date        = "2024-11-14"
tags        = [ "Audible" ]

+++

I promised I'd read no more about the Trump 2017-2021 presidency, and I kind of
kept that promise. But the newly re-elected president still looms large over the
narrative here. It's a fairly thrilling story of how the US under Biden tried to
hold the world together while Putin and Netenyahu try to destroy their enemies.

[Home](/blog "ohthreefive")
