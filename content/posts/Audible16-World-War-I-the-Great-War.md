+++

date = "2017-08-11T21:14:12Z"
title = "World War I the Great War"
description = "Audible audiobook listens #16"
tags = [ "Audible" ]

+++

I dived back in to the Great Courses for more history. Unfortunately the condensed nature of the lectures and my lack of basic background knowledge meant I found it hard to keep up. But it's in my library forever now, for another time!

3/5 

[Home](/blog "ohthreefive")
