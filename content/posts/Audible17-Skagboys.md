+++

date = "2017-08-17T22:02:48Z"
title = "Skagboys"
description = "Audible audiobook listens #17"
tags = [ "Audible" ]

+++

I was recommended this Irvine Welsh by a close friend who has read all of them. Apparently, this is the funniest one. Well, I can confirm it is laugh out loud hilarious. Renton, Sick Boy, Spud, Begbie, all present and correct, just younger than the characters I knew from the Trainspotting movie.

Two things irked me, however. the initial descent in to drug taking was a little quick. More importantly, a large proportion of the final third dragged, as if the narrative had just run out of steam.

I'm conflicted: enormously entertaining but disappointingly onerous.

3/5 

[Home](/blog "ohthreefive")
