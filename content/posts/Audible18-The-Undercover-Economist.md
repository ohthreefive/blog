+++

date = "2017-10-14T20:38:01Z"
title = "The Undercover Economist"
description = "Audible audiobook listens #18"
tags = [ "Audible" ]

+++

I am a Harford fan. Whether it’s his [TED talks][1] or the incomparable [More or Less podcast][2], I find him thought-provoking and informative.

This old bit of work is fantastically interesting and zippy.

Can’t say I remember much of the details now, but I really enjoyed it at the time!

[Home](/blog "ohthreefive")

[1]: https://www.ted.com/speakers/tim_harford "Tim Harford on TED website"

[2]: http://www.bbc.co.uk/programmes/p02nrss1/episodes/downloads "BBC More or Less podcast"
