+++

date = "2017-10-14T20:53:40Z"
title = "The Wise Man’s Fear"
description = "Audible audiobook listens #19"
tags = [ "Audible" ]

+++

Book 2 of the Kingkiller Chronicles was another cracker.

I’ll pick nits first - the will-they-won’t-they with Denna is tiresome. The constant struggles with tuition
fees etc - yawn again.

However, moving the story away from the university (the Fae, Ademre, Severen) was refreshing.

What I really want is a conclusion to Kvothe’s story with the Chandrian. I hope book three delivers! (And
maybe a sequel series following Bast)

5/5

### The re-read (July 25th, 2021)

Man, this one's a beast! The sudden ripping of Kvothe from University to Maer to bandit-hunting to Fae to
Ademre is actually quite jarring, but helps expand the world. Doors of Stone now please.

[Home](/blog "ohthreefive")