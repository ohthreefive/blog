+++

date = "2017-10-14T21:13:24Z"
title = "Bad Pharma"
description = "Audible audiobook listens #20"
tags = [ "Audible" ]

+++

I’m as big a Goldacre fan as I am a Harford fan. [Bad Science][1] was/is indispensable.

Ben’s anger at the drug companies leaps of the page here. Unfortunately, that’s the main down side of the book, as the reader feels chastised by the author; complicit with pharma.

Its message is more important than the Bad Science book; but its execution is more abrasive.

4/5

[Home](/blog "ohthreefive")

[1]: http://www.badscience.net/ "Bad Science website"
