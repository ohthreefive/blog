+++

tags = ["Audible"]
date = "2017-11-21T22:15:34Z"
description = "Audible audiobook listens #21"
title = "Nomad"

+++

Years ago, "I, Partirdge" was the first audiobook I ever downloaded. It slew me.
Nomad was a little slighter, a little more chucklesome than laugh out loud, but
having Partridge narrate his own words was again such a highlight. Terrific
stuff.

4/5

[Home](/blog "ohthreefive")
