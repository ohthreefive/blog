+++

tags = ["Audible"]
date = "2017-11-21T22:20:42Z"
description = "Audible audiobook listens #22"
title = "The Road to Little Dribbling: More Notes From a Small Island"

+++

I have read precious few Bill Bryson, but my memory of his book about Europe
('Neither Here Nor There'?) read while at university was that he could be laugh
out loud funny.

Little Dribbling wasn't quite laugh out loud, but it's terrifically funny. Some
of Bryson's gripes are a little eye-rolling - I'm at least twenty years younger
and less easily annoyed than he is - but an eye-rolling smirk is still a smile!

3/5

[Home](/blog "ohthreefive")
