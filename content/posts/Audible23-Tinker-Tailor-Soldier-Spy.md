+++

date = "2018-03-04T20:15:12Z"
description = "Audible audiobook listens #23"
tags = ["Audible"]
title = "Tinker Tailor Soldier Spy"

+++

I like to think I am not too proud and so I will admit I struggle to *picture* a
novel while reading it. I therefore find that if I have seen a film or TV
adaptation, it is extremely helpful.

In the case of TTSS, it is also helpful due to the plot intricacies. Spoilers be
damned, knowing the story already really improved this for me, and allowed me to
bask in the extra information contained in the written text.

Superb.

5/5

[Home](/blog "ohthreefive")
