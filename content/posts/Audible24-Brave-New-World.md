+++

date = "2018-03-04T20:22:02Z"
description = "Audible audiobook listens #24"
tags = ["Audible"]
title = "Brave New World"

+++

Unlike many (most) of my Audible purchases thus far, I have previously read
Brave New World (around eighteen years ago!)

I remembered some broad themes but little detail.

What can I say about such a well-known and discussed novel? It's truly
wonderful and terrifying.

5/5

[Home](/blog "ohthreefive")
