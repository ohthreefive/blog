+++

date = "2018-03-04T20:25:39Z"
description = "Audible audiobook listens #25"
tags = ["Audible"]
title = "World War Z"

+++

I read this mostly on the advice of [Empire][1] magazine's finest person,
[Helen O'Hara][2]. As an aside, I don't even know if Helen is employed by
Empire any more after being let go for some damn reason. However, she is a
regular on their [podcast][3](which, by the way, is usually much much worse in
her absence) and she still writes many bold and well reasoned reviews for the
magazine. As an aside to an aside, check out her proclamation on Kylo Ren's
status in the Star Wars bad-guy pantheon in her [Force Awakens][4] review -
brilliant and prescient. She was rightly proud of it during a podcast discussion
of The Last Jedi ("I look like a frickin genius now" - I paraphrase.)

WWZ, however, was a chore. The retrospective nature of the narrative robbed so
much immediacy and terror from most of the vignettes, and it just seemed to go
on forever.

I forgive you, Helen!

2/5

[Home](/blog "ohthreefive")

[1]: https://www.empireonline.com/ "Empire magazine website"
[2]: https://twitter.com/HelenLOHara "Helen O'Hara on twitter - remember the L!"
[3]: https://tools.planetradio.co.uk/core/podcasts/rss.php?name=the-empire-podcast "RSS link for the feed - far too hard to find, Empire!"
[4]: https://www.empireonline.com/movies/star-wars-force-awakens/review/ "Helen's SW:tFA review"

