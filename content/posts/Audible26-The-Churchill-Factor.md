+++

date = "2018-03-04T20:45:08Z"
description = "Audible audiobook listens #26"
tags = ["Audible"]
title = "The Churchill Factor"

+++

Oh, Boris, is this how well off we would all be if you were a full-time writer
rather than politician?

Pacey, lively and thoroughly wonderful.

5/5

[Home](/blog "ohthreefive")
