+++

date = "2018-03-04T20:50:39Z"
description = "Audible audiobook listens #27"
tags = ["Audible"]
title = "The Golem and the Djinni"

+++

Advertising works - I liked the title, the cover and the presence of this novel
in the Audible app and on the website. Read a couple of reviews and went for it.

I'm delighted that I did. It's a rather simple story at heart, with a nice
streak of strangeness to me - a western gentile with no in-built knowledge of
the mythology of Golems or Djinnis.

4/5

[Home](/blog "ohthreefive")
