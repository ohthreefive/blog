+++

date = "2018-03-04T20:53:52Z"
description = "Audible audiobooks listens #28"
tags = ["Audible"]
title = "Steve Jobs"

+++

The Isaacson biography had been a present. I knew I'd never get through a tome
of that size so used an Audible credit.

It is helluva boring. Perhaps in an attempt to maintain objectivity, I simply
felt I was reading a history book. Nothing of the author came through.

At least many of the plain old facts were sensational in their content if not
the telling.

2/5

[Home](/blog "ohthreefive")
