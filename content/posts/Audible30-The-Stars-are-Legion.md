+++

date = "2018-04-22T21:10:42Z"
title = "The Stars are Legion"
description = "Audible audiobook listens #30"
tags = [ "Audible" ]

+++

I’m unsure which of these two phrases is better suited to describe this book: ‘utter tosh’ or ‘utter bobbins.’

You [let me down, Liptak.][1]

1/5

[Home](/blog "ohthreefive")

[1]: https://www.theverge.com/2017/2/8/14516168/kameron-hurley-the-stars-are-legion-science-fiction-book-review "Andrew Liptak's Verge review of the novel"
