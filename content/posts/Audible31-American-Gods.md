+++

date = "2018-04-22T21:14:52Z"
title = "American Gods"
description = "Audible audiobook listens #31"
tags = [ "Audible" ]

+++

Like Pratchett, Gaiman was one of those authors I knew I should like. Unlike Pratchett, I’d come across many adaptations of Gaman’s work, and enjoyed them thoroughly. I read this just as the tv adaptation was airing.

I just didn’t get into it. I found it meandering without being quite interesting enough. But I was *disappointed* not to like it. How weak of me!

2/5 

[Home](/blog "ohthreefive")
