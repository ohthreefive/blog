+++

date = "2018-04-22T21:22:12Z"
title = "Lincoln in the Bardo"
description = "Audible audiobook listens #32"
tags = [ "Audible" ]

+++

Another recommendation from [the man who got me in to Audible][1].

I struggled through this, really failing to engage.

Then it won the Booker. Am I just an idiot who only likes sci-fi/fantasy? (I can already hear my sister saying yes.)

Deserves a re-read some time on account of the prize.

2/5 

[Home](/blog "ohthreefive")

[1]: https://leolaporte.com/ "Leo's home page"
