+++

date = "2018-07-05T07:28:47Z"
title = "Adapt"
description = "Audible audiobook listens #33"
tags = [ "Audible" ]

+++

I’m always keen to improve my thinking processes and decision making. I’d read
Undercover Economist and listen to [More or Less][1] so I know I like Tim
Harford’s style. This was a fun listen, though the emphasis on the strategies
used in the war on terror at the start of the novel were a bit overpowering.
Slightly out of kilter with the overall tone of the book.

[Home](/blog "ohthreefive")

[1]: https://www.bbc.co.uk/programmes/b006qshd "More or Less podcast page, BBC"
