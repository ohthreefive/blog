+++

date = "2018-08-10T09:09:37Z"
title = "The Martian Chronicles"
description = "Audible audiobook listens #34"
tags = [ "Audible" ]

+++

A really enjoyable tale of the interactions between earth & mars. I particularly
enjoyed the first few chapters, which would work nicely as short stories. Given
how [this became a novel][1], that’s unsurprising!

3/5

[Home](/blog "ohthreefive")

[1]: https://en.m.wikipedia.org/wiki/The_Martian_Chronicles#Structure "The Martian Chronicles on Wikipedia"
