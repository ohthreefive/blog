+++

date = "2018-08-10T09:15:04Z"
title = "The Mote in Gods Eye"
description = "Audible audiobook listens #35"
tags = [ "Audible" ]

+++

I first came across Pournelle by his appearances on [TWiT][1] and thought I’d
listen to one of his novel’s after his passing.

I don’t remember now, but suspect this was recommended on TWiT’s tribute. A
thoroughly interesting novel of distrust and doubt. Have recently listened to
[the Three-Body Problem][2], I can see its influence in modern classics.

4/5

[Home](/blog "ohthreefive")

[1]: https://twit.tv/people/jerry-pournelle "TWiT's Jerry Pournelle bio"

[2]: https://en.m.wikipedia.org/wiki/The_Three-Body_Problem_(novel) "Three-Body Problem on Wikipedia"