+++

date = "2018-08-26T14:47:14Z"
title = "I Think You’ll Find It’s a Bit More Complicated Than That"
description = "Audible audiobook listens #36"
tags = [ "Audible" ]

+++

I had found Goldacre’s previous (Bad Pharma) to be terrifying but a bit too
shout-y.

This is a collection of individual articles, mostly published in the Guardian
Bad Science column. As such, I’d read the majority already. The shorter pieces
vary in tone and entertainment, but almost always fully de-bunk some nonsense in
a fun-to-read, easy-to-understand way. It’s a shame Ben has better things to do
these days: I miss the short form writing.

4/5

[Home](/blog "ohthreefive")
