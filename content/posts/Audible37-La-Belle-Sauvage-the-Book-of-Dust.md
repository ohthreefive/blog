+++

date = "2018-08-27T21:37:39Z"
title = "La Belle Sauvage: the Book of Dust"
description = "Audible audiobook listens #37"
tags = [ "Audible" ]

+++

His Dark Materials was an outstanding trilogy, and I had heard little of
Pullman’s return to the world before finding out this was available. It was
added to my basket promptly!

The peril of the prequel is that to fill in/flesh out backstory that was
previously only implied can lead to disappointment. Not only can the prequel
disappoint in its own right, but its betrayal of the ‘original’ can be so
complete that it can even diminish a once loved narrative.

The first in the Book of Dust novels avoids this fate, and yet I still wish
Lyra’s story had remained contained only within Dark Materials. A mythical and
fantastical chase story, La Belle Sauvage races along nicely and has a terrific,
if familiar, villain.

3/5

### The Re-read (May 11th, 2023)

On re-read, I noticed that the fantastical elements of the story actually don't
occur until quite late in the story.

In addition, there were striking signposts about Bonneville: a mute demon, his
ability for both charisma and brutality. Can't believe I didn't see that first
time.

[Home](/blog "ohthreefive")
