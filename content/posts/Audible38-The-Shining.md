+++

date = "2018-08-28T07:48:08Z"
title = "The Shining"
description = "Audible audiobook listens #38"
tags = [ "Audible" ]

+++

It was time to scratch the King itch. A highly enjoyable read, though inevitably
I compared it to the Kubrick adaptation, which I’ve seen often enough for it to
seem the canonical version of the story.

Enough to convince me to revisit King some day.

3/5

[Home](/blog "ohthreefive")
