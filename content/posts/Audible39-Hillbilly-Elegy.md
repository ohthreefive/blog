+++

date = "2018-08-29T08:24:15Z"
title = "Hillbilly Elegy"
description = "Audible audiobook listens #39"
tags = [ "Audible" ]

+++

A Christmas present last year, this is not the kind of book I would read, or
even hear about, on my own.

A fascinating autobiography with some terrifying glimpses - and often more than
just glimpses - of rural life in modern America.

Proof that it’s worth venturing outside your "filter bubble".

4/5

[Home](/blog "ohthreefive")
