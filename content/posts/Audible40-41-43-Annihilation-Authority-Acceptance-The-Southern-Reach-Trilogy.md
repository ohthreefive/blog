+++

date = "2018-08-29T08:31:02Z"
title = "Annihilation, Authority & Acceptance: The Southern Reach Trilogy"
description = "Audible audiobook listens #40, #41 & #43"
tags = [ "Audible" ]

+++

The books, or the first of the trilogy at least, were up front and centre of my
algorithmically-generated Audible suggestions long before Natalie Portman was
being advertised on Netflix.

It wasn’t until Alex Garland’s name was attached as director that I decided to
take the plunge. I needn’t have bothered, as the movie is [not a straight
adaptation of the novel][1].

Meandering and meditative. Highly disorientating. Not entirely satisfying
(though I suspect that was never the author’s intention.) Interesting work, but
not essential.

3/5 - for the trilogy. I won’t score them individually. Reading only
Annihilation would be highly dissatisfying!

[Home](/blog "ohthreefive")

[1]: https://ohthreefive.gitlab.io/post/annihilation/ "My brief Annihilation movie review"
