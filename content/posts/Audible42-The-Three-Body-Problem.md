+++

date = "2018-08-29T08:41:47Z"
title = "The Three Body Problem"
description = "Audible audiobook listens #42"
tags = [ "Audible" ]

+++

A [Barack Obama recommendation][1] which I missed, preceded by a [Hugo award][2]
were insufficient to make this novel known to me.

A mention on the [Empire podcast][3] by the superb [Helen O'Hara][4]? It went
straight on to my wish list and soon in my library. I hadn’t even realised it
was a trilogy by this point nor did I have a **clue** what it was about!

This is dense, unapologetic science fiction (I think, "Hard," is the preferred
term) with an extremely healthy streak of purely entertaining and exciting set
pieces.

Then I found out there were more of these to read. **Yes!**

5/5

### The re-read (March 4th, 2024)

With the imminent Netflix release I thought I’d re-visit the series. It was much
easier to follow the narrative (even nearly 6 years later) but I definitely rubbed
up against a slightly janky translation this time, which I hadn’t noticed before.
Full of interesting ideas. Perhaps less satisfying second time around. Will I
complete a series re-read this time? I’m actually not sure.

[Home](/blog "ohthreefive")

[1]: https://www.nytimes.com/2017/01/16/books/transcript-president-obama-on-what-books-mean-to-him.html "Barack Obama literature interview"

[2]: http://www.thehugoawards.org/hugo-history/2015-hugo-awards/ "2015 Hugo winners"

[3]: https://www.empireonline.com/tags/empire-podcast/ "Empire movie podcast home page"

[4]: https://mobile.twitter.com/helenlohara "Helen O'Hara on twitter"
