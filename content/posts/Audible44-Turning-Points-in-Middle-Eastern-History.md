+++

date = "2018-08-31T07:42:51Z"
title = "Turning Points in Middle Eastern History"
description = "Audible audiobook listens #44"
tags = [ "Audible" ]

+++

I returned to the Great Courses due to my embarrassing lack of knowledge about
the Middle East.

A side-effect (or perhaps just an *effect*) of the short (30 min) lectures by an
expert in the field does make these series exhausting listens. I try to listen
to each twice but there is about eighteen hours of content: who has time?

An absolutely outstanding lecture series. I have perhaps retained ten percent of
it. However, it is in my library forever now!

5/5

[Home](/blog "ohthreefive")
