+++

date = "2018-08-31T07:51:52Z"
title = "The Dark Forest"
description = "Audible audiobook listens #45"
tags = [ "Audible" ]

+++

The sequel to the Three-Body Problem expands and enriches the first and deepens
my love of the series.

I saw some similar themes between TBP and the Mote in God’s Eye. In the Dark
Forest, the most noticeable influence I saw was not thematic but rather felt
like the author paying homage to Brave New World. The ‘umbrella’ transports of
the future must be a nod to the personal gyrocopters in Huxley’s novel, surely?

After finishing, I went straight to the Audible store in search of the third and
discovered the third in the trilogy, Death’s End, isn’t on Audible in the UK!!

**NOOOOOOOOOO!**

5/5

### THE RE-READ, December 19th 2024

6 1/2 years since reading this originally!? *Tempus fugit*! Satisfying to know
where this was going and just enjoy the ride, though these books are so dense
that I’d forgotten as much as I’d remembered!

[Home](/blog "ohthreefive")
