+++

date = "2018-08-31T08:06:33Z"
title = "Consider Phlebas"
description = "Audible audiobook listens #46"
tags = [ "Audible" ]

+++

I’ve been meaning to get in to the Culture series for some time. I believe
Phlebas is an outlier - being set thousands of years separate from the rest of
the series. It was enjoyable without being essential, with the protagonist being
the strongest aspect of the novel; a great character.

3/5

[Home](/blog "ohthreefive")
