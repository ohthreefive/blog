+++

date = "2018-09-09T12:10:27Z"
title = "Brexit: What the Hell Happens Now"
description = "Audible audiobook listens #48"
tags = [ "Audible" ]

+++

Ian Dunt’s concise run-down of the various issues which Britain will have to
negotiate is a fantastic, easily-digestible summary. It tips to the negative,
heavily, but justifies this with the evidence presented. I’ll make several
returns trips to this in the next year, I imagine.

5/5

[Home](/blog "ohthreefive")
