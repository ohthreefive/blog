+++

date = "2018-09-23T20:21:24Z"
title = "The Left Hand of Darkness"
description = "Audible audiobook listens #49"
tags = [ "Audible" ]

+++

I had been wanting to read Ursula K le Guin for some time and was disappointed
not to find Tales from Eathsea on Audible. This was my other option.

This started as some really intriguing and thoughtful science fiction but then
came the walk across the ice. This section, which took up most of the last third
of the novel, struggled to keep my attention.

And now that I’m using my [new technique](/post/Reading-better.md) for reading
and recall, I have proof that my attention was wandering: my chapter summaries
get briefer towards the end.

Also, some appallingly patronising opinions of women are expressed, something I
believe is not unusual for this author.

2/5

[Home](/blog "ohthreefive")
