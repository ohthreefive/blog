+++

date = "2018-10-13T21:44:52Z"
title = "Factfullness"
description = "Audible audiobook listens #50"
tags = [ "Audible" ]

+++

I've watched just about every TED talk, lecture, interview etc. Hans Rosling
ever gave. Because of this, I correctly answered most of the introductory
questions correctly. But that does not mean I had nothing to learn from the
book. Far from it. This is a succinct and yet comprehensive overview of the
state of the world today and a structured way to think about it. Essential
reading. Essential re-reading.

5/5

[Home](/blog "ohthreefive")
