+++

date = "2018-11-03T19:52:53Z"
title = "Saudi America"
description = "Audible audiobook listens #51"
tags = [ "Audible" ]

+++

An outstandingly concise re-telling of the American shale revolution and its place among US and world energy production, consumption, import & export. I listened to each chapter twice, as they are information dense, but still shot through the book.

5/5

[Home](/blog "ohthreefive")
