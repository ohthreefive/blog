+++

date = "2018-12-22T07:50:35Z"
title = "The Silmarillion"
description = "Audible audiobook listens #52"
tags = [ "Audible" ]

+++

I’m a huge Tolkien fan and read this at least twice before but found it dense
and unappealing. I was surprised when I saw [Jason Kottke's recommendation][1]
and feeling enough LotR withdrawal to give it a chance.

I’m glad I did! The outstanding narration is part of the enjoyment, but for the
first time I felt the plot (the fulfilment of the oath of Fëanor) more than the
history lesson. it’s still dense, but it’s manageable!

4/5

### The re-read (November 12th, 2024)

Re-read at the behest of child 1, yet again these tales improve in the
repetition. I still get a little lost with all the Beleriand goings on, but
overall it was great. Again.

[Home](/blog "ohthreefive")

[1]: https://kottke.org/18/08/letter-of-recommendation-tolkiens-the-silmarillion-read-by-martin-shaw "Kottke recommends the Silmarillion audiobook"
