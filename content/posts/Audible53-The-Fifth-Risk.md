+++

date = "2019-01-03T09:18:54Z"
title = "The Fifth Risk"
description = "Audible audiobook listens #53"
tags = [ "Audible" ]

+++

This is the first Michael Lewis book I've read. It seems to be an assault on the
Trump presidency, with an introduction documenting how unprepared his transition
team were. The apparent lack of knowledge and interest shown by the Trump
presidency in the workings of government is detailed in the chapters, but more
emphasis is placed on detailing how vast, complex and important the US
government is. This is never more apparent than in the closing chapter, an
absolutely sprawling deep dive in to data, weather and tornado prediction.

So really, this is a love letter to the US government and a cautionary tail
about what might be lost by dismantling it from the inside. The lack of shock,
bile and fury directed towards Trump does lend the book a more balanced tone,
but overall it feels like there is a bit of a lack of focus.

3/5

[Home](/blog "ohthreefive")
