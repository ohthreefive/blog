+++

date = "2019-02-15T22:52:42Z"
title = "Death’s End"
description = "Audible audiobook listens #54"
tags = [ "Audible" ]

+++

An epic of epic epicness! A monumental conclusion to the trilogy. The level of
technical detail slows the plot and characters tend to suffer from two flaws
(gender stereotypes and near-perfect logical thinking and deductions.) But the
ideas, the scope and the invention of this series is just astonishing.

5/5

[Home](/blog "ohthreefive")
