+++

date = "2019-03-16T08:33:11Z"
title = "Fear - Trump in the White House"
description = "Audible audiobook listens #55"
tags = [ "Audible" ]

+++

A terrifying account of a man generally thought of as a habitual liar being in
the seat of the most powerful man in the world. The efforts of those around him,
largely in circumventing, distracting and attempting to manipulate him, are
Herculean and terrifying in their own way. Wow.

5/5

[Home](/blog "ohthreefive")
