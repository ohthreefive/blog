+++

date = "2019-03-25T14:15:40Z"
title = "The Thief"
description = "Audible audiobook listens #56"
tags = [ "Audible" ]

+++

After some epically long reads, this relatively short novel was wonderfully
concise. An exciting tale with just enough twists and turns to be fulfilling and
satisfying without the reader feeling cheated or tricked.

The name of the series of which this novel is the first part is a bit of a
spoiler: avoid it if you can!

4/5

[Home](/blog "ohthreefive")
