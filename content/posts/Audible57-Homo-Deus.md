+++

date = "2019-04-24T05:27:27Z"
title = "Homo Deus"
description = "Audible audiobooks listens #57"
tags = [ "Audible" ]

+++

Sapiens was one of my earliest listens (#12!) and I had found it to be a
marvellous piece of work.

Homo Deus is more difficult. It’s a dense tome, mixing pure conjecture with
seemingly unquestionably believable prediction.

The author is patient with his subject matter, regularly going into depth and
embracing repetition. For the reader, how tolerable this style is depends on
your interest in the particular subject.

Overall, the author has enough passion in his narrative and sufficient insights
to spark thought to pull the reader through it all.

3/5

[Home](/blog "ohthreefive")
