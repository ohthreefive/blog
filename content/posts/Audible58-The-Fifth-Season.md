+++

date = "2019-05-25T20:06:10Z"
title = "The Fifth Season"
description = "Audible audiobook listens #58"
tags = [ "Audible" ]

+++

I enjoyed the world, the mythology and most of the characters, but the narrative
escaped me: I just didn't know where it wanted to go and this was frustrating.
There was no resolution at the end either, just sequel bait. That said, I am a
sucker for an interesting world and am still tempted by book 2!

2/5

[Home](/blog "ohthreefive")
