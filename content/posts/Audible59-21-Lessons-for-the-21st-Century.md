+++
date = "2019-06-08T15:09:25.237Z"
title = "21 Lessons for the 21st Century"
description = "Audible audiobook listens #59"
tags = [ "Audible" ]

+++

Harare has proved himself quite the modern philosopher with his three books.
There is a lot of re-tread of Homo Deus, enough to negate the need to read that
book in full. There seem to be wild swings in theme, with a slightly left-field
conclusion extolling meditation as humanity's defence against the machine. Truly
fascinating and enjoyably bizarre. I wonder where he'll go next.

4/5

[Home](/blog "ohthreefive")