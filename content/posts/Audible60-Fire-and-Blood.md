+++
         
date = "2019-07-27T06:48:10.341Z"
title = "Fire and Blood"
description = "Audible audiobook listens #60"
tags = [ "Audible" ]

+++

A historical account within a fictional world might not sound like a page turner, and in many sections it is quite the drag, but there are many fun and exciting stories in here. It took a certain amount of discipline to avoid too many tie-ins to the Song of Ice and Fire, for which Martin should be applauded. The major bit of dangling fruit, the three stolen dragon eggs, was perfect!

3/5

[Home](/blog "ohthreefive")