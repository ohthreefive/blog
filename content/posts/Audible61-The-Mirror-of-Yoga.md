+++
date = "2019-09-01T19:36:40.668Z"
title = "The Mirror of Yoga"
description = "Audible audiobook listens #61"
tags = [ "Audible" ]

+++

I've been practising yoga for a year and a half now and have tried very very
hard to shed my rugby-player mentality towards the - well - mental side of the
practice. So when someone in the class recommended this to me I thought, in the
spirit of open-mindedness, I should try it. Despite some very interesting
passages, overall this book was just too deep for me. Much of it completely
washed over me in a haze of Sanskrit! Maybe give it another few years!

2/5

[Home](/blog "ohthreefive")