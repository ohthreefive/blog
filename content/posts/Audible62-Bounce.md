+++
date = "2019-09-14T07:56:25.321Z"
title = "Bounce"
description = "Audible audiobooks listens #62"
tags = [ "Audible" ]

+++

The idea that practice makes expert is central to this exploration of achieving
greatness, and central to that is the research of Anders Ericsson. By chance, I
critically appraised his paper in my Sports Science degree fifteen years ago.
While the theory has its many detractors, Syed's book is thankfully a more broad
discussion on how to achieve expertise, and I particularly enjoyed the
exploration of psychology.

3/5

[Home](/blog "ohthreefive")