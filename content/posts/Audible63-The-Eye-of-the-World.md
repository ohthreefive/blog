+++
date = "2019-10-13T20:30:08.084Z"
title = "The Eye of the World"
description = "Audible audiobook listens #63"
tags = [ "Audible" ]

+++

I am such a sucker for fantasy. This had me hooked early and maintained my
attention for its near thirty hour length. Magnificent.

5/5

[Home](/blog "ohthreefive")