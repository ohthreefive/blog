+++
date = "2019-11-02T07:32:40.291Z"
title = "The Secret Commonwealth"
description = "Audible audiobook listens #64"
tags = [ "Audible" ]

+++

The second book in the Book of Dust trilogy is vastly different than *La Belle
Sauvage* and appropriately so, being set 20 years further on. Lyra's existential
crises, her loss of confidence and her estrangement from Pan, who has retained
these qualities of her youth, is painful to read and Pullman does well to resist
the temptation to resolve it quickly. The plot is quite the paradox: moving at a
relentless forward place but without ever solidifying with a clear purpose or
threatening to give the reader closure on any thread. This was rather
frustrating, with the twin intrigues of the red building and the blue hotel
being dangled from early on but remaining beyond explanation by close of story.
I confess it did not dawn on my until at least two thirds of the way through
that there would be no resolution in this part of the story. When I did realise
that, my reaction was disappointment. The curse of the middle child!

3/5

### The Re-read (June 17th, 2023)

So I made a mistake on the Belle Sauvage re-read. Bonneville, despite the daemon’s
similarity to Mrs. Coupler’s monkey, is *not* Lara’s relative: the new character
Delamaire is. ‘The Furnace Man’ remains one of the best chapters of any of these
novels. It struck me this time around how the powering of the man’s machine was
achieved in quite the opposite way to Asriel powering the ripping open of the sky.

My main take away is that this is a much slower narrative than any of the previous
novels, and leaves so many questions to be answered. Will they ever be?

[Home](/blog "ohthreefive")