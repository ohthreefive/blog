+++
date = "2019-11-19T23:26:21.089Z"
title = "The Secret Barrister"
description = "Audible audiobook listens #65"
tags = [ "Audible" ]

+++

An exposé of sorts, reminding me of Ben Goldacre's deep criticisms within the
medical and science communities. Enjoyable, though not my favourite narrator.

3/5

[Home](/blog "ohthreefive")