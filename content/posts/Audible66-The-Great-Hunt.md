+++
date = "2019-12-10T20:10:51.832Z"
title = "The Great Hunt"
description = "Audible audiobook listens #66"
tags = [ "audible" ]

+++

Book two in the Wheel of Time series has taught me that this series will not be
rushed! When the Great Hunt was referenced in the first book, I thought it might
be a side quest in a future book, not the topic of its own novel. However, the
writing and world-building remains excellent and I'll be starting book three
soon!

4/5

[Home](/blog "ohthreefive")