+++
date = "2019-12-21T22:36:23.270Z"
title = "Messy"
description = "Audible  audiobook listens #67"
tags = [ "Audible" ]

+++

I've listened to and read Tim Harford for years, so much of this was not new to
me, but his writing and his arguments are just so clear and simply persuasive. A
great read and great life advice.

4/5

[Home](/blog "ohthreefive")