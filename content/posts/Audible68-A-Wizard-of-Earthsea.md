+++
date = "2020-01-02T20:16:34.414Z"
title = "A Wizard of Earthsea"
description = "Audible audiobook listens #68"
tags = [ "audible" ]

+++

Now over half a century old, it is hard to think that the story about a great
wizard being told not at the time of his greatness but at the time when he was
just learning was not par-for-the-course - I am of the Harry Potter generation.

Ged's story is one of hardship and trouble preceding his greatness. It is very
fast-paced and short on the meandering detail I have gotten used to with Game of
Thrones, Wheel of Time and Three-Body Problem, to name but three.

This was both jarring and refreshing - I'm sure I'll continue the series.

3/5

[Home](/blog "ohthreefive")