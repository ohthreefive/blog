+++
date = "2020-01-09T09:20:41.245Z"
title = "The Sun King"
description = "Audible audiobook listens #69"
tags = [ "Audible" ]

+++

The first Audible original to which I have listened. It is presented as a 6
episode podcast series but can easily be read as a short novel. Essential
history or Rupert Murdoch.

5/5

[Home](/blog "ohthreefive")