+++
date = "2020-01-24T09:45:15.957Z"
title = "Exhalation"
description = "Audible audiobook listens #70"
tags = [ "Audible" ]

+++

I was keen to read something by Chiang due to his story being the inspiration
for the movie, Arrival (I haven't read that particular story: it's not on
Audible.)

He is clearly an excellent science fiction mind, with many of his stories being
intellectually fascinating, though somewhat lacking in heart. They are also
mostly cautionary or melancholic tales; I would have liked more wonder!

3/5

[Home](/blog "ohthreefive")