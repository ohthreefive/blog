+++
date = "2020-02-05T20:58:31.509Z"
title = "The Last Days of August"
description = "Audible audiobook listens #71"
tags = [ "Audible" ]

+++

This is the first of Jon Ronson's works I've listened to - it was excellent. He
approaches the subject with sympathy and while overall he could be accused of
stirring up a hornet's nest without offering them resolution or catharsis, I
think he treads the right side of this line.

4/5

[Home](/blog "ohthreefive")