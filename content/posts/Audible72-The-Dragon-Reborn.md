+++
date = "2020-03-07T15:13:28.792Z"
title = "The Dragon Reborn"
description = "Audible audiobook listens #72"
tags = [ "Audible" ]

+++

The third book of the Wheel of Time series has driven home the message to me:
Robert Jordan is in *no rush*!

While the story feels fast-paced at times - essentially all groups realise they
have to get to and then some of them end up racing to the same place - this goal
signposted early in the novel. There is no question that this goal will actually
be achieved mid-novel and the group move on to something new. In fact, the
conclusion of the novel has been signposted since book one. Some might find this
deliberateness a bit slow or frustrating; I love the patience and care of it!

Less good is the near complete sidelining of Rand, Lan, Loial, Min and, to a
lesser extent, Moiraine. I suspect future novels might have more for them to do,
and again this is an example of Jordan's well thought out plan.

Another minor quibble is that all three in the series have so far concluded in a
fairly similar way. Still, I love it!

4/5

[Home](/blog "ohthreefive")