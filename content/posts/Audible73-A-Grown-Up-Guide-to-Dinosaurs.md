+++
date = "2020-03-07T15:19:32.864Z"
title = "A Grown Up Guide to Dinosaurs"
description = "Audible audiobook listens #73"
tags = [ "Audible" ]

+++

A lively update about dinosaurs, trying to re-capture adults' interest in them.
Could have been longer.

3/5

[Home](/blog "ohthreefive")