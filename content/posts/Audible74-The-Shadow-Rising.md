+++
date = "2020-04-27T13:15:00.000Z"
title = "The Shadow Rising"
description = "Audible audiobooks listens #74"
tags = ["Audible"]

+++

For the first time, the patient, slow style of Jordan's writing frustrated me
here. Rand proclaimed himself Dragon Reborn to the world in the last novel and
yet the conclusion of this one was him doing the same, specifically for the
Aiel.

It was refreshing not to have a one-on-one confrontation as the novel's
conclusion. Well, there kind of was, except it was more a fight for control of
an object, less for survival. This meant that the action-based climax to the
novel was Perrin's Two Rivers defence which, unfortunately, I was not too
interested in.

I would like Padan Fain/ Moredeth/ Ordeith to be more central and I'm interested
to where Slayer/ Luc will re-appear. Five novels in, I'm confident Jordan has a
plan for these characters, but maybe I'm starting to think things could speed
up!

3/5

[Home](/blog "ohthreefive")
