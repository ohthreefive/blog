+++
date = "2020-06-18T19:26:11.221Z"
title = "The Fires of Heaven"
description = "Audible audiobook listens #75"
tags = [ "Audible" ]

+++

OK, I have to admit it, I got pretty bored through the whole mid-section of this
part of the Wheel of Time. Rescued by a blockbuster ending.

3/5

[Home](/blog "ohthreefive")