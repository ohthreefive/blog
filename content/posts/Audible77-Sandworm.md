+++
date = "2020-08-28T05:24:35.558Z"
title = "Sandworm"
description = "Audible audiobook listens #77"
tags = [ "Audible" ]

+++

A confounding, scary and exciting assessment of one of the world's greatest
threats.

4/5

[Home](/blog "ohthreefive")