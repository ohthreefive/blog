+++
date = "2020-10-06T13:18:10.519Z"
title = "Rage"
description = "Audible audiobook listens #79"
tags = [ "Audible" ]

+++

Woodward's second book about Trump is done with interviews with the president,
in contrast with 'Fear'. Trump implicates himself in every possible way. He is
entirely without humility. Woodward has successfully recorded this for history,
but I think I have read my last book about the man. There is nothing new to
learn; nothing behind closed doors.

4/5

[Home](/blog "ohthreefive")