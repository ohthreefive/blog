+++
         
date = "2020-10-30T17:44:12.721Z"
title = "Why I’m No Longer Talking To White People About Race"
description = "Audible audiobook listens #81"
tags = [ "Audible" ]

+++

A similar piece to [Natives][1], but with an outstanding chapter on feminism.

5/5

[Home](/blog "ohthreefive")

[1]: https://ohthreefive.gitlab.io/post/audible78-natives/ "My little review for Natives"
