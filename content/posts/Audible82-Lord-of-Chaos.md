+++
date = "2020-12-05T17:04:42.539Z"
title = "Lord of Chaos"
description = "Audible audiobook listens #82"
tags = [ "Audible" ]

+++

In a first for this endlessly patient series, two of the main threads don't even
reach their conclusions (the women in Ebou Dar and Rand's Illian plan). Another
beautifully textured tale but the glacial pace continues.

3/5

[Home](/blog "ohthreefive")