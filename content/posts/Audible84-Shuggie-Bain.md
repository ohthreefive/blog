+++
date = "2021-01-30T08:52:10.076Z"
title = "Shuggie Bain"
description = "Audible audiobook listens #84"
tags = [ "Audible" ]

+++

Ooh, get me. Two Booker prize winners in two years. To quote Wittertainment,
there's a lot of Shawshank before the Redemption. in fact, it really is only the
last line of the novel which offers much in the way of hope. Bleak.

4/5

[Home](/blog "ohthreefive")