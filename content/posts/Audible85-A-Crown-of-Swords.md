+++
date = "2021-02-27T20:16:09.554Z"
title = "A Crown of Swords"
description = "Audible audiobook listens #85"
tags = [ "Audible" ]

+++

A lot happens and not a lot happens. The bowl of the winds is found but goes
unused. The Seanchean return in style. Rand takes on another Forsaken but the
result is more ambiguous than previous encounters. The Aes Sedai see more
examples of how they have failed the world but do not recognise their mistakes.
This is a saga in no hurry and with no simple truths. Just as well it's so rich.

4/5

[Home](/blog "ohthreefive")