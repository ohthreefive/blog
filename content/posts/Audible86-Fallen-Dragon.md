+++
date = "2021-03-16T12:12:01.639Z"
title = "Fallen Dragon"
description = "Audible audiobook listens #86"
tags = [ "Audible" ]

+++

A really enjoyable sci-fi, especially the second half, which rattled along to a
satisfying conclusion. Took a while to hit its stride.

3/5

[Home](/blog "ohthreefive")