+++
date = "2021-04-21T19:34:42.739Z"
title = "The Path of Daggers"
description = "Audible audiobook listens #88"
tags = [ "Audible" ]

+++

I think this is the first Wheel of Time book since Robert Jordan died. It's
shorter but a bit light on plot.

2/5

[Home](/blog "ohthreefive")