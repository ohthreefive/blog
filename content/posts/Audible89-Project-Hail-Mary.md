+++
date = "2021-08-06T18:47:18.750Z"
title = "Project Hail Mary"
description = "Audible audiobook listens #89"
tags = [ "Audible" ]

+++

Superior survivalist sci-fi

5/5

(It's almost exactly 6 years since I joined Audible and that's me circled back
to Andy Weir after my first listen:  [The Martian] [1])

[Home] (/ "ohthreefive")

[1]: https://ohthreefive.gitlab.io/post/audible01-the-martian/ "my first Audible review"