+++
         
date = "2021-08-15T07:12:51.032Z"
title = "Hiroshima"
description = "Audible audiobook listens #90"
tags = [ "Audible" ]

+++

Like most people, I knew the Allies had dropped atomic bombs on Hiroshima and Nagasaki in 1945, but until I read this, I had never really thought about the people that were there, *really* there, survived, and had to go on living their lives.

5/5

[Home](/blog "ohthreefive")