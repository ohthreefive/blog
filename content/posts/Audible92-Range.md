+++
date = "2021-09-03T08:12:00.388Z"
title = "Range: Why Generalists Triumph in a Specialized World"
description = "Audible audiobook listens #92"
tags = [ "audible" ]

+++

Many interesting points, and a good lesson for parents not to push their
children towards a head start.

3/5

[Home](/blog "ohthreefive")