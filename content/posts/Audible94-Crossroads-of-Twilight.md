+++
date = "2021-10-23T06:36:12.784Z"
title = "Crossroads of Twilight"
description = "Audible audiobook listens #94"
tags = [ "Audible" ]

+++

After the blockbuster closing chapter of the previous novel, this overlapping
narrative is remarkable lacking in action. The weakest so far.

2/5

[Home](/blog "ohthreefive")