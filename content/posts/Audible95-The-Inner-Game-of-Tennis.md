+++
date = "2021-11-08T09:51:59.982Z"
title = "The Inner Game of Tennis"
description = "Audible audiobook listens #95"
tags = [ "Audible" ]

+++

Fascinating analysis of the interaction between body and mind told through the
lens of tennis, but applicable anywhere.

4/5

[Home](/blog "ohthreefive")