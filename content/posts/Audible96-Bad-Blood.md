+++
date = "2021-11-08T09:54:17.586Z"
title = "Bad Blood: Secrets and Lies in a Silicon Valley Startup"
description = "Audible audiobook listens #96"
tags = [ "Audible" ]

+++

I'd heard the name Theranos in the tech news (not the news news) for years
without knowing anything other than, "They lied." I finally decided to learn
more about it and my jaw hit the floor.

5/5

[Home](/blog "ohthreefive")