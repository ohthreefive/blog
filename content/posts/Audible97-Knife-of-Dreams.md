+++
date = "2021-11-29T10:13:41.656Z"
title = "Knife of Dreams"
description = "Audible audiobook listens #97"
tags = [ "Audible" ]

+++

Wow this was a 'return to form' after the Crossroads of Twilight. I don't tend
to delve too far into reviews, but I believe I'm not alone in finding CoT an
absolute slog. Things are getting moving again before the Sanderson finale!

4/5

[Home](/blog "ohthreefive")