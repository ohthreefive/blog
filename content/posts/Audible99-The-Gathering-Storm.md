+++
date = "2022-01-10T10:11:02.130Z"
title = "The Gathering Storm"
description = "Audible audiobook listens #99"
tags = [ "Audible" ]

+++

The first in the trilogy that is the final novel (if that makes sense) and the
first Sanderson is an absolute treat, with plot momentum feeling unstoppable
while still leaning heavily into the complexity of Jordan's storytelling. Even
with two more stories of this size to go, I don't see how it can all come to an
end!

5/5

[Home](/blog "ohthreefive")