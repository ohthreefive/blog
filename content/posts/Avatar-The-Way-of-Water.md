+++
date = "2023-01-02T15:02:36.070Z"
title = "Avatar: The Way of Water"
description = "Brief movie reviews #72"
tags = [ "movie" ]

+++

Visually, this was a reminder of what is possible with time and money in visual
effects terms. For an unbelievable runtime with an unbelievable proportion of
the screen being rendered on a computer, this movie exposed that the CGI
weaknesses in other 'big budget' movies (looking at you, MCU and DCEU) owe more
to impatience on the executives' part than limited skills of the visual artists.

Story-wise, it was surprisingly more insular - **one** 'sky people' boat versus
**one** clan - disappointingly inconsistent - why were the Sullys left alone at
the conclusion of the final battle? - and repetitive in a way that lacked
ambition - Quaritch was a great bad guy in the first movie and he's already
bland by the end of this one.

But overall, this is how blockbusters should be made, with no concessions to the
experience. Shame there's only one James Cameron to force the issue though.

3/5

[Home](/blog "ohthreefive")