+++

date = "2019-04-30T07:57:09Z"
title = "Avengers: Endgame"
description = "Brief movie reviews #31"
tags = [ "movie" ]

+++

An unprecedented achievement. A three hour blockbuster with more character than
action. The conclusion of Infinity War is both re-enforced and reversed, retro-
actively improving that film too. It’s still not perfect, and hasn’t left me
giddy the way Avengers Assemble/The Avengers did, but the odds of pulling off a
film this good were as heavily stacked as those against the heroes. So never in
doubt!

4/5

[Home](/blog "ohthreefive")
