+++

date = "2018-04-30T10:30:54Z"
title = "Avengers: Infinity War"
description = "Brief movie reviews #22"
tags = [ "movie" ]

+++

Hmmm. Tricky one this. Outstanding entertainment. Outstanding. But the finale:
there's going to be a big switcheroo of sorts in Avengers 4, and that lessens
the boldness of this climax.

[I hope I'm wrong][1] and they really, REALLY, go for it. The last time I
REALLY wanted a movie to go for it was when Kylo asked Rey to join him after
*that* scene. I wish she'd taken his hand; that would have been so interesting.

I suspect Avengers 4 (and Star Wars 9) will find their own satisfying ways to
conclude their stories, but I don't know if I'll prefer them to what I would
like to happen.

Also, if they do resolve the Infinity War climax the way I've predicted in my
tweet, then, in the words (word) of Mark Kermode, [**consequences**][2].

So yes, outstanding entertainment, but the more I think about it, the more I
think the resolution will weaken the setup. Perhaps that's unfair.

4/5 today; wonder what it'll be after Avengers 4.

[Home](/blog "ohthreefive")

[1]: https://twitter.com/ohthreefive/status/990720031775391745 "I predict a full flip-reverse"

[2]: https://www.youtube.com/watch?v=ngbTkkRa2tg "Kermode reviews infinity war. If this many deaths are reversible, the MCU will be weakened forever"