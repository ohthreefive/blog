+++

date = "2017-07-27T21:29:04Z"
title = "Baby Driver"
description = "Brief movie reviews #5"
tags = [ "movie" ]

+++

I'm a **huge** Edgar Wright fan: particularly of the Cornetto trilogy, though I also enjoy Scott Pilgrim to a certain degree. I was desperate to see Baby Driver, especially after the glowing reviews.

The movie left me oddly un-moved. The opening scene was wonderful and Wright's attention to detail - here more in music-to-action choreography as compared to the outstanding call-backs in the Cornettos - was peerlees as always.

The middle third sagged, something I'd forgive if the conclusion was up to scratch but alas, no. I was hoping for an unbelievable car chase (you know, it's a movie about a driver) but what I got was a Baby vs surprisingly-hard-to-kill Buddy for twenty or so minutes. So when the epilogue rolled around, I found it un-earned.

However something Edgar definitely has earned is a second viewing, which I'll give this film at some point.

2/5 

[Home](/blog "ohthreefive")
