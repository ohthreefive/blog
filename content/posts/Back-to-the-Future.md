+++
date = "2024-04-11T21:02:10"
title = "Back to the Future"
description = "Brief movie reviews #86"
tags = ["movie"]

+++

On at the IMAX (yes, Back to the Future on an **IMAX** screen!) as part of the Glasgow comedy festival.

It really is a showcase for the phrase, "They don't make them like they used to."

5/5

[Home](/blog "ohthreefive")