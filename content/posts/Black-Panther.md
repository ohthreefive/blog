+++

date = "2018-03-04T19:35:53Z"
description = "Brief movie reviews #19"
tags = ["movie"]
title = "Black Panther"

+++

Just to remind myself, here is how I've done at the last few year's Marvel films
with regards to seeing them at the cinema:

- Thor Ragnarok: Cinema. (Entertaining but flimsy)
- Spiderman Homecoming: Rental.
- GoG v2: Rental.
- Dr. Strange: IMAX. (Entertaining but flimsy too, plus Dr. Strange went from
  zero to very powerful between this and Ragnarok TOO quickly for me.)
- Civil War: Rental. (Damnit. What a big-screen miss.)

The five before that, all the way back to Thor Dark World - rental. Iron Man 3
was the last cinema outing before Dr. Strange: 3 1/2 years!

Anyway suffice to say I think Marvel makes good films but rarely great ones. Why
did I choose to see Black Panther in the cinema? He was great in Civil War.
Infinity War is coming and I wanted to be up to date. The reviews were GREAT.

Again, however, Marvel comes up with some great entertainment which is largely
instantly forgettable. Visually, yes, new territory. Cast and crew of colour,
yes, great development for Marvel and hopefully Hollywood.

But the story is just more of the same Marvel. Moreover, and I know this will be
unpopular, Killmonger does have a sympathetic motivation but saddling him with
the revenge-for-dead-dad angle detracts from his character, especially as by the
end of the movie he seems more angry at T'Challa for his father's crime than
Wakanda's non-interventionist stance.

Finally, the CGI for the all important climactic fight is terrible. A hero with
physics defying power/ability is a bit of an unfair challenge for a medium which
often stands out in a bad way when it reveals that it does not have to obey the
laws of physics! But that's not my problem!

3/5

[Home](/blog "ohthreefive")
