+++

date = "2017-11-04T19:25:50Z"
title = "Blade Runner 2049"
description = "Brief movie reviews #13"
tags = [ "movie" ]

+++

I’m a fan of Blade Runner, not a super-fan. 2049 is a worthy successor. Despite taking its time, it still barrels along for its first two hours (!) but unfortunately grinds to a halt just before the setup to Ryan Gosling’s final rescue. Without this complete pause in proceedings, it would be near perfect.

I was able to telegraph the switcheroo on the target of the retiring a mile off, but even though I was trying, I couldn’t work out who the real target would be before it came! So obvious in hindsight!

Best thing in it: Sylvia Hoeks.

4/5

[Home](/blog "ohthreefive")
