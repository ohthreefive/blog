+++
date = "2020-03-07T15:22:17.024Z"
title = "Booksmart"
description = "Brief movie reviews #45"
tags = [ "movie" ]

+++

I'm so sad to have missed this in the cinema: it is outstanding. Such affection
shown for *all* the characters, not just the protagonists. Hilarious.

5/5

[Home](/blog "ohthreefive")