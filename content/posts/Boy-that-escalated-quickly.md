+++

date = "2017-04-13T11:19:26Z"
title = "Boy, that escalated quickly"
description = "Placeholder"
tags = [ "movie" ]

+++

The [Thor Ragnarok trailer][1] is excellent. A ridiculous 80s font and a good
mix of peril and humour has made me hope it'll be the best of the Thor
franchise.

But poor old Mjolnir - disintegrated by Galadriel! When I see unthinkable things
happen to such important artefacts on screen, Ron Burgundy always comes to mind.
His understated, "Boy, that escalated quickly," summary of the news team street
fight in Anchorman perfectly describes this phenomenon.

It affects many franchises/sequels, but it's commonplace in the Marvel cinematic
universe. So I'll use that to illustrate my point.

### Unstoppable force meets immovable object

Heroes/superheroes have powers/superpowers/abilities/kit which tend to
distinguish them or reflect fundamental aspects of their character. In their
separate storylines, a hierarchy of powers/kit is *relatively* straightforward.
Bring them together, however, and things get complicated. The most difficult
issue to resolve is what to do when the most powerful of these abilities clash.
Top Trump vs Top Trump.

Let's run down the Avengers who have invincible attributes:

**(I'm only picking those who have been in more than one film; it takes more
than one film for the 'jumping up a notch' effect)**

- Captain America: shield made of vibranium (indestructible)
- Hulk: well, he's, "Strongest there is"
- Thor: the hammer Mjolnir
- Iron Man: he has limitless money, intelligence and resources, but nothing
invincible I suppose

Invincibility tends to reduce dramatic tension, so it needs to be played down.
But it needs to be handled delicately, so as not to detract from the character.
Let's see what Marvel have done so far:

### Cap's shield

In the First Avenger, it's his top weapon. In the Avengers, it deflects a direct
blow from Mjolnir. So far so powerful. Then in Winter Soldier it's useless
against Bucky because he can just catch it. Before Age of Ultron:, the
marketing suggested it'd be broken by Ultron. (In the end it wasn't, but this
potential was used to play up the threat of Ultron.) In Civil War, it's openly
mocked by Spiderman for defying physics before being given up by Cap at the end.
So from primary weapon to discarded joke in 5 movies.

### Thor's hammer

Thor was all about Thor proving himself worthy of wielding Mjolnir, a weapon so
powerful it could take out the Destroyer. In the Avengers it was shown to be
strong enough to concuss the Hulk. But then in the Dark World it's swatted aside
by Algrim/Kurse, a rather uninteresting baddie. It's importance dials up again
in Age of Ultron, where it's used to demonstrate that Vision is trustworthy. But
now in the Ragnarok trailer it's destroyed by Hela.

### Hulk's self

In Avengers, Hulk takes out a giant Chitauri flying dragon thing with one punch.
In Age of Ultron, he loses a fight to a man in a metal suit. *sigh*

### Why do they do this?

Too many invincible characters ruin drama - it's why Superman movies need
kryptonite and Lois Lane and why X-men movies try to incapacitate Prof X. So
Marvel clearly wants to downplay the most powerful aspects of its characters.
But for me it's detracting from the characters with this approach, and I'm
looking for more consistency.

[Home](/blog "ohthreefive")

[1]: https://m.youtube.com/watch?feature=youtu.be&v=v7MGUNV8MxU "we're friends from work"
