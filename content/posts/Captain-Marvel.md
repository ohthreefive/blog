+++

date = "2019-03-12T20:56:21Z"
title = "Captain Marvel"
description = "Brief movie reviews #30"
tags = [ "movie" ]

+++

A triumphant entrance in to the MCU for both Brie Larson and Carol Danvers, with one of my favourite Marvel finales of recent years. Roll on, Endgame.

4/5

[Home](/blog "ohthreefive")
