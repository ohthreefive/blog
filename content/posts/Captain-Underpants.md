+++

date = "2017-08-11T21:09:57Z"
title = "Captain Underpants: the first epic movie"
description = "Brief movie reviews #8"
tags = [ "movie" ]

+++

I saw this with two children. Both my own, in case you were wondering(/worried.) One is a cinema going pro, the other doesn't have the attention span (yet.) So my experience was rather ruined, but not before a good few chuckles and getting the general impression of warmth and happiness from this kiddie flick.

Needs another watch, with fewer distractions. looking forward to that already!

3/5

[Home](/blog "ohthreefive")
