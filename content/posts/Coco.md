+++

date = "2018-03-04T19:25:22Z"
description = "Brief movie reviews #18"
tags = ["movie"]
title = "Coco"

+++

Pixar produces one of the most visually breathtaking movies I've ever seen, with
an unfamiliar colour palette, and continues to blur near photo-realism with
deliberately artificial animation so subtly it looks easy (see the clam from
Finding Dory deliberately aping Henson-like puppetry in a movie with ocean so
real you could taste the salt.)

The kicker? The above paragraph is completely meaningless. Coco is a delight due
to the strength of the theme and storytelling, the complexity and the heartache.

Bravo Pixar.

4/5

[Home](/blog "ohthreefive")
