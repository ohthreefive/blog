+++

date        = "2017-02-08T22:28:32Z"
title       = "Copperhead OS"
description = "Having fun with a new phone and new OS"
tags  = [ "android", "phones" ]

+++

### MY EXPERIENCE INSTALLING COPPERHEADOS

#### BACKGROUND

I have decided to give non-Google android another try and what could be better
than a security-focused project called [Copperhead OS][1]. You can read about
the project there but it currently supports Nexus 5X, 6P and 9. Devices with
full verified boot (and another few things) are required which means nothing
older than these. I believe a Pixel has recently been donated so the team are
getting to work on this device.

#### WHICH DEVICE

Simples - the 6P was sold out in 64 GB on Amazon. Coming from the unwieldy Nexus
6 I felt like a down-size anyway. The 5X was available at 280 GBP which was
cheap enough to tempt me away from this project altogether and towards a OnePlus
3T! Pixel devices are too expensive and not officially supported. First
impression: the 5X makes my Nexus 6 feel even bigger and heavier than I already
know it is. It's also snappier, much snappier (Snapdragon 808 vs 805, 2 GB RAM
vs 3 GB, FHD screen vs QHD, 64 bit vs 32 - specs aren't everything!)

#### INSTALLING

Instructions are [here][2]. I use Ubuntu Linux, including Mate on a Raspberry Pi
3\. I already had `android-tools-adb` etc. installed but the project clearly
states to avoid distribtion adb packages. I went with the full Android Studio
install as I want a play with Android Studio at some point anyway. This meant
not using the Pi.

#### ENABLE OEM UNLOCKING

This is fairly self-explanatory - I'd been through this previously with my Nexi
4 and 6.

#### FLASHING THE FACTORY IMAGES

One of the first things it tells you to do here is to verify the images you've
downloaded. It provides `gpg` commands to copy and paste. This is nice, but I'd
expect as much given the project's security focus. However, to the less security
minded, what it doesn't tell you is the expected output! So I dutifully copy
pasted the commands and read the output - it looked legit - but have no idea
if I've been spoofed or not. In addition, the command didn't work first time but
a quick search suggested putting the full path to the files to be verified even
though I was in the correct directory. This didn't work:

{{< highlight bash >}}
gpg --verify bullhead-factory-2016.05.03.18.54.15.tar.xz.sig
{{< /highlight >}}

This did:

{{< highlight bash >}}
gpg --verify /path/to/directory/bullhead-factory-2016.05.03.18.54.15.tar.xz.sig
{{< /highlight >}}

I ran the `fastboot` command (having rebooted to bootloader) and got no output.
Back to the search! It needed to be run as root and also as I hadn't configured
a PATH, the command that worked was
`sudo ./home/me/Android/Sdk/platform-tools/fastboot devices`. Or at least this
was the command I ran to prove it worked. The output showed the device so I
proceeded to `fastboot flashing unlock` (with added `sudo` as above.)

The next hurdle was running the flash-all.sh script. It resided within a
directory in the download from CopperheadOS while the command it relied upon
(`fastboot`) was in the platform-tools directory, and had to be run as root. I
tried `sudo ./path/to/flash-all.sh` from the platform-tools directory and
`sudo ./path/to/platform-tools/fastboot flash-all.sh` from the CopperheadOS
download's directory but neither worked. I tried some other combination too - to
be honest I'm doing this all from memory so can't be sure these are the exact
things I tried, but I did fail a few times with what I thought were sensible
attempts!

Eventually I remembered something I did in the past to update my rooted Nexus 6
manually without wiping the device or losing root. I would download the factory
image from Google, open the flash-all.sh script and remove a `-w` (for WIPE!)
flag from one of the lines. It was a tip I got from XDA developers (I think) and
I wish I recorded from where I got it because it is so simple and so useful.

It turned out to be useful here too! I didn't remove `-w` but I did at least
think to open the small flas-all.sh file. There are four or five calls to the
`fastboot` command. In a rare moment of good thinking, I simply pre-fixed all
the calls to `fastboot` with the correct path on my system
(`home/me/Android/Sdk/platform-tools/fastboot`) and ran `./flash-all.sh` with
`sudo` seeing as I'd needed `sudo` for all previous `fastboot` commands. I was
utterly shocked to see that this worked perfectly. Next thing I knew, I saw the
CopperheadOS boot sequence!

#### LOCKING THE BOOTLOADER

Child's play compared to above!

#### VERIFIED BOOT

Immediately on boot, the bootloader displays a (scary) message saying "Oi you,
you're not running the originally installed software, are you sure you haven't
BROKEN YOUR DEVICE!? You should stick with what was installed if you know what's
good for you." The actual message is shorter, but this is what it seems to be
saying to me. It also shows the fingerprint of the software that ***is***
installed (which I am happy to say matches that provided by CopperheadOS. The
message disappears quickly and the device boots as normal. It does, however,
repeat this every time it boots. A minor irk.

### SUMMING UP

Another adventure in Linux, device flashing, command line and so on. I could not
have got through this install a year or two ago so I'm delighted with where I
am.

Now I just need to get some apps on my Nexus 5X and start using it. And get all
my friends on to Signal. And continue to migrate away from Gmail. And this. And
that. And the other!

[1]: https://copperhead.co/android/ "CopperheadOS for Android"

[2]: https://copperhead.co/android/docs/install "Installing CopperheadOS"

[Home](/blog "ohthreefive")
