+++

date = "2017-07-06T22:31:57Z"
title = "Create change and delete Linux user"
description = "To improve my Raspberry Pi security, I had to make some changes"
tags = [ "raspberrypi", "commandline" ]

+++

I got my first Raspberry Pi Zero W recently and planned to use it headless. The default username and password `pi` and `raspberry` are well known and exploitable, especially over `ssh`. Partly for this reason, the Raspberry Pi foundation have disabled `ssh` by default on Raspbian but it [trivial to switch it back on][1], even headless.

### PLAN

I could just have changed the `pi` user's password, but I thought I'd add a new user and then delete the `pi` user instead, just for the practice of doing it. Firstly, `ssh` into the Pi using the default name and password then get to work.

### ADD USER

The Pi foundation has [good documentation][2] for doing this too. It's simple:

    sudo adduser myname

And then fill in a password for the new user.

I want my new user to have `sudo` access and, for simplicity, I want password-less root privileges. Also simple:

    sudo visudo
 
 Then add this line: (the 'root' line will already be there)
 
     # User privilege specification
     root     ALL = (ALL:ALL) ALL
     myname   ALL = NOPASSWD: ALL
 
 Done!
 
### CHANGE USER

    su myname

Should switch you to the newly created user, after which you can test the new user's root privileges. Once satisfied, log out and `ssh` back in as the newly created user.

### DELETE USER

    sudo userdel -r pi

Should delete the `pi` user. When I tried this first it kicked up a fuss, saying the `pi` user couldn't be deleted as it was still doing something or other. I typed:

    ps aux | grep pi

To identify whatever process the `pi` user might be doing then (trepidatiously):

    sudo kill -9 <pid>

Replacing `<pid>` with whatever process id the `ps aux` command had shown.

### BONUS ROUND

Why leave the hostname as Raspberry, however? Let's change it too! (Hat tip to [nixCraft][3] for this.)

    sudo nano /etc/hostname

Delete `raspberry` and add the new name. How about `first-pi-zero `? Next:

    sudo /etc/hosts

Replace all `raspberry` instances with `first-pi-zero `. Reboot, re-ssh in to the pi and you should see:

    myname@first-pi-zero:~$

### WRAP

Job done! A more individualised, and more secure, Pi!

[Home](/blog "ohthreefive")

[1]: https://www.raspberrypi.org/documentation/remote-access/ssh/ "Pi website has good documentation"

[2]: https://www.raspberrypi.org/documentation/linux/usage/users.md 

[3]: https://www.cyberciti.biz/faq/ubuntu-change-hostname-command/
