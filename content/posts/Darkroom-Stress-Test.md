+++
date = "2020-04-27T12:44:33.918Z"
title = "Darkroom Stress Test"
description = "testing how much Darkroom can automate on iOS”"
tags = [ "iOS" ]

+++

I currently shoot on my iPhone 11 Pro and a mixture of
[Halide](https://halide.cam/download) and
[Moment Pro](https://apps.apple.com/us/app/moment-pro-camera/id927098908)
as well as, of course, the stock Camera app.

[Darkroom](https://darkroom.co/download) is my image editor of choice, with its
beautiful UI and great functionality.

Moment Pro recently introduced time lapse and it's an outstanding
implementation. I took 2998 photos of the overnight sky and wanted to play with
them.

## Issues

I used the time lapse export from within Moment but the resulting video was
large and I didn't have space on my iCloud storage (my plan being to tweak the
video on [LumaFusion](https://itunes.apple.com/app/apple-store/id1062022008?pt=5767814&ct=website&mt=8&at=1000lv3W)).

I have also noticed that despite shooting 4:3 photos, the Moment time lapse
export only support 16:9, losing data (and stars!).

## AirDrop

I hardly ever use AirDrop but have heard good things about it.

Wow.

Moment does let you export individual jpegs so that's what I did. While AirDrop
didn't handle all ~3000 photos, it comfortably dealt with 6-700 at a time (I
can't remember what I topped out at) and it was *fast*.

## Darkroom update

Darkroom recently [updated its app](https://darkroom.co/updates/) adding video
support among other things. I wasn't aware of the update, but apparently it also
added the ability to batch paste edits made to a photo.

I picked a photo from my time lapse sequence, made my edits and copied them. I
then selected all the photos in the time lapse sequence.

Darkroom doesn't have a `select all` function, but supports the keyboard
behaviour of selecting one item and `shift + click` another and all items in
between are selected.

When I'd done this, I hit paste and all ~3000 photos had the edits pasted. It
took a while for the visual update to the photos, but there were no crashes.

NB, I went [full Viticci](https://www.macstories.net/stories/ipad-diaries-my-first-48-hours-with-the-new-12-9-ipad-pro/)
and got a 1 Tb 2018 iPad Pro to get the 6 Gb of RAM. Good decision.

## Modify original

Of course, the edits in Darkroom aren't reflected in Photos until you
Share>Modify Original.

This time, Darkroom could not handle ~3000 photos at once and I was running
short of patience (the process so far hadn't been long, but I only had a small
amount of time to work on the time lapse that morning).

Here is a screenshot of the photos' metadata:

![Photo metadata][pic1]

So I started by selecting 100 and choosing Modify Original, then 200 and so on.

## Uncertain limits

I started to screenshot when I hit 500:

![504 photos selected][pic2]

504 images took a good couple of minutes to export but the progress bar was
informative:

![504 photos being overwritten in Photos by Darkroom][pic3]

Once the export is done there is a permission pop-up:

![Edit 504 photos at once? Yes please!][pic4]

I'll admit I was worried the 'Modify' step might take as long as the 'Export'
step, but it doesn't.

Note, the photo preview was blank - a bug?

Very quickly, a 'Successful' pop-up appears:

![Export was successful][pic5]

This step still took a few seconds, and this time the circle was just a spinner,
not a progress bar, but given how quick it was, that wasn't a worry after I'd
seen it work successfully (and quickly) the first time.

For my last export, I pushed it to something like 560 images and it worked fine,
but I never went beyond that.

Well done Darkroom!

On a less good note, it was NOT fun to try to import 3000 photos to a LumaFusion
timeline to manually create a time lapse. I'll confess I gave up!

And on a very final note, I subsequently edited a different time lapse video
simply using a Darkroom filter and tweaking it. Nice video editing too,
Darkroom!

[pic1]: https://ohthreefive.gitlab.io/blog/images/File-metadata.png "Metadata screenshot"
[pic2]: https://ohthreefive.gitlab.io/blog/images/Multi-select.png "Selection screenshot"
[pic3]: https://ohthreefive.gitlab.io/blog/images/Multi-export.png
[pic4]: https://ohthreefive.gitlab.io/blog/images/Multi-modify.png
[pic5]: https://ohthreefive.gitlab.io/blog/images/Success.png

[Home](/blog "ohthreefive")
