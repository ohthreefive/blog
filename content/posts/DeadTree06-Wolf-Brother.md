+++
         
date = "2021-10-05T07:03:47.515Z"
title = "Wolf Brother"
description = "Real dead tree reads #6"
tags = [ "Dead Tree" ]

+++

I read this to my daughter recently. Decent read; a bit bleak. She loved it.
The series is called, "Chronicles of Ancient Darkness." - dark much!?

### The re-read (April 17th, 2022)

Child 2 got jealous and wanted in on the Torak action. Still great second
time around.

[Home](/blog "ohthreefive")