+++
date = "2021-11-08T09:59:47.405Z"
title = "Spirit Walker"
description = "Real dead tree reads #7"
tags = [ "Dead Tree" ]

+++

Book two of the Chronicles of Ancient Darkness expands the world. I preferred
the narrative of Wolf Brother, my daughter preferred this one.

3/5

### The re-read (June 10th, 2022)

Re-read with child 2. I preferred it this time around; the narrative seemed
more logical.

[Home](/blog "ohthreefive")