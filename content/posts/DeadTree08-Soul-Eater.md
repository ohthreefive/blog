+++
date = "2021-12-11T08:29:08.633Z"
title = "Soul Eater"
description = "Real dead tree reads #8"
tags = [ "Dead Tree" ]

+++

For children's books, these really do go pretty dark, and this is the darkest
yet. I just can't see the series ending happily for Torah, Renn and Wolf.

4/5

### The re-read (August 10th, 2022)

A good re-read. Though the book really struggles to regain momentum after
what seems like the climax in the Forest of Stone. I see a few hints in here
that I didn’t notice the first time - especially towards Rennes lineage.
There is also an intriguing interaction between Fin-Kedinn and an eagle owl.
Not sure what to make of that.

[Home](/blog "ohthreefive")