+++
date = "2022-03-12T07:40:47.283Z"
title = "The Lord of the Rings: The Fellowship of the Ring"
description = "Real dead tree reads #9"
tags = [ "Dead Tree" ]

+++

My third read through of the first of the trilogy. I'm fairly sure the last one
was around the time of the movies - 20 years! The children loved the story and
the characters. I noticed the ponderous prose more than ever in this read
through, and yet it remains captivating.

5/5

[Home](/blog "ohthreefive")