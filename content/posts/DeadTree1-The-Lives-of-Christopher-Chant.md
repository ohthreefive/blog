+++
date = "2021-03-09T15:43:40.595Z"
title = "The Lives of Christopher Chant"
description = "Real dead tree reads #1"
tags = [ "Dead Tree" ]

+++

This was a present from a friend of mind: and what a charm it was! I read it to
my daughter. We both had some laugh out loud episodes when Christopher was a
little careless with his lives!

4/5

[Home](/blog "ohthreefive")