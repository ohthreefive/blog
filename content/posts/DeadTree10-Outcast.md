+++
date = "2022-03-12T07:44:24.216Z"
title = "Outcast"
description = "Real dead tree reads #10"
tags = [ "Dead Tree" ]

+++

Another fast-paced and scary narrative as Torah goes through his biggest
challenge yet. Bale is now an established character and for the first time
Rennes secrecy drives a wedge between her and Torak. "Now you know what it feels
like!" She should tell him!

(Daughter remains enthralled)

4/5

[Home](/blog "ohthreefive")