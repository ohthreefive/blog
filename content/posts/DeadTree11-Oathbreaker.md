+++
date = "2022-04-14T21:36:19.232Z"
title = "Oathbreaker"
description = "Real dead tree reads #11"
tags = [ "Dead Tree" ]

+++

Another cracker from this series; originally the penultimate of six stories. The
synopsis rather ruins an albeit very early plot point, but even that didn't
prepare my daughter for it. It prompted an odd amount of denial from her, which
I've never seen before.

4/5

[Home](/blog "ohthreefive")