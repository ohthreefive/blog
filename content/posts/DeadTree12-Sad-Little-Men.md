+++
date = "2022-05-02T07:30:13.126Z"
title = "Sad Little Men: Private Schools and the Ruin of England"
description = "Real dead tree reads #12"
tags = [ "Dead Tree" ]

+++

A rather meandering but ultimately tragic book. I found myself getting lost by
the middle of each chapter: "What point was he trying to make here?" My sister
is much smarter than I am. Without having read it, she managed to summarise,
"Sounds like it would have made an excellent long article in the Guardian."

Agree.

2/5

[Home](/blog "ohthreefive")