+++
date = "2022-05-26T12:52:19.894Z"
title = "Ghost Hunter"
description = "Real dead tree reads #13"
tags = [ "Dead Tree" ]

+++

Almost relentlessly bleak, with plot points which upset my daughter (not to the
point of tears or wanting to stop, but still pretty upset), this would serve as
a satisfying conclusion to the Chronicles, but three more have been released
since. Though I believe number 9 is now the proper end.

Torak remains stubborn - always wanting to go out on his own. Renn remains more
measured and thoughtful. Wolf remains loyal.

3/5

[Home](/blog "ohthreefive")