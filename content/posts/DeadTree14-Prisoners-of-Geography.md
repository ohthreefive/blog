+++

title       = "Prisoners of Geography"
description = "Real dead tree reads #14"
date        = "2022-06-10"
tags        = [ "Dead Tree" ]

+++

I gift from my father-in-law. I know little about geo-politics. I know
a little more know! There is so much information in here that it would
be impossible to retain it all but I suspect that's not the point. This
is a primer, a little hint at the complexities that underly modern
political decisions.

4/5

[Home](/blog "ohthreefive")
