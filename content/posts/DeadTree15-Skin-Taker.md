+++
date = "2022-08-09T09:55:41.574Z"
title = "Skin Taker"
description = "Real dead tree reads #15"
tags = [ "Dead Tree" ]

+++

The penultimate tale in the sage certainly starts with the most spectacular
piece of action in the series, but I'm not too sure it was particularly strong
thereafter. I don't like the antagonist. Ark's side plot was a little too
convenient. Renn and Torak seemed a little aimless in their task, succeeding
through chance as much as skill. These are great books; this wasn't the
strongest.

3/5

[Home](/blog "ohthreefive")