+++
date = "2022-08-09T09:59:24.794Z"
title = "The Norm Chronicles: Stories and numbers about danger"
description = "Real dead tree reads #16"
tags = [ "Dead Tree" ]

+++

A book about statistics by Spiegelhalter? Immediately right up my street. This
takes an different look at probabilities - trying to apply them to 3 archetypal
people. The authors revel in the contradiction that there are no people like
these people - everybody is different. That's the point. That's why we struggle
to communicate and understand probabilities. They are hard. There is never a
definitive answer.

But they're still worthwhile, and this book should teach you why.

4/5

[Home](/blog "ohthreefive")