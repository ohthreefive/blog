+++
date = "2022-09-05T05:21:16.284Z"
title = "A Tale Dark and Grimm"
description = "Real dead tree reads #17"
tags = [ "Dead Tree" ]

+++

I discovered Adam Gidwitz through his excellent podcast Grimm, Grimmer, Grimmest
and this book was advertised on that. It doesn't disappoint. Confidently
gruesome - trust your children to be able to deal with it - and with important
life messages, simply told. Thoroughly entertaining.

4/5

[Home](/blog "ohthreefive")