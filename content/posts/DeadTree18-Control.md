+++
date = "2022-09-17T08:06:24.726Z"
title = "Control: The Dark History and Troubling Present of Eugenics"
description = "Read dead tree reads #18"
tags = [ "Dead Tree" ]

+++

In some ways, Rutherford is too successful with his message in this book. He
makes his argument seem so simple that it almost hides the dangers of eugenics.
It's clearly pseudoscience, though many eminent scientists were involved in its
foundations. It's history is really not that old. It's not just Nazis; in fact
many otherwise heroes had a fondness for it. Just because someone expressed pro-
eugenics views, doesn't mean their other accomplishments must be written off
(put that firmly in the, "I think you'll find it's more complicated than that"
column.) Lastly, it still needs to be talked about, because it's still hanging
around in the 21st century, and our *real* genetic technologies are only getting
better.

Great stuff.

4/5

[Home](/blog "ohthreefive")