+++
date = "2022-11-05T17:37:55.623Z"
title = "The Last Firefox"
description = "Real dead tree reads #19"
tags = [ "Dead Tree" ]

+++

Child two loved the cover. The prose was simple and the story familiar. Went
down well enough with the children though.

[Home](/blog "ohthreefive")