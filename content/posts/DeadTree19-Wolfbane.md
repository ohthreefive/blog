+++
date = "2022-10-10T10:59:04.038Z"
title = "Wolfbane"
description = "Real dead tree reads #19"
tags = [ "Dead Tree" ]

+++

The finale of the Chronicles of Ancient Darkness was as rich as ever - spending
time with another clan, for example - but some parts of the story were somewhat
repetitive to readers of earlier books.

A great series though, thoroughly enjoyed by parent and child.

3/5

[Home](/blog "ohthreefive")