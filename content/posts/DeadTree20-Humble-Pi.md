+++
date = "2022-11-19T07:52:31.389Z"
title = "Humble Pi: When Math Goes Wrong in the Real World"
description = "Real dead tree reads #20"
tags = [ "Dead Tree" ]

+++

There is no doubt that this is firmly in my comfort zone, but it was a true joy
to read. Parker doesn't labour the message that everyone makes mistakes and that
expecting anything different is crazy, but it's a very strong message and
perfectly illustrated here.

5/6

[Home](/blog "ohthreefive")