+++
date = "2022-11-22T21:28:02.660Z"
title = "Children of the Quicksands"
description = "Real dead tree reads #21"
tags = [ "Dead Tree" ]

+++

A beautiful magical tale which certainly broadened mine and my daughter's
cultural horizons. I've rarely seen her so thrilled when a few days after
starting the novel, a child joined her class who spoke Yuroba.

4/5

[Home](/blog "ohthreefive")