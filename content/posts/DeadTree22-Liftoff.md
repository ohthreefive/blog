+++
date = "2022-12-10T20:34:12.490Z"
title = "Liftoff: Elon Musk and the Desperate Early Days That Launched SpaceX"
description = "Real dead tree reads #22"
tags = [ "Dead Tree" ]

+++

For one of the first times in my life, I stuck out a boring first third to half
of a book only to find the remainder to be compelling. Given it opens with a
whole lot of hiring and ends with a whole lot of blasting rockets into space
(sometimes), that's maybe inevitable!

4/5

[Home](/blog "ohthreefive")