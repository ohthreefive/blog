+++
date = "2023-01-21T20:40:40.219Z"
title = "Alex Neptune: Dragon Thief"
description = "Real dead  tree reads #23"
tags = [ "Dead Tree" ]

+++

After the disappointment of [the Last Firefox][1], this was a treat for child 2
(child 1 listened to much of it too).

Well written and entertaining.

4/5

[Home](/blog “ohthreefive”)

[1]: https://ohthreefive.gitlab.io/blog/posts/deadtree19-the-last-firefox/ “my review of the Last Firefox”