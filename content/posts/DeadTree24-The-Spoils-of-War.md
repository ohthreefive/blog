+++
date = "2023-03-28T16:04:32.479Z"
title = "The Spoils of War: Power, profit and the American war machine"
description = "Real dead tree reads #24"
tags = [ "Dead Tree" ]

+++

Staggering expenditure and political manipulation soon became numbing in this
harrowing tale of greed. This had the paradoxical effect of lessening the impact
of the central message after a few chapters. Thoroughly depressing.

3/5

[Home](/blog "ohthreefive")