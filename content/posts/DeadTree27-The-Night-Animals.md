+++
date = "2023-09-24T12:12:39"
title = "The Night Animals"
description = "Real dead tree reads #27"
tags = ["Dead Tree"]

+++

A bittersweet tale of friendship and resilience. A good read for an emotionally-maturing 10 year old.

4/5

[Home](/blog "ohthreefive")