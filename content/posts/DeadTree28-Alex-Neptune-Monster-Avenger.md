+++
date = "2023-12-02T17:55:24Z"
title = "Alex Neptune: Monster Avenger"
description = "Real dead tree reads #28"
tags = ["Dead Tree"]

+++

Book three in the Alex Neptune series has a familiar villain but a slightly different twist on the quest narrative. It remains a fun and captivating series, and child two is already looking forward to the next one.

3/5

[Home](/blog "ohthreefive")