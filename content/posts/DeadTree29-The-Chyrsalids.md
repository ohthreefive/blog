+++
date = "2024-02-20T18:26:58Z"
title = "The Chyrsalids"
description = "Real dead tree reads #29"
tags = ["Dead Tree"]

+++

A present from my sister in law. Excellent sci fi. Brutal and efficient. Full of menace. The ‘telepathic’ community seem to view ‘normal’ humans with as much derision as ‘normals’ view the ‘mutants.’ This is hinted at but not explored, leaving me with a sense of trepidation at the end of the novel.

[Home](/blog "ohthreefive")