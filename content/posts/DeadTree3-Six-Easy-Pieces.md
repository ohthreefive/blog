+++
         
date = "2021-06-19T19:41:26.506Z"
title = "Six Easy Pieces: Essentials of Physics Explained by Its Most Brilliant Teacher"
description = "Real dead tree reads #3"
tags = [ "Dead Tree" ]

+++

Well, what's easy for Feynman is not necessary easy for me!
Beautifully written and I bet they were a joy to witness.

4/5

[Home](/blog "ohthreefive")