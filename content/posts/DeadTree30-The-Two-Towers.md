+++
date = "2024-05-04T09:27:56"
title = "The Two Towers"
description = "Real dead tree reads #30"
tags = ["Dead Tree"]

+++

The Tolkien re-visit continues with only Child 1 (all a bit too slow for Child 2). A fascinating re-exploration. I love these books: their mythology, their bittersweet immersion in an ancient world full of past glories. There is sadness and decline, but also hope.

Also, having seen the movies, Child 1 made a point that I never saw even considered. It’s about Return of the King, but I want to record it here. Isn’t it interesting how Sauron lost the ring by losing his fingers to Isildur and that’s exactly how Frodo lost the ring to Gollum? I bet there’s a Reddit thread about that already… 

[Home](/blog "ohthreefive")