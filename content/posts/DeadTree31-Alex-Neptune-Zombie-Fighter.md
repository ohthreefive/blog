+++
date = "2024-07-20T08:46:03"
title = "Alex Neptune: Zombie Fighter"
description = "Real dead tree reads #31"
tags = ["Dead Tree"]

+++

The penultimate (I assume) novel in this series was another great read, with a computer-game like structure (the three levels) which was new to the series. I’m a little skeptical of Alex’s treatment of Argosy (listen to Anil) and the villain choices in this series are a bit stale, but overall this is a really great children’s series.

4/5

[Home](/blog "ohthreefive")