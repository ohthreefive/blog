+++

title       = "Last Chance Dance"
description = "Real dead tree reads #32 "
date        = "2024-08-20"
tags        = [ "Dead Tree" ]

+++

The third of Efua Traore's novels has none of the fantastical elements of the first two but is inspiring nonetheless. My daughter and I learned more about Nigeria and enjoyed following Jomi on his quest.

4/5

[Home](/blog "ohthreefive")
