+++

title       = "Everything Is Predictable: How Bayesian Statistics Explain Our World"
description = "Real dead tree reads #33"
date        = "2024-08-27"
tags        = [ "Dead Tree" ]

+++

No doubt, the opening third to half of this book absolutely blew my mind even if
it will require multiple re-reads or expanded reading to understand the maths
and thinking required.

Embarrassingly, the second half, which brought Bayes starkly into real everyday
life, was less interesting to me! Just the maths then. Like a proper nerd.

4/5

[Home](/blog "ohthreefive")
