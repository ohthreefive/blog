+++
date = "2021-08-26T08:58:55.600Z"
title = "The Hobbit"
description = "Real dead tree reads #5"
tags = [ "Dead Tree" ]

+++

I was very surprised that my children were both so keen for me to read this to
them and so enthusiastic when listening to it! They loved it. Quicker paced than
I remember it, this is a very fun romp and significantly better than the bloated
movies. Child 1 was very upset at the fate of Fili and Kili. Very upset.

4/5

[Home](/blog "ohthreefive")