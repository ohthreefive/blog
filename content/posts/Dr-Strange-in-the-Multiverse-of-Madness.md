+++
date = "2022-05-26T12:51:37.464Z"
title = "Dr Strange in the Multiverse of Madness"
description = "Brief movie reviews #63"
tags = [ "movie" ]

+++

My issues with many recent Marvel movies has peaked! I enjoyed Multiverse of
Madness as I watched it, but within minutes of leaving the cinema, I realised it
had left no lasting impression on me. It was an enjoyable romp (top marks for
music, by the way) but seemed to be just another in an endless line of Marvel
movies. Ragnarok was the first time I felt like this. Now, I would go as far as
to say that the recent output is proving [Martin Scorsese][1] right.

It was mostly the Illuminati. The Scarlet Witch's power was emphasised by
Strange and Wong then demonstrated in her attack on Kamar-Taj. Then we have some
fan-service/Marvel showing just how far they will use this new Mutliverse
direction as the Illuminati are introduced. Within minutes, Scarlet Witch has
dispatched five super-powerful beings with whom we have no relationship. Marvel
is manipulating us: "Look and cheer at the cameo's!" ... "But beware our villain
who is super-powerful" ... "But we have so many movies we can't have our
villains killing characters we need for the ongoing story" ... "So they'll kill
super-powerful beings that you don't really care about" ... "But we'll get rid
of them by the end of the movie as they're not our current direction."

Let's look at the output since Ragnarok:

- Thor: Ragnarok (2017) - Hela destroys Mjolnir and out-classes Thor, but
doesn’t make it out of the movie, being too powerful a villain to fit into the
story so close to the end of the Infinity Saga. The film did little more than
level up Thor. By the end, no stakes, little emotion other than sympathy for
Thor.
- Black Panther (2018) - A contained story within Wakanda to introduce a
character who was already perfectly introduced in Civil War. No stakes, and I
didn’t feel huge emotional attachment to Killmonger and his mission.
- Avengers: Infinity War (2018) - Loki (briefly) and Heimdall taken out early,
with Vision (un-bleached version...) going down in the most emotional Marvel
death so far. The absolute peak. Even the all too obviously temporary dusting
still feels devastating on re-watch.
- Ant-Man and the Wasp (2018) - I genuinely cannot remember the villain.
Inconsequential. No stakes, no emotion.
- Captain Marvel (2019) - Set a bit in the past, a bit on other planets and
mostly stand alone. The Skrulls are introduced - the only real thing of
consequence? No stakes, no emotion.
- Avengers: Endgame (2019) - Stark and Thanos go out in satisfying fashion,
Black Widow less so and Steve Rogers differently but no less well done. Loki
returns. How many returns will there be in future?
- Spider-Man: Far From Home (2019) - Aggressively stand alone. Forgettable
villain, although with a strangely accurate fake account of the Multiverse
(including Earth’s designation)! ‘Peter Parker might be the new Tony Stark’ is
all it really seems to say. The finale seems to have stakes, but by the end of
No Way Home, they have only affected Peter Parker, not the rest of the world.
- Black Widow (2021) - Didn’t she die already? No stakes, no emotion. Oh,
whoops, I meant to also so, "Welcome, Jelena!”
- Shang-Chi and the Legend of the Ten Rings (2021) - Transports us to another
realm for a conclusion that has no impact on the ‘main’ MCU storyline, and no
impact on the viewers’ hearts. No stakes, no emotion.
- Eternals (2021) - What a strange movie. Celestials? Really? And they haven’t
been known about before? Just SO strange. I wonder if there will ever be a
continuation of _this_ story, let alone having the Eternals interact with the
rest of the MCU. No stakes, no emotion.
- Spider-Man: No Way Home (2021) - Along with the Loki TV show, the Multiverse
is properly put in our faces. This is definitely a Peter Parker movie though,
and it satisfies in that regard.
- Doctor Strange in the Multiverse of Madness (2022) - see above.

Of the last 12 movies, only Infinity War/Endgame _really_ meant something.
That's a poor return rate.

2/5 (and I feel I ought to downgrade other recent movies!)

[Home](/blog "ohthreefive")

[1]: https://www.nytimes.com/2019/11/04/opinion/martin-scorsese-marvel.html "NY Times article where Scorsese explains his comments"