+++
date = "2024-03-23T11:58:27Z"
title = "Dune: Part 2"
description = "Brief movie reviews #85"
tags = ["movie"]

+++

After missing the original in IMAX (damn you, COVID) I booked these tickets with eager anticipation. When it came around, there was a problem. Something was wrong with the projection. The close ups were pin sharp but the wide shots and, surprisingly, the subtitles, were just a bit out of focus.

After ten minutes the movie was stopped and a refund offered but I stayed for the experience and I was absolutely overwhelmed. I thought it barrelled along almost too quickly so was very surprised to hear any reviews lamenting its pacing. One thing it lacked for me was any sense that Paul would not fulfil his ‘destiny’, which somewhat reduced the tension (I have read the book too).

However, this was just an astonishing experience that I cannot wait to repeat. And repeat.

5/5

[Home](/blog "ohthreefive")