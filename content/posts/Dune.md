+++
date = "2021-11-08T09:56:55.926Z"
title = "Dune: Part One"
description = "Brief movie reviews #53"
tags = [ "movie" ]

+++

If I was told that Part Two would continue straight away if I just stayed in my
seat, I'd have given away the next three hours of my life without a second
thought.

5/5

[Home](/blog "ohthreefive")