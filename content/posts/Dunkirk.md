+++

date = "2017-08-11T20:53:24Z"
title = "Dunkirk"
description = "Brief movie review #7"
tags = [ "movie" ]

+++

With Inception being the highlight thus far, Chris Nolan movies are an **event** for me. His embrace of the IMAX format is also a joy.

Dunkirk is unlike anything I've ever seen. Three stories, interwoven but shown over different time periods. No forced narrative. Just 100 minutes of eavesdropping on a short period of horrific tension. Though eavesdropping implies a separation, a divide between you and the events. Dunkirk makes you feel like part of them, or rather that you can't escape them.

Oh, and the transition to Elgar. Stirring.

5/5

[Home](/blog "ohthreefive")
