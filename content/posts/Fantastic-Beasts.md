+++

date = "2017-04-01T09:14:06Z"
title = "Fantastic Beasts and Where to Find Them"
description = "Brief movie reviews #4"
tags = [ "movie" ]

+++

A magical disappointment from the first cinematic departure from Harry in the Potter universe, Fantastic Beasts wandered without direction through its first hour and a bit to reach a tension free finale which only served to set up the sequel(s).

The CGI creatures were uninteresting and lacked physical depth or heft.

Top marks to Eddie Redmayne and Dan Fogler, however.

2/5

[Home](/blog "ohthreefive")
