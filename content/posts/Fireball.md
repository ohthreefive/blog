+++
date = "2021-01-03T09:30:19.605Z"
title = "Fireball"
description = "brief movie reviews #49"
tags = [ "movie" ]

+++

Werner Herzog vs life, the universe and everything. Trippy and fascinating.

3/5

[Home](/blog "ohthreefive")