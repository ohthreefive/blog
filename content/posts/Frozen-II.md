+++
date = "2019-12-21T22:34:34.696Z"
title = "Frozen II"
description = "Brief movie reviews #38"
tags = [ "movie" ]

+++

A denser and more rewarding plot than the original, but slower and with the
wheels of motion sometimes being visible, creaking under the weight of the
storytelling.

3/5

[Home](/blog "ohthreefive")