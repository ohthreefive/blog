+++
date = "2024-06-04T18:41:54"
title = "Furiosa"
description = "Brief movie reviews #88"
tags = ["movie"]

+++

I found this a more satisfying cinema experience than Fury Road. I think that says a lot about me: I’m happy to become immersed in a weird tale that moves at a slow pace with plenty of world-building and character development. Fury Road was a relentless chase movie, and though I can appreciate that style too, I just didn’t click with it either in the cinema or a previous home watch.

Naturally, after the success of this, I’ll want to revisit it. One thing that will definitely stand out is that Anya Taylor-Joy, while a terrific actress, lacks the physical presence of Charlize.

4/5

[Home](/blog "ohthreefive")