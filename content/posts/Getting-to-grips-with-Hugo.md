+++

date        = "2017-01-29T20:32:31Z"
title       = "Getting to grips with Hugo"
description = "Just documenting some things I've learnt messing about with Hugo"
tags  = [ "meta" ]
 
+++

### TOML and YAML

I know hee-haw about either of these but they're important for new pages in
Hugo. I've worked out (from the fact my config is `config.toml`!) that I need to
be using TOML. So metadata is contained within three plus signs (`+++`.) Groovy.

#### DESCRIPTIONS

Page descriptions have to go on one line (no `\n`) which messes up my pretty 80
character wide `markdown` files!

#### CATEGORIES

Categories are useful for site organisation. Post tags can be listed in a
comma separated list in square brackets, eg `[ "example" ]`

#### CREATING NEW POSTS

In this theme (Lanyon), posts are contained within `content/posts/`. This means
when I type `hugo new` I need to pre-fix my file name with `posts/` which is a
little annoying! Hyphenate the post title and remember to add `.md`.

Soon I'll be ready to start actually using this thing!

[Home](/blog "ohthreefive")
