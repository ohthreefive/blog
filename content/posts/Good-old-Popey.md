+++

date        = "2017-02-16T21:24:07Z"
title       = "Good old Popey"
description = "Alan Pope of Canonical is a good chap."
tags        = [ "random" ]

+++

### ALAN POPE DOES NOT KNOW WHO I AM

I've been listening to the [Ubuntu UK podcast][1] for at least two years. It's
usually fun, informative and mercifully short in a world of 90min+ podcasts.

That's where I first came across [Popey][2], a Canonical employee and rather
pleasant sounding chap.

I recently e-mailed the show to share my experiences with the [Nexdock][3] and
was delighted when it was read out. FAME!

Anyway, this is as close as Alan Pope is to knowing of my existence.

### I'M A MASOCHIST, BUT NOT THE COOL KIND

For at least 9 years now, I've been playing about with computers. Most recently
I've been playing with Ubuntu and Raspberry Pis and forming strong opinions
about privacy.

I've also been finding some (*short to medium term*) more difficult (*for me at
least*) ways of doing simple things. Let's take password management as an
example. I used [1Password][4] happily for some time, syncing my passwords over
Dropbox to give me my passwords on OS X, iOS, Android and Linux (albeit in a
roundabout way.)

But Dropbox and privacy don't marry well and I didn't want my Linux solution to
be lacklustre. I discovered [The Standard Unix Password Manager][5] via
another podcast ([Linux Voice][6]) and decided to give it a go.

My only issues were very poor passing knowledge of `git` and no prior use of
`gpg` whatsoever. I've now got to grips with `pass` but have been getting more
and more interested in `gpg`.

### I HAVE NO SUITABLY TECH-Y MATES!

In a [previous blog post][7] I went through my steps installing CopperheadOS
on a Nexus 5X I bought. Given it's slant on privacy, I thought I'd try sending
an encrypted e-mail. I'd read up on how to do this using the K-9 email client on
Android and was confident I'd manage. But who could I send it to?

None of my friends use Linux (except ChromeOS...) Most are iOS users. All think
I'm a prime NERD. So someone I knew was out. So with the cursor blinking in the
search box on Keybase I thought, "Randomly sending an encrypted email to Alan
Pope wouldn't be a completely dickish move, would it!?"

I kept things as short and apologetic as possible. Spam email = bad. Encrypted
spam = double bad. Word-y encrypted spam = doubleplus bad. It was an effort, I
have to say: as you can see from this post, I can string out a 'thank you' some
way.

Anyway, in among all the email he must get day to day, he found time to open
mine and reply, letting my know it had worked.

#### THANK YOU, ALAN POPE!

[1]: http://ubuntupodcast.org/ "No 2017 episodes yet!"

[2]: https://twitter.com/popey "@popey on twitter"

[3]: http://nexdock.com/ "New Nexdock coming this year!"

[4]: https://1password.com/ "excellent app/service"

[5]: https://www.passwordstore.org/ "i love you, pass"

[6]: https://www.linuxvoice.com/category/podcasts/ "highly recommended"

[7]: https://ohthreefive.gitlab.io/post/Copperhead-OS/ "what a well-written piece"

[Home](/blog "ohthreefive")
