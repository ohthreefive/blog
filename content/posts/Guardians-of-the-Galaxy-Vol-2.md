+++

tags = ["movie"]
date = "2017-09-21T16:44:33+01:00"
description = "Brief movie reviews #10"
title = "Guardians of the Galaxy Vol. 2"

+++

I *liked* Guardians, but I didn't love it. Drax was the highlight for me, in
terms of humour. Chris Pratt was also excellent.

Vol. 2 was hilarious on many occasions, predominantly through baby Groot.
However, overall the film was just too hollow. I don't know the comics, but I'm
guessing many people don't either, so things like Yondu's arc had little emotional impact on
me.

Drax has been nearly ruined, heading towards full Gimli. Why, in a movie full of
wise-crackers, one character needs to be reduced to one-note comic relief I do
not know. His tragic back story is too leavened by the nipple and cock gags.

The third act was also fairly yawn-some.

2/5

[Home](/blog "ohthreefive")
