+++

title       = "Hacking With SwiftUI Day 10"
description = "i see you, `struct`!"
date        = "2022-06-09"
tags        = [ "programming", "swift" ]

+++

A `struct` is basically an `object`, yeah?

I am asking for no credit if I'm correct, as I _never_ got my head around objects when learning Python or JavaScript.

`self` and `init` brought out goosebumps.

`method` - not much better.

Ah well, onward!

[Home](/blog "ohthreefive")
