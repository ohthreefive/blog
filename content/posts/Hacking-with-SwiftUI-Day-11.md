+++

title       = "Hacking With SwiftUI Day 11"
description = "i'm about to be left behind"
date        = "2022-06-10"
tags        = [ "programming", "swift" ]

+++

So, I solved the Checkpoint 6 quickly:

{{< highlight swift >}}
struct Car {
	let model: String
	let numberOfSeats: Int
	private(set) var currentGear: Int = 1
	
	public mutating func changeGearUp() {
		if currentGear + 1 == 11{
			print("You're at your highest gear!")
		} else {
			currentGear = currentGear + 1
		}
	}
	
	public mutating func changeGearDown() {
		if currentGear - 1 == 0 {
			print("You're at your lowest gear!")
		} else {
			currentGear = currentGear - 1
		}
	}
	
//    init(model: String, numberOfSeats: Int) {
//        self.model = model
//        self.numberOfSeats = numberOfSeats
//    }
}

var tesla = Car(model: "Tesla", numberOfSeats: 5)
print(tesla)
tesla.changeGearUp()
print(tesla)
{{< / highlight >}}

But I'm worried if I maybe did it *too quickly*? Is this just a basic but working solution that's fine for checkpoints but not real progress? Hmmm.

[Home](/blog "ohthreefive")
