+++
         
date = "2022-06-11T15:39:54.617Z"
title = "Hacking with SwiftUI Day 12"
description = "Inheritance is confuddling"
tags = [ "programming", "swift" ]

+++

{{< highlight swift >}}

class Animal {
    var legs: Int
    
    init(legs: Int) {
        self.legs = legs
    }
}

class Dog: Animal {
    func speak() {
        print("I'm a dog, woof woof!")
    }
    
}

class Corgi: Dog {
    override func speak() {
        print("Yappity yap")
    }
}

class Poodle: Dog {
    override func speak() {
        print("GRRRRRrrrrrrr!")
    }
}

class Cat: Animal {
    var isTame: Bool
    
    func speak() {
        print("Listen to my lovely meow!")
    }
    
    init(legs: Int, isTame: Bool) {
        self.isTame = isTame
        super.init(legs: legs)
    }
}

class Persian: Cat {
    override func speak() {
        print("Listen to my purrrrrr!")
    }
}

class Lion: Cat {
    override func speak() {
        print("Listen to my ROAR!")
    }
}

var genericBird = Animal(legs: 2)
print(genericBird.legs)
var genericDog = Dog(legs: 4)
genericDog.speak()
var genericCat = Cat(legs: 4, isTame: true)
genericCat.speak()
var genericCorgi = Corgi(legs: 4)
genericCorgi.speak()
var genericPoodle = Poodle(legs: 4)
genericPoodle.speak()
var genericPersian = Persian(legs: 4, isTame: true)
genericPersian.speak()
var genericLion = Lion(legs: 4, isTame: false)
genericLion.speak()

{{< / highlight >}}

It's not the prettiest, and it took me a LOOONG time, but it satisfies the requirements!

[Home](/blog "ohthreefive")