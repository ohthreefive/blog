+++

title       = "Hacking With SwiftUI Day 13"
description = "Surely I'll be found out soon?"
date        = "2022-06-12"
tags        = [ "programming", "swift" ]

+++

My tiny brain can see how important protocols and extensions are.

**BUT WILL I EVER UNDERSTAND THEM!>**

The requirements for checkpoint 8 were a little too easy:

{{< highlight swift >}}
protocol Building {
	var rooms: Int { get set }
	var cost: Int { get set }
	var estateAgent: String { get set }
	
	func salesSummary()
}

struct House: Building {
	var rooms: Int
	var cost: Int
	var estateAgent: String
	
	func salesSummary() {
		print("""
The house has \(rooms) rooms,
costs $\(cost) and is being sold
by \(estateAgent).
""")
	}
}

struct Office: Building {
	var rooms: Int
	var cost: Int
	var estateAgent: String
			
	func salesSummary() {
		print("""
The office has \(rooms) rooms,
costs $\(cost) and is being sold
by \(estateAgent).
""")
	}
}

let buchananDr = House(rooms: 5, cost: 500_000, estateAgent: "GSPC")
let bathSt = Office(rooms: 4, cost: 750_000, estateAgent: "Taylor's")

buchananDr.salesSummary()
bathSt.salesSummary()
{{< / highlight >}}

I solved that without much effort. It feels like more effort should have been required to prove I understood today's lessons!

[Home](/blog "ohthreefive")
