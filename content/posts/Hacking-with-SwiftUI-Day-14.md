+++

title       = "Hacking With SwiftUI Day 14"
description = "I took my first day off yesterday"
date        = "2022-06-14"
tags        = [ "swift", "programming" ]

+++

Just as well I took a day off, as optionals nearly crashed me.

I had to watch every video twice, then break down the checkpoint step by step.

But I got there, and I even managed to figure out the type annotation for an array of optional integer arrays!

{{< highlight swift >}}
var myArray: [Int]? = [Int]()          // creates an empty optional array
var myRealArray: [Int]? = [1, 2, 3, 4] // creates an filled optional array
var doesNtExist: [Int]? = nil          // this array does not exist - it is not 'empty'
									   // myArray and myRealArray COULD be non-optional if i wanted

if let myArray = myArray {             // all three arrays are optional, this one is empty
	print("Optional array exists")
	print(myArray)
}

if let myRealArray = myRealArray {     // 'if let' only works on optionals and _UNWRAPS_ them into a
	print("Optional array exists")     // temporary var or constant, traditionally of the same name
	print(myRealArray)                 // this array _has_ values which are printed
}

if let doesNtExist = doesNtExist {     // this optional array has NO value and does not
	print("Optional array exists")     // satisfy the 'if' because of that fact
	print(doesNtExist)
} else {                               // if let runs the code if the optional exists
	print("That array does not exist!")// but you can have an else statement too to do something
}

// convert our 'ifs' to a function

func doesArrayExist(name: [Int]?) {    // has to accept optional arrays; if we tell Swift we are
	if let name = name {               // passing a non-optional array, that's what it expects
		print("If let says: Optional array exists") // and so 'if let' would make no sense
		print(name)
	} else {
		print("If let says: That optional array does not exist!")
	}
}

doesArrayExist(name: myArray)
doesArrayExist(name: myRealArray)
doesArrayExist(name: doesNtExist)

// 'guard let' runs some code if the optional does not exist but MUST return something to end it's
// statement. 'guard let' includes 'else' as it's effectively the 'else' side of an 'if let'
// after the return, you can carry on with code, including input parameters, without another else

func doesArrayExistGuarded(name: [Int]?) { // guard let allows code to stop running as soon as a
	guard let name = name else {           // check has not been satisfied
		print("Guard says: That optional arrays does not exist")
		return
	}
	print("Guard says: Optional array exists")
	print(name)
}

doesArrayExistGuarded(name: myArray)
doesArrayExistGuarded(name: myRealArray)
doesArrayExistGuarded(name: doesNtExist)

// use nil coalescing to return a default, NON OPTIONAL, value

var i = 0
while i < 2 {
	let randomNo1 = myRealArray?.randomElement() ?? Int.random(in: 5...100)  // passes - array exists and has values
	let randomNo2 = myArray?.randomElement() ?? Int.random(in: 5...100)      // fails - random element returns 'nil'
	let randomNo3 = doesNtExist?.randomElement() ?? Int.random(in: 101...200)// fails - optional array does not exist so 'nil' is returned
	print("\(randomNo1) & \(randomNo2) & \(randomNo3)")
	i += 1
}

// a one line function which accepts an optional array and returns a random val or nil coalesces
func checkpoint(arr: [Int]?) -> Int { arr?.randomElement() ?? Int.random(in: 1...100) }

// an arrary of optional integer arrays
let arrayOfArrays: [[Int]?] = [myArray, myRealArray, doesNtExist]

// loop through the array, calling the function
for i in arrayOfArrays {
	print("generating random number: ")
	print(checkpoint(arr: i))
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
