+++
date = "2022-06-16T20:50:57.118Z"
title = "Hacking with SwiftUI Day 16"
description = "Whoop! I’ve made something appear on a screen!"
tags = [ "programming", "swift" ]

+++

Well, for the first time in my life, I have typed something on a screen and seen
it on a device... albeit an emulator.

Building apps is cool. SwiftUI is cool. But this looks **HARD**.

[Home](/blog "ohthreefive")