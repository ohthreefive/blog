+++

title       = "Hacking With SwiftUI Day 17 Fail"
description = "My project went wrong..."
date        = "2022-06-17"
tags        = [ "programming", "swift" ]

+++

Here's a screenshot of the dodgy toolbar:

![An iPhone emulator screenshot](https://ohthreefive.gitlab.io/blog/images/screenshot.png "The toolbar has two 'done' buttons")

[Home](/blog "ohthreefive")
