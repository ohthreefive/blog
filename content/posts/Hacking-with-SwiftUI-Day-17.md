+++

title       = "Hacking With SwiftUI Day 17"
description = "holy crap i made an app!"
date        = "2022-06-17"
tags        = [ "programming", "swift" ]

+++

Wow. Paul's tutorials really are something!

I have created a bare bones app.

Sadly I've already made a bug, with a duplicated label on the
keyboard toolbar.

Here's the code:

{{< highlight swift >}}
struct ContentView: View {
	@State private var billAmount = 0.0
	@State private var noOfPeople = 2
	@State private var tipPercentage = 20
	@FocusState private var amountIsFocused: Bool // Swift knows when this var has focus
	
	let tipPercentages = [0, 10, 15, 20, 25]
	
	var totalPerPerson: Double {
		let peopleCount = Double(noOfPeople + 2) // in the picker, '2 people' is the 0th option...
		let tipSelection = Double(tipPercentage) // we need all our numbers to be doubles
		let amountPerPerson = (billAmount + (tipSelection / 100 * billAmount)) / peopleCount
		
		return amountPerPerson
	}
	
	var body: some View {
		NavigationView {
			Form {
				Section {
					TextField("Amount", value: $billAmount, format: .currency(code: Locale.current.currencyCode ?? "USD"))
						.keyboardType(.decimalPad)
						.focused($amountIsFocused) // the text field sets the FocusState var
					
					Picker("Number of people:", selection: $noOfPeople) {
						ForEach(2..<50) {
							Text("\($0) people") // the picker won't be selectable unless this form is in a navigation view
						}
					}
				}
				
				Section {
					Picker("Tip percentage", selection: $tipPercentage) {
						ForEach(tipPercentages, id: \.self) {
							Text($0, format: .percent)
						}
					}
					.pickerStyle(.segmented) // a nice style showing all options
				} header: {                  // adds a little header to the section
					Text("How much tip?")
				}
				
				Section {
					Text(totalPerPerson, format: .currency(code: Locale.current.currencyCode ?? "USD"))
				} header: {
					Text("Total per person:")
				}
				.navigationTitle("WeSplit") // the FORM has this title, not the Nav view
				.toolbar {                    // a toolbar for this whole form
					ToolbarItemGroup(placement: .keyboard) {  // toolbar above keyboard
						Spacer()              // add whitespace first
						
						Button("Done") {      // the toolbar has a button labelled done
							amountIsFocused = false  // pressing 'done' removes focus
						}
					}
				}
			}
		}
	}
}
{{< / highlight >}}

Maybe the kind forum people will help me!

Here's the screenshot:

[Screenshot](https://ohthreefive.gitlab.io/blog/images/screenshot.png)

[Home](/blog "ohthreefive")
