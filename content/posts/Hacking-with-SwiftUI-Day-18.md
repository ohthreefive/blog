+++

title       = "Hacking With SwiftUI Day 18"
description = "Aaaaaand relax!"
date        = "2022-06-18"
tags        = [ "programming", "swift" ]

+++

Day 18 was a joy compared to day 17!

Just some tweaking to the WeSplit code.

As an added bonus, my problem with my `.toolbar` was fixed by someone in
the forums - wrong placement of my toolbar code!

[Home](/blog "ohthreefive")
