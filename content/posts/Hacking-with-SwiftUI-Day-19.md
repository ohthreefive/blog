+++

title       = "Hacking With SwiftUI Day 19"
description = "This app is MY app"
date        = "2022-06-20"
tags        = [ "programming", "swift" ]

+++

Day 19 has a challenge to build a 'from scratch' converter app. I chose temperature, to minimise the conversions.

I managed this in about 90 minutes in a coffee shop on Swift Playgrounds on an iPad tethered to my phone so I could read through the tutorial text from the WeSplit app.

This was great fun!

What I really wanted, however, was not to simply use a string array of temperature units and a bunch of `if..else` calls. I thought a `set` (or having seen other people's solutions, an `enum`) and a bunch of `switch..case` would have worked better, but I elected to stop with what I got to.

I also would have loved to have got the units working better (as in, displaying the degrees Celsius symbol etc) but got almost nowhere with `.formatted()`. Almost nowhere, but not entirely nowhere.

I discovered, by reading and copy-pasting Apple's documentation, that Swift has a built in way to display a value as a temperature in the user's locale-based preferred temperature.

{{< highlight swift >}}
let string = Measurement<UnitTemperature>(value: 38, unit: .celsius).formatted()
{{< / highlight >}}

Could be replaced with:

{{< highlight swift >}}
let display = Measurement<UnitTemperature>(value: inputTemp, unit: .celsius).formatted()
Text(display)
{{< / highlight >}}

In my output section. It would take the input temperature and display <some temp>˚C, which is neat. Unfortunately I couldn't find a way of dynamically changing the `unit:` for input between the options Celsius, Farenheit and Kelvin, and as the formatter is designed to display the locale-based temperature unit, there was no easy way I could see to convert to a chosen format. Fun learning though!

{{< highlight swift >}}
import SwiftUI

struct ContentView: View {
	@State private var inputUnit = "Farenheit"
	@State private var inputTemp = 0.0
	@State private var outputUnit = "Farenheit"
	@FocusState private var tempIsFocused: Bool
	
	let units = ["Celsius", "Farenheit", "Kelvin"]
	
	// Regardless of user's choice of input, convert temperature to celsius
	var celsiusAsInputTemp: Double {   // Computed property; never displayed
		if inputUnit == "Farenheit" {
			return Double(((inputTemp - 32) * 5 / 9))
		} else if inputUnit == "Kelvin" {
			return inputTemp - 273.15
		} else {
			return inputTemp
		}
	}
	
	// Take temp we have just converted to celsius and make conversion based on it
	var outputTemp: Double {
		if outputUnit == "Farenheit" {
			return Double(celsiusAsInputTemp * 9 / 5 + 32)
		} else if outputUnit == "Kelvin" {
			return celsiusAsInputTemp + 273.15
		} else {
			return celsiusAsInputTemp
		}
	}
	
	var body: some View {
		NavigationView {
			Form {
				Section {
					TextField("Input temperature?", value: $inputTemp, format: .number)
						.keyboardType(.decimalPad)
						.focused($tempIsFocused)
					// let string = Measurement<UnitTemperature>(value: 38, unit: .celsius).formatted()
					// For locale: en_US: 100.4°F
				} header: {
					Text("Temperature to convert:")
				}
				Section {
					Picker("Select input unit", selection: $inputUnit) {
						ForEach(units, id: \.self) {
							Text($0)
						}
					}.pickerStyle(.segmented)
				} header: {
					Text("Units to convert from:")
				}
				Section {
					Picker("Select output unit", selection: $outputUnit) {
						ForEach(units, id: \.self) {
							Text($0)
						}
					}.pickerStyle(.segmented)
				} header: {
					Text("Units to convert to:")
				}
				Section {
					Text(outputTemp, format: .number)
				} header: {
					Text("Converted temperature:")
				}
			}
			.navigationTitle("Temperature converter")
			.navigationBarTitleDisplayMode(.inline)
			.toolbar {
				ToolbarItemGroup(placement: .keyboard) {
					Spacer()
					Button("Done") {
						tempIsFocused = false
					}
				}
			}
		}
	}
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
