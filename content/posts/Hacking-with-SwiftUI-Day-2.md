+++

title       = "Hacking With SwiftUI Day 2"
description = "So far so good; common issue re-encountered"
date        = "2022-06-01T21:44:25.755Z"
tags        = [ "programming", "swift" ]

+++

Ok, I remember strings and interpolation, though Swift does it a bit
differently.

Major issue though: it's day 2 and I barely found time to fit this in. Yikes.

[Home](/blog "ohthreefive")
