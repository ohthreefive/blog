+++

title       = "Hacking With SwiftUI Day 20"
description = "A taste of what's to come"
date        = "2022-06-21"
tags        = [ "programming", "swift" ]

+++

Well Paul, thanks for introducing a whole lot of useful new functionality.

I strongly feel like my biggest struggle will be with what goes where -
inside this view? That view?

But I guess that's what I'm going to need to learn!

[Home](/blog "ohthreefive")
