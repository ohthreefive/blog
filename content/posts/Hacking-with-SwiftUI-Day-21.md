+++

title       = "Hacking With SwiftUI Day 21"
description = "I am not a designer"
date        = "2022-06-23"
tags        = [ "programming", "swift" ]

+++

I see what Paul is doing - introducing us to the vast array of options SwiftUI
gives us to customise our app and showing us how quickly we can see what they
look like. It piques the interest and rather glosses over the fact that there
are so many options, the syntax is somewhat confusing and UI/UX design is a
whole different field from coding.

Man, SwiftUI is a pleasure though!

[Home](/blog "ohthreefive")
