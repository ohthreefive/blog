+++
date = "2022-06-24T21:09:34.020Z"
title = "Hacking with SwiftUI Day 22"
description = "Hmmm, I completed the challenge but not in the pre-ordained manner"
tags = [ "programming", "swift" ]

+++

The tutorials leading up to the challenge telegraphed a couple of the
challenges, but not the 'reset the game' one. Paul's only hint was to use the
`reset()` method but after a bit of searching my memory (decided it hadn't come
up in the previous tutes) and then searching the web (unhelpfully vague search
term) I rolled my own solution to reset the Guess the Flag game.

{{< highlight swift >}}
import SwiftUI

struct ContentView: View {
    @State private var scoreTitle = ""
    @State private var finalTitle = ""
    @State private var showingScore = false
    @State private var finalScore = false
    @State private var countries = ["Estonia", "France", "Germany", "Ireland",
                                    "Italy", "Nigeria", "Poland", "Russia",
                                    "Spain", "UK", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var userScore = 0
    @State private var totalQuestions = 0
    var body: some View {
        ZStack {
            RadialGradient(stops: [
                .init(color: .white, location: 0.45),
                .init(color: .gray, location: 0.45)
            ], center: .top, startRadius: 150, endRadius: 700)
                .ignoresSafeArea()
            VStack {
                Spacer()
                Text("Guess the Flag!")
                    .font(.largeTitle.bold())
                    .foregroundColor(.primary)
                VStack(spacing: 15) {
                    VStack {
                        Text("Tap the flag of")
                            .foregroundColor(.secondary)
                            .font(.subheadline.weight(.heavy))
                        Text(countries[correctAnswer])
                            .font(.largeTitle.weight(.heavy))
                    }
                    ForEach(0..<3) { number in
                        Button {
                            flagTapped(number)
                        } label: {
                            Image(countries[number])
                                .renderingMode(.original)
                                .clipShape(Capsule())
                                .shadow(radius: 5)
                        }
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.thinMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 20))
                Spacer()
                Spacer()
                Text("Score: \(userScore)/\(totalQuestions)")
                    .foregroundColor(.primary)
                    .font(.title.bold())
                Spacer()
            } .padding()
        }
        .alert(scoreTitle, isPresented: $showingScore) {
            Button("Continue", action: nextQuestion)
        } message: {
            Text("Current score: \(userScore)/\(totalQuestions)")
        }
        .alert(finalTitle, isPresented: $finalScore) {
            Button("Start again", action: resetGame)
        } message: {
            Text("Final score: \(userScore)/\(totalQuestions)")
        }
    }
    func flagTapped(_ number: Int) {
        totalQuestions += 1
        if totalQuestions < 8 {
            if number == correctAnswer {
                scoreTitle = "Correct"
                userScore += 1
            } else {
                scoreTitle = "Wrong, that's \(countries[number])"
            }
            showingScore = true
        } else {
            if number == correctAnswer {
                userScore += 1
                finalTitle = "You got your last question right!"
            } else {
                finalTitle = "Bad luck, that was \(countries[number])"
            }
            finalScore = true
        }
    }
    func nextQuestion() {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
    }
    func resetGame() {
        userScore = 0
        totalQuestions = 0
    }
}
{{< / highlight >}} 

[Home](/blog "ohthreefive")