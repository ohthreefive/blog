+++

title       = "Hacking With SwiftUI Day 23"
description = "Practice before theory"
date        = "2022-06-25"
tags        = [ "programming", "swift" ]

+++

A really enjoyable series of tutorials. Paul took us through the theory of a 
number of things we'd already been putting into practice (mainly modifiers). By
doing so, it was easier to understand the tutorials. Smart!

Then all the 'custom' things came in. I suspect I will struggle when putting
these into practice.

[Home](/blog "ohthreefive")
