+++

title       = "Hacking With SwiftUI Day 24"
description = "i wrote custom things!"
date        = "2022-06-26"
tags        = [ "programming", "swift" ]

+++

Today's challenges really are a testament to Paul's teaching style.

I've learnt some Python, JavaScript, R, and BASH in the past, but I've *NEVER*
done any interface design.

By abstracting away the design element and showing me that SwiftUI is just
code describing a layout, this series has so far allowed me not only to design
a simple app from scratch within three weeks of starting, but has also taken me
further into the understanding of the Swift language than other tutorial series
have for their languages. Obviously what I'm achieving now is also built on the
back of what I learnt before, but I have never written anything as complex as I
am doing now.

And I've certainly never enjoyed it so much.

On the fly decision over whether text should be red or the system's colour
based on a `State` using a ternary operator:

{{< highlight swift >}}
Text(billAmount + (billAmount * Double(tipPercentage) / 100), format: userCurrency)
	.foregroundColor(tipPercentage == 0 ? .red : .primary)
{{< / highlight >}}

A custom `View` that returns a formatted image which can be called inside
another `View`:

{{< highlight swift >}}
struct FlagImage: View {
	var country: String
	
	var body: some View {
		Image(country)
			.renderingMode(.original)
			.clipShape(Capsule())
			.shadow(radius: 5)
	}
}

...

ForEach(0..<3) { number in
						Button {
							flagTapped(number)
						} label: {
//                            Image(countries[number])      this is the old code
//                                .renderingMode(.original)
//                                .clipShape(Capsule())
//                                .shadow(radius: 5)
							FlagImage(country: countries[number])
						}
					}
{{< / highlight >}}

A `ViewModifier` which takes the `content` it's fed and modifies it (in this
case, just some font formatting changes) and returns this formatted `View` AND
an extension to the whole `View` protocol which allows us to call our
`ViewModifier` in fewer clicks!

{{< highlight swift >}}
// below is a view modifier whose purpose is to modify the content passed to
// it by changing its font to largeTitle and colour to blue
// to us as is, under an eg Text field, we would write:
// .modifier(ProminentTitle())
struct ProminentTitle: ViewModifier {
	func body(content: Content) -> some View {
		content
			.font(.largeTitle)
			.foregroundColor(.blue)
	}
}

// to make that less cumbersome, we can make our struct and extension of the
// View protocol, now all we would need to write is .titleStyle()
extension View {
	func titleStyle() -> some View {
		modifier(ProminentTitle())
	}
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
