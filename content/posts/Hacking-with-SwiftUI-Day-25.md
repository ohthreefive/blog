+++

title       = "Hacking With SwiftUI Day 25"
description = "Another challenge completed in my own basic way"
date        = "2022-06-28"
tags        = [ "programming", "swift" ]

+++

A nice challenge that didn't take me too long.

My solution remains fugly and I'm not confident about the logic of the 'correct
answer picker'



{{< highlight swift >}}
struct ContentView: View {
	let choices = ["🪨", "📃", "✂"]
	
	@State private var score = 0
	@State private var currentChoice = "🪨"
	@State private var shouldWin = Bool.random()
	
	// the logic for whether or not the user's choice is correct
	var correctReponse: String {
		var answer: String {
				if currentChoice == "🪨" {
				return shouldWin ? "✂" : "📃"
			} else if currentChoice == "📃" {
				return shouldWin ? "🪨" : "✂"
			} else {
				return shouldWin ? "📃" : "🪨"
			}
		}
		
		return answer
	}
	
	func buttonPressed(_ chosen: String) {
		if chosen == correctReponse {
			score += 1
			nextQuestion()
		} else {
			score -= 1
			nextQuestion()
		}
	}
	
	func nextQuestion() {
		shouldWin.toggle()
		currentChoice = choices[Int.random(in: 0...2)]
	}
	
	var body: some View {
		NavigationView {
			ZStack {
				LinearGradient(colors: [.white, .gray],
							   startPoint: .top,
							   endPoint: .bottom)
				.ignoresSafeArea()
				
				VStack {
					
					Spacer()
					
					VStack {
							Text("Current score: \(score)")
					}
					
					Spacer()
					
					VStack {
							Text("We chose: \(currentChoice)")
							Text("You should: \(shouldWin ? "WIN" : "LOSE")")
							// Text("Correct response: \(correctReponse)")
					}
					
					Spacer()
					Spacer()
					Spacer()
					
					VStack {
						Text("CHOOSE!")
					}
					
					HStack {
						ForEach(choices, id: \.self) { choice in
							Button {
								buttonPressed(choice)
							} label: {
								Text("\(choice)")
									.font(.system(size: 75))
							}
						}
					}
					
					Spacer()
					
				}
			}
			.navigationTitle("Rock paper scissors brain trainer!")
			.navigationBarTitleDisplayMode(.inline)
		}
	}
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
