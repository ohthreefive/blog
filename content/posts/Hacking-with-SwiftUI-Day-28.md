+++

title       = "Hacking With SwiftUI Day 28"
description = "i think it's starting to sink in..."
date        = "2022-07-07"
tags        = [ "programming", "swift" ]

+++

I'm slowing down, with enforced day or two breaks in between sessions, which is
not ideal. However, I'm still able to keep up with the tutorials, and today
(well, yesterday - I'm late to post) was another challenge day.

Paul had given us the tools to build an ML model and insert it into a simple
app. There were three inputs and one output, generated with a button on the
toolbar and showing the result as an alert.

Paul asked us to change the `VStacks` to `Sections` (easy, and with use of
padding I thought they looked better).

We had to change a `Stepper` to a `Picker`. Again, fairly easy. I preferred the
look of the `Stepper` and forgot the `id: /.self` in the `for` loop to ensure
the `Picker`'s selection reflected the true order of the `Picker`. ie. option
0 should reflect the first item (which might have been 1, not 0, cups of
coffee).

The last change was to make the output, the 'go to sleep' calculation, to be
permanently on screen and remove the calculate button.

I scratched my head and tried various complicated ways of achieving this. How
could I trigger the `function` (`method`) to calculate this time every time any
of the three input `variable`s are changed? Naturally I approached this in an
old-fashioned way, discovering the `.onChange` Modifier and trying to attach it
to all three inputs. The major issue I had, however, was that I thought I would
have to make the go to sleep time a computed property AND a `@State` so that the
`Text` view I would use to generate it would update automatically. In the back
of my mind was Paul's maxim that Swift's "views are a function of their state".
(Not Paul's maxim, truly, but one he quotes).

I was kind of on the right track, but was over-complcating things. I recognised
that the `function` that caused the `alert` to show could have a `return` and
that `return` could be the go to sleep time. But I couldn't think beyond
attaching that to a `property` and displaying the `property`. Turns out the
`Text` view could just run the `function`, and what I wanted would be achieved.
I like how simple that is!

So here it is:



{{< highlight swift >}}
import CoreML
import SwiftUI

struct ContentView: View {
	@State private var coffeeAmount = 1
	@State private var sleepAmount = 8.0
	@State private var wakeUp = defaultWakeTime
	
	@State private var alertTitle = ""
	@State private var alertMessage = ""
	@State private var showingAlert = false
	
	static var defaultWakeTime: Date {
		var components = DateComponents()
		components.hour = 7
		components.minute = 30
		return Calendar.current.date(from: components) ?? Date.now
	}
 
	var body: some View {
		NavigationView {
			Form {
				Section {
					Text("When do you want to wake up?")
						.font(.headline)
						.padding(.vertical, 10.0)
					
					DatePicker("Please enter a time",
							   selection: $wakeUp,
							   displayedComponents: .hourAndMinute)
					.padding(.vertical, 20.0)
					.labelsHidden()
				}
				
				Section {
					Text("How much sleep do you want?")
						.font(.headline)
						.padding(.vertical, 10.0)
					
					Stepper("\(sleepAmount.formatted()) hours",
							value: $sleepAmount,
							in: 4...12,
							step: 0.25)
					.padding(.vertical, 20.0)
				}
				
				Section {
					Text("Cups of coffee today?")
						.font(.headline)
						.padding(.vertical, 10.0)
					
					Picker("Number of cups", selection: $coffeeAmount) {
						ForEach(0..<21, id: \.self) {
							Text($0 == 1 ? "\($0) cup" : "\($0) cups")
						}
					}
					.padding(.vertical, 20.0)
					}
				
				Section {
					Text("Ideal bedtime:")
						.font(.headline)
						.padding(.vertical, 10.0)
					Text("\(calculateBedtime())")
						.font(.largeTitle)
						.padding(.vertical, 20.0)
					
				}
			}
			.navigationTitle("BetterRest")
		}
	}

	
	func calculateBedtime() -> String {
		do {
			let config = MLModelConfiguration()
			let model = try SleepCalculator(configuration: config)

			let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
			let hour = (components.hour ?? 0) * 3600
			let minute = (components.minute ?? 0) * 60

			let prediction = try model.prediction(wake: Double(hour + minute), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount))
			let goToSleepAt = wakeUp - prediction.actualSleep
			
			print(coffeeAmount)

			return goToSleepAt.formatted(date: .omitted, time: .shortened)

		} catch {
			return "ERROR"
		}
	}
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
