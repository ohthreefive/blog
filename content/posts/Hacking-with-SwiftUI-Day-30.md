+++

title       = "Hacking With SwiftUI Day 30"
description = "Another day, another bunch of code"
date        = "2022-07-12"
tags        = [ "programming", "swift" ]

+++

The WordScramble game has been a pleasure so far - more methods than any project
before and a simple UI.

The way to make methods interact - and therefore split code apart - is well
demonstrated here and it's a definite weakness of mine. This will definitely be
a project I review!

Next up, Challenge day!

[Home](/blog "ohthreefive")
