+++

title       = "Hacking With SwiftUI Day 31"
description = "Easiest challenge day yet?"
date        = "2022-07-12"
tags        = [ "programming“, “swift" ]

+++

I don’t know if it was the nature of the challenges or truly progressing with my learning, but I knocked out the challenges for Word Scramble in no time.

A score field - easy text view linked to a `var`.
A scoring system - I decided one point per letter apart from anagrams (8 letters) which scored double. I figured out when to make that calculation in the code (when the user submits the word - so no new method or button).
A flourish - use the circled word length SF Symbol to represent the code - I figured out an `if…else` to display ‘16’ if word length was 8 and change its colour to highlight the bonus points.
Lastly a reset game button - lifted toolbar code from a previous project, more or less.

Good fun!

{{< highlight swift >}}
struct ContentView: View {
	@State private var usedWords = [String]()
	@State private var rootWord = ""
	@State private var newWord = ""
	@State private var currentScore = 0
	
	@State private var errorTitle = ""
	@State private var errorMessage = ""
	@State private var showingAlert = false
	
	var body: some View {
		NavigationView {
			
			List {
			
				Section {
					TextField("Enter a word", text: $newWord)
				}
				
				Section {
					Text("Score: \(currentScore)")
						.font(.title2)
						.fontWeight(.semibold)
						.foregroundColor(Color.purple)
						
				}
				
				Section {
					ForEach(usedWords, id: \.self) { usedWord in
						HStack {
							
//            this ternary operator neatly shows the word count of
//            the word, which is its score, unless it's an anagram
//            (ie. 8 letters) in which case you score double (16)
//            I could not work out how to change the color within
//            the ternary operator, however, hence the if...else

// usedWord.count == 8 ? Image(systemName: "16.circle") : Image(systemName: "\(usedWord.count).circle")
							
							if usedWord.count == 8 {
								Image(systemName: "16.circle")
									.foregroundColor(.red)
							} else {
								Image(systemName: "\(usedWord.count).circle")
							}
							Text(usedWord)
						}
					}
				}
			}
		.navigationTitle(rootWord)
		.onSubmit(addNewWord)
		.textInputAutocapitalization(.never)
		.onAppear(perform: startGame)
		.alert(errorTitle, isPresented: $showingAlert) {
			Button("OK", role: .cancel) { }
		} message: {
			Text(errorMessage)
		}
		.toolbar {
			ToolbarItemGroup(placement: .navigationBarTrailing) {
				Button("New word") {
					startGame()
					currentScore = 0
					usedWords = [String]()
				}
			}
		}
		}
	}
	
	func addNewWord() {
		let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
		
//        use the FOUR validation checkers +/- show an error
		guard isOriginal(word: answer) else {
			errorAlert(title: "You've used that before",
					   message: "Think harder!")
			return
		}
		
		guard isPossible(word: answer) else {
			errorAlert(title: "That doesn't work",
					   message: "You can only use letters contained in '\(rootWord)'.")
			return
		}
		
		guard isLongEnough(word: answer) else {
			errorAlert(title: "Too short",
					   message: "At least three letters long, please")
			return
		}
		
		guard isRealWord(word: answer) else {
			errorAlert(title: "Is that even a word?",
					   message: "Consult your own dictionary - mine says no!")
			return
		}
		
//        the code only 'gets' here if the 'word' is valid... so score it!
		currentScore += wordScore(word: answer)
		
//        add the correct word to the top of the list of answers
		usedWords.insert(answer, at: 0)
		
//        reset newWord
		newWord = ""
	}
	
	func startGame() {
//        try to find the start.txt file
		if let wordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {

//            load it (if let is needed as these return optionals)
			if let startWords = try? String(contentsOf: wordsURL) {
				
//                now that we have the file of words, make an array from it
				let allWords = startWords.components(separatedBy: "\n")
				
//                pick a random element from this array
				rootWord = allWords.randomElement() ?? "backlash"
				
//                all being well, we can exit the function
				return
			}
//            an error type if any of above has failed
			fatalError("Start.txt could not be found")
		}
	}
	
	func isOriginal(word: String) -> Bool {
//        flip to 'not' with '!' to check if the new word ISN'T in usedWords
		!usedWords.contains(word)
	}
	
	func isPossible(word: String) -> Bool {
//        temporary copy of rootWord for validation
		var tempWord = rootWord
		
//        loop through the letters of 'word', which is the user input
		for letter in word {
//            find the position of each letter of user entered word in root/tempWord
			if let pos = tempWord.firstIndex(of: letter) {
//                if you find it, remove that letter from tempWord then check next letter
				tempWord.remove(at: pos)
			} else {
				return false
			}
		}
//        if every letter in user entered 'word' has been checked to be in
//        tempWord, the 'word' is possible
		return true
	}

	func isLongEnough(word: String) -> Bool {
//    single line ternary operator method to check entered word length
		word.count > 2
	}
	
	func isRealWord(word: String) -> Bool {
//        objective c code
		let checker = UITextChecker()
		let range = NSRange(location: 0, length: word.utf16.count)
		
//        this checker checks WHERE in a word the misspelling is and returns
//        an NSRange object
		let misspelledRange = checker.rangeOfMisspelledWord(in: word,
															range: range,
															startingAt: 0,
															wrap: false,
															language: "en")
		
//        we can check if the returned object has a .location (ie was misspelled)
		return misspelledRange.location == NSNotFound
	}
	
	func wordScore(word: String) -> Int {
		word.count == 8 ? 16 : word.count
	}
	
	func errorAlert(title: String, message: String) {
		errorTitle = title
		errorMessage = message
		showingAlert = true
	}
}
{{< / highlight >}}

[Home](/blog "ohthreefive")
