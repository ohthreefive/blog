+++

title       = "Hacking With SwiftUI Day 32"
description = "Bring on the animations!"
date        = "2022-07-17"
tags        = [ "programming", "swift" ]

+++

Good little introduction to animations - they're trivial!

Let's try not to ANIMATE ALL THE THINGS! in future projects!

[Home](/blog "ohthreefive")
