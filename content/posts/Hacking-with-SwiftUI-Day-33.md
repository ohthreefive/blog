+++
date = "2022-07-24T14:04:56.092Z"
title = "Hacking with SwiftUI Day 33"
description = "Sloooooooowing down"
tags = [ "programming", "swift" ]

+++

It's taken me seven days to get through the first two days of videos of Project
6.

I fear my days of barrelling through SwiftUI happily are at an end and I'll be
scratching my head more and more in future.

I really must try to keep up with it though - it is **COOL**!

Animations - nifty.
Their syntax - ok.
Custom view modifiers and extensions ... I'm not there yet!

[Home](/blog "ohthreefive")