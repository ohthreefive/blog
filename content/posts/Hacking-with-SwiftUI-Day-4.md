+++

title       = "Hacking With SwiftUI Day 4"
description = "still enjoying this, still mostly revision"
date        = "2022-06-03"
tags        = [ "programming", "swift" ]

+++

Excellent early stuff from Paul Hudson. I was particularly interested in today's challenge -
rather than have some sort of `count unique` thingy for unique values in an Array,
just make a `Set` from them and count it!

[Home](/blog "ohthreefive")
