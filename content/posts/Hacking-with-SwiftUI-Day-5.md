+++

title       = "Hacking With SwiftUI Day 5"
description = "Still going strong - bring it on!"
date        = "2022-06-04"
tags        = [ "programming", "swift" ]

+++

I think the last time I took [CodeAcademy's](www.codeacademy.com) Javascript tutorial
I learnt about `case ... switch` and got really confused by ternary operators. Just
lovely to see them popping up in Swift!

[Home](/blog "ohthreefive")
