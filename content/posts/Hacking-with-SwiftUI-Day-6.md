+++

title       = "Hacking With SwiftUI Day 6"
description = "I still suck at while loops"
date        = "2022-06-06"
tags        = [ "programming", "swift" ]

+++

It only took until day 6 for me to forget my 'one post a day'!

`for` loops? I'm ok with that.

Using `_` when the iterator isn't needed? Interesting.

`while` loops? Still need some practice!

[Home](/blog "ohthreefive")
