+++

title       = "Hacking With SwiftUI Day 7"
description = "Functions - the limit of my experience!"
date        = "2022-06-06"
tags        = [ "swift", "programming" ]

+++

Well, that's the first week done and already we're at functions, which is I think
where I reach my limit of comfort with programming. Only 97 days to go!

[Home](/blog "ohthreefive")
