+++

title       = "Hacking With SwiftUI Day 8"
description = "F**k you, closures"
date        = "2022-06-08"
tags        = [ "programming", "swift" ]

+++

What have I done?

I'll be honest, I did successfully complete the 'lucky numbers' challenge:

{{< highlight swift >}}
let luckyNumbers = [7, 4, 38, 21, 16, 15, 12, 33, 31, 49]

let makeOddInOrderLucky = luckyNumbers.filter { $0.isMultiple(of: 2) == false } .sorted() .map { "\($0) is a lucky number" }

for i in makeOddInOrderLucky {
	print(i)
}
{{< / highlight >}}

Though I don't know if the challenge intended the `print` call to be built into one big function.

I will forget this whole _closures_ thing SO QUICKLY.

[Home](/blog "ohthreefive")
