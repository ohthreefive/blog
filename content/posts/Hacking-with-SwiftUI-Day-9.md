+++

title       = "Hacking With SwiftUI Day 9"
description = "Day 9 is day 8"
date        = "2022-06-09"
tags        = [ "programming", "swift" ]

+++

In my excitment at learning the new concept of throwing functions, I forgot my daily post.

So here is my day eight post, entitled Day 9 and posted on Day 10.

I'll be honest, I like the idea of posting my solutions to Paul's 'checkpoints', so here is checkpoint 4 by me:

{{< highlight swift >}}
enum sqrtError : Error {
	case outOfBounds
	case noRoot
}

func manualSqrt(_ number: Int) throws -> Int {
	if number < 1 || number > 10_000 {
		throw sqrtError.outOfBounds
	}
	
	var theSquare = Int()
	
	for i in 1...100 {
		if number == i * i {
			theSquare = i
		}
	}
	
	if theSquare == 0 {
		throw sqrtError.noRoot
	}
	
	return theSquare
	
}

let testInt = 2

do {
	let result = try manualSqrt(testInt)
	print("The square root of \(testInt) is \(result)")
	} catch sqrtError.outOfBounds {
		print("Please pick a number between 1 and 10,000")
	} catch sqrtError.noRoot {
		print("There is no square root of \(testInt)")
	}
{{< / highlight >}}

[Home](/blog "ohthreefive")
