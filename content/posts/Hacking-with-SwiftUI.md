+++

title       = "Hacking With SwiftUI"
description = "I’ve done myself an injury and need a project..."
date        = "2022-05-31T21:44:25.755Z"
tags        = [ "programming", "swift" ]

+++

*sigh*

My medial meniscus is done in. Properly done in. I see the surgeon in 90 minutes
but already fear the inevitable - surgical repair!

So naturally I’m going to try to learn programming. Again. This time it’s Swift,
with the aid of Paul Hudson’s [Hacking with SwiftUI][1].

First impression - great resource. For me, this was revision of concepts I’ve
already learned, but hey, it was only day one.

[Home](/blog "ohthreefive”)

[1]: https://www.hackingwithswift.com/100/swiftui/1 "Hacking with SwiftUI starts here!"