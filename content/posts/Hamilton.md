+++
date = "2024-03-23T11:52:10Z"
title = "Hamilton"
description = "Musical theatre!"
tags = ["Random"]

+++

When it was announced that Hamilton was coming to Scotland for the first time I made sure I was in place to get tickets as soon as possible.

After waiting just over a year, I’m happy to report it was well worth it. The whole family loved it. The first half is so much stronger than the second, but then, the first half is just banger after banger.

5/5

[Home](/blog "ohthreefive")