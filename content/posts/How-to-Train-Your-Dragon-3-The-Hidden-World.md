+++

date = "2019-02-07T20:37:25Z"
title = "How to Train Your Dragon 3: The Hidden World"
description = "Brief movie reviews #29"
tags = [ "movie" ]

+++

It’s probably the weakest of the trilogy, but what a lovely trilogy. Growing up
with Hiccup has been a pleasure.

3/5

[Home](/blog "ohthreefive")
