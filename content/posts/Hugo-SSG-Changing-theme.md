+++

description = "Theme changing with Hugo is as easy as described, except when it's not."
date        = "2017-03-15T17:23:50Z"
title       = "Hugo SSG: Changing theme"
tags  = ["meta"]

+++

### MOBILE FIRST

I started this site using the [Lanyon][1] Hugo theme, largely because that's
the theme used by the example [Gitlab Pages Hugo example site][2]. As
suggested, I cloned and edited the example website.

The theme was nice, except on mobile. That's not good enough, unfortunately.

### CHANGING THEME

According to the [documentation][3][(see also here)][4], changing theme in
Hugo is as simple as cloning the new theme to your `/themes` directory and
changing the theme in `config.toml/yaml/json`. What could go wrong?

### THEME SELECTION

There are a very wide number of themes to chose from [here][5], with a
similarly wide variety of `README` files (in terms of quality.) I scanned the
list of themes first to see which had relatively recent commits and then worked
through these to see their mention of how they looked on mobile.

### AFTER DARK

The [After Dark][6] theme looked nice and retro, had a recent commit, a good
`README` and, most importantly, because it was near the start of the alphabet, I
came across it first. I downloaded (rather than cloned) the repo, unzipped it,
moved it to the correct directory, followed the `README`s instructions and
changed my `config.toml` to `theme = "after-dark"`.

### WHAT WENT WRONG

I pushed to my Gitlab repo and watched the test build fail. And fail. And fail
and fail and fail, no matter what little tweaks I made. I started a new Hugo
site locally and watched it build perfectly. To cut a long story short, the
example GitLab Pages Lanyon site puts posts in `/content/posts/` while most Hugo
themes use `/content/post/`. ***SIGH***, just one letter difference.

### WRAP

Anyway, after the appropriate rename of the directory, the new look is up and
running!

[Home](/blog "ohthreefive")

[1]: https://github.com/tummychow/lanyon-hugo "Lanyon for Hugo, on github"

[2]: https://gitlab.com/pages/hugo "GitLab Pages example Hugo site"

[3]: https://www.gohugo.io/themes/installing/ "Installing one (or all) Hugo themes"

[4]: https://www.gohugo.io/themes/usage/ "Using a Hugo theme"

[5]: https://github.com/spf13/hugoThemes "All Hugo themes"

[6]: https://github.com/comfusion/after-dark/ "After Dark theme"
