+++

title       = "I Broke and Fixed My Blog and Still Hate Git"
description = "Why are some things so hard?"
date        = "2024-08-27"
tags        = [ "meta", "commandline", "programming" ]

+++

## Uh oh

So, every so often, something technical fails with my blog. My blog is created
with `Hugo` because I liked the idea of static site generators when I learned
about them and hosted on [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/ "Gitlab Pages documentation") which is nice and neat and means I can update my
blog from a mobile device using only a text editor and a `git` connection. AKA
[Working Copy](https://workingcopy.app/ "Working Copy homepage") on iOS and
iPadOS.

It is usually set up and forget - I've fought with the `config.toml` and
`.gitlab-ci.yml` in the past but not for some time. Until I tried to post
something new last week.

## I try, I really try

When I got the e-mail saying that my pipeline had failed, I knew what to do.
This is not my first rodeo - the wrong set of quotes in my front matter has
tripped me up numerous times. I headed to the repository on Gitlab.com then to
the 'Build' and 'Jobs' section to find the failed job. I've even found that
though it is not intuitive, you can click on the failed job and see the output.

Here's what I saw:

```
ERROR deprecated: config: languages.en.readotherposts: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.dateformatsingle: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.dateformatlist: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.writtenby: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.keywords: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.menumore: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.olderposts: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.readmore: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.minutereadingtime: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.newerposts: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
ERROR deprecated: config: languages.en.subtitle: custom params on the language top level was deprecated in Hugo v0.112.0 and will be removed in Hugo 0.133.0. Put the value below [languages.en.params]. See https://gohugo.io/content-management/multilingual/#changes-in-hugo-01120
```

This sort of output does not put me off - after the first immediate fright of
course! I simply read that there were a variety of `params` that I had set that
were 'deprecated' and it (helpfully) told me what to do (`Put the value below
[languages.en.params]`) and gave me a link to read about the change.

I followed the link, I read the information, re-read it because I didn't have a
clue what it meant, then set about trying to find where these params were.

The way I do this is simple: fire up my preferred editor for this stuff
([Nova for MacOS](https://nova.app "Nova app website")) and searched for the
first error (`languages.en.readotherposts`). Nova helped me quickly identify
that all these `params` are in my config (a more comptent user may have known or
guessed this) and I corrected them (or so I thought) one by one.

## One down, one to go

I saved, committed and pushed the new config back to Gitlab. The Job failed
again, which I thought it would as I hadn't corrected the second error in the
previous Job, but I wanted to see if the error messages for the `params` had
gone - and they had!

So that left:

```
ERROR deprecated: .Site.GoogleAnalytics was deprecated in Hugo v0.120.0 and will be removed in Hugo 0.133.0. Use .Site.Config.Services.GoogleAnalytics.ID instead.
```

Clearly, I just had to follow the same path I had above, which I did. I found
that string in a file within the theme I was using for my blog. Then something
different happened: when I tried to push the commit, it asked for Github login
credentials. I entered mine: nothing. Naturally I repeated this, multiple times,
assuming that was the most sensible course of action.

Then it tickled my brain that it was asking for Github credentials and I was
hosting this blog on Gitlab. So what was going to Github?

...

...

Ah yes, I didn't download my theme for Hugo, I added it as a `git submodule`
presumably because, at the time, some tutorial suggested it and I thought it was
a good idea. As I didn't own the repo, I couldn't exactly go making changes to
it. But wait, I thought, could this be my first ever pull request!

## Fork you

Alas, no. The author of the theme had retired it and left a very nice message
saying it's open to anyone to fork. I did some extensive Stack Overflowing and
thought I could handle the fork. From inside the directory in which I store
my theme, I ran: `gh repo fork panr/hugo-theme-hello-friend --fork-name "hello-friend-mh"` (yeah that's right I even tagged on a rename - PRO).

Fork complete.

## Submodules, where it all gets incredibly frustrating

I had my new theme, I just wanted to purge the `hello-friend` submodule in
favour of my originally named fork `hello-friend-mh`. Stack Overflow suggested
some editing of my `.gitmodules` file, which I was able to do based on the
information in that file - it seemed to make sense what to do.

I was also told to delete the directories from both the `modules` and `themes`
directory of my Hugo repo, which I did. Then it was simply a case of
`git submodule sync` then `git submodule update`. With this done, I tried a
commit & push - Job failed.

More Stack Overflow: `git submodule sync --recursive`,
`git submodule deinit -f -- themes/hello-friend` - no output, no build joy.

On Gitlab.com, the Job output contained this line:
`fatal: No url found for submodule path 'themes/hello-friend' in .gitmodules`
which was interesting because nothing in `.gitmodules` in my local directory
contained any reference to the old theme anymore.

Back to Stack Overflow, and I came across something else to try:
`git rm --cached /themes/hello-friend`. This gave my my first error in some time
which read `fatal: Invalid path '/themes': No such file or directory` which was
interesting. I ran a `git status` which informed me:

```
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
		deleted:    themes/hello-friend
```

This was interesting and useful - as far as my brain interprets that, I haven't
committed the deleted old theme from `.git`. Something told my to look at
Gitlab's `.gitmodules` directory. Sure enough, it still showed hello-friend, in
contrast to my local repo. I somehow had to update the remote.

Next I decided that I should have removed the `modules` directory within `.git`
rather than just the theme itself (`rm -rf .git/modules`). Please don't judge my
use of `rm -rf` without requisite knowledge. At least I'm not `sudo`ing it.

I don't know exactly why, but I decided to re-run `git submodule sync` and this
time I actually got output!
`Synchronizing submodule url for 'themes/hello-friend-mh'`

I was getting excited, and when I'm excited, I calm myself with a `git status`:

```
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
		deleted:    themes/hello-friend
```

No way - something I had done had done something! (If you haven't realised by
now that this is not a tutorial and that if I had to do this again today, one
day later, I would probably mess it up again, then let me confirm both of those
as facts.)

I committed and pushed.

## Kill me now

It still didn't work. In a moment of calm before massively losing my shit, I
used Nova to search for 'hello-friend' in my local directory. Unsurprisingly,
there were numerous references to it in panr's theme. I went through them one by
one and changed any which seemed like they might be important.

And it worked! My site built!

## Why is this so hard?

I am an enthusiastic amateur user of computers who regularly tells people: you
are not stupid, do not let your difficulty using a computer convince you that
you are stupid, it is the computer that is stupid and can only interpret
instructions given in the appropriate way. And this tale of woe then redemption
supports all of that, but also highlights how hard this sort of thing can be
even for someone who tries really hard to engage with real computing. Should I
just have a Squarespace site instead? Where's the fun in that?

## Never surrender!

Obviously I will persist with this setup and this blood pressure increasing
method of learning. After all, it's all sorted now.

Time to run a `hugo new post` command... what's that Mr Terminal:

`WARN 2024/08/27 13:05:43 Module "hello-friend-mh" is not compatible with this Hugo version; run "hugo mod graph" for more information.`

OH GO FORK YOURSELF!

(fyi it still created the post. I can solve this problem another day.)

[Home](/blog "ohthreefive")
