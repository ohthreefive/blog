+++

title       = "Imaging in colorectal cancer"
description = "Summary information for SSC students"
date        = "2017-02-03"
tags  = [ "radiology" ]

+++

### Radiology in colorectal cancer: summarised

---

#### **AT PRESENTATION:**

##### **In a symptomatic patient**

* **GP REFERRAL**

    * AXR, CXR ("abdo pain, obstruction? perforation?")

* **A&E PRESENTATION**

    * As above, may also go directly for CT abdomen & pelvis

##### **In a screened patient**

* **COLONOSCOPY**

* **CT COLONOGRAPHY**

    * [SIGN][1] calls it a, "Safe and sensitive" alternative to colonoscopy
    * [NICE][2] says use it if colonoscopy cannot be done or is done but is
      incomplete

---

#### **FOR DIAGNOSIS:**

* **COLONOSCOPY**

    * Tissue is needed
    * Emergency presentation may go straight to theatre and be diagnosed there

* **CT ABDOMEN & PELVIS**

    * Most patients (ie not screened) will have CT before colonoscopy or
      operation
    * For those unfit to proceed to colonoscopy or operation diagnosis may be
      'radiological'

---

#### **FOR STAGING:**

* **CT CHEST, ABDOMEN & PELVIS** ([SIGN][1], [NICE][2])

    * The 'workhorse'

* **MRI LIVER, US LIVER**

    * To characterise uncertain liver lesions on CT ([SIGN][1])

* **MRI RECTUM**

    * If tumour is rectal ([SIGN][1], [NICE][2])
    * (Endoluminal US can have a complementary role according to [SIGN][1])

* ***PET-CT WILL BE DISCUSSED BELOW***

---

#### **FOR TREATMENT:**

##### **Of bowel obstruction**

* **COLONIC STENT**

    * Routine role in treatment of obstruction prior to operation is not
      established
    * Definitely indicated for palliative treatment of bowel obstruction
      ([SIGN][1])
    * Indicated if obstructed patient not immediately fit for surgery
      ([SIGN][1])
    * Not indicated for right-sided obstruction ([NICE][2])
    * NB definite risk of perforation during/after insertion which would
      increase complexity of any subsequent operation

##### **Of metastases**

* **ABLATION** (radio-frequency or micro-wave, cryo-ablation not used in liver)

    * Indicated for liver metastases if there are no extra-hepatic metastases
      and resection is not an option ([SIGN][1])
    * Ablation of lung metastases is also possible but does not appear in
      [SIGN][1] or [NICE][2] guidelines

* **PET-CT**

    * Comes together with ablation (and 'metastasectomy') as all patients being
      considered for such should have PET-CT to check for extra-hepatic
      metastases ([SIGN][1], [NICE][2])
    * [NICE][2] also says PET-CT is indicated when there are extra-hepatic
      metastases that are being considered for resection (NB other than lung, I
      don't think I've ever heard of resection for extra-hepatic metastases)

---

#### **FOR FOLLOW-UP:**

* **CT CHEST, ABDOMEN & PELVIS** ([SIGN][1], [NICE][2])

    * [NICE][2] recommends at least two in the first three years of follow-up

* **MRI RECTUM**

    * For rectal tumours ([SIGN][1], [NICE][2])

* **PET-CT**

    * 'Normal' or equivocal CT but rising CEA ([SIGN][1])
    * Also good for pelvic recurrence (not first line) or pre-sacral mass
      ([SIGN][1])

[1]:  http://sign.ac.uk/guidelines/fulltext/126/index.html "SIGN CRC guideline"

[2]: https://www.nice.org.uk/guidance/cg131 "NICE CRC guideline"

[Home](/blog "ohthreefive")
