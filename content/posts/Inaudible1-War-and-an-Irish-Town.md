+++
date = "2021-05-14T10:00:21.773Z"
title = "War and an Irish Town"
description = "The first audiobook I haven’t been able to finish; eventually listen #102"
tags = [ "audible" ]

+++

I really want to learn about 'The Troubles' (talk about an understated title) in
Northern Ireland, and I've got a lot out of non-fiction audiobooks in the past,
but despite persevering for about half of this book, I wasn't getting enough out
of it.

I must retry, or maybe buy the dead tree version.

### The re-read (31st March, 2022)

I made good on my promise to re-visit this book and got through it all. Maybe it’s
McCann’s narration, maybe it’s his style, but the endless history that made up the
second half of the book was painful to listen to. Not in the sense it should have
been. Tales of discrimination and poor decisions were being told, but I couldn’t
keep focused enough to listen to them. A real disappointment.

[Home](/blog "ohthreefive")