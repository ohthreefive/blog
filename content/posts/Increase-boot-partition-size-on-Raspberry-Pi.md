+++

tags = ["commandline", "linux", "raspberrypi"]
date = "2017-05-22T21:02:26+01:00"
description = "I wanted to upgrade my Ubuntu Mate Raspberry Pi install. There was a problem with the /boot partition"
title = "Increase boot partition size on Raspberry Pi"

+++

---

***PSA*** if you're only interested in how to do this, skip the following
pre-amble! Dive back in at the 'Adafruit' header.

---

I run Ubuntu Mate on a Raspberry Pi and enjoy it very much! For a frankly
not-good-enough reason, I decided to upgrade from 16.04. I should just stick to
LTS, but hey!

Following some instructions on the [Mate website][1], I enabled notifications
for all distro upgrades in the Software & Updates GUI. Then I dropped in to the
terminal for some

    sudo apt update && sudo apt dist-upgrade

action.

### HITCH-Y McHITCHFACE

Nothing happened. I um-ed and ah-ed a little and found no useful information by
reading the Mate website or a (probably too brief) online search.

I then ran the GUI Software Update and, lo! The dialogue offered me to upgrade
to 16.10. Now, I'm not sure if you can just go from 16.04 to 17.04 (which is
what I wanted to do) but I decided just to do 16.10 now, hopefully doing 17.04
shortly after!

### HITCH-Y McHITCHFACE: THE REVENGE

Having downloaded the necessary files, the GUI then calculated the upgrade and
declared my boot partition to be too small by around 2 measly megabytes!

Helpfully, it suggested emptying the trash (already empty) and running

    sudo apt clean

but unfortunately no space was recovered.

Naturally, I turned again to a search. Initially duckduckgo but I didn't have any luck
so enter Google and, after some false steps (increasing /root rather than /boot is
pretty well documented out there!) I found a useful Adafruit article.

### ADAFRUIT TO THE RESCUE

What comes below is adapted from [this Adafruit article][2]. Full credit to them.

Required:

1.  microSD to SD adapter
2.  Another computer
3.  [The Gnome Partition editor][3]
4.  A terminal emulator (not required *per se*)

Steps:

* Insert the microSD+adapter into the other computer.
* Locate the microSD and note the boot partition's location (e.g. /dev/sdb1) and file format (fat16 in my case) using GParted
* Copy the contents of the /boot partition to somewhere safe,

e.g.

    sudo cp -r /dev/sdb1 ~/Desktop/

(The next instructions all use GParted)

* Unmount both the /boot and /root partitions
* **Delete** the /boot partition
* Create a new partition with the same file system as previous (e.g. fat16) and importantly a bigger size (I opted for 1024 Mb)
* Mount the new partition (mine mounted to /mnt/sdb1)

NB both the 'delete' and 'create' tasks above only enqueue the task in GParted; you have to hit the green tick for the task to be carried out.

Finally, copy the files back to the new, bigger /boot partition:

    sudo cp -r ~/Desktop/sdb1 /mnt/sdb1

Double check everything looks as it should. The above command probably nests a sdb1 directory within a sdb1 directory but that's easily solved!

I've used this method twice successfully in the last two days.

### MAYBE DON'T UPGRADE

So after achieving this, my upgrade to 16.10 lost me access to the Pi 3's internal wifi and bluetooth! Disaster. But at least I learnt something. I'm clean installing 16.04 to fix things.

In future, I'll stick to LTS releases and security upgrades!

[Home](/blog "ohthreefive")

[1]: https://ubuntu-mate.org/raspberry-pi/ "Thanks, Wimpy!"

[2]: https://learn.adafruit.com/resizing-raspberry-pi-boot-partition/edit-partitions "great wee tutorial"

[3]: http://gparted.org/ "GParted ftw!"
