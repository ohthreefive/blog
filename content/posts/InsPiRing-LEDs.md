+++

date = "2017-07-06T21:33:22+01:00"
description = "Alex Eames' Kickstarter has delivered, and I've made my own code for his circle/clock"
tags = ["raspberrypi", "programming"]
title = "InsPiRing LEDs"

+++

Alex Eames has done several Raspberry Pi based Kickstarter campaigns and he's
delivered promptly on every one I've backed.

His most recent was for some lovely [LEDs][1] and these delivered two months
early.

### NTP CLOCK

I backed his circular board with the plan to make it in to a clock for my
daughter. The code on his [Github repository][2] has an example clock, but it
mimics an analogue clock and my daughter can't read a clock yet... but she can
count.

### MY CLOCK

At my wife's suggestion, I planned to make the LED ring display the hour as the
number of LEDs lit. Alex already had a nifty way of making 24 LEDs represent
60 seconds so I've also got a minute marker.

I've achieved this and put the code on Gitlab [here][3]. I need to add comments
to it and also do something to display minutes. Hopefully I'll get around to
doing this soon!

### NIGHT LIGHT

As my girl is only meant to get up at seven and goes to bed at seven, there's no
point in it displaying the time overnight. So I've added a little function that
checks whether it's before 7am or after 7pm and if so, it switches all the LEDs
on at minimum brightness with a warm yellow light. Actually, it's a bit green
just now - I'll need to work on that!

---

Thanks for the hardware, Alex!

[Home](/blog "ohthreefive")

[1]: https://www.kickstarter.com/projects/raspitv/raspio-inspiring-connect-rgb-led-shapes-sculpt-you 
[2]: https://github.com/raspitv/raspio-inspiring
[3]: https://gitlab.com/ohthreefive/InsPiRing_scripts/blob/master/clock_test.py
