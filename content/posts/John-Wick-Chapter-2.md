+++

date = "2017-09-21T08:54:08+01:00"
title = "John Wick Chapter 2"
description = "Brief movie reviews #9"
tags = ["movie"]

+++

I came to John Wick a little late, after hearing good things. I found it very
enjoyable with beautifully choreographed action. Though the bad dudes missed a
number of chances to JUST SHOOT HIM IN THE FACE.

The sequel to a cult or word of mouth hit is a risk. You want to please the
audience who *discovered* the awesomeness first time around but if you play it
up too much then it's in your face and too forced.

JWC2 swings just too far to the try hard side of things. In the first, a cool
shadowy assassin underworld was hinted at, tasted. Here, it's the setting. In
the first, you could hurt John but he'd keep on coming, even with a limp. Here,
he is barely slowed down by being hit by a car and later he's actually bullet
proof.

The outstanding action choreography is in full view again, however, which makes
up for many sins.

3/5

[Home](/blog "ohthreefive")
