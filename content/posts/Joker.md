+++
date = "2019-10-27T08:11:15.183Z"
title = "Joker"
description = "Brief movie reviews #36"
tags = [ "movie" ]

+++

When I walked out of the cinema, I knew I'd seen something singular and
compelling, I'm just not sure if I *liked* it. Phoenix was magnetic, however,
and deserves recognition.

3/5

**SPOILERY**

A few things really detract from it objectively:

- The one seen of Arthur in his neighbour's garden was enough to confirm their
  relationship was in his head; the subsequent flashback were both lazy and, to
  an extent, cowardly - Phillips didn't trust, or respect, the audience enough
  to have figured this out
- Including the death of the Wayne's was unnecessary. It brought Batman into a
  movie which didn't need him.
- The final scene in the mental health hospital was also unnecessary. Was
  Phillips implying it was all in Arthur's head? If so: lazy rug pull. If
  pretending to be a rug pull but deliberately ambiguous: trying to be too
  clever by half. Did it add anything? No. I also thought Phoenix's face looked
  considerably fuller. Unplanned reshoot? Deliberately signposting it was in an
  alternate time, or reality? Regardless, I hated it.

[Home](/blog "ohthreefive")