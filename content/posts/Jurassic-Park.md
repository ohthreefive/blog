+++
date = "2023-09-24T12:11:38"
title = "Jurassic Park"
description = "Brief movie reviews #80"
tags = ["movie"]

+++

A film made for a dinosaur obsessed 9 year old returns to the screen so that I can take my 8 and 10 year old to see it. Utterly wonderful. A testament to cinema and Spielberg. His film-making style is so lean - I noticed this too with recent Raiders rewatches. No superfluous backstories. No deep dives into mythology. Just forward movement. One brief example: Lex and Tim have an entirely believable sibling bicker about her liking computers in the middle of the movie. Come the climax, her computer skills come into play. That’s it. No constant references. No Palm Pilot in her pocket. Just one throwaway and genuine back and forth that pays off later in the movie. Simplicitly itself.

5/5

[Home](/blog "ohthreefive")