+++

date = "2018-03-05T20:14:17Z"
description = "Quickly get the Kill Sticky bookmarklet on iOS"
tags = ["ios"]
title = "Kill Sticky"

+++

### BACKGROUND

Sticky elements floating up and down web pages eat up screen real estate.

Read [this take][1] for more info. Gruber's post links to [this post][2].

A simple Javascript bookmarklet to nuke the sticky header? Yes, please.

### IOS NOT SO SIMPLE

Except that installing a bookmarklet on a desktop browser is as easy as drag 'n'
drop. On a mobile browser, specifically mobile Safari, this does not work.

Neither a long press nor 3D touch on the bookmarklet gives you anything other
than a useless `Open` option.

So if you use iOS Safari and have no access to macOS Safari to sync bookmarks,
another method is required.

### WORKAROUND

The Javascript for the bookmarklet is contained in the link above and as a
linked GitHub Gist. I've reproduced it here:

{{< highlight javascript >}}
(function () { 
  var i, elements = document.querySelectorAll('body *');

  for (i = 0; i < elements.length; i++) {
    if (getComputedStyle(elements[i]).position === 'fixed') {
      elements[i].parentNode.removeChild(elements[i]);
    }
  }
})();
{{< /highlight >}}

1.  Copy the above Javascript
2.  Bookmark any page via the iOS Share icon
3.  Access Bookmarks via the Bookmark icon
4.  Find the just-created placeholder Bookmark
5.  Press 'Edit' (bottom right of screen on iPhone X)
6.  Select to edit the placeholder bookmark
7.  Rename (to 'Kill Sticky' if you wish)
8.  In 'Address', type 'javascript:' then paste the Javascript above

### JAVASCRIPT GIVETH, JAVASCRIPT TAKETH AWAY

That should be it done. Now, if you're on a site with a floating header and/or
footer (try Wired or the Verge,) open your bookmarks and hit 'Kill Sticky.'

Dickbar be gone!

[Home](/blog "ohthreefive")

[1]: https://daringfireball.net/linked/2017/06/27/mcdiarmid-sticky-headers "Daring Fireball hates sticky headers"
[2]: https://alisdair.mcdiarmid.org/kill-sticky-headers/ "Alisdair McDiarmid kills sticky headers"
