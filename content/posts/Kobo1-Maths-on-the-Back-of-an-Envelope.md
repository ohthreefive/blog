+++
date = "2020-10-06T13:24:36.296Z"
title = "Maths on the Back of an Envelope: Clever ways to (roughly) calculate anything"
description = "Kobo e-reader reads #1"
tags = [ "Kobo" ]

+++

Physical media!

Well, actually, no - a physical device (an old Kobo e-reader that has too long
gone unused) but digital books!

(I have actually recently read actual paper - my daughter having started the
Harry Potter series)

This has been on my to-read list but I could not get it on Audible and I was
concerned that listening would not be the ideal medium for the topic.

A really quick and helpful way to look at rough (and accurate!) calculations.
I've already used 'zequals' quite effectively in real life!

4/5

[Home](/blog "ohthreefive")