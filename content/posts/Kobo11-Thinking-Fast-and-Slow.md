+++
date = "2022-02-21T14:00:09.598Z"
title = "Thinking: Fast and Slow"
description = "Kobo e-reader reads #11"
tags = [ "Kobo" ]

+++

After a fascinating a simple start (system 1 vs system 2), this classic gets
bogged down in dry repetitiveness. I wish I had listened to the
[Cortex book club][1] review prior to reading! 4 months of effort. Alas, the
reproducibility crisis.

2/5

[Home](/blog "ohthreefive")

[1]: https://www.relay.fm/cortex/119 "link to Cortex podcast book club episode about Thinking: Fast and Slow"