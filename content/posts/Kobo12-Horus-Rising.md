+++

title       = "Horus Rising"
description = "Kobo e-reader reads #12"
date        = "2023-05-02"
tags        = [ "Kobo" ]

+++

Really indulging the guilty pleasures here!

I was a *big* 40K fan growing up and will always remain a hard sci-fi and
fantasy apologist. I just couldn't resist peeling back the cover to peak at the
now epic Horus Heresy book series.

**Sigh**. I demolished it and have downloaded book 2.

4/5

[Home](/blog "ohthreefive")
