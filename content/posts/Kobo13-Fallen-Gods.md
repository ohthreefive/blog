+++
date = "2023-05-07T13:27:20.268Z"
title = "Fallen Gods"
description = "Kobo e-reader reads #13"
tags = [ "kobo" ]

+++

Not as good as the first, but I still tore through it.

Despite the writer's afterword, I still think a lot of Horus' agency was taken
out of his fall - he got stabbed with what seems to be a Chaos blade, nearly
dies and gets a tour of a possible future by an agent of Chaos. Once healed, his
personality is forever changed. I thought his downfall would be over about a
dozen books in a series this size, with an ever-increasing list of bad decisions
mounting until he's undoubtedly against the Emperor. Oh well. On to the next...

[Home](/blog "ohthreefive")