+++
date = "2023-08-02T09:19:04+02:00"
title = "Human Error"
description = "Kobo e-reader reads #14"
tags = ["Kobo"]

+++

Well, there’s a reason it’s been nearly three months since my last Kobo read. Tim Harford, you have a lot to answer for! ‘Human Error’ may be a benchmark in exploration of, well, human error, but it’s dry as a stick. This was an absolute slog. I shouldn’t have pushed to the end.

2/5

[Home](/blog "ohthreefive")