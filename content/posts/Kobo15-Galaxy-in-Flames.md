+++
date = "2023-08-02T14:42:38+02:00"
title = "Galaxy in Flames"
description = "Kobo e-reader reads #15"
tags = ["Kobo"]

+++

The end of the initial trilogy in the Horus Heresy series was a full scale tragedy. There were many heroes that I didn’t want to say goodbye to. I absolutely blitzed through this and have already purchased the next one with its slightly spoiler-y title. Great series. Appeals to my core!

4/5

[Home](/blog "ohthreefive")