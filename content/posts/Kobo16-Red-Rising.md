+++
date = "2023-08-15T11:17:28"
title = "Red Rising"
description = "Kobo e-reader reads #16"
tags = ["Kobo"]

+++

A recommendation from a friend, this was a real page-turner. Nothing particularly new: heavy YA vibes and a Hunger Games - Battle Royale mix. The laziest character was Pax. I really enjoyed it, but it wasn’t anything ground-breaking.

3/5

[Home](/blog "ohthreefive")