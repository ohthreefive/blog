+++
date = "2023-08-15T11:21:07"
title = "The Flight of the Eisenstein"
description = "Kobo e-reader reads #17"
tags = ["Kobo"]

+++

Book 4 of the Heresy breaks away from the original trilogy and follows one Death Guard and his mission away from Isstvan III. This was the first novel where interplanetary travel seemed trivial, though I guess when you have a spacefaring Legion as shepherds it probably is. Also, given the first 3 were a trilogy, it felt odd that there wasn’t a natural ‘next step’ in the narrative by the end of the book.

3/5

[Home](/blog "ohthreefive")