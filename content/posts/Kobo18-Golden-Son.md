+++

title       = "Golden Son"
description = "Kobo e-reader reads #18"
date        = "2023-08-26"
tags        = [ "Kobo" ]

+++

The second in the Red Rising series immediately reminded me of what I didn't
like about in book one - mass casualties in what amounts to a university wargame
- I just can't buy that as being acceptable in a society.

In book one I was disappointed by the lazy character, Carlo Guercio. Oh wait, I
mean Pax. I was further disappointed to learn that the author sealed Pax's fate
by drawing lots. This was taken one further in Golden Son when Pax was
essentially replaced by Ragnar. Not an identical character, but similar enough
to raise an eyebrow.

HOWEVER, the narrative remains a page-turner and I will keep reading the series.
Just with plenty of eye-rolling.

3/5

[Home](/blog "ohthreefive")
