+++
date = "2023-09-18T07:27:06"
title = "Fulgrim"
description = "Kobo e-reader reads #19"
tags = ["Kobo"]

+++

Horus Heresy Book 5! Fulgrim’s descent is nuanced but again, like Horus, Chaos just tempts him to make bad decisions. Also like Horus and the Anatheme, a weapon is involved. By the end of the novel, we have lost two Primarchs - one very dead, one supplanted by a daemon. The interweaving narratives mean that the overall story moves forward just a little each novel, but so far I’m happy to be patient.

4/5

[Home](/blog "ohthreefive")