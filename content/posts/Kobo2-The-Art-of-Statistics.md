+++
date = "2020-11-13T14:32:02.354Z"
title = "The Art of Statistics: Learning from Data"
description = "Kobo e-reader reads #2"
tags = [ "kobo" ]

+++

An outstanding concise book - but probably only for someone already interested
in the subject!

5/5

[Home](/blog "ohthreefive")