+++
date = "2023-10-12T12:36:11"
title = "Morning Star"
description = "Kobo e-reader reads #20"
tags = ["Kobo"]

+++

The writing remains brutally simplistic, probably part of the reason I’m able to read these so easily, and some of the plotting is ridiculous, but I’m still hooked.

The Pax-gnar ‘event’, much like the original, is cheap emotion and there’s a bit of bait and switch at the end (which I saw coming to an extent - what was written just didn’t feel right) but there’s been enough in this trilogy to pull me back into Iron Gold. Although I see it’s a third sequel in a row that’s longer than all before it. Hmmm.

3/5

[Home](/blog "ohthreefive")