+++
date = "2023-11-20T20:47:01Z"
title = "Descent of Angels"
description = "Kobo e-reader reads #21"
tags = ["Kobo"]

+++

The first novel that is a direct continuation of the Heresy story suffers a little bit for that reason. It also gives a disappointing hint of how the Empereror’s character might be handled. However, I still burned through it.

3/5

[Home](/blog "ohthreefive")