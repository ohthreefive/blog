+++
date = "2023-12-13T21:38:04Z"
title = "Iron Gold"
description = "Kobo e-reader reads #22"
tags = ["Kobo"]

+++

Book 4 of Red Rising and Pierce Brown goes all GRRM in structure. It’s a mixed bag - I don’t feel too invested in Lyria or Ephraim and I’d love some Sevro POV.

Plots have become a bit predictable (I was able to guess the Ash Lord & Atalantia pay offs), some developments are poorly handled (an off-screen death is such a strange decision that I still don’t know if I believe it) and some things are not earned (stop trying to make Wolfgar happen!)

I’m just such a sucker for the simple plot though. So I’ll persevere.

3/5

[Home](/blog "ohthreefive")