+++
date = "2024-02-20T18:29:12Z"
title = "Legion"
description = "Kobo e-reader reads #23"
tags = ["Kobo"]

+++

The latest Heresy novel touches on a Legion I knew nothing about prior to reading it. It differs somewhat from the preceding novels with a lack of battles and in truth paucity of Astartes. But it offers an interesting reason for a Legion to join Horus. Nice and short too.

4/5

[Home](/blog "ohthreefive")