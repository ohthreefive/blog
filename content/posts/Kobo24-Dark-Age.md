+++
date = "2024-03-23T12:09:19Z"
title = "Dark Age"
description = "Kobo e-reader reads #24"
tags = ["Kobo"]

+++

I can’t believe I got through this in a month! It’s … quite something.

Brown’s casual disregard for life is on show again. We hardly knew ye, Ulysses.

His ‘stop trying to make fetch happen’ habit of introducing something and telling us
how important they are before killing/destroying them is back. Pax was earned,
Wulfgar was not. Hello Pandora, Orion, welcome to the club.

One major step up here was portraying Darrow from the point of view of his enemies
as being utterly terrifying worked really well. Often we’ve heard about Darrow’s
terrifying enemies and how brutal they are. Well, in Dark Age, we see that Darrow,
our ‘hero’, scares the crap out of his enemies as an unstoppable killing machine.

Not sure I like Lysander’s arc. And I saw the off-screen-death-so-it-doesn’t-count
a mile away. Actually, that’s two this week, after Masters of the Air did the same
with Austin Butler.

Oh my, I wonder where this will go next!

3/5

[Home](/blog "ohthreefive")