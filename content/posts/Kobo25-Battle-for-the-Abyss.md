+++
date = "2024-04-11T21:03:32"
title = "Battle for the Abyss"
description = "Kobo e-reader reads #25"
tags = ["Kobo"]

+++

My beloved Ultramarines make their Heresy debut in what is ultimately a disappointing novel.

*ONE* World Eater, **ONE** Thousand Son, ***ONE*** Space Wolf - it’s all a bit generic.

Oh well, they can’t all be bangers!

2/5

[Home](/blog "ohthreefive")