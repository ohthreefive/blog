+++
date = "2024-05-04T09:33:03"
title = "Lightbringer"
description = "Kobo e-reader reads #26"
tags = ["Kobo"]

+++

Definitely a return to form as far as the Red Rising series goes. An interesting arc for Darrow, although his early escape from death was a bit too convenient. Sevro had a nice arc. I almost like Lyria now. I never got on board with Atlas. Lysander is now irredeemable (and a worse character for his *bad* decision coming after his very *good* decision with Diomedes. I had thought we might head towards a Darrow, Diomedes & Lysander alliance against Atalantia and towards a new rule. Darrow representing the new, Lysander the old and Diomedes the Rim. But now, no. Oh, and that character who definitely wasn’t dead makes an excellent return before being cast aside. Sorry, but that’s my least favourite development. Lazy use of the reader’s affections to give this event more emotional impact. It’s like Brown just wanted a major death at the end.

Oh, and before the last book, just let me say this now: **hands off Victra**. She sees this out. Got it?

[Home](/blog "ohthreefive")