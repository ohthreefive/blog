+++
date = "2024-05-19T19:36:43"
title = "Mechanicum"
description = "Kobo e-reader reads #27"
tags = ["Kobo"]

+++

Let me be honest, I have never cared much about Mars in the 40k universe. But I would like to know the definitive story of the Dragon of Mars.

In the end, I enjoyed some of Dalia and Koriel Zeth’s tales, but was less interested in the Dark Mechanicum side of the story.

2/5

[Home](/blog "ohthreefive")