+++
date = "2024-05-19T19:41:30"
title = "When We Cease To Understand the World"
description = "Kobo e-reader reads #28"
tags = ["Kobo"]

+++

Wow this one really hit me for six. I didn’t know what was going on or where it was going but I couldn’t put it down. Part of me was fighting to work out what was fiction and what was non-fiction, the other part of me didn’t care when it was all so engrossing.

5/5

[Home](/blog "ohthreefive")