+++
date = "2024-06-04T18:39:14"
title = "Tales of Heresy"
description = "Kobo e-reader reads #29"
tags = ["Kobo"]

+++

The latest in the Horus Heresy series is the first anthology. I enjoyed some more than others. Meeting the Custodes properly was fun and spending a good amount of time with the Emperor, finally, was satisfying. It really allowed some of his perspective and his priorities to come through. Not sure I agree with how he likes to go about things though!

3/5

[Home](/blog "ohthreefive")