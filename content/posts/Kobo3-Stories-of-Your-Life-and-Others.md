+++
         
date = "2021-01-01T08:15:02.279Z"
title = "Stories of Your Life and Others"
description = "Kobo e-reader reads #3"
tags = [ "kobo" ]

+++

The second collection of Chiang short stories that I've read (after [Exhalation][1]) is a similar affair with fascinating ideas but only the occasional truly _fully_ satisfying narrative.

4/5

[Home](/blog "ohthreefive")

[1]: https://ohthreefive.gitlab.io/post/audible70-exhalation/ "my Exhalation review"