+++
date = "2024-07-20T08:42:59"
title = "The Maniac"
description = "Kobo e-reader reads #30"
tags = ["Kobo"]

+++

Well, lightning didn’t strike twice. Benjamin Labatut’s, “When We Cease to Understand the World,” hit me like a, well, lightning bolt. This was tougher to dig into and, after the autobiography I have previously read, is the second book about John von Neumann that has failed to capture me the way I feel his story should have.

By the end, I was strongly invested and as for the extended Coda about Go, well, despite that being a slight left turn, it was captivating.

3/5

[Home](/blog "ohthreefive")