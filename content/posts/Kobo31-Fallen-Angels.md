+++

title       = "Fallen Angels"
description = "Kobo e-reader reads #3"
date        = "2024-08-11"
tags        = [ "Kobo" ]

+++

A nice companion piece to Descent of Angels, though the, "Something festers at the heart of this world," narrative is beginning to feel repetitive.

3/5

[Home](/blog "ohthreefive")
