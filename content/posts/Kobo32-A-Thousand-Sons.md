+++

title       = "A Thousand Sons"
description = "Kobo e-reader reads #32"
date        = "2024-08-11"
tags        = [ "Kobo" ]

+++

Yup, I am officially sympathetic to the Thousand Sons and fond of Ahriman after this. Excellent tale. Such hubris - and that applies to the Emperor as much as Magnus.

4/5

[Home](/blog "ohthreefive")
