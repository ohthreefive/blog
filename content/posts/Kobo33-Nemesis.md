+++

title       = "Nemesis"
description = "Kobo e-reader reads #33"
date        = "2024-11-24"
tags        = [ "Kobo" ]

+++

An entire novel about the Officio Assassinorum? Yup, I'm here for that. I
felt the Culexus was poorly served but otherwise this was a fun read,
especially introducing the concept of a tangible divide between Erebus and
Horus' aims, methods and allegiances.

[Home](/blog "ohthreefive")
