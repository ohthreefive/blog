+++

title       = "The Elegant Universe: Superstrings, Hidden Dimensions, and the Quest for the Ultimate Theory"
description = "Kobo e-reader reads #34"
date        = "2024-11-30"
tags        = [ "Kobo" ]

+++

The first third of this book was within my grasp and I loved exploring physics again.

Once things went Planck-sized, I really struggled. Eventually I just started skimming, trying to grab the gist and the concepts only and to *not* try to understand what was being written.

Hard, but achievable.

3/5

[Home](/blog "ohthreefive")
