+++
         
date = "2021-01-11T17:00:56.570Z"
title = "How To Take Smart Notes"
description = "Kobo e-reader reads #4"
tags = [ "Kobo" ]

+++

A fascinating read and a workflow I wish I'd read about long ago. It's time
to stop writing my summaries and start writing smart notes.

4/5

[Home](/blog "ohthreefive")