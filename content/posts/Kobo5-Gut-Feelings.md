+++
date = "2021-03-07T20:09:16.328Z"
title = "Gut Feelings"
description = "Kobo e-reader reads #5"
tags = [ "Kobo" ]

+++

I heard of Gert Gigerenzer a long time ago and was delighted to finally get the
chance to read some of his work. This is a fascinating dive into the reasons why
we shouldn't dismiss our initial reactions to a question, situation or similar.

4/5

[Home](/blog "ohthreefive")