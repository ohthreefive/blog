+++

title = "Maths for Mums and Dads"
date = 2021-03-30
tags = [ "kobo" ]
description = "Kobo e-reader reads #6"

+++

A good read to help understand why maths is taught differently these days. I am good at maths, but I am not a primary school teacher! This will help me shout at my children less!

3/5

[Home](/blog "ohthreefive")