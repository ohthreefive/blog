+++
date = "2021-08-26T09:02:11.483Z"
title = "The Code Breaker"
description = "Kobo e-reader reads #8"
tags = [ "kobo" ]

+++

A fascinating re-telling of the race to CRISPR discoveries and the role or
CRISPR and RNA in the ongoing coronavirus pandemic. In between is a discussion
about the morals around genetic alterations, which I found the least satisfying
section of the book.

4/5

[Home](/blog "ohthreefive")