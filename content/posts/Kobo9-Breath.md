+++
date = "2021-09-11T06:50:20.398Z"
title = "Breath: The New Science of a Lost Art"
description = "Kobo e-reader reads #9"
tags = [ "Kobo" ]

+++

Fascinating book, though it remains difficult to separate science from pseudo-
science in this field.

4/5

[Home](/blog "ohthreefive")