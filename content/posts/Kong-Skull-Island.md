+++

date = "2017-07-27T21:47:00Z"
title = "Kong Skull Island"
description = "Brief movie reviews #6"
tags = [ "movie" ]

+++

I was feeling significantly blockbuster-ed out so, with Guardians vol. 2 not yet being available, I opted for this creature feature on iTunes.

It's certainly not charmless, and the opening battle between Kong and the helicopters is great, but it's otherwise rather forgettable.

Shorter than Peter Jackson's take, though!

2/5 

[Home](/blog "ohthreefive")
