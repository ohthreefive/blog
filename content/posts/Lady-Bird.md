+++
         
date = "2020-02-23T09:52:56.040Z"
title = "Lady Bird"
description = "Brief movie reviews #43"
tags = [ "movie" ]

+++

My first Gerwig experience and this was a top-tier coming of age tale. Lady Bird is *such* an arsehole; it takes some pretty skilled writing to feel any empathy for her.

4/5

[Home](/blog "ohthreefive")