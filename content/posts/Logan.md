+++

title = "Logan"
date = "2017-03-30T14:29:50+01:00"
description = "Brief movie reviews #3"
tags = [ "movie" ]

+++

- Yes! X-Men
- Yes! X2
- Yes! X-Men: The Last Stand
- Yes! X-Men Origins: Wolverine
- No ! X-Men: First Class
- No ! The Wolverine
- No ! X-Men: Days of Future Past
- No ! X-Men: Apocalypse
- Yes! Logan

The above list are the X-Men movies I've seen in the cinema. Seems it had been
eight years since one of these tempted me out of my house. (I have seen all of
the others at home.)

**Worth it.**

Logan achieves grittiness and realism without being dour and miserable. Or
rather, it doesn't make the mistake of removing all humour in the hopes of
appearing more worthy. The violence is appropriate - death by Wolverine is
always going to be brutal - but violence is also at the heart of two very light
hearted moments. Logan's aimless beating of his broken-down truck evokes John
Cleese in Fawlty Towers.

Laura dishes out as much carnage as Logan. I don't think I've seen a child
actor since Chloe Grace Moretz as Hit Girl who genuinely looked capable of doing
the things her character does on screen. She is ferocious and feral; it's an
outstanding physical performance. But she also gets her moment of levity:
punching Logan in the face because she can't get through to him is another
smile-inducing moment of violence.

The fine line between spectacular and ridiculous is also deftly traversed. While
the introduction of X24 dragged the film down a little, I was impressed at how
short-lived and down to earth his confrontation with our heroes was. Not 10
minutes of invulnerable beings pummelling each other while leaping tall
buildings in a single stride, but two short, brutal close quarters slug fests.

Jackman, Stewart, Keen, Mangold: take a bow.

5/5

[Home](/blog "ohthreefive")
