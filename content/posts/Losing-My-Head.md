+++

date = "2017-06-12T22:31:56Z"
title = "Losing My Head"
description = "My first ever attempt to set up Ubuntu server"
tags = [ "commandline", "Linux" ]

+++

Background to the post first, I'm afraid: my wife bought a [Lenovo 3000 N200][1] nine or ten years ago. It remains the only Windows machine anyone in my family has ever bought! A few years and a *lot* of slow-down later, she was moving on to a MacBook. Not being one to discard working tech, I became the machine's custodian.

We were in late 2011, and Chrome OS had recently appeared. "Great!" I thought, "I only need a browser anyway!" I was technically savvy enough to have heard of Chrome OS, but not to know anything about its installation (i.e., no Chromebook, no Chrome OS. ) When I discovered this disappointing fact, I turned my search engine to equivalents / clones and discovered the no-longer-with-us [Jolicloud][2]. Jolicloud was alas not a great experience, so I thought I'd try my hand at this Linux malarkey.

**(interesting: I just learned that Jolicloud actually pre-dated Chrome OS)**

I managed to get Ubuntu 12.04 up and running and upgraded to 14.04 in due course. I think I popped another Gb of ram in the machine at some point but still it struggled with the Unity desktop. I had since acquired another hand-me-down that was working better and so was considering retiring the Lenovo.

Then I discovered [Plex][3]. I'd love to use something open as my media centre but the remote access in Plex is too simple to neglect. The Lenovo became a server and, over time, acquired a few extra tools. Most importantly I used its internal hard drive as kind-of personal cloud storage first by using BitTorrent Sync then Syncthing.

As my Linux knowledge progressed I realised Unity was pretty stupid on a 'server' and an old machine. I'd heard about Ubuntu Mate and thought it sounded like a better fit. I wasn't ready to go headless and wanted a desktop so that if anything went wrong, I could just take the laptop out the cupboard and try to fix it.

Having never installed alternative distros or desktop environments, I found the process a challenge but, apparently, succeeded. In hindsight, I'd merely installed the Mate desktop on top of my current install rather than installing pure Ubuntu Mate. I persevered for a while (and grew to like the Mate desktop whenever I had to use it) but something was still bothering me.

Whenever the room in which the server was became relatively quiet, there was a good chance I could here a certain laptop's fan absolutely going for it! This was regardless of whether or not I was streaming a film or whether Syncthing was transferring files.

So, around five years after my first Ubuntu desktop install, I decided to try my first Ubuntu server install. This would test the hardware once and for all: if the fans still roared when the machine was idle, it was time to ditch the Lenovo.

### GRABBING THE ISO

From [Ubuntu's download page][4] I grabbed the .torrent file to [torrent the ISO for 14.04 server for 32-bit architecture][5] and had the ISO in no time. I have a horrible habit of trying to make things hard for myself, e.g. trying to find how to get the ISO on to a USB using the command line. Happily, while searching for this, I came across [this official Ubuntu page][5] which informed me there's a no hassle gui way of doing this built right in to my Ubuntu desktop. Common sense prevailed and I had my bootable USB.

### INSTALLING

I don't need to go in to too much detail here, as the install was relatively quick and straight forward. A few questions that the installer asked required a bit of head scratching but I got there! I was particularly impressed that the installer asked for wifi credentials so it could pull down updates. Slick.

### TIME TO BOOT

The first boot went fine and I logged in as the user I'd set up during install. One quick `sudo apt update` later was enough to ruin everything. No network connection! Now, quite why the installer can't transfer the wifi credentials to the OS is one matter, but I'd plugged the damned thing in to my router via ethernet so was expecting instant internet access.

Embarrassingly, I had to turn to the internet for my solution. Yup, I couldn't access the internet without being able to access the internet. Oh, brave new world! The wonderful [Ubuntu forums][6] rescued me.

Like the person who posted that link, I'd run `ifconfig` and been rather disappointed not to see `eth0`!  I'd added the following to `/etc/network/interfaces`

    auto eth0
    iface eth0 inet dhcp

with no joy. Based on the post, I ran `lshw -C network` but wasn't immediately sure what to do with the output. Turns out, the important info is in the `logical name` line for the Ethernet controller. Ubuntu doesn't use the standard `eth0` so I needed to specify the name shown in the above command.

After that, `sudo service networking restart` got everything working. phew!

### ALL THE SOFTWARE!!

After all that, the only things I wanted to install were Syncthing and Plex. A quick web search turned up ways to get them to auto start on boot. So, as well as a server install, I messed with `systemd`! Syncthing [has documentation][7] for start on boot with `systemd`. As I was logged in as a user, I thought a user service made more sense than a system service (this was solely based on my opinion/guess.)

Syncthing has a web gui and a really simple way to access a headless node's gui from another Linux machine. The [FAQ][8] describes a few methods but I prefer `$ ssh -L 9090:127.0.0.1:8384 user@othercomputer` then visiting `127.0.0.1:9090` from that computer. Works like a charm. 

I found a `systemd` service file for Plex somewhere on its forum, but I've lost it now unfortunately. But it's working as planned.

### HUSH

The server is quiet as a mouse now, with no untoward turbo fan action seven days after install! Success.

### WRAP

I'm really proud of my journey from flashing a new OS to setting up a headless server. Fingers crossed I get a few more years out of the Lenovo!

[Home](/blog "ohthreefive")

[1]: http://www.trustedreviews.com/Lenovo-3000-N200-Notebook-review "BRIIIIIIIIIIIIICK!"

[2]: https://www.jolicloud.com/ "Died mere weeks ago"

[3]: https://www.plex.tv/ "Media centre goodness!"

[4]: https://www.ubuntu.com/download/alternative-downloads "lots of nice things here"

[5]: https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu "helpful"

[6]: https://ubuntuforums.org/showthread.php?t=2332332 "so much info"

[7]: https://docs.syncthing.net/users/autostart.html#linux "decent, but not perfect, docs"

[8]: https://docs.syncthing.net/users/faq.html "lovely tricks!"
