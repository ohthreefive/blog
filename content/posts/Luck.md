+++
         
date = "2022-10-10T11:02:27.226Z"
title = "Luck"
description = "brief movie reviews #69"
tags = [ "movie" ]

+++

Surprisingly dated looking animation and a complete lack of subtlety.

Good slapstick humour at the start.

2/5

[Home](/blog "ohthreefive")