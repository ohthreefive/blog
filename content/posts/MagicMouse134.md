+++
date = "2020-03-25T10:42:33.584Z"
title = "Apple Magic Mouse 1 support iPadOS13.4"
description = "Support for the original Magic Mouse is suboptimal"
tags = [ "iOS" ]

+++

iPadOS 13.4 was out yesterday with new trackpad support.

I've been using my Magic Mouse 1 (ie. the one with batteries) for some time with
Assistive Touch, obeying the adage, "Treat it like a finger, not a mouse," and
I've been loving it.

Using the mouse with the new support is better... and worse.

Overall, the support for a mouse _as a mouse_ is better and I like the
equivalent of 'hot corners' to show the dock, the slide over apps and
notification centre, though I've accidentally activated notification centre a
fair few times, which is a little annoying.

However, gestures are not supported at all. No scrolling on webpages, no multi-
finger gestures to swipe between apps and the like.

The thing I miss the most is the scrolling: with assistive touch I could 'touch'
the screen by clicking the mouse then fling the screen up or down as required. I
can't do that now with the Magic Mouse 1. I miss it and don't like reaching
up for the screen to scroll; I've even started using my Apple Pencil more as a
scroll device!

I have a Magic Trackpad 1 somewhere and I'll try that when I find it, but I
suspect I'll be shelling out for either a Magic Trackpad 2 or the new Magic
Keyboard cover with built in trackpad.

[Home](/blog "ohthreefive")