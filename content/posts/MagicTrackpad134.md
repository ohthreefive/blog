+++
date = "2020-03-25T18:48:24.751Z"
title = "Magic Trackpad 1 + iPadOS 13.4 != happiness"
description = "The original trackpad is as hamstrung as the mouse"
tags = [ "iOS" ]

+++

Yup, as expected, the Magic Trackpad does not function as a multi-touch device
just like the mouse.

It's a better pointing device though, in the seconds I've used it, so it'll be
my device of choice until I get my hands on the new keyboard cover!

[Home](/blog "ohthreefive")