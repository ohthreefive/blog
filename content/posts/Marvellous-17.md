+++

tags = ["movie"]
date = "2017-11-21T21:56:45Z"
description = "My ranking of Marvel's output"
title = "Marvellous 17"

+++

### CRAP

Bottom of the list are:

* Thor: The Dark World (2013)
* Iron Man 2 (2010)

### FINE

Watchable, not necessarily re-watchable:

* The Incredible Hulk (2008)
* Thor (2010)
* Captain America (2011)

### GOOD

But I wanted even better:

* Ant Man (2015)
* Guardians of the Galaxy vol 2 (2017)
* Doctor Strange (2016)
* Iron Man (2008)
* Guardians of the Galaxy (2014)
* Avengers: Age of Ultron (2015)
* Captain America: The Winter Soldier (2014)
* Thor Ragnarok (2017)
* Spiderman: Homecoming (2017)

### GREAT

Best of the bunch:

* Iron Man 3 (2013)
* Captain America: Civil War (2016)
* The Avengers (2012)

In that order.

[Home](/blog "ohthreefive")
