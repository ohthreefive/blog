+++

date = "2019-01-03T11:59:49Z"
title = "Mary Poppins Returns"
description = "Brief movie reviews #28"
tags = [ "movie" ]

+++

Practically perfect in every way? Well, yes. I loved the songs, the
choreography, the child actors, the Van Dyke. I also loved the more subtle
aspects: Michael’s transformation from chill dad to more like the George Banks
of the original is quietly done (and quickly in-done.)

Emily Blunt is the best thing in the movie, however. I started watching with
concern: how would she emulate Julie Andrews? The answer was clear as soon as
she started speaking. I found myself asking, "Is that how Julie Andrews sounded?
I don’t think so..." But this was of course the point. This is a familiar and
different Mary Poppins. Other than the voice, the biggest difference I noticed
was this Poppins’ obvious delight in the flights of fancy.

Olly Richards of Empire also noticed this, and [articulated it much better in
his review.][1]

[Home](/blog "ohthreefive")

[1]: https://www.empireonline.com/movies/mary-poppins-returns/review/ "Empire's review of Mary Poppins Returns, written by Olly Richards"
