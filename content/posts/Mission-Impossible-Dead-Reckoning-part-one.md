+++
date = "2023-07-16T13:38:08"
title = "Mission: Impossible - Dead Reckoning, part one"
description = "Brief movie reviews #78"
tags = ["movie"]

+++

If I was to rank truly excellent blockbusters since the release of [Fallout] [1], Tom’s last three movies would be top 5, easily.

It starts slower than Fallout, including a surprisingly average pre-credits scene, and for me, built momentum much more slowly than its predecessor. It felt like there were fewer standout action scenes than Fallout too, though when the stunts did come… wow.

Hayley Atwell is superb, as is Vanessa Kirby in her small-but-a-bit-less-small role. Pom Klementieff is also good value, even if at first I thought her character was going to be a one-note caricature.

Cruise was as fantastic as ever but Esai Morales packed less punch than Henry Cavill - not too surprising.

So while it didn’t surpass Fallout, there’s not many other movies that it didn’t.

4/5

[Home](/blog "ohthreefive")

### The list

These aren’t in a true ranking, and Infinity War was the same year as Fallout, just earlier in the year. Tenet is the only other ‘real world’ movie, though significantly more sci-fi than Mission. It is heartening to notice that with Tom, Nolan and Villeneuve, the film-making team involved in over half of the list vocally believes in *cinema*, not just profit.

1.  [Fallout] [1]
2.  [Top Gun: Maverick] [2]
3.  [Avengers: Endgame] [3]
4.  [Tenet] [4]
5.  [Dune: Part One] [5]
6.  [Spider-Man: Into the Spider-Verse] [6]
7.  [Spider-Man: Across the Spider-Verse] [7]
8.  Dead Reckoning

[1]: https://ohthreefive.gitlab.io/blog/posts/mission-impossible-fallout/ “My Fallout first impressions”
[2]: https://ohthreefive.gitlab.io/blog/posts/top-gun-maverick/ “My Maverick first impressions”
[3]: https://ohthreefive.gitlab.io/blog/posts/avengers-endgame/ “My Endgame first impressions”
[4]: https://ohthreefive.gitlab.io/blog/posts/tenet/ “My Tenet first impressions”
[5]: https://ohthreefive.gitlab.io/blog/posts/dune/ “My Dune first impressions”
[6]: https://ohthreefive.gitlab.io/blog/posts/spider-man-into-the-spider-verse/ “My Into the Spider-Verse first impressions”
[7]: https://ohthreefive.gitlab.io/blog/posts/spider-man-across-the-spider-verse/ “My Across the Spider-Verse first impressions”

(Rebecca Ferguson and Beloved Talos in the same week - 💔)