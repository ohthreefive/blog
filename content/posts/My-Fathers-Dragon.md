+++
date = "2022-11-13T08:49:09.926Z"
title = "My Fathers’ Dragon"
description = "Brief movie reviews #70"
tags = [ "movie" ]

+++

For a studio with a unique visual aesthetic, some of this was a little
derivative. The turtle was a bit Neverending Story, the cats a bit Where The
Wild Things Are. The air of melancholy, common to Cartoon Saloon projects, added
to the poignancy, particularly for the adults in the room. And the howler monkey
- so unsettling!

4/5

[Home](/blog "ohthreefive")