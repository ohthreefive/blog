+++
date = "2024-03-23T11:54:48Z"
title = "My Neighbour Totoro"
description = "The theatre, dahling!"
tags = ["Random"]

+++

Shortly after getting tickets to Hamilton, I learnt that the RSC adaptation of one of my favourite movies of all time. The production were keen to minimise online images of the show and I avoided any chances of ruining the experience too.

After just under a year of waiting, it was better than anything I could have hoped for. The absolute wonder was captured. The puppet Totoros were better than I thought was possible. There was spontaneous applause numerous times. Me and the children were absolute buzzing at the interval.

As I said to my wife, I could go to the theatre every night for the rest of my life and I doubt I’d have an experience quite like it.

5/5

[Home](/blog "ohthreefive")