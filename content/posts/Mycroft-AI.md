+++

date = "2017-10-11T08:58:42+01:00"
description = "Mark 1 arrived a while ago but it's only just out of its box."
tags = ["hardware"]
title = "Mycroft AI"

+++

On July 17th I [finally received][1] my Mycroft Mark 1. I've just checked my
Kickstarter messages and Joshua Montgomery, the main man behind the project,
thanked me for backing it on September 7th 2015. Mycroft was long-awaited.

The hardware doesn't disappoint. It's assembled (which I didn't take for granted
for a hardware project), sturdy and attractive in a retro techway. The widely
spaced large LED eyes are, frankly, cute. This is a big plus. I'm happy with
Mycroft sitting in the corner of one of my living rooms. It's conspicuous in a
pleasant way, and it's not creepy like the monolithic Echo. (I can't judge the
new Alexa- or Google-powered projects as I haven't seen them in person.)

The setup, with [web assistance][2] was easy.

That's the end of the praise.

It's slow. It doesn't respond to the wake word nearly consistently enough. I
don't know when (or why) it will respond to me with voice sometimes and with
something on-screen other times.

The software is *years* behind Alexa, Google Assistant and Siri. Inside the box,
however, was a card reminding the owner of the beta status of the device and
saying that come February 28th 2018, the device will be ready for mainstream.

We shall see.

I've just recently seen that [Mark 2][3] has been teased. Mycroft AI: please
focus on software, not new hardware!

[1]: https://twitter.com/ohthreefive/status/886935032161599488 "thanks for the import fee..."

[2]: https://mycroft.ai/

[3]: https://www.facebook.com/techcrunch/videos/10156031925967952/ 

[Home](/blog "ohthreefive")
