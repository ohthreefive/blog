+++

date        = "2017-02-25T22:50:47Z"
description = "Installing Mycroft on Raspberry Pi - first impressions"
title       = "Mycroft Picroft"
tags  = [ "raspberrypi" ]

+++

### MYCROFT

[Mycroft][1] is, "AI for everyone." Open source AI which can run on loads of
different devices. I backed [the Kickstarter][2] which is running a little late
(7 months by my count.)

### PICROFT

More recently, the team announced an [image for the Raspberry Pi][3] so I
downloaded the `.zip` and extracted the image (nearly 4 Gb.) The install
instructions are poor (Windows only!) but it's an `.img`, just like Raspbian or
any of the other images offered on the Raspberry Pi website, so the [Linux 
install instructions][4] work perfectly.

### FIRST BOOT

Picroft boots in to a CLI and tells you to connect to a wifi hotspot generated
by Mycroft. Once connected, it let's you connect the Pi to a wifi network via a
web interface. Thereafter on boot the CLI tells you Mycroft is awaiting the
wake word 'hey Mycroft' and shows you output as you speak.

### PI 3 VS PI 2

I have a Pi 3 and a spare Pi 2 lying around. I tried Picroft first on the Pi 3
and it worked really well. When I tried it on the Pi 2 is was ***SLOW*** and
even after taking forever to recognise 'hey Mycroft', it never recognised what I
asked! So I guess Pi 3 is recommended.

### WRAP

I really think voice controlled home automation / AI etc will be huge. Alexa has
been very successful and I have tried the [Alexa Raspberry Pi][5] combo. But I
am more than a little concerned about ceding this private information to a third
party. I'm prepared to accept the smaller feature set of Mycroft for the
benefits of using an open source project.

![Mycroft image][pic1]

[1]: https://mycroft.ai/

[2]: https://www.kickstarter.com/projects/aiforeveryone/mycroft-an-open-source-artificial-intelligence-for/description

[3]: https://mycroft.ai/mycroft-now-available-raspberry-pi-image/

[4]: https://www.raspberrypi.org/documentation/installation/installing-images/linux.md

[5]: https://github.com/alexa/alexa-avs-sample-app/wiki/Raspberry-Pi 

[pic1]: https://ohthreefive.gitlab.io/blog/images/mycroft.jpg "Mycroft"

[Home](/blog "ohthreefive")
