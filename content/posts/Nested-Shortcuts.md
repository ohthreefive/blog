+++

date = "2018-10-16T21:36:34Z"
title = "Nested Shortcuts"
description = "Shortcuts, née Workflow, is amazing. But sometimes it's annoying."
tags = [ "ios", "programming" ]

+++

I have several Linux machines but iOS is my main computing platform. To that
end, I've been using Workflow (now Shortcuts) for some time to do things most of
my friends didn't think were possible on an iPad.

I am no developer, but I have dabbled a fair bit in Python and a tiny bit in
JavaScript. `if...else` clauses are bread and butter logic in programming. They
**must** be basic, as I don't seem to be able to write a script without them!

However, as in real life, things are rarely only one thing or the other[^1].
That's where `elif/else if` or `or/ and/ not ||/ &&/ !` comes in. Except not in
Shortcuts. There is just If...Otherwise, meaning multiple If...Otherwise can be
required in a single step in a shortcut.

This makes it hard to keep track of where you are in the logic (which Otherwise
is this!?) and also indents the Shortcuts horrendously. Honestly, try going three
deep[^2] in portrait mode on a phone. Unpleasant.

I had a simple shortcut to do one of two things based on whether it is Monday or
Thursday. I have the wonderful app Things remind me to run this Shortcut on both
of these days. It doesn't only remind me, the reminder also contains a url
scheme to launch the shortcut from the reminder itself. Just thinking about it
makes me feel warm and fuzzy.

All was well until I forgot to run Monday's shortcut. Option: re-write the
shortcut to always ask the day? **NO**. It's called automation. I don't want to
have to interact. Option: write some logic to treat Tuesday as Monday and Friday
as Thursday? Maybe, after all, I'm unlikely to forget for more than one day. In
the end, no, too complicated (an extra step would need to be written for both
days and it still doesn't cover me being lazy and not running the shortcut until
a Wednesday.)

Solution: add a check so that if it's not Monday or Thursday, I'm asked to pick
one of those days. Fully automatic? No, but I deserve the hassle for ignoring
Things' helpful prompt.

I quickly realised the lack of copy and paste of blocks of actions in Shortcuts
and the nested Otherwise clauses were annoying and confusing, respectively, and
that this was turning out to be more work than it was worth.

Until I thought about functional programming. If I split my programme in two,
with one 'function' picking the day and the other 'function' performing an
action based on the day, this becomes really easy to deal with.

The Run Shortcut action in Shortcuts runs a named shortcut and passes the last
output to it. So the last output of my day picking shortcut is a Get Variable
(the day I want) and the first action of the second shortcut is to store that
variable (ie. Set Variable.) I don't know if there is another, better way of
passing info between the two (Magic Variables?) but this is pretty simple.

I was so happy with how this was I mistakenly uttered the word, "Perfect," under
my breath. But of course it's not perfect. I've just added two shortcuts to the
(by-design) hideously disorganised Shortcuts 'Library' view. _Shudder_. When
will someone come up with a better organisation system?

To sum up, treat shortcuts as building blocks and you might find some logic gets
simpler!

[Home](/blog "ohthreefive")

[^1]: "only a Sith deals in absolutes" owk
[^2]: Oo-er!
