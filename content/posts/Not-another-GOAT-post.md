+++

date = "2019-01-26T12:45:30Z"
title = "Not another GOAT post"
description = "Who wins all the Slams EXCEPT their favourite?"
tags = [ "tennis" ]

+++

---

### UPDATE TIME 2

I think this is the first update since mid 2019. No further slams for
Roger. Rafa and Novak have another incredible 5 each. Rafa ascends to
the top of the total numbers, and moves to third in the 'without
favourite tournament' table, still well behind Roger and Nole.
Roger remains top of that table, though the ever flexible Novak is
gaining. I think he will overtake him, although his vaccination
obstinance might stand in his way.

I expect Roger and Rafa to retire in 2013 after Wimbledon and Roland
Garros, respectively. Roger will do so sooner if he doesn't think he
will manage to play SW19 2023. Rafa will play longer if he wins RG 2023!

---

### UPDATE TIME

7 slams since the original post (after Rafa completed la decima) so let’s
update. Unsurprisingly, Roger, Rafa and Novak have shared all seven!

Skip the original text below to see the updated table.

---

**VAMOS!!**

A spectacular fortnight for Rafa and a well deserved 10th French Open title.
Truly the greatest clay court player of all time.

Uh-oh, did someone nearly say GOAT!? A subject which always brings debate; never
resolution.

Rather than try to settle it, allow me to add something to the discussion.
Something I've never personally seen discussed elsewhere. Rafa is the king of
clay. Roger and Pete were grass masters. Novak thrives in Melbourne. But just as
great teams need to win home and away, how do the top Slam winners fare away
from their favourite tournament?

Here's how:

| Player | Slam final W-L | W-L without favourite tournament | % wins at fave tournie |
|:---:|:---:|:---:|:---:|
| Rafael Nadal | 22-8 | **8**-8| 64% |
| Roger Federer | 20-11 | **12**-7 | 40% |
| Novak Djokovic | 20-11 | **11**-11 | 45% |
| Pete Sampras |14-4 | **7**-4 | 50% |
| Roy Emerson | 12–3 | **6**-2 | 50% |
| Rod Laver | 11–6 | **7**-4 | 36% |

Interesting, eh?

Just look at how many Slam finals Fed, Rafa & Nole have reached. Add in 11
finals for Murray. That's 103 Slam finals (admittedly many against each other!)
among just 4 players of the same generation. Wow!

[Home](/blog "ohthreefive")
