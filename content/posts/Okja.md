+++
date = "2020-02-22T12:22:28.329Z"
title = "Okja"
description = "Brief movie reviews #41"
tags = [ "movie" ]

+++

Not a new release, but as I'm seeing the acclaimed 'Parasite' soon and it's been
years since I saw The Host, I thought I'd remind myself about the director. I
caught Snowpiercer recently on Netflix too - fantastic.

A wonderful and weird tale.

4/5

[Home](/blog "ohthreefive")