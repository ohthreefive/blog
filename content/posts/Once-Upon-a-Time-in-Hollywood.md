+++
date = "2019-08-17T11:40:26.231Z"
title = "Once Upon a Time in Hollywood"
description = "Brief move reviews #35"
tags = [ "movie" ]

+++

A relaxed and deliberate 'day' in the life of an old-fashioned actor at the end
of old-fashioned Hollywood, with some revisionist history bolted on for a fire-
cracker of a finale. This feels like it came from Quentin's heart, and while the
violence of the finale is over the top (here's looking at you, fireplace) my
reaction to the revisionism ("here's what could have been") was a deeper and
stronger emotion than the fist-pumping reaction to the Basterds finale. This is
also the first QT I've seen on the big screen since Basterds, and I won't miss
his next.

4/5

[Home](/blog "ohthreefive")