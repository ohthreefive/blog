+++
date = "2023-08-02T09:15:29+02:00"
title = "Oppenheimer"
description = "Brief movie reviews #79"
tags = ["movie"]

+++

Nolan’s first biopic is a triumph. Fascinating deep dive into a man of unfathomable depths. Too much to take in in one viewing, but perhaps not the movie I would want to revisit lightly.

4/5

[Home](/blog "ohthreefive")