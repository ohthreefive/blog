+++

date = "2017-12-31T19:30:32Z"
title = "Paddington 2"
description = "Brief movie reviews #17"
tags = [ "movie" ]

+++

The original Paddington movie was charming but didn’t really hold my attention.
The charm returns with a lot more humour, and it’s the better movie for it.

3/5 

[Home](/blog "ohthreefive")
