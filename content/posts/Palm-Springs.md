+++
date = "2022-01-10T10:04:16.938Z"
title = "Palm Springs"
description = "Brief movie reviews #54"
tags = [ "movie" ]

+++

A delightful little gem. Funny, the correct mixture of dark and warm-hearted.
SHORT!

5/5

[Home](/blog "ohthreefive")