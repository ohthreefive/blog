+++
         
date = "2022-01-10T10:06:57.404Z"
title = "Portrait of a Lady on Fire"
description = "Brief movie reviews #56"
tags = [ "movie" ]

+++

Intense and gripping. Unlike anything I'd ever seen before.

4/5

[Home](/blog "ohthreefive")