+++
date = "2022-08-25T06:18:44.123Z"
title = "Prey"
description = "Brief movie reviews #67"
tags = [ "movie" ]

+++

Such a shame this was a streamer only. Lean and focused, with a little shonky
CGI and one dimensional (human) villains being the only low points. Great stuff.

4/5

[Home](/blog "ohthreefive")