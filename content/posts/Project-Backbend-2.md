+++
date = "2022-02-03T08:32:16.582Z"
title = "Project Backbend 2"
description = "I stood up from a backbend… with help!"
tags = [ "Random" ]

+++

9 months to the day since I first dropped back into a backbend, I stood up from
one without the assistance of another human. With 5 stacked yoga blocks under my
hands, I actually found it pretty easy. Now it's time to work on decreasing
those blocks! Woohoo!

[Home](/blog "ohthreefive")