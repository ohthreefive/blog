+++

title = "Project Backbend 3"
date = "2022-03-26T11:32:16.582Z"
tags = [ "Random" ]
description = "Sooner than expected"

+++

Saturday March 26th 2022! That's the day when, as if from nowhere,
I stood up from a backbend with no blocks, no help, and no idea it
was going to happen!

This is year 5 of my yoga journey. When I began, I couldn't straighten
my arms in backbend, and who knows what else was holding me back.

That means it was 10 months from drop back to stand up, having been
about ten months from concentrating on back bend to drop back and about
30 months of yoga before I felt able to concentrate on back bend.

I'm pretty stoked with this effort!

[Home](/blog "ohthreefive")