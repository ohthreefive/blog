+++
date = "2021-05-14T10:05:33.514Z"
title = "Project Backbend"
description = "I dropped into a backbend! Half way there."
tags = [ "random" ]

+++

So on the 1st May, 2021 I dropped back from standing into a backbend/bridge both
without serious head injury and landing on both hands simultaneously. In the two
weeks since, I haven't done it again half as gracefully, but I have repeated the
feat every Tuesday and Saturday at my Mysore classes.

Project backbend started in July of last year. Or at least it re-started then.
It initially started at the beginning of 2020 when I moved to second series, but
then I broke my wrist in April, so things went on hold.

It has been an incredibly hard year of constant work on shoulder
opening/external rotation, hip flexor strengthening but also lengthening, not to
mention increasing mobility in my actual back.

Project backend is not complete - I still need to stand up from a backbend - but
I'm now half way there and I'm now confident I won't complete the project in a
year as originally hoped. A year was an arbitrary and silly target, anyway!

Now, about Project Handstand...

[Home](/blog "ohthreefive")