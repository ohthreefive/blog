+++
date = "2021-08-15T07:18:23.272Z"
title = "Project Handstand"
description = "Learning to handstand"
tags = [ "Random" ]

+++

Project Handstand began as an offshoot of project backbend on 28th September
2020.

The aim was not only to handstand, but to improve my backbend as the shoulder
opening required for both was something I was hugely lacking in.

In May/June this year I began to think something might be wrong with my
cerebellum: I was still regularly struggling to achieve a five second hold.

However, on 11th August 2021, I managed a 23 second hold! 20 seconds was the
first milestone I wanted to reach.

From here I just need to keep the banana minimised then increase to one minute!

[Home](/blog "ohthreefive")