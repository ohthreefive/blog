+++
date = "2023-02-26T10:34:04.493Z"
title = "Puss in Boots: The Last Wish"
description = "Brief movie reviews #74"
tags = [ "movie" ]

+++

I didn't see any Shrek past no. 2 and didn't see any of Puss' solo adventures,
but this was a treat.

The animation style clearly borrows from Spiderverse but is a worthy imitator -
it's beautiful.

Funny, fast and with a strong message. A pleasant surprise.

3/5

[Home](/blog "ohthreefive")