+++

date = "2017-11-25T20:48:27Z"
title = "Quad9 DNS - setting it up on iOS"
description = "Setting up a recommended DNS service on iOS"
tags = [ "security", "ios" ]

+++

#### BUT FIRST...

[Security Now][1] is an essential podcast. I started listening this year and
recommend you do too! Quad9 is the first topic Steve has covered that’s been a
useful-sounding service/product to me. But anyway, SUBSCRIBE!

#### QUAD9

Having listened to the pod three days ago, I finally got round to visiting the
website today. Simple enough stuff, but there are only Mac & Windows
instructions on the website. My casual browsing is on an iPad and my main
machines run Solus and Ubuntu Linux.

#### iOS INSTRUCTIONS

1. Go to Settings> Wi-Fi
2. Hit the ‘i’ in a circle beside the WiFi icon of the network to which you’re
   connected
3. Under ‘DNS’ select ‘Configure DNS’
4. Change from ‘Automatic’ to ‘Manual’
5. For me, the only DNS server listed was my WiFi router’s local IP address; I
   deleted this
6. Add 9.9.9.9 as a new DNS server

That’s it!

The Mac instructions suggest adding 9.9.9.9 to the "top" of your list of DNS
servers, so maybe you should re-add your original server below 9.9.9.9, but I
genuinely don’t know what’s correct here. I’m going to leave it deleted for now.

Quad9 can be found [here][2] and there’s an ArsTechnica article about it
[here.][3]

(Must try on my Linux machines next)

[Home](/blog "ohthreefive")

[1]: https://twit.tv/shows/security-now "Security Now on TWiT; also available at grc.com"

[2]: https://quad9.net/ "Quad9 website"

[3]: https://arstechnica.co.uk/information-technology/2017/11/new-quad9-dns-service-blocks-malicious-domains-for-everyone/ "Ars article"
