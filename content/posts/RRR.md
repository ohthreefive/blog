+++
date = "2023-03-28T16:08:37.757Z"
title = "RRR"
description = "Brief movie reviews #75"
tags = [ "movie" ]

+++

I kind of ignored the hype cycle around this ("Just another bit of Netflix
marketing") but should have investigated more before dismissing it.

I started watching earlier in the year and had the privilege of watching
[Oscar-winning][1] song Naatu, Naatu *before* knowing it was an Oscar
winner. I thought it deserved an award!

I was absolutely blown away with the inventiveness of the action here:
uncontrolled broad-faced grinning and even laughter.

The script and the acting - by the English speaking cast at least, are
**terrible**, but I was able to forgive that for the freshness of it all.

My first comment coming out of [Mission Impossible: Fallout][2] was that it
highlighted how poor recent Bond movies had been.

RRR makes is clear how poor recent superhero movies have been.

Because this is a superhero movie!

5/5

[Home](/blog “ohthreefive”)

[1]: https://www.youtube.com/watch?v=OsU0CGZoV8E “YouTube link for Naatu, Naatu”

[2]: https://ohthreefive.gitlab.io/blog/posts/mission-impossible-fallout/ “My
brief review of Mission Impossible: Fallout”