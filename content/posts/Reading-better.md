+++

date = "2018-09-23T20:10:30Z"
title = "Reading better"
description = "Making changes to my audiobook experiences"
tags = [ "Audible", "random" ]

+++

I came across a great [blog post][1] either via BoingBoing or Lifehacker by a
reader frustrated by his lack of remembrance of books. This resonates with me: I
am terrible for forgetting the plot of novels I’ve read, even those I’ve
enjoyed.

This is worse for audiobooks: it’s so easy for your mind to wander and it’s less
easy than when reading to flick back to the distraction point and re-read.

So, using [Drafts'][2] excellent dictation, I’m stopping reading after every
chapter I listen to from now on and summarising it. If I can’t summarise it,
I’ll need to listen to the whole thing again. Wish me luck!

[Home](/blog "ohthreefive")

[1]: https://robertheaton.com/2018/06/25/how-to-read/ "Original article by Robert Heaton about reading."

[2]: https://getdrafts.com/gettingstarted/ "Website for the ios app Drafts"
