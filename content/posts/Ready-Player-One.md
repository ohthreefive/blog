+++

date = "2018-04-10T22:55:54Z"
title = "Ready Player One"
description = "Brief movie reviews #21"
tags = [ "movie" ]

+++

Devoid of tension. Devoid of suspense. A huge disappointment. The ‘real world’
portion of the movie was effectively side-lined until the finalé, when we’re
suddenly supposed to care about it.

And from the ‘Berg too. Such a shame.

2/5 

[Home](/blog "ohthreefive")
