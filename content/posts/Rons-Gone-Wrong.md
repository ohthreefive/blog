+++
         
date = "2021-10-23T06:35:00.805Z"
title = "Ron’s Gone Wrong"
description = "Brief movie reviews #52"
tags = [ "movie" ]

+++

Thoroughly enjoyable animated fare with a gentle positive message.

4/5

[Home](/blog "ohthreefive")