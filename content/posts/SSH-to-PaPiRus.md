+++
date = "2020-05-18T19:45:20.999Z"
title = "Sending messages to PaPiRus over SSH using Siri"
description = "Who says an iPad isn’t a real computer?"
tags = [ "command line", "iOS", "raspberrypi" ]

+++

I backed PaPiRus, an ePaper Raspberry Pi HAT a *long* time ago. This
[Kickstarter update][1] announced that shipping to backers finished in January
2016.

It sat in a drawer.

For some reason, I've had a lot of time on my hands recently.

It came out of the drawer!

## Obsolete hardware?

I think my PaPiRus is v1.8 and Pi Supply are now up to v2.0. Thankfully the only
difference I could see was that the v1.8 had a place for you to solder a pin
allowing the Real Time Clock to wake the Pi which is compatible with the Pi 2B.

I only had a 3B to hand... but thankfully didn't need the RTC wake-up
functionality!

The older HAT fitted the newer Pi fine.

## Soldering not required then?

Well actually the four buttons on the screen all need to be soldered on. I am
terrible at soldering but persevered with the task despite the fact I probably
won't use the buttons.

Like I said, time on my hands.

## Software

The install of the software is blissfully easy and documented on the [Github
README][2]. Yes, it's a curl & then run as root. Not best (even good?) practice,
but I trust the site.

That same README details ways to use the HAT with `python` and `bash`. They are
*really* easy.

## Enter SSH

I `SSH`'d into the Pi and quickly played with the `bash` commands and added a
couple of lines to a little `python` maths programme I've been writing with my
daughter so that PaPiRus prints the answer to the screen (only if it's correct!)

## Enter Siri

I've been playing extensively with Shortcuts on iOS recently and something told
me to have a look for SSH functionality.

Sure enough, there is a Run Script over SSH option. You can chose a key or
user/password for authentication and the rest of the options are
straightforward. I chose user/password so I didn't have to deal with keys,
something I've never done in Shortcuts.

I started my shortcut with an Ask for Input action which will let me type if I
select the Shortcut by pressing or speak if I invoke it with Siri.

In the Script section of the Run Script over SSH I simply wrote: `papirus-write
'provided input'` which inserts my input into the simplest of `bash` commands.

(There is also an option to pass a file to `stdin` which I haven't explored.)

I included a Show Result action at the end to see what happened, and the answer
made me smile. The 'result' is what the 'script' prints, which in the case of
`papirus-write` is confirmation of success. Show result speaks the result if the
shortcut is invoked by Siri.

Here is the shortcut. It's only three actions, but need only be two.

![Screenshot of shortcut actions][pic1]

## Video

[Here is the link][3] to a video of it in action!

[Home](/blog "ohthreefive")

[1]: https://www.kickstarter.com/projects/pisupply/papirus-the-epaper-screen-hat-for-your-raspberry-p/posts/1470013

[2]: https://github.com/PiSupply/PaPiRus/blob/master/README.md "PaPiRus instructions on Github"

[3]: https://youtu.be/z0wdJtbhOqU "A short video of all this writing in action"

[pic1]: https://ohthreefive.gitlab.io/blog/images/Screenshot_Shortcut-2.jpeg "Not complicated!"
