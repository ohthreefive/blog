+++
date = "2021-09-13T07:51:27.471Z"
title = "Shang Chi and the Legend of the Ten Rings"
description = "Brief movie reviews #50"
tags = [ "Movie" ]

+++

Marvel is starting to annoy me. Pre-Slattery, there was promise, with two tight,
well choreographed fight scenes only mildly ruined by unnecessary CGI. Post-
Slattery, YAWN, including a terrible big bad out of nowhere.

2/5

[Home](/blog "ohthreefive")