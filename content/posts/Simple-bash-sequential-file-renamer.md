+++

date = "2017-07-03T11:24:09Z"
title = "Simple bash sequential file renamer"
description = "Sequentially renaming files with an interactive shell script"
tags = [ "linux", "commandline" ]

+++

### BACKGROUND

Back when I was an OS X user, I had my first introduction to programming (or at
least programmatic/computational thinking) with the excellent [Automator][1],
bundled free with the Mac operating system.

I could drop a bunch of photos into a workflow I created and they would be
renamed Photo-01.jpg, Photo-02.jpg and so on.

I managed to replicate this on my Android handset using [Tasker][2] back when I
was heavily using Tasker on my Nexus 4.

Since I've moved to desktop Linux, I've never managed to replicate this simple
bit of automation. I can't just shift the files on to my mobile as I've also
stopped using Tasker since I stopped rooting my phones. **(root isn't *required*
for Tasker, but it makes it better!)**

### PYTHON OR BASH

I am very very poor at both Python and BASH (due to lack of consistent use) but
I thought this task could be accomplished using one of them.

I sit in a terminal emulator a lot and know how to (basically) interact with the
file system. On the flip side, I use Python rarely and have no idea how to
interact with the file system using Python. BASH it is!

### THE SCRIPT

I've made a public repository on GitLab [here][3]. Download (or clone) it,
make the script executable:

    sudo chmod +x BASH_seq_rename.sh
 
Then navigate to the directory with the photos and execute:

    ./path/to/BASH_seq_rename.sh

You'll be asked for the file extension of the files you want to rename. This is
essential both for the finding of the files and the renaming. If you have
multiple file extensions, sorry, the script isn't ready for you yet!

It then asks for a prefix, e.g., "Yet-another-wedding".

It'll then show you what's about to happen, e.g.:

    Photo-01.jpg -> Yet-another-wedding-0001.jpg
    Photo-02.jpg -> Yet-another-wedding-0002.jpg
    etc

Then the renaming takes place (automatiically.)

### NOTES

As above, only one file extension is possible due to the nature of the `for`
loop (sorry if you have both jpg & jpegs!)

The order of output will be the same as input (should be ok for digital cameras
& phones; they tend to name sequentially in the first instance.)

The new number is padded to 4 digits so things will work fine up to 9999 files
to be renamed!

I plan to add a tiny bit more interactivity, most importantly an option to STOP
the script after the preview of the changes, in case you don't like what's
about to happen.

### CONCLUSION

It's my first ever interactive shell script and it does a very simply job for me
and it does it well. Maybe someone else will get some use out of it and if so,
great!

[Home](/blog "ohthreefive")

[1]: https://developer.apple.com/documentation/automator "Automator developer page"

[2]: http://tasker.dinglisch.net/ "Enormously excellent Android app"

[3]: https://gitlab.com/ohthreefive/renamer "a barebones repo!"
