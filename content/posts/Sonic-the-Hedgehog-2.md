+++
date = "2022-05-04T07:08:58.862Z"
title = "Sonic the Hedgehog 2"
description = "Brief movie reviews #61"
tags = [ "movie" ]

+++

A wet bank holiday and poor resolve got me dragged to this sequel to a film I
haven't seen. Pop culture humour that will age poorly. Jokes aimed at the
parents which missed the mark. Worst of all, being reminded that James Marsden
should have been a bigger star. Poor.

1/5

[Home](/blog "ohthreefive")