+++
date = "2021-01-01T08:11:17.849Z"
title = "Soul"
description = "Brief movie reviews #47"
tags = [ "movie" ]

+++

A welcome return to daring storytelling from Pixar and yet another foray into
abstract visuals.

4/5

[Home](/blog "ohthreefive")