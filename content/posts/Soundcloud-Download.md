+++

date        = "2017-02-23T21:03:03Z"
title       = "Soundcloud Download"
description = "One way to download tracks from Soundcloud"
tags  = [ "random" ]

+++

### OFFLINE RULES!

I listen to a lot of podcasts. I use RSS feeds mostly and they're great, except
when they don't work. Or they're not offered. One reason for a podcast not using
RSS is because its listeners access it through  Soundcloud, a service which I've
never used so can't really comment on.

One thing that I've never enjoyed about Soundcloud is that there is no easy way
(I know of) to download a track. I really really prefer downloading an episode
rather than downloading the Soundcloud app, creating a (presumably mandatory)
account and subscribing to an artist/playlist.

Today, I made a download-from-Soundcloud method up for myself!

### START AT SOUNDCLOUD.COM

I saw a post on the Atlantic that offered an audio version of the same freely
available article but as an embedded Soundcloud player but not as a downloadable
file.

FUCK YOU!

I went to the Atlantic's Soundcloud page. What you need to do first is click to
share the episode:

![Soundcloud episode page][pic1]

### SHARE... WITH YOURSELF

This brings up a pop-over with the share options. You want to click on 'Embed':

![Share popover][pic2]

Then you want to grab all the text from the code block. Helpfully, there's more
text than can be seen so a 'right click' and 'select all' (or equivalent) are
your friends:

![Embed Soundcloud player options][pic3]

### THERE MUST BE A QUICKER WAY

Paste that code somewhere and look in the string for api.soundcloud.com. For the
episode above, I got `https%3A//api.soundcloud.com/tracks/301859480` (with a lot
of text before and immediately after.) Removing `%3A` leaves
`https//api.soundcloud.com/tracks/301859480`. Looks like an API call to
Soundcloud (like I know what API calls actually are.)

Sticking this link into the browser does nothing, but sticking it in to
[Offliberty][1] generates another URL, which I right-clicked and saved as
'whatever.mp3' on my desktop and lo! There it was.

Perfect, but an incredibly manual process!

[pic1]: https://ohthreefive.gitlab.io/blog/images/soundcloud.png "Share! Share! Share!"

[pic2]: https://ohthreefive.gitlab.io/blog/images/embed.png "How meta would it be to embed the episode in this post?"

[pic3]: https://ohthreefive.gitlab.io/blog/images/copycode.png "important info herein!"

[1]: http://offliberty.com/ "a new website i found"

[Home](/blog "ohthreefive")
