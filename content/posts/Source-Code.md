+++
tags = ["movie"]
date = "2017-03-23T21:19:38Z"
description = "Brief movie reviews #2"
title = "Source Code"

+++

I hadn't seen Source Code since it was out at the cinema. I remember enjoying it
but having a *real* issue with the ending. For me, it would have been much
had it ended with the slow motion pan through the train carriage after
Gylenhaal had saved the day.

Anyway, on a re-watch, Source Code has got even better! It's lean and mean,
pacey and entertaining. Gylenhaal is a fantastic leading man; it's a shame he
hasn't stayed in the A-list leading man game. I know he's doing excellent and
interesting things (Nightcrawler) but he belongs front and centre in the big
leagues.

I still hate the ending, but I've softened a little now that I've realised quite
how short the movie is. There's no filler or bloat anywhere else, so why not
forgive a slow ending?

4/5

[Home](/blog "ohthreefive")
