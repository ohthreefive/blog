+++
date = "2019-08-17T11:33:32.813Z"
title = "Spider-Man: Far from Home"
description = "Brief movie reviews #34"
tags = [ "movie" ]

+++

*Spoiler alert* **review contains a mention of the contents of the final scene
in the movie** *Spoiler alert*

It is definitely becoming a visible effort for Marvel to ground their inter-
Avengers movies, but with that conceit accepted, Far from Home is another fun-
but-light entry in the MCU. Of all the things it got right, the reaction of MJ
in the closing scene, sold perfectly by Zendaya's performance, to swinging
through New York was the most perfect, and my favourite moment in the movie.
Good to see Marvel continuing to push forward in the post-credits sting, too.

3/5

[Home](/blog "ohthreefive")