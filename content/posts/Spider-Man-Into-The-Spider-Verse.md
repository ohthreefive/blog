+++

date = "2018-12-24T10:55:16Z"
title = "Spider-Man: Into The Spider-Verse"
description = "Brief movie reviews #27"
tags = [ "Movie" ]

+++

I have never seen a film with such a look and style as this and it was
(literally) awesome. The style gave the impression of rotoscoping, at least for
the non-action scenes.

But style comes second to substance here. The freedom from the MCU is palpable:
this Spider-Movie can and does chose its own path, have self-contained and
meaningful events and consequences.

It's my second favourite Spider-Man after Spiderman 2 (doesn't that seem like an
unimaginative title now!?)

4/5

[Home](/blog "ohthreefive")
