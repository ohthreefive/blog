+++
date = "2022-01-10T10:07:51.765Z"
title = "Spider-Man: No Way Home"
description = "brief movie reviews #57"
tags = [ "movie" ]

+++

The best MCU movie since Endgame. Hit me in the feels a number of times.
Lacklustre action couldn't even blunt the finale. Left me with a smile on my
face rather than the recent shrug of, "Yup, that's another one."

4/5

[Home](/blog "ohthreefive")