+++

tags = ["movie"]
date = "2017-11-21T21:47:10Z"
description = "Brief movie reviews #15"
title = "Spiderman: Homecoming"

+++

I'm a Marvel completist, and Spiderman was excellent in Civil War, so having
enjoyed Ragnarok in cinemas, I decided to catch Spidey as soon as he was
available to rent.

Where I found Thor hilarious, I found Spidey amusing. But where I found Thor too
casual, too happy to dismiss its huge stakes, I found Spiderman's smaller stakes
to be more gripping and worth investing in.

Like Ant Man, removing global annihilation as the threat actually ups the ante
and helped me invest more in the character. Add in Tom Holland's fantastic
performance, and this was once of the best Marvel movies for some time.

4/5

[Home](/blog "ohthreefive")
