+++

title       = "Spirited Away"
description = "The theatre, dahling!"
date        = "2024-08-11"
tags        = [ "Random" ]

+++

In the shadow of Totoro, a less enjoyable experience. But as a theatre piece, just fabulous.

4/5

[Home](/blog "ohthreefive")
