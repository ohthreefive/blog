+++

date = "2017-12-30T20:13:39Z"
title = "Star Wars The Last Jedi"
description = "Brief movie reviews #16"
tags = [ "movie" ]

+++

Wow.

Holdo’s attack is the most visually stunning moment in any Star Wars movie.

Ren’s faltering, "...please" at the end of the classic villain command, "Join
me," coming in the same scene as, "You’re nothing. But not to me," make for the
most emotional (extended) moment in any Star Wars movie.

I’m with [Helen O'Hara][1] - Kylo Ren is Star Wars’ best villain. Rey is
wonderful again. Poe gets a bit more to do. And Laura Dern takes her short
screen time and becomes unforgettable.

That’s before even mentioning Luke & Leia, both of whom were handled
beautifully, in my opinion.

Finn is the worst served of the Force Awakens alumni, but mostly because his
(and Rose’s) main screen time is the movie’s only yawnsome period: a shrug of a
side plot. A shrugplot.

Based on this, I’d rather Johnson than Abrams was doing ep IX.

4/5

[Home](/blog "ohthreefive")

[1]: https://www.empireonline.com/movies/star-wars-force-awakens/review/ "Helen's Force Awakens review"
