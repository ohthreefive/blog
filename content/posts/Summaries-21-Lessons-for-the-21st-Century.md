+++
         
date = "2019-06-29T08:26:11.406Z"
title = "Summaries: 21 Lessons for the 21st Century"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# 21 lessons for the 21st-century

**Yuval Noah Harari**

## Introduction

- The author comments that Sapiens was a history and a Homo Deus a look at the
  future.
- This book will examine the present.

## Part One: The Technological Challenge

    Humankind is losing faith in the liberal story that dominated global
    politics in recent decades, exactly when the merger of bio-tech and
    info-tech confronts us with the biggest challenges humankind has ever
    encountered.

### Disillusionment: The End of History has been Postponed

- A recap of some of the author's previous work.
- Imperialism gave way to a battle between communism and liberalism, which was
  thoroughly won by liberalism.
- Liberalism is now under great challenge.
- An interesting point is made that the panic in response to attacks on
  liberalism such as the election of Donald Trump and the Brexit vote is
  actually hubristic.
- By panicking, we are saying that we know things will get worse.
- A more humble response would be bewilderment – why has this happened and what
  will be the result?
- Technology and data as potential new ideologies and the consequent rise of the
  useless class are raised.
- Whatever ideology comes to the fore in this modern world must address these.

### Work: when you grow up, you may not have a job

- Starting with a discussion about artificial intelligence and automation, the
  chapter again raises the idea of a useless class – those people who will not
  be required when computers are able to perform our jobs.
- It mentions that while people may initially be required to help run or assist
  these algorithms or robots (eg drone pilots), eventually, they will be able to
  look after themselves.
- In other words, training to become a drone operator in order to ensure you
  still have work will only be a temporary solution and repeated re-training may
  be required over a lifetime.
- A potential solution would be universal basic income.
- A downside of this is that what can objectively be defined as a basic need
  does not cover what is subjectively required for basic happiness and that as
  levels of comfort increase, levels of expectation will also rise.
- Therefore, to supplement the universal basic income, pursuits, be they
  sporting, religion or otherwise, would be required for a contented populace.

### Liberty: Big Data is watching you

- Another long introductory chapter.
- A major theme of the authors previous book is summarised – with machine
  learning, artificial intelligence and ubiquitous connected computering
  (including microphones and cameras pointing at **you**) algorithms may know
  you better than a person or government ever could.
- Going further, input may also include embedded chips which monitor, for
  example, serotonin levels.
- Fully analysed, this may mean algorithms are able to interpret our true
  responses to stimuli, eg. entertainment or feelings towards a certain
  political party, better than any focus group or even ourselves.
- Liberty and free will essentially die in this vision of the future.
- The author also argues that philosophy will remain, or even grow in,
  importance.
- Questions such as how to define safety algorithms of self driving cars cannot
  be answered by algorithms or people with agendas themselves.
- The difference is drawn between intelligence – solving problems – and
  consciousness – feelings we experience.
- Computers and algorithms clearly have intelligence but do not yet have
  consciousness.
- The author suggests that for every dollar invested in machine intelligence, we
  should invest a dollar in exploring, understanding and expanding human
  consciousness.

### Equality: Those who own the data on the future

- Inequality has existed for some time but has previously been based on assets
  and status symbols with no real difference between people other than their
  wealth.
- However, if human enhancement is possible, inequality may become tangible or
  physical: true differences between humans.
- In this scenario, inequality may worsen.
- Data is so powerful that it may become the ultimate arbiter of equality.
- The author is fearful of the power of such data in the hands of corrupt
  politicians – this may be worse than the data where it currently resides in
  the hands of technology companies.
- A major issue will be how to regulate the control of global data.

## Part Two: The Political Challenge

    The merger of bio-tech and info-tech threatens the core modern values of
    liberty and equality, any solution to the technological challenge has to
    involve global co-operation, but nationalism, religion and culture divide
    humankind into hostile camps and make it very difficult to co-operate on a
    global level.

### Community: Humans have bodies

- A discussion about the intersection of online and off-line communities.
- Online communities may be easier but are equally subject to manipulation.
- The reach of technological communities can extend off-line, examples such as
  Google Glass and Pokémon GO are given.
- The separation of online from offline may become a fond distant memory.

### Civilisation: There is just one civilisation in the world

- This chapter heavily emphasises the similarities in the modern world and the
  links between seemingly different communities.

### Nationalism: Global problems need global solutions

- The chapter is an attack on nationalism: not an attack on the right wing
  ideology apparently becoming more common  in modern life but on the concept
  that a nation can be defined and is indeed particularly relevant in the modern
  era.
- Nuclear war remains a concern because the laws of physics are universal and
  therefore eg. North Korea does have a chance of producing a bomb as they are
  working with the same laws as nuclear powered countries.
- The United States/West and the Soviets long since moved past mutually assured
  destruction – something for which the author gives humanity great credit in
  achieving - but there is nothing stopping other countries developing nuclear
  powers.
- Ecological issues also extend beyond a single nation.
- Global warming will affect everyone, though not equally as, for example,
  Russia has a few vulnerable coastal assets whereas some other countries may
  entirely disappear under the waves.
- Technological warfare, cyber attacks and the like, are another modern threat
  which will not be stopped by borders.
- The author returns to the nuclear problem saying that if AlphaGo learns to
  play nuclear chess, we as humans may not even know who is moving the pieces.
- The author goes so far as to argue that it is the global nature of the world
  that allows current nationalist movements to foster – Scotland would probably
  not wish independence without the assurance that Europe will be a major force
  in averting nuclear war regardless of Scotland's place in the UK.
- I think the author's main point is that strict nationalist agendas are
  outdated in the modern world and should not be the driving force in modern
  politics – globalising politics should be.

### Religion: God now serves the nation

- Another chapter largely summarising a theme raised in Homo Deus.
- Religions are ancient and modern issues are not covered in their texts.
- Religion, however, it remains more popular than science as a belief system
  worldwide.
- The author argues that religion is now a lens through which modern issues are
  viewed, with priests et cetera being the interpreters.
- There is no real lesson from the chapter, other than to reiterate that
  religion is going nowhere any time soon and so the modern world must live
  with it.

### Immigration: some cultures may be better than others

- Immigration is discussed and three questions form the basis, framed from the
  perspective of the country opening its borders.

1.  Should we let them in?
2.  Should they be expected to adopt our culture?
3.  Should we treat them as first class citizens?

- The chapter begins by discussing the interplay between these three questions
  then moves to a discussion about racism.
- Immediately, the author criticises modern discussion about racism.
- For the most part, we have moved on from traditional racism, where some humans
  were thought to be biologically better than others, towards what the author
  calls culturalism, whereby we consider some cultures to be superior to others.
- Culturalism is much more complicated.
- One suggestion by the author is that when meeting culture specific comments,
  we must be comparative rather than vague.
- For example, to say that Muslims are an intolerant culture is meaningless: the
  modern Taliban are intolerant; historical Muslims, on the other hand, were
  significantly more tolerant than Christians, depending on which historical
  time period you choose.
- The author swings back to his discussion about immigration and uses the
  European experiment as the basis of his discussion.
- If the European Union can continue and succeed in integrating, for example,
  refugees while retaining the basic and laudable values of so-called Europeans,
  then this could be a global template for a multicultural world.
- However, if Europe fails, how can we expect the world to unite and combat the
  already introduced issues of nuclear war, ecological collapse and cyber-
  attacks?
- He launches is an attack on terrorism, commenting on how compared to some
  other significant problems in the world, terrorism causes very few problems
  but has a massive amount of publicity.
- Of course, this is exactly what the terrorist wants and how terrorism works.
- The author will next discuss how terrorism came to this position of
  prominence.

## Part Three: Despair and hope

    Though the challenges are unprecedented and though the disagreements are
    intense, humankind can rise to the occasion if we keep the fear under
    control and be a bit more humble about our views

### Terrorism: Don't panic

- An attack on terrorism, commenting on how it is a show of weakness, not
  strength.
- In truth, casualties from terrorism barely register compare to casualties of
  real wars.
- In war, an attack is only made with the aim of reducing the enemy's ability
  to fight back.
- In contrast, terrorists know they can do no material harm and so seek to
  incite their victims into causing the harm themselves.
- A parallel drawn is to the attacks on Pearl Harbor – those by Japan were an
  act of war, disabling the American pacific fleet.
- A terrorist attack would have attacked an American passenger vessel and left
  the Pacific sleep intact to retaliate.
- Such an act would make no sense in war.
- Terrorism is likened to a fly in a china shop buzzing into a bull's ear so
  that the bull does the destruction (as the fly does not have the ability to do
  so itself.)
- The comfort and security afforded by modern society actually worsens the
  impact of terrorist attacks.
- In other words, we are so safe that acts of terrorism seem more shocking than
  they are.
- This is likened to a single coin in an empty jar making a large noise when
  rattled.
- The key, which is so often ignored, is to give terrorism as little publicity
as
  possible.
- A caveat is if terrorists gained weapons of mass destruction such as nuclear,
  biological our cyber weapons, they could truly inflict damage.

### War: Never underestimate how stupid humans are

- An initial discussion is made about how modern circumstances dictate that
  large-scale war would have a few material gains and significant material
  losses and is therefore a less appealing prospect than historical conquests.
- An exception to this is the recent Russian annexation of the Crimea.
- He also points out it is a little scary that other world powers have allowed
  this to be successful and states that he hopes it remain an exception.
- He concludes by saying that the most important thing to prevent future war may
  be simply some humility on behalf of major powers.

### Humility: You are not the centre of the world

- The author uses his own community/religion – Judaism – to pierce the inflated
  egos of people's belief in the importance of its own people.
- Despite important individuals and scientific discoveries attributable to them,
  human civilisation is unlikely to have been massively influenced by Jews or
  Judaism itself.
- The author believes that an appreciation of the insignificance of individual
  parts of humanity would be beneficial for all.

### God: Don't take the name of god in vain

- The author argues that there are two concepts of God, the cosmic entity which
  explains the unexplainable and the worldly lawgiver who tells men not to sleep
  with other men, for example.
- The author deliberately pours ridicule on the idea that these two concepts
  could be linked.
- Why would any cosmic entity responsible for the laws and creation of the
  universe have any concern for the behaviour of tiny apes on one of its
  planets?
- Personally, he is unhappy to hear references to god when they are used as an
  excuse for, for example, Islamic State's actions or the Christian Crusades.
- This is where he would reiterate the chapter title to the followers: don't
  take the name of god in vain; it is humans - **you** - who are causing these
  atrocities.
- Subsequently, he discusses that the moral benefits some religions lay down to
  their followers are entirely independent of religion itself and that good
  morals do not require god.

### Secularism: Acknowledge your shadow

- The author discusses secularism, based on features such as pursuing the truth,
  equality, freedom and responsibility.
- He is full of praise for secular dogma in the way that it both challenges and
  accepts, for example, religious dogma of their followers who wish to be
  accepted in secular societies.
- Regarding truth, he says that it is better to ask a question which you cannot
  answer than not to ask a question you can (i.e., because your religion forbids
  it.)
- However, even secularism has a shadow.
- The author says that you have to ask yourself how Marxism lead to the Gulags
  or how modern genetics can led to Nazism.
- Finally, the author says that he has some faith that secularism is not afraid
  to ask these questions of itself.
- Personally, he would ask of any dogma which sought to ascend in the modern
  world: what was your biggest mistake? If the answer is not serious, you should
  not trust that dogma.

## Part four: Truth

    If you feel overwhelmed and confused by the global predicament, you are on
    the right track. Global processes have become too complicated for any single
    person to understand.

### Ignorance: You know less than you think

- The author first dismisses rationality – human decisions are classically
  emotional, even amongst those who think themself rational – then dismisses
  individuality – group think is much stronger than appreciated.
- There is too much information in the world for anyone to know all of it and –
  in fact – the diversity of information means we know no less than we used to,
  instead relying on the knowledge of others.
- This is actually an advantage to some extent and has allowed us to progress to
  where we are.
- The difficulty raised is the comparison that if you are a hammer, the whole
  world looks like a nail and if you have power, the whole world looks like it
  needs your intervention.
- The author believes that new knowledge exists on the periphery whereas power
  resides in the centre.
- Those with power do not have the time to actually explore the periphery and
  therefore progress is slowed.
- Moreover, progress is diverted by agendas.
- He also believes this problem the increase in future and defences will be
  needed against this.

### Justice: Our sense of justice may be out of date

- A short chapter in which the author argues that the world is so complicated
  that it is nearly impossible to adhere to any strict moral code.
- Actions are so interconnected that even those which are well-meaning and which
  the person believes have no broader impact may be harmful to others.
- The author does not particularly offer a solution to this problem.

### Truth: Some fake news lasts forever

- The author argues that much of human history has been based on fiction, for
  example religion and money.
- It is this ability of humans to come together and accept these fictions that
  has allowed us to progress.
- The key to avoiding fiction in the modern world is to ensure that when ever
  you want reliable information, you are willing to pay for it rather than to be
  offered it by, for example, social media providers where you may indeed be the
  product rather than the new story.
- The author also concludes that due to the importance of the science fiction
  genre, it could be argued that scientists should invest effort in writing
  science fiction.

### Science fiction: The future is not what you see in the movies

- The author believe that most science-fiction is entirely devolved from
  science.
- He believes any science-fiction in which the antagonist is a robot identified
  as a woman is actually simply a man's view and fear of empowerment of women,
  for example.
- Two works of science fiction, which are terrifying in their accuracy, are
  praised.
- The film Inside Out, which on the surface appears to be a coming-of-age tale
  but when looked at deeply, you see that it actually shows that the protagonist
  really does not have a true self and is instead controlled by the interaction
  of her emotions.
- It also essentially denies free will.
- The second is the novel Brave New World, in which the protagonist John the
  Savage is so convinced that the biologically controlled world in which has
  happiness is the only pursuit is robbing him of his right to feel unhappy.
- The author argues that an attempt to escape this reality which he rejects is
  impossible because it is actually an attempt to escape from himself.
- His suicide is ultimate escape.

## Part Five: Resilience

    How do you live in an age of bewilderment when the old stories have
    collapsed, and no new story has yet emerged to replace them?

### Education: Change is the only constant

- The author argues that future world will be so unpredictable that education
  based on current fact is insufficient.
- He thinks, best thing we could teach children to be able to react to constant
  change.
- This is very difficult to achieve!
- The author states that the goal of corporations is to hack us, not our
  computers, so that they can sell more of their products by knowing usbetter
  than we know ourselves.
- Therefore, the best advice is to know yourself.
- That is, assuming you are unhappy with algorithms controlling you, your
  actions and your decisions.

### Meaning

- The chapter begins with a comprehensive takedown of structures, especially
  religion, which purport to ascribe meaning to life.
- How can there be any meaning, any afterlife, or significance to reincarnation
  when humans have only been around for approximately 2.5 million years, these
  religions have been around for much less time but the universe will continue
  for 13.5 billion years from today?
- Most life that has ever existed has left no trace of itself; no legacy.
- How will humans be different?
- The author launches an astonishing and uncompromising assault on all fictions
  which have attempted to convey meaning and demonstrates how unimportant they
  are.
- One example he provides is the willingness of the majority of German fascist
  both leaders and followers in giving up their beliefs and re-joining normal
  and tolerant society after the collapse of the Nazi party.
- Very few committed suicide or went insane after their supposedly all-
  encompassing belief system vanished.
- Every religion and religious text, every political dogma, every belief system,
  code of conduct and moral path has been written by the hand of a human.
- The universe has no meaning.
- Human fictions attempt to give meaning to the universe, to life.
- Even Buddhist beliefs which recognise this truth fall victim to the human
  desire for a narrative and even Buddhists commit atrocities in defence of
  their religion.
- Finally, the author again draws on a theme from Homo Deus: suffering is the
  ultimate judge of reality.
- Nations cannot suffer in war; soldiers can.
- If you want to look for meaning, seek to end suffering.

### Meditation: Just observe

- The author concludes his novel by again stating that there is a major
  difference between the brain and the mind.
- The mind remains essentially unexplored and unknown.
- In his opinion, and he stresses that experience may differ for other people,
  meditation is the only true way to explore the mind.
- He re-iterates that as humankind has become more complex, increasingly complex
  fictions have made it more difficult to explore one's true mind as instead,
  these preformed narratives present themselves.
- In the future, algorithms will take the place of these fictions and attempt to
  make decisions for us.
- If we do not take the time now to understand and know ourselves, in a few
  decades it may be too late to deconstruct the complex algorithms which have
  controlled us.

[Home](/blog "ohthreefive")