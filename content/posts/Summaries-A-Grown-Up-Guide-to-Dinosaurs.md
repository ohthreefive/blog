
+++
         
date = "2020-03-07T15:11:17.357Z"
title = "Summaries: A Grown Up Guide to Dinosaurs"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# A Grown-Up Guide to Dinosaurs

_Professor Ben Garrod_

## Episode One: What Is a Dinosaur?

- The series aims to recapture an adult's love of dinosaurs by updating them
  with all the new information that has been learned since this adult was a
  child.
- The opening episode discusses the origin of dinosaurs and the Triassic and
  early Jurassic periods.
- Dinosaurs were few in the Triassic but a mass extinction event at the end of
  that period allowed them to thrive and spread.
- The Jurassic would then be a period of survival of the fittest, which in this
  case usually made the largest.

## Episode Two: Size Matters

- This episode deals with the most massive group of dinosaurs: the sauropods.
- These peaked in the Cretaceous area of era.
- Two recently discovered contenders for the largest land dinosaur ever are
  Dreadnaughtus and Patagotitan, both found in Patagonia.
- Estimating their true size is still a matter of debate, with both dinosaurs'
  discoverers believing theirs is larger.
- There are theories as to what modifications or specialisations they had to
  achieve their massive size, including how they ate and how their lungs
  functioned, including getting rid of excess heat.
- The size of carnivorous dinosaurs in the Cretaceous period was likely a
  response to and effect of the massive prey.

## Episode Three: Feathered Freaks

- The evidence that birds are evolutionary ancestors of dinosaurs is now so
  strong that dinosaurs could more accurately be called split into dinosaurs and
  non-avian dinosaurs, such as the sauropods.
- How and when took flight is still not known but two requirements for flight,
  feathers and hollow bones, were present long before they took to the skies.
- Feathers have now been discovered on three completely separate dinosaur
  groups: the theropods such as velociraptor, the pterosaurs such as pteranodon
  and another group which included dinosaurs such as stegosaurus.
- Two theories are that either feathers are an inevitable evolutionary
  consequence, having arisen in three separate groups, or they were present in a
  common ancestor of these three groups.
- That second theory is currently favoured.
- The implication of this is that even the first dinosaurs were feathered.

## Episode Four: Who Needs Bones?

- The episode discusses all the things we have learned about dinosaurs from
  studying things such as their footprints, coprolite, scratches they made on
  rocks and their nests.

## Episode Five: What Is Not a Dinosaur

- An exploration of the sea-based animals, such as ichthyosaurs, plesiosaurs and
  and aerial animals: the pterosaurs.
- These animals were not dinosaurs and reached their peak long before the
  dinosaurs.

## Episode Six: The Fall of the Dinosaurs

- An explanation is provided as to how scientists discovered the asteroid impact
  which helped wipe out the dinosaurs.
- An explanation is also made about is magnitude – 10 billion times the force of
  the bombs dropped on Hiroshima and Nagasaki.
- More important is a discussion that dinosaurs were probably past their peak at
  the time and that there was ongoing climate change in the millions of years
  leading up to the asteroid strike.
- Another interesting point is that after this fifth mass extinction, the
  surviving dinosaurs – largely birds – probably outnumbered mammals 2-to-1 but
  the mammals managed to rise above them.
- I had previously assumed the asteroids took out dinosaurs and it was this
  which allowed the mammals to thrive.

[Home](/blog "ohthreefive")