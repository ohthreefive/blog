+++
         
date = "2021-06-05T20:15:48.839Z"
title = "Summaries: A Storm of Swords"
description = "The Last Summary"
tags = [ "Audible", "summary" ]

+++

# A Storm of Swords

**George R R Martin**

## Prologue

- The novel begins from the point of view of a mutineer (Chett) within the
  Night's Watch who plans to assassinate Lord Mormont and several other Watch
  leaders and escape from his vow.
- This is taking place during a great ranging north of the Wall, with the Watch
  camped at the Fist of the First Men.
- His plans are interrupted at the last minute when scouts sound a warning horn
  three times – three blows of the horn means 'Others'.

## Jaime

- Catelyn Stark releases Jaime (and his cousin Cleos Frey) under Brienne's
  protection in order to get her daughters back.
- Jaime and Brienne trade insults.
- He is witty and cunning; she is humourless and stern.
- A galley from Riverrun catches up to them: Brienne somehow climbs a cliff and
  sinks it with a boulder before diving back from the cliff into the river.
- Jaime shows a first sign of respect for her by helping her back in to their
  boat rather than clubbing her to death with an oar.

## Catelyn

- Catelyn Stark awaits her judgement in Riverrun.
- She talks with her ailing father, who babbles about both about a person called
  Tansey and possibly a lost child.
- This unsettles Catelyn.
- Her brother Edmure returns from battle, declaring he has put a price on the
  safe capture and return of Jaime.
- This immediately puts Catelyn's desperate plan in serious jeopardy as she does
  not expect Brienne to make it all the way to King's Landing with Jaime if they
  are being actively sought.

## Arya

- Arya, with Hotpie and Gendry, are on the run from Harrenhall.
- Perused by Vargo Hoat and the Brave Companions/Bloody Mamas.
- Gendry behaves stubbornly, Hotpie stupidly and Arya tries to be as brave and
  clever as her mentors Jaqen H'gar and Syrio Forel.
- While dreaming, Arya wargs in to (presumably) Nymeria and sees her and a pack
  of wolves attack and kill the pursuers.
- Arya is still unaware of her warging.

## Davos

- Ser Davos survived the battle of the Blackwater and is stranded on an island
- he has been struggling to survive, and for the will to survive, having lost
  sons in the battle.
- He eventually decides to continue to live in order to gain revenge on
  Melisandre, whom he blames for the catastrophic defeat.
- Her worship of the 'Lord of Fire' also grates with him, as it was wildfire
  that caused the devastation of Stannis' fleet.

## Tyrion

- Tyrion is still recovering from his serious injuries via attempted murder at
  the Battle of the Blackwater.
- He learns from his mercenary Bronn that the majority of the other hired swords
  have been paid off and left King's Landing.
- The Tyrell family has moved to King's Landing to great local welcome.
- Margaery Tyrell is to marry Joffrey.
- As his position has been severely weakened (his father has taken the role of
  Hand of the King), Tyrion wants to stop wasting time in his sick bed and get
  back in the game.
- In conversation with his father, he is frustrated to receive very little
  praise for his part in the victory and has his claim as the heir of Casterly
  Rock utterly rejected.
- He correctly realises that this is because he has still been using a
  prostitute, although the one that Cersei captured was not his real mistress.
- Tywin says he will hang the next whore Tyrion is found with.

## Sansa

- Sansa Stark is invited to dinner with Margaery and the Tyrells.
- Initially wary, she unknowingly falls in to their plan which is to protect
  Margaery and remove Sansa from the Lannisters by marrying her to Margaery's
  brother, Willis.
- She meets Margaery's grandmother, Olenna, the real brains of the Tyrell family
  and as accomplished at scheming as the Lannisters or Varys, or at least as
  keen on it.

## Jon

- Jon Snow is led to the encamped Wildlings/Free Folk.
- He discovers the horde to be enormous but disorganised.
- He meets the leader, Mance Rayder, and some other chieftains of the gathered
  peoples.
- With some flair, he convinces Mance that he is truly a deserter, saying his
  bastard status at Winterfell had embittered him towards the establishment.

## Daenerys

- Daenerys Targaryen is leading her small khalasar from Qarth to Pentos by sea.
- This is significant: her followers the Dothraki hate the sea but are willing
  to cross it for her.
- Daenerys' three dragons are described as still small and essentially harmless
  but growing all the time.
- During the crossing, Jorrah Mormont voices his suspicions about the two new
  companions, Strong Belwas and his squire Arstan.
- He particularly mistrusts Arstan, whom he sees as too old and noble to be a
  squire (Arstan is truly Ser Barristan Selmy.)
- Jorrah is clearly worried about others influencing Daenerys, namely the two
  newcomers and Ilyrio Mopatis who sent them.
- Arstan also hints that Jorrah has secrets.
- Jorrah convinces Daenerys not to go to Ilyrio in Pentos but to redirect to
  Slaver's Bay first be in order to acquire an army of the highly organised
  eunuchs known as the Unsullied.
- He then says he wants to ride a dragon alongside Daenerys and allows his true
  affection for her to come out.

## Bran

- Bran, Meera, Jojen and Hodor continue their escape from Winterfell.
- Bran continues to inhabit his direwolf Summer in his dreams but Jojen
  chastens him that that he is exerting no control over the wolf and he should
  aim to do so.
- Jojen tells Bran that all he can do is dream about the future, whereas Bran
  will be able to do much more with his powers.
- It is for this reason they should seek out the three eyed raven, who will be
  able to instruct Bran better than Jojen ever could.
- Bran is tempted to command the Reedes to go to the castle of a house Stark
  ally, however the urge to, "Fly", the promise made to Bran by the three eyed
  raven, is too strong for the crippled boy and he decides they should all
  travel north of the Wall, as Jojen wants.

## Davos

- Davos returns to Dragonstone and meets with Salladhor Zhan.
- He confides in Zhan his plan to kill the red woman Melisandre.
- Salladhor Zhan tries hard to discourage him.
- First, he mentions that the power of Stannis Baratheon and has waned and says
  Davos could return to work as a smuggler, working with Zhan.
- Furthermore, he draws attention to Davos' living relatives, a wife
  and two other sons, and says he should return to them.
- Finally, he attempts to scare Davos by saying that Stannis only takes counsel
  from her and she holds great power now.
- Her followers on Dragonstone are
  everywhere and she seems to know and hear everything.
- She will likely kill
  Davos before he has a chance to kill her and, if Davos does manage to kill her,
  he will be killed in an act of vengeance by her followers.
- Davos, however, reiterates his loyalty to Stannis, reiterates that he blames
  Melisandre for sending the fire to the Battle of Blackwater Bay and also tells
  Salladhor Zhan that it was one of Davos' gods, the Mother, who sent the ship
  to pick up Davos.
- The island on which he was stranded is out of the usual way for large vessels
  and he thinks it was no coincidence. He therefore sees a divine reason to him
  killing Melisandre.
- He also regrets betraying his gods by allowing Melisandre to burn effigies of
  them earlier on Dragonstone.
- Salladhor Zhan is angry at Davos' belligerence and they part on unfriendly
  terms.
- Davos travels to the castle and is shown to a garden, in which he meets king
  Stannis' daughter Shareen and late king Robert's bastard son, Edrick Storm.
- He briefly conversers with Axel Florent, who is loyal to Melisandre.
- Florent declares that he knows Davos plans to kill Melisandre and arrests him.

## Jaime

- Jaime, Brienne and Cleos Frey continue their journey to King's Landing.
- They come across an inn, inhabited by an unknown man rather than the
  innkeeper, who has died/been killed.
- We learn about the effects of the war on the poor, with villages burned etc.
- They converse with the man and buy three horses from him.
- Upon leaving, Jaime is for the first time impressed with Brienne's
  intelligence as she deliberately travels a path different to that which the
  man in the inn suggested, realising he is likely to set an ambush for them.
- He also refers to Brienne by name for the first time.
- When challenged by Brienne about his breaking of his vow as one of the king's
  guard, he recalls the last days of the Mad King Ares.
- We learn that Sir Illyan Payne lost his tongue for saying that Tywin Lannister
  was the true ruler of the seven kingdoms.
- We learn that Jaime joined the Kingsguard to avoid an arranged marriage and
  to spend more time with Cersei in King's Landing.
- However, upon his appointment to the Kingsguard, Tywin resigned his position
  as King's Hand and took his daughter back to Casterley rock with him.
- The chapter ends with Jaime thinking little of Brienne's hatred of him due to
  her naïveté.

## Tyrion

- Tyrion pays Varys visits learns - unsurprisingly - that he
  knows everything that is going on in King's Landing.
- They engage in a conversational back and forth, during which Tyrion learns that
  both his sister and father remain dangerous to him.
- Varys maintains no single allegiance.
- In the end, Tyrion realises he must trust Varys with one task and that is to
  bring Shea to him for the final time.
- He has realised that Shea must not remain in King's Landing for her own
  safety.
- On his way to Shea, Tyrion briefly encounters Sir Loras Tyrell, who he finds
  impulsive and quick to anger.
- His meeting with Shea does not go to plan and he ends up promising to protect
  her.
- The chapter concludes with him meeting Bronn, now a knight, and seemingly
  less in need of Tyrion than he was before.
- He asked Bronn to bring the singer of Simon (a friend of Shea's) to him,
  seemingly for Simon's own safety.

## Arya

- Arya, Gendry and Hotpie continue their travels towards Riverrun.
- They are discovered and essentially captured by three outlaws.
- Although they are not named, I think these outlaws belong to the Brotherhood
  without Banners; they say they are loyal to Robert.
- They are taken to an inn where the men say they need to take their horses.
- We learn that the innkeeper is the same who tried to trick Brienne, Jaime and
  Cleos into a trap earlier.
- At the end of chapter, another party of the outlaws arrive, one of whom is a
  man of Winterfell, to whom Arya are openly reveals herself.

## Catelyn

- Robb Stark returns to Riverrun.
- He greets his mother with a strange forgiveness for her letting Jaime
  Lannister go.
- Quickly, we learn that this is because he has reneged upon his vow to Walder
  Frey and has married a girl called Jayne Westerling.
- In response, the thousand knights of the Frey's have left his side.
- We also learned that at his wife's behest, he no longer keeps his direwolf by
  his side.
- Catelyn is dismayed by these developments.
- At the end of chapter, Robb, Brendon and Edmure Tully discuss tactics with
  Catelyn.
- The extent of Edmure's failure is revealed, as his holding his position was
  designed to pull Tywin Lannister away from the main fighting of the war.
- Instead, despite defeating him, he freed the Lannister's to aid in the defence
  of King's Landing.
- They decide it is essential to win back the friendship of the Freys and at
  the end of chapter, Catelyn appears to realise that one of the ways to do this
  would be to offer Edmure in marriage to one of his daughters.

## Jon

- Jon is shown around the wildling camp by Tormund and sees giants, mammoths
  and other creatures.
- He is mocked for not having made a move on Egret.
- He sees a warg called Varamyr Sixskins and is immediately afraid of him.
- They travel to the Fist of the First Men, and find that White Walkers have
  attacked the Night's Watch there.
- On the way, Jon is attacked by the eagle of another warg he killed and nearly
  loses an eye.
- At end of chapter, Jon and Egret get together.

## Sansa

- Queen Cersei commissions a dress for Sansa.
- Sansa is conflicted, desperate for the beautiful gown but still fearful of the
  Lannisters.
- She wants to marry Willis Tyrell as soon as possible and escape to High
  Garden.
- Her secret protector, Ser Dontas, wants her to stick to his plan of escape on
  Joffrey's wedding night.
- Sansa does not know what is the right thing to do.

## Arya

- Arya and Gendry are being led by the outlaws they encountered.
- Arya is convinced they are heading south, not north.
- The outlaws explain that Tywin Lannister's plan was to lure Ned Stark out of
  King's Landing with the Mountain, but Jaime, unaware of the plan, broke his
  leg.
- Beric Dondarrion went in his stead.
- Most of the party were killed, and Beric was impaled by Gregor Clegane with a
  spear.
- He was revived by Thoros of Myr, and appears stronger after his resurrection.
- This was his fourth death and second at the hand of the Mountain.
- The outlaws serve the throne but not Joffrey.
- They tell Arya that their number has been increasing through volunteers from
  the common folk.
- Arya learns from one of them that Jaime has been set free.
- She is told that she is not being taken to Riverrun but to Beric Dondarrion,
  who will decide her fate.
- On hearing this, Arya tries to escape but fails.

## Samwell

- The Night's Watch barely escape an attack by wights at The Fist of the First
  Men.
- Sam fails in his only appointed task: to send a raven.
- An Other attacks two of Sam's brothers; Sam summons the courage to stab it with
  dragonglass/obsidian and it melts.

## Tyrion

- Tyrion attends a small council meeting where Tywin discusses war tactics.
- Bailish will go to the Eyrie and try to woo Lysa Arryn and secure the Vale as
  allies.
- Tywin is confident about the war; Tyrion is suspicious as to why.
- At end of meeting, Tywin dismisses all but his family.
- He tells Cersei she will marry Willis Tyrell and Tyrion he will marry Sansa
  Stark.
- This will secure Highgarden and Winterfell for the Lannisters.
- It will also stop the Tyrell's plan to marry Sansa to Willis.
- Tywin reveals he has learned of Robb Stark's marriage to Jayne Westerling,
  meaning Stark will have lost the Freys.

## Catelyn

- Lord Rickard Karstark and a group have killed a Lannister and Frey captive as
  recompense for the killing of two of the Karstark children by Jaime Lannister,
  who has subsequently been set free by Catelyn.
- Naturally, she feels responsible for the deaths of these two young squires,
  and sees in their bodies the faces of her sons Bran and Rickon on.
- Lord Karstark compounds his treason by questioning Robb's leadership and sense
  of justice.
- Robb personally executes him for treason, musing on how difficult ruling is as
  he has just killed a powerful ally because that ally killed two of his
  enemies.
- The Karstark men and knights abandon Robb as he predicted.
- He expresses his frustration that his aunt Lysa Arryn has not responded to his
  plees as the Knights of the Vale would be powerful allies and could swing the
  tide of the war.
- The chapter concludes with Catelyn questioning her legacy, knowing that her
  husband is dead, father is dying, two sons are dead and both daughters
  missing; Robb is all that is left to her.
- She counsels Jayne Westerling to provide an heir as soon as possible.

## Jaime

- Jaime and Brienne continue travelling, coming across an entirely ruined town
  called Maidenpool where the dead are strewn around and clog the river.
- Unseen archers fire upon them from the trees and succeed in killing Cleos
  Frey.
- Jaime manages to get his hand on his cousin's sword and attacks Brienne with
  all his skill, fury and strength.
- Despite his prolonged time in captivity and the fact he is fighting double
  handed due to his chains, he expects little resistance but finds Brienne both
  in swordsmanship and in strength his equal.
- In fact, she bests him.
- The noise attracts a group called the Brave Companions, or the Bloody Mamas to
  outsiders, who announce that they have changed allegiance from house Lannister
  to house Bolton and will therefore bring Jaime and Brienne to Roose Bolton.
- Jaime makes attempts to ensure Brienne is not raped by their captors.
- His internal thoughts suggest he would have kept true to his word to return
  Catelyn Stark's daughters.
- He attempts to bargain with Vargo Hoat, and for his trouble his right hand is
  chopped off.

## Arya

- Arya and Gendry continue their travels with the Brotherhood Without Banners,
  looking for Beric Dondarrion.
- It is explained that the Lightning Lord does not share his plans openly and
  that his followers do not congregate en masse, for all of their protection.
- We hear to tales of Beric Dondarrion's death, once by hanging and once by
  being stabbed through the eye by the Mountain.
- His companion, Thoros of Myr, is said to have healed him as the Lightning Lord
  is reportedly difficult to kill.
- Gendry tells Arya that the flaming sword Thoros carries is just an alchemist's
  trick, being simply an ordinary sword covered with wildfire.
- During their travels, they learn bits and pieces about the war, including that
  there are Northmen searching for the Kingslayer and that apparently he was
  released rather than escaped, a fact which shocks both Arya and the
  Brotherhood.

## Daenerys

- Daenerys is in Astapor, a slave city with giant pyramids, one of which is
  topped with a bronze harpy statue.
- A slaver is showing her the Unsullied, eunuch soldiers trained from birth and
  fearsome.
- She is hostile towards the idea of slavery (her brother's forces fought out of
  love for him) but thinks she needs an army.
- Jorah Mormont reminds her that her brother lost and died.
- Daenerys has noted that Astapor is poorly defended but Jorah points out that
  the fear of the Unsullied is one of the reasons (the other being that the
  slave trade is revered in the East.)
- Arstan Whitebeard (actually Barristan Selmay) is appalled by the slaves.
- The slaver does not realise Daenerys can speak Valyrian and is highly
  insulting to her.

## Bran

- Bran is continuing to travel off the King's Road to the Wall with Jojen,
  Meera, Hodor and Summer.
- Meera tells him a story featuring a she wolf and dragon prince.
- Jojen is surprised to hear that Bran has not heard the story before.

## Davos

- A prisoner for his attempt on Melisandre's life, Davos is surprised to find
  that he is fed and kept alive, regaining his strength.
- He has a visit from Melisandre, who blames her not being at the Blackwater
  for their defeat.
- She tells Davos that Stannis is Azor Ahai, the promised champion of light.
- The prophecy mentions making dragons of stone, which Melisandre believes can
  be done on Dragonstone, but may actually refer to Daenerys.
- He is joined in his cell by Stannis' former hand, who has been imprisoned for
  offering terms to Tywin Lannister without consulting Stannis.

## Jon

- Jon is travelling with Egret and the Thens towards the Wall.
- He sends Ghost away.
- He is torn between his vows to the Night's Watch and his promise to Qhorin; he
  does not think he is a traitor.
- He is falling in love with Egret.
- Close to the Wall, Egret finds a cave and half-jokingly suggests they abandon
  all duties and just stay there together.

## Daenerys

- Daenerys comes to an agreement that she will sell the slavers one dragon for
  the entirety of the Unsullied.
- However, the plan was a trap and as soon as she has secured the services of
  the Unsullied, she turns her dragons on the slavers.

## Sansa

- Cersei Lannister surprises Sansa by revealing she is to be married to Tyrion.
- Her dreams of marrying Willis Tyrell are over.
- To her surprise, Tyrion refuses to consummate their marriage until Sansa
  consents.

## Arya

- Arya and Gendry continue to be led by the Brotherhood Without Banners.
- She sees and hears some confusing things – men loyal to her brother are being
  tortured for participating in pillaging and rape; there is a rumour that her
  mother set the Kingslayer free.
- In the morning, she sees that a local outlaw called 'the great huntsman' has
  made a significant capture.

## Jon

- Jon climbs the Wall with Ygritte and another 10 Wildlings.
- More than half fail to make the claim.
- At the top, Ygritte regrets that the Wildlings never found the Horn of Winter,
  which would bring the Wall down.

## Jaime

- The Brave Companions lead Jaime and Brienne back to Harrenhal.
- They discover the outcome of the Battle of the Blackwater and that Robb Stark
  betrayed the Freys.
- Jaime meets Maester Qyburn, who sees to his amputated sword arm.

## Tyrion

- Tyrion inspects the great damage to the city which he will have to find the
  money to repair.
- He laments his difficulties with both Sansa and Shea.
- His father shows him two swords he has had forged from Ice, Nedd Stark's
  greatsword.
- News from the Wall arrives of the Wildling threat.
- Tywin is dismissive, instead seeking a way to control the Night's Watch.
- He wants Tyrion to get Sansa pregnant to help secure the north, and seems to
  expect Jaime's safe return.

## Samwell

- All hell breaks loose at Craster's Keep when the Watch's discipline finally
  breaks as Craster continues to deny them food.
- Craster is killed, as is Lord commander Jeor Mormont.
- Many of the Night's Watch break their vows.
- Sam is approached to take Gilly and her newborn son to safety – the woman who
  approached him say that his brothers will be coming for him soon.
- The implication is that the male children that Craster sacrifice to the gods
  have been turned into Others.

## Arya

- The Mad Huntsman's captive is none other than the Hound, Sandor Clegane.
- He is to face trial by combat against the Lightning Lord, Beric Dondarrion.
- Despite the flaming sword of the new disciple of the Lord of Light terrifying
  the Hound, he manages to win the duel.
- Arya is devastated and makes to kill him with a stolen dagger but the
  resurrected Lightning Lord stops her.

## Catelyn

- Catelyn buries her father and worries for the strength of character of her
  brother, Edmure.
- Robb tells her he has learned Sansa has married Tyrion, presumably to secure
  Winterfell should Robb die.
- Catelyn is distraught - she has no word as to how Cleos Frey (dead) and
  Brienne are progressing in their attempt to exchange Jaime Lannister for the
  Stark girls - and now she learns that one of the girls may no longer be
  available to trade.
- A Frey informs the Starks that Winterfell was burned being retaken from the
  Iron Men.
- Ramsay Snow, Rose Bolton's mad bastard, was responsible.
- This news worries Robb and his mother.
- Walder Frey will accept a marriage to Edmure for one of his daughters, and a
  personal apology from Robb, to make up for Robb's betrayal.
- Catelyn considers the personal nature of the apology to be a distraction but
  the marriage acceptable.

## Davos

- Davos is unexpectedly summoned from the dungeon for a meeting with the King,
  in which Stannis tests him then raises him to Lord and Hand of the King.
- Then, with the aid of leeches that have sucked the blood of Edrich Storm, he
  and Melisandre perform a ritual in which Stannis name is Joffrey, Balon
  Greyjoy and Robb Stark as usurpers.

## Jaime

- Jaime recounts the stockpiles of wildfyre that the Mad King Ares planned to
  use, in his paranoia, to destroy King's Landing rather than lose it to Robert.
- He wants Brienne to call him Jaime, not 'Kingslayer'.
- They dine with Rose Bolton, during which Jaime realises the Northman is going
  to send him back to King's Landing but keep Brienne.
- Jaime senses Bolton's loyalty is for sale.

## Tyrion

- Tyrion rides to meet Prince Doran Martell of Dorne but instead meets his
  dangerous and vengeful brother, Oberyn.
- Tyrion can see little good of the Red Viper coming to King's Landing.

## Arya

- Arya watches the Brotherhood Without Banners decimate a Brave Companions
  raiding party.
- She, Thoros of Myr and Beric Dondarrion and discuss his coming back from the
  dead.
- Gendry asks to be smith for the Brotherhood.
- Arya feels abandoned: by many of her allies to death and now by Gendry to the
  Brotherhood.
- The Hound returns, asking for his gold back from the Brotherhood; Beric
  realises it is all he has left.

## Bran

- Bran, Meera, Jojen, Hodor and Summer continue to make their way through the
  New Gift towards the Wall.
- They shelter in an abandoned village but a group of men arrive and a
  thunderstorm starts.
- Bran wargs into Hodor – the first time he has done that to a person – to get
  him to stop shouting in the storm.

## Jon

- The group of men Bran could see were Jon and the Wildlings.
- Jon still feels conflicted with regards to his allegiances.
- During the thunderstorm, an unexpected attack by Summer, though he did not
  recognise the wolf, allows Jon to escape, though with an arrow in his leg
  which he thinks may have been from Ygritte.

## Daenerys

- Daenerys and her armies are on the outskirts of Yungkai, a better prepared
  slave city then Astapor.
- She meets with the sellsword defenders and offers them terms of surrender.
- Later, one of the Stormcrows captains, Daario Naharis, returns with the heads
  of the other captains and promises his allegiance.
- Danny takes the city later that night, with Mero the Titan's Bastard and
  captain of the Second Sons, having fled.
- When she enters the city, the slaves greet her as a 'mother'.

## Arya

- Still travelling with the Brotherhood, they encounter a witch - possibly a
  Child of the Forest - who names Arya a bringer of death.
- Arya meets a noble boy who tells her that he was milk brother to Jon snow
  with a woman called Wylla.
- He also tells her of her father's affairs with Asmara Dayne, sister of Sir
  Arthur Dayne.
- Thoros has a vision of a Lannister attack on Riverrun.
- A distraught Arya makes to escape but is caught by the Hound.

## Jaime

- Jaime has an attack of conscience and returns, with his Bolton escort, to
  rescue Brienne from Harrenhall and the Brave Companions.

## Catelyn

- Making their way to the Twins for Edmure's wedding, Robb Stark receives news
  of Balon Greyjoy's death and the return of his Exiled brother, Euron Greyjoy.
- Given the expectation that Victarion Greyjoy and Balon's daughter Asha will
- return from the North to contest the throne, Robb plans and attack to reclaim
  the North.
- He intends for his mother to be kept safely away and agrees that should he
  die, Jon Snow should be legitimised and the new head of house Stark.

## Samwell

- Sam and Gilly are slowly making their way to the Wall when they are attacked
  by Wights, many of them former companions of Sam's.
- They are saved by a man with black skin and hands as cold as ice.

## Arya

- Arya continues to travel with the Hound, and eventually he tells her that his
  plan is to return her to her mother at the Twins, not to the Lannisters.

## Catelyn

- The Starks arrive at the Twins for the wedding and find a slightly warmer
  welcome than me might expected.
- Catelyn has some suspicions.
- Roose Bolton is there and delivers them more news about Winterfell and Theon
  Greyjoy.

## Jon

- Jon manages to make it back to Castle Black, severely injured, and raises the
  alarm about the Wildling attack.
- He hears the news of the tragedy at Winterfell.

## Arya

- Arya and Sandro Clegane reach the Twins.
- She looks for friendly faces or Stark loyalists, but struggles to make out
  banners in the dark and wet.
- Clegane means to deliver her to Robb Stark and Robb Stark alone.

## Catelyn

- At Edmure Tully and Roslyn Frey's waiting, the traitorous Freys and Boltons
  have conspired to have Robb and Catelyn assassinated and the Stark army
  destroyed.

## Arya

- Arya and the Hound arrive in the midst of battle.
- Arya is confused but the Hound immediately realises what is going on.
- He advises her to flee with him but she runs for the castle instead, forcing
  him to run her down.

## Tyrion

- Tyrion hears his fathers master plan: with Robb Stark dead, he will marry
  Roose Bolton's son Ramsay to a fake as Arya Stark and use the Boltons to fight
  the Iron Born and hopefully bring more Northmen under their banner.
- By this stage, Tyrion should hopefully have impregnated Sansa, bringing
  Winterfell under Lannister rule.
- He plans to frame a dead knight for some murders of Dornish children during
  Robert's Rebellion, for which the Red Viper is seeking revenge.
- During discussions, Tyrion had, only partly in jest threatened King Joffrey.

## Davos

- Davos, Stannis, Melisandre and others argue over whether the deaths of Robb
  Stark and Balon Greyjoy are enough evidence to allow the sacrifice of Edric
  Storm.
- Davos manages to argue that As Joffrey is still alive, nothing is proved.
- While learning to read, he reads the letter from the Night's Watch, warning of
  a Wildling attack and asking for help.

## Jon

- The Then-led Wildlings attack Castle Black from the south in the night.
- Jon helps lead a successful defence, though significant portions of the stair
  have to be burnt to keep the Wildlings from pursuing the Watch up the Wall.
- Jon finds Ygritte dying in the yard but is relieved to see that it is not one
  of his arrows through her chest.

## Bran

- Bran, Meera and Jojen spend the night in the Night Fort, though Bran insists
  there is no way through the Wall from within it.
- Sam and Gilly appear from the bottom of a deep set of stairs, having been
  saved by 'Cold Hands', who said Sam had to guide someone back through the Wall
  but he could not pass the Wall himself due to its magic.
- Sam leads them through an ancient Werewood.
- Bran asks Sam not to tell Jon, if he is even alive, that Bran has gone north.

## Daenerys

- Daenerys prepares to take Meereen, a city much bigger than Astapor and
  Yungkai.
- As she makes her plans, she is attacked by the Titan's Bastard who has been
  hiding within her slaves.
- Arstan, the old man, kills him with just a staff and is revealed to be
  Barristan Selmy.
- In turn, he reveals Jorrah Mormont to have initially worked as a spy for
  Robert Baratheon.
- Feeling betrayed by these two men, Dany is distraught and asks them to leave
  her.

## Tyrion

- Tyrion considers his various predicaments after having told Sansa what
  happened to her brother and mother.
- His first thought is that he might marry Shea to a knight.

## Sansa

- On the morning of Joffrey and Margaery's wedding, a nervous Sansa tries to
  ensure her husband does not speak out of turn and that she can get through the
  day with as little pain as possible.
- Tyrion thinks he has learned that Joffrey sent the assassin to kill the
  wounded Bran Stark.

## Tyrion

- Tyrion suffers humiliations at his nephew's wedding.
- Joffrey, however, dies, appearing to choke on a pie.
- Cersei instead tells the Kingsguard to arrest Tyrion and Sansa for his murder.

## Sansa

- Sansa is spirited away from King's Landing and learns that the architect of
  both her escape and Joffrey's murder has been Petyr Baelish.

## Jaime

- Jaime returns to Kings Landing.
- He has Brienne imprisoned to protect her from Lyra's Tyrell (she does not see
  the purpose in his actions and he is frustrated).
- He asks Cersei that they go public with their relationship but she rejects the
  idea.
- His father then tells him he is to be released from the Kingsguard to become
  true heir to Casterly Rock and also explains his plans for tactical marriages.
- In growing frustration, Jaime says he wants to remain Lord Commander; Tywin
  disowns him.

## Davos

- Knowing that Joffrey is dead, Davos arranges for a Edrick Storm and to be
  taken from Dragonstone.
- He confesses as much to Stannis and also tells him news from the Wall.

## Jon

- Jon helps successfully defend the Wall against the first wave of attack by the
  Wildlings but the main gate is severely damaged.
- Maester Aemon him the right man to lead the defence for now.

## Arya

- Arya feels lost without her family, so lost that she cannot even bring herself
  to run away from the Hound.
- Lingering uncertainty about her mother's fate is dissipated by a wolf dream in
  which she sees her corpse.
- Eventually, the Hound decides he wants to try to ransom her to the Blackfish
  at Riverrun, given that going to her aunt at the Erie would be too difficult
  in this weather.

## Tyrion

- Tyrion has his trial but is hopelessly outmanoeuvered by his sister.
- Bronn refuses to be a champion.
- At the last minute, with the chance to kill the Mountain, the Red Viper agrees
  to be Tyrion's champion.

## Jaime

- Jaime resumes his role as Lord Commander, perusing the history and feeling
  inconsequential besides Barristan the Bold.
- He spends time talking to each of the members of the Kingsguard about how they
  might keep Tommen safe.
- He saves the most time for Loras Tyrell, with whom he discusses the evidence
  against Brienne having killed Renly.

## Sansa

- Sansa arrives at Littlefinger's ancestral home; he pities and despises the
  place.
- He plans to wed her aunt, Lysa Arryn, and hide her in the Vale as his bastard
  daughter Elayne.
- He explains it was actually Olenna Tyrell who murdered Joffrey, seeing Tommen
  as a better King for her granddaughter.
- He also explains that the three Kettleblack boys, one of whom has made it into
  the Kingsguard, were originally in his pay.
- When Lysa arrives, she insists on marrying Petyr immediately, going against
  his wishes for a more public and legitimate wedding.
- Petyr tells Lysa who Sansa really is; Lysa wants to we Sansa to her son Robin.

## Jon

- Day after day, Jon defends the Wall against the Wildlings.
- Alliser Thorne returns from East watch with Janos Slynt to try Jon for oath-
  breaking.

## Tyrion

- The Crown's last witness is Shea, who humiliates Tyrion.
- In response, he confesses guilt, not to killing Joffrey, but to being a dwarf.
- He chooses trial by combat.
- Prince Oberon Martell tactically humiliates the Mountain, coping well with the
  danger of the huge man and pinning him to the ground with a spear through his
  chest.
- However, while desperately trying to get a confession from Gregor, he gets too
  close, allowing the Mountain to grab him and kill him before dying himself.

## Daenerys

- Daenerys has taken the great city of Mereen.
- She receives an envoy from Astapor, where a butcher to a former slave has
  taken the throne and wishes her hand in marriage.
- News from Yunkai is that some slaves have voluntarily gone back into
  servitude.
- She judges Barristan and Jeor, pardoning the former and exiling the latter.
- She resolves to stay and rule in Mereen, rather than to conquer and move onto
  Westeros.

## Jaime

- Tywin Lannister has decreed that the Mountain should be healed of his wound so
  that he can be executed by the crown for killing a bystander during his dual
  with the Red Viper.
- He wants to Robb Dorne of its defeat of Gregor.
- Jaime meets with Cersei but she rejects him when he refuses to plead for her
  not to be married off by their father using him agreeing leaving the
  Kingsguard as a bargaining tool.
- He meets with Brienne and explains to her that a false Arya Stark will be
  married to Roose Bolton's now-legitimised son and there is no one left to be
  able to contradict her veracity.
- He gives Brienne his sword, naming it Oathkepper, having learnt that he is
  useless at fighting with his left hand.
- He says that she must now try to find Sansa Stark and keep her safe from his
  sister.

## Jon

- As punishment before trial for Oathbreaking, Jon is sent to respond to a
  request for parley by Mance Rayder, but Janos Slynt and Alisser Thorne
  actually want him to kill the King Beyond the Wall.
- Mance tells Jon that he has the Horn of Winter but does not want to use it as
  he needs the Wall to protect his people from the others.
- However, if the Night's Watch do not allow them to pass in three days, the
  horn will be blown.
- Before Jon can decide what to do, the Wildling army is set upon by that of
  King Stanis.

## Arya

- Arya and the Hound get in a tavern fight with some of the Mountain's men,
  which the Hound manages to win but is badly injured.
- Arya recovers her sword Needle and leaves the injured, feverish Hound to his
  fate.
- She finds a harbour and surprises a Braavosi sailor with the coin given to her
  by Jaqen H'ghar.
- He treats her with unexpected respect when she presents the coin and says the
  words, "Valar morghulis."
- "Valar dohaeris"

## Samwell

- Samwell, Gilly and her son made it back to the Wall after being picked up by
  the Night's Watch and Stannis' army.
- Sam sees an opportunity to prevent Janos Slynt coming out on top of the
  Choosing of a new Lord Commander.
- He hopes to send Gilly to his home and parents, for her safety.

## Jon

- Stannis offers to legitimise Jon so that he might rule Winterfell and bring
  the North to him.
- Jon is highly tempted.
- However, when he says he plans to marry Jon to Mance's wife's sister, Jon
  starts to have doubts.
- He asks for time.
- Stannis plans to have Mance executed for deserting the Night's Watch.

## Tyrion

- Jaime comes to spring Tyrion from jail, citing his guilt over lying about
  Tyrion's first wife.
- The betrayal enrages Tyrion enough for him to falsely confessed to murdering
  Joffrey and swearing revenge on Jaime.
- Varys means to spirit him east on a ship but before Tyrion will leave, he goes
  to see his father, whom he discovers has been keeping Shea as his whore.
- He strangles Shea and shoots Lord Tywin in the belly with a crossbow, killing
  him.

## Samwell

- King Stannis asks much of the night's watch: firstly, that they pick the Lord
  Commander soon; secondly, that they give over their abandoned castles to him
  and thirdly, that they give over the Gift to him.
- Maester Aemon inspects the sword Lightbringer and notices that although it
  produces light, it produces no heat.
- Sam manipulates to front runners for Lord Commander to try to combine their
  vote away from Janos Slynt and towards Jon.

## Jon

- Jon venture of North of of the Wall to consider his options.
- Unexpectedly, he reunites with Ghost.
- When he returns, he discovers his name has been put in for Lord Commander and
  that evening, he is chosen.

## Sansa

- In a fit of jealousy, Lysa threatens Sansa near the Moon Door.
- Littlefinger arrives in time to try to calm her, but in her madness she
  reveals that it was Littlefinger's idea to poison Jon Arryn with the Tears of
  Lys and to write to Catelyn Stark, claiming to suspect the Lannisters of the
  murder.
- Littlefinger pushes her out of the Moon Door, intending to frame a court
  singer for the murder.

## Epilogue

- A Frey goes to pay the ransom to the Brotherhood Without Banners for one of
  his kin.
- Instead, he is hanged, but not before seeing the re-animated corpse of Catelyn
  Stark, who appears to give orders to the Brotherhood.

[Home](/blog "ohthreefive")