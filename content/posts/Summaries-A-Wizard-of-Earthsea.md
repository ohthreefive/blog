+++
date = "2020-01-02T20:54:02.579Z"
title = "Summaries: A Wizard of Earthsea"
description = "A summary to help me remember what I read better"
tags = [ "audible", "summaries" ]

+++

# A Wizard of Earthsea

**Ursula K. Le Guin**

> *"Only in silence, the word.*
> *Only in dark, the light.*
> *Only in dying, life.*
> *Bright, the hawk's flight on the empty sky."*
> The Creation of Ea


## Chapter 1: Warriors in the Mist

- A narrator tells us the story will be about a famous wizard called Sparrowhawk
  but it will not be about his deeds after his fame but about his life before.
- He was born in a village with some notoriety for producing wizards and his
  innate power was noticed by his aunt, a witch, when he was young.
- She was not particularly talented but took it upon herself to teach him.
- When the village was attacked by raiders, the boy, called Duny, shrouded the
  village in mist allowing them to survive the attack.
- When the tale of this deed spread, a mage (Ogion the Silent) came to visit and
  advised that the boy would need to be given his adult name soon and on that
  day, the mage returned and offered to take him for teaching.
- The name given was Ged.

## Chapter 2: The Shadow

- Ged is initially frustrated by Ogion's teaching: the mage is near silent
  and mostly gets him to learn ancient runes, as they contain the true names of
  things and these are core to mastery of enchantments.
- In his impatience, Ged allows himself to be tricked by a girl and reads a
  dark spell invoking a shadow.
- Ogion saves him from this and then offers him a choice to either go to Roke
  Island and learn from the wizards there or to stay with him and learn
  something which he could not learn on the island but could learn only from
  Ogion himself.
- Despite his affection for Ogion, Ged chooses to go to Roke Island.
- The journey is long.

## Chapter 3: The School of Wizardry

- Ged arrives at his school and quickly finds something of an enemy in the
  pretentious Jasper and a friend in the amiable Vetch.
- His pride and desire to show Jasper up pushes him in his studies and he excels
  when sent to a tower to learn the true names of many things, something he
  achieves or begins to achieve much quicker than his peers.
- On return from the tower, he binds a rare wild animal to himself by calling it
  by its true name.
- All seems a little happier upon his return but his resentment towards Jasper
  remains.

## Chapter 4: The Loosing of the Shadow

- Jasper finally successfully goades Ged into making a major mistake.
- Determined to prove his superiority, Ged tries to summon a dead spirit.
- Instead, he appears to tear a hole in the world through which the shadow
  creature emerges, attacked him and flees.
- Only with the intervention of the Archmage does Ged survive.
- He spends weeks unconscious, during which time the Archmage dies; Ged's
  friends and enemies continue their studies.
- He is slow to recover and learns slower after his recovery.
- Due to his scars and his hurt pride, he chooses to study in the tower learning
  more about the names of things.
- Vetch has finished his training and on leaving invites Ged to visit him
  when he has finished.
- At length, Ged does complete his studies and leaves the island.

## Chapter 5: The Dragon of Pendor

- For his first mission as a mage, Ged is sent to a series of islands.
- It is an inauspicious posting, but a necessary one as an ancient dragon who
  lives there has recently acquired eight offspring, putting the islands at
  risk.
- Ged's first serious task is when he is asked to save a young child with a
  fever.
- He realises the task is futile and remembers first and last lesson of healing
  was to let the dying die.
- However, he feels he must try.
- He sends his spirit after the boy, chasing it to the land of the dead.
- Waiting for Ged between the worlds of the living and the dead is the shadow he
  released.
- Ged cannot save the boy but manages to escape the shadow, realising now that
  the shadow will find him and he must leave to keep the island safe.
- In order to do that, he makes an attack on the dragons.
- He kills five and injures one of the young eight with relative ease, including
  transforming himself into a dragon, before the old dragon faces him.
- To this dragon, Ged is no more than an inconvenience and he teases him by
  saying he knows the name of the thing which chases him.
- To the Dragon's surprise, Ged knows his name and he uses it to bind the
  Dragon to do no harm the people of the islands, relinquishing the opportunity
  to force the dragon to reveal the name of the shadow.
- Ged's long study into the names of things yielded this victory.

## Chapter 6: Hunted

- Ged tries to get back to Roke Island but finds the protective wind around
  the island will not let him through.
- He turns back and learns from a stranger that a sword which may help someone
  fight a shadow can be found in a tower in Osskil.
- He goes there and a man offers to guide him to the tower but this man is
  actually possessed by the shadow and attacks him.
- He is only just able to make it to the safety of the tower.

## Chapter 7: The Hawk's Flight

- Ged awakes and finds himself being looked after by a beautiful woman who
  says they have met before; she serves the master of the tower.
- To his great sorrow, he no longer has his pet Ottak with him.
- The woman tells Ged of his greatness and that his power is not lost.
- She shows him a hidden stone in the centre of the tower which he recognises as
  the Terrenon.
- She says he can master this stone and it will give him power to be the
  greatest mage in the world.
- It will also give him the name of the shadow which hunts him, giving him the
  power to master it.
- Ged fears the stone and initially refuses to engage in the conversation
  but over time, she begins to convince him, until at the last moment, he
  realises that she has been trying to entrap him as a slave to the stone.
- He also recognises her as the same girl from Gont who teased him in his youth.
- The master of the tower is angry at her for her failure and attacks her.
- Ged is able to save her and they flee the tower, pursued by guards.
- Ged find the body of his dead Otak outside the tower walls and, despite
  Serret's plees that they change into birds and fly, he instead lifts a blade
  of grass and transforms it into a wizard's staff.
- His attacks and turns himself into a mighty hawk but does so
  too late to save the girl, who had turned into a seagull and tried to fly
  away.
- In his hawk form, Ged flies all the way back to his old master Ogion.
- He takes counsel with him and in Ogion's opinion, the only sensible move
  for Ged is to stop being the hunted and become the hunter.
- He makes a new staff for Ged and shortly thereafter, Ged goes on his
  hunt.

> *"It is very hard for evil to take hold of the un-consenting soul."*

## Chapter 8: Hunting

- Ged takes to the sea to hunt his shadow.
- He has two encounters with it.
- In the first, it appears on the sea and runs from his attack.
- However, it eventually tricks Ged, leading him to crash on a small reef.
- The reef is inhabited by an old man and woman who can no longer communicate
  but Ged thinks they may have been an abandoned prince and princess,
  usurped in a coup.
- Ged notices that the shadow does not try to kill him and thinks that
  Ogion was correct in saying that it drew its power from his retreat.
- He therefore keeps up the attack and gets back in his boat.
- As if from nowhere, Ged finds his shadow sitting at the back of his boat.
- He immediately lunges for it, not using magic or any spells but with his
  hands.
- Though he touches it, he cannot hold it.
- This act of touching it gives Ged the connection he needs to no longer
  hunt blindly for his shadow.
- He prepares for their next encounter which he believes will be their last.
- He also comes to realise that his purpose is not to undo what he did but
  rather to finish what he did.

## Chapter 9: Iffish

- Ged finds himself on the island of elfish, where his friend Vetch is the
  wizard.
- He meets Vetch and spends some time with his family.
- They debate the shadow and in the end, Vetch is determined that they finish
  the quest together given that they started to get together.
- Ged is unable to convince his friend otherwise.
- After more time spent with the family, during which Ged becomes close to
  Vetch's sister, the two prepare to leave.

## Chapter 10: The Open Sea

- Ged and Vetch travel eastward in a boat.
- Ged is confident they are on the track of the shadow, navigating by his
  senses.
- Vetch is astounded by some of the magical power Ged demonstrates on the
  journey.
- Only once does Vetch question whether this could be another trap.
- Along the way, the two men bond further with Vetch telling Ged his
  sister's true name.
- When finally they encounter the shadow, having travelled further east than
  anyone ever before, it appears on a sandy island and takes many forms in front
  of Ged, who goes to confront it.
- The confrontation is simple – Ged and the shadow utter each other's name –
  Ged.
- Watching from the board, Vetch thinks Ged is in trouble and goes to rescue
  him.
- However, the sand was just an illusion and quickly at fades into the sea.
- Vetch is able to get back to the boat and rescue Ged.
- The shadow was a part of him, the shadow of his own death, and now he is whole
  again.
- Their journey home is long and never remembered for what it truly was in any
  song our history.

[Home](/blog "ohthreefive")