+++
         
date = "2020-08-31T20:06:01.875Z"
title = "Summaries: Black and British"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Black and British

_David Olusoga_

## Preface & introduction

- Olusoga documents the racism he encountered growing up in Britain and how it
  ultimately led to him becoming a historian.

- The introduction begins with a discussion about the West African slave
  factories, partivularly Bunce Island in Sierra Leone.
- For around 140 years from the late 17th Century, enslaved Africans passed
  through this facility, with as many as 50,000 ending up in the United States.
- After its closure, it was partly forgotten by history and upon its
  rediscovery, was thought to be Portugese.
- However, its roots in the British Empire were soon uncovered.
- The ruined site has become one of pilgrimage for some Gullah.
- Olusoga moves on to Enoch Powell after this introduction to the horrors of the
  slave trade, not to the Rivers of Blood speech but to one made nearly a decade
  earlier.
- In it, Powell tries to erase colonial history and talk of returning Britain
  (or more accurately England) to a pure pre-colonial and - unspoken - white
  country.
- Olusoga tears apart his argument by documenting the prevalence of black
  Britons long before the time to which Powell wants to return.
- The introduction closes with a mention of the complicity of the British Royal
  family in the slave trade.
- They did not simply benefit from the proceeds: the Royal African Company was
  founded with the Duke of York James Stuart (future King James II) as a member.
- Olusoga sets the scene as Britain as a mixed race country for long before when
  most racists would accept.
- History has forgotten much; he plans to address that.

## Chapter 1: Sons of Ham

- The initial discussion is of new techniques producing evidence suggesting
  black inhabitants in Britain as early as the third century.
- Mediaeval descriptions of Africa and Africans often described them as a
  fantastical and grotesque.
- Britain's initial post-Mediaeval involvement with Africa was not to do with
  the slave trade but about gold.
- Britain wanted to exploit the riches with which the Portuguese were doing so
  well at the time.
- In the 1600s however there is definite evidence of slave trading.
- Famously, John Hawkins received royal approval, including a knighthood, for his
  initial successes on the west african coast although after a disastrous third
  expedition, British slaving paused for some time.
- Despite many first-hand accounts of Africa being available after these early
  missions, the mediaeval mythologies were not dispelled.
- Explanation was sought for the darkness of the skin and a passage from Genesis
  about one of Noah's sons, Ham, and his curse, somehow became racialised with
  his descendants being thought of as black.

## Chapter 2: Blackamoors

- Black Africans in Britain were commonplace pre-slavery, often in roles of
  servitude.
- Notable high-profile black Britons include John Blank, a member of the Tudor
  court, who is even captured on a canvas playing trumpet with four other
  musicians in a procession for the King, a performance for which he was
  unequivocally paid.
- Black as a colour, with no particular connection to race, had negative
  associations in society, something which can be seen reflected in
  Shakespeare's plays.
- One factor which resulted in the emergence of British slavery was the sugar
  trade.
- An important contradiction is that slavery in England was illegal.
- Indentured servitude was akin to an exceptionally harsh form of
  apprenticeship, but the demands of the sugar trade meant that it was
  insufficient and Britain started to employ black African slaves, first on
  Barbados, then moving to the much larger Jamaica, to sustain this trade.
- Initially slave traders had a royal charter, but after lobbying and
  campaigning, the Royal Africa Company and other royal slave trading companies
  were disbanded as the practice was deregulated and taken over by private
  companies.

## Chapter 3: For Blacks or Dogs

- Georgian culture somehwat resembled 21st century Britain, with hedonism and
  excess being widely practiced.
- Black Britons were commonplace.
- They held a mixed place in society, from slaves to active societal
  participants.
- Life for those who served a role similar to white servants was particularly
  precarious, with one misstep potentially leading to a boat to the caribbean.
- A picture is painted of great disparity but overall of subjugation.

## Chapter 4: Too Pure an Air for Slaves

- An initial discussion about the legal uncertainty surrounding slavery in
  England in the late 18th century.
- A major character is Granville Sharp, who was known to offer care to the
  downtrodden, and who became an unlikely civil rights activist and tireless
  abolitionist after an encounter with a black boy beaten near to death by his
  master.
- In 1772, a ruling by Lord Chief Justice Mansfield in a case of a black man
  against his English slaver, ruled that the slave laws of Virginia did not
  apply in England and that this man should be dismissed.
- This was far from the end of slavery in England, however, as the precarious
  financial situation of many black servants and slaves in England led them to
  remain beholden to slavers.

## Chapter 5: Province of Freedom

- Mansfield's verdict came just shortly before the second American revolution.
- Black slaves were are used as a tactical advantage by the British.
- Taking into account the ramifications of Mansfield's verdict in England, they
  offered black positions in the infantry and cavalry if they abandoned their
  revolutionary masters.
- The situation was desperate for these slaves with neither offer really being
  appealing.
- One of the ultimate betrayals of the British was at the Battle of Yorktown
  near the end of the war.
- Having already slaughtered his horses to fight off starvation, British General
  Cornwallis forcibly released his black slave fighters in order to have fewer
  mouths to feed.
- They were caught between a British force that did not want them and their
  former American slave masters.
- In the aftermath of the defeat, retreating British forces sought not to repeat
  the betrayal of Cornwallis.
- In peace negotiations years later, slavers on either side manage to include an
  amendment – literally in the margin – saying that any other colonial forces
  leaving America were not allowed to take American slaves with them.
- While 35,000 white British loyalists waited in New York expecting financial
  ruin and abandonment, 3000 black loyalist who had fought in these awful wars
  with a hope or promise of freedom, now expected to return to slavery.
- Returned to England, many of the black former slaves and soldiers found so-
  called free life difficult, with work nearly impossible to find and keep.
- They became a group known as the black poor.
- There was fear, hatred and resentment towards them and calls for their
  resettlement.
- Obviously, America was now out, Africa seemed unlikely and there were no other
  ideal places for them.
- Eventually, a plan was enacted for resettlement as freeman in Africa.
- The chosen destination was Sierra Leone but for various reasons, the voyage
  was delayed by up to four months.
- At the same time, the first ship of convicts were also waiting to leave for
  Botany Bay and English media could not help but compare the two.
- In the end, fewer than 500 black poor left for Africa, and the group included
  many white poor among them, including prostitutes.
- Men and women married, having mixed-race children.
- Arriving in Sierra Leone, they were granted 20 mi.² of land, which Granville
  had thought to call the Province of Freedom but the settlers named Granville
  Town.
- Disaster was soon to strike: the delay in departure meant the settlers had
  arrived just in time for the rainy season.
- All crops planted and settlements quickly erected were washed away.
- Within a year, up to a quarter of the settlers had died from tropical fevers
  such as malaria.
- When Granville heard the news, he was first slow to attribute the disaster to
  poor planning – in particular the delay and proximity of arrival to the rainy
  season.
- He also decried the settlers deviation from his regulations – a rare example
  of his puritanical mind set being negative.
- In a devastating irony, some of the settlers sought refuge in neighbouring
  Bunce island and became part of the slave trade again.
- The final demise of Granville Town was fire and then attack by the next ruler
  of Sierra Leone.
- Driving the settlers out, they were ironically rescued by canoes from the
  slavers of Bunce island.
- Granville did not give up on his idea of a free black province in Sierra
  Leone.
- Black American loyalists who had fled to Nova Scotia, including a former slave
  of George Washington, were also living in uncertainty, and particularly in
  fear of slave hunters.
- After an appeal to the British Home Secretary, it was decided they were to
  become the second settlers of Granville Town.
- A greater number than expected volunteered, including Washington, and though
  the preparations for their arrival were completely inadequate, they were able
  to establish a large settlement which they named Freetown and which still
  stands today.

## Chapter 6: The Death of a Monster

- The abolition movement can trace its origins back to Mansfield's first
  judgement in the Somerset case, limited in scope though it was.
- Unsurprisingly, Granville Sharp was at the heart of the white abolitionist
  movement.
- Important allies were the politician William Wilberforce and a man named
  Thomas Clarkson.
- There were also black abolition groups, notably the Sons of Africa.
- Both groups wrote and campaigned relentlessly.
- On the opposite side – anti-abolitionism – was George Hibbert.
- Political uncertainties caused by the French Revolution and the horrendous
  bloodshed in the successful slave rebellion on Haiti quelled the abolitionist
  movement.
- Wilberforce brought forward bills every year between 1794 and 1799 and was
  eventually successful in 1807 in passing a bill making the slave trade
  unlawful.
- Wilberforce and his allies believed in gradualism, however, meaning that they
  were concerned slaves would have difficulty integrating back into freedom.
- Despite making the slave trade illegal, there were still slaves for some time.
- A census of the slave population brought their plight back to the attention of
  the abolition movement about a decade later and the movement reignited.
- Granville Sharp died in 1813 and by the time of the second wave of abolition,
  this time targeting slavery itself, Wilberforce was an aged man.
- Thomas Clarkson still remained an active campaigner.
- Despite the passage of years, gradualism remained the core message of the
  leaders of the abolition movement.
- The followers however were much more forthright in wanting immediate
  manumission.
- A separate female abolitionist group also campaigned for immediate freedom.
- A rebellion by slaves accelerated the success of the movement.
- In the West Indies, what began as peaceful civil disobedience was followed by
  months of malicious hunting down and killing black slave rebels.
- Reports of the incidents helped with the eventual passage of an abolition bill
  in 1832.
- Even this carried with it a six year unpaid quasi-apprenticeship (four years
  for domestic slaves) to help the slaves move from captivity to freedom.
- Again, campaigning did manage to reduce this term and slaves were freed on
  July 31st, 1838.
- Modern history has largely forgotten the female abolitionist movement, the
  Sons of Africa and the campaigning of prominent former black slaves.
- William Wilberforce's sons successfully painted him as the giver of freedom
  (rather than freedom being fought for) and Thomas Clarkson is poorly
  remembered in their versions of history.

## Chapter 7: Moral Mission

- The abolition of slavery in Britain came with a significant financial bailout
  for slavers.
- This may have represented some sort of way for Britain to absolve itself.
- In any case, Victorian Britain became a haven for American slaves, with the
  abolition movement nowhere near at the same place in the new world as it was
  in the old.
- Several escaped slaves became famous and toured, lecturing on their lives.
- An important contradiction is well demonstrated by the famous Victorian author
  Charles Dickens.
- Dickens was clearly appalled by slavery, judging by his writings on his visits
  to America.
- However he also demonstrated a discomfort with the appearance of black people.
- He even went so far as to remove a portrait of famous black former slave
  author Benjamin Douglas from a book he was sending to a friend.
- In the Victorian times, there were two cultural phenomena of differing tones,
  both of which were profoundly anti-slavery.
- Uncle Tom's Cabin was a novel by Harriet Beecher Stowe which told of the awful
  suffering and eventual death of the titular character.
- Its tone veered strongly towards melodrama which lessens its impact on reading
  it today but it had a profound effect on Victorian audiences.
- Uncle Tom's Cabin was such a phenomena and that even spawned what we would now
  call fan merchandise.
- A much more comedic tone was struck by Thomas D. Rice and his character Jim
  Crow, a name which would be appropriated by popular culture to refer to the
  oppressed black population during the civil rights movement.
- Common to both and to Victorian culture at the time, black faced minstrels
  often performed pieces and, while delivering profoundly anti-slavery messages,
  performed with mocking caricatures of black people.
- The contradiction is hard to accept in modern times but was commonplace.
- Minstrel troupes who performed songs enjoyed widespread popularity and even
  popularised some songs still used today.
- By the late 1850s, many of the campaigners for abolition were aged and a new
  generation with hardened racial attitudes were emerging.
- Combined with alleged scientific evidence of fundamental differences between
  the races, a new wave of feeling that Britain should not involve herself in
  the American slavery row and that perhaps abolishing slavery was a mistake
  was appearing.

## Chapter 8: Liberating Africans

- After the abolition of the slave trade by Britain in 1807, two ageing vessels
  were sent to police the West African coast.
- This was out of the Navy of over 700 ships.
- After about a decade, the number had increased to 10 and the group was known
  as the West Africa Squadron.
- The Portuguese had used Britain's exit from the slave trade to strengthen
  their own position.
- Britain's minimalist efforts meant that the task facing the West Africa
  Squadron was all but insurmountable.
- In time, however, and with changes to laws allowing more liberal capture of
  ships suspected of rather than actively participating in the slave trade,
  Britain did start to be able to make an impact.
- One of the most celebrated ships was a nimble captured slave trading ship
  known as Black Joke, whose importance was such that when it was finally burned
  due to rotting timbers, slave traders allegedly celebrated.
- At the time of the abolition of the slave trade, it was still in a phase of
  rapid expansion.
- Countries such as Portugal accelerated to fill the void left by Britain.
- This both highlights how radical Britain's decision to abolish the trade was
  and how difficult the task for the West Africa Squadron was.
- While they probably liberated in excess of 100,000 slaves over about half a
  century, over 2 million slaves were moved at that time.
- Liberated Africans were delivered to Freetown, but the journey could be
  dangerous for various reasons and many former slaves and sailors died.
- The region of Freetown into which the liberated Africans arrived was known as
  the King's Yard and had a heavy medical presence; medical facilities still
  exist there today.
- Life was still not easy for the re-captives of Freetown.
- Disease still took a huge toll but was of course not limited to the Africans.
- Only about half of missionaries survived deliberately shortened terms in
  Sierra Leone with others returning to England and Wales unwell.
- One tragedy of Freetown was that some of the children were initially given
  then being sold as apprentices.
- Some people viewed these apprenticeships as little more than slavery by
  another name and even worse, some of the apprentices were sold for profit back
  into the slave trade.
- A number of the liberated Africans joined the British Armed Forces, either in
  the Royal Africa Core or even in the Navy, helping to free more slaves.
- Queen Victoria had a young African former slave who she gave patronage to, and
  was godmother to the woman's, Sarah, daughter.

## Chapter 9: Cotton is King

- In the mid 19th century, Britain was economically linked to America more than
  to any other country.
- In the grips of the industrial revolution, Britain was responsible for nearly
  1/4 of the world's exports.
- Around 40% of these exports were cotton goods, produced in the north-west.
- The cotton for their production, however, was slave-picked in the southern
  American States.
- Much of the cotton was sold to Africa, which in turn supplied America with its
  slaves: triangular trade.
- There were up to 4,000,000 enslaved Africans in the southern states, with only
  just over 2 million white Americans, included among them some of the richest
  people in the world at the time.
- Up to 50% of America's exports at the time was cotton and at its peak, around
  97% of Liverpool's imported cotton came from America.
- Despite coming 50 years after the abolition of the slave trade and 30 years
  after slavery was outlawed, Britain's dependency on slaves was great.
- In the late 1850s, South Carolina Senator John Henry Hammond and his infamous
  mudsill speech, declared slavery as a necessary means to perform menial tasks.
- His racist views on black Africans made them ideal slaves as part of natural
  law as he saw it.
- Around the same time, David Christy produced a book called Cotton Is King
  which expressed similar views.
- Both men displayed startling hubris and believed King Cotton was the saviour,
  such was its economic value, of their views and way of life.
- While the American slave trade and slavery in America could have been
  abolished earlier, cotton was now so important that slavery could never be
  abolished.
- Similarly, the southern cotton producing states could never be attacked due to
  the power of King Cotton: Hammond had openly said he would be willing to stop
  cotton production for up to 3 years if attacked.
- Despite this, just three years later when the American Civil War began,
  Abraham Lincoln blockaded southern exports, crippling that economy.
- Similarly, the brewing American Civil War had been foreseen in Europe and
  there was a stockpile of cotton there.
- In addition, surplus production of cotton for years meant that there was also
  a stockpile of produced goods.
- Despite some degree of preparedness, the effect of the Civil War in America
  was still so devastating that eventually the Lancashire economy collapsed into
  a period known as the cotton famine.
- But the British economy, despite the horrendous effects in Lancashire, was
  stronger than the southern states had predicted and overall was able to
  weather the storm.
- In part, the war offered financial opportunities to Britain.
- Officially, Britain was neutral in the war but throughout Britain, there was
  sympathy for the southern cause.
- Nowhere was this more openly expressed that in Liverpool, which produced and
  crewed ships for the southern armies, quite openly in rebellion against their
  own country.
- In January 1862, Abraham Lincoln changed the motive for the war in British
  hearts and minds.
- What was a war to preserve the American union became a war against slavery as
  Lincoln declared he would free the slaves of America.
- The emancipation proclamation was made in September of that year.
- Strength returned to the abolitionists in Britain and they declared open
  support for Lincoln as he won the war.
- The 13th Amendment freed any remaining slaves when it was ratified in 1865.

## Chapter 10: Mercy in a Massacre

- On his second visit to Britain in the late 1850s, former slave Frederick
  Douglass noticed a new racism spreading.
- This racist sentiment is commonly thought to have come from America but was
  more likely transported from the West Indies, where the abolition of slavery
  had devastating effects, particularly to Jamaica.
- Slavery as an economic model had always been unstable due to uprisings and
  rebellions but slave traders seem blind to this.
- Coinciding with the publication of, "On the Origin of Species," a racist
  splinter group of so-called scientists formed the Anthropological Society of
  London.
- They were strong believers in social Darwinism – something Darwin himself did
  not believe – and that Africans were a separate race to white humans and their
  purpose was servitude.
- The West Indies and Jamaica in particular were to feel the impact of the
  American abolition of slavery strongly.
- Jamaica fell into desperate hard times, as bad as those as Lancashire, due to
  poor management of the countries economy, including an over reliance and under
  diversification around growing sugar.
- The 'Jamaican poor' proposed a diversified economic strategy to help them but
  were dismissed by the British and condescendingly told that one of the better
  options might be to return to the fields.
- In Morant Bay in 1865, during the trial of a black man, an African onlooker
  disrupted proceedings and British troops rather unexpectedly opened fire into
  the crowd.
- When the firing and shock had subsided, the Jamaicans rose up and killed up to
  18.
- The governor, named Eyre, enacted swift and disproportionate retribution.
- This included killings, trials, executions and punishments which amounted to
  torture.
- Despite less than half a percent of Jamaica's black population being involved
  on the day, the situation was painted as a black racial uprising and
  rebellion.
- Despite eyewitness accounts reaching Britain to the contrary, many anti-
  abolitionist newspapers favourite Eyre's side of the story.
- The scandal dragged on and included an enquiry.
- The Governor Eyre Defence and Aid Committee was formed.
- Despite the racial pseudoscience used to justify his action against black
  Africans, it was not scientists but more authors and literary figures who
  supported Eyre's actions and made up this committee.
- One conclusion came out that the actions of year would have been inexcusable
  where it not for the fact that it was negro blood that was spilt.

## Chapter 11: Darkest Africa

- In 1884, Britain was celebrating 50 years of abolition.
- At the same time, the 'scramble for Africa' was about to begin.
- Technological advances meant that the British could now advance into central
  and sub-Saharan Africa, having previously been limited to coastal regions.
- In 1870, 90% of Africa was ruled by Africans; by 1900 this was down to 10%.
- Britain was the most successful of the European colonists.
- This period also included travelling shows and literary works about this
  exotic continent.
- Pseudo-scientific racism reinforced some stereotypes about the ability of
  African adults to mature beyond childishness.
- An excellent argument against this is a group of Senegalese professional
  actors who were making a tidy profit by touring with these exhibitions.
- Many African kings visited Britain in the Victorian era.
- One of the most famous is a visit by three kings of Bechuanaland in 1895.
- They sought an audience with Queen Victoria but received a lengthy tour of the
  country before this was granted. They were highly regarded in Britain,
  especially as one of their aims was to seek protection from the Monarch.
- This apparent submission to British rule was viewed warmly.
- Their true purpose was to seek refuge from Cecil Rhodes and the British South
  African Company, the man being recognised as having love only for the African
  country and not her people.
- Bechuanaland remained under the protection of the crown until 1966 when
  Botswana was formed.
- Racist cultural attractions persisted, particularly either minstrel troops or
  songs from the minstrel song book.
- Alongside this however where groups of black singers bringing African gospel
  music and songs to Britain, including Swing Low Sweet Chariot.

## Chapter 12: We Are a Coloured Empire

- There was a strong appetite to join the First World War in particular by the
  West Indian Africans.
- Britain weighed their offer of support.
- This European war was, in their opinion, of white man's war.
- However, rejecting their request to form a regiment may have unintended
  negative consequences and so the British West Indies Regiment was formed.
- It was deployed first in Egypt, thinking that the Africans would be better
  suited to the climate.
- However, the regiment was in some cases segregated from white soldiers and was
  tasked with manual tasks rather than combat.
- Even when reinforcements at the Western Front would have been invaluable,
  there was racial prejudice preventing the black Africans being sent there.
- One issue was a racist assumption that all the black soldiers were physically
  strong and imposing.
- In contrast to this, many British recruits were malnourished and small.
- These men alongside each other would raise questions about white European
  racial dominance.
- Among those who questioned the reluctance to involve Africans in the British
  Army was Winston Churchill, who in a 1916 speech brought up the fact that
  France were deploying 100,000 of their Africans.
- He argued that when history recalls these events, and when the British Empire
  was in its greatest need of troops, would it look fondly on the decision not
  to use there Africans?
- Despite this, the racial barriers persisted and black troops were thought not
  suitable to fight the Germans in Europe.
- Just as some under age or under sized white men managed to slip through
  recruitment, some black men did manage to join the war in Europe as they
  desired.
- After the war, the British West Indian Regiment returned to Jamaica somewhat
  demoralised by the politicised way in which they were already being written
  out of the war effort.
- In 1919, a black soldier named Charles Wootton was chased by crowd of up to
  2000 in Liverpool and sought escape by entering the water.
- As a police officer went to help him out, he was struck struck on the head by
  a rock and drowned.
- An inquest brought no charges.
- There were few areas of society in which there was not at least some degree of
  resentment towards the former black soldiers.
- The lower wages they tended to be paid was seen as an unfair advantage as
  people sought jobs.
- The lack of jobs in the country resulted in the reemployment of many white
  soldiers at the expense of the black.
- Renewed disgust was expressed towards the idea of mixed race couples of black
  men and white women; notably again the reverse was not even discussed.
- In Liverpool, the idea of repatriation was even mooted but eventually rejected
  as potentially too incendiary.
- The Empire was faced with a difficult issue – it ruled over tens of millions
  of black Africans, many of whom had fought for its freedom, but these people
  were not given the fruits of their labour or even the freedom of empire.

## Chapter 13: We Prefer Their Company

- In 1944, there was a new peak black population in Britain of around 150,000.
- In part, this had to do with the 1942 deployment of American troops in
  Britain.
- Similar to the First World War, Britain regarded this European war as a white
  man's war.
- However, they had no control over American policies other than ensuring that
  the incoming soldiers were representative of the American population, i.e. 10%
  black.
- The black soldiers found the British populations attitude towards them in
  general more liberal than in the United States, a fact which sometimes angered
  white American soldiers.
- Nevertheless, racist stereotypes persisted both in the public and politically.
- Attacks by white American soldiers on their black allies were common.
- Attitudes towards black man forming relationships with white women were
  persistently backward.
- Though not immune to displaying racism himself, Winston Churchill was even the
  subject of racist abuse himself when a political colleague noted that his
  physical stature compared to that of his family suggested he had black blood
  in him.
- The majority of violence against black soldiers in Britain was from white
  American soldiers.
- On the second deployment of American soldiers in Britain, rules and
  protections had to be set up.
- Despite the mostly positive reception of black American soldiers by the
  British public in general, after the war, this was sometimes forgotten.
- One result of this was that some mixed race children were abandoned.
- The political establishment were at something of a loss about what to do with
  them.
- A British woman who travelled to Virginia after the war to marry her black
  American soldier husband found herself deported and him imprisoned because
  this was still against the law.
- Black Africans were eventually deployed outside of Africa in the Second World
  War, but notably to fight the Japanese, not Europeans.
- Among them was the grandfather of Barack Obama.

## Chapter 14: Swamped

- After the Second World War, there were huge labour shortages in Britain and an
  economic crisis in the West Indies.
- There was also a feeling that racism had led to the Nazi concentration camps.
- Black activists in Britain tried to use these factors to their advantage but
  the politicians, Clement Attlee being prime minister at the time, were
  resistant.
- They wanted to discourage mass black immigration into Britain and instead make
  up the labour shortage with war-displaced white Europeans.
- However, they also recognised the negative impression this may give to black
  African Britons throughout the Commonwealth.
- They did not seem to be able to resolve this paradox.
- In 1948, the British Nationlity act was passed allowing free movement movement
  within the Commonwealth and intended to allow Great Briton's living overseas
  to return.
- Instead, it allowed for mass black immigration, the very thing the same
  government wanted to avoid.
- In 1948, the Empire Windrush arrived from the West Indies with 498 black
  Britons, despite some attempts to stop it.
- Such were the labour shortages that within months, all but 12 had found jobs.
- Liverpool was again something of a centre for racism, with trade union
  organised rates and attacks on black African workers.
- In the very early 1950s there was a sudden increase in the number of migrants
  from the West Indies due to both a devastating hurricane in Jamaica and
  immigration policies of the United States becoming more strict.
- When now-Conservative MP Winston Churchill returned as prime minister in 1951,
  there was an even further swing towards anti-immigration.
- Key to not offending or risking the Commonwealth was not singling out black
  immigrants over white immigrants.
- An attempt was made to deliberately blur the terms 'black' and 'immigrant' so
  that immigrants rather than black Africans could be talked of as unwanted in
  Britain.
- A highly racist report was produced by the government which highlighted many
  severe flaws in black Africans.
- The racist behaviours and policies of the government crept their way back into
  sections of the British public.
- Racist opinions regarding mixed race relationships and marriages became one of
  the most common forms of racism in Britain.
- They were certainly one of the sparks of the Notting Hill riots.
- Freedom of movement enjoyed by black Britons since the nationality act where
  almost completely rolled back by the end of the 1960s.
- Enoch Powell displayed casual disregard for science and statistics with his
  rivers of blood and other speeches.
- He contended that black and mixed-race people could never integrate in
  Britain.
- His sacking just days after the infamous speech shows there was political
  opposition to racism but of the over 100,000 letters he received, fewer than
  3000 disagreed with him.
- Tensions in Britain continued for decades culminating in the 1980s with the
  Brixton riots.

## Chapter 15: Conclusion

- The Empire Windrush and its one particular journey in 1948 among many across
  its time before it sinking, has become the most famous point of black British
  history.
- Though its celebration is commendable, it should not cloud the truth that
  black British history extended centuries before it and continues to be written
  now.
- Between the census of 2001 and 2011, the number of black Africans in Britain
  doubled, partly due to population increase and partly due to immigration.
- Racism persists in Britain, but the influence of black African culture on
  post-empire but now globalised Britain.

[Home](/blog "ohthreefive")