+++
         
date = "2019-09-14T18:45:47.041Z"
title = "Summaries: Bounce"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Bounce

**Matthew Syed**

## Part I: The Talent Myth

### Chapter 1: The Hidden Logic of Success

- The book opens with the author keen to debunk the notion of innate talent
  trumping practice.
- He immediately draws reference to the work of Anders Ericsson, who studied
  violinists and found that without exception, those of whom who became the most
  famous soloists had practiced more, significantly more, than the others.
- The author draws attention to several seemingly superhuman abilities, commonly
  assumed to represent talent but frequently observed as secondary to practice,
  for example extreme memory challenges or quick reactions returning tennis
  serves.
- A form of pattern recognition is learned by experts by their extensive practice.
- This can translate to the subconscious, to the extent that an expert may be able
  to perform exceptionally with no recollection of planning and executing
  particular shots but simply being able to express why they played a particular
  shot, the desired outcome of the shot and its impact on the match.
- This bank of experience is put forward as the reason Gary Kasparov was able to
  beat Deep Blue and also the explanation that, after IBM employed a former chess
  grandmaster to help improve Deep Blue, it beat him at the second attempt.
- The ability of the computer to make decisions quickly was no more of an
  advantage in the initial match up than having fast reaction times when trying to
  return a tennis serve.
- Unfortunately, two aspects of the opening chapter are a bit outdated –
  Ericsson's 10,000 hour rule is [regularly criticised][1] and Google developed
  an AI called [AlphaZero][2] which taught itself chess strategy using a
  generative adversarial network and, a mere four hours after being taught only
  rules of chess, could not be defeated by Stockfish, the current world champion
  of chess-playing programmes, over 100 matches.
- AlphaZero is not a chess-playing programme, 'just' an AI.
- In the second of those examples, the author could argue that the hours of the
  GAN playing itself constituted the practice, pattern recognition and other
  'superhuman' skills that human experts acquire.
- Hopefully the rest of this not-too-old book won't seem too outdated!

[1]: https://www.vox.com/science-and-health/2019/8/23/20828597/the-10000-hour-rule-debunked "Vox article about Ericsson's 10,000 hour rule"

[2]: https://www.theverge.com/2017/12/6/16741106/deepmind-ai-chess-alphazero-shogi-go "Verge article explaining AlphaZero's achievements"

### Chapter 2: Miraculous Children?

- This chapter focuses on what it calls the myth of the child prodigy.
- By looking at those who have achieved expertise or greatness early, you can
  always identify that real practice began early: earlier than 'normal'.
- An important proviso is that the motivation must come from the child.
- And example is given of a Hungarian psychologist who groomed his three daughters
  for chess greatness: they all achieved well beyond anyone before.

### Chapter 3: The Path to Excellence

- This chapter focuses on Ericcson's theory of deliberate practice, called
  purposeful practice by the author.
- Practice must have the ultimate goal of improving performance.
- Years or decades after casually commuting in a car, your driving is likely no
  better or even worse than in the past.
- This is because most people drive on 'autopilot'.
- Built into deliberate practice is the need for interpretable feedback.
- If controllable aspects of your performance (an example given being the authors
  forehand slice shot in table tennis which varied wildly as a youngster) are not
  controlled or predictable, then getting feedback when the shot goes wrong is
  difficult.
- If the shot is entirely reproducible, any variations in the outcome can be
  analysed.
- Professional golfers, for example, on a practice round may hit five or six shots
  from the same position, analysing their feedback on the shot and also getting
  feedback from coaches or videos afterwards.
- The amateur may be satisfied with their first shot.
- When Roger Federer hits an unconventional, seemingly invented shot to get out
  of trouble, he has probably practised this shot more than most amateurs have
  practised their standard forehand, making it seem like raw talent.
- Quantum leaps, bolts from the blue or unexpected innovations are born of this,
  rather than being exceptions to this.
- Picasso worked up to his masterpieces, as did Mozart.
- With this considered, for complex tasks, we are likely nowhere near the limits
  of human performance.
- The chapter ends with a comparison between deliberate practice in an economic
  and societal sense versus that of the sportsman, hoping it will become an even
  more beneficial thing in society but noting that it is only currently adopted
  comfortably by a sports people.

### Chapter 4: Mysterious Sparks and Life-Changing Mind-Sets

- This chapter introduces two very interesting variables on the path to
  excellence.
- Firstly, there is an observable incidence of sparks of inspiration which have
  affected practitioners of a given field.
- An example given is a female Korean golfer winning a tournament, inspiring a
  nation and leading to further Korean success in golf 5 to 6 years later.
- There are many other real-world examples of this and it has also been tested
  experimentally, example being giving a group of students a motivational text
  before a test, and giving the author of the motivational text the same birthday
  as the student in half of the cases.
- This meaningless and artificial coincidence improved performance.
- In the real world, a timelag is experienced between the motivational event and
  more widespread success, which can be explained by the persistent requirement
  for practice, which takes time.
- We all know that another factor which is required is that this motivation must
  be maintained.
- An important variable here is the practitioner's mind-set.
- Tests have shown that those who believe in practice as the route to expertise
  persist longer and are more creative and motivated; those who believe in talent
  and predetermined gifts for an enterprise give up when challenged beyond their
  abilities.
- The growth mindset prioritises effort and learning over success.
- It has been experimentally shown to be at least in part inducible, for example
  by praising someone for working hard, or recognising someone's hard work when
  they achieved success rather than for praising the success itself.
- If hard work has clearly not been required, you can apologise for failing to
  challenge the practitioner and suggesting you try something challenging next
  time as it will be more fun.
- There is even a suggestion that praising achievement can have a negative impact,
  making the practitioner more keen to avoid challenges so that the praise is not
  removed.
- The author says a place in which this mindset is strikingly apparent is the Nick
  Bollettieri tennis Academy – even more surprising considering Bollettieri has
  never heard of the famous researcher who investigated this phenomenon.

## Part II: Paradoxes of the Mind

### Chapter 5: The Placebo Effect

- The chapter discusses the placebo affect within sporting performance.
- It starts by introducing the effect, referring to historical accounts and indeed
  experiments with operations performed without anaesthetic.
- It also introduces the example of Jonathan Edwards, a Christian whose belief -
  in his belief - helped him perform better.
- In these examples, a falsehood or a delusion was improving performance or
  outcome.
- Sports psychology has strongly adopted the placebo affect and most elite sports
  people have some sort of ritual before a match.
- A common technique is to associate something which you have never failed at with
  something with which you are about to perform, for example, visualising picking
  a golf ball out of the hole before attempting a putt.
- Convince your mind that you cannot fail – when have you ever failed to pick up a
  ball? – and you will convince your mind that you will sink the putt.
- An interesting facet of sporting placebo is akin to George Orwell's notion of
  doublethink.
- Before a 40 foot putt, and elite golfer can both believe entirely that the putt
  will be successful but also acknowledge the unlikeliness that it really will be
  successful and ensure that they leave themselves in a position where the second
  putt will be unmissable.

### Chapter 6: The Curse of Choking and How to Avoid It

- The phenomenon on of an athlete choking is presented as a consequence of the
  conscious mind trying to take control of what the athlete has already encoded
  into their subconscious.
- For example, the high pressure of the situation made the author tried to control
  all aspects of his topspin forehand during an important table tennis match.
- However, the topspin forehand is a highly complex shot, honed over years of
  practice and encoded into the subconscious.
- Trying to move control of it back to the conscious brain caused him to choke and
  miss shots which were easy.
- Importantly, this explanation shows that there is no lack of courage to blame
  when an athlete chokes.
- Simple tasks can be improved by explicit concentration on them; complex tasks
  are too complex and the implicit performance of them is required.
- One way to avoid choking employs more doublethink.
- An athlete can convince themselves or attempt to convince themselves that the
  task ahead of them is no more important than practice and allow their
  subconscious to control complex tasks such as shot making.
- The conscious mind can therefore concentrate on tactics and gameplay.

### Chapter 7: Baseball Rituals, Pigeons and Why Great Sportsmen Feel Miserable After Winning

- The chapter is divided into two parts.
- The first part discusses rituals and superstitions which are common among sports
  people.
- The author believes the source of superstition is attributing a trivial action
  to a desired outcome.
- Superstitions have been shown to be experimentally inducible in pigeons.
- The author stresses that superstitions are not necessarily negative – they may
  help an athlete relax and therefore perform better.
- They are only negative if adherence to them has a detrimental outcome, for
  example insisting being the last player out of the changing room at half-time
  when a teammate is undergoing prolonged treatment for an injury and therefore
  not leaving the changing room until after the second-half has begun (Kolo
  Toure.)
- The second part of the chapter is about the anti-climax many athletes feel after
  achieving victory.
- They have spent so long preparing for a victory and its achievement is a blip in
  comparison.
- There is an evolutionary benefit to this.
- If contentment could be forever gained by achieving a single goal, people would
  stop after achieving their first goal.
- Elite athletes appear to have a severe version of this and need to continue to
  pursue new goals.

## Part III: Deep Reflections

### Chapter 8: Optical Illusions and X-Ray Vision

- This chapter deals with a less well understood way in which experts can appear
  superhuman.
- Blind people who recover their site later in life, for example a man in his 50s
  who had corneal implants, can struggle at first to visually recognise someone
  who they have met a few times, despite having the same visual input as everyone
  else.
- Though this seems implausible, it is compared to how listening to a foreign
  language can sound incomprehensible and unstructured whereas listening to your
  native language, even when deliberate distractions are put in it, usually
  results in understanding.
- The author's phrase is it that the expert has so much experience and knowledge
  in his field that he has extra "perceptual bandwidth" to deal with more input.
- Examples given are the Harvard students asked to count basketball passes in a
  video, half of whom failed to notice the man in a gorilla suit walking on
  screen during the video.
- Their perceptual bandwidth was saturated by counting the passes.
- A more tragic example is an airline crash, when the pilot and co-pilot were so
  preoccupied trying to decide whether a light was faulty that they failed to
  recognise both a decreasing altimeter and an alarm.
- The expert therefore does not have superhuman eyes or ears, simply more capacity
  to recognise, analyse and act upon the input we all receive.

### Chapter 9: Drugs in Sport, Schwarzenegger Mice and the Future of Sport

- The chapter opens with a personal story about anabolic steroid use in east
  Germany.
- It then presents somewhat controversial proposal – forget about controlling
  methods of performance enhancement and instead focus on keeping performance
  enhancement safe.
- He uses an example of the fact that altitude training to increase haematocrit to
  an unsafe level is perfectly legal whereas erythropoietin injection to increase
  it within safe levels is illegal.
- The invention of the telescope was judged to be unnatural by some at the time.

### Chapter 10: Are Blacks Superior Runners?

- A thorough scientific, political and sociological takedown of the racial
  stereotyping of blacks as superior runners.
- The author presents evidence that there is no natural, racial nor even genetic
  advantage to being black in sport, nor for that matter any other race or
  ethnicity.
- He lambasts the self-perpetuating nature of these falsehoods.

[Home](/blog "ohthreefive")