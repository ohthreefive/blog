+++

date = "2019-02-17T20:41:42Z"
title = "Summaries: Death’s End"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Death's End

**Cixin Liu**

## Preface

- A brief introduction by a narrator who says that the events which we are about
  to read are designed to be what he calls a frame as the exact details of the
  events have already been recorded and sent out into many universes, to be
  discovered and read.
- He hopes this frame will aid in that understanding.
- The narrator says the events did not take place in the past, are not taking
- place now and will not take place in the future, and that he regrets that this
  is the case.
- He also makes some comments which appear to grant him some sort of
  omnipotence: specifically he talks about moving the Sun.

## Part One

### May 1453, C.E.: The Death of the Magician

- Emperor Constantin is enduring what is sure to be the last days of
  Constantinople, under siege by the Ottomans.
- A prostitute is brought before him, claiming magic powers (by presenting an
  un-stealable artefact) and passing his test by inexplicably presenting the
  brain of a heavily guarded prisoner without external damage to him.
- She claims to see these inaccessible places as, "Open," to her.
- Her magic can only be practised from one place (a ruined minaret) and no one
  must go there or it will stop working, she says.
- She is given a mission to assassinate the attacking Sultan.
- When she fails to do so, she is confronted in the minaret.
- Inexplicably, the minaret is now intact.
- She tells the soldiers that she can no longer go, "There," a place she says
  has so much space.
- She is killed; Constantinople falls.
- The narrator reveals that the magic practised by the prostitute was a
  byproduct of a high dimensional fragment falling through the Earth and was
  temporary.
- The author concludes the chapter definitively by stating that everything has
  an end – clearly a reference to the novel's title.

### Crisis Era, Year 1: The Option for Life

- Yang Dong is visiting a particle accelerator, which has been shut down, to
  collect some of her things.
- She has recently discovered hidden files on her mother's (Ye Wenjie)
  computer, giving her Trisolaran information about the (crowded) universe.
- Before leaving the accelerator, she speaks to the only scientist working
  there.
- He is running a mathematical model of the Earth on supercomputer.
- He shows Yang Dong what the Earth would look like if life had never existed:
  it is a barren wasteland.
- His point is that life has shaped the Earth rather than the Earth simply
  sustaining life.
- She asks if the same thinking could be applied to the universe however the
  scientist says life is so rare within the universe that the universe would be
  unchanged if life had never existed.
- Yang Dong of course knows the truth that the universe is full of life.
- The chapter concludes with Yang Dong questioning if nature is really natural.

### Crisis Era: year 4: Yun Tianming

- Yun Tianming is receiving palliative care for metastatic lung cancer.
- He notes that the news does not often mention the Trisolaran crisis anymore: 4
  years in to the Crisis Era and with 396 years still to go, attention has
  wandered.
- However, he notes a recent euthanasia law has been passed and an acquaintance
  in the hospital plans to use it.
- He is visited by an old school friend, who tells him that he has turned an old
  idea of Tianming's into a profitable business and wants to give him $3m.
- Yun Tianming learns his cancer is incurable, regardless of money, so instead
  decides to buy a star and give it to a lost love from his past.

### Excerpt from A Past Outside of Time: Infantilism at the Start of the Crisis

- The effects of the Trisolar crisis on humanity (including religion, culture et
  cetera) was so great that there are barely any analogies in history.
- This is thought to be the reason for the erratic behaviour of humanity in the
  first 20 years of the crisis – the two prime examples being the Wallfacer
  project and The Stars: Our Destination project.
- The Stars: Our Destination project had two motivations: to raise money for the
  UN and as a response to the rise of escapism.
- The UN planned to sell nearby stars.
- It was largely a failure.

### Crisis Era, Year 4: Yun Tianming

- We follow Tianming buying his star for his last love, Cheng Xin.
- His money is insufficient to buy any any attractive star.
- However, as he is the first Chinese person to want to purchase a star, the
  government sets the price artificially low so he can buy one.
- He returns to the treatment centre and begins the euthanasia process, during
  which he reminisces about his unrequited love for Xin.
- As he is answering the final question, doctors and nurses burst into the room
  and stop the process.
- Behind them is Xin, who tearfully tells him that the euthanasia law was passed
  specifically for him.

### Crisis Era, Years 1-4: Cheng Xin

- Cheng Xin, the object of Yun Tianming's affection, was a jet propulsion
  engineer.
- She strongly believed that chemical propulsion engines were already a thing of
  the past.
- After the beginning of the crisis, she went to work for the Planetary Defence
  Council's (PDC) Strategic Intelligence Agency (PIA), focused on Trisolaris and
  the fleet rather than Earth defence.
- The PIA was led by an American called Wade.
- He believed they should devote their time to sending a probe to intercept the
  Trisolaran fleet and spy on it.
- This would require achieving 0.1 times the speed of light speed.
- Only Xin could come up with an idea to get a probe there in time: that being
  nuclear propulsion (setting off nuclear bombs behind the probe, which would be
  powered by a 'nuclear sail.')
- The plan was rejected by the PDC.
- Undeterred, Wade changed the plan to include a human spy with in the probe.
- One of the PDCs criticisms of the plan was that the probe would have too few
  sensors to provide any real information.

### Excerpt from A Past Outside of Time: Hibernation

- The hibernation technology was initially heavily regulated due to the fact
  that people thought death would be cured within a few centuries and therefore
  its popularity was immense.
- However, after the onset of the Trisolar crisis, research and funding of
  hibernation was allowed to progress unconstrained as the future no longer
  held any great attraction.

### Crisis Era, Years 1-4: Cheng Xin

- The PIA are attending the PDC on the same day that the Wallfacer project is
  announced and the assassination attempt is made on Luo Ji.
- NASA have conducted early experiments on the nuclear sail option and conclude
  that it will not work unless the mass being sent is significantly cut.
- The group are disheartened but Wade has a plan: to send just a brain.

### Excerpt from A Past Outside of Time: The Staircase Program

- The nuclear sail is likened to historical inventions (such as the repeating
  crossbow) which used primitive technology to 'mimic' future technological
  advance (for example the machine gun.)
- It is noted however that they did not really represent technological advances.
- A new 'space race' would not arise from the nuclear sail: it was just a
  desperate attempt to make do with the available resources.

### Crisis Era, Years 5-7: The Staircase Program

- Xin's colleague, who's brain would have been ideal for the probe, dies in a
  car accident caused by retinal haemorrhages secondary to undiagnosed
  leukaemia.
- However, due to the nature of the car accident, they could not recover his
  brain in time.
- Xin briefly wonders if his leukaemia had been induced in an attempt to recruit
  him.
- Tianming performs well in his testing (there are 6 potential brains) by
  showing a lack of reliance on humanity and lack of connection to the world.
- Wade tells Xin there is only one thing Tianming is attached to; she fails to
  understand.
- Tianming passes the final test by refusing to swear an oath of allegiance to
  planet Earth.
- Only one other recruit refuses the oath but she dies as a complication of her
  illness shortly after.
- Just before Tianming is euthanised and his brain removed, Wade tells Xin that 
  he bought the star for her.
- Xin is immediately remorseful and tries to stop the process but is too late.
- A neurosurgeon consoles her that perhaps he will be cloned by the Trisolarans.
- Wade asks Xin to go in to hibernation to liaise with Tianming in the future.
  She agrees.
- The probe is launched but after 997 nuclear detonations, and after reaching
  speeds never before achieved by humans, it is blown off course.
- Despite this, Xin is still put in to hibernation: partly to represent and
  defend the Starcase Program in the future.

## Part two

### Deterrence Era, Year 12: Bronze Age

- The crew of the ship Bronze Age are returning to Earth.
- It is 14 years since the destruction of Earth's fleet and ensuing battles
  amongst the survivors, including the destruction of Quantum by Bronze Age.
- For the first 18 months, communications continued from Earth then they stopped
  abruptly in November of Crisis Era year 208.
- The crew are excited to be returning home, having been adrift in space for so
  long, and foresee heroes' welcomes.

### Excerpt from A Past Outside of Time: Nyctohylophobia

- Humanity's discovery of the universe as a Dark Forest led to an immediate ban
  on radio transmissions and the like.
- This is despite the fact that even at their peak, the electromagnetic waves
  generated by mankind would only have reached two light years away.
- Future developments by mankind included transmission via neutrinos and
  gravitational waves, the latter of which played a part in the Deterrence Era.
- The Dark Forest discovery caused mankind to become paranoid and afraid.

### Deterrence Era, Year 12: Bronze Age

- The natural response on Bronze Age to the cessation of communication from
  Earth was that Earth and the Solar System had been completely conquered by
  Trisolaris.
- Therefore, they made a course for the nearest star with potentially habitable
  planets.
- Ten days later, a surprise transmission from fleet headquarters explained that
  Earth had developed a deterrence and that both Bronze Age and Blue Space,
  which had also been sent the transmission, should return home immediately.
- Naturally, Bronze Age was sceptical of this at first.
- Just as they were about to ignore the order and head out into the stars, a
  Sophon appeared and established a connection with Earth, and Bronze Age were
  told that they were considered heroes for surviving the destruction of the
  fleet and were to return home as such.
- They were met by the first ship built since the destruction of the fleet,
  Gravity.
- Gravity's entire hull was a gravitational wave antenna which, along with the
  gravitational waves transmitters on Earth, formed Earth's main Deterrence
  mechanism.
- In the time since, Earth was now represented by a single space fleet rather
  than the three previously. All members of Bronze Age were invited to join the
  new space fleet but also, if they wished to retire, they would be allowed.
- Once all the crew were out of the ship, however, it was announced that they
  were all dishonourably discharged from the space fleet for their part in the
  fighting after the doomsday battle and that they would be tried for crimes
  against humanity.

### Deterrence Era, Year 13: Trial

- The crew of Bronze Age are put on trial for the destruction of Quantum.
- The captain attempts to assume all responsibility but given the overwhelming
  evidence that a large majority of the crew voted in favour of the attack, he
  is unable to convince the judge.
- Other senior crewmembers told their stories: eg of how the desperate situation
  completely changed the mindset of all people on board the ship in a way that
  those on Earth could never understand; or about the cannibalism (of the
  Quantum crew) performed by the members of Bronze Age in their desperation not
  to run out of food.
- They are found guilty, other than just over 100 of the 1800 crewmembers.
- The most senior half dozen are sentenced to life in prison and 1700 others are
  sentenced to between 20 and 300 years.
- The prison is off-world and so all these crewmembers never land back on Earth.
- The captain manages to briefly escape and send a message to Blue Space telling
  it not to return home.
- He is killed while doing this.
- Blue Space immediately accelerates, chased by Gravity and two of the
  Trisolaran droplets.
- Although the droplets could catch and destroy Blue Space quickly, Earth wanted
  to deal out justice itself and so the droplets are just an escort for Gravity.
- Gravity is only 10 years younger than Blue Space and so its engines are only
  slightly faster, meaning it would be 50 years before Gravity catches Blue
  Space.

### Deterrence Era, Year 61: The Swordholder

- Cheng Xin is awoken after about 300 years.
- We learn that the world has been able to return somewhat to nature given the
  prolonged underground habitation.
- However, since the Deterrence Era, humans have begun to live on the surface
  again.
- They have also rejected masculinity with the men of this era looking more like
  women.
- Humanity has developed the tools necessary to investigate further reaching
  stars and her star has been discovered to have two promising planets.
- None of the other owners of stars remain alive, nor do their descendants.
- When she wakes, she is given liaison known as AA who has studied people
  from her time.
- The planets and the energy resources from her star are sold to the UN but
  keeps the star for herself. This makes her incredibly rich.
- AA quits her job for the UN to work for Xin.
- We learned that Luo Ji is being considered for trial under mundicide for his
  destruction of the possibly inhabited planet.
- AA wants to show Xin something about the new world but Xin is intercepted on
  her way by Wade, who has also re-awoken.
- He tries to kill Xin saying that he wants to be the Swordholder, though he
  does not say what this is.
- He admits that in the past, he helped induce the leukaemia that killed Xin's
  colleague.
- His ancient gun misfires on third attempt and he is apprehended by the police
  before he is able to fire again.

### Excerpt from A Past Outside of Time: The Ghost of the Wallfacers

- Deterrence game theory was a new field after Luo Ji's intervention.
- Ji handed control of the deterrence mechanism to the UN shortly after its
  initiation.
- The deterrence mechanism worked because of Trisolaris' belief that it
  **would** be used: it would destroy both civilisations.
- The UN realised that Luo Ji's distance and disconnection from humanity was
  actually his greatest strength and within 18 hours, handed back control of the
  deterrence mechanism to him.
- Trisolaris did not act (for example trying to destroy the ring of nuclear
  bombs around the Sun) in this 18 hours, which is thought to have been one of
  their greatest strategic mistakes.
- The deterrence mechanism and deterrence game theory is likened to Cold War
  strategies.
- Deterrence is also likened to the sword of Damocles, with Luo Ji likened to
  the single horse's hair holding the sword and therefore the term Swordholder
  was created.
- Any Earth policy not approved by Luo Ji was not taken seriously by Trisolaris
  and so he quickly became seen as something of a dictator.
- Two factions are mentioned:
    1. the Hawks, wanted to use the Swordholder to impose more severe sanctions
       on Trisolaris.
    2. the Doves, wanted the entire universe to be considered 'human' for the
       purpose of human rights, and therefore they believed Ji should be tried
       for mundicide.
- Luo Ji ignored both groups.
- There was also consideration of using an AI to control the deterrent mechanism
  but the AI was tested and performed poorly at deterrence decision-making,
  having an AI as a dictator was even less politically welcome than having Ji
  and sophon manipulating of artificial intelligence was possible.
- Luo Ji was Swordholder for over 50 years and into his 11th decade.

### Deterrence Era, Year 61: The Swordholder

- Xin is recovering well in hospital.
- The doctors say that as long as her brain is intact, they have the technology
  to revive her even if she had been more gravely injured.
- I wonder if this may play a part later in the brain-in-the-probe plot.
- There has been no murder on Earth for 28 years and so the police were slow and
  inefficient in investigating when another Swordholder candidate had tipped
  them off that Wade might be up to no good.
- Mysteriously, this other Swordholder candidate is not named.
- AA shows Xin art, including music, film and paintings, from this era.
- Xin finds them astonishingly beautiful and is then surprised to learn that all
  of these artworks were created by Trisolarans.

### Excerpt from A Past Outside of Time: Cultural Reflection

- Much to the surprise of humanity, Trisolaris quickly sheared minutes
  scientific knowledge and removed the sophon block.
- In fact, Trisolaris was disappointed by the slow rate at which humanity
  absorbed the knowledge.
- In return, Trisolaris observed human culture and marvelled at its complexity
  and beauty.
- Within 10 years, they were producing beautiful and subtle works of art.
- Their home world, culturally, began to reflect Earth.
- This sharing is termed the cultural reflection.
- The only thing Trisolaris did not share was information about its own home
  planet and culture, which they claimed was inappropriate to share with Earth
  at this time.

### Deterrence Era, Year 61: The Swordholder

- Cheng Xin is taken to meet Sophon, and android created by the sophons on Earth
  to be an ambassador for Trisolaris.
- She is struck by her grace and beauty.
- Sophon prepares tea for her and a careful ceremonial way and says that she
  must volunteer to be Swordholder so the careful balance can be maintained.
- Next, Cheng Xin meets six other men from her era, all of whom are Swordholder
  candidates.
- They argue with her vehemently that she should not put her name forward to be
  the Swordholder.
- In their eyes, this modern era has become soft and believes peace with
  Trisolaris will be reached.
- Therefore, they put less emphasis on selecting a strong Swordholder.
- Therefore, if she puts her name in for selection, she will win.
- Later, Cheng Xin tries to explain the mindset of power seeking Common Era
  people to AA.
- She also tried to explain that Wade was not the most dangerous Common Era
  person as he showed his intentions externally.
- These six men wore impassive masks and it was unable to determine what their
  underlying motives were.
- Next, Cheng Xin goes to the UN for a formal ceremony to handover ownership of
  her two planets.
- An ambassador tries to convince her to enter the race to be the next
  Sworholder.
- He says that if any of the Common Era man becomes Swordholder, there will be
  disharmony and concerns that he will align with the anti-Trisolaran people on
  Earth.
- Cheng Xin is overwhelmed with emotion, especially as she is treated as some
  sort of Messiah by the crowds gathered there.
- She feels a maternal protection for this world in which she has awoken.
- We learn that Cheng Xin is an orphan, having been found by her mother on a
  park bench.
- When the time came for her to go into hibernation, she was unable to tell her
  adopted mother the truth but she could tell that her mother knew anyway.
- With this sacrifice in mind, and knowing about Tianming floating in space, she
  decided she was duty-bound to endure the torture of being the Swordholder and
  announced her candidacy.

### Deterrence Era, Year 62: Gravity in the Vicinity of the Oort Cloud

- Over 50 years, Gravity has gradually caught up with Blue Space.
- Despite repeated hailing attempts to get Blue Space to surrender, they have
  never responded.
- Recently, the sophons on Gravity had abruptly lost their ability to
  communicate through quantum entanglement, meaning communications with Earth now
  involved a 30 month round trip.
- The loss of the quantum entanglement is described as not entirely
  unpredictable.

### Excerpt from A Past Outside of Time: More Indirect Evidence of the Dark Forest

- As well as the sophons on Gravity, all sophons sent by Trisolaris to explore
  the universe at some stage lost their quantum entanglement.
- Any planets encountered by sophons were found to be uninhabited.
- The theoretical conclusion is that any inhabited planets seek to hide their
  locations by destroying this sophons.

### Deterrence Era, Year 62: Gravity in the Vicinity of the Oort Cloud

- Gravity could no longer see inside Blue Space due to the loss of the sophons.
- The droplets were now commanded by their own on-board artificial intelligence.
- The captain of Gravity decided the best course of action was to accelerate at
  full throttle.
- For the first time, Blue Space communicated, offering surrender of 2/3 of
  their crew including the main suspects.
- This was rejected.
- As the droplets could easily destroy Blue Space, when the ships were 15
  astronomical units apart, Blue Space surrendered and the crew of Gravity awoke
  to prepare their ship for combat.
- Four days after awakening from hibernation, the psychiatrist on board Gravity
  received his first visit: from the head of security on Gravity.
- He tells the psychiatrist that recently while walking the corridors of
  Gravity, he was saluted by an officer from Blue Space.
- The psychiatrist believes that the sleep deprivation, stressful situation and
  extensive surveillance suffered by and performed by the head of security was
  the reason for this apparent hallucination.
- However, the security chief says he was mentally sound after the Doomsday
  battle and believes what he saw.
- Later, an accident occurred within Gravity.
- Some tubing within an ecological area was damaged by what was unequivocally
  and micrometeorite.
- This was despite the fact that there was no hull damage and that the velocity
  achieved by the micrometeorite was such that it could not have originated from
  within the ship.
- One of the engineers in the ecological area had had another strange experience
  earlier that day.
- This engineer had awoken one night to see a spherical hole cut in the wall of
  his living quarters. Through the hall, he could see into his wardrobe and his
  next door neighbours cabin. Her legs were cut off by this fear.
- He went to sleep until it disappeared. When he saw his neighbour, her legs
  were fine.
- He was reticent to visit the psychiatrist as he thought he would likely be
  subjected to psychological evaluation and put into hibernation.
- The psychiatrist goes to visit another man: as civilian scientist on board.
- He has become claustrophobic overnight.
- He says he had a dream or at least he thinks it was a dream in which she was
  in and unfathomably wide open space.
- Now, the ship Gravity is causing him claustrophobia.
- He then meets an analogy of the universe as a paraplegic bloated corpse.
- As the universe is 16 billion light years in diameter and expanding
  constantly, and the speed of light is only 300,000 km/s, nothing will ever reach
  from one end of the universe to the other.
- The analogy is that the neural signals of a paraplegic persons brain will
  never reach their limbs and similarly their limbs could never be aware of there
  was a brain.
- Some parallels are being drawn here with the magician in the Constantinople
  chapter.
- The chapter ends abruptly with an alarm sounding aboard Gravity due to an
  imminent droplet attack, which both the scientist and the psychiatrist know they
  will not survive.


### Deterrence Era, Year 62: November 28th, 4pm - 4:17pm, Deterrence Centre

- Xin descends 45 km from the surface to the deterrence mechanism centre to
  complete the handover ceremony.
- She finds its design spartan.
- This is because Luo Ji's only instruction was that the centre should feel like
  a tomb.
- She feels a great weight of expectation on her shoulders.

### Excerpt from A Past Outside of Time: The Choice of the Swordholder

- The gravitational wave transmitter technology was given to Earth by
  Trisolaris.
- At first, plans were made for 100 such transmitters however due to the price
  and the fact that the technology could not be miniaturised, this was reduced to
  23.
- New radical factions on Earth, such as the Sons of the Earth movement,
  attempted to capture one of the transmitters, so their number was reduced to 4
  for safety.
- Four of the ten droplets were banished to a distance that would take them 10
  minutes to reach Earth: the length of time the sword holder with have to make
  their decision in the event of an attack.
- Two of these droplets then accompanied gravity on its mission.
- Of the remaining six, Trisolaris said they rejoined the fleet but they were
  not believed on Earth.
- Of the many things Trisolaris had learnt from Earth was the ability to deceive
  and hide one's motives.

### Deterrence Era, Year 62: November 28th, 4pm - 4:17pm, Deterrence Centre

- Xin observes Ji in his last 10 minutes at as Swordholder.
- He steadfastly carries on until the last minute, his concentration never
  wavering.
- At the end of his watch, he rises with physical strength unexpected from a
  101-year-old man, bows to his enemy, gives Xin the trigger and leaves,
  ignoring his arrest for mundicide.
- Xin's tenure as sword holder would last 15 minutes compared to Ji's 54 years.

### The Final Ten Minutes of the Deterrence Era, Year 62: November 28th, 4:17:34pm - 4:27:58pm

- As soon as Ji has left the deterrence centre, the alarms begin, warning of an
  incoming droplet attack.
- The ultimate error of Xin was that in all her preparation, she did not truly
  believe the possibility of an attack could happen.
- She saw her role as protector, not destroyer.
- In the 10 minutes it took the droplets to reach Earth, the weight of history
  is on Xin but she knows she will not act.
- Her only comfort is that soon, at her torment will be over.
- However, the droplets destroy all the gravitational wave transmitters but
  leave her alive.

### Post-Deterrence Era, First Hour: A Lost World

- Xin returns to the surface where she discovers no one understands what has
  happened.
- The destruction of the Asian gravitational waves transmitter is seen on the
  surface as a volcano triggered by an Earthquake.
- She is filled with sorrow and wants to comfort her companion AA, but finds her
  completely unaware as well.

### Excerpt from A Past Outside of Time: Reflections on the Failure of Deterrence

- The wrong choice of second Swordholder was the ultimate mistake of mankind,
  but there were other fundamental flaws.
- The gravitational wave transmitters were too easily destroyed. Even if there
  had been 100 of them on Earth, it would not have taken the droplets long to
  destroy them. However, had they been scattered in space as multiple ships like
  gravity, Trisolaris would not have had the confidence that they could have
  destroyed them all without deterrence being enacted.
- Reasons this path was not chosen include the vast expense of Gravity.
- In addition, should a single ship go rogue, it would have the power to destroy
  Earth and Trisolaris, a thought which was terrifying both civilisations.

### Post-Deterrence Era, First Hour: A Lost World

- Xin travels to the crater above the Asian gravitational wave transmitter.
- She considers casting herself into it but is stopped when Sophon appears.
- Trisolaris knew Earth of this era would choose Xin.
- She explains that as soon as she was chosen, Trisolaris alerted the droplets
  of the time of the handover so that they were to destroy Gravity at this time.
- Xin finally realised some truths.
- Humanity clearly benefited more from the peace than Trisolaris but she had
  failed to see this from the enemy's point of view.
- She had been kept alive to allow anyone else with a previously unknown
  deterrence mechanism to also shirk their responsibility - using Xin's failure
  as their scapegoat.
- A devastated AA tries to attack Sophon but is easily beaten away.
- Sophon concludes by asking for the regards of Trisolaris to be passed on to
  Ji, who they greatly respected.
- They also say an apology should be given to Wade, whose resolve was such that
  Trisolaris knew they would never be able to attack if he was Swordholder.
- It was even possible that in the time of his being Swordholder, Earth would
  have caught up technologically with Trisolaris.
- The chapter closes with Sophon telling Xin and AA to get ready to go to
  Australia.

### Post-Deterrence Era, Day 60: A Lost World

- A second Trisolaran fleet was detected however evidence suggested it could
  travel at light speed.
- It appeared reticent to travel at this speed close to Trisolaris or close to
  our Solar System.
- It was presumed that this was because light speed travel could cause damage to
  either place.
- Calculations estimated the power generated by these faster than light drives
  being two orders of magnitude greater than that of the Sun, which seemed
  incomprehensible.

### Excerpt from A Past Outside of Time: Technological Explosion on Triolaris

- It was generally thought that the influence of human culture, for example
  freedom of speech and thought, was responsible for the technological explosion
  on Trisolaris, which had previously been totalitarian due to the chaotic eras.

### Post-Deterrence Era, Day 60: A Lost World

- Sophon declared that humanity would have one year to relocate to Australia.
  The population was currently 4 billion.
- Trisolaris now 'loved' humanity so would not destroy them utterly.
- When the fleet arrived, they would help mankind settle portions of Mars and
  elsewhere in space and coexist peacefully.
- Sophon was initially ignored so a droplet attack killed three hundred thousand
  was called.
- Now, if the relocation was not complete within the year, all other humans
  would be killed.

### Post-Deterrence Era, Year 2: Australia

- Over the course of a year, 4.2 billion people relocate to Australia and New
  Zealand when Australia becomes to fill.
- It is a hellish time, with infighting and vast numbers of casualties.
- 8 million people remain without: 5 million on Mars and elsewhere in space, 1
  million in the Earth security force working for Sophon and 2 million in the
  Earth resistance.
- Xin is generally shunned and lives with AA and an aboriginal called Fraisse.
- She visits Wade who advises she leaves Australia by whatever means she can.
- At the end of the resettlement, Sophon announces that for three months,
  humanity will be banned from using any electricity, meaning they will have to
  fight, kill and eat each other to survive.

### The Final Ten Minutes of the Deterrence Era, Year 62: November 28th, 4:17:34pm - 4:27:58pm

- We are back aboard Gravity and introduced to a cook named Hunter.
- In actual fact, he is a high-ranking official, working undercover on Gravity
  as a person able to disable the gravitational wave transmitter in the event of
  Gravity being captured.
- In that respect, he is equal and rank to the captain.
- Hunter has been unable to go into hibernation and so when the droplet attack
  is sounded, he is actually relieved, but he watches as both droplets miss
  their targets.
- He watches a shuttle from Blue Space apparently repel and stop the droplets,
  examine and retrieve something from it.
- He tries to activate the gravitational wave transmitter but the disarmament
  switch fails; the captain’s mechanism also fails.
- He tries to manually destroy the transmitter but encounters a member of Blue
  Space's crew already there.
- In the ensuing fight, the Blue Space officer uses a weapon which removes
  Hunter’s heart without damaging his skin, calling into mind the Constantinople
  chapter.
- Blue Space captures Gravity and tells them they have detected a second
  Trisolaran fleet heading for Earth; deterrence on Earth has failed.
- The only course of action they can see is to activate Gravity’s gravitational
  wave transmitter, which within two months will have decayed to uselessness
  anyway, and try to buy time for the human race to escape Earth.
- A vote is held and the gravitational waves transmitter is activated.

### Post-Deterrence Era, Year 2: The morning After the Great Resettlement, Australia

- The now blind Cheng Xin is in a crowd, listening to Sophon speak but
  struggling to understand over what she comes to realise is cheering.
- News of the activation of the gravitational waves transmitter has reached
  Trisolaris, who are now in full retreat.
- Sophon has said that the Earth Security Force has been ordered to help
  evacuate Australia.
- Xin has been told she will be allowed to be amongst the first to return home
  to China.

### Post-Deterrence Era, Day 1-5: Gravity and Blue Space, Deep Space Beyond the Oort Cloud

- The principle characters are the civilian cosmologist from Gravity, Guan
  Yifan, Gravity’s captain, Morovich, its psychologist Dr West and Blue Space’s
  captain, Chu Yan.
- Chu takes Guan & Morovich in to a ‘warped point’, a spherical anomaly through
  which the 4th dimensional fragment could be entered.
- The 4th dimensional fragment, likened to a soap bubble on a bit of paper, with
  the paper being the 3 dimensional universe, is about the size of the solar
  system.
- In the fourth dimensional fragment, three dimensional objects are completely
  open.
- The analogy is a three-dimensional being looking at a circle: they can see the
  inside of the circle without opening it up. A two-dimensional being on the
  surface of the same paper as the circle could only see its edges.
- Blue Space discovered these secrets quicker because of its larger crew and the
  less strict psychological restrictions on the crew meaning they were more
  forward in divulging their encounters.
- Being in 4 dimensional space was dangerous – micrometeorites could pass
  through the hull of their three-dimensional shape as if it was not there.
- However, this is also how Blue Space disabled the droplets: by destroying
  their internal mechanisms from 4 dimensional space without having to
  understand them.
- Navigating in four dimensional space was also dangerous: instruments on their
  three-dimensional ship could not measure the fourth dimension and their eyes
  also struggled to perceive this fourth dimension.
- However, scientists were able to discover over 100 apparently fabricated
  objects within this fragment and, although the captain of Blue Space ordered a
  retreat to outside of the fourth dimensional fragment, the cosmologist was
  able to argue for a single exploratory visit to the closest object.
- It appeared like a giant floating ring and its insides were not visible from
  within the fourth dimension, meaning it must be a four dimensional object.
- They discover the ring is a tomb by conversing with it, or rather the control
  system of the ship.
- It tells them the fourth dimensional fragment is like a puddle drying out and
  this tomb and the other objects they have detected are all like fish who are
  either already dead or dying.
- The Dark Forest also exists in four dimensional space and the race which
  caused the fragmentation of four dimensional space is not affected by these
  drying puddles.
- The tomb tells them not to worry as lower dimensional civilisations are no
  threat to higher dimensional civilisations so the Dark Forest principle does
  not exist between levels of dimensionality.
- Gravity and Blue Space retreat out of the fragment.
- They theorised that a four dimensional fragment existing and three-dimensional
  space must inevitably decay and watch as this does indeed happen.
- Before moving on, any humans who wish to return home were allowed to do so,
  and about 200 souls chose this path.

## Part Three

### Broadcast Era, Year 7: Cheng Xin

- Cheng Xin’s blindness is diagnosed as initially psychosomatic followed by
  retinal detachment.
- To treat it, a new retin is grown from her stem cells, a process taking at
  least five years.
- She went into hibernation for this period.
- In her absence, AA grew her company into a massively successful organisation.
- Evacuating Australia had taken four months and the Earth Security Force were
  responsible for keeping order in this time.
- However, as they had been aiding Trisolaris in the past, they were all put on
  trial and half convicted of crimes against humanity.
- Capital punishment had returned and many were put to death.
- On the other hand, the crimes of Blue Space were forgiven and the Earth
  Resistance were hailed as heroes.
- Peace returned as most people thought they would be dead before a Dark Forest
  strike on Earth.
- Many also started by casting doubt on the Dark Forest theory.

### Excerpt from A Past Outside of Time: Delusions of Cosmic Persecution

- During the Broadcast Era, all evidence for Dark Forest theory was heavily
  criticised and humanity moved heavily towards thinking that Dark Forest theory
  was just a, "Delusion of cosmic persecution."

### Broadcast Era, Year 7: Cheng Xin

- Cheng Xin’s life was one of constant misery, one of the only happy things
  being her conversations over the phone with Fraisse.
- One night however, Fraisse phoned her and told her to look up.
- The destruction of Trisolaris and the confirmation of the Dark Forest theory
  was visible in the sky above them.

### Excerpt from A Past Outside of Time: A New model for Dark Forest theory

- Based on calculations around the destruction of Trisolaris, it was concluded
  that Dark Forest strikes originated from spaceships, not planets as was
  originally thought.

### Broadcast Era, Year 7: Sophon

- Sophon invites Luo Ji and Cheng Xin for tea.
- The UN and Fleet International are obviously very interested in this and plan
  some questions.
- Sophon tells Cheng Xin that humanity probably has longer left until they are
  destroyed than Trisolaris had.
- One reason is that Trisolaris was a bigger threat in the universe than Earth
  but Sophon absolutely refuses to elaborate on this.
- Sophon grants Luo Ji one question to which he will be given an honest answer,
  but that answer can only be yes, no or I don’t know.
- Luo Ji asks if mankind can broadcast a signal or sign to demonstrate its
  passivity in the Dark Forest which will ensure that safety.
- Sophon’s answer is yes.

### Excerpt from A Past Outside of Time: The Cosmic Safety Notice

- Mankind came together to attempt to solve this riddle.
- However, given Sophon’s assertion that Dark Forest strikes were casual and
  therefore no civilisation would investigate a safety notice, no answer could
  be found easily.
- In addition, no one understood why Sophon and Trisolaris would not simply give
  them the answer: it seemed to pose no threat to Trisolaris.

### Broadcast Era, Year 7: Sophon

- Religion had re-established itself on Earth.
- Trisolaris, once seen as gods, then as monsters, became angels or saviours as
  they were able to give them the secret to the cosmic safety notice.
- The religious believed they needed to pray to them sufficiently for this to
  happen.
- Blue Space had become devils again, their decision to use deterrence causing
  this uncertain state on Earth.
- Cheng Xin’s image had been rehabilitated, with Earth feeling pity for her and
  her struggles.
- This increased Xin’s depression and she prepared to commit suicide.
- However, phone call from Fraisse stopped her.
- This was followed by a phone call from Sophon, inviting her and Luo Ji for
  tea.
- Sophon declared she was leaving the next day but had one more mission to
  complete: Tianming wanted to speak to Xin.

### Excerpt from A Past Outside of Time: The Long Staircase

- The staircase program had been forgotten by history or viewed as a failure and
  so it was with surprise that a relic of it appeared to provide humanity’s
  salvation.
- The main worry on Earth was whether or not Tianming still acted with
  humanity’s interests at heart
- People were unsure how the first Trisolaran fleet encountered Tianming but it
  was at least partly down to luck.

### Broadcast Era, Year 7: Yun Tianming

- Cheng Xin travels to meet Yun Tianming.
- The meeting will be monitored by Triolaris and if any information that
  Triolaris does not want to be exchanged is, Xin will be killed without
  warning.
- The meeting is therefore initially tentative.
- Tianming has had a new body cloned and is farming the seeds Xin packed for him
  within an odd dome.
- Tianming unexpectedly changes the conversation to discussing fabricated
  fairytales which he says that he and Xin used to exchange as children and he
  has been telling the people of Triolaris for years.
- These are, of course, fake and have been strategically invented by Tianming to
  impart information secretly.
- Tianming says he was the author of many of the tales but Xin was the author of
  some.
- He tells her back three of the stories which she allegedly authored.
- Xin memorises them and on return to Earth, records them in a sophon-free room
  (a new technology.)
- The parting words of Xin to Tianming are the same as of Sophon to Xin: "The
  universe is grand but life is grander.”

### Broadcast Era, Year 7: Yun Tianming's Fairytales

- A deciphering committee has been established to get to the bottom of
  Tianming’s fairytales.
- It is noted how much work he put into creating them and implanting them in
  Trisolaran culture.
- His comment that he and Xin should meet at their star is taken to mean that he
  does not believe he will ever return Earth and or also that the Earth will not
  survive.
- During the initial debate, Xin believes some credit should be given to Wade as
  the creator of the staircase program but no one shares her belief.

### The First Tale of Yun Tianming: "The New Royal Painter"

### The Second Tale of Yun Tianming: "The Glutton's Sea"

### The Third Tale of Yun Tianming: "Prince Deep Water"

- Tianming's three tales are one consecutive story.
- The principal players are:
- Two princes and a princess.
    - One prince is evil and seeks the throne, the other is in exile.
    - The exiled prince has the odd characteristic of not obeying the laws of
      perspective.
- Two painters, master and apprentice.
    - The apprentice aids the evil prince, the master comes to help the
      princess.
    - They can literally paint people in to pictures, effectively removing them
      from reality forever.
- A captain of the guard.
- A sea which separates the evil prince from the exiled prince, inhabited by
  foreign fish which can and do devour everything.
- A place with a strange name, from which numerous magical artefacts are said to
  come.
- After a coup by the evil prince aided by the apprentice painter, the princess
  and captain travel to find the exiled prince.
- They manage to cross the deadly sea and bring the exile prince back, who kills
  his brother and takes the throne.
- The princess and captain are said to wander forever in safety thereafter.

### Broadcast Era, Year 7: Yun Tianming's Fairy Tales

- The fairy tales are universally praised for their skill but are thought to be
  too complex ever to be deciphered.
- The strange place name is thought to be key.
- As time goes by, politicians try to nudge the work towards their goals.
- Eventually, the fairy tales are largely abandoned in favour of trying to use
  what mankind has learned from the Dark Forest strike on Trisolaris to survive
  one itself, called the Bunker Project.

### Excerpt from A Past Outside of Time: The Bunker Project

- In essence, mankind will take to the stars, using the gas giants to shield
  themselves from the fallout of a Dark Forest attack.
- This will take at least ninety years.

### Broadcast Era, Year 7: Yun Tianming's Fairytales

- AA is the first to discover a meaning within the fairytales.
- She works out that the princess' boat crossing the sea using soap is a
  demonstration of altering surface tension to cause motion.
- She shares this first with Cheng Xin.
- With the knowledge that space is curved, they work out that the paper being
  flattened in the stories is a metaphor for flattening space behind a
  spaceship.
- The resulting altered surface tension of space would 'pull' the ship forward
  at light speed.
- Thus the method of Tisolaran light speed travel and that by which humanity
  might escape the Solar System has been revealed.

### Excerpt from A Past Outside of Time: Motion Through Bending Space

- Space is not flat but curved.
- Light speed travel by bending space and transferring from one point to another
  had been proposed but rejected in the past as impossible.
- Flattening space behind you (decreasing its surface tension), would mean the
  curved, increased tension space in front of you would pull you forward.
- This was thought to be achievable.

### Broadcast Era, Year 7: Yun Tianming's Fairytales

- With the first mystery solved, more of the fairytales became clear.
- The cosmic safety notice was discovered to be reducing the speed of light.
- Below 16.7 km/s, light could not escape the gravity of the Sun and therefore
  the Solar System would appear as a black hole to the rest of the universe.
- As it was not truly a black hole, it was called a black domain.
- The metaphor surrounding the paintings which captured peoples lives was the
  only one which could not be solved.

### Excerpt from A Past Outside of Time: Three Paths of Survival for Earth Civilisation

- The Bunker Project was popular as it was devised by humanity without the aid
  of Tianming’s messages.
- The Black Domain project was popular as it allowed humanity to remain on
  Earth.
- Its main drawback was that it seemed to be technologically unachievable.
- The third path was achieving light speed travel.

### Broadcast Era, Year seven: Fate's Choice

- A false alarm by the early warning system caused mass havoc on Earth.
- 10,000 people were killed as people try to leave Earth at overcrowded space
  ports.
- It was discovered that wealthy individuals were trying to build lightspeed
  craft for themselves so they could escape in the event of a photoid strike.
- This inequality cause resentment of light speed to travel in the aftermath.
- During the false alarm, Xin expressed a wish to remain on Earth even though
  arrangements had been made for her to be able to escape.

### Exerpt from A Past Outside of Time: Space Sentries

- Dark forest photoid strikes relied on accelerating a mass to close to the
  speed of light.
- As it had mass, it could not reach the speed of light and it could be affected
  by gravity (for example from nearby stars or planets.)
- The photoid itself caused radiation.
- This had no mass and so would arrive at its destination before the photoid.
- The early warning system relied on detecting this radiation.

### Broadcast Era, Year eight: Fate's Choice

- Two days before the false alarm, the first early warning station was
  activated.
- It observe the Trisolaran system and saw two bubbles within the clouds of
  debris.
- One of these was unequivocally the location at which the second Trisolaran
  fleet had entered light speed.
- It was quickly theorised that curvature space propulsion acceleration left a
  footprint of unknown duration on space and therefore could reveal the location
  of the civilisation.
- This station immediately reported this finding back to Earth.
- As the report was unscheduled, it was misinterpreted as a warning before being
  properly analysed, causing the mass panic.
- Light speed research on Earth was banned
- More of these bubbles where discovered, and as they could not be attributed to
  Trisolaris, they were seen as further evidence for multiple civilisations in
  the universe.

### Excerpt from A Past Outside of Time: Terror of the Endless night

- Light speed travel was abandoned by all but the elite/wealthy who continued in
  secret.
- The Black Domain was seen as isolationist by some, though they admitted it
  would guarantee mankind’s safety.
- The Bunker Project also retained some popularity.
- Overall, mankind had gone from a civilisation which looked to the stars to one
  which was afraid of them and unsure how to proceed.

### Broadcast Era, Year eight: Sun-Earth Lagrangian Point

- Xin attends a scientific proof and propaganda exercise in favour of the Bunker
  Project.
- Unexpectedly, she is confronted by Wade who knows that she believes light
  speed travel is humanity’s most progressive option.
- He agrees about this and asks to take control of her company as he believes he
  can deliver on light speed with her resources but she cannot.
- Xin agrees with the caveat that if ever the plan puts the survival of humanity
  at risk, she retains the right to reaffirm control of her company.
- Reluctantly, Wade agrees and Xin and AA go into hibernation.

### Bunker Era, Year 11: Bunker World

- Cheng Xin awakes after 62 years hibernation to find herself upon one of
  humanities space cities orbiting Jupiter.
- Over 900 million people now live on space cities and planet Earth is sparsely
  populated and returning to wilderness.
- There is great variation in design and the wealth of the cities.
- Xin is shown round many of them by an emissary for Wade – she is being asked
  to make her own judgement before going to see him.
- Presumably, Wade has awoken her because his research has put humanity at risk.

### Bunker Era, Year 11: Halo City

- Wade and the Halo group have continued to work on light speed travel.
- This was originally accepted but recently he moved to research curvature
  propulsion.
- This has led to Halo City being blockaded and calls for the Halo group to
  stop.
- Wade has also secretly worked on creating and weaponising anti-matter.
- He has agents on every space city and is ready to defend himself.
- As per his agreement, he gives Cheng Xin the final say over his actions.
- Without hesitating, she tells him to surrender and he agrees.
- He is executed for his crimes and the Halo group is broken up.
- Cheng Xin returns to hibernation.

## Part Five

### Bunker Era, Year 67: Orion Arm of the Milky Way

- An alien who monitors the Dark Forest sees the destroyed Trisolaris.
- Curious, he investigates and notices the odd back and forth between Trisolaris
  and Earth, which is highly unusual in his experience.
- He wants to investigate the Solar System but is told this is a waste of
  resources.
- He recognises that a photoid strike would leave safe spots within the Solar
  System so chooses another weapon.
- He mentions a civil war within his civilisation, with the home world being
  willing to drop in to two dimensional space to survive.

### Bunker Era, Year 67: Halo

- Cheng Xin and Ai AA are re-awoken.
- A Dark Forest strike has been discovered by the early warning system however
  it is not a photoid and humanity is helpless to survive it.

### Bunker Era, Year 66: Outside the Solar System

- Trisolaran physics information during the Deterrence Era was partly falsified.
- The Dark Forest strike takes the form of a business card sized apparent blank
  piece of paper.
- A ship is sent to investigate: they find no way to examine or interact with
  the sheet.
- Without warning, the white cover disappears revealing the weapon as a small
  piece of two dimensional space.
- Just as the four dimensional space collapsed in three dimensional space
  previously, this small fragment of two dimensional space causes three
  dimensional space around it to collapse in to it.
- A scientist on board quickly calculates that the only way to escape this is
  light speed travel.
- The crew have also discovered the mystery of the paintings in Yun Tianming's
  fairy tales.

### Bunker Era, Year 68: Pluto

- Xin and AA are asked to go to Pluto, where a museum of Earth history has been
  created.
- There they meet a nearly two hundred year old Luo Ji, who has been a
  custodian.
- He explains that despite scientific advances, the most durable way of
  recording information was discovered to be rock carvings.
- Xin sees Van Gogh's Starry Night, and sees an illustration of string theory in
  the painting.

### Bunker Era, Year 68: The Two-Dimensional Solar System

- Cheng Xin and AA watch the Solar System to collapse in to two dimensions from
  Pluto, continuing their mission to rescue some artefacts from the museum over
  which Luo Ji is caretaker.
- Seemingly at the last moment, Luo Ji reveals that their ship Halo has the only
  curvature propulsion engine in possession of mankind (there are another two
  but they are currently on un-manned trial runs.)
- He also tells them that the wake of the curvature propulsion engine causes the
  speed of light in a vacuum to decrease dramatically.
- Therefore, of the Bunker Project, the Black Domain Project and light speed
  travel, light speed was the key, allowing both escape and hiding.
- Xin directly blocked light speed research and feels immediate remorse: she
  failed as Swordholder and she failed in her judgement of Wade.
- Luo Ji says he does not want to accompany them, but they should seek out Blue
  Space and Gravity.
- Xin instead choses to seek Yun Tianming at the star he bought for her.

## Part VI

### Galaxy Era, Year 409: Our Star

- AA and Cheng Xin arrive at the planetary system and discover a single person
  is on one of the planets – Guan Yifan, this civilian scientist from the ship
  Gravity.
- He explains much to Cheng Xin including about the Galaxy human civilisation
  and the depths of the horrors of the Dark Forest.
- Galaxy humans developed curvature lightspeed about 200 years after Solar
  System humans.
- They live on four planets, one of which has shrouded itself in a black domain
  as it thought its location had been revealed.
- Beings of god-like power exists and the fundamental laws of physics are used
  as the ultimate weapons in the universe.
- The second Trisolaran fleet has been destroyed in one of these battles but the
  fate of the first fleet and Yun Tianming is not known.
- Sufficiently powerful civilisations can reduce the speed of light to zero,
  effectively nullifying anything they choose to trap within a region of slow
  light speed space.
- Similarly, sufficiently powerful beings are able to reduce their number of
  dimensions to survive a Dark Forest strike like in the two-dimensionalisation
  of the Solar System.
- In fact, it is theorised that all dimensions from 10 down to 3 have all been
  destroyed in such battles and that this three-dimensional universe will
  eventually fall into two dimensions.
- There are races who actively want to accelerate the reduction of dimensions,
  theorising that once all dimensions are collapsed, the universe will reset.
- This is obviously horrific news for Cheng Xin.
- A scout reports activity on the second planet of this Solar System and Guan
  Yifan and Cheng Xin go to investigate.
- Whoever had been to the planet has already left and left behind death lines,
  which are left behind by ultra powerful curvature propulsion engines.
- These death lines are the wake of accelerating ships and within them the speed
  of light is zero.
- They are unstable and if disturbed, can explode outwards rapidly, trapping
  things within them.
- When expanded, the speed of light within them increases to above zero but not
  by much.
- Cheng Xin and Guan Yifan therefore retreat quickly back to the other planet,
  only to be greeted by a message from AA stating that Yun Tianming has
  unexpectedly also arrived on that planet.
- The arrival of Yun Tianming accidentally destabilises the death lines and Guan
  Yifan and Cheng Xin are trapped within very low light speed space, orbiting
  the planet at the speed of light, now reduced.
- Guan Yifan has been preparing for this. Nothing electronic will work, however
  neural computers, which are chemically powered, have been developed.
- These are slow and will take up to 12 days to boot.
- Guan Yifan and Cheng Xin enter chemically induced hibernation for this period
  but, as they are travelling at the speed of light within this domain, due to
  relativity, millions of years will pass in the outside universe before they
  are able to slow their ship down.

### About Seventeen Billion Years After the Beginning of Time: Our Star

- Xin and Yifan land again on the blue planet of Xin's star.
- It is deserted; they calculate they travelled 18 million years in to the
  future by relativity.
- Xin remembers that AA was with her when Luo Ji explained that carving on rocks
  was the most long-lasting form of communication.
- Their ship scans below the surface and discovers a message saying that AA &
  Tianming lived a happy life and have left them a gift.
- Soon after, a door appears before them.
- They step through together.

### Outside of Time: Our Universe

- The Trisolarans (and other advanced civilisations) have developed the
  technology to create self-contained universes.
- The plan is to await the re-setting of the 'greater' universe then re-enter an
  Edenic universe after the next Big Bang.
- Tianming arranged this small universe as a gift to Xin and Yifan.
- Sophon is there to guide them and keep them company: she tells them it will
  take about 10 years in this universe for the greater universe to reset.
- Yifan studies advanced Trisolaran physics and realises that the greater
  universe might not reset if any mass contained within it is lost, ie. they
  cannot remain outside the universe.
- Advanced beings in the greater universe also believe this and manage to send a
  message to all the artificial universes, asking them to return.
- Xin and Yifan realise that though they will not survive, it is better that the
  universe survives, and prepares to return.

### Excerpt from A Past Outside of Time: The Stairs of Responsibility

- Xin is the author of this history.
- She describes escalating responsibilities in her life, eventually leading to
  her having some responsibility for the continuation of the universe.

### Outside of Time: Our Universe

- Sophon, Xin and Yifan dismantle the artificial universe and prepare to return
  to the greater universe.
- Sophon has found just one planet on which they might be able to live out the
  rest of their lives.
- Sophon and Xin work to prepare a self-contained ecosystem, containing some
  plants and fish from Earth.
- They decide to leave this behind in their artificial universe, hoping that
  both it will be discovered by beings from the new universe, and that the small
  amount of mass will not affect the re-setting of the universe.

[Home](/blog "ohthreefive")
