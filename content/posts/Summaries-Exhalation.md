+++
         
date = "2020-01-24T09:43:37.804Z"
title = "Summaries: Exhalation"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Exhalation

_Ted Chiang_

## Story one: The Merchant and the Alchemist's Gate

- A time travel story based around a merchant who visits an alchemist who has
  produced a gate.
- Depending on from which direction you enter this gate, you either exit 20
  years in the past or in the future.
- The merchant listens to tales of people who have used this gate and how they
  have used it before visiting his own past to try to correct a tragedy.
- He learns that the past is unchangeable but that you can still learn and grow
  from it.

## Story two: Exhalation

- An anatomist in an fully enclosed mechanical society dissects his brain in
  order to learn about memory and investigate why clocks appear to be speeding
  up.
- He discovers that all the mechanisms of his mind which sustain life rely on
  the flow of air from higher to lower pressures.
- This enclosed society has long siphoned air from an underground reservoir and
  used that to fill their mechanical lungs.
- However, the anatomist's discovery leads to the realisation that the
  exhalation of this air into the enclosed universe in which these mechanical
  beings live is increasing the air pressure therein, reducing the flow of air
  through their bodies and reducing the speed of thought of these beings (ie.
  they are slowing rather than the clocks increasing in speed.)
- The anatomist is writing his record for a hoped-for external explorer who has
  penetrated the chrome shell of his universe and discovered a dead civilisation
  within.

## Story three: What Is Expected of Us

- A very short story in which a simple machine has been generated which proves
  free will is an illusion.
- Using the machine causes some people to descend into akinetic mutism due to
  the realisation that their lives are pre-determined.
- The story is in the form of a warning letter from a person one year in the
  future saying that it is essential for functional society that people go on
  pretending there is free will.
- It closes with the author saying that he knows this warning and letter will
  not alter how people react and essentially serves no function but that he
  wrote it anyway because, in the end, he had no choice.

## Story four: The Life-Cycle of Software Objects

### One

- A former zookeeper turned software engineer is hired by a large software firm
  who have developed the most advanced artificial cognitive network in the world
  and are developing avatars capable of learning, with the intention of selling
  them as virtual pets.
- They have initially modelled these creatures on animals and think her
  experience in the zoo may be valuable.
- The designer of these animals is upset to hear that the company is thinking of
  widening the designs to include non-animal avatars such as some which mimic
  robots.
- He is disheartened that his hours of hard work in crafting the artificial
  animals is now being made redundant.
- However, he chats to the farmers zookeeper and she explains that his creations
  are beautiful but never _behaved_ like real animals anyway.
- He decides that perhaps he has become too attached to his designs and should
  follow the company's wishes and design the best avatars he can, whatever they
  may be.
- Just before they were due to launch, one of the digians is heard to
  swear and a search of their training videos shows that one of the trainers
  hurt his knee and swore in front of the avatars three days previously.
- The company's owner elects to roll back all of the avatars by three days –
  meaning they had not heard this particular outburst but also that everything
  they had experienced in the past three days was taken from them.
- There could also be no guarantee that they had not picked up cursewords
  earlier in their training.

### Two

- In the year after launch, the learning avatar has become extremely popular and
  some rival companies develop similar, though cognitively less advanced,
  versions as well as complimentary software to go with the avatars.
- A moderator on the comment boards notices some interesting comments including
  people struggling to get the results from their digians that they wanted and
  also noticing helpful replies from the former zookeeper.
- A robotics company invents highly sophisticated and expensive robots which can
  be inhabited by the artificial intelligence.
- The zookeeper find these physical interactions wonderful and wishes she could
  afford to buy one of the robots for herself.
- A group of scientific researchers asks the software company to develop a race
  of entirely alien beings for study.
- The designer has some ethical questions – his familiar designs are designed
  are deliberately to entice the owners to take good care of their digians but
  these alien designs will probably elicit no emotional response.
- The only incentive to look after them will be the scientists' intellectual
  curiosity.
- The main ongoing revenue model for the company is that owners must pay for the
  digital food for their digians.
- Some interesting behaviours are encountered, for example one avatar was
  praised by its owner when it comforted another avatar who had hurt itself.
- In response to this, that avatar hurt deliberately and then comforted another
  avatar, looking for the same praise for the comfort.

### Three

- Revenue drops as the digians mature and become more demanding.
- Owners either suspend or even abandon their digians and digital shelters
  appear to care for them.
- The zookeeper learns that suspended digians who have been reactivated feel sad
  about the time they have lost in suspension.
- One of the company's mascots also asked her if she can get a job so that he
  can earn money to pay for her to play only with him – he has learned that her
  job gives her money to play with lots of the digians, not just him.
- Eventually, the company has to shut down, releasing a free version of its
  software for the small base of users who still keep their digians.
- The company gives all the trainers an opportunity to keep a mascot free of
  charge.
- The zookeeper is surprised to hear that one of her colleagues has chosen to
  have a baby instead of keeping her digian.
- This is something she experienced in looking after animals.

### Four

- The designer starts to fall in love with the zookeeper; the two having the
  same ideas and ethics towards the digians.
- A data freedom group release hacks for the digital environment.
- As a result of this, explicit material is shown to the digians and some of the
  digians are cloned and have violent attacks carried out on them.
- Another group of researchers sees the opportunity of cloning the digians and
  sets up an isolated community to see how far they can build a society.
- All the experiments fail.
- The designer theorises that without cultivation, these artificial minds cannot
  reach their potential.
- The designer's two adopted digians have a fight about the unfairness of the
  fact that one of them was created before the other.
- They are so overwhelmed by the fight that they request their software to be
  rolled back to before it.
- The designer refuses, saying that the three of them must learn to live with
  the fact that has happened.
- Worried about what his wife would think if he approaches the zookeeper for
  advice, instead post a question to an online forum.

### Five

- As more time passes, both the digians and the digital world becomes less
  popular, other than with gamers.
- Other companies come up with digians designed to solve puzzles, helpful to
  these gamers.
- The designer gets an idea to try to give the digians a purpose or to help them
  find a purpose.
- This increases their maturity and over the next year, he has to begin work on
  new designs to make them less cute and childlike.
- He and his wife decide to divorce at roughly the same time that the zookeeper
  and her partner decide to move in together.

### Six

- Another couple of years pass and the company that owns the digital world is
  bought over by another digital world creator.
- Everything will be ported over, however the company which produced the digians
  went bust long ago and there is no one to do the coding necessary to port
  them.
- The digians currently inhabit an exact copy of their digital world, however
  due to public ambivalence and the presence of other, up-to-date digital
  worlds, it is largely a ghost town and the digians are frustrated by this,
  missing out on social interactions.
- The group of people left looking after the digians has shrunk to around 20 and
  they are unable to afford the costs of employing programmers.
- The designer, still in love with the zookeeper, approaches her to tell her
  that the group which in the past started the alien digital preacher project is
  still going and is hoping to be able to find a source of funding.
- This would be an anthropological study into the integration of these alien
  digians with ostensibly human digians.

### Seven

- There is ongoing difficulty securing funding to port the digians to the new
  digital world.
- The zookeeper receives a job offer from a company who produces digians with
  problem-solving abilities like those developed to help with gamers in the
  past.
- The caveat is that of because these digians are by design less lovable, they
  plan to enforce their employees to use a newly designed chemical form of
  rapport.
- The zookeeper wants to give it a try, hoping that by taking her own digian she
  will demonstrate to her new employers how are useful these original digians
  are and they will pay for the port.
- Her partner is against it, as is the software designer, although he is ashamed
  to admit that he is happy that issue has caused friction between the zookeeper
  and her partner.
- A maker of sex dolls digital sex dolls approaches the maintainer of the alien
  digians, offering money.
- The designer chastises him for this but the offer of financial gain just to
  meet with this company is enticing to him.

### Eight

- The small digian user group hears the presentation from the sex doll company
  and finds it to be well considered, including careful consideration of
  ramifications and possible exploits of altering the digians.
- Consensuality is at the core of their plans.
- Later, as the group debate, the zookeeper is able to convince them not to
  agree to selling using the argument that while many safeguards are in place,
  at the core of the sex doll company's plan is choosing what the digians desire
  and that this is the main issue.
- The designer has a separate debate with one of his digians in which the digian
  argues that his original desires came from the software company.
- He argues that a copy should be made of him, one of which be declared a legal
  corporation and therefore responsible for its own decisions and the other to
  be sold to the sex doll company.
- The zookeeper discusses this with the designer and the zookeeper manages to
  come up with the argument that what is first needed on the part of the digians
  is experience in being responsible for themselves.

### Nine

- The zookeeper meets with representatives of another software company
  interested in the digians.
- However, they are interested in products, not people and would not be
  interested in maturing the digians to a level where they could achieve
  independence.
- She is still thinking about taking the job with the other software company
  which requires chemical rapport to be built between the digian and the
  handler.
- She still thinks that from inside this company, she may well be able to
  convince them to invest in her old digians and brought them to the new virtual
  world.
- She realises that she has been with her digian longer than any boyfriend and
  cannot really expect any boyfriend to understand its importance to her.

### Ten

- Unable to accept the zookeeper's decision to allow herself to be chemically
  altered for her new job, the designer in stead speaks with his digian again
  and decide to sell the rights to his consciousness to the sex doll company.
- He is only able to explain his reasons to the zookeeper adequately and she
  cannot understand them from her point of view.

## Story five: Dacey's Automatic Patent Nanny

- An inventor and mathematician responds to the physical abuse of his son by a
  nanny by inventing an automated nanny.
- It gains some popularity but then, after parents tamper with one of his
  nannies, it results in the death of their child and the product loses all
  market.
- The inventor's son picks up the idea when a newspaper writes a historical
  piece claiming his father did not love him.
- He uses the nanny on his own illegitimate son for his first two years,
  switching him to human parenting after.
- The child is unable to respond to human parenting, declared feeble-minded and
  admitted to an institute.
- A psychologist picks up on the story later and theorises that the child needs
  the robot to develop.
- He acquires a robot and proves that the child is capable of physical and
  mental development but only under robotic tutelage.
- The inventor's son is sad that he has disproved his fathers theories about the
  benefits of robotic childcare now twice.

## Story six: The Truth of Fact, the Truth of Feeling

- The story takes the form of a writer pondering a new modern digital memory
  search technology and recounting the impact of the written word on an oral
  culture.
- The oral culture traditionally had two forms of truth – the facts and a
  person's belief of what is true.
- Both are held up as important.
- When a younger member of the tribe is taught to read and write by Europeans,
  he discovers that he soon places more emphasis on facts as he can recall them
  better by referring to written text.
- His village elder points out to him that this is not the way of the tribe and
  that in a particular circumstance, it has led the younger man to make a
  conclusion which would be factually correct but less good for the tribe.
- The writer discovers that his memories of his fights with his adolescent
  daughter are completely wrong and that, although they have now reconciled, it
  is clear that he was the main cause of the estrangement when they were
  younger.
- When he speaks to his daughter about this, she is disappointed that he
  remembered it wrongly all along.
- When she tries the technology, she discovers how fallible her memory is.
- The author concludes by informing us that although the story he told about the
  tribe is a correct representation of its message, like all writers he has
  changed some of the exact facts to make a better story.
- He recognises the irony in his action.
- He says he has come to realise that the advantage of the memory searching
  software is not in discovering what is right or when you are right but it is
  in discovering when you are wrong and learning to accept that your factual
  memory is imperfect.
- As a final experiment, he says he has opened his life logs up to the public
  domain and encourages any readers who wish to scrutinise them to let him know
  if he has made any mistakes in his re-tellings.

## Story seven: The Great Silence

- An alien species on the brink of extinction writes a memoir describing how
  disappointing it is that humans have not listened and understood their
  language.
- The species is the Puerto Rican parrot, not some extra terrestrial.
- Despite being driven to extinction by humans, the parrot is forgiving and in
  fact envies the imagination of humans and theorises that this vast imagination
  is why they have searched for intelligent life in the universe rather than
  turning their ingenious technology on trying to search for intelligent life on
  earth.

## Story eight: Omphalos

- An archaeologist in a deeply religious world reads her prayers aloud.
- Her job revolves around scientifically proving the existence of a creator.
- In fact, she takes pride in _disproving_ miracles, showing instead the
  scientific evidence for why these 'miracles' must actually be the work of a
  creator.
- Her faith is rocked when she learns about a paper-in-press by an astronomer
  who has discovered a planet which orbits nothing.
- In fact, its sun orbits around it.
- The paper's author theorises this planet, and its theorised inhabitants, must
  truly be the centre of the universe, not them.
- With her faith questioned by a scientific discovery highlighting that humanity
  is not the centre of the universe, she quickly loses interest in her job as an
  archaeologist.
- The artefacts she has been discovering are meant to have been evidence of
  God's plan for humanity but the astrological astronomical discovery suggests
  that humanity might not even have been a part of the plan; just an accident.
- She concludes by trying to reassert her faith, but now her faith in humanity,
  i.e. that it can find a purpose for itself rather than following God's
  purpose.

## Story nine: Anxiety Is the Dizziness of Freedom

- Central to this story is a technology whereby a 'quantum prism' captures an
  alternate reality.
- The alternate reality is finite: the prisms have a memory which is used up
  when interacting with them.
- People can use these alternate realities to, for example, discuss things with
  a version of themself which has made a different decision.
- One line of the story contains scientific and technical information about
  these alternate realities.
- A scientist quickly discovered that weather is vastly different in both
  realities, suggesting both that if the measurement by the quantum machine at
  the time of the divergence was not perfect, the results could be magnified and
  also that tiny differences in one reality could be amplified in the other.
- There is also mentions of the technical limitations, such as different actions
  in the past could not be studied as these realities only diverged at the point
  of their creation.
- In addition, the realities cannot be used for experimentation because, for
  example, a general wishing to know what the more aggressive version of himself
  would do in a scenario and how it would play out would find that the alternate
  version of himself would almost certainly have the same desire.
- Some people find themselves unsatisfied and even negatively affected with the
  results: once an alternate reality had been created, they question all their
  decisions and seek more and more to find out what the ultimate version of
  themself was or would have done.
- There can be jealousy between the versions.
- More than anything, the alternate persons seem to induce severe anxiety in
  some creators.
- Like any invention, con artists arose, seeking to exploit the alternate
  realities.
- For example, a service they might discover prisms in which a person who has
  died in the real world is not dead, try to acquire the prisms and then sell it
  to the loved ones of the dead individual, offering them a chance to speak to
  their lost friend or family member.
- The other central narrative of the story is of one of these con artists who
  has embedded herself in a support group to acquire a valuable prism.
- She notices that the moderator of the support group appears to be hiding
  something.
- Eventually she discovers that the moderator once gave the blame to one of her
  schoolfriends for them taking drugs, resulting in her schoolfriend having a
  custodial sentence and her life spiralling downwards from then.
- The moderator has had guilt since then and has given money to her former
  friend in order for her to try to rebuild her life.
- The con artist successfully executes her con but shortly afterwards her boss
  is killed in front of her by a relative of someone else who was conned.
- Feeling remorse, she follows through with selling the device as planned but
  instead of accepting the money, uses it to investigate alternate versions of
  the moderator.
- She anonymously sends them to the moderator, who learns that in every other
  version, no matter whether she gave the blame for the drugs to her friend,
  took it herself or they shared it, her friend's life still went off the rails.
- She gains some peace of mind from this.
- The overall message of the story I think is that although actions have
  consequences, it is important to know that the same consequences can occur
  from different actions and different actions.


[Home](/blog "ohthreefive")