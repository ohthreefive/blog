+++

date = "2018-10-23T20:38:16Z"
title = "Summaries: Factfullness"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# factfullness

**Hans Rosling**

## Introduction

- The introduction of the book labours the point that most people perceive the
  world incorrectly.
- It highlights that this is not a _lack_ of knowledge or _outdated_ knowledge
  but a systematic misinterpretation of how the world is.
- This is likened to an optical illusion – for example when one knows that two
  lines are of the same length but the brain cannot help but see one as longer.
- There is a rather poignant section where Rosling describes this as his final
  attempt to convey the facts and dispel the misconceptions.
- This is poignant as Rosling died of around the time of the book publication.
- The introduction concludes with a rather forceful peptalk – Rosling has
  decades of experience and knows that the reader will probably feel stupid when
  learning how wrong they are about the world but he says given that everyone is
  like this, it should not put you off.

## 01 the gap instinct

- This chapter is dedicated to dispelling the common misconception of 'gaps.'
- People see groups as separate entities with a gap in between them.
- Examples are developed versus developing countries, the west versus the rest
  and the rich versus the poor.
- The truth is that there are no or at least very few gaps. Things are spread.
- Four levels of income are described which divide the world.
- Those who earn $1-2 per day, $2-8 per day, $8-32 per day and over $32 per day.
- Respectively, 1, 3, 2 and 1 billion people live at each level.
- This simply demonstrates that the majority of the world live at a level of
  income within the perceived gap.
- It is argued that this four level view is the first key to looking at the
  world properly.

### Factfullness

_Factfullness is recognising when a story talks about a gap and remembering that
this paints a picture of two separate groups with a gap in between. The reality
is often not polarised at all. Usually the majority is right there in the
middle, where the gap is supposed to be. To control the gap instinct, look for
the majority._

_Beware comparisons of averages: if you could check the spreads you would
probably find they overlap. There is probably no gap at all._

_Beware comparisons of extremes: in all groups of all countries and peoples,
there are some at the top and some at the bottom. The difference is sometimes
extremely unfair, but even then the majority is usually somewhere in between,
right where the gap is supposed to be._

_The view from up here: remember, looking down from above distorts the view.
Everything else looks equally short, but it's not._

## 02 the negativity instinct

- This chapter deals with reasons behind why people are systematically biased in
  the wrong direction with regards to world progress.
- Despite claiming a number of reasons, I think most of the blame is laid at the
  feet of the media.
- An important lesson is not thinking of better and bad as exclusive
  descriptions.
- The world today has many bad aspects but in almost every measure is better
  than 30 years ago, for example.

### Factfullness

_Factfullness is recognising when we get negative news and remembering that
information about bad events is much more likely to reach us. When things are
getting better, we often don't hear about them. This gives us a systematically
too negative impression of the world around us, which is stressful. To control
the negativity instinct, expect bad news._

_Better and bad: Practice distinguishing between a level, for example 'bad', and
a direction of change, for example 'better'. Convince yourself that things can
be both better and bad._

_Good news is not news: Good news is almost never reported, so news is almost
always bad. When you see bad news, ask whether equally positive news would have
reached you._

_Gradual improvement is not news: When a trend is gradually improving, with
periodic dips, you are more likely to notice the dips than the overall
improvement._

_More news does not necessarily mean more suffering: More bad news is sometimes
due to better surveillance of suffering, not a worsening world._

_And finally, beware of rose-y pasts: People often glorify their early
experiences and nations often glorify their histories._


## 03 the straight line instinct

- Systematic bias (again, even experts are worse than random when guessing)
  means people misconceive the world's population as increasing relentlessly
- This is due to the accelerated increase over the last ~100 years
- What is explained is that extreme poverty is a major contributor to population
  growth and extreme poverty is declining
- Rosling explains that lines between two points are easy to see, but it is more
  likely that it is just a line within a more complex shape (s-shapes curves,
  humps, slides etc)

### Factfullness

_Factfullness is recognising the assumption a line will just continue straight
and remembering that such lines are rare in reality. To control the straight
line instinct, remember that curves come in different shapes._

_Don't assume straight lines: Many trends do not follow straight lines, but are
s-bends, slides, humps or doubling lines. No child ever kept up the rate of
growth it achieved in its first six months and no parents would expect it to._

## 04 the fear instinct

- This chapter deals with the confusion between things which are frightening and
  things which are dangerous.
- Examples include DDT, natural disasters, nuclear contamination and, most
  contemporarily, terrorism.
- One figure produced this means that roughly forty people are killed as a
  result of alcohol for every one person killed as a result of terrorism in the
  United States.
- There is also a mention that the proportion of people who fear a loved one
  will be killed by terrorism is unchanged in the 14 years after the September
  11th attacks.
- The chapter concludes with advice to calculate risk whenever fear is
  encountered and to defer action on that fear until the risk can be assessed.

### Factfullness

_Factfullness is recognising when frightening things get our attention
remembering that these are not necessarily the most risky. Our natural fear of
violence, captivity and contamination makes us systematically overestimate these
risks. To control the fear instinct, calculate the risks._

_The scary world: Fear versus reality. The world seems scarier than it is,
because what you hear about it has been selected, by your own attention filter
or by the media, precisely because it is scary._

_Risk equals danger times exposure: The risks something poses to you depends not
on how scared it makes you feel, but on a combination of two things - how
dangerous is it and how much are you exposed to it._

_Finally, get calm before you carry on: When you are afraid, you see the world
differently. Make as few decisions as possible until the panic has subsided._

## 05 the size instinct

- This statistics heavy chapter focuses on the potential meaninglessness of an
  isolated number.
- Trends and rates (for example per capita) should be preferred and sought where
  possible.
- The striking example uses the 4.2 million children who died in infancy in
  2016. This seemingly astonishingly high number is the lowest the figure has
  ever been.

### Factfullness

_Factfullness is recognising when a lonely number seems impressive, small or
large, and remembering that you could get the opposite impression when compared
with or divided by some other relevant number. To control the size instinct, get
things in proportion._

_Compare: Big numbers always look big. Single numbers on their own are
misleading, and should make you suspicious._

_Always look for comparisons: Ideally, divide by something._

_Eighty/twenty: Have you been given a long list? Look for the few largest items
and deal with those first. They're quite likely more important than all the
others put together._

_And finally, divide: Amounts and rates can tell very different stories. Rates
are more meaningful, especially when comparing between different sized groups.
In particular, look for rates per person when comparing between countries or
reasons._

## 06 the generalisation instinct

- This can be easily summarised by saying that while we may group people
  together by their culture, country, religion etc., it is their wealth or
  rather  their position on the 4 levels of wealth which connects them.
- People of different cultures on the same income level probably live more
  similarly than those of the same culture but on different levels.

### Factfullness

_Factfullness is recognising when a category is being used in an explanation and
remembering that categories can be misleading. We can't stop generalisation and
we shouldn't even try; what we should try to do is to stop generalising
incorrectly. To control the generalisation instinct, question your categories._

_Look for differences within groups, especially when the groups are large. Look
for ways to split them in to smaller, more precise categories and look for
similarities across groups. If you find striking similarities between different
groups, consider whether your categories are relevant, but also look for
differences across groups._

_Do not assume that what applies for one group, for example, you and other
people living on level four, or unconscious soldiers, applies for another, for
example, people not living on level four, or sleeping babies._

_Beware of the majority: The majority just means more than half. Ask whether it
means 51%, 99% or something in beteeen._

_Beware of vivid examples: Vivid images are easier to recall, but they might be
the exception rather than the rule._

_And finally, assume people are not idiots: When something looks strange, be
curious and humble and think, "In what way is this a smart solution?"_

## 07 the destiny instinct

- This chapter details the systematic flaw in thinking that some countries or
  cultures will never change because it is in their nature.
- What is illustrated is that growth or change in these countries or cultures
  can be so slow as to be imperceptible to a single person, but that they add up
  over time.
- In addition, up-to-date knowledge in the social sciences is very difficult to
  achieve as the metrics involved are constantly changing and so need to be
  constantly updated.
- Again, there is a 'them' and 'us' bias which pervades the thoughts of those in
  wealthy countries about those in other countries.

### Factfullness

_Factfullness is recognising that many things, including peoples, religions,
countries and cultures appear to be constant just because the change is
happening slowly and remember that slow even small changes gradually add up to
big changes. To control the destiny instinct, remember slow change is still
change._

_Keep track of gradual improvements: A small change every year can translate to
a huge change over decades._

_Update your knowledge: Some knowledge goes out of date quickly - technology,
science, culture, societies and religions are constantly changing._

_Talk to grandpa: If you want to be reminded of how values have changed, think
about your grandparents values and how they differ from yours._

_Collect examples of cultural change: Challenge the idea that today's culture
must also be yesterday's and will also be tomorrow's._

## 08 the single perspective instinct

- This chapter deals with the narrow/blinkered ways in which people can think.
- Compared to the other chapters, its message is rather simple: be creative,
  question what you know, and do not think your expert knowledge in one area can
  be easily applied to another.
- One of the most controversial ideas in the book so far is raised: that neither
  capitalism nor communism are the solution in isolation and more importantly
  neither democracy nor dictatorship can be thought of as perfect.
- An example given is the communism of Cuba - where the government provides good
  health care but appalling public services - compared to the capitalism of
  America where free markets abound but healthcare is disproportionately
  expensive.

## Factfullness

_Factfullness is recognising that a single perspective can limit your
imagination and remembering that it is better to look at problems from many
angles to get a more accurate understanding and find practical solutions. To
control the single perspective instinct, get a toolbox, not a hammer._

_Test your ideas: Don't only collect examples that show how excellent your
favourite ideas are. Have people who disagree with you test your ideas and find
their weaknesses._

_Limited expertise: Don't claim expertise beyond your field. Be humble about
what you don't know. Be aware too of the limits of expertise of others._

_Hammers and nails: If you are good with a tool, you may want to use it too
often. If you have analysed a problem in depth, you can end up exaggerating the
importance of that problem or of your solution. Remember that no one tool is
good for everything. If your favourite tool is a hammer, look for colleagues
with screwdrivers, wrenches and tape measures. Be open to ideas from other
fields._

_Numbers but not only numbers: The world cannot be understood without numbers,
and it can't be understood with numbers alone. Love numbers for what they tell
you about real lives._

_And finally, beware of simple ideas and simple solutions: History is full of
visionaries who used simple utopian visions to justify terrible actions. Welcome
complexity. Combine ideas. Compromise. Solve problems on a case by case basis._

## 09 the blame instinct

- This chapter deals with the natural human instinct to find someone to blame
  (or indeed someone to praise) for an outcome.
- This instinct is deconstructed in two ways: firstly, there is almost never a
  single person responsible for an outcome and so the instinct is counter-
  intuitive.
- Secondly, once such a person has been identified, any further efforts to
  analyse the outcome (and, for example, discover the real causes) are usually
  abandoned, so as well as being counter-intuitive to seek someone to blame, it
  is also counter-productive in terms of solving the problem.
- This instinct is used to partly absolve the media, which I had thought had
  been blamed in numerous previous chapters, by reminding us that the media is
  not an objective organisation and therefore has its own agendas.
- At one point, Rosling urges us to look in the mirror, highlighting that some
  policies of level four countries are indeed to blame for problems at level
  one.

### Factfullness

_Factfullness is recognising when a scapegoat is being used and remembering that
blaming an individual often steals the focus  and blocks our ability to
solve similar problems in the future. To control the blame instinct, resist
finding a scapegoat._

_Look for causes, not villains: When something goes wrong, don't look for an
individual or a group to blame. Accept that bad things can happen without anyone
intending them to. Instead, spend your energy understanding the multiple
interacting causes or symptoms that created the situation._

_And finally, look for systems, not heroes: When someone claims to have caused
something good, ask whether the outcome might have happened anyway, even if that
individual had done nothing. Give the system some credit._

## 10 the urgency instinct

- The message of this chapter is simple: things are rarely urgent and when
  something is presented in an urgent matter, particularly when spun together
  with fear, decision-making suffers.
- Base decisions and actions on data rather than on the perceived need to act
  now.

### Factfullness

_Factfullness is recognising when a decision feels urgent and remembering that
it rarely is. To control the urgency instinct, take small steps._

_Take a breath: When your urgency instinct is triggered, your other instincts
kick in and your analysis shuts down._

_Ask for more time and more information: It’s rarely, "Now or never,” and it’s
rarely, "Either…or.”_

_Insist on the data: If something is urgent or important, it should be measured.
Beware of data that is relevant but inaccurate or accurate but irrelevant. Only
relevant and accurate data is useful._

_Beware of fortune tellers: Any prediction about the future is uncertain. Be
wary of predictions that fail to acknowledge that. Insist on a full range of
scenarios; never just the best or worst case. Ask how often such predictions
have been right before._

_And finally, be wary of drastic action: Ask what the side effects will be. Ask
how the idea has been tested. Step-by-step practical improvements and evaluation
of their impact are less dramatic but usually more effective._

## 11 factfullness in practice

- The book concludes with a chapter summarising how to counteract the instincts
  which have been demonstrated to be negative and even harmful.

## 12 Outro

- Poignant conclusion to the novel written by Rosling's son and daughter.
- It serves as a mini tribute to their father and the work they have done.

[Home](/blog "ohthreefive")
