+++

date = "2019-03-16T08:36:42Z"
title = "Summaries: Fear"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Fear: Trump in the White House

**Bob Woodward**

## Prologue

- The USA and South Korea had a trade agreement.
- More importantly, the USA could detect missile launches from North Korea
  within seven seconds.
- The trade deal left the US with an $18 billion trade deficit.
- Trump wanted out, regardless of the national security issues.
- Senior staff removed and obfuscated draft letters repeatedly to distract the
  president.
- This sets up a recurring theme.

## Chapter 1

- In 2010, trump first expressed an interest in becoming president.
- Steve Bannon met him, and was impressed with him but did not think he would be
  able to do any of the things required to become president.
- He was entirely removed from the political system, which was seen as a
  disadvantage but was at least unique among the presidential candidates.
- In this first discussion with Bannon, it is noted how quickly he changed his
  opinion about major issues such as abortion.

## Chapter 2

- Just months before the 2016 election, Donald Trump was disastrously behind in
  the polls and Steve Bannon came in effectively to replace Paul Manafort and
  Kellyanne Conway became campaign manager.
- Trump's advantage was seen to be his outsider status – that had allowed him to
  win the Republican nomination and, as long as they concentrated on eight key
  states, that could win him the election.

## Chapter 3

- The final nail in Paul Manafort's coffin was alleged cash payments from
  Ukraine.
- Bannon was scrambling but both he and Kellyanne Conway thought Trump had a
  chance if he could make the campaign about Hillary Clinton.
- Clinton was seen as a relic of traditional politics and without identity or
  personality.
- One of the three key hurdles to be overcome was the presidential debates.

## Chapter 4

- Suspicion of Russian collusion in the 2016 presidential election was first
  brought to Barack Obama's attention in 2015.
- On the day it was made publicly known, the infamous recording of Trump's lewd
  comments about women was also released.
- Trump released an apology of sorts, mentioning that his historical words pale
  in comparison to the actions of Bill Clinton.
- Shortly afterwards, John Podesta's hacked emails were released which included
  speeches given by Hillary Clinton to Wall Street executives.
- Trump booked then cancelled a live interview where he was to apologise on air.
- The next day, only Rudolph Giuliani appeared on television to defend Trump.
- Despite this, Trump thought Giuliani and his defence of him was weak and
  simply carried on as if nothing had happened.
- By this stage, Steve Bannon had halved the polling deficit.

## Chapter 5

- Steve Bannon used Jarrod Kushner to eke money out of Donald Trump himself,
  seeing possible victory at hand.
- When Trump wins the election Bannon immediately realises how unprepared Trump
  is for winning.

## Chapter 6

- Donald Trump appointed John Mathis as his secretary of defence.
- He had been recommended to Trump by Trump's first choice, who declined the
  offer.
- Mathis was a well respected general with several nicknames, including mad dog.
- He sees Iran as a huge threat and thinks the only way to deal with ISIS is,
  "Annihilation."

## Chapter 7

- Trump meets Gary Cohn, a Goldman Sachs executive, after Jared Kushner sets up
  the meeting.
- Cohn, a democratic voter, impresses Trump with his plans to enhance the US
  economy.
- Trump offered him the job of Treasury secretary during the meeting, even though
  the unannounced Treasury secretary Steve Mnuchin is also at the meeting.
- Mnuchin possibly then leaked his upcoming appointment to the press in self-
  preservation.
- Cohn accepted another job on the condition he had significant power over
  American economic policy.

## Chapter 8

- A group of intelligence agency chiefs was to brief Donald Trump about Russian
  interference in the election.
- Included in this were allegations that Donald Trump had paid prostitutes to
  urinate on a bed which Barack and Michelle Obama had stayed in in a state owned
  Moscow hotel.
- The chiefs knew they had to present a united front as there would be
  significant pushback from the president elect.
- Donald Trump responded to media speculation about the document/dossier by
  attacking intelligence agencies and the press.
- In the end, FBI chief James Comey presented the dossier about Trump and the
  prostitutes privately.
- The dossier was subsequently published on buzzfeed.
- The author comments that it is largely based on unverified reports from
  sources and that presenting it to the president was poor form.
- The author also presented this opinion on national television and was
  supported by Donald Trump on Twitter for this assertion.

## Chapter 9

- Trump goes to the repatriation of the first American war casualty since he
  took office.
- This was after a not entirely successful raid in Yemen which Barack Obama had
  long delayed.
- His response to these crises and his behaviour around the families was noted
  to be hesitant and did not display his usual confidence.
- Trump was persuaded out of his initial assertion that he wanted to leave the
  'obsolete' NATO.
- However, despite being convinced to stay with the NATO, he used it as an
  opportunity to attack countries who are not paying 2% of their GDP into NATO
  while the US paid more than that.

## Chapter 10

- Michael Flynn was forced to retire resign due to comment he made about Barack
  Obama's sanctions on Russia in response to their meddling in the 2016 election.
- The New York Times published a story about staffers in the Donald Trump
  election campaign having repeated contact with Russia, implying collusion.
- An FBI insider told a member of the Trump administration that the story was
  largely false.
- However, the FBI where unable to go on the record as they could not be seen to
  be commenting on individual stories lest their lack of comment be seen as a
  confirmation in future.
- The press interpreted this as the White House attempting to get the FBI to
  debunk a story, which was nearly the opposite of the truth.
- Four months later, James Coney went on the record to say that the majority of
  the New York Times article was false.

## Chapter 11

- To replace Michael Flynn, Donald Trump appoints general H R McMaster.
- This is despite his initial dislike of the man for appearing too intellectual
  and liberal.
- Steve Bannon was also against the choice.
- However, the main thing that Donald Trump saw as an advantage was how well the
  media would accept this appointment – and they did.

## Chapter 12

- North Korea and its burgeoning nuclear program was a major concern for Obama
  at the end of his presidency.
- Trump had long said he would negotiate with North Korea.
- The Trump administration were quick to criticise their predecessors after
  taking over.

## Chapter 13

- New strategies for dealing with North Korea are presented.
- Donald Trump is convinced to reach out to former political enemies in order to
  progress.
- As part of his plan to deal with North Korea, Trump wants to trade deal with
  South Korea to be re-negotiated in America's favour.

## Chapter 14

- The first foreign summit of Donald Trump's presidency was held with Saudi
  Arabia and the charismatic Mohammed bin Salman (MBS).
- This was that the advice of Derek Harvey, a former general and National
  Security Council advisor who saw Iran as a great threat and saw the Obama's
  strategy towards Iran as a failure.
- Saudi Arabia was somewhat pressurised into acquiring over $100 billion worth
  of defence contracts from the United States.
- When it appeared negotiations were successful, MBS was invited for lunch at
  the White House, a breach of protocol as at the time he was no more than a mid-
  ranking official.
- One month after the successful summit, MBS was officially appointed the heir
  to the 81-year-old king of Saudi Arabia.

## Chapter 15

- From long before his presidential campaign, Donald Trump heavily criticised
  the Afghanistan war.
- His generals had to tiptoe around him while coming up with a strategy for him.
- It is said that the priorities for Donald Trump were simple answers to the
  question of why they were in Afghanistan and how the conflict would end.
- Despite significant effort, Trump rejected a plan largely driven by H R
  McMaster.
- He had met with footsoldiers who said the war was a disaster and so he wanted
  out.
- John Mathis was able to counter his desire to leave Afghanistan by telling him
  the quickest way to get out of it was to lose.
- This caused Trump to pivot briefly.
- However, he remains against remaining in Afghanistan until after Senator
  Lindsay Graham told him that he could continue the Obama playbook of trying to
  leave Afghanistan and, if that led to another terrorist attack, that would be on
  his hands.
- The other option would be to try strategies his generals had produced and
  perhaps achieve a better conclusion.

### Chapter 16

- Trump wanted out of Obama's deal with Iran, commenting that it was a terrible
  deal for the United States.
- There were numerous squabbles and in-fighting around this but come the next 90
  day deadline to sign the deal, Trump signed, stating that they would never be
  able to get him to saying this again.

## Chapter 17

- Trump and various members of his administration clash over trade issues.
- Trump has long been opposed to the North American free trade agreement and
  wants manufacture brought back to the United States.
- Gary Cohn states this is outdated and that over 80% of Americans GDP comes
  from the service industry.
- There is a back-and-forth between the old-fashioned thinkers who wish to
  preserve America's manufacturing industry and the Wall Street types who think
  that trade deficit is not a bad thing.
- Trump signs is an act to end NAFTA in his first week in office and the
  negotiations about trade policy continue.

## Chapter 18

- Steve Bannon's disquiet in the White House builds.
- He tries to attack Jared Kushner and Ivanka Trump as 'staffers' but is advised
  to back off as he will never convince Trump to remove them from the White
  House.
- Around this time, Bashar Al-Assad carries out a chemical weapon attack.
- Trump is enraged and wants action against Syria.
- A number of plans are drawn up. Bannon is against any action which could be
  seen as war-mongering.
- A strike against an air field is carried out, winning support politically and
  in the media for decisive action.
- Trump became briefly obsessed with Syria and, as a consequence, wanted
  military options for further strikes and interventions in other settings.
- However, his attention soon wanders away from military actions.

## Chapter 19

- In terms of leaving NAFTA, the major players in Donald Trump's administration
  essentially hide or conceal all documents he has to sign in order to delay
  what they think is a hasty and ill-conceived withdrawal decision and plan.
- Apparently, the president's memory is such that he will forget his plans
  unless they are immediately carried out or if he is reminded of them.
- Similarly, when Trump wants (in particular steel) trade with China to be
  negotiated in America's favour, Wilbur Ross and John Mathis try to stall using
  bureaucracy – the secretary of defence must be personally consulted on all
  such trade matters.
- To an extent, this tactic backfires because the DoD's analysis into the
  military's need for steel shows that they would be able to acquire it from
  elsewhere.

## Chapter 20

- Donald Trump fires James Comey, head of the FBI.
- Steve Bannon warned him against doing this, saying the permanent institutions
  such as the FBI will always outlast the president and will go to war with him.
- Comey mentions publicly that Trump had previously asked him to go easy on his
  investigations into Michael Flynn.
- There are concerns that Trump will be impeached.
- Robert Mueller is appointed head of special counsel of an investigation into
  Russian collusion with the Trump campaign during the 2016 election.
- These events send Trump into an uncontrolled rage.

## Chapter 21

- Trump hires an experienced lawyer to help with the Mueller investigation.
- The lawyer advises and enacts cooperation with the investigation.
- This seems to be a correct move as no concrete evidence of collusion appears.
- The chapter pivots to discussing Donald and Melania Trump – saying they spent
  little time together but there did seem to be genuine affection between them.
- Note is made of a comment Trump made to a colleague while discussing him
  confessing infidelity – Trump says it is important never to show any weakness
  or never admit any failure.

### Chapter 22

- A two-pronged chapter.
- The first discusses national defence, referencing the historical invasion of
  Iraq and strategic plans for North Korea.
- The second part discusses immigration, a point on which Trump was felt to be
  relatively liberal by Bannon, who was hard line.
- Steve Bannon worked behind Trump's back to ensure stricter immigration
  policies were enacted.

## Chapter 23

- Trump's daughter and son-in-law were supportive of the Paris climate change
  agreement but Steve Bannon and environmental protection agency chief Scott
  Pruitt were desperate for America to get out of it.
- Bannon and Pruitt conspired to have Trump announce America's withdrawl but
  without going through due process.
- While the initial attempts to block Bannon and Pruitt's plans were successful,
  the seeds had been sewn with the president and Trump soon announced a plan to
  withdraw.
- Bannon fought with John Mattis over China where Bannon wanted increased
  economic sanctions.
- Mattis was fixated on Afghanistan and finishing the job of defeating Isis. He
  was happy with the China situation.
- Bannon suggested that if Mattis supported him in China, he would support
  Mattis on Afghanistan.

## Chapter 24

- Another chapter of two halves.
- The first half continues the story of the Mueller investigation with several
  newspapers publishing reports both about internal goings-on during the Trump
  campaign and about new avenues into which Muellar might be moving.
- Trump was furious about both.
- His lawyer reached out to the investigation team, seeking clarification and
  getting it that the stories in the paper about new targets of the investigation
  were false.
- The second half of the story is about decision-making around gender
  reassignment for troops within the US military.
- Trump tweets his plans without going through the formal processes.

## Chapter 25

- Trump's staff go to enormous lengths to reduce or at least monitor his use of
  Twitter.
- He agrees to their suggestions of his tweets being vetted, edited or pre-
  approved.
- Subsequently, he ignores this and continues to tweet by him self.
- Trump meets Malcolm Turnbull, Prime Minister of Australia, and during the
  discussion says he will exempt Australia from steel tariffs.
- Eight months later, when due to meet him again, he tells Gary Cohn he will
  simply deny the conversation they previously had.
- Subsequently, during the meeting, Trump again says he will exams Australia
  from steel tariffs; Australia remains exempt.

## Chapter 26

- McMaster confronts Rex Tillerson and John Mattis who have been circumventing
  the president to try to enact policy on their own.
- This is because they believe the president is poor at decision-making.
- Donald Trump launches into Jeff Sessions both on television and Twitter.
- Steve Bannon calls Sessions in and tells him this is a deliberate diversion
  due to Jared Kushner testifying at the Mueller investigations.
- Sessions is appalled at the behaviour but does not wish to resign.
- Although using the attacks as a diversion, Trump is still furious at Sessions
  for recusing himself from the investigation after being appointed attorney
  general.

## Chapter 27

- Senior advisors invite Donald Trump to a meeting in the tank, the Joint Chiefs
  of Staff's conference room in the Pentagon, which is secluded.
- They plan to make clear the benefits of trade as well as make the directions
  and aims of the administration clear so they can set a public agenda and ideally
  stick to it.
- The plan backfires with Trump ignoring the lectures and Steve Bannon seizing
  the opportunity to turn the meeting to Trump's hatred of the Iran trade deal and
  of the money and troops America was giving to South Korea.
- Rex Tillerson ended the meeting thinking that the president and the presidency
  were in worse shape now than in the first month – we are six months into the
  presidency at this point.

## Chapter 28

- Rex Tillerson comes to speak to Reince Priebus and vent his frustration and
  mention his possible resignation.
- His main dissatisfaction is how Trump treats his generals.
- Trump's anti-trade agenda continues and his chiefs of staff continue to find
  ways to distract him with his inability to follow through on a plan being the
  only thing that saves the trade deals at this stage.
- In July 2017, Trump appoints Anthony Scaramucci she as head of communications.
  Scaramucci launches a public attack on Priebus.
- Priebus decides to resign and goes to speak to the president who tells him he
  is considering John Kelly as a replacement.
- One discussion, they agree the best course of action is to let things cool
  down for a few days before a decision.
- Within minutes of leaving, Priebus sees a tweet from the president announcing
  John Kelly as the new chief of staff.
- Later, Kelly tells Priebus that the tweet was the first he had been given the
  offer of the job, which he could then not refuse.
- Later, Priebus commented that Trump had staffed his White House with predators
  who were not interested in discussion but only interested in winning and it
  was this shared mentality that resulted in the gross dysfunction.

## Chapter 29

- After the riots in Charlottesville, Trump uses the phrase, "On all sides," in
  a speech which earns him condemnation for potentially sympathising with the
  white supremacists.
- His chiefs of staff manage to convince him to give a second speech in which he
  uses much more sensitive language and specifically targets white supremacists,
  Nazis and the KKK for their unacceptable racist beliefs.
- The chiefs of staff are delighted by both the speech and its reception.
- However, Trump is furious as the press see this as an apology of sorts and he
  never wants to apologise for anything.

## Chapter 30

- Trump makes a third speech about Charlottesville in which he again says there
  were wrongdoers on both sides and there were good guys on both sides.
- There is again a media frenzy and his staff are appalled.
- CEOs of major companies who were part of trumps manufacturing advisory group
  resign, forcing Trump to announce the disbandment of the group before it ended
  up folding through lack of members anyway.
- Gary Cohn signalled his resignation, to which Trump responds furiously,
  launching personal attacks on Cohn face-to-face as well as engaging and near
  blackmail as Cohn as in the middle of major negotiations about tax reforms.
- John Kelly and Rob Porter both praise him and the restraint he has shown and
  say he has their full support with whatever public statement he wishes to
  issue about the Charlottesville matter.
- For Porter, it is the last straw and he realises the president will never be
  presidential but will only ever act like himself.

## Chapter 31

- Bannon expresses that he will soon resign.
- He is worried however about the upcoming National Security Council meeting
  about strategy in Iraq Afghanistan.
- Despite the agendas of many different people, the sentiment of John Mathis
  wins the day, that being that the swift withdrawl from Iraq left the vacuum
  into which ISIS expanded and potentially led to the 9/11 attacks.
- Trump decides to go with his generals' plans.
- Trump publicly presents the new strategy as something that will lead to
  victory however others publicly express in an oblique way is that a victory
  will not be possible.
- During Bannon's resignation, he infuriates Trump by publicly saying they have
  no military strategy to deal with North Korea.
- Trump tries to turn the narrative by saying he is firing Steve Bannon.

## Chapter 32

- Kelly believes he can bring some order to the presidency.
- However, Porter assures him that efforts have been made before and have
  failed.
- The same themes recur: Trump wants to pull out of the South Korea and other
  trade deals and wants to impose steel tariffs.
- Enormous efforts are made to both dissuade him and subsequently to distract
  him when he wants to go ahead regardless of his advice.
- One day Trump begins complaining about how he has no good lawyers.
- Porter recognises this behaviour and cautions Kelly that this could be a
  difficult period to get through with the president being so distracted.

## Chapter 33

- In order to combat Trump's endless quest against current trade deals, Peter
  Navarro is essentially demoted.
- He will have to report directly to Gary Cohn, rather than have the president's
  ear.
- Navarro is powerless to stop this.
- It is nearly 2 months until Donald Trump comments that he has not seen Navarro
  for a while but he does not follow up on this.

## Chapter 34

- Trump's ongoing confused message over North Korea is discussed.
- This segues into a discussion about John Kelly, whose attempted negotiations
  with North Korea were undermined by the president in a tweet.
- Kelly and the president do not get on, possibly due to Kelly's initial thought
  that he could control the present.
- However, Trump rejects any form of control.
- Trump reaches out to Reince Priebus for some advice.
- It is noticed that Trump often asks people for their opinions about others.
- He uses discussions like these as leverage in future.

## Chapter 35

- Tax reform is passed, the only major legislation passed by Donald Trump in his
  first year as president.
- Corporate tax rate is significantly lowered, taking it more in line with other
  countries.
- Personal tax is slightly reduced.
- In order to get the bill through the house, Gary Cohn as well as other senior
  White House officials have to work hard around the president's unrealistic
  aims for more extreme cuts.

## Chapter 36

- Trump and Steve Bannon have a war of words with each other.
- Steve Bannon is thought to be the major source for Michael Wolff's book Fire
  and Fury.
- He believes that Trump is not the person to radicalise politics away from the
  so-called 'old guard'.
- Bannon still believes that the Trump administration has no plans for dealing
  with North Korea and voices this publicly.
- Trump tweets directly to Kim Jong Un that he has a more powerful and reliable
  nuclear arsenal than him.
- This sends US and worldwide diplomats into a frenzy – as well as the media of
  course.
- Trump continues an aggressive stance internally towards North Korea but is
  generally persuaded and distracted away from the hostility.

## Chapter 37

- In personal conversations with the President of South Korea, Trump criticises
  the KORUS trade deal and says he is ready to withdraw.
- Both the South Korean president and his chiefs of staff again try to convince
  him that trade and security are tightly linked and South Korea is a hugely
  valuable ally.
- For example, with South Korea, a nuclear launch can be identified within seven
  seconds; it would take 15 minutes for detection from equipment in Alaska.
- The rhetoric over nuclear armageddon continues but at length, the National
  Security Council and CIA manage to convince Donald Trump that North Korea is
  not credible nuclear threat yet and so does not deserve the attention he is
  giving it.

## Chapter 38

- Another chapter about Afghanistan.
- Trump hears how the Afghans have offered their mineral wealth and insists that
  America should go and get it.
- This is despite the fact that the operations required would take years if not
  decades to yield any meaningful return.
- Overall, the picture is that Afghanistan is unwinnable however due to the
  president's rhetoric, the ground commander is echoing his sentiments that
  America can win.
- Privately, withdrawl is seen as the only option.
- The president changes tack publicly by announcing George W Bush as a warmonger
  for starting the Iraq and Afghanistan wars.
- He also rounds on H R McMaster, blaming him for convincing him to send more
  troops to Afghanistan.
- Trump is then clear that he wants out of Afghanistan.

## Chapter 39

- Trump's immigration plans are handled largely by Stephen Miller and a senior
  Democrat.
- They appeared to be making excellent progress but were stopped in their tracks
  way in more a hard line Republicans encouraging the president to be more strict.
- During discussions, the president uses the phrase, "shithole countries," to
  refer to some countries, and this is leaked to the press.

## Chapter 40

- The Mueller investigation turns its attention towards wanting testimony from
  Trump.
- Dowd think this is a bad idea and tries to convince the president by
  interviewing him himself.
- When the topic changes to James Comey, Trump loses his temper.
- Dowd is able to convince Trump towards a written response to Mueller saying that
  he did not wish to testify but would produce scripted answers to specific pre-
  submitted questions if desired.
- Trump is extremely happy with the management strategy.

## Chapter 41

- At the closing stages of internal negotiations about Trump imposing steel
  tariffs, Porter, Cohn and others continue to urge the president to wait until
  announcing a formal investigation and the results of such into possible Chinese
  stealing of intellectual property from the United States.
- They feel that if they are able to provide evidence of Chinese wrongdoing, they
  are steel tariffs will gain support worldwide.
- At this time, two former wives of Rob Porter publicly accused him of abuse.
- He resigns; Gary Cohn notes that this means the removal of one of the great
  restrainers of Trump.
- Without Porter, Trump is convinced to announce steel tariffs without any
  publication of the investigation into Chinese espionage.
- Gary Cohn resigns as a reaction to the whole trade fiasco.
- Trump still wants to publicly talk up a positive relationship with China.

## Chapter 42

- The book concludes with a chapter about the Mueller investigation.
- Mueller and Dowd have a meeting in which Dowd comes to believe that Muellwr has
  no real case against Trump and wishes to catch him out on a perjury charge when
  Trump is testifying.
- Dowd realises that this is a real threat as Trump is a habitual liar.
- He strongly counsels the president, mentioning that every possible document has
  been released and every requested witness has given a statement and answered
  every question the Mueller investigation has.
- Trump is still determined to testify, possibly because it is what his base
  voters would expect of him.
- Dowd resigns, saying that if the president cannot take his advice, he has no
  role.
- Trump appears genuinely grateful for all the work Dowd has done for him.

[Home](/blog "ohthreefive")

