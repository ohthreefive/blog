+++
         
date = "2019-07-28T19:25:31.279Z"
title = "Summaries: Fire and Blood"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Fire and Blood

**George R R Martin**

## Aegon's Conquest

- The Targaryen's were not the most powerful house in Valeria.
- They left Valeria over a decade before its ruin after a vision by one of their
  number.
- Aegon left with his two sister-wives and his great black dragon, Balerion the
  Black Dread.
- The Targaryen's had set foot on Westeros before this and Aegon was friendly
  with the Baratheon house.
- Aegon's conquest began when another house – Lord Haraan of house Hall –
  insulted his Baratheon friend, Oris.
- He conquered many houses in battle with his sisters and their dragons,
  including victory over the Lannisters and Gardeners at the Field of Fire, at
  which all three dragons fought.
- Torrin Stark bent the knee after hearing about the Field of Fire and wishing
  to avoid the same fate for his people, earning him the name of the King who
  Kneeled.
- Dorne refused to submit.
- Aegon was officially named King of the Seven Kingdoms in Oldtown but chose to
  take the place where he first arrived as his capital – calling it King's
  Landing.

## Reign of the Dragon - The Wars of King Aegon I

- Aegon fought many wars, subduing the remaining Iron Islanders and letting them
  chose their next leader after the end of the Hall line; they chose a Greyjoy.
- The Tyrell's, stewards to the Gardeners, gained Highgarden.
- Aegon allowed those who submitted to him to retain their name and lands.
- Dorne still refused to submit and under the guidance of their aged princess
  Meria, the Yellow Toad, they fought a long, vicious and cruel war with the
  Targeryen's, including at various times deserting strongholds only to fall
  upon the bewildered assailants.
- The troubles peaked when Rhaenys was killed after a bolt caught Meraxes in the
  eye and killed her.
- Aegon razed Dorne but never conquered it.
- When Meria died her son Nymor sued for peace without submitting.
- Many counselled Aegon to refuse without submission, but on reading a letter
  sent by Nymor, Aegon agreed to peace.
- No one knows what the letter contained.

## Three Heads had the Dragon – Governance under King Aegon I

- Aegon was said to be a just king.
- He would regularly travel the realm, believing that to prevent any rebellion
  was better than to have to quell one.
- Realising his hastily assembled capital at King's Landing was vulnerable when
  he and his sister-wives were away, he arranged the building of seven walls
  around the city.
- He established the role of Grand Maester, being sent by the Archmaesters of
  the citadel.
- His elder sister, Visenya, who was said to be harsher than his younger sister
  and himself, outlived him.
- She established the order of the King’s Guard: seven chosen knights who would
  give their lives to defend their king.
- Aegon was said to rank first of the 17 Targaryen kings in both war and peace
  times.

## The Sons of the Dragon

- King Aegon I had one son by each of his sister wives.
- The first son, Aenys, was born of his younger wife Rhaenys three years before
  she and her dragon died in battle.
- At first small and sickly, he began to thrive after he was given his own
  dragon, Quicksilver, when he was three years old.
- He was intelligent, artistic and diligent, capable with a blade but not
  inspiring.
- He gave Aegon five grandchildren. Three sons and two daughters.
- The eldest, a daughter called Rhaena, had great influence and gave dragons to
  many of her cousins.
- Aegon's other sister, Visenya, had just one son – Maegor.
- He was a powerful child, quick to anger and masterful at arms.
- He was knighted at only 16 but by this time, married for three years, had not
  produced any children.
- He took no dragon, saying only one was worthy of him.
- Aegon died after 10 years of peace with Dorne.
- The crown passed to his elder son Aenys, who gifted his brother Maegor with
  his father's sword.
- Maegor now possessed two valerian steel swords.
- Aenys was widely seen as _not_ a warrior and this supposed weakness resulted
  in immediate rebellions, including in Dorne, the Vale of Arryn and by a man
  claiming to be the grandson of Harren in the Black.
- All of these rebellions were defeated, most dramatically by Maegor descending
  upon the supposedly impregnable Eyrie, riding upon his father's Dragon,
  Balerion.
- Maegor's mother Visenya was among those who thought he would be a better king.
- In the aftermath of the rebellions, Aenys named Maegor as his Hand.
- The rule of these two brothers was only quiet for two years.
- At that time, Aenys had another child and Maegor, possibly frustrated by his
  wife's inability thusfar to bear him a child, announced he was marrying
  another, much to the condemnation of the Faith.
- However, Aenys allowed the marriage.
- Shortly thereafter, Aenys announced he was wedding his first child, Rhaena, to
  his first son, Aegon.
- Again, the High Septon condemned this but for once Aenys was stubborn and
  decisive and carried on with his plan.
- On their wedding day, he announced that Aegon was now Prince of Dragonstone,
  causing Maegor's mother Visenya to storm out and fly back to that island.
- Aenys had underestimated the strength of the High Septon and near open
  rebellion occurred throughout Westeros.
- His aunt urged him to torch the Starry Sept in Oldtown but Aenys did not want
  to.
- The psychological stress had physical consequences and Aenys appeared
  prematurely to wither and age prematurely.
- He died a young man.
- At this, Visenya immediately departed on her Dragon and returned with her son
  Maegor riding Balerion.
- Maegor killed any who would oppose his claim to the throne, including the
  Grand Maester.
- His second wife reappeared with a woman from Pentos, thought to be a witch,
  healer or poisoner.
- She helped heal Maegor after the wounds he had taken claiming the throne.
- Later, he married her.
- Aegon was in Lannisport and his wife gave birth to twin daughters.
- He was denounced by the High Septon, as were his children.
- He was also thought to possess the same apparent weaknesses as his father.
- To put down the threat from the Faith once and for all, in a night of
  violence, first Visenya then Maegor took to their dragons, destroyed their
  main enemies in Westeros and headed for Oldtown.
- On arrival, the found the gates open and the Targaryen flag flying alongside
  other Westerosi flags.
- The High Septon had died unexpectedly overnight.
- The cause of death, which was widely suspected to be murder, was never known
  truly; the Septon was in excellent health the previous day.
- While in Oldtown, Maegor reconciled with his first wife, who in turn accepted
  his other wives.
- It was a happy time from Maegor, but soon another threat from back in King's
  Landing presented itself.
- Aegon and his sister wife Rhaena snuck in to King's Landing, not to take the
  throne but to take dragons – Aenys' Dragon Quicksilver and Dreamfire.
- Aegon was able to muster a large force of rebels, but notably with no great
  Lords.
- The great Lords were reluctant to join the rebellion until they saw some
  victory from Aegon.
- That victory would not come.
- In his first battle, Aegon was set upon by Maegor upon Bavarian, four times
  as large as Quicksilver.
- Both dragon and rider were killed and the rebellion effectively destroyed.
- At that time, Visenya and Aenys' wife (Alyssa Velaryon) and her remaining
  children were on Dragonstone.
- Viserys, Aenys' remaining son, was summoned to be squire to the king; he was
  much loved by the people.
- Maegor's first first wife became pregnant but miscarried.
- His third wife, the Pentoshi witch and Mistress of Whisperers, told Maegor
  that she had become pregnant by another man.
- The King’s Hand, Alyssa Velaryon's father, had arranged men to visit her to
  get her pregnant.
- Maegor swiftly questioned and then executed these men and other men that were
  implicated in the interrogations before descending upon the house of his Hand
  and destroying his lineage entirely.
- Around this time, Harrenhal became known as a cursed place.
- The Queen was also killed for her betrayal.
- A melée was held between 23 lords to decide the fate of Harrenhal but the
  winner died within a fortnight due to his wounds, strengthening the notion of
  a curse.
- At this time, Maegor's mother died on Dragonstone and in the aftermath of her
  death, Aenys' wife Alyssa and daughters fled the island successfully.
- Maegor killed Viserys for this betrayal.
- Maegor's first wife Ceryse then died, possibly another murder but never truly
  known.
- The Red Keep was finished and afterwards, Maegor had its builders killed to
  protect the castle's secrets.
- When his third wife, hated and feared throughout the kingdom, failed to give
  Maegor any children, he decided to take another – three others in fact.
- One of these would be the sister wife of his defeated nephew, Aegon.
- Rhaena already had twin daughters by Aegon and she disguised and sent them
  away from her, never learning where they went.
- She knew, however, that she could not escape.
- The summons from Maegor came, and Rhaena obeyed.
- She and the other two new wives were widows and so the three became known as
  the black brides.
- The Faith were still essentially at war with Maegor, and denounced the
  marriage.
- Tyanna produced Rhaena's two daughters at the wedding ceremony.
- Maegor declared the elder daughter to be his lawful heir and dis-inherited his
  nephew.
- The two other ways wives became pregnant within the year but when the first
  miscarried, Maegor realise the truth.
- He prepared to torture has Tyanna, but she confessed willingly to having
  poisoned the mothers of all of his children.
- Maegor killed her.
- As per Tyanna's prediction, Maegor's other wife also miscarriage.
- At this time, Maegor's rule was falling apart and from Storm's End, Rogar
  Baratheon revealed that he was hiding Alysanne Targaryen and Aenys' remaining
  son, Jaehaerys.
- After their escape from Dragonstone, Jaehaerys possessed the Valerian steel
  sword Dark Sister.
- On hearing about Jaehaerys and his claim to the school and, Rhaena fled King's
  Landing on her Dragon, stealing the other Valerian steel blade Dark Fire.
- Maegor was found dead and slashed to pieces on the Iron Throne.
- No-one knows whether it was suicide or murder, or whether the Iron Throne
  itself killed him.
- The young Jaehaerys, with his mother and Rogar Baratheon's support, was the
  new king.

## Prince into King – The Ascension of Jaehaerys I

- Jaehaerys was a good king and ruled for 55 years.
- He was known as, "The Conciliator," and it was said that while his
  predecessors conquered the seven kingdoms, he united them.
- His first task, while still being watched over by a Protector of the Realm and
  Queen Regent, was to deal with those who had been faithful to Maegor.
- He did so with justice and mercy.
- Jaehaerys' only remaining threat was a Septon who sought to displace the high
  Septon.
- It was removed when the Septon was murdered.
- He was crowned.
- His twin nieces, daughters of his elder brother and sister's marriage secretly
  changed places, the more pious going to train as a Septa, the more outgoing
  returning to King's Landing.
- She would be heir until Jaehaerys had a male son.

## The Year of the Three Brides - 49 AC

- 1st to marry was Rhaena, once wed to Prince Aegon and to King Maegor.
- She married the son of the Lord of Fair Isle, who had sheltered her during her
  darkest time.
- She did so without consulting the crown and without the presence of any of her
  family.
- This was said to anger her mother, Alyssa and relations between these two
  Queens was said to be poor.
- Next to marry was Queen Alyssa, the Queen Regent, who married the King’s Hand,
  Rogar Baratheon.
- This wedding was as grand as any the realm had ever seen, "The Golden
  Wedding."
- In the celebrations, the remaining members of Jaehaerys' King's Guard were
  selected from the winners of the tournament battles.
- In this regard, it is thought that this King’s Guard where the most formidable
  of any ever in Westeros.
- Next to marry was the king: Rogar and Alyssa sought to arrange the match.
- They agreed that the problems caused by the Targaryen at the problems caused
  by two Targaryens marrying each other were not worth it and that separate
  matches should be found for Jaehaerys and Alysanne.
- Their first plan was to marry Alysanne to Rogar's brother.
- On hearing this, Alysanne told her brother and she and Jaehaerys took to
  Dragonstone upon their dragons and married each other.
- Due to Alysanne's youth, Jaehaerys did not consummate the marriage.
- When Rogar Baratheon arrived and tried to discredit the marriage as it had not
  been consummated, Jaehaerys' King’s Guard would not back down and let him
  separate the two.
- At the intervention of Queen Alyssa, all agreed to no further conflict.
- The marriage would be kept a secret for now and, also in secret, Rogar still
  sought to discredit it.

## A Surfeit of Rulers

- In the year after the year of the Three Brides, there was unresolved conflict.
- Jaehaerys had never been asked for his blessing for his mother and his Hand to
  marry.
- He openly said he did not need another father, just an advisor.
- Jaehaerys' marriage to Alysanne was against the plans and wishes of these
  elders.
- Queen Alyssa sought to influence Alysanne by filling her court with her own
  choice of handmaidens and so on.
- Within this group, Rogar placed a woman who was meant to tempt Jaehaerys
  before he had consummated his marriage to Alysanne, thus ending the union.
- On Fair Isle, Queen Rhaena had married the second son of the Lord.
- Many thought her true love was really this man's sister.
- When the Lord died, his heir, elder brother to Rhaena's husband, told her to
  leave.
- She was taken in by the Lannisters, however she believed they were spying on
  her and wanted to use her to further their house.
- She left but found the same problems wherever she went.
- When Alyssa's and Rogars plans to influence and tempt Jaehaerys and Alysanne
  failed and the two became openly close at court, at last the truth of their
  marriage became known.
- Rhaena returned to King's Landing for the formal wedding of Jaehaerys and
  Alysanne, asked for Dragonstone and returned there.
- Shortly afterwards, Rogar spoke against Jaehaerys openly at court and on
  hearing his words, Alyssa dismissed him: she was the Queen Regent and so had
  the true power.
- She declared his words treason.
- He spoke against her and at that, the knights present threatened to kill him.
- Alyssa stripped him of his title, and naturally their marriage fell apart.
- In one last folly, Rogar attempted to kidnap the Targaryen twin training to be
  a Septa in Oldtown and expose her as truly the other Targaryen and therefore in
  his opinion the true heir to the Iron Throne.
- He sent his brother to do this but due to suspicions in Oldtown, the plan
  failed and his brother was imprisoned.
- On hearing of this further failure, Rogar realised he was finished, wrote a
  confession absolving his brothers and and resigned himself to a place in the
  night's watch.
- Shortly afterwards, Jaehaerys turned 16 and was able to rule in his own right.

## A Time of Testing – The Realm Remade

- After arriving in King's Landing, Jaehaerys set about re-arranging his
  closest counsellors.
- He had some major issues to overcome.
- Firstly, the realm was in terrible debt.
- His new master of coin is a low born tradesmen from Pentos.
- Secondly, he will need to reveal his wedding to his sister to the realm.
- He was confident of success as he was different from his brother Aegon.
- Both he and his wife were Dragonriders and so instill fear in those who would
  speak against them, for one thing.
- He appointed seven speakers to go and spread the word that his union with his
  sister is not the abomination it is said to be.
- For whatever reason, he was successful and Alice was accepted.
- Thirdly, there was the issue of Rogar Baratheon.
- He summoned him to King's Landing and the former Hand accepted, believing that
  Jaehaerys was honourable and would not have him executed without giving him
  the opportunity to take the Black.
- He proved justified in this faith as Jaehaerys in fact says that Rogar only
  spoke of treason but committed none and that as long as he becomes Alyssanne's
  foremost champion, he cannot only keep his life and titles but also remain in
  court advising Jaehaerys.
- His brother who tried to kidnap Jaehaerys’ niece, as well as any accomplices,
  were banished for 10 years.
- Lastly, Jaehaerys’ older sister Rhaena recounted her tail of woe that she had
  no place in the seven kingdoms where she is neither feared nor seen as a weapon
  to advance the house into which she has been accepted as guest.
- She pleads to be given Dragonstone and Jaehaerys consents on the condition
  that it is as a gift by his leave and not her own separate house.
- Rhaena mocks him saying he must be insecure about his control over the realm
  to ask this of her but says she will agree so long as she allows her daughter
  Aerea to return home with her.
- Some in the kingdom thought Aerea and her twin Rhaella, currently training to
  be a Septa in Oldtown, had traded places in the past, each settling where they
  were more comfortable.
- Jaehaerys agrees, and so his current heir is now under the former Queen's
  guard.
- Queen Alysanne is credited with enlivening Court and capturing the hearts of
  people.
- Her greatest gift to Jaehaerys, however, comes months after their public
  wedding when she announces she is pregnant.

## Birth, Death and Betrayal under King Jaehaerys I

- Jaehaerys travelled as much as Aegon but in smaller parties.
- His pregnant wife was nearly attacked while visiting some healing waters.
- His older sister Rhaena became pregnant and had a son by Rogar Baratheon.
- Queen Alice had her child prematurely, a boy called Aegon, who died within a
  few days.
- She was bereft and blamed the attack.
- Rhaena and her daughter Aerea were essentially strangers to each other and
  Aerea thought Dragonstone was boring compared to King's Landing but at least
  it had plentiful dragons, one of which she was asked to choose.
- Back at King's Landing, the King’s Master of Coin was scrambling to continue
  to find and raise money for the king's various building projects, most of
  which were aimed to improve life in King's Landing.
- It is said the Master of Coin was most despised for his outrageous wealth.
- Rhaena's sister in law, Elissa Farman, asked for money to build a great ship
  but Rhaena could not bear to be parted with her.
- Elissa was displeased.
- Later she fled the island and took with her three dragon eggs.
- The following year, 54 AC, would be known as The Year of The Stranger due to
  its multiple tragedies, the seeds of which had already been sewn.
- First to die was Septon Oswyck, who had bravely stood against the wishes of
  some of his faith and had married Jaehaerys and Alysanne.
- Jaehaerys’ mother Alyssa then became pregnant by Rogar for the second time, no
  aged 46, and some feared for her health.
- Next to die was the High Septon in Oldtown.
- Jaehaerys recognised that the faith was essential for his rule and so decreed
  that he and his queen would immediately fly there on their dragons to pay their
  respects.
- Once there, Jaehaerys said he would remain until the new High Septon was
  chosen.
- While many rejoiced at the presence of their King and the Dragons, the faith
  were understandably worried that they were now choosing their new leader under
  the gaze of two dragons.
- The chosen Septon was one of Jaehaerys original seven speakers.
- This was a highly unusual choice and it is theorised that a deal was done that
  this Septon, who accepts the king's incestuous marriage, was the best choice
  for now and that his successor would be a Hightower.
- Bad news followed with Jaehaerys’ mother Alyssa said to be dying – she was
  late in her pregnancy but not near delivery yet.
- Jaehaerys and Alysanne flew their dragons as quickly as possible to Storm’s
  End and found Rogar Baratheon paralysed by grief.
- The only solution with any happy outcome was to deliver the child in the hope
  of saving it – Alyssa was doomed.
- Jaehaerys had to convince Rogar to make this choice but he did so and a
  daughter was born to him.
- Shortly afterwards, Jaehaerys’ older sister Rhaena arrived to make amends with
  her mother.
- On seeing what had happened, she blamed Rogar for getting her mother pregnant
  again and said should he remarry, she would burn his castle down.
- Rogar openly mocked her threat saying he did not even fear Maegor.
- However, he did not remarry.
- When she returned to Dragonstone, Rhaena was dismayed to encounter a disease
  spreading throughout her people.
- When the symptoms and pattern of the deaths were made known to the small
  council, poison was suspected.
- Rhaena's largely spurned husband confessed and committed suicide.
- Rhaena had no easier a time from her daughter Aerea, who wished to return to
  King's Landing but was denied.
- The King’s Hand, whose niece had died in the poisonings on Dragonstone,
  resigned and Jaehaerys had a difficult decision to replace him.
- Once this was done, celebrations were planned but these were cut short as
  Rhaena arrived, announcing Aerea had escaped Dragonstone on top of Balerion the
  Black Dread.

## Jaehaerys and Alysanne – Their Triumphs and Tragedies

- It was widely agreed that overall, the reign of Jaehaerys was a success, not
  least the vast periods of peace.
- One major issue was the disappearance of first Aerea on Balerion, and then her
  mother Rhaena on Dreamfyre, who went to look for her.
- Numerous resources were spent in an effort to find them.
- Another escapee of Rhaena, Elissa Farman, received the ship she had bought in
  return for the stolen Dragon eggs.
- She set sail from Braavos with a warning from the Sea Lord: she was being
  hunted.
- Alysanne became pregnant again and bore Jaehaerys a son and heir – Aemon
  Targaryen.
- The kingdom celebrated.
- A sighting of Rhaena was reported on an island where an old acquaintance of
  hers was living.
- The fugitive Elissa Farman, going by the name Alice Westerling, assembled a
  crew to explore uncharted seas.
- Jaehaerys considered chasing after her on his Dragon but decided against it.
- Another fugitive of Dragonstone, Aerea, returned to King's Landing in a
  dreadful state.
- She died shortly afterwards.
- Her cause of death was never made public but she was found to be infested by
  unknown creatures which essentially burned her to death from the inside.
- The secret records of Septon Barth theorised that she could not control
  Balerion, who took her to his home: doomed Valeria.
- This is where she became infected.
- Balerion was also severely wounded when he returned.
- The Septon did not know what could have caused these wounds.
- Aerea's mother Rhaena could not make it to her daughter in time before she
  died.
- She had now missed her mother and her daughter's deaths.
- She withdrew entirely from public life, never again setting foot on
  Dragonstone or King's Landing.
- She died around 15 years later.
- Septon Barth was soon elevated to Hand of the King and sent to Braavos to try
  to recover the Dragon eggs from the Sea Lord, or get information about who had
  them.
- He returned with a bargain: of course the Sea Lord had no knowledge of the
  eggs, but if the Iron Throne would leave them alone, the Iron Bank would
  forgive some debts.
- Jaehaerys planned a progress to the north.
- He was delayed in order to oversee peace negotiations between Tyrosh and
  Pentos.
- These ended up delaying him by six months and even when the peace treaties
  were signed, they did not go down well in a Essos.
- Queen Alysanne went to the north on her own.
- She visited many castles and enchanted the north.
- She gained affection for the people of the Night's Watch and negotiated their
  lands – the gift – to be doubled in size.
- While speaking to woman from Molestown, she gained a better understanding of
  the impact of the ancient right of first night.
- With skilful and forceful negotiation, she managed to get this ancient law
  removed.
- After 10 years in power, King Jaehaerys and Queen Alysanne were ruling over a
  relatively peaceful and happy Westeros.

## The Long Reign: Jaehaerys and Alysanne - Policy, Progeny and Pain

- Winter came to Westeros and with it a shivering illness.
- The common people, starving, attacked the Master of Coin in the streets and
  killed him.
- A greater tragedy was that the first child of Jaehaerys and Alysanne,
  Daenerys, caught the shivering illness and died.
- As well as the loss of their daughter, this death also called into question
  the 'exceptional' nature of the Targaryens: they had never previously been known
  to fall victim to the illnesses of Westerosi people.
- The cause of the shivering illness or any treatment for it were never known.
- Shortly thereafter, Alysanne fell pregnant again and had another daughter, who
  she named Alyssa after her mother.
- A third war in Dorne occurred, though compared to previous conflicts it was no
  more than a skirmish.
- However, Jaehaerys’ prominent involvement both increased his adulation
  throughout the realm and gave him some pleasure after his impotence against the
  shivering sickness.
- He and his wife continued to have children.
- His eldest remaining boy Aemon he married to Jocelyn Baratheon, they soon had
  a daughter, Rhaenys.
- This boy was knighted at 17 and took his first dragon to wing,  Caraxes, the
  fiercest of the young dragons.
- His younger brother, Baelon, not willing to be outdone, was knighted at 16 and
  took Visenya's Dragon Vhagar, who had not been ridden in 29 years.
- He was married to his sister, Alyssa, and they soon had two sons, Viserys then
  Daemon.
- Queen Alysanne continued to have children, though of her last three, two boys
  died within a year and a third barely made it through infancy.
- One more of their sons, Vaegon, was sent to the citadel and one of their
  daughter's, Daella, was married to the Lord of the Vale.
- She was a shy and meek child, and once pregnant, sent for her mother as she
  was scared.
- Shortly after giving birth to a daughter, she died secondary to infection.
- Queen Alysanne bore some resentment towards the King for his insistence that
  she married.
- The King's daughter Saera provided a different challenge.
- Constantly mischievous, when a young girl teenager she seduced many older men.
- When the truth came out, Jaehaerys was furious but Saera was unrepentant.
- She was sent to become a Silent Sister but after a year and a half escaped.
- She was last seen in the Lysene pleasure house.
- This whole affair was said to forever haunt Jaehaerys.
- Similarly, Queen Alysanne largely withdrew from public life when these rulers
  entered the twilight years of their reign.
- Another of their daughters, Viserra, was equally mischievous but more
  ambitious and less scandalous.
- She wished to marry her older widowed brother in order to become queen.
- Instead she was betrothed to a Northman, to improve ties with that kingdom.
- On the night before she was due to depart, she decided to embark on one more
  night of fun.
- This ended with a horse riding accident in which she broke her neck.
- Heartbroken at the loss of many daughters, three in five years, Queen Alysanne
  appealed to the king to let her bring their daughter Saera back.
- Jaehaerys spoke strongly against this and drove a wedge between him and his
  wife which was only healed when their daughter, the septa Maegelle, healed the
  wound using their granddaughter's wedding as a path to healing.
- There was further tragedy for Jaehaerys.
- His son and heir, Aemon, went to aid Tarth in a battle against Myr.
- A scout fired a crossbow bolt at the Evenstar of Myr but missed and killed
  Aemon instead.
- In response, his younger brother slaughtered the Myrish.
- Though the realm and king hailed this brother as a hero, the king was forever
  changed.
- Viserys came of age and chose Balerion as his mount.
- Despite the beast's age, he coaxed him to the sky again but in 94 A.C., the
  last living creature who had seen Valeria in its grandeur died.
- Death came to Queen Alice in 100 A.C.
- She had outlived 10 of her 13 children, with her son the Spring Prince Baelon
  surviving, her son Vaegon in the citadel remaining emotionally distant to her
  and her disgraced daughter Saera still being dead in the eyes of the King.

## Heirs of the Dragon – A Question of Succession

- The prolific marriage of Jaehaerys and Alysanne left a major issue around
  succession.
- Jaehaerys’ first son had been killed and he had named his second son as his
  heir.
- Before his death, this son had fathered a daughter Rhaenys, who had married
  Corlys Velaryon and she had a daughter Laena and a son Laenor.
- Jaehaerys’ second son tragically died of sickness before his father but he
  also had had a son, Viserys.
- Jaehaerys also had one other living son, Vaegon, but he was an archmaester and
  his vows precluded him taking the throne.
- Jaehaerys convened a massive council where succession was discussed and his
  grandson and great-grandson were thought to be the two most appropriate heirs
- The claim of the grandson – Viserys – won out convincingly.
- His reign was said to be the pinnacle of the Targaryen dynasty with more
  dragons than ever before or since and more people claiming the blood of the
  dragon within them.
- His brother Daemon was a proud warrior and took control of the City Watch.
- He was well disciplined but harsh.
- Daemon was thought ill suited to be a ruler and Viserys did not name him his
  heir.
- His wife had a daughter, Rhaenyra.
- However, when Aemma then died giving birth to a son who subsequently died one
  day later, Daemon's claim to the Iron Throne was much improved.
- Viserys took a new wife, Alicent Hightower, who quickly gave him children,
  Aegon, Helaena, Aemond and Daeron.
- He had passed over another choice, Leanna Velaryon, which angered her father,
  Corlys Velaryon.
- He thought that his son had been spurned in the race for succession now his
  daughter had been spurned in marriage.
- He and Daemon embarked on a war with Essos to control the trading route
  between the kingdoms.
- Viserys had named his daughter by his first wife, Rhaenyra, as his heir – even
  though female succession was against the ruling of the council which made him
  king.
- This did not change when his new Queen gave him sons.
- The eldest son was married to the eldest daughter (Aegon to Helaena; they had
  three children including a daughter Jaehaerys.)
- The Queen and the Princess therefore formed rival factions within King's
  Landing.
- Prince Daemon returned to court, reconciled with his brother and a happy six
  months ensued.
- However, a scandal involving Princess Rhaenyra resulted in his banishment.
- Queen Alicent wished Rhaenyra to marry her son but instead Viserys and his
  council chose Leanor Velaryon, to strengthen the bloodlines and the ties
  between these families.
- Their marriage was loveless, with Leanor said to prefer the company of men.
- In fact, the sons his wife bore for him (Jacaerys, Lucerys and Joffrey) did
  not resemble him at all, but rather Harwin Strong.
- Daemon's wife died and he use the opportunity to marry Leana Velaryon in an
  attempt to strengthen his claim to the throne.
- Rhaenyra became close with Leana and Daemon and betrothed two of her sons to
  their twin daughters.
- Laena finally gave Daemon a son but he died within the hour and Laena not long
  afterwards.
- Leanor was killed by a knight from his own house.
- The King’s Hand and one of his sons died in a fire which was presumed to have
  been started deliberately.
- Daemon married Rhaenyra hurriedly and quickly had two sons, naming one of them
  Aegon despite Viserys already having a son by Alicent called Aegon.
- The second son was called Viserys.
- That Aegon married Jahaera and she gave him twins, a boy and a girl.
- The competition between Queen Alicent and Princess Rhaenyra was intensifying,
  both having an eldest son called Aegon.
- King Viserys's health failed and he left most of the ruling of the kingdom to
  his small council before dying and 129 A.C.

## The Dying of the Dragons – the Blacks and the Greens

- After Viserys died, Queen Alicent kept the news secret, convened a council and
  planned to place her son Aegon II on the Iron Throne.
- The first casualty of the conflict was the Master of Coin, who believed
  Rhaenyrs was the rightful queen.
- When word reached Dragonstone, Rhaena convened her own council.
- She had fewer supporters but more dragons, though she lacked sufficient riders
  for all her dragons and she did not wish to destroy Westeros just to claim
  leadership of it.
- A final attempt at brokering a truce failed.

## The Dying of the Dragons – A Son for a Son

- Rhaena seeks new allies.
- Her son Jacaerys/Jace successfully treats with Harrenhal, the Vale of Arryn
  and Winterfell.
- Her second son, Lucerys/Luke, goes to Storm's End where, due to previous
  family ties, Rhaena is confident, perhaps even complacent, about their
  allegiance.
- However, Prince Aemond is already there and the lord, Borros Baratheon, asks
  Luke which of his daughters he will marry for this allegiance.
- Luke is already betrothed whereas Aemond is not and has agreed to marry a
  daughter.
- Borros therefore declares that his allegiance is with Aegon II.
- Aemond then challenges Luke to pay recompense for the eye he lost when Luke
  tried to stop him claiming Vhagar.
- Borros forbids any bloodshed under his roof.
- One of his daughters, who was not been chosen to wife by Aemond, goads him
  into a frenzy and he mounts his dragon, which was five times larger than
  Luke's, and kills them both.
- Almost everyone within Queen Alicent's court thinks that this was an unwise
  and brash move.
- All except her son King Aegon.
- On hearing the news, Daemon hires two men in King's Landing who sneak into the
  Red Keep and capture Aegon's wife Heleena and their three children.
- Helaena was asked to choose one of her sons to pay for Luke;
  the assassin kills the other son.
- Helaena never recovered and could not look the son she had chosen (but had
  been spared) in the eye again.

## The Dying of the Dragons – The Red and the Gold

- After the death of the two sons, open war replaced narrow schemes.
- Rhaenyra and Aegon made numerous efforts to maximise their allegiances.
- Rhaenyra was reticent to directly attack King's Landing whereas Aegon made a
  direct attempt on her life on Dragonstone.
- Aegon laid siege to Rook's rest as a trap into which Rhaenys fell with her red
  dragon, Meleys.
- Though she began to break the siege, she was ambushed by the Aegon on Sunfyre
  and Aemond on Vhagar.
- In the fight, Rhaenys and Meleys died, Aegon was severely burned and Sunfyre
  rendered flightless.
- Vhagar was relatively unharmed.
- Rhaenys, the Queen Who Never Was, was 55 years old.
- King Aegon was so severely hurt that he spent the best part of the next year
  in bed with his brother came and came and assuming the title Prince Regent.
- Rhaenyra's eldest son, Jace, helped her find refuge for his younger three
  brothers.
- Rhaenys' husband blamed Rhaenyra for her death, saying that the Queen was the
  one who should have answered the summons to break the siege.
- Allies who Jace had courted, including Winterfell, were nearly ready to enter
  the fray.
- An assault on King's Landing looked like it would be the next move.
- King's Landing now only had two dragons defending it, and the rider of one was
  Helaena, effectively useless due to her grief.
- To maximise their advantage in dragons, Jacaerys would seek bastard
  Targaryens, born largely due to the right of First Night.
- Despite many deaths, eventually Jace had five dragonriders on his side,
  including himself.
- The assault on King's Landing was imminent.
- However, Otto Hightower had conspired with the Triarchy of Essos.
- They led a fleet of ships against Dragonstone.
- By chance, they intercepted Jace's two younger brothers, capturing one.
- The other escaped on his Dragon.
- They then assaulted Dragonstone, and were close to victory until the five
  dragons fell on them.
- Somehow, in the heat of battle, Vermax was felled and, though he leapt to
  safety, Jace was killed shortly afterwards.
- Aemond was worried about his uncle Daemon at Harrenhal and took his dragon to
  assault him.
- Rhaenyra pressed the attack on King's Landing.
- With Queen Haelena still incapacitated, King's Landing was effectively
  leaderless and dragonless.
- Dowager Queen Alicent organised the defence but she was betrayed.
- Daemon was not in Harrenhal as he was part of the planned attack on King’s
  Landing.
- When the city watch – named the Gold Cloaks for the cloaks Daemon gave them
  when he was their captain – saw him and his dragon, they turned on their
  leaders.
- Seeing defeat, Alicent gave up the throne to Rhaenyra.
- King Aegon and his remaining children somehow escaped.
- It was noted that Rhaenyra had cuts on her hands and legs after sitting on the
  Iron Throne – the wise saw this as an omen that her reign would be short.

## The Dying of the Dragons – Rhaenyra Triumphant

- Aemond arrived at Harrenhal, discovering it deserted and thinking himself
  victorious.
- This increased his anger when he heard that Daemon had actually left to assist
  the taking of King's Landing.
- His forces suffered a defeat when returning as the north joined the fight.
- He sent his commanders away but planned to fight a guerilla war by himself on
  top of his dragon.
- His hope was that Rhaenyra would eventually send one of her dragons against
  him and he would defeat it, evening the numbers of dragons on each side.
- In King's Landing, Rhaenyra, due to enemies she chose to execute, taxes her
  master of coin started to levy and soldiers she chose to knight, became very
  unpopular.
- A major tragedy which turned the tide for Rhaenyra was the fate of Maelor
  Targaryen.
- He was the son who Helaena had chosen to die but had been spared by the
  assassin's cruelty.
- He was hidden and under the protection of a knight but was discovered.
- In the confusion over how to reap the reward for his capture and who would get
  their award, the crowd of people who had discovered him killed him and nearly
  tore him apart.
- Though nothing to do with her, it was commonly spread that it was Rhaenyra who
  had condemned the young child to death.
- When the north eventually joined the fighting, they were instrumental in a
  victory that saw the death of the Hand of King Aegon/Prince Aemond.
- Due to persistent threats, advisors counselled Rhaenyra to seek some sort of
  peace.
- She chose a different path, sending out a massive offensive to try to end the
  war but keeping herself and three dragons to defend King's Landing.
- A major defeat was to come, however.
- Lord Ormund Hightower's massive army were to be fought outside Tumbletown, led
  by the two base-born dragon riders Ulf White and Hugh Hammer.
- Though a valiant sally from the northmen saw Lord Hightower killed, the
  two seeds turned sides when they saw Daeron on Tessarion and poured dragon
  fire on Rhaenyra's army.
- To the north, Prince Daemon and the young seed Nettles sought Aemond upon
  Vhagar over the Trident, but never found him.
- There were rumours that Daemon and the girl were lovers.
- Aemond’s attacks continued to be short, effective and over before anyone was
  able to catch him.
- With all the betrayals, Queen Rhaenyra became overly suspicious.
- She declared that the girl Nettles should be put to death but that this should
  be kept secret from Prince Daemon.
- The dragonrider Addam Velaryon was to be questioned to see if he was a traitor
  lie the other seeds, but when the Gold Cloaks went to seize him, he burst from
  the Dragon pit on Seasmoke, being forewarned by his father.
- Corlys was therefore arrested.
- In the chaos of the multiple betrayals, both the city of King's Landing, now
  closed off, and the camp of Lord Hightower in Tumbleton descended into what
  approached chaos and debauchery.
- A preacher, the Shepherd, rose up within King's Landing claiming the Targaryen
  family to be evil and cursed.
- One of the turncoats, Hugh Hammer, declared himself fit to be the next king.
- At Maidenpool, the Lord's maester delivered the Queen's orders for Nettles'
  head.
- Seeing no good conclusion, the maester delivered the letter instead to Daemon
  Targaryen, claiming the Lord had never seen it.
- The next morning, Nettles departed, never to be seen again.
- Daemon declared that he would go to Harrenhal alone and await battle with
  Aemond.
- The Lord of Maidenpool changed his allegiance from Rhaenyra to Aegon.
- On the 14th day, Aemond arrived at Harrenhal with Alice Rivers.
- The young prince with the old Dragon Vhagar thought the old prince Daemon with
  the younger dragon Caraxes.
- Daemon fought more wisely, using his faster Dragon to grab Vhagar by the neck
  and fly directly at the God's Eye river.
- Daemon had wisely neglected to clip himself into his dragon and he nimbly
  moved from one dragon to the other, removing Aemond's helmet then killing him
  with his sword.
- The dragons hit the ground with a force strong enough to kill them both.
- Aemond’s bones were found at the bottom of the river in later years, still
  chained to his dragon.
- The body of Prince Daemon was never found and, although there are rumours that
  he survived and lived out the rest of his life in hiding with Nettles, these
  are fanciful.
- As this duel had been nearly private, word of their death was not widely known
  for some time.
- Vhagar was 181 years old, the last living creature who had been alive during
  Aegon's conquest.

## The Dying of the Dragons – Rhaenyra Overthrown

- With the warrant for the arrest of Addam Velaryon and the imprisonment of his
  father Corlys, the Velaryon forces loyal to them began to leave Rhaenyra.
- Queen Helaena committed suicide by throwing herself from the battlements.
- There were rumours of it being murder with Queen Rhaenyra being responsible
  for the order.
- Helena had been popular; Rhaenyra was hated.
- In combination with the deaths of Helaena's two children, the presumed murder
  on Rhaenyra's order sent the common people into a frenzy.
- The preacher known as the Shepherd and his followers attacked and defeated the
  Gold Cloaks.
- Numerous other bands of common people attacked Rhaenyra's followers within
  King’s Landing.
- Holed up in the Red Keep, Rhaenyra's 10 year old son Joffrey was angry that he
  was not allowed to fight, snuck away, loose and climbed on top of his mother's
  dragon.
- He was trying to get to the dragon pit, on which 10,000 of the Shepherd's
  followers were converging to kill all the dragons.
- He never made it, falling from the dragon to his death.
- At great cost, the horde were able to kill the four dragons chained up in the
  Dragon Pit and then killed Rhaenyra's dragon when it turned on the attackers
  rather than flying to safety.
- A month of chaos followed in King's Landing, with three heroes of the common
  folk declaring themselves as kings.
- With Aemond dead and Aegon Targaryen missing since his escape from King’s
  Landing, the green's had no leader.
- Hard Hugh Hammer declared himself a worthy king, saying he had the same right
  as Aegon the Conqueror, namely a dragon.
- With Vhagar dead, Vermithor was the largest dragon alive by far.
- The remaining Targaryen, Daeron the Daring, was only a child in the eyes of
  Hugh and did not deserve the throne or his allegiance.
- As the loyalists to Daeron prepared to murder Hugh, Addam Velaryon launched a
  surprise attack upon his dragon Seasmoke.
- Eager to prove his loyalty to Queen Rhaenyra, he had spent his brief time in
  exile raising an army, 4000 strong, for her.
- This army now attacked Tumbleton in the second battle of that town.
- Hugh and Daeron were killed on the ground; Addam's dragon managed to kill two
  of the three dragons but died itself in the process.
- The last remaining dragon rider was the traitor Ulf White and his dragon
  Silverwing.
- He declared that they would march upon King's Landing, the high lords could
  have the city but he would have the throne.
- He was assassinated shortly afterwards.
- Queen Rhaenyra decided she had to return to Dragonstone and attempt to hatch
  another dragon.
- There, she suffered her final betrayal.
- King Aegon had secretly snuck onto the island and began a coup.
- The only part of his plan that went wrong was that Baela Targaryen, daughter
  of Daemon by his first wife, escaped, mounted her dragon and attacked him.
- She nearly killed both Aegon and Sunfyre but her dragon died; Baela was
  captured.
- Aegon fed Rhaenyra to his dragon.
- He kept her son Aegon III as hostage.

## The Dying of the Dragons – The Short, Sad Reign of King Aegon II

- The Dragon Sunfyre only lasted nine days before dying from his wounds.
- Enraged, Aegon considered executing Baela but was advised against this as she
  was of the Velaryon family as well as the Targaryen family.
- Aegon was unable to leave Dragonstone as Alyn, son of Corlys Velaryon, the
  Seasnake, was blockading it with his fleet.
- King's Landing was finally liberated from its three self-proclaimed kings, not
  by the Hightower army from Tumbleton, but by Borros Baratheon, who had begun to
  march north from the Stormlands when he heard Rhaenyra had fled the city.
- Queen Alicent was restored to the throne as dowager queen and a council was
  held which decided that they should treat with Corlys, despite his previous
  betrayals, he was an old man and likely to die soon so would not be a problem
  for long - furthermore his son was preventing Aegon from returning to the
  city.
- His demands were that Aegon III be named as successor to Aegon II and also
  that Baela Targaryen, his granddaughter should be released.
- Aegon’s return to King's Landing was less than glorious – he was carried in in
  a concealed litter.
- Once he had dealt with the peasant kings, he decided on a campaign against the
  former loyalists of Rhaenyra.
- This proved unwise as it strengthened their resolve against him.
- In particular, in the Vale, Baela's twin Rhaena had a hatchling dragon.
- Though it would not be battle worthy for years, it legitimised her as a true
  Targaryen.
- Only three other living dragons remains in Westeros and Aegon wanted none of
  them.
- He sent word to Dragonstone that he wanted a new hatchling, so seven eggs were
  sent to him but none successfully yielded a dragon.
- Aegon announced he did not wish to keep the promises his mother had made to
  Corlys Velaryon – he did not wish to marry Baela nor did he wish another
  marriage betrothal promised by Alicent, namely of Aegon's daughter Jaehaera to
  Aegon III.
- This angered Corlys who stormed out of the council.
- As Alyn Velaryon still held power on the sea, Aegon’s advisors cautioned the
  king.
- Chief among them was Larys Strong, the Clubfoot, who said they should continue
  to deceive Corlys by going along with his wishes.
- He said that he would go to Corlys on the King’s behalf and restore the peace
  between them.
- In truth, he told Corlys of the king's plan to deceive him.
- Swiftly, Aegon the second was overthrown, being assassinated using poisoned
  wine.
- His mother Queen Alicent was imprisoned, again.
- Baela was spirited away from the cells.
- A few more loyalists to Aegon were also killed.
- Corlys Velaryon declared, "The King is dead; long live the King," producing
  Aegon III as King – just as Aegon II had agreed.
- Also as part of that agreement, Aegon III married Jaehaera.
- The Dance of the Dragons was over; the greens and the blacks were re-united in
  marriage.

## Aftermath – The Hour of The Wolf

- Though King Aegon III's rain lasted 26 years, it was neither a happy time
  in the kingdom nor was he a happy or memorable king.
- He took the throne at 10 years old and the peace he came in to at the end of
  the Dance of the Dragons lasted only two weeks.
- Corlys Velaryon wanted to move quickly to spread peace throughout the realm.
- Baela and Rhaena Targaryen returned to the city with Rhaena's hatchling, to
  much adoration from the common folk.
- Aegon III was appalled by the dragon.
- However, Cregan Stark arrived at King's Landing, determined to fulfil the
  promises he had made to Jace Targaryen.
- He was appalled by the method of poisoning used on Aegon II.
- He blamed Corlys for this murder and imprisoned him, Larys Strong and many
  more in the court.
- He believed the war would not be over until the Stormlands, Oldtown and
  Casterly Rock were conquered.
- Such was his strength and relative experience compare to many of the other
  high lords, they fell in line.
- However, skilful diplomacy thwarted his war.
- With his war taken from him, Stark, now Hand of the King, turned his attention
  to seeking justice for the poisoning of Aegon II.
- He condemned multiple people to death, the most important being Corlys
  Velaryon and Larys Strong.
- Aegon III wanted Corlys released; Stark agreed, despite the young prince
  having no authority over him until he turned 16.
- The majority of the condemned pleaded to be sent to the Wall and Stark
  consented.
- The two who chose to die were Larys Strong and a member of the king's guard
  who thought it wrong he had outlived his king.

## Under The Regents – The Hooded Hand

- Aegon and Jahaera were married.
- Cregan Stark gave back the position of hand to return north.
- The true rulers of Westeros were now the Lord Protector, a council of regents
  and Tyland Lannister, the new Hand of the King, who in truth had the most
  power in part due to his intelligence and designs.
- He was so disfigured after his torture that he wore a hood and was known as
  the Hooded Hand.
- His first priorities included rebuilding the Dragon Pit and commissioning the
  building of 50 war galleys, thought by many to be an attempt to lessen the
  reliance on house Velaryon.
- In the second year of his reign, the venerable Seasnake Corlys Velaryon died
  at age 79.
- His legitimised bastard son Alyn took his place, though this was disputed by
  two nephews who lost their lives in the ensuing fight.
- Alyn, age 16, expected to take his father's place on the council of regents
  but an older man had already been placed there.
- This simple dispute about succession in the Velaryon house brought home to
  Tyland Lannister that succession of the Iron Throne was currently a major
  issue.
- There was general agreement that Aegon's twin half sisters, Rhaena and Baela,
  were the only real successors currently alive.
- The council could not decide which who had the better claim.
- Rhaena and Baela were very different.
- Rhaena was more ladylike; Baela more outgoing.
- It was agreed that a husband should be sought for Baela as soon as possible.
- A marriage was arranged for Baela but she disliked it, fled the city and
  married Alyn Velaryon.
- Lord Tyland decided it would be better to cover up Baela's defiance and so
  presented the match to the realm as his own doing.
- Her defiance however did settle the matter on who should be Aegon’s here – it
  should be Rhaena.
- Eager to avoid the mistakes he had previously made, Tyland involved Rhaena in
  the decisions about her marriage.
- It was a time of widows, with so many Lords having died during the Dance of
  the Dragons, never before had women had such positions of power.
- One of these was Alys Rivers, paramour of Aemond Targaryen who now held
  Harrenhal, in which she claimed to have Aemond's trueborn son and a dragon.
- The year ended peacefully but just three days in to 133 AC, winter fever
  arrived in Westeros.
- As with all great epidemics, the high born were not spared.
- Dowager Queen Alicent was one notable death.
- As the curse of the epidermic was slowing, the last major death was hand of
  the king Lord Tyland Lannister.

## Under the Regents – War and Peace and Cattle Shows

- Aegon announced new King's guard and regents, including promoting Alyn
  Velaryon and bringing Baela back to the capital.
- However, he had still not come of age and the new hand, Unwin Peake,
  overturned the king's wishes.
- Aegon accepted but there was no love between him and Unwin.
- A battle for the Stepstones at sea was planned, involving the Velaryon fleet.
- Alyn disagreed with the plan and launched his own attack, successfully
  defeating the Braavos fleet and earning him the name Oakenfist for one of his
  ramming attacks.
- Unwin Peake rewarded him by promoting him to admiral then immediately
  declaring it was his duty to go and battle the great joys on the southern
  coast of Westeros.
- It was a shrewd move – victory would be beneficial for the realm and defeat
  would lessen house Velaryon.
- Before he left, Baela told Alyn she was pregnant.
- The Iron Throne was able to negotiate a truce with Braavos, but due to the
  vast price to Sea Lord demanded, a loan from the Iron Bank had to be taken
  and to pay this back, many unpopular taxes which Tyland Lannister had
  abolished were re-instated.
- Rhaena Targaryen in miscarried.
- The 10-year-old Queen Jahaera threw herself from the walls of Maegor's
  Holdfast, just as her mother had done.
- Also just like her mother's suicide, murder was suspected and not even the
  King was free of suspicion.
- Unwin Peake then made a mistake.
- Too soon after the death of Jahaera, he suggested Aegon remarried and said
  that it should be his daughter (and only remaining child) that he married.
- The Council of Regents denied this request and instead a ball was planned,
  during which Aegon could see the eligible ladies in his kingdom.
- The schemes of Unwin did not stop however and many of the most eligible met
  with unfortunate accidents before the ball.
- Just as the stream of suiters was nearly finished, Baela and Rhaena Targaryen
  entered the throne room with Daenaera Velaryon.
- The king picked her.
- Unwin protested as this girl was not of childbearing age and would not be for
  some time, his council over ruled him and they were married before the end of
  the year.

## Under the Regents – The Voyages of Alyn Oakenfist

- Sent to defeat the Iron Born, Alyn's job was done for him by one of the Red
  Kacken's salt wives, named Tess, who murdered him in his sleep on the eve of
  battle.
- The Iron Fleet disbanded.
- Alyn agreed to linger at Lannisport until their fleet was sufficiently rebuilt
  to defend against any research and Iron Born.
- On his way to the Iron Islands, Alyn had befriended the ruler of Dorne and the
  eccentric self-styled 'queen' of the Stepstones, Racallio Ryndoon.
- Stopping in Dorne on his way back, an envoy from Tyrosh told Alyn a secret
  which caused him to immediately change his course for Lys.
- Back in King's Landing, the young Queen Daenaera was a hit and lightened the
  spirits of Aegon.
- Unwin Peake grew more bitter and resentful of Baela and Rhaena in particular.
- He was suspicious that Baela was planning to put her own child on the throne.
- Even when she had a daughter, Peake was not satisfied.
- Alyn Oakenfist send word ahead that he was returning to King's Landing with a
  great treasure, again angering Peake.
- The treasure was none other than Viserys Targeryen, younger brother of the
  king, from whom he had been separated when the ship their mother had put them
  on to send them to safety was attacked.
- Aegon had always felt great shame that he had escaped on his dragon and left
  his brother behind.
- Viserys’ guardian had married him to his daughter, who was 19 compared to
  Viserys’ 12 years.
- Alyn Velaryon had bargained for the return of Viserys, including agreeing to
  legitimise this marriage.
- The question of succession was now much less contentious.
- Viserys was the king's heir and his wife, compared to the king's six-year-old
  wife, was likely to bear a son and further heir, taking the pressure off the
  young queen.
- This was all too much for Unwin Peake, especially when the council of regents
  agreed with all Alyn Velaryon had done, and he resigned his office.

## The Lysene Spring

- Viserys remained ever popular in King's Landing, but his wife Larra was not,
  principally because she kept her old gods from Lys.
- Rhaena flew her dragon Morning for the first time, which delighted Viserys and
  Daenaera; Aegon was unnerved.
- Rhaena went to Dragonstone where, she said, dragonriders were more welcome.
- A series of tragedies befell house Targaryen, leading many to conclude that
  the seven gods of Westeros had rejected them once and for all.
- Larra's position in King's Landing allowed many noble houses from Lys to
  advance themselves significantly, this being referred to as the Lysene Spring.
- A deformed dragon hatched from the egg of Alyn and Baela's child, attacked her
  and had to be killed.
- Viserys and Larra had a child, a boy who they named Aegon after the king.
- Aegon decreed that no dragon should be allowed within the Red Keep which
  angered his brother and there was silence between them for a time.
- They were reconciled after an assassination attempt on the king.
- His dinner was poisoned, successfully killing his food taster, a boy of whom
  the king was extremely fond.
- His young wife was also poisoned but saved by powerful purgatives.
- The brothers reconciled but Aegon returned to his sullen and disinterested
  ways.
- Rhaena's husband, one of the regents, was sent to the Vale of Arryn to settle
  a succession debate and end the last open conflict in Westeros.
- After a heated debate outside the gates of the castle, he was slain by
  crossbowman, reigniting the war.
- The involvement of a Lysene commander in this campaign in the Vale caused
  further disquiet, especially among the pious nobleman of King's Landing who
  especially despised the foreign gods of the Lysene.
- Any hope the king had that the prominent position given to this foreigner
  might be overlooked was dismissed when the campaign began so disastrously.
- Unsurprisingly, Alyn Oakenfist was successful by sea but on land, the passage
  through the mountains was treacherous due to the lingering winter snows, hill
  tribes and the unfortunate discovery of Nettles and her dragon Sheepstealer in
  a cave.
- Nearly 100 men were killed or wounded, and dragon and rider fled deep into the
  mountains, their presence never to be recorded again in Westeros history.
- Numerous of the important men and houses of list died either in suspicious
  circumstances or an obvious assassinations shortly afterwards.
- The perpetrator was uncertain.
- In Lys, the noble family from whom Viserys’ wife Larra came were overthrown,
  and many Lysene who had set up a banking system in King's Landing now found
  themselves without financial backing.
- This led to a hasty attempted coup, with many Lysene being killed and the king
  having to retreat to the Red Keep.
- A relatively bloodless siege by what remained of the king's council of regents
  occurred, which only ended when the king was told that his former Hand was
  ready to confess to the whole conspiracy to both kill him and place his brother
  on the throne.
- A trial was held within the red keep but Viserys was able to work out that the
  confession of the former Hand was false.
- Attention was therefore turned to the king's confessor, who surprisingly
  easily admitted his crimes and revealed to the conspiracy.
- It was not the Lysene foreigners who had attempted to remove Aegon III from
  the throne but Westerosi from within his council of regents, court and even
  king's guard.
- It was long suspected that Unwin Peake was the unseen mastermind.
- When all the trials were finished, and a new hand from the north was in place
  with a new council of regents and King’s Guard, Westeros returned to a sort of
  peace.
- On the day of the his 16th birthday, Aegon declared he no longer needed
  regents and dismissed the hand that had been chosen for him.
- Though he gladly return home, he was resentful towards the king.
- Regency had come to an end.

[Home](/blog "ohthreefive")