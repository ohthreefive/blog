+++

date = "2019-04-25T11:56:01Z"
title = "Summaries: Homo Deus"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Homo Deus: A Brief History of Tomorrow

**Yuval Noah Harari**

### 1. The New Human Agenda

- The main issues affecting pre-20th century mankind were famine, plague & war
- With these difficulties solved, mankind will push towards new aims.
- The first explored is that of prolonging life.
- The author notes that modern medicine has really just prevented premature
  death but that lifespan has not really increased.
- Artificial augmentation of the human body or manipulation of it might extend
  life significantly.
- The second aim is for happiness and the author theorises this will be achieved
  through chemical manipulation of the brain.
- The third aim is the upgrading of humans – a palpable example is the move from
  plastic surgery being developed to help war victims to it being a multi-
  billion dollar industry to improve the appearance of healthy individuals.
- The discussion extends from this superficial process to genetic manipulation.
- These three aims are defined as a mortality, bliss and divinity.
- The author is careful to point out that these predictions are not a framework
  or a goal but a basis for discussion.
- He also mentions that the purpose of studying history is to inform current and
  future practice.
- He gives the example of the lawns: having a lawn was a sign of great wealth in
  European society and developed to be a sign of middle-class affluence and
  nowadays is aspirational in nature.
- The author suggests that with this knowledge, you may wish a Japanese rock
  garden or other decoration in front of your house.
- The lawn is historical and knowing its origin should free modern people from
  this apparent status symbol to pursue their own aspirations.

## Part I: Homo sapiens Conquers the world

### 2. The Anthropocene

- The author describes the previous 70,000 years as the era of humanity.
- Before long, the effects of mankind on the world will be similar – in terms of
  extinction of other species – to global disasters such as the meteorite which
  wiped out the dinosaurs or previous ice ages.
- The chapter segues into discussing how, while there are huge numbers of
  animals in the world, domesticated and industrial animals far outnumber wild
  animals.
- A further segue is taken into discussing the difference between the physical
  and the psychological requirements for life.
- A deliberate contradiction is presented.
- Early parenting advice was to feed the children on schedule, ignoring
  emotional cues such as crying.
- This has largely been abandoned, of course.
- The contradiction is made with cows and other farming animals which are often
  separated from their mothers at birth.
- The author notes that studies on animals such as chimpanzees has clearly shown
  that the psychological needs of the infant are as important as the physical
  needs such as food.

### 3. The human spark

- A discussion about the soul, consciousness and the many other things which are
  said to make humans different from animals.
- There is a discussion about how, when we understand how electrical impulses
  from the brain cause actions in the limbs, for example, there is any need for
  any subjective feelings, e.g. fear or similar.
- The notion of describing organisms as collections of algorithms is discussed.
- It is noted that previous generations described organisms as engines as
  engines were the peak of their scientific and engineering prowess and so were
  the best comparison for complex organisms.
- In the modern world, computers and algorithms are as complicated as it gets so
  it is natural that we compare ourselves to them.
- Issues with this theory include the fact that if all actions can be explained
  by a predetermined set of instructions, then why is there a need for decision-
  making?
- Similarly, what are the differences between humans and other animals if all
  actions are written within algorithms?
- The limitations of our understanding of how the mind and the brain may be
  separate are laid bare.
- There is a long discussion about how the fact that human can form huge groups
  separates us from animals.
- These groups are organised by fictions or subjective truths, something which
  both we think animals lack and which cannot (yet) be explained by biology.
- Examples are religion, money etc.

## Part II: Homo Sapiens Gives Meaning to the World

### 4. The Storytellers

- The chapter details the various ways in which the subjective fictions already
  introduced have allowed humans to form these massive organisational groups
  which have in turn allowed them to progress beyond other animals on the
  planet.
- It is noted that many fictions (such as religion) existed long before history
  as writing had not been invented yet – and at this time it was *not* possible
  to form truly massive groups as there was no record of the fictions.
- This changed when the Sumerians invented writing; which was invented at the
  same time as money.
- In order for the subject of fictions to hold any strength, there must be
  belief in them.
- The best example of this often seems to be money: without belief in the value
  of pieces of paper, they would have no value.
- The author makes a good point that one can identify fictions by asking the
  question: can they suffer?
- A country can lose a war but the country comes to no harm.
- If a soldier is shot, he truly suffers the harm.
- The chapter also discusses the fact that fictions can be stronger than the
  truth.
- An example is Europeans arbitrarily dividing Central Africa into countries.
- The borders had no basis in reality.
- Years later, when the European empires collapsed, the Africans retained these
  borders as they were now too ingrained.

### 5. The Odd Couple

- A chapter discussing science and religion and their place in the world.
- Ethics is linked thematically to religion.
- Religion does not require truth as science does: who says god dictated the Ten
- Commandments? What if Moses made them up? Who observed this?
- Religion and ethics are not dismissed by the author, however.
- An example is the Three Gorges Dam over the Yangtze: economists could estimate
  its cost and benefit, engineers could calculate the load it would have to bear
  etc. but which branch of science can prove whether or not the ecological
  changes (flooding, potential loss of the river dolphin) are outweigh by the
  benefits?
- Can science measure the impact of an extinction, and if not, is a non-
  fact/truth based world-view not also necessary?
- The chapter concludes with a discussion about humanism, which is kind of an
  amalgamation of both world-views.
- The author states he does not think humanism will last and will also be the
  subject of the remainder of the book.

### 6. The Modern Covenant

- The chapter begins by saying that the modern world is exchanging meaning for
  power.
- Pre-modern beliefs in predetermined fates (as in religious dogma such as, "If
  you live well you will go to heaven,") give comfort but fate was
  predetermined, removing power.
- Science aims to increase power but the modern human wants to have his cake and
  eat it by also having the security of knowing what will happen.
- Religion gives meaning to the universe; science declares that there is none.
- There is a long discussion about growth and capitalism.
- Capitalism is spoken of as like a religion.
- Growth is said to be uniquely human: a fox cannot increase the supply of
  rabbits.
- Growth comes at a cost of using up finite earthly resources, unless that
  growth leads to new resources.
- For example, inventing a new way to mine coal increases the use of a finite
  resource, but inventing a new way of harassing solar power does not.
- The modern covenant is said to be a contract – we give up our belief in the
  divine plan and, by extension, there being any meaning to existence and in
  return we gain power by controlling her own destinies.
- The author states that loss of organised religion should lead to mass chaos
  and disorganisation, however this has not come to pass.
- He believes this is because of the rise of humanism.

### 7. The Humanist Revolution

- Humanism, a development in the previous few centuries, places the human at the
  centre of the universe.
- The author presents the phrase, "Beauty is in the eye of the beholder," as a
  neat, albeit reductive, definition of humanism: art is art because somebody
  finds it beautiful.
- There is a discussion about ethics in capitalism and and argument which states
  that the consumer decides the ethical standards, not the producer, eg.
  chickens which produce enough meat to feed the world but which cannot stand on
  their own are ethical because customers choose to pay for them.
- Humanism can incorporate religion: I believe in god because I chose to.
- A very long discussion about liberalism, capitalism, communism and socialism
  occurs, in which these are described as replacing or overcoming religious
  dogma as the forward driving force in the 19th and 20th centuries.
- Eventually, the chapter turns back towards discussing the future and
  mentioning that these political systems will also be replaced by, essentially,
  technology as the new religion.
- The author makes an excellent point about why traditional and fundamental
  religions will be left behind.
- The Bible or the Quran had nothing to say about artificial intelligence,
  genetic manipulation and biotechnology.
- How can religions in which ancient texts set the agenda for society in the
  future?
- The author does not say that these religions will be abolished, but that their
  role will become more and more reactive.
- Similarly however, the author states that these humanist revolutions he has
  described will also be challenged by technology.
- With algorithms which can predict the desires and requirements of individuals
  being deciphered, how can society based on the will of humans be considered to
  exist?
- The final part of the book will discuss this.

## Part III: Homo Sapiens Loses Control

### 8. The Time Bomb in the Laboratory

- An assault on free will and the idea of the individual self and the effect of
  such on the meaning of life.
- Free will is debunked by drawing attention to the numerous ways in which it
  has already been debunked in science: a patient in an MRI scanner asked to
  press one of two switches will display activity in their brain which can be
  used to predict which switch they will press before they have even decided
  themselves.
- While the decision to do something may still be due to the will of the person,
  the _desire_ to make that decision may be predetermined.
- The brain is divided in 2 to hemispheres which, although connected, can be
  disconnected both by disease and surgery.
- When this happens, we have discovered that they have different properties.
- There is both an experiential and a narrative memory or self.
- The experiential self relies on strict facts and memory but these are
  unreliable.
- The narrative self averages experiences.
- It has been experimentally shown to give a person a better memory of a
  prolonged unpleasant event with some less pleasant moments than a short,
  entirely unpleasant event.
- An example of an evolutionary advantage of this is the euphoric response which
  many experience after the painful experience of labour.
- Further children are dependent on this!
- If there is no free will or free desire and there is no one self then how can
  the humanist dogma of the person being the meaning of life hold true?
- The chapter concludes with the author stating that new technology which will
  not respect the notion of free will is coming and will present a huge
  challenge.

### 9. The Great Decoupling

- A long, detailed chapter talking about how when algorithms and computers
  replace various aspects and tasks currently performed by humans, a new class
  of useless humans will be created and only the elite/super rich, who are able
  to technologically upgrade their bodies, will have any real purpose in
  shaping society.
- The simple example is the myriad virtual assistance built by technology
  companies.
- With enough data, they will be able to know their owner better than the owner
  of themselves as these assistants do not have a narrative self which clouds
  the truth.
- Personal assistants may negotiate with one another on behalf of their owner.
- The poor and even the middle-class, who had both military and economic
  significance, will lose that in future due to technology.
- With the rise of this new useless class, the fundamental principles of
  liberalism make no sense and saw something will have to fill the void.

### 10. The Ocean of Consciousness

- Two potential future technology-based religions will be discussed by the
  author.
- The first is said to be the less extreme: techno-humanism.
- Techno-humanism seeks to maintain some of the features of humanism but with
  some upgrades to the human mind.
- Two main issues are raised with this.
- Firstly, if a technological upgrade can improve decision-making, it may reduce
  empathy as this slows the decision-making process down, which might actually
  be a downgrade of the complexity of the human mind.
- Secondly, techno-humanism maintains the emphasis on the human will.
- However, if as biology is predicting, the human will is no more than an
  algorithm or a balance of chemicals, the will can be manipulated and therefore
  becomes meaningless.
- For example, how much hurt could Romeo and Juliet have avoided had they drunk
  a potion which made them love a member of their own house rather than drinking
  poison.

### 11. The Data Religion

- Data-ism is the more extreme example of what the new religion might be.
- Capitalism is likened to this data-centric world in that it is de-centralised,
  whereas communism was a centralised system.
- People are the infrastructure of the data religion.
- Again compared to capitalism, people set the price of bread through choice and
  dictated where production would rise and fall.
- In communism, there was a central minister for bread who could not hope to
  process all the information and act correctly.
- Currently, the world's most advanced algorithms are at least partly human
  designed, but no individual person can understand, for example, the Google
  search algorithm.
- Increasingly, AI will design the algorithms and they may be entirely
  unintelligible to humans.
- If all data is free, computers will have the information required to design
  algorithms that make all possible decisions in the best interest of humans,
  but without them.
- Again, this is largely theory: organisms may not be algorithms.

> If we take the really grand view of life, all other problems and developments
> are over-shadowed by three interlinked processes:
>
> 1. Science is converging on an all encompassing dogma, which says that
>    organisms are algorithms, and life is data processing.
> 2. Intelligence is de-coupling from consciousness.
> 3. Non-conscious but highly intelligent algorithms may soon know us better
>    than we know ourselves.
>
> These three processes raise three questions:
>
> 1. Are organisms really just algorithms and is life really just data
>    processing?
> 2. What's more valuable: intelligence or consciousness?
> 3. What will happen to society, politics and daily life when non-conscious but
>    highly intelligent algorithms soon know us better than we know ourselves?

[Home](/blog "ohthreefive")
