+++
date = "2020-12-29T08:04:11.884Z"
title = "Summaries: If... Then"
description = "A  summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# If Then

_Jill Lepore_

## Epilogue

> A quote from Simulmatics' stock offering, 1961:
> "The company proposes to engage principally in engaging probable human
> behaviour by the use of technology."

## Prologue

> "The mystery surrounding Simulmatics started with its name."
> Statement to Simulmatics corporation stockholders, 1966

- Jill Lepore summarises the history of the Simulmatics corporation and its
  forgotten influence on modern day democracy and technology companies.

## Part One: The Social Network

### Chapter 1: Madly for Adlai

- A brief summation of American political history from the Second World War to
  the 1952 presidential election.
- Democratic candidate Adlai Stevenson wanted to run an intellectual and
  somewhat ethical campaign compared to Eisenhower's Madison Avenue-inspired
  advertising-heavy campaign.
- At the same time, Remington industries had produced the Univac computer which
  was used for the first time to predict the presidential election result from
  exit polling data.
- It predicted a rather unexpected Eisenhower landslide – so unexpected that its
  owner did not reveal this to TV networks in case it was wrong.
- It was correct.
- Ed Greenfield saw this marvellous creation live on TV and began searching for
  scientists to power a prediction machine.

### Chapter 2: Impossible Man

- This is the story of Eugene Bardach, a policy expert employed by Greenfield
  and by Stevenson in his attempt to win the 1956 collection.
- Bardach was well versed and studied in behavioural sciences.

### Chapter 3: The Quiet American

- The story of a computer and data driven man called Ithiel de Sousa Pool.
- As a youth, he had to answer charges about whether or not he was a Communist
  before he gained sufficient security clearance to work for the Pentagon.
- When Greenfield saw the Univac computer in action, he realised he needed a
  computer man and reached out to Pool.

### Chapter 4: Artificial Intelligence

- A period including the late 1950s and early 1960s is discussed.
- During this time frame, the programming language Fortran was invented.
- Artificial intelligence became something of interest and Greenfield was at the
  forefront of pursuing it.

### Chapter 5: Project Macroscope

- The story of a maths genius with mental health issues – his wife had him
  committed for a period – whose election prediction engine, codenamed Project
  Macroscope, was highly sought after by Greenfield.
- There is a clear through-line from Project Macroscope to Cambridge Analytica
  and the like.
- Some thought it immoral at the time but Greenfield pursued.

## Part Two: The People Machine

### Chapter 6: The IBM President

- Members of the Simulmatics Corporation quietly began to move their allegiance
  towards John F Kennedy in the lead up to the 1960 election.
- They saw Stevenson as unelectable.
- Key to their offering was detailed data on black voters, who traditionally
  supported the party of Abraham Lincoln – Republicans.
- They believed their data could help Kennedy defeat Nixon.
- During the actual election, predictions based on Simulmatics data was the
  first to predict and narrow Kennedy win, including in the popular vote.
- When this came to pass, Simulmatics used the result to go on marketing
  offensive.

### Chapter 7: Billion-Dollar Brain

- After the 1960 election, Simulmatics used their success to advertise
  furiously.
- Bobby Kennedy saw this as his political campaign being used for the gain of
  the company and held a grudge because of it.
- For this reason, by the time of the 1962 midterms, Simulmatics were not
  employed by the Democrats and thought that any work for the GOP would be
  political suicide.
- They therefore sought work and projects elsewhere but data were scarce and
  their price was high.
- There were attempts to discredit Kennedy's election win because of the
  influence of the computer.
- The group at the core of the Simulmatics Corporation was personally
  dysfunctional.

### Chapter 8: Fail-Safe

- The Simulmatics Corporation was eventually hired by the NY Times to assist
  with their reporting of the election and to help them keep up with competition
  from television.
- Around the same time, the Cuban missile crisis and America's war with
  communism was heating up.
- Also, advertising companies had caught on to the idea of simulation and data
  mining.
- They had an advantage over the Simulmatics in that they had been collecting
  data about consumers for decades and so they already had material to analyse.
- Simulmatics and the Times had a fractious relationship with the former failing
  their first test at the mid-term elections due to difficulties communicating
  between computers.
- Simulation and computer automation became a subject of negotiations during a
  press strike at that time.
- The Times and Simulmatics renegotiated their contract with the former
  insisting that the latter conduct trials before the 1964 election to prove
  that they would be useful.
- However, the Times terminated the contract in late 1963.
- An interesting aside is that the invention of email was the result of two
  Simulmatics employees were struggling to communicate with each other in real
  time as they did not share shifts.

### Chapter 9: The 480

- The assassination of President Kennedy obviously had a huge impact on the
  members of the Simulmatics Corporation.
- Around the same time, Eugene Bardach was planning to release his novel about
  the corporation, called The 480 after the 480 categories of voters.
- The novel became a success but he sold the rights to a TV company who changed
  the story somewhat and prevented it being made into a film for some time,
  which may in part explain history forgetting Simulmatics.
- President Johnson presented an opportunity for work: Simulmatics would try to
  determine who would be his best vice president to help him win the 1964
  election.
- Around the same time, the Vietnam war was beginning and the civil rights
  movement continued.
- ARPA approached Simulmatics to ask for help in the Vietnam war and by 1965,
  the corporation had an office in Saigon.
- Eugene Bardach died of a heart attack – he had a long-standing heart condition
  – aged just 46, unaware of Simulmatics' involvement in Vietnam.

## Part Three: Hearts and Minds

### Chapter 10: Armies of the Night

- Simulmatics had the best funding it ever had from ARPA.
- The Vietnam war was unpopular and so the Department of Defence was unpopular
  and so several other organisations reached out to Simulmatics for assistance
  rather than to the DOD.
- Robert McNamara wanted a way to assess whether the battle against communism
  was being won and this was one of Simulmatics' chief functions.
- They also created their first dynamic model, essentially a virtual Vietnam
  War, to try to predict the outcome.
- An interpreter working for the ceramics corporation in Vietnam noted the
  outfit to be amateurish and lacking in significant minds.
- Simulmatics' outfit in Saigon was seen with not much direction, including by
  its employees.
- When Greenfield visited, he was clearly not welcome nor comfortable himself to
  be there.
- This war in Vietnam was not what he had intended for his company.
- In 1967, it is alleged that the Department of Defence asked of their
  computers, based on the data, when would they win.
- The answer was: "You won in 1965."

### Chapter 11: The Things They Carried

- Pool continued to come up with experiments for Vietnam but failed to find the
  best researchers to carry them out – it was his claim that he told ARPA the
  best would be used but in truth mediocre or frankly unqualified people were
  sent to and used in Saigon.
- Anti-war protests increased.
- At the end of 1967, ARPA terminated its contract with Simulmatics and the
  Saigon office was shut down along with all but one of his projects, its
  paperwork being shredded.
- The opinion of Simulmatics in Saigon could not have been lower.

### Chapter 12: The Fire Next Time

- Around the time Simulmatics was flailing in Saigon, its New York offices were
  working on 'urban sciences'.
- Specifically, ARPA wanted to know whether they could predict race riots.
- Like their efforts in Vietnam, Simulmatics provided no useful predictions.
- In 1967, first Martin Luther King Jr then Robert F Kennedy were assassinated.
- In early 1968 after the unforeseen Tet Offensive, ARPA ceased all contracts
  with Simulmatics.
- In the financial fallout, all New York staff were laid off.
- Ed Greenfield's wife died after falling from a balcony.
- Lyndon Johnson elected not to run for a second term.
- A new simulation company offered its services to Richard Nixon.
- It appeared to originate with Pool, who appeared to have defected to the
  Republicans.
- Nixon was wary, and offered a small amount of funding only.

### Chapter 13: An Octoputer

- Pool wanted a national data centre: this collection of data would feed his
  algorithms.
- There was political and public opposition, especially as the idea was linked
  to communism.
- A prescient technologist said that the developing internet (or arpanet) would
  make a national data centre unnecessary.
- The NDC never came to pass.
- Noam Chomsky and Pool became public enemies with one of Chomsky's books
  decrying Simulmatics' operation in Saigon.
- Nixon became president.
- Greenfield failed to sell Simulmatics.

### Chapter 14: The Mood Corporation

- The closing chapter details where many of Simulmatics early executives,
  employees or contractors ended up.
- Though many died decades ago, many stayed in prominent positions.
- None less so that Ithiel de sola Pool, who maintained influence and standing.

### Epilogue

- The author uses the epilogue largely to impress her personal reflections and
  opinions on the events she has been recounting.
- She cautions strongly against forgetting the past: noting that many modern
  corporation see modern simulations based on vast amounts of data as being a
  new phenomenon, forgetting the ancient failure of Simulmatics and what can be
  learned from it.

[Home](/blog "ohthreefive")