+++
date = "2019-12-22T15:22:10.770Z"
title = "Summaries: Messy"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Messy

_Tim Harford_

## Introduction

- An anecdote of a jazz pianist forced to play and essentially unplayable piano,
  resulting in a strange but wonderful and hugely successful performance,
  introduces the idea that controlling and structuring everything may actually
  stifle innovation.

## Chapter 1: Creativity

- The influence of messiness on creativity is explored firstly through the
  Oblique Strategy cards used by Brian Eno.
- The author talks about how to combine randomness and talent to increase
  creativity.
- Using the example of the jazz pianist, the random event was putting him in
  front of a duff piano; his talent raised the level of the performance above
  even his usual high standards.
- Injecting some randomness into a situation has been shown experimentally
  multiple times to improve creativity.
- What is required is the ability to cope with the randomness and the ability to
  be allowed to cope with the randomness, which may include or cause failure.
- One of the ways Brian Eno himself explains how these work is that when you are
  not challenged, the brain moves onto autopilot; being bored.
- The fear or anxiety induced by these random events increases alertness.
- The author presents research that the most successful scientists seem to skip
  from one subject to another in their publications.
- This means that when a block is encountered in one field, the scientist can
  switch to another, potentially learning from the failures and breakthroughs in
  each field.

## Chapter 2: Collaboration

- This chapter is an exploration of not simply collaboration but what makes
  _good_ collaboration and why this occurs.
- Heavy reference is made to a mathematician Paul Erdo who was a voracious
  collaborator.
- The key seems to be that groups of like-minded people will enjoy collaborating
  but are less likely to achieve excellent results due to what could be called
  groupthink.
- In order not to upset the pleasing social harmony, people in comfortable
  groups suppress any of their own dissenting (possibly even correctly
  dissenting) views.
- Experimentally, only a single dissenting voice may be required to improve the
  output of the group.
- Theories behind this include members of the group being less confident in
  their own views being simply accepted by their friends and therefore having to
  work hard and put effort into justifying the views they are expressing when
  collaborating with an outsider.
- It is also pointed out that research has shown that these heterogenous groups
  are inherently less stable and tend not to last but again that is a necessary
  aspect or definition of them.
- Highly important in bringing groups together are bridging individuals, who may
  have something in common with someone in each group.
- The author summarises the message of the chapter into four points,
  paraphrased here:

1.  Recognise the tendency in yourself to spend time with people who look and
    think like yourself
2.  Place great value on people who connect together disparate teams
3.  Constantly remind yourself of the benefits of tension
4.  You have to believe that the ultimate goal of the collaboration is worth
    achieving and worth enduring the mess for

## Chapter 3: Environment

- The work environment heavily influences productivity.
- While clean and tidy environments can be seen as imposing or unappealing, what
  is most important is that occupants are given the opportunity – not just the
  feeling but the fact that they have autonomy – to change their environment to
  suit them.
- Therefore, an office space may have several tidy and several messy desks,
  perhaps some covered with sentimental items, perhaps some only with functional
  items.
- The key is the choice.

## Chapter 4: Improvisation

- The author begins by discussing brilliant 'preparers', for example Martin
  Luther King, to more careless improvisers, for example Ed Miliband forgetting
  to discuss the budget deficit in a pre-election speech.
- He then goes on to talk about brilliant improvisers such as Miles
  Davis, and discusses how marked activity in the pre-frontal cortex can be
  detected by functional MRI scanning when someone is improvising.
- One key to improvisation which is rarely appreciated is the ability to listen;
  listen to the reaction to what you are saying or doing and judge where to go
  based on that.
- The conclusion of the chapter is about how the usually meticulously prepared
  Martin Luther King delivered his 'I have a dream' speech as an improvisation,
  having seen the carefully scripted words provoking no reaction.
- Obviously, practice is very important for improvisation.

## Chapter 5: Winning

- The chapter focuses on people who have adopted apparently strange or
  suboptimal strategies but succeeded massively with them.
- Examples include the German general Rommel, Jeff Bezos, Donald Trump and
  chess legend Magnus Carlsen.
- The author says that it is these people's willingness to act quickly and
  unpredictably which leads to their success, but not because their actions are
  brilliant: for example, computerised chest software rates many of Carlsen's
  moves as average.
- The key is the destabilising effect these unpredictable and quick moves have
  on competitors.
- There is a definite similarity between the themes of improvisation in the
  previous chapter.
- More than this however, the strategies discussed here involves constant
  forward movement and unpredictability.
- A willingness to seek and seize opportunities and to forget or move on from
  failures is also very important.
- Jeff Bezos and Amazon are clearly held up as the best model and example of
  this though the ethics of their meteoric rise are not touched upon.
- Quick and unusual thinking we are also hallmarks of David Stirling, founder of
  the SAS, who is also discussed at length.
- Closing the chapter, the author points out that sometimes careful planning
  cannot be defeated by fast and unpredictable action.

## Chapter 6: Incentives

- The negative impact that incentives, or alternatively targets, can have on
  performance and outcomes is explored.
- The first avenue of exploration is the Apgar score – developed in the 1950s to
  assess the health of newborn babies.
- As soon as the score became available, improving the score became a target or
  a game for hospitals.
- Complex obstetric procedures such as forceps deliveries fell out of favour
  compared to more predictable procedures such as cesarian section.
- Apgar scores improved but cesarian section rates soared.
- The negative impact is of course the increased rates of abdominal surgery on
  mothers – many of which are unnecessary but more predictable than the
  alternatives.
- There is a discussion about two forms of incentive or target: the rule of
  thumb or the complex algorithm.
- The Apgar score as an example of the rule of thumb – simple and measurable
  without a calculator.
- Banking regulation is an example of a complex algorithm.
- Both of these can be gamed and unsurprisingly it is more simple to gain the
  rule of thumb method than the complex algorithm method.
- Regulatory bodies should be able to spot systems which are being gamed however
  the regulatory body may not be impartial; they can have their own agendas.
- In addition, bland and predictable assessment of these industries may do
  nothing to help.
- An excellent example is the Volkswagen CO2 emissions scandal.
- Software on Volkswagens was easily able to determine when the car was being
  tested for emissions and alter the engine's performance accordingly.
- All it took to discover this was an independent organisation measuring
  emissions on a simple journey from city to city.
- The predictability of the test meant that even though it was simple, it was
  useless.
- Even more worryingly, multiple long haul tracking companies had been found
  guilty of gaming the same test 20 years before.
- The test had not changed significantly.
- The author clearly prefers rules of thumb to complex algorithms and says that
  this can be applied to regulation but only if regulation adopts some
  unpredictability – he suggests echoing school and university exams.
- Students are forced to study and know well a broad range of topics within
  their subject of choice but will only ever be examined on a very narrow few.
- A broad range of regulatory standards being assessed randomly, i.e. some
  companies only being assessed on some standards, and more importantly at
  random times, is the messy way to provide incentives.

## Chapter 7: Automation

- The major critique of automation – algorithms and computer assistance – is
  that it allows the human not to try or not to engage with the task.
- This means that should the automation fail for some reason, they are less able
  and less likely to be able to quickly assess and deal with the situation.
- Introducing a little messiness or confusion into a situation keeps people
  alert.
- An example given is a government official who reduced road signage and made
  navigating a small village much more difficult with the result that accidents
  dropped and traffic flow actually increased.

## Chapter 8: Resilience

- The core message of the chapter is that uniformity breeds fragility and
  diversity breeds resilience.
- This has been shown to be true scientifically, socially and politically.
- Counteracting this is a personal, social and political comfort with the
  concept of neatness and tidiness.
- For example, short-term gains can often be had by increasing uniformity or
  tidying up small messes but the long-term harms, which can be catastrophic,
  are difficult to perceive and easy to ignore.

## Chapter 9: Life

- Several examples of messiness in life are given.
- Firstly is the organisation of papers on the desk.
- There is no evidence that strict filing systems offer any advantage.
- Simple and practical systems such as taking recently use papers and putting
  them at the top of the pile will automatically sort the papers from the most
  to the least used without any real time expenditure.
- The same can be said for organising an email inbox - the simple solution is
  using a search functionality compared to the electronic version of the filing
  cabinet, which is folders and tags.
- Strict calendar management is also a modern tidy habit but this reduces
  flexibility.
- A major topic is one of modern society's most widely used attempt to tidy an
  aspect of life – online dating.
- There is little evidence that the algorithmic dating is any more effective
  than simply matching people by their relative heights and where they live.
- The author's final example is that of children's playgrounds: modern,
  sanitised, structured playgrounds with soft rubberised flooring have not been
  demonstrated to be any safer than a playground which actively mimics a
  building site with real nails, hammers, saws and the ability to light fires.
- Children adapt to the increased risk and unpredictable environment by
  displaying greater care.
- The chapter concludes the book, mentioning one more time that life is messy
  and the best way to be successful is to except rather than fight against this.

[Home](/blog "ohthreefive")