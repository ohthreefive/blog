+++
         
date = "2020-09-15T19:01:43.659Z"
title = "Summaries: Natives"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Natives: Race & Class in the Ruins of Empire

_Akala_

## Chapter 1: Born in the 1980s

- Akala introduces his life and in particular childhood.
- He is a mixed race Briton whose grandfather was a so-called 'immigrant' on the
  Empire Windrush.
- He is a child of the 1980s, and documents how his childhood was harder then
  than he thinks it would be now.
- He lists numerous contradictions about racism in Britain, for example how
  local drug dealers refused to allow him work due to the opportunities his
  intelligence offered.
- There is clear anger in his writing about the injustices suffered by non-white
  Britons, but more than anything he seems to be emphasising the complexity of
  racism compared to its portrayal in the media and its perception among white
  people.

### Interlude: A Guide to Denial

- Akala methodically works through the various excuses and similar criticisms
  levelled at black, mixed race and other non-white groups for their apparent
  'complaints' about their lives.
- Again, he carefully points out that most of these criticisms are narrow-minded
  and simplistic compared to the complex world.
- Others are just pure nonsense.

## Chapter 2: The Day I Realised My Mum Was White

- The first racist abuse Akala remembers is as a five year old and when
  recounting it to his mother for the first time, realised she was white like
  his abuser.
- He discusses the way she pushed him to learn more about black history,
  including managing to get him to be one of the first mixed race pupils at a
  local pan-African school.
- As a teenager, he struggled with his opposition to black oppression and his
  white mother.
- He goes on to discuss the myths of race and nationality and their fluidity,
  for example his skin colour means he is assumed to be working class or poorer
  in the UK but middle class in Jamaica.

## Chapter 3: Special Needs

- Akala documents his early schooling and many of the difficulties he faced.
- At a tragically young age, he already found himself struggling to tell whether
  teachers or fellow pupils were simply being mean or whether their comments
  towards him were racist.
- He discusses how the education system in Britain seems structured to promote
  white Briton's achievements more than those of others.
- He also touches upon how both his race, and that of his friend who is a
  classical composer, continues to influence peoples' opinions of his
  achievements.
- He is often seen as _surprisingly_ overachieving and thinks similarly
  accomplished white people may just be seen as overachievers.

## Chapter 4: Linford's Lunchbox

- A discussion about the long-standing racism that exists when black sporting
  dominance, particularly of sprint events, is discussed.
- Nuanced arguments are made highlighting its scientific and evidential
  falsehoods.
- An interesting point is raised that there is no reason why the achievements of
  black sprinters should not be a motivation to white children, rather than
  something that will put them off trying to be elite sprinters.
- Another point is raised that it only ever seems to be a comparison between
  white and black: Asian and Indian lack of success in sprinting is rarely
  highlighted.
- A point is raised that the white nationalist oppression of black people has
  led the black nationalist movement to be one which is upheld as an
  inspirational movement for freedom.
- Examples are given such as Bob Marley and various black rap and hip-hop
  artists who become the idols of white youths, looking for inspiration.
- Akala comments on the while he can admire John Lennon, he would never uphold
  him as a symbol of freedom

## Chapter 5: Empire and Slavery

- The chapter begins with a discussion about slavery, specifically the
  lionisation of William Wilberforce as the "one person" who brought about the
  end of slavery.
- The concept that a trans-Continental business could be ended by a single
  person is of course ridiculous.
- In addition, heavily criticised is the idea that freedom was _given_ to the
  slaves, rather than freedom being theirs and having been taken from them.
- The second half of the chapter is dedicated to the survey finding that more
  than half of Britons are proud of the British Empire.
- Akala systematically dismantles all the arguments, such as "let the past be
  the past" and "get off your moral high ground", and sums up simply by saying
  that if Britons had been the subject of a brutal and murderous imperial power
  rather than the creators of it, they might think differently about forgiving
  its negative aspects and focusing on its achievements.
- Wearing poppies on Remembrance Day to commemorate those who gave their lives
  to stop the Nazi regime is so commonplace in the UK – should the victims of
  another imperial power not to be remembered in such a widespread manner by
  Britons?

## Chapter 6: Scotland and Jamaica

- Akala documents the profound impacts that two visits to family homes – Jamaica
  aged seven and Benbecula aged 10 – profoundly affected his young life.
- On a six week trip to Jamaica, he recognised inherent snobbery within himself,
  was able to quash it and developed an enormous pride in Black Caribbean
  culture.
- In Benbecula, he encountered a white community which did not seem to have the
  learned racism he had experienced elsewhere.
- He is keen to point out that this cannot be generalised to all of Scotland's
  regions and historical timeframes but took it as evidence that the racism he
  encountered was not something built into white people.
- The chapter closes with a brief discussion about the origins of racism and its
  difference from ethnicity.
- Racism is unequivocally modern and also claims to have scientific basis.
- The three most powerful racist empires in history – the slave trade, South
  African apartheid and Nazi Germany – have all been defeated.

## Chapter 7: Police, Peers and Teenage Years

- A heartbreaking and emotional chapter in which Akala explains how when he was
  first searched unlawfully by police aged 13, he really had been expecting it.
- Despite his intelligence and the opportunities afforded to him by his
  footballing talents, he still spent years of his adolescent angry and violent,
  and says it is no more than chance that he never spent time in jail.
- He explains how the class system in the UK, and the police which protect it,
  offer no aid, comfort or support to him or other black people.

## Chapter 8: Why Do White People Love Mandela? Why do Conservatives hate Castro?

- A discussion about the different opinions of the white west towards Nelson
  Mandela and Fidel Castro.
- Mandela's opposition to the apartheid regime, including choosing his own
  extended imprisonment over giving into the oppression, is of course highly
  commended.
- The debt Mandela and his African National Congress party owed to Castro
  is highlighted: Mandela's ongoing gratitude to Cuba extended to making it the
  first country he visited after his release.
- Akala comments that the West's acceptance of Mandela's victory over the racist
  apartheid regime is partly due to the fact that this was merely a political
  victory and that financial apartheid still persists in South Africa today,
  with the ANC being, of course, politically imperfect regime.
- Contrast is made with the demonisation of Fidel Castro and his Communist
  regime in Cuba, which rejected what it saw as oppression from America.
- Akala is keen to point out that there are many things for which Castro is
  rightly to blame for, but there are also many achievements which Cuba has made
  which put the west to shame.
- The most visible is health care with, for example, Cuba boasting 5000 more
  doctors then Canada.
- In addition, Cuba trains tens of thousands of foreign doctors for free with
  the only price being that they must provide healthcare to the poor when they
  return to their own countries.
- The political victory of the black population in South Africa was accepted
  because it was no hindrance to the white west still exploiting the natural
  riches of South Africa (in terms of minerals etc).
- Castro's opposition was entirely political and so there was no subterfuge by
  which it could be in anyway supported.

## Chapter 9: The Ku Klux Klan Stopped Crime by Killing Black People

- Akala discusses institutional racism within British education in the context
  of an horrendous anecdote from his secondary schooling.
- He had a history teacher with whom he immediately clashed politically, her
  being a fan of Margaret Thatcher and Winston Churchill.
- During a debate in the black history section of the curriculum, a section
  which the teacher had declared was her least favourite of the curriculum, he
  appeared to be winning, including rallying much of the class to his side of
  the argument.
- In apparent panic, she blurted out the racial epithet which gives the chapter
  its title.
- Akala felt some vindication at this: she was clearly racist to him and had now
  essentially openly stated that to the whole class.
- He also thinks he saw something akin to relief on her face that she was able
  to say what she had really wanted to say for sometime.
- He experienced one of the most deadly forms of racism – the casual
  indifference of supposed liberals at the time when action is required – when
  the headmaster did not discipline her at all and only reluctantly agreed to
  move him out of the class.
- With this as the backdrop, the exclusion gap, whereby black Caribbean children
  are up to 3 times as likely as white children to be expelled from school, and
  the attainment gap, whereby for every 3 white children put forward for top
  tier GCSEs, only two black children are, are presented.

## Chapter 10: Birds of a Feather – Britain and America

- Akala documents how black British culture as he was growing up was heavily
  influenced in a one-way direction by America.
- With the dawn of the Internet as well as other worldwide platforms, the
  transaction has become two way.
- Akala notices one negative impact: calls Black Americans seem to downplay
  British racism and Briton's seem to think American racism is far worse.
- Akala notices two contradictions.
- Firstly, all countries are different and so all countries will obviously have
  different racism; noticing a difference is meaningless.
- In addition, British colonisation is the foundation of American racism so in a
  sense they are inseparable.
- Factually, black Britons are more likely to be imprisoned than white Britons
  than black Americans are than white Americans.
- It just so happens that America incarcerates so many of its population, that
  significantly more black Americans are incarcerated.
- For balance, Akala also points out the fact that indigenous Australians are
  more likely to be incarcerated than white settlers at a rate greater than any
  other minority group.
- The rise in black voices throughout the world has led to some discord.
- Akala highlights Samuel L Jackson complaining about Daniel Kaluuya getting the
  starring role in Get Out when it should've gone to an American who better
  understands racism.
- There will always be differences in the racism experienced by black
  communities in the world but America cannot claim the high ground.
- The chapter goes on to discuss the inherent racism in the term black-on-black
  violence – for example, most of the major conflicts of the 20th century were
  white on white violence and yet are never labelled as such.

## Chapter 11: The Decline of Whiteness, the Decline of Race or the End of Capitalism

- The closing chapter deals with the recent (the book was published in 2018)
  rise of far right and white nationalist movements.
- Akala is as always keen to point out contradictions, namely that the white
  nationalists claiming to be oppressed are neither colonised, nor have been
  subject to genocide or even minor harms compared to those inflicted on black
  Africans and Caribbeans throughout the world.
- He remains pessimistic that the scales of justice will ever be balanced but
  takes heart from his feeling that regimes such as South African apartheid
  _should_ remain defeated forever and that multiculturalism and minority voices
  are on an inevitable increase.

[Home](/blog "ohthreefive")