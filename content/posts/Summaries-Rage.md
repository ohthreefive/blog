+++
date = "2020-10-18T09:30:48.394Z"
title = "Summaries: Rage"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Rage

_Bob Woodward_

## Epilogue

> "I bring rage out. I *do* bring rage out. I always have. I don't know if
> that's an asset or a liability but whatever it is: I do."

## Prologue

- Woodward begins his second book about Donald Trump by mentioning that in early
  February, national security advisors were warning him of the potential
  severity of Corvid 19.
- Other advisors were playing its threat down.
- Trump admitted, in an already famous quote (due to the media attention it
  received prior to the book being published) that he was downplaying the virus.
- Not included in the many media reports is the context that Trump said he was
  downplaying it so as not to cause panic.
- Woodward remembers that Trump was talking about the seriousness of the virus
  to him about one month before he ever did the same in public.
- The novel will return to the beginning of the presidency.

## Chapter 1

- James Mad Dog Mattis received a call from vice president elect Mike Pence
  telling him that Trump wants him for national secretary of defence.
- Mattis reminds Pence that as he has served in the military within the last
  seven years, he is ineligible for the post without a congressional waiver,
  which has only been granted once, in the wake of World War II.
- Considering it to be his duty, he meets with Trump and is keen to emphasise
  his belief that NATO is a good thing and that enhanced interrogation/torture
  is a bad thing.
- He is surprised to hear a very important question from Ivanka Trump – namely
  how to rewrite the strategy in Afghanistan.
- Again, thinking it is his duty as an American citizen, Mattis accepts the
  job.

## Chapter 2

- The focus is on Rex Tillerson and his acceptance of the secretary of state
  job.
- He impressed Trump at their first meeting, particularly with his knowledge of
  multiple world leaders, including Putin.
- Before accepting the job, he gave Trump some rules, including never
  disparaging him in public but instead coming to him if he had a problem.

## Chapter 3

- Rex Tillerson and James Mathis meet before their confirmation.
- Despite some differences, in particular Tillerson being keen to work with
  Russia while Mattis was untrusting, they agreed to try to work better than
  previous secretaries of defence and of state.
- Trump also wants to keep in close contact with Tillerson and Mattis when in
  office.
- Mattis is confirmed by all but one senator.
- For Tillerson, it is closer, at 57 to 43 or thereabouts.

## Chapter 4

- 16 years Senator Dan Coats is asked by Mike Pence to serve.
- Like Tillerson and Mathis, Coats does not want a role in government.
- He and Pence share a strong religious beliefs.
- After a long discussion, a discussion with his wife in which she says that she
  hopes the office of president will make Trump humble and a meeting with Trump
  during which Coats asserts that he will not hide any hard truths from Trump,
  Coats agrees to be director of national intelligence.
- In his early period in office, Coats finds Trump to be very difficult, in
  particular not listening to and inventing his own facts.
- He is concerned about the lack of sleep Trump appears to be having.
- His wife is amazed that the office does not seem to have humbled him in the
  slightest.

## Chapter 5

- Brad Byers is a former fighter jet pilot who was appointed to an advisory role
  by Jim Mattis.
- He was popular with Trump and found himself in a lot of private meetings.
- Mattis was keen to assert that he was _his_ guy.
- Byers became embroiled in the steel tariff debate in the White House.
- Trump declared that he wanted wanted tariffs to be imposed, regardless of the
  intelligence, just to see what happened.
- This infuriated Gary Cohn.
- Later, after changing who he wanted to negotiate steel tariffs, Trump declared
  in a meeting that his generals were pussies and more concerned with alliances
  than trade.
- Byers reported this back to Mattis, who asked him to document it in an email.
- He was furious at this relatively open betrayal of his generals.

## Chapter 6

- Trump had been warned that North Korea might be a major headache for him and
  his presidency.
- The Obama policy of ignoring North Korea, hoping its own government would fall
  apart, was presented as a failure.
- Mike Pompeo recruited former CIA career expert Andy Kim to form the North
  Korea Mission Centre within the CIA with the ability to perform covert actions
  to de-stabilise the Kim regime if President Trump ordered it.

## Chapter 7

- Rob Rosenstein was 12 days into his stint as deputy Attorney General when Jeff
  Sessions recused himself from the investigation into Russian meddling into the
  2016 presidential election.
- He found himself thrust into an obsession of Donald Trump's: getting rid of
  James Comey.
- Trump was furious that Comey privately admitted but refused to publicly
  disclose that the FBI investigation into Russian meddling was not directly
  investigating Trump himself.
- At a meeting in which he was discussing with senior members of staff how to go
  about firing Comey, he presented them with a four page letter he had dictated.
- Rosenstein found it to be rambling, incoherent and highly emotional.
- Rosenstein seized an opportunity to tell Trump he could easily fire Comey for
  the way he handled the Hillary Clinton email investigation.
- A delighted Trump then told Rosenstein to write a memo, send it to Sessions
  who would send on to him, and this is what he would use to fire Comey.
- He did add that he would like the Russian stuff to be included as well.
- Shortly after the news broke, the White House pinned the motivation for
  sacking Comey on Rosenstein.
- Rosenstein knew he could not lie and so declined to do a press conference
  where he would tell them that it was not his idea.
- Both the acting FBI leader director and James Comey himself had dictated many
  memos recounting conversations with Trump.
- They had the impression of a habitual liar with casual disregard for the law.

## Chapter 8

- Rosenstein was taken aback when Andy McCabe, deputy and now acting director
  general of the FBI, _does_ make Trump a subject of the Russia enquiry.
- Rosenstein did not even realise he had the authority to do that.
- In response, he is quick to appoint a special counsel to lead investigation –
  Robert Mueller – someone who will seem independent to the public and also who
  will report to Rosenstein as opposed to Trump or McCabe.
- Trump apparently went into a fury not seen before about this, commenting that
  it would be the end of his presidency and that it was Jeff Session's fault,
  Rosenstein's fault, Comey's fault, anybody's fault but his own.
- Rosenstein later meets with Mueller and McCabe to inform McCabe that he should
  have no part into this investigation, something which disappoints McCabe.

## Chapter 9

- Jarrod Kushner occupied a role as de facto chief of staff.
- Trump gave him significant foreign-policy powers, counteracting the influence
  that Tillerson and Mattis together sought to have over the president's foreign
  policy.
- On Trump's first foreign visit as president, to Israel, Netanyahu showed him a
  crude spliced together a video of Mahmoud Abbas apparently calling for the
  murder of children.
- In a panic, Kushner went to get Tillerson to help him defuse the situation.
- Calmly, Tillerson declared the video an obvious creation, saying they were
  trying to influence him.
- Trump disagreed, as they had Abbas on tape.
- Publicly, the two leaders talked cordially when they met shortly afterwards.
- Within the year, Trump had made severe cuts to aid spending towards Palestine.
- Jarrod Kushner denies Trump's response to the video as presented here.

## Chapter 10

- A few months into his tenure, Rob Coats was feeling the strain and informed
  his staff that he needed four things to survive.
- More sleep, regular good meals, regular exercise and a deputy with significant
  powers.
- Mattis gravitated towards Coats, finding him to be a decent man.
- The two became close and apparently shared an opinion of Trump that he could
  mistake his opinion for truth and he had no moral compass.
- At the same time, Coats became more distant from his old friend Mike Pence.
- He and his wife both noticed had become subservient and passive in the vice
  president role.
- At a dinner, he is alleged to have privately said to them both, "Just stay the
  course."

## Chapter 11

- A chapter dedicated to the escalation between North Korea and USA with regard
  to the former's intercontinental ballistic missile tests and the latter's
  response.
- Mattis experienced extreme existential crisis about this.
- He did not like Trump's potentially provocative public demeanour towards Kim
  Jong Un.
- He felt it was remarkable that a man could have no realisation that it was
  impossible to go out alone in this modern world.
- Trump later recalled to the author that it was his own personal meeting with
  the North Korean dictator that averted war.

## Chapter 12

- Trump has publicly called on Rex Tillerson not to bother negotiating with
  North Korea, which has had the effect of making any attempted negotiations
  Tillerson tries with North Korea pointless.
- Trump visits South Korea and has shown around by general Vincent Brooks.
- He is impressed by the wealth on display but says this means South Korea
  should pay for everything (they had paid 92% of a multi hundred billion dollar
  military facility but as the complex was both Korean and American, US law
  stated that America had to pay some proportion.)
- Brooks is keen to point out that the Korean success is because they have
  followed the American capitalist path.
- The relationship was more complex than a simple military contracts.
- In a later speech, Trump compares Korea's success to what he sees as American
  success under him and calls out North Korea for it backward ways.

## Chapter 13

- North and South Korea enter some sort of negotiation at the North's request.
- In addition, they also reach out to America.
- Trump agrees to a meeting with Kim Jong-Un.
- His critics jump on it, saying he has legitimised the regime.
- Trump is dismissive, saying it is only a meeting.
- Tillerson, who has been frozen out of diplomacy with North Korea by his own
  government, received a phone call from chief of staff John Kelly telling him
  that he will be fired on his return to Washington.
- While preparing to go to the White House, he gets a call from his own chief of
  staff telling him that Trump has fired him on Twitter.
- CIA director Mike Pompeo will be his successor.
- Tillerson is furious at the betrayal, particularly as he specifically asked
  Trump never to have any argument with him in public.
- There is some suspicion that Trump's dislike of Tillerson dates back to when
  Tillerson called him a fucking moron at a meeting of the Joint Chiefs of
  Staff.
- Tillerson and Mattis had a conversation saying how they had very much enjoyed
  working together and were frustrated that he hadn't managed their mission of
  controlling Trump's foreign policy.
- Pompeo goes to North Korea while still CIA director to meet about four pledges
  North Korea said they would make, the most important being denuclearisation.
- Over a long dinner, he realises he is getting nowhere and leaves.
- Despite this, shortly afterwards, North Korea releases three American
  prisoners, to which Trump claims responsibility and praises Kim for their
  treatment in captivity.

## Chapter 14

- Senator Lindsey Graham was one of Trump's main allies in the Senate.
- They publicly disagreed about Robert Mueller – Trump was unhappy about his
  appointment to investigate Russian collusion; Graham was satisfied.
- He told the president that if Mueller was the man he thought he was, he would
  confirm the president's response that there was no collision.
- The investigation was a major worry for Trump.

## Chapter 15

- After exchanging 27 letters, Trump and Kim meet.
- By the end of the meeting, North Korea has agreed to ongoing negotiations
  about denuclearisation and America has agreed to stop its training exercises
  in South Korea.
- Trump is impressed with himself, thinking he de-escalated the situation that
  Bush and Obama were afraid of.
- Mattis thinks the decision was a poor one, especially as the South Korea
  training exercises were essential for training troops about to go to
  Afghanistan or Iraq.

## Chapter 16

- This chapter details the deteriorating relationship between director of
  national intelligence Dan Coats and Trump.
- Russia was the main source of the dispute.
- Trump stood beside Putin and said that he disagreed with his own intelligence
  that Russia had been involved in meddling with the 2016 election because Putin
  had told him personally that there was none.
- Several important figures: Republicans, Democrats and members of Trump's own
  staff, publicly decried these comments, some going as far as calling them
  treasonous.
- In public on two occasions, Dan Coats made comments expressing his concerns
  about Trump and Russia, both of which understandably upset the president.
- He had asked Coats to stop the Mueller investigation but Coats said he did not
  have oversight over the criminal side of the FBI, just the intelligence side.
- Trump thought he was simply being subordinate.

## Chapter 17

- Mattis meets with his Chinese counterparts.
- He is keen to avoid any armed conflict with China and feels for the first time
  that the man opposite him is listening.
- He is not immune to some aggressive tactics – mentioning that if he had to go
  to war, he would choose to do it against a country whose army and generals had
  not seen armed conflict, i.e. China.

## Chapter 18

- Mattis believes he has found a good new chair of the Joint Chiefs of Staff,
  currently in a NATO role.
- Instead, and after some lobbying, Trump appointed general Milley, the general
  whom Mattis hoped would take over the NATO role.
- Mattis has respect for Milley but does not think the role suits him and thinks
  the appointment is too political.
- He realises that he is still failing to make a connection with Trump.

## Chapter 19

- Mattis attends a meeting with the 77 Allied nations fighting ISIS in Syria.
- He was delighted with the general agreement that the troop providing nations
  would not pull out until the rebuilding work had been done after the fighting
  was over.
- Within two weeks, Trump tweeted that United States had been victorious over
  ISIS in Syria and he was pulling the troops out.
- Mattis was again astounded that he had not been consulted and also noted that
  it would seem as if he had lied in the previous meeting.
- He drafted a resignation letter which she kept private until after a meeting
  with Trump where he would try to change his mind.
- The letter which stated openly that the differing views on allies was the
  reason for his resignation.
- Later, Trump would refer to Mattis as overrated as a general and commented
  that Obama had fired him and essentially he had done the same.

## Chapter 20

- Jarrod Kushner's close relationship with many foreign leaders was of some
  concern, to the extent that his security clearance was downgraded then
  revoked.
- Trump however wanted to be re-instated.
- From 2018 onwards, Kushner's focus was on re-election.
- He explained how Trump's decision-making worked and argued that it was always
  someone else's fault when he appeared indecisive or when he changed his mind a
  lot.
- Chief of staff John Kelly disagreed.

## Chapter 21

- Dan Coats makes a public statement of what he thinks are the security issues
  facing America.
- It is in direct contradiction to nearly all of Trump's public beliefs,
  increasing the tension between the two men.
- When a senior official publicly comments that he has seen the Russia
  investigation and sees no connection to Trump, Trump ask Coats if he will make
  the same assertion in public.
- Coats declines, stating that it is not his job to do so.
- In the following weeks, he hears numerous anonymous and named reports about
  senior White House officials who have lost confidence in him.
- He confronts the president, recognising this undermining tactic as having been
  used before.
- He decides to draft a resignation letter but on discussion with Trump, decides
  to stay until after the publication of the Mueller report.
- He realises he has just fallen prey to Trump's deal-making abilities and also
  realises he has an unshakeable but unprovable concern about Trump's
  relationship with Puntin.

## Chapter 22

- Mueller's conclusion that there is no direct evidence of collusion, with the
  caveat that the report did not exonerate Trump, was shortly followed by
  attorney general William Barr's four page summary of the document.
- The report was criticised for being somewhat incoherent and in particular, it
  had no aim to exonerate the president, just to determine whether or not there
  should be any prosecution.
- When the redacted report became publicly available, Barr came under criticism
  for having over-simplified it and perhaps even steered the political discourse
  in Trump's favour deliberately.
- Trump was delighted, declared the report an exoneration and marvelled that
  the whole affair seem to disappear.

## Chapter 23

- In the three months after declining his resignation, Trump's relationship with
  Coats improved little.
- In despair, Coats phoned Mattis, asking for advice.
- Mattis said that Trump's actions around Syria where the line for him.
- They discuss that something must be done about Trump but that anyone who had
  spoken out in the past had had little impact.
- Coats thought that the Senate was the key but noted that every Republican
  Senator he had spoken to was staying loyal to Trump, hoping to outlast him.

## Chapter 24

- Trump secretly called for some investigation into Joe Biden and his son's
  connections with Ukraine.
- He apparently also tried to leverage aid payment to Ukraine when discussing it
  with them.
- At Trump's Florida golf resort one day, he came across Dan Coats and,
  according to his wife, acted somewhat as if he was feeling guilty about
  something.
- While on the course later, Coats read Trump's tweet replacing him with an
  ardent Trump supporter.
- Later, his wife theorised that the discovery of a whistleblower lead to Coats'
  replacement – he would likely be heavy on Trump.
- In the end, the whistleblower's revelations about Trump asking for a Biden
  investigation as well as the aid money led to the Senate impeaching Trump.

## Chapter 25

- Details of the many letters sent between Trump and Kim after their first
  Singapore meeting are given.
- The letters are full of praise for each other and what they have achieved.
- The summit is set up in Hanoi but talks quickly breakdown.
- North Korea only wanted to dismantle one of its five nuclear weapons
  facilities and the United States were only going to rollback some sanctions.
- Trump presented the meeting as having been stopped at his suggestion.
- Realising Kim was not ready to make a deal, he claimed to have quickly told
  the dictator this and said that he would leave as his friend and he could come
  back to the table later.
- Correspondence continued, again full of praise, until Trump made a historic
  walk into North Korea to meet the leader.
- However, not long after this, the relationship soured somewhat as Kim sent a
  letter full of dismay at ongoing United States – South Korean military
  exercises, which he saw as a direct threat to him.
- Trump did not present the details but presented this letter as yet another
  great letter from North Korea when speaking to the press.
- The CIA reportedly were astonished at how well written all the letters to
  Trump were, full of praise and grandiosity, perfectly tailored to flatter him.

## Chapter 26

- Woodward details his first meeting with Donald Trump, three months before the
  coronavirus outbreak.
- The recent meeting with Kim was one of the main subject of discussion, with
  Trump again keen to point out how well he had done and how historic his
  walking into North Korea was.
- Woodward is keen to press him about what he thinks presidency means and his
  presidency is about.
- Trump is full of disparaging remarks about opponents such as Nancy Pelosi and
  Barack Obama.
- He thinks the current democratic nominees are beneath him and he is looking
  forward to the election.
- With regards to North Korea, he thinks he has better intelligence himself than
  that of the CIA.

## Chapter 27

- Woodward meets with Trump shortly after his impeachment.
- He appears un-phased by it.
- They initially talk about North Korea, about which Trump remains positive,
  talking up how he has made more progress than anyone else and that Kim is
  simply not ready to embrace denuclearisation.
- He talks about Japan and China and how he is trying to improve American trade
  deals with them.
- On the subject of Afghanistan, out of which Trump wants to pull his troops but
  has been advised against it by his generals, he says it is a difficult
  situation but he is listening to his generals.
- The meeting ends with Trump insisting Woodward takes a poster sized picture of
  him and Kim home with him.
- He says that Trump is the only leader or person for whom Kim smiles.
- North Korea regularly releases pictures of the dictator smiling.

## Chapter 28

- For context, Woodward notes that the conversation he is about to detail took
  place in the five days between Trump and Lindsey Graham discussing Trump
  wanting to attack Iranian general Sulejmani, something which Graham was keen
  he did not do, and the drone strike which killed him.
- Woodward presses Trump about the reason for his impeachment.
- Firstly, he wants to know if Trump thinks that releasing the transcript of his
  phone call to the Ukrainian president was a sword with which his enemies
  attacked him.
- Trump insists that it was the right thing to do as the phone call was a good
  call and that otherwise, he would have to defend against the opinion of a
  whistleblower.
- Woodward counters that the whistleblower's account, especially as the
  whistleblower openly admitted it was second hand, was not good evidence, but
  the transcript was.
- Trump could not be convinced that there was anything wrong with the
  transcript, the phone call or him releasing it and implied that he was scared
  what the whistleblower's account could have been abused without the
  transcript.
- Woodward strongly presses the president on whether he thinks that the
  President of the United States should ask a foreign leader to investigate one
  of his political opponents.
- Trump stands by his line that he did not ask for investigation of an opponent
  but investigation of corruption, something he thinks it is perfectly
  acceptable for him to do, especially given the amount of money in the United
  States gives to Ukraine.
- Woodward tries to persist and re-frame the question, for example asking Donald
  Trump what he would think if his successor asked for help to investigate him,
  but Trump stands by his line that he did nothing wrong and that he was asking
  for corruption to be investigated, something which she did think was the
  president's job.
- Woodward finally asks if Trump thinks that an apology, taking responsibility
  even if not admitting direct fault, would have made the situation go away.
- Woodward opined that if Nixon had apologised immediately after the Watergate
  burglars had been found, he probably would have survived the scandal.
- Trump says he has no problem with apologising but only if he is wrong.
- He believes that apologising is the same as admitting guilt and so he would
  not do it in this context.
- He believes that the media is strongly biased against him and even hate him
  and that he would jump on his apology and use it to attack him.

## Chapter 29

- Senator Lindsey Graham, reflecting on changes to the judiciary during Trump's
  first term, thinks it has become more partisan and ideological.
- He is concerned that if Trump wins a second term, it will get worse.

## Chapter 30

- The first official government report on Covid-19 in the United States was
  written on the 1st of January 2020.
- Two of America's most senior infectious diseases experts, doctors Fauci and
  Redfield, started to investigate at great pace.
- Redfield established contact with China but found their official response to
  be unhelpful – neither asking for the Americans to come over and help nor
  refusing them.
- By the middle of the month, and with first reports of a case outside of China,
  Redfield received word from his Chinese contact that the situation was much
  worse than the government were releasing officially.
- Redfield feared the worst public health crisis since Spanish flu.

## Chapter 31

- In an interview, Trump asks Woodward if he is going to make fun of Kim, saying
  this would risk nuclear war.
- Woodward insists he will only report the truth from the letters Kim sent to
  Trump; he would like to see Trump's letters back but is denied.
- Talking about China, Trump praises President Xi but says he has got an
  excellent deal and America's economy is in a better place than China's.
- Discussing Saudi Arabia, Trump states that the killing of the Washington Post
  journalist Khashoggi was dreadful but that he would have been crazy not to
  have sold all the weapons to Muhammed bin Salman.
- Discussing the Washington Post, Trump cannot comprehend that Jeff Bezos
  remains independent despite Woodward insisting that in his 49 years at the
  Post, the newsdesk was always independent of the owner.
- At this time, the impeachment was in the news but the coronavirus was starting
  to dominate.
- By the end of January, White House officials were convinced there was a cover-
  up of the scale of the epidemic in China and, after meeting with the
  president, convinced him to stop all travel from the country.
- American citizens would be re-patriated but would have to be quarantined for
  14 days.
- Despite reaching this decision on the advice of at least five senior advisors,
  Trump has always claimed the decision to shut down the country was his alone,
  unable to share credit for a good decision.

## Chapter 32

- Trump survives his impeachment after senators vote in a partisan manner, with
  the exception of Mitt Romney, who only voted against Trump in the matter that
  he had misused his power.
- Trump offers to speak to President Xi of China directly when health officials
  still struggle to make good contact with the Chinese.
- President Xi is evasive, saying that the WHO are helping China coordinate and
  that maybe America and the CDC should coordinate with them.
- When an American eventually does make it in, he reports back to Fauci that the
  Chinese response is good and looks to be working but that the measures they
  are employing would not be accepted by Western society, at least not with the
  low level of infections currently in the countries.
- The real alarm bells start to go off when community spread, as in spread to an
  American without travel or contact with a traveller from China is discovered
  in the 15th and then 16th patients in the United States.
- The final klaxon, one which has yet to stop sounding, goes off for Redfield
  when a New York infection is traced to travel in Italy.
- Coronavirus finally finds its way into the re-election campaign.
- Attorney general William Barr makes a public criticism of Trumps tweeting,
  saying it makes his job hard when Trump tweets that the proposed sentence for
  Roger Stone is too harsh.
- Woodward tries to press Trump on the dispute with the attorney general, but
  Trump deflects talking about how good social media has been to him and that
  social media is a good thing.
- Woodward is experiencing the same slipperiness that Trump employs when
  answering difficult questions, the slipperiness which lead Coats and other
  officials to get frustrated with him and end up leaving.
- America's first coronavirus death had no history of travel or contact with a
  traveller.

## Chapter 33

- Advice Woodward had received told him that in order to write a good biography,
  one had to speak to people who knew the subject best.
- That meant Jarrod Kushner; Woodward was surprised when he was so forthcoming.
- He cited for sources which would help one understand Trump.
- Woodward did not think they painted his father-in-law in a good light.
- Kushner is optimistic and defensive of the president.

## Chapter 34

- Kushner goes on to defend Trump's strategies.
- He notes that Trump's team is stronger than it was before – in 2016, 20%
  thought Trump was saving the world and 80% thought they were saving the world
  from Trump and now the reverse is true.
- Kushner seems to fail to grasp what he has just said about a fifth of Trump's
  staff.
- He talks about how his strategy with people involves making sure they are
  confident of their own position by constantly trying to wrong foot them.
- His media/Twitter strategy is the same.
- His message gets across while opponents spend time picking apart facts.

## Chapter 35

- Woodward speaks to campaign manager Parscale.
- He is a confident and ebullient man, 6 foot 8 in height and for appearance
  reasons seldom photographed beside Trump.
- He talks about how effective his campaign is and how effective the president
  is.
- Data, he says, is his big weapon.
- Asked early in 2020 what he thought might derail Trump's real action campaign,
  his answer was simple: the coronavirus and its impact on jobs.

## Chapter 36

- The chapter largely deals with the early March deterioration in the
  coronavirus situation.
- Stock markets reacted poorly and Trump had given a disappointing speech.
- Jarrod Kushner was pulled onto the team to help deal with coronavirus.
- Major public health officials were trying to coordinate.
- Towards the middle of the month, Trump agreed that a 15 day shutdown was worth
  trying.
- The most vocal opponent was Treasury Secretary Steve Mnuchin.
- Around this time, Kushner learnt that there were only 1.2 million swabs
  available for carrying out coronavirus tests – the scale of the problem was
  hitting home for him.

## Chapter 37

- Woodward has another interview with Trump in the middle of the coronavirus
  crisis escalation.
- He finds Trump to be obsessing over the media's opinion of his handling of it.
- He rates his own handling of it as good.
- In a conference call with Preston Xi of China, he asks Trump to stop his
  officials calling it the Chinese virus.
- He says that China has under control and that distancing, early diagnosis,
  isolation, testing and treatment are key.
- Realising that the easiest source of manufacturing protective equipment was in
  China, Kushner attempted to lean on his Chinese contacts saying that they
  should make every effort to provide equipment to the United States or it will
  look bad for them.
- He was also looking for production within the United States.
- Woodward pressed Trump to define his priority so that it could be recorded for
  history.
- Falling back on his often expressed sentiment, Trump initially said that there
  was a lot of fake news about the virus.
- Woodward tried once more to focus the president, he then said that his
  priority was always saving American lives.
- As the 15 day lockdown came to a close, advisors convinced Trump to extend by
  another 30 days.
- By this time, a death total between 100,000 and 200,000 was thought to
  represent some sort of victory.

## Chapter 38

- Woodward has an interview with Trump in which he wants to cover fourteen
  points about the coronavirus which he has researched.
- He finds Trump to be evasive, defensive and dismissive of his list and is
  frustrated as a journalist and worried as a citizen by the president's
  responses.
- Specific points include valuing hydroxychloroquine above a vaccine because
  hydroxychloroquine is available and tested now, even though it is not proven
  to work.
- He has also failed to press the Chinese to close wet markets because he is
  worried about jeopardising the trade deal he has negotiated with them.
- Despite being pressed numerous times, he refuses to say that he has committed
  the United States to Manhattan Project scale governmental mobilisation to
  fight the virus.
- Lindsey Graham called the president to tell him that in his opinion, the
  coronavirus was key to re-election.
- Keep the body count down and he would be near unbeatable.
- His own research around the subject was that testing was the current key –
  United States were currently not doing enough but could increase.
- Privately however he worried that the president's Achilles heel would be the
  economy.
- When people stop getting paycheques and looked to blame the president, he
  would be more likely to reopen the country and risk letting the virus out of
  control.

## Chapter 39

- Woodward meets with Trump and again wants to know his overarching coronavirus
  policy.
- He thinks coronavirus will define Trump's presidency and so he wants a clear
  description of his policy for the historical record.
- Trump is again difficult to pin down but thinks things are going well.
- By the middle of April, he is increasingly frustrating with the economic
  impact of lockdown and asks his medical advisors to come up with a plan for
  coming out.
- When he announces the plan, he uses grandiose language to describe the fight
  against coronavirus, including saying that it is the biggest federal
  government mobilisation since World War II.
- He says things have gone well.
- US death toll in April alone was 50,000.
- Around this time, relations with North Korea we are becoming increasingly
  strained.

## Chapter 40

- Privately, Trump talks disparagingly about testing and vaccination.
- However, he tasks Jarrod Kushner with sorting them out.
- The newly appointed vaccination's Tsar announces that America will have its
  vaccine by the end of 2020.
- He says this despite owning $10 million of stock in a vaccine company.
- At the same announcing by the Tsar, Trump talks up the vaccine but then and
  literally his next sentence says that the country is opening up regardless of
  vaccines.

## Chapter 41

- Trump and National Security Advisor Rob O'Brien both expressed the opinion
  that China may have deliberately let coronavirus out of its borders so that
  the economic effect it had on China was are not isolated to the growing
  superpower.
- Travel from Wuhan to Beijing was impossible at a time when travel from Beijing
  to London was easy.
- In their discussions, Trump says he expects the Woodward will write badly
  about him, like the way he did about Bush.
- Social media reports from China suggested apocalyptic scenes in some regions,
  though they were quickly suppressed.

## Chapter 42

- Woodward interviews Trump after the murder of George Floyd by a white
  policeman and the breaking up of the protest around the White House for
  Trump's photo opportunity at the church of the presidents.
- Trump immediately interrupts Woodward to tout his new law and order direction.
- He is massively critical of protests throughout the country which he says we
  are exclusively caused by Liberal Democrats.
- He says it is thanks to him that they were broken up.
- Woodward briefly digresses by asking about Trump's visit to the White House
  bunker – something Trump says was an inspection but Attorney General Barr says
  was not.
- Around this time, Jim Mattis publicly lambastes the president's use of
  America's troops against its own citizens.
- Trump turns back to the election, saying that he will have the economy back on
  track by September or October and will therefore win the election easily.

## Chapter 43

- Lindsey Graham thinks Trump's photo opportunity was an unequivocally bad move.
- This caused a bit of division between the two men but Trump quickly reopened
  communications to discuss campaign strategy.
- Graham thought that the Democrats had again provided Trump away back into the
  White House.
- He said that currently, there was no chance Trump would be re-elected but if
  he worked hard on the issues of police reform and social justice, he would
  easily win.
- All he would have to do would be to make some noises slightly against his base
  but in a way that would not completely lose them.
- Trump is highly sceptical, wanting to push law and order alone.

## Chapter 44

- By the middle of the year, Fauci was convinced that Pottinger had been right
  all along – the Chinese had covered something up at the start of the virus
  outbreak.
- Woodward pressed Trump on this issue, particularly asking if there was
  something nefarious about China's failure to keep coronavirus within their
  borders.
- Trump is noncommittal and draws attention to the trade deal he negotiated with
  China.
- Woodward again attempts to engage the president in a discussion about racial
  inequality and white privilege.
- Trump is mocking of Woodward when he talks of himself as a privileged white
  man.
- He repeatedly claims that he has been the best president for black people
  since Abraham Lincoln.
- He continually swings back to the economy, saying it was brilliant before
  coronavirus and it will rebound again and the black people will get their
  jobs.
- He refuses to direct the answer whether he has won the hearts and minds of
  minority communities.
- Throughout the interview, Trump refers to coronavirus as the 'plague'.

## Chapter 45

- Speaking on the phone again, with extreme effort, Woodward manages to get
  Trump to say that he does feel black people in America suffer inequality.
- Talking about the election, Trump refers to Clinton as a horrible person and
  crooked Hilary but says she was a tougher opponent than Biden.
- He implies that Biden has dementia.
- He ignores the polls, saying that they are against him just like the media is.
- He again says that he has done so well with the economy and that will carry
  him through November 3.

## Chapter 46

- In Woodward's last two interviews with the president, he feels like he still
  is ambivalent towards the importance of his office.
- He says he supports racial justice but doesn't really put much strength behind
  his words.
- He blames the virus for impacting his campaign for re-election rather than
  acknowledging the gravity of the situation America is still facing.
- Around this time, Kushner says that for five and a half years Trump been on
  the offensive but has had to go on the defensive for the previous three months
  due to coronavirus.
- He is about to re-enter the offensive.
- Trump says that he has been a victim of the media, blames the radical left and
  the Mueller investigation for much of his woes.
- Woodward poses him a question he says he has posed to many presidents: what
  has he learned about himself.
- Trump is notably simplistic about this, saying that he has learnt he can
  handle more than most people.

## Epilogue

- Woodward concludes the novel with the sentence 'Trump is the wrong man for the
  job.'
- He lays out a summary of his arguments but it is his initial statement which
  provides the most succinct summary.
- Trump once told Woodward that in the White House, there is a stick of dynamite
  behind every door.
- Woodward initially believed that this stick of dynamite was an external
  occurrence such as the coronavirus pandemic.
- In conclusion however, he thinks that Trump is the stick of dynamite.

[Home](/blog "ohthreefive")