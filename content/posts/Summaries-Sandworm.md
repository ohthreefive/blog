+++
         
date = "2020-09-14T19:47:10.755Z"
title = "Summaries: Sandworm"
description = "A summary to help me remember what I read better"
tags = [ "summaries", "Audible" ]

+++

# Sandworm

_Andy Greenberg_

## Introduction

- The novel will focus on Sandworm, a Russian group of hackers, and use
  investigation of them to discuss modern cyber warfare.
- The 2009 Stuxnet worm which attacked Iranian centrifuges was the first great
  example of the ability of cyber attacks to effect the physical world.
- NotPetya, unleashed by Sandworm in 2017, had worldwide effects but its
  epicentre was Ukraine, in which the first ever cyber-induced blackouts
  occurred.

## Prologue

- A Ukrainian cyber security expert called Oleksii Yasinsky responded to a
  December 2016 blackout at exactly 00:00 with paranoia – he felt sure of the
  same people he was investigating for various attacks over the previous 14
  months were behind this one.
- It was as if they had reached out from his computer to attack him.

## Part I: Emergence

> "Use the first moments in study. You may miss many an opportunity for quick
> victory in this way, but the moments of study are insurance for success. Take
> your time and be sure."

### 1: The Zero Day

- An American cyber security firm called iSIGHT received news from their
  Ukrainian branch that they may have discovered a zero day vulnerability in
  Microsoft PowerPoint.
- The expert hackers in that company's 'black room' spent a day testing,
  reporting and creating a proof of concept.
- Despite confirming that this was indeed a rare zero day vulnerability in a
  near ubiquitous piece of software, they had not yet grasped its significance.

### 2: BlackEnergy

- Analysing the software which was installed on a victim computer, an iSIGHT
  analyst recognise it as the hacking tool BlackEnergy.
- BlackEnergy was originally released in 2007 by a Ukrainian hacker targeting
  Russia.
- It started as a crude distributed denial of service (DDoS) attack but matured.
- It hadn't been seen for a while but this resurfaced version was much more
  sophisticated, including key stroke logging, the ability to take screenshots
  and send malicious emails from the target computer.
- An IP address was discovered to which this software reached out and the iSIGHT
  analyst was surprised to discover that when visiting this IP address, this
  command and control server was unprotected and appeared to contain what seemed
  to be a manual for how to use the malicious software.
- It was written in Russian.

### 3: Arrakis02

- Discovering the documentation on the command and control server was written in
  Russian was an unexpectedly useful clue but most of the puzzle was still
  uncovered.
- An iSIGHT analyst had to unpack the layers of decryption and, once achieved at
  great effort, had only binary machine code.
- Using software assistance, running the malware line by line, he was eventually
  able to decipher it, and discover a campaign document.
- The title of this campaign was a Arrakis02, immediately recognisable to any
  science-fiction fan as a planet from the novel Dune.
- Using tools and a Google-hosted malware repository of sorts called VirusTotal,
  he checked for malware with similar fingerprints and found multiple examples,
  all of which with campaign names referencing Dune, and dating back up to five
  years.
- The targets mostly seemed like standard cyber-espionage but there were some
  inconsistencies.
- The targets were also mostly Eastern European.
- iSight realised that for this threat (which they saw as very important) to be
  given appropriate media attention in the US, the group responsible needed a
  good name as the media might be less interested in malware that did not seem
  to be targeting Americans.
- As its discoverers, iSight were the ones to give them the name.
- The analyst wanted to use 'Bene Gesserit' from Dune.
- His boss, who had not read the novel, suggested 'Sandworm' instead.

### 4: Force Multiplier

- An analyst for the firm Trend Micro read iSIGHT's report with interest.
- He did his own search for Sandworm fingerprints and found a single match – an
  attack on a Swedish central infrastructure system.
- Due to his own background in infrastructure, he recognised the danger of this.
- This was not just cyber espionage: this group wanted to reach out from the
  digital and affect the physical world.
- The iSIGHT owner read Trend Micro's report with interest.
- It had filled in some blanks in their own investigation – namely why some
  infrastructure targets had been found instead of the usual political targets.
- Despite becoming an evangelist, it was difficult to generate much interest in
  Sandworm.
- Eventually, iSIGHT learned that the Department of Homeland Security's
  Cybersecurity and Infrastructure Security Agency had an infrastructure
  response team also investigating Sandworm.
- One other group who did appear to have noticed all the publicity iSIGHT was
  trying to generate was Sandworm itself.
- The original command and control servers were taken off-line and a newly
  detected server had no reference to Dune.
- It would be around a year until Sandworm resurfaced, and with a changed
  priorities.

### 5: StarLightMedia

- A year before the blackout of the prologue, Yasinske worked for Ukrainian
  media company StarLightMedia.
- He was telephoned to be told that two of the servers had gone off-line.
- Two servers going off-line was unusual, but when he heard they went off-line
  at almost the exact same minute, he became highly suspicious.
- When he then discovered these two servers were specific servers with access to
  other computers on the network, he was sure they have been the victim of a
  cyber attack.
- After careful analysis, he discovered that the attack had used another
  modified version of the BlackEnergy malware, now adapted to evade detection
  and more sophisticated than any tool he had seen in the past.
- It deployed another piece of malware called KillDisk which essentially wipes
  the infected computer by overwriting the Master Boot Record with zeros.
- StarLightMedia have been lucky, the initial attacked servers wiped themselves
  before infecting 90% of their planned targets.
- While deconstructing and studying the attack, various from government and non-
  government entities throughout the Ukraine also reporting similar attacks.

### 6: Holodomor to Chernobyl

- A very brief history of Ukraine and its troubles, particularly to do with
  Russia, is provided to contextualise the incoming cyber war.
- Holodomor refers to a period of starvation, particularly inflicted by Russia
  stealing grain from Ukraine, in which up to 13% of the Ukrainian population
  died.
- The Chernobyl nuclear disaster occurred not long before Ukrainian political
  independence.
- The communist party at the time was slow and limited in its evacuation of
  Ukraine and the harm from the disaster was and remains immeasurable.

### 7: Maidan to Donbas

- Ukrainian political independence was largely in name only as their rulers
  tended to be Russian-backed to Russian-controlled.
- Russia meddled in elections, including via a cyber attack group called
  CyberBerkut.
- The first so-called Orange Revolution was peaceful and managed to install a
  ruler independent of Russia.
- He turned out to be ineffectual and another Russian puppet took over again.
- Political revolution soon took hold again and after a pivotal moment, a march
  on the main square of Maidan took place.
- Snipers fired on the protesters, killing 103.
- Realising that this only strengthened the rebellion, the ruler fled to
  Russia.
- Not willing to lose political strength, this is when Vladimir Putin illegally
  invaded Crimea.
- He intended to paralyse Ukraine politically and economically and make sure no
  one could use its advantage.
- The author interviewed a former Ukrainian politician who asserted that he
  thought this was also Russia using an opportunity to essentially try out its
  cyber warfare tactics.
- In 2015, an iSIGHT executive got the chance to brief the Pentagon and
  predicted that Russia would try to turn out the lights in Ukraine.
- Although the group took him relatively seriously, the myriad other issues in
  the world meant that it soon moved on from this Russian threat.

### 8: Blackout

- We are introduced to Robert Lee, a former NSA analyst and expert on threats to
  industrial control systems.
- He was first contacted about the blackout in Ukraine and was quickly able to
  deduce that they were an act of cyber warfare and that they were likely the
  same group that had made a smaller scale attack on America months previously,
  namely Sandworm.

### 9: The Delegation

- Lee was forced to resign from the air force not long after writing a piece
  which condemned the way they wasted talent in his eyes.
- He tried to set up a private version of his NSA industrial control system
  group.
- In the wake of the blackout, he was determined that Sandworm and Russia should
  be publicly outed but received government advice not to.
- Instead, a delegation of both government and private analysts went to the
  Ukraine.
- They discovered attacks on three power stations which were similar but with
  some differences.
- In one, a remote computer was used to clone the control computer and send
  commands to circuit breakers.
- In another, this was done but in addition, spam calls were made to the
  company's help desk.
- In the third, instead of using a remote clone, the hackers exploited a
  weakness in the power company's IT helpdesk software to remote-control the
  real computers of the power station and manually switch off the circuit
  breakers.
- Lee thought it was as if the hackers where deliberately trying to show the
  breadth of their attack vectors, or perhaps trying to test them.

## Part II: Origins

> "Once, men turned their thinking over to machines in the hope that this would
> set them free but that only permitted other men with machines to enslave
> them."

### 10: Flashback: Aurora

- We are introduced to another cyber security expert and an experiment codenamed
  Aurora.
- The Idaho National Laboratory was testing an attack against a massive diesel
  generator they had acquired.
- It was still generally thought that a cyber attack on the network, causing a
  blackout, would simply require resetting the network and turning back on the
  power to resolve.
- The expert proved with malware of only about 30 lines that an attack on a
  safeguard mechanism could destroy the generator: real and physical harm in
  that is not easy to fix.
- After the successful experiment, he felt like he had just witnessed the start
  of something terrible.

### 11: Flashback: Moonlight Maze

- In the mid to late 1980s, hackers trying to steal information from the United
  States and sell it on to the KGB were traced to West Germany.
- This is be one of the first known examples of cyber espionage.
- Fast-forward more than 10 years and the US discovered a number of attacks on
  government information systems and give them the code name Moonlight Maze.
- By the end of the year, the Pentagon and set up the JTF NCST (Joint Task Force
  National Cyber Security Threat) to investigate.
- At this time, Russia was post Soviet and pre-Putin and so America reached out
  to them for assistance after the attacks were traced to a Russian Internet
  service provider.
- As something of a surprise, they were met with warm cooperation, particularly
  from a general who welcomed the delegation to Russia, allowed them to visit
  the ISP and openly admitted that it was part of the Kremlin intelligence
  bureau who performed these attacks.
- Almost immediately after this, that general was never heard of again,
  presumably having been unaware that this was the Kremlin's real new direction
  rather than Russo-American cooperation.
- Towards the end of that year, at a JTF NCST address, a stark picture of cyber
  warfare was painted.
- The most critical system under threat was thought to be the electrical grid as
  so much depended on it.
- Despite this, many still considered Moonlight Maze to be cyber espionage
  rather than cyber war and the threat of cyber war was still underestimated.
- The next year, President Clinton gave a White House lawn address in which he
  was ostensibly advertising for cyber security experts but, when looked back at
  from today, was actually painting a picture of what was to come.

> "Today our critical systems, from power structures to air traffic control are
> connected and run by computers. There has never been a time like this in which
> we have the power to create knowledge and the power to create havoc and both
> those powers rest in the same hands. We live in an age where one person
> sitting at one computer can come up with an idea, travel through cyberspace
> and take humanity to new heights, yet someone can sit at the same computer,
> hack into a computer system and potentially paralyse a company, a city or a
> government."
> President Clinton, January 2000

### 12: Flashback: Estonia

- In 2017, after the removal of a Russian statue, seen by the Russians as
  celebrating Russian expulsion of Nazis but seen by the Estonians as
  ushering in Russian control of their country, Estonian Internet sites
  were taken offline in a DDoS attack.
- In the first few days after it, Estonian efforts to stem the flow proved
  inadequate and it was becoming clear that the attackers were more
  sophisticated than originally suspected.
- All signs point to towards Russian involvement.
- The attacks disappeared then reappeared with more sophistication and more
  targets, including ATMs and credit card systems.
- The scale of the attack was seen as evidence by some for overarching
  organisation, presumably by the government and presumably Russia.
- The Kremlin openly denied involvement while Putin openly supported the
  retaliation for the removal of the statue.
- NATO to did little to chase or investigate Russia.
- The attack is seen by some as the first example of cyber warfare.

### 13: Flashback: Georgia

- One year later in August 2008, Russia occupied part of Georgia in an
  unbalanced retaliation.
- The attack was preceded by cyber attacks, mostly DDoS, and within them was the
  tool BlackEnergy.
- The attacks were linked to the group the Russian Business Network but no one
  was paying much attention to the cyber aspects compared to the physical
  aggression.
- One analyst who was involved was later an iSIGHT analyst.
- Years later, he would remember BlackEnergy's involvement in this attack.

### 14: Flashback: Stuxnet

- Stuxnet, named by Microsoft, was part of project Olympic Games, conceived by
  the Bush administration as a way of avoiding military conflict with Iran over
  its attempts to enrich weapons-grade plutonium.
- It was a joint venture with Israel.
- It was so important the Bush personally briefed Obama on it in 2009.
- The malware was incredibly advanced.
- A zero day vulnerability in Windows meant that the malware would get to work
  as soon as a USB stick containing it was plugged into a Windows PC; no user
  interaction required.
- Three more zero days were contained within, allowing it to spread and destroy.
- Four zero days in one piece of malware was all but unprecedented.
- It destroyed Iranian centrifuges and sought to avoid detection.
- It successfully destroyed the centrifuges but may not have slowed Iran's
  nuclear ambitions much.
- A Belarusian analyst discovered the worm in 2010 when assessing an Iranian
  laptop.
- By 2012, American newspapers were publishing that it was an American weapon.
- In the fallout, it was likened to the dropping of the atomic bomb.
- Zero day vulnerabilities had been horded to attack, not released to protect
  users.
- Physical infrastructure had been targeted and destroyed.
- Ironically, it was the dropping of the atomic bomb, and the race for nuclear
  weapons, which had started America down the path to this devastating cyberweapon
  - what had they started now?

## Part III: Evolution

### 15: Warnings

- A blackout had long been the nightmare scenario of cyber war, and its
  perpetration the equivalent of a red line being crossed.
- Despite this, and much to the private security analysts' dismay, the White
  House refused to categorically blame Russia and denounce the attack.
- The role of BlackEnergy in the attack was even officially downplayed, perhaps
  because that tool had been used in the US the year previously and the
  administration did not want to link this attack with a threat to America.
- Another reason to downplay the attack was that it happened to the Ukraine, not
  even a member of NATO, and not to America.
- The response was a stark contrast to the sanctions and retaliation against
  North Korea for their hacking of Sony.
- In the opinion of Robert Lee, Sandworm had crossed the red line, attacking
  civilians rather than Stuxnet attacking military, and US had just shown them
  that they could get away with it.

### 16: FancyBear

- Any further investigation or revelations about Sandworm and the Ukraine
  blackout were derailed when the Democratic National Convention underwent a
  cyber attack.
- Groups called CosyBear and FancyBear were soon identified as the perpetrators.
- Despite this, a lone hacker called Guccifer 2.0 publicly claimed response.
- The thin veil was soon lifted – Guccifer 2.0 was clearly a Russian invention.
- Months of attacks against the Democratic party ensued culminating in the
  election of Donald Trump.
- Retaliatory sanctions by Obama administration were strong.
- In contrast, Sandworm had still faced no retaliation.
- The implicit message was that they could carry on as they were.
- FancyBear appeared less sophisticated and more playful than Sandworm.

### 17: FSociety

- A few months into the year after the blackouts, analysts discovered malware
  bearing hallmarks of Sandworm.
- It seemed the group had resurfaced.
- In Ukraine, another wave of cyber attacks hit, this time claiming to be from a
  group called FSociety.
- Again, there was a thin veil over clear state-level backing.
- On December 17 2016, Ukraine experienced cyber war and blackouts for the
  second time in as many years.

### 18: Poligon

- Poligon is a Russian term and Yasinsky labelled Sandworm's efforts as such.
- It means something akin to a 'training ground'.
- He noted that at with all of their attacks, they had always stopped before
  reaching the maximum destructive potential.
- They also carried out their most controversial attacks in Ukraine, relatively
  safe from western interference.
- By this point, Yasinsky had left StarLightMedia and now worked for cyber
  defence in the Ukrainian government.
- After the second blackout, he was given the task of deciphering the attack.
- With his typical forensic detail, he was able to discover that Sandworm had
  probably never been fully kicked out after the first attack.
- This new blackout attack was more refined: like a ninja compared to the first
  attacks bareknuckle brawler.
- In addition, it targeted central infrastructure in the capital of Kiev rather
  than outlying nodes.
- The reemergence of this threat and the knowledge that similar tools had been
  used in America did bring the issue to the agenda of the American government,
  but the response was still to downplay the threat.

### 19: Industroyer/Crash Override

- A Slovakian team discovered much about the malware used for the second
  blackout.
- They shared a bit, but not all of it with Lee, whose interest was picqued.
- The malware was exceptionally sophisticated but, most forebodingly, highly
  modular.
- Parts of the code could be swapped in and out to allow it to attack different
  systems.
- This was clearly not a one-use virus.
- Frustrated at not being shown the whole of the code, Lee and his private
  company launched a sleepless 72 hour investigation into the malware and were
  able to produce a report hours earlier than the Slovakians.
- Both security firms published but the name chosen by Lee stuck.
- The act did not please the security community but Lee was not concerned.
- Instead he felt dismay: because of the Russian election tampering scandal, the
  Trump administration was suppressing this news of Russian hacking as the Obama
  administration had done a year previously.
- Lee regretted that it had become a partisan issue.
- In the rush to complete their investigation, Lee's company had missed
  something in the code.
- It was a small piece of code that attacked a protection relay.
- Attacking this could have the same physical – and importantly lasting – harm
  to the power system.
- This was exactly what Asante had demonstrated with Aurora in 2007 and had then
  predicted could be used as a weapon.

## Part IV: Apotheosis

> Out of the sand haze came an orderly mass of flashing shapes, great rising
> curves with crystal spokes that resolved into the gaping mouths of Sandworms.
> A massed wall of them, each with troops of fremen riding to the attack. They
> came in a hissing wedge, robes whipping in the wind as they cut through the
> melee on the plain.

### 20: Maersk

- The author found most Maersk employees would only speak to him anonymously.
- He recalls a conversation with a security worker, present on the day of the
  hack, who watched as computers around him went dark.

### 21: Shadow Brokers

- Ten months before Maersk felt this attack, a private security researcher
  found a blog post by a group calling themselves Shadow Brokers.
- They claimed to have hacked the NSAs Tailored Access Operations (that agency's
  cyber attackers).
- They promised a code sample and an auction for other code.
- Former NSA workers at the firm saw the code and thought the threat credible.
- There were some devastating tools, many of wehich exploited zero days.
- Despite the Shadow Brokers appearing amateurish, many thought they were
  plainly Russian operatives.
- The attack may have been to divert attention from the GOP or even election
  hacks.
- Alternatively it may have been Russia's way of saying, "If you point the
  finger at us, we'll show the world what you have been up to."
- When the same former NSA hacker blogged publicly about the Shadow Brokers,
  they responded by outing his past work in the NSA.
- He had never revealed and in fact taken steps to hide his past government
  work.
- Now, he lives in fear of foreign indictment for his past work.
- His life has changed forever.

### 22: EternalBlue

- The Shadow Brokers again returned in 2017, but this dump contained something
  special: EternalBlue.
- By exploiting a zero day vulnerability in every version of Windows pre-Windows
  8's Server Messaging Block, Windows machines could be taken over.
- Except it wasn't a zero day any more: almost exactly one month earlier,
  Microsoft had released a patch.
- The NSA had quietly tipped them off.
- But analysts knew that releasing the patch did not mean it was installed, and
  soon saw widespread evidence of EternalBlue having been used worldwide.
- When it was used for the WannaCry ransomware attack, an English security
  researcher called Marcus Hutchins quickly analysed the code, saw that it
  'called home' to a random URL, presumably command and control server, saw to
  his surprise that the URL was not registered and promptly bought it himself.
- He sat for four hours watching traffic coming in and so tracking the spread of
  WannaCry until someone alerted him to an unexpected side effect.
- Affected computers which reached out to the URL and found it stopped their
  attack pre-file encryption - Hutchins had found the kill switch.
- The presence of a kill switch, as well as sloppy practises such as a poor
  system for collecting money from victims and keeping a record of them led some
  to believe the malware had 'got out' before it was ready.
- Other sloppiness was some code re-used from the North Korean attack on Sony:
  North Korea were blamed.
- The Shadow Brokers got their data dump from an NSA operative who took some of
  their work home with them - in his words to study it more - and received 66
  months in prison as a result.
- There is no direct link between Sandworm and either WannaCry or the Shadow
  Brokers, but it is doubtless that Sandworm watched the attack with interest.

### 23: Mimikatz

- In 2011, a French hacker called Delphi discovered a flaw in Windows which
  allowed a certain username and password to be extracted from memory.
- He alerted Microsoft, who played down the flaw.
- About a month later therefore, he released a closed source proof of concept,
  not designed as a hacking tool per se.
- Over the following months, it was widely used by hackers.
- In 2012, Delphi was attending a Moscow security conference to speak about
  Mimikatz.
- Twice he felt like he was being targeted by the Moscow government for his
  creation.
- The second of these was a direct confrontation with a man who asked for his
  slides and the source code of Mimikatz to be copied to his USB stick.
- For his own safety, Delphi complied but before leaving Moscow, released the
  Mimikatz source code on Github to allow defenders to mitigate against it.
- In Windows 8.1, Microsoft turned off by default the target of Mimikatz.
- However, this was an incomplete mitigation as many systems are not up-to-date
  and the attack vector can be turned back on by a hacker with remote access and
  enough knowledge.
- Mimikatz has since become ubiquitous hacking tool, used by Sandworm in their
  attack on StarLightMedia and Ukrenergo power plant.
- However, they elevated it to a new level of danger when the combined it with
  the EternalBlue flaw.

### 24: NotPetya

- In 2017, Yasinski was called to attend to a Ukrainian bank which had been the
  victim of a crippling malware attack.
- On the scene, he quickly realised something more complex was at work.
- Within hours, the malware was detected in major firms across the Ukraine and
  then across borders.
- Similarities with the Petya worm led to it being called NotPetya but it was
  soon discovered that this new malware had no decryption mechanism.
- It used a combination of Mimikatz and EternalBlue to spread more contagiously
  than any worm in history.
- It cause havoc worldwide, including in Russia, the country from which it was
  assumed to originate.
- However, nowhere was as hard hit as the Ukraine.

### 25: National Disaster

- NotPetya ripped through Ukraine, paralysing credit card systems, healthcare
  and the behemoth that is the Ukrainian Postal Service.
- The delicate Chernobyl cleanup process was also hit but thankfully their
  computers which deal with the critical extraction of the nuclear material are
  air-gapped.
- Even when people tried to get on top of it by forewarning government
  departments, by the time the computers were disconnected, it was often too
  late.
- Ukrainian citizens found themselves unable to pay for basic amenities with a
  credit card and unable to find cash machines which either worked or had enough
  money left to give them sufficient cash.
- Elsewhere, a single Maersk employee asked for a piece of accounting software
  to be installed on his computer.
- This would be the only way in which NotPetya managed to gain a foothold in to
  the company.
- Of note, Ukrainian systems using IBM are Linux operating systems were
  unaffected.

### 26: Breakdown

- The Maersk infection crippled ports around the world as tens of thousands of
  lorries were stuck with cargo that could not be accounted for or put in the
  right place.
- A desperate scramble took place at Maersk's IT centre in Maidenhead.
- Backups between three and seven days old for all but one piece of critical
  infrastructure were found but one was missing.
- Unfortunately, this was the domain controller, a system so important that one
  Maersk employee said that if they could not recover it, they could not
  really recover anything.
- Eventually, a single copy was found worldwide.
- In a bizarre twist of fate, a power cut in Ghana had taken the domain
  controller offline and it had not been put back online before the malware
  attack.
- However, no Ghanaian employee had a British passport so one had to fly to
  Nigeria and hand the precious back up to a Nigerian employee who then flew to
  London.
- It would be two weeks before Maersk's online ordering system was back in any
  fit state.
- In that time, methods such as Gmail and WhatsApp were used to order massive
  shipping deals.
- Employees' work-issued laptops were taken from them, wiped regardless of what
  personal content might have been on them, and reissued with fresh installs of
  Windows.

### 27: The Cost

- Financially, the worst hit companies by NotPetya reported estimated losses of
  just under $900 million.
- However, the cost was not entirely financial.
- The dictation software company Nuance, whose main customer was healthcare at
  the time, was hit by the attack.
- Hospital transcriptions were lost or delayed.
- One hospital reported being logged into the Nuance server at the time of the
  attack and NotPetya was able to gain access to that hospital's computers
  directly.
- For example, the MRI scanners in that hospital did not use Windows and so were
  unaffected but the computers which extract the images from the scanners were
  and the images were not available for analysis.

### 28: Aftermath

- One researcher who had seen the flaw in the accounting software being
  exploited shortly after the WannaCry attacks was quick to spot it being used
  again by NotPetya.
- Using his prior knowledge, his forensic investigation showed the
  sophistication and longevity of the attackers.
- As well as the powerful takeover of the accounting software and using it as a
  foothold from which to spread NotPetya, there was separate malware designed to
  maintain a foothold if the other was somehow discovered and removed.
- The encryption mechanism was unravelled as something akin to an evolution of
  KillDisk and the researcher concluded that this attack had probably been
  developing since 2015 and was likely almost certainly Sandworm.
- Another security researcher found a strange kind of Killswitch in the code.
- If a certain file was present in the Windows main directory, NotPetya would
  not encrypt files but would still continue to spread.
- It was a soft cure though as it had to be present prior to the malware taking
  hold of the computer.
- Its discovery did give the researcher of the privilege of very early access to
  the original servers attacked by NotPetya.
- In the process of his research, he discovered that the attackers likely had
  access to the accounting software's servers for about 20 months before the
  attack.
- He marvelled at the elegance and patience of the attackers.

### 29: Distance

- When interviewing the creator and owner of the accounting software, she stated
  that the idea that her little bit of software could lead to global catastrophe
  seemed distant to her.
- The distance between the United States and Ukraine geographically is also
  portrayed as a reason for the United States' lack of preparation or lack of
  taking the threat seriously.
- The true motives of the NotPetya attack are not known.
- The hackers would have gained knowledge of all targets they would be able to
  infect and so could have gone after precise targets.
- Instead, they unleashed the worm where ever it wanted to go.
- The implicit message is that if you do business with Ukraine, you are a fair
  game target.
- Yasinsky noted one piece of information that many others passed over.
- The kill switch mechanism discovered in NotPetya was found on about 10% of
  computers in businesses which had not been attacked.
- Security workers for these firms reported that they had never installed the
  kill switch.
- Yasinski theorises that perhaps the hackers had deliberately spared some
  computers and business entities from the attack perhaps because they had
  installed other, as yet undiscovered, back doors in to these companies which
  would be able to persist after the eradication of NotPetya from infected
  computers.

## Part Five: Identity

> Treachery within treachery within treachery

### 30: GRU

- After years of research, the author was confounded by the fact that he had no
  idea of any individuals who were part of Sandworm.
- In the six months after NotPetya, the international community was near silent
  about naming Russia as the perpetrators of the attack.
- A Washington post article in January 2018 claimed a CIA source had confidently
  claimed that the administration thought it was a Russian attack.
- Despite this, there was no public naming of Russia and no rebuke or
  retaliation.
- Rumours were beginning to surface that Sandworm was part of the Russian
  military organisation GRU.
- Furthermore, CyberBerkut and Fancy Bear were also part of this institution.
- Though these claims were unsubstantiated, the author was shocked to think that
  the same group had been responsible for the three worst cyber attacks in human
  history in three consecutive years.

### 31: Defectors

- What little is known about the GRU and their methods mostly comes from
  defectors.
- A picture is painted of a ruthless organisation, more than happy to use widely
  destructive methods.
- The last defector from the GRU started working for MI6 in 1996.
- After a brief imprisonment, he was released as part of a spy swapping deal.
- He and his daughter were poisoned by Novichok in Salisbury in 2018 and
  narrowly escaped death.

### 32: Informatsionnoye Protivoborstvo (informational confrontation)

- The GRU was not always an elite cyber unit.
- It underwent a relatively recent metamorphosis, coinciding with the rise of
  Sandworm.
- A little read paper by a Russian described what he called asymmetric war and
  the current functions of the GRU seem to subscribe to that entirely.
- The author states that he found it very difficult to find out much about the
  hierarchy and ambitions of the GRU, but found it even more difficult to find
  out about any of their rank and file hackers.
- What he did learn was that they were treated as soldiers to follow orders,
  regardless of the consequences.
- He also learned that the GRU would recruit cyber criminals to prevent them
  going to prison and would lean heavily on anyone who they wanted to cooperate
  with them by threatening them with their destructive powers.

### 33: The Penalty

- It was not until the middle of 2018 that America (and GCHQ in Britain) finally
  openly named Russia as the perpetrators of NotPetya, closely followed by the
  other Five Eyes nations.
- The United States also placed heavy sanctions on many GRU officials.
- Around the same time, however, the Department of Homeland Security released
  information that hackers had gained access to some American critical
  infrastructure.
- While they caused no physical damage, it appears they had penetrated deep
  enough into the systems that they could have if they wanted to.
- The group employed none of the methods of Sandworm and were thought to be
  separate to it.
- There was clear evidence, however, that they were Russian.
- Sandworm's tactics appeared to be spreading.

### 34: Bad Rabbit, Olympic Destroyer

- Later in 2018, a new crypto-malware attack surfaced, named Bad Rabbit.
- While it did seriously affect some Ukrainian government systems, Russia was
  far worse hit.
- The malware shared as much as 2/3 of its code with NotPetya.
- Analysts linked it with Sandworm but struggled to reconcile the severity of
  impact on Russian civilians.
- One analyst, however, noted that the Russian targets seemed random but the
  Ukrainian ones specific: was the Russian hit a smoke screen?
- Later that year, during the open ceremony, the head of the South Korean
  Olympic Games was informed that they were under a major cyber attack.
- Thanks to careful planning and an overnight 12 hour session of restoring
  cripple systems, the games carried on without a hitch.
- The wilful attacks on Russian civilians and an apparently apolitical entity
  such as the Olympic Games showed the willingness of the GRU to attack whatever
  they wanted.

### 35: False Flags

- The Olympic malware was named, by Cisco, Olympic Destroyer.
- During forensic analysis of it, numerous false flags were found.
- Some code was seem to have been used by a North Korean hacking group called
  Lazarus.
- North Korea had, however, been on a charm offensive with South Korea and the
  countries' hockey teams were playing together in that Olympics as Korea.
- Other code was Chinese and yet other was yet other code was Russian.
- These numerous false flags were incredibly vexing until a researcher
  discovered a very rare bit of provably false finger-pointing.
- The meta data header of the malware had been forged to make it look as if it
  was North Korean.
- This meant that the only provable provably false false flag pointed the finger
  at North Korea and so presumably North Korea was innocent.
- While everyone assumed this was a Russian attack, the baffling array of false
  flags meant that all evidence - even what had been considered clear evidence -
  had to be questioned, making piecing together these attacks all the more
  difficult.

### 36: 74455

- One of the analysts who was in frequent correspondence with the author
  contacted him to tell him that he believed he had identified Sandworm as unit
  number 74455 within the GRU.
- One of his workers had meticulously unpacked various pieces of malware and
  performed DNS lookups and reverse DNS lookups and was quite certain he had
  found this elusive group.

### 37: The Tower

- The Robert Mueller investigation into Russian interference in the 2016
  election published details of the unit it believed represented Fancy Bear and
  also named unit 74455 as Sandworm.
- It also named three Sandworm members, with some addresses included, and a
  building outside Moscow which they called The Tower, believed to be Sandworm's
  headquarters.
- The author heads to Russia.

### 38: Russia

- The author attends a Russian security conference but most hackers talk about
  Russia as not the sort of country with the resources to do the things he is
  claiming.
- He has a sit down at Kaspersky labs and is walked through all the ways the
  Olympic attack could not have been China or North Korea.
- He stops short of naming Russia, despite the obviousness of the evidence.
- The author leaves Russia thinking he has learned little about Sandworm.

### 39: The Elephant and the Insurgent

- The analyst who linked Sandworm specifically to GRU unit 74455 was expressing
  an opinion not universally held.
- Other security experts wondered whether Sandworm and other cyber attackers
  were like any specialists – some groups did some bits, others did others.
- The analogy of a group of blind men feeling an elephant and each coming up
  with its own conclusion of what they must be feeling was applied to Sandworm.
- The author seemed to favour the concept that Sandworm is not a single,
  discrete entity.
- A report by the British cyber security agency categorically named the GRU as
  the perpetrators of numerous cyber attacks, citing but not divulging clear
  _physical_ evidence.
- The GRU might well be the overarching enemy but with multiple different parts
  performing different functions, i.e. different kinds of cyber attacks.
- One way to reconcile this is to think of the cyber attacks not as traditional
  warfare but as a form of insurgency.
- The confusion caused by these varying attacks (both in methodology, target and
  destructive potential) causes doubt in the victim about whether they are safe,
  who they are fighting and what they can do about it.
- It can also lead lead to disproportionate reactions.
- This cyber insurgency made sense for Russia, whose physical warmaking and
  leadership potential probably has no chance of unseating the west as the
  Centre for global power.

## Part VI: Lessons

> The concept of progress acts as a protective mechanism to shield us from the
> terrors of the future

### 40: Geneva

- The author speaks to senior security personnel in both the Obama and Trump
  administrations after they had left their positions.
- From both, he has to almost painfully extract admissions that they had some
  guilt or reservations about the lack of response to Russia's attacks on
  Ukrainian civilians.
- Both seemed to fall back on the defence that targeting critical infrastructure
  during war may be required.
- For this reason, they did not see it as sensible to severely punish Russia for
  doing so, in case the USA were to wish to do so in future.
- The author notes of course that Russia may have considered itself at war with
  Ukraine, but the wider world did not and even so, the targets of the blackouts
  had no military connection.
- He notes a speech by a Microsoft employee in Geneva about the idea of a
  digital Geneva convention – civilians should not be subject to the output of
  digital war.
- Again, he is told that such a digital Geneva convention is a long way off.

### 41: Black Start

- A DARPA experiment pits defenders of a false power plant against attackers.
- The defenders quickly find that the same sort of methods they would use to
  restart power after eg a hurricane induced blackout are useless.
- The end of the experiment is largely a victory for the attackers.
- Rob Lee had predicted that because of the USA's use of automated systems,
  while it might be harder to infiltrate their critical infrastructure than it
  was to infiltrate the Ukraine's, it might be harder for the USA to reverse a
  cyber blackout.
- He also noted that the Ukraine attacks did not actively destroy any machinery,
  something which was eminently possible.
- Sandworms attacks, which were visible to the world, have led to an escalation
  in nations investing in their own cyber security and cyber attack units.

### 42: Resilience

- Dan Gear is a cyber security specialist whose primary focus is not on
  preventing the next cyber attack, but on recovering from it.
- He argues that our world is now more interdependent than ever, and
  specifically on digital interconnections, that attacks on one aspect could
  have cascading effects.
- He argues for a need for both analogue backups and, of course, people who know
  how to use them.
- Ukrainian postal, power and pension services, for example, had either paper
  backups or physical switches to allow them to recover.
- The recovery was slow and hard but possibly easier than recovering multiple
  interconnected digital systems.
- He acknowledges that the motivation to maintain both knowledge and physical
  infrastructure of the analog world might be difficult to find, given that they
  are essentially a backup, but he is certain that another NotPetya will happen,
  and another, and another.

### Epilogue

- Ukraine's geography has meant it has been a country of near constant low-grade
  physical war.
- For similar reasons, it has found itself the subject of Russia's testing of
  cyber war – the sort of tests that would not have been tolerated by the west
  had been carried out against a European or other NATO country.
- On the Internet however, everyone is Ukraine.

### Appendix: Sandworm's Connection to French Election Hacking

- A security researcher investigating NotPetya phoned a command and control
  server both using the TOR network to obfuscate its geographical location and
  serving as a TOR node.
- While this behaviour is intended to conceal a location, it actually helps
  serve as a fingerprint.
- While the security expert could not locate the hackers, he could use the
  fingerprint to assess other malicious servers and managed to identify 20
  similar servers all set up around 2017.
- When he investigated these servers, he found that one of them was the fishing
  address contained within the fishing email used to access Emmanual Macron's
  personal email account.
- It therefore seemed clear that the same people who had created the command and
  control server for NotPetya had also created the command control server which
  hacked France's current president.

[Home](/blog "ohthreefive")