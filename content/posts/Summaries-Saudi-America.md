+++

date = "2018-11-03T20:04:39Z"
title = "Summaries: Saudi America"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# Saudi America: the Truth About Fracking and How It's Changing the World

**Bethany McLean**

## introduction

- The ban on exporting crude oil was lifted by President Obama in 2015, having
  stood for four decades.
- Between 2006, at which time 2/3 of America's oil consumption came from
  imports, and the lifting of the ban, America had become the world's largest
  producer of natural gas, due to fracking.
- America has since become the second highest producer of oil, after Saudi
  Arabia.
- By 2022, America may become a net petroleum exporter, in that the total
  exports will more than offset the total imports.
- The notion of energy independence is described as a difficult and unreliable
  term.
- The author offers the definition that North America (including Canada and
  Mexico) produce more oil than they consume as 'energy independence.'
- Oil is unpredictable and fracking does not change the fact that it remains a
  boom and bust industry.
- The fracking industry is currently heavily dependent on Wall Street –
  historically low interest rates since the financial crash in particular. It is
  said that economics, rather than geology, is responsible for the fracking
  revolution but noted that it has behaved in a more resilient manner than many
  would have expected.
- The introduction concludes by introducing us to a character named Aubrey
  McClendon, a pioneer of the fracking industry, held up at as both the good and
  the bad face of the industry.
- He died in a car accident just months after the ban was lifted.

## shale revolution part one:

## 01 America's most reckless billionaire

- The story of Aubrey McClendon and Chesapeake energy.
- It begins at the end: his death in a car accident had some features making
  people suspect suicide, especially as McClendon had been indicted the day prior.
- McClendon came from a rich, oil based family.
- McClendon is described as a fearless risk taker, and at one time his company
  produced the second highest amount of oil in America, second only to Exxon
  Mobile.
- McClendon was a cult figure, with some viewing him as a conman and others a
  visionary.
- His personal interest in oil was piqued by an article he read about two
  nobodies who drilled a well, struck oil and ended up earning $100 million.
- The concept of peak oil is raised, which follows that as the amount of oil is
  finite, production must eventually decline.
- The peak in US production (9.6 million barrels per day) was in the 1970s and
  had been steadily declining from then on.
- One year after Texas oil production began to slow, OPEC (formed by Iran, Iraq,
  Saudi Arabia and Venezuela) flexed its oil muscle by announcing an export
  embargo. This worsened the US decline.
- However, the combination of newly available oil from Alaska, Mexico and the
  North Sea, combined with a global recession, caused an unexpected fall in the
  price of oil in the early 1990s.
- Saudi Arabia responded in 1985, unleashing its full oil production potential
  and causing a crash on the oil price.
- McClendon bet against the falling prices, buying land for gas (not oil)
  drilling rights.
- Gas is noted to be expensive to transport compared to oil.
- In the US, the land owner, not the government, owns the minerals under their
  land; McClendon was therefore a so-called 'land man.'
- Over a decade, McClendon managed to keep Chesapeake energy afloat by securing
  funding, whereas his partner, university acquaintance Tom Ward, ran the business
  from the back end.
- A technological mastermind called Mitchell pioneered horizontal drilling and
  hydraulic fracturing, but there was scepticism, both from investors and oil
  companies, about whether this process would ever be financially viable.
- Chesapeake went public in 1993 despite not being highly regarded stock, but as
  part of the IPO McClendon and Ward retained 2.5% of profit from all wells.
- Gambling on a successful well, McClendon spent vastly in the same area but the
  gamble failed and nearly bankrupted the company. Remarkably, McClendon managed
  to borrow his way out of this crisis, despite contemplating selling, and
  eventually gas prices rose.
- Mitchell had sold his drilling company to a company called Devon, which then
  saw great success, including in an area which was, by chance, part owned by
  Chesapeake energy.
- The vast, aggressive spending made McClendon popular on Wall Street, due to
  the fees they gained, but unpopular with traditional oil companies.
- McClendon also benefitted from political fear about declining production of
  oil and gas in America, which caused an act to be passed in 2005 allowing
  **gas** drillers not to declare the chemicals used during the hydraulic
  fracturing process. This effectively allowed them to avoid regulation.
- McClendon's persistent bullishness about gas stability convinced the industry
  and markets that gas prices would be stable.
- However, this led to countries such as the United States and Russia
  betting on gas and therefore a production surplus.
- Such as surplus meant that prices could not be sustained.

## 02 the brain trust

- In 1999, Enron, based in Texas, span off a company named Enron oil and gas.
  The company quickly renamed itself EOG Resources.
- The company was ignored by the stock market until it started to use horizontal
  drilling techniques in the Barnett shale.
- When they were successful at producing gas, the stock began to rise.
- The company’s CEO describes them as first movers in the shale revolution.
- In some ways, EOG was the opposite of Chesapeake energy. Its offices were non-
  descript rather than lavish. The stock market valued them as a company which
  made technological advances.
- An EOG executive realised that as the company was producing vast quantities of
  gas but that there was no gas export market, they could not become a very
  profitable company unless they moved into oil.
- EOG turned their attention toward a large formation called the Bakken, known
  to contain oil but never previously exploited. EOG successfully targeted this
  region and, around the same time, started to pioneer fracking for oil, even
  though it was widely regarded as a technique which would not work for oil as it
  did for gas.
- EOG grew on the back of this technological advance.
- These companies and this industry was booming around the same time that the
  media was focusing on the new technological billionaires such as Jeff Bezos and
  Mark Zuckerberg.
- For example, a small and struggling traditional oil company sold in 2009 for
  $4.5 billion due to the fact that it was located above a huge shale reserve –
  almost a coincidence.
- Numerous examples of vast new and unbelievably large sums of money are given,
  and direct comparison is drawn to the millionaires and billionaires of the
  technology industry at the time – the author is suggesting that silicon valley
  had nothing on these oil men.

## 03 debt

- Due to low gas prices, Chesapeake energy's share price dropped significantly
  in the late 2000s.
- McClendon's borrowing had included shares in Chesapeake as collateral and, due
  to the falling price, the banks asked for their money, almost immediately
  reducing McClendon's wealth from $2 billion to -$500 million dollars.
- McClendon had to sell 94% of his Chesapeake shares and the company lost $30
  billion of shareholder money that year.
- Despite this, McClendon was given a vast bonus that year and was the highest
  paid CEO in corporate America.
- This is absolutely ridiculous!
- Chesapeake was also paying its board members very highly at this time and
  allowing them free use of the company's planes.
- A portfolio manager wrote a letter to the Chesapeake board describing their
  corporate governance as completely dysfunctional and their finances as
shameful.
- By this stage, all of Mcclendons enterprises were afloat on complex,
  interconnected debt. (The 2.5% profit pay out he had retained during the IPO
  came with a hitch, in that he must also shoulder the cost of the drilling
which
  would produce that profit.)
- Even McClendon's personal belongings were mortgaged to raise money for
  Chesapeake.
- McClendon had missed the potential of fracking for oil, particularly in the
  Bakken, and made an attempt to remedy this by buying other sites, but it did not
  pay off.
- For a decade from 2002, Chesapeake never reported positive cash flow.
- Chesapeake went outside of America to raise money – China, India and
  Australia. It did so predominantly by selling land, and was able to pass on the
  cost of drilling in some of these deals.
- This process made Chesapeake look like a better performing company that it
  was. Notably it was becoming a land trading company rather than a gas production
  company.
- Allegedly, dramatic claims were used to convince investors, for example that
  this was the only chance to get on board with the American shale revolution and
  that there would be no more major shale discoveries after the next couple of
  years so it was now or never.
- While this convinced investors, it was a short-term play and the next method
  of raising money would need to be found.
- Chesapeake debt it was alleged to be as high as $23.6 billion.
- In what seems like a typical move, McClendon argued that the still low gas
  prices were actually an opportunity, if America was to convert, for example,
  its transportation to gas.
- Essentially, McClendon was still banking on gas prices prices to rise. To this
  end, he struck a deal with a gas transportation company which was deliberately
  onerous on production – i.e. production would have to be consistent and large
  for it to be a good deal for Chesapeake.
- Among the deals, which raised over $1 billion, were parts considered morally
  and legally questionable and, in part based on this, the Chesapeake board
  removed McClendon's chairmanship in 2012.
- Removal of McClendon helped attract new investors and board members, but they
  quickly found out that little had changed. In January 2013, it was announced
  that McClendon would leave Chesapeake energy.
- The incoming CEO was astounded by the inner workings of the business – being
  much worse than he thought and with a remarkable amount of the business having
  nothing to do with oil and gas.
- McClendon seemed to have been planning for his departure and was later sued by
  Chesapeake energy. He is alleged to have taken corporate information and used it
  and Chesapeake contacts to make his next play.
- In a petty and aggressive move, McClendon leased office space directly opposite
  Chesapeake and advertised for employees openly and widely.
- His new umbrella company was called American Energy Partners and contained
  what is described as a smorgasbord of smaller companies.
- The companies were designed to specialise in one area only and, allegedly,
  were designed so that he could be quickly grown and sold.
- Remarkably, McClendon was again able to raise a vast sum – $15 billion.
- Coincidently, at this time, the price of gas was rising. Experts predicted
  that the price of oil would remain consistent at this time.
- The chapter concludes saying that these predictions turned out to be wrong.

## 04 sceptics

- Short sellers were unsurprisingly interested in Aubrey McClendon but perhaps
  more surprisingly were interested in the entire shale oil industry.
- The same short seller who correctly predicted trouble within Leaman Bros bank
  publicly Dec ried the entire shale oil industry.
- His analysis of the biggest 16 tracking firms showed that between 2006 and
  2014, they had spent $80 billion more than they had made from oil or gas
  production.
- A major issue is that fracked oil wells decline in production sharply – as
  much as 85% in three years – whereas traditional oil wells decline by about 10%
  in the time same time frame.
- Huge investment in new production in the current year is required to offset
  the decline in production from the previous year.
- Ultra low interest rate loans from the federal reserve were the only reason
  this sort of thing was viable.
- Another worrying sign was that companies such made deliberately misleading
  presentations, quoting 45 to 100% as internal rate of return. These rates are
  said to be spectacularly high.
- However, negative earnings were generated by these companies during the same
  time period.
- This was justified by the odd facts that financial results from an individual
  well do not include corporate expenses such as obtaining leases or land and they
  also exclude expenditure required to maintain production.
- In addition, with shale oil wells, there is no guarantee that two closely
  situated wells will both be productive of oil, even if one is, making it
  unpredictable.
- In addition again, company’s estimates of how much oil is available on their
  land are based on unreliable formulae.
- Evidence of companies reporting much higher predicted oil production to
  investors than to the SEC is presented.
- Shale gas production was much less heavily criticised by one investor as it is
  easier to extract from shale than is oil.
- The declines encountered in shale wells are also much less steep for gas than
  oil.
- Another sceptic drew attention to the fact that shale executives were paid
  well based on their ability to grow production rather than to generate profit.

## 05 bust

- In 2014, with oil prices dropping and Saudi Arabian marketshare falling, the
Saudi Arabian oil minister along with his OPEC colleagues faced a decision.
- They could cut production, propping up the price of oil and as a result, also
the American fracking industry.
- However, high oil prices theoretically speeds up the movement of a society
towards renewable energies.
- Additionally, fracking for oil costs about five times as much as traditional
drilling.
- So the Saudi minister thought it was that it was paradoxical to cut cheap
production with the effect of maintaining expensive production.
- Additionally, in the 1980s, when Saudi Arabia face a similar decision and
_did_ cut production, the response of the market was to buy from elsewhere and
Saudi Arabia's market share fell.
- OPEC reached out to other countries to try to agree a widespread cut and
protect their market share, but the risks would be high.
- In the end, Saudi Arabia elected to maintain its current production levels.
This was followed by an unpredicted huge drop in the price of oil (which had
been high for an unusually long time) and this of course impacted the American
oil fracking industry.
- The decision was regarded in the American press as a direct attack on American
oil.
- Within two years, multiple fracking companies were declaring bankruptcy.
- Up to 500,000 jobs were lost.

## 06 it changes the world, but it ends in tears

- McClendon continued to make deal after deal even during the bust.
- These were largely unsuccessful.
- He leveraged his personal assets – possibly multiple times over – in these
deals.
- Given his success as a dealmaker, it is possible he may have been able to
survive financially.
- However, in 2014, Michigan brought charges against Chesapeake energy about
ease price rigging with competitors.
- These were denied, of course, but a year later Chesapeake settled out of
court.
- As part of their settlement, they agreed a leniency clause. This was only
possible because they directly fingered McClendon for blame.
- Later in 2015, Chesapeake launched their own lawsuit against McClendon,
claiming he stole corporate information from them when he set up his new firm.
- The final straw for McClendon was also in 2015 when Oklahoma City indicted
him for a lease rigging.
- Overnight, McClendon prepared a statement denying all charges.
- The next day, he died in a car accident with some features of the accident
suggesting suicide but with an official cause of death as accident.

## shale revolution part two:

## 07 America first

- The story of the lifting of the ban on exporting crude oil in America.
- The ban is unlikely to have been lifted without the oil bust, ironically.
- America's wealth of natural gas, which may become a global game changer in the
  coming years, is no more than coincidence or luck.
- Fracking promised an era of American energy abundance; few had recognised it.
- Oil exports had not been considered by US companies for years, simply because
  there was not anything to export.
- As oil production increased, first the companies sought to get the ban lifted,
  and naturally turned to Washington seeking allies.
- 2013 saw the publication of a document arguing for allowing exports.
- Argued pros: the US had become, for the first time in over 60 years, a gross
  exporter of refined oil products (eg gas and diesel.) The argued that as these
  refined products were not banned, why not lift the ban on crude?
- Doing so would demonstrate America's commitment to free trade.
- Removing the ban would increase America's oil production, thus increasing
  their security in terms of oil.
- Argued cons: Exports may raise the price of domestic oil, but as the bust had
  put prices so low, this was a small concern.
- Lifting the ban would increase drilling, which can be environmentally
  damaging.
- It would also put more money in to the pockets of an already wealthy industry.
- Some refiners even argued to keep the ban: if crude remained cheap, their
  profits would be higher. The used the 'energy independence and security
  argument' for keeping oil domestic.
- At the same time, people were realising oil was not the only product: Obama's
  2012 state of the union speech talked highly about America's natural gas
  prospects and that his administration would develop this field.
- In 1981, Reagan's advisors warned that the trans-Siberian pipeline,
  transporting gas from Russia, could increase Russia's political power by making
  Baltic, Eastern and Central European countries dependent on Russia for energy.
  This was an accurate prediction.
- During the Obama administration, there was a push to increase liquified
  natural gas (LNG) facilities by the US and Europe combined, to get Europe out of
  Russia's energy/political net.
- Against this was the idea that it is hypocritical to accuse Russia of using
  energy politically if America was doing the same itself.
- The US approved the production of its first LNG plant in 2012.
- Exporting gas was a relatively small administrative task, in terms of
  permission, compared to exporting oil, which would require congressional
action.
- The first congresswoman to call for repeal did so in early 2014, starting a
  movement in support.
- The Obama administration distanced itself from the argument, saying it was a
  policy decision for the commerce department to make. Obama even threatened to
  veto a bill, arguing that further reliance on fossil fuels would result.
- In summer 2014, an oil product called condensates was allowed to be exported
  as it was re-defined as a (not banned) 'refined product.'
- To avoid appearing partisan, a lobbyist for lifting the ban approached former
  Obama administration officials to join his cause.
- A price fall in 2014 intensified pressure to lift the ban - free trade might
  create jobs, build the economy and reduce the trade deficit.
- Pro-repealists realised that a stand alone bill repealing the law was unlikely
- it would have to be tagged on to something bigger.
- It was tucked in to the year end $1 trillion dollar 2015 spending bill.
- Extensions to tax credits for soon-to-expire wind and solar power were granted
  in exchange.

## 08 Permania

- While on the campaign trail, Donald Trump trumpeted an aspiration of energy
  independence as one of is selling points.
- In office, he installed a cabinet heavy in so-called energy men.
- Trump continued to promise energy independence in office.
- He also talked of energy 'dominance', beyond independence.
- Oil was booming when he came in to office, nowhere more so than in Texas.
- The Midland basin occupies about 75,000 acres within a larger area known as
  the Permian basin in Texas.
- Assets in the Permian were being acquired by a company backed by Apollo
  finance in the early 2010s.
- Fracking survived for a few reasons:
- West Texas is the main oil 'story' worldwide, outside Saudi Arabia.
- It is always known to have had oil - for at least the last 100 years - and had
  been extensively drilled in the past, as recently as the 1980s.
- As few as 43 rigs were working there in 1999.
- In mid 2000s, those few companies still drilling tried horizontal drilling and
  fracking and hit upon an unexpected bonanza.
- Geologically, it has horizontal bands of mineral-containing rock. One lease
  can yield a lot of hydrocarbon and one (expensive) rig can access multiple
  layers.
- The Permian had infrastructure (pipelines etc.) already in place historically,
  greasing the wheels.
- The Permian may help the US outproduce Russia and Saudi Arabia in terms of
  crude oil by 2023.
- New technology also helped: 'shale 2.0.'
- Horizontal wells are approaching 4 miles.
- Drillers have altered their fractures to be more complex, smaller and more
  efficient, reducing the risks of wells communicating.
- Rigs can drill multiple wells from one spot.
- Rigs can move a few feet without having to be de-mobilised, saving money.
- Well extraction is improving, as is 'break-even cost' - how much it costs to
  get a barrel of oil out of the ground.
- Optimists believe the Permian is only in the beginning of its boom.
- However this is not certain.
- Fracking is still unsustainable as an industry: it is still built on debt and
  costly.
- So-called 'service costs' may have accounted for up to 50% of the break-even
  cost reduction. This is important as service costs rise and fall with the oil
  price & demand.  They were low due to the long bust at the start of Permania.
  They naturally are increasing.
- The availability of Wall Street capital at low interest rates is still
  propping up the shale industry.
- Investors are disappointed in shale companies as they don't make enough money
  directly from oil.
- Shale companies are disappointed in investors as they value growth highly and
  growth is difficult and expensive.
- There may be as little as 5 years of oil left, and some companies believe it
  should be drilled more slowly (ie less growth) to boost sustainability for the
  US.
- Only 5 fracking companies report more earnings than they spent in the first
  quarter of 2018, according to WSJ.
- Remember that the shale wells deplete: the Eagle Ford and Bakken well counts
  are falling and there may be a sizeable drop off in the Permian after 2020.
- There remain many unknowns.

## 09 Game of Thrones

- By 2017, American oil import was approaching an all-time low and export was
  high.
- Saudi Arabian oil was less resilient to this than al Nayeemi, long time Saudi
  oil minister, predicted.
- Saudi Arabia and other Arab countries use their minerals to prop up government
  patronage systems: funding public infrastructure and other such things also
  helped reduce any political unrest in these countries.
- The concept of the 'fiscal break even price' is the minimum price of a barrel
  of oil that a country requires for it to break even in a given year.
- With increased oil prices, public spending had significantly increased and
  therefore so had the fiscal break even price.
- The Arab Spring has also led the Saudis to pay a large sum of money for public
  spending to placate the populous.
- Falling oil prices therefore had a hugely deleterious effect on the Saudi
  economy.
- In late 2014, the Saudi Arabian king died. The new king's son was named
  Mohammad bin Salman, also known as MbS.
- He was given massive control over the Saudi economy, including oil.
- The long-term oil and finance ministers were replaced. It is noted that the
  new oil minister was a proponent of cutting production.
- MbS is the architect of a plan called Saudi Vision 2030, which aims to
  increase the number of Saudis in Private occupation, reduce the Saudi dependency
  on oil and sell a stake of Aramco to the public.
- The value of Aramco is of course, dependent on the value of oil and so this
  plan of MbS to move Saudi Arabia away from royal dependence is ironically,
  dependent on investor confidence in oil.
- With all the financial factors in the mix, OPEC and Saudi Arabia agreed to cut
  oil production in late 2016.
- Vladimir Putin is said to be the main negotiator between Iran and Saudi Arabia
  in 2016 prior to this decision.
- Increased oil prices would help his 2018 re-election campaign, favouring
  production cuts.
- Saudi Arabia and Russia dramatically cut oil production but allowed Iran to
  mildly increase its production.
- The resulting increase in oil prices will have played a large part in the
  recovery of the American shale oil industry, which has allowed America to become
  an oil superpower again, possibly the most powerful at this time.

## 10 a new era?

- Due to complex interdependences, it would be a gross oversimplification to
  state that American oil dominance is a universal benefit.
- For example, the African oil industry produces a crude which is similar to
  shale and therefore shale has been a negative development for them.
- The consequences of economic instability in regions such as these could be
  increased terrorism or even civil war.
- American dominance may reduce Russian power, and Russia has traditionally been
  seen as a country which aggressively uses its energy as a political weapon.
- Europe has invested on importing American gas, even though it may always prove
  more expensive than importing Russian gas: at least now there is a choice.
- This may force the Russian exporters to negotiate on things such as supply and
  price, rather than enjoying a monopoly in Europe.
- Despite the harm done to Russian oil by American shale, it has continued to
  aggressively make deals worldwide and has replaced China as the chief financial
  supporter of Venezuela.
- However, Venezuela's economy remains in a dire state and Russia may not
  receive repayment for its loans.
- China has become an enormous importer of American oil and has recently signed
  a long-term deal to import American gas.
- Again, this may reduce the amount of influence Russia is able to have over
  China.
- President Trump's planned tariffs on, for example, importing Chinese steel
  however may lead to retaliation in imaginative ways, again demonstrating how
  interconnected world politics and economics are and how ridiculous the notion
  of American energy independence is.
- The influence of low shale oil prices may be the greatest to the OPEC nations,
  none more than Saudi Arabia.
- American imports of OPEC oil have fallen to record low levels and there was
  disappointment in the Obama administration is lack of recompense for this.
- Despite the rhetoric of the Trump administration about oil dominance and
  energy independence, Trump has done little to distance himself from Saudi
  Arabia, including meeting with MbS early on in his first term.
- Trump has even made a large arms deal with the Saudis.
- Clearly there are more reasons than simply oil for maintaining a good
  relationship with the Middle East.
- Both China and Europe are dependent on middle eastern oil, without large
  supplies of their own like America.
- Much of America's economy is connected to that of Europe and China again
  showing that so much of politics and economics is connected and that the concept
  of energy independence is narrow-minded.
- Since rising to power as Prince in Saudi Arabia, MbS has ruthlessly cracked
  down on corruption at home, spend lavishly abroad and started plans for a new
  city running entirely on renewable energy.
- The plans to take Aramco public were put on hold due to disappointing oil
  prices and therefore share prices.

## 11 make America great again

- Thanks to the shale revolution, America has abundant natural gas.
- Manufacturing industries relying on energy production are therefore thriving
  due to the low prices.
- Somewhat counter to this, however, is the Trump administration's pledge to
  protect the coal industry.
- Attempts have been made to force adoption or continued use of coal but they
  have been rejected.
- Natural gas emits significantly less carbon dioxide than coal burning and this
  is reflected in America's
  decreasing CO2 rates.
- It does, however, partly due to leaks, emit methane.
- Some scientists believe methane is just as dangerous a greenhouse gas as
carbon dioxide.
- There are still plans by the trump administration to enforce coal use and to
  de-regulate production of natural gas.
- If natural gas production increases significantly, prices may go down.
- Low prices, paired with increasing interest rates, me actually be the death
  knell for shale gas.

## 12 losing the race

- The closing chapter reminds us that fossil fuels are by definition finite.
- How quickly and how completely the move to renewables will be made is
  uncertain and will vary across the world.
- An interesting point, especially given how the shake industry is supported by
  loans and capital, is that it will only take the markets to **believe** that the
  era of fossil fuel is is over for it to truly be over, as their support will
  disappear.
- The Trump administration, which we learned has backed coal, has made several
  moves to harm the renewable industry.
- These include a tariff on importing foreign built solar panels.
- Contrast to this is China's commitment to renewables.
- As well as China, a country with little mineral wealth, Saudi Arabia, under
  the auspices of MbS, are making a massive push to renewables.

## 13 epilogue

- The author states that her position coming into writing this book was as a
  sceptic of the shale revelation.
- She states that her main learning point is that making definitive predictions
  is a fool's errand.
- Her closing chapter introduces a three pronged prediction by a research agency
– rivalry, autonomy and vertigo.
- Rivalry is the base case in which gas will displace oil to an extent and
  renewable energy will compete with both fossil fuels and nuclear power.
- The second scenario, autonomy, is a quicker than expected adoption of
  renewables, possibly secondary to social or technological evolution.
- Vertigo is economic and political uncertainty and volatility about renewables
  and energy supply and therefore ongoing boom and bust cycles.
- The autonomy scenario is risky for America due to autonomy's emphasis on
  renewables (America is unprepared for such.)
- However, vertigo could be an even greater risk.
- The possibility of global uncertainty is an argument for conservation of
  current hydrocarbon resources rather than pushing production and goals such as
  energy independence.
- Hydrocarbons also have uses out with the energy industry, for example in
  agriculture, for which there are currently no substitutes.
- The author states that one way of thinking about the current situation is that
  America is the only country in the world who has moved to unconventional oil and
  gas but that is because America is the only country in the world who has
  exhausted its supply of conventional oil and gas.
- America may be the first mover in the shale revolution but, if required, other
  countries may also be able to exploit this resource.
- It is noted that America is oil and gas are very different – natural gas is
  ultra low-cost and abundant to the extent that conservative estimates believe
  there is enough to last at least a century.
- The ideal recovery price and the ultimate supply of it are much less
  predictable – one certainty is that other countries have more of it.
- The author circles round to a quote by Aubrey McClendon stating that he
  believed America should embrace natural gas to reduce its consumption of oil and
  embrace natural gas to reduce its reliance on coal.
- Currently, storage of renewable energy is so troublesome that any predicted
  energy grid largely reliant on renewable energy would require a backup of
  hydrocarbon, which in America's case could be from its natural gas abundance.
- In geopolitical terms, America's potential ability to export natural gas could
  be used to its advantage for some time to come as long as consumption is kept
  within a reasonable limit.

[Home](/blog "ohthreefive")
