+++
date = "2021-01-31T19:23:12.391Z"
title = "Summaries: Shuggie Bain"
description = "A summary to help me remember what I read better "
tags = [ "Audible", "summary" ]

+++

# Shuggie Bain

_Douglas Stuart_

**1992**

**The South Side**

## Chapter 1

- Shuggie is a near 16 year old living alone in a bedsit.
- He considers himself neither catholic nor protestant.
- We do not know if his parents are dead or just gone, but alcohol has played a
  part in whatever.
- He works in a supermarket and sometimes socialises with some of the middle-
  aged women who work there.
- He has an interest in hair dressing but struggles to find a way to pursue it.
- Another man in the bedsit may be a paedophile.

**1981**

**Sighthill**

## Chapter 2

- Agnes Bain lives with her husband, two teenage and one young child in her
  mother's flat.
- She is embarrassed by this living arrangement.
- Her first two children were by her first husband, a poor but faithful
  catholic.
- She left him for her new Protestant husband, Shug, father of her third and
  youngest child.
- He is a handsome man, losing his looks as he gets older and prone to
  infidelity.
- He has a history of physical and sexual abuse of Agnes.

## Chapter 3

- Big Shug commits infidelity regularly while on shift in his taxi.
- He sees the changes happening in Glasgow in the punters in his back seat.
- He has plans to rent a flat from a local business.
- The shift is interrupted by drunken phone calls from Agnes.

## Chapter 4

- Agnes recalls how happy Shuggie had been when she got him a doll to play with.
- She is playing with him in his bedroom when big Shug arrives home for his
  first break during his shift.
- Drunk and furious with him, she sets fire to the room; Shug is only just able
  to save his wife and son.

## Chapter 5

- Catherine, the eldest of Agnes's children, returns home from work to see the
  burning embers of her home's curtains on the ground outside.
- Instead of going home, she goes to find her younger brother who is hiding out
  in a favourite den of his.
- There is clear love between Shuggie's stepsiblings, but Alec ('Leek') is
  resentful of Catherine, who he thinks will marry and leave them.
- Big Shug has conspired to make Catherine go out with his nephew.

## Chapter 6

- It's the day after Agnes has set fire to her room.
- She is confronted by her father, who thinks he has spoiled her, trying to
  make up for his sisters' impoverished upbringing.
- Despite her being 39 years old, he belts her.
- Later, she argues publicly with her mother, whom she blames for not
  intervening when she was bring beaten.
- She tells her mother that she and Shug are leaving as soon as Shug can afford
  it.

## Chapter 7

- Leaving her parents' flat, Agnes recalls leaving her first husband.
- She was 26 and found leaving somewhat difficult and undignified.
- She complained to Shug that he was late but he replied that he had to leave a
  wife and four children behind.
- Brendan kept trying to win Agnes back for three years until he heard his
  daughter use the surname 'Bain', at which point his pride could take no more.
- As Shug, Agnes and the children head to the north-east of Glasgow, the
  attractive suburban gardens lift Agnes's spirits and expectations.
- However, they soon drive through the region, past a closed coal mine and out
  of work miners outside a miners' club and arrive in a spartan-looking scheme.
- Agnes can see the disappointment on Shug's shoulders.
- He has already told her that there is no pub in the nearby area and the only
  place to get alcohol is at the miners' club, where neither will be welcome as
  neither are miners.

**1982**

**Pithead**

## Chapter 8

- Shortly after unloading the car, big Shug announces to Agnes that he is
  leaving her because he cannot make her happy as she is always wanting more and
  more.
- A few days later, he returns to have sex with her and this pattern continues
  for a while.
- Catherine asks if they can return to Sighthill but Agnes understands that if
  she does this, Shug will no longer return to her in the evenings.
- He has exerted complete control over her.

## Chapter 9

- Agnes starts to become friends with the other women in the scheme.
- Despite her not telling them, they have recognised what Shug has done and have
  also recognised Agnes' alcoholism.
- They speak to her with some empathy: many of them have had alcohol problems in
  the past.
- One of them offers Agnes a Valium to help with her stress.

## Chapter 10

- Shuggie experiences life with an alcoholic mother – no one is there to help
  him get ready for school so he does not think he needs to go.
- He meets a boy who teases him for owning a doll.
- Catherine has been spending more time with Donald Junior.
- Agnes tries to get messages to Big Shug through women.
- After a year in Pithead, Leek decides to walk to his granny's house in
  Sighthill, desperate for a rest.

## Chapter 11

- Shuggie has taken to getting himself ready for school.
- Agnes faces a constant battle with alcohol withdrawal when her Monday and
  Tuesday benefits run out.
- In a flash of inspiration, she decides to walk to a far away pawnshop.
- In her mink coat, she is soaked before she gets there and stops in the garage.
- The garage owner is a recovering alcoholic and recognises it in her.
- He tries to give her some life advice.
- He makes a comment that implies he knows exactly who she is.

## Chapter 12

- Catherine secretly sneaks Shuggie to their uncle's house to meet Donald Junior
  and re-meet his father, whom Shuggie has largely forgotten.
- They also meet his new woman.
- She gifts Shuggie a pair of roller skates which he loves in spite of his
  feelings towards his estranged father and this new woman.
- Donald Junior has lost his job shipbuilding in Glasgow and has been offered
  one in Johannesburg.

## Chapter 13

- Leek is on top of a slag heap watching the Glasgow to Edinburgh train go by.
- He has a letter from Catherine from South Africa and another from Glasgow
  School of Art offering him a place, dated two years previously – roughly when
  the family moved to Pithead and then split.
- Leek wants to steal copper from the abandoned mine and has brought Shuggie
  along to be a watch man.
- He tries to give Shuggie some tips on how to fit in with the other boys at
  school.
- When the watch man appears, Shuggie runs from him instead.
- He comes across a slag crater and nearly gets stuck inside.
- At the last minute Leek rescues him at the last minute and Shuggie notices his
  face is heavily bruised.
- Leak berates his brother for failing to keep watch and says he had to hurt the
  watch man to escape.

## Chapter 14

- Concerned that he is too effeminate, Agnes approaches a neighbour's husband
  asking him to take her fishing.
- He demands sex in return.
- On the day of the fishing trip, he drives past Agnes and Shuggie on the
  street.
- Later Agnes resolved to tell his wife but before she does so, another woman
  comes and tells his wife that he has been cheating on her.
- Colleen collapses in despair, tearing her clothes, ripping her hair out and
  falling into a stupor.
- Agnes has to hide a smirk that finally there is someone at the same level she
  is.

## Chapter 15

- Willie Campbell is dying in hospital and is visited by his wife and daughter;
  the daughter is more in denial than her mother.
- His wife re-collects a story of when she when he came home from war in North
  Africa.
- In order to make ends meet and secure food for her family, she had begun an
  affair with a local greengrocer and had against her plans had a child by him.
- Shortly after his return, Willie went out with the baby in a pram and returned
  alone.

## Chapter 16

- Agnes is still upset about her mother's decision to tell her the story about
  her father when her mother dies.
- She stepped off the kerb and was hit by a bus; it is thought to be suicide but
  Agnes chooses not to believe it.
- Leek phones Catherine in South Africa to tell her of the bad news; she does
  not want to speak to her mother.

## Chapter 17

- Agnes awakens from a significant blackout finding that Shuggie has as usual
  made some things ready for her and taken himself to school.
- She vaguely recalls a sexual assault by a taxi driver the night before and is
  horrified and ashamed.
- A fellow alcoholic comes by to check on her but then starts drinking with her
  and invites a single, younger and wealthier local man over, luring him with
  Agnes' good looks and her notoriety as being new in the scheme.
- Things seem to be going well until Agnes vomits on the man.

## Chapter 18

- Shuggie appears to find an ally in the playground in the form of an older
  girl.
- However, he quickly takes offence when she asks him questions about his
  homosexuality.
- Shuggie recalls the pain of numerous men visiting to use alcohol to get sex
  from his mother and women visiting to drink with her.
- Periods of sobriety are rare.
- Agnes visits a far away AA meeting to try to stay away from familiar faces.
- However, a woman there still recognises her.

## Chapter 19

- After a few months of sobriety, Agnes manages to find work on the night shift
  in a petrol station.
- She becomes popular with night shift taxi drivers.
- Her sons at home are thrilled with her sobriety.
- A new, young taxi driver catches her eye.
- The two go on a date and she discovers he is her neighbours older brother.
- She manages not to implode at this knowledge, though is tempted by the drink.

## Chapter 20

- Agnes continues to stay off the drink and progress with Eugene.
- Shuggie is delighted to have his sober mother back but is somewhat jealous of
  the attention she gives Eugene.
- For one of the first times, he benefits from one of her strengths when he is
  seen dancing wildly by the McAvennie's.
- His mother tells him that he must continue and not give them the satisfaction
  and he realises that this is why she always presents herself so well no matter
  what she has done while in the drink the night before.

## Chapter 21

- Shuggie experiences homophobic bullying from teachers at school and also gets
  in a fight.
- Leek puts on a party to celebrate Agnes having spent one year sober and at
  Alcoholics Anonymous.
- Shuggie meets Eugene for the first time at the party.
- Eugene gets a little bit self-conscious as the only non-alcoholic at the party
  and leaves, hurting Agnes's feelings.
- This resurfaces her negative emotions and she takes two Valium before she
  feels good enough to go back to the party.

## Chapter 22

- Having not heard from her for a while and having his pride hurt, Shug pays
  Agnes a visit, even bringing her cans of lager, but she manages to resist.
- That said, the author leaves a hint that she may have had sex with Shug even
  though she didn't take the alcohol.
- Eventually, Eugene gets in touch again.
- He finds it hard to accept that Agnes is the same as the other, in his own
  words, pitiful alcoholics that he met at her party.
- He takes her for a meal and insists that with him in her life, she will not
  feel the need to drink to excess.
- He convinces her to have a drink and she arrives home later, pitifully drunk,
  to a furious Leek and distraught Shuggie.

## Chapter 23

- On Hogmanay, Shuggie recalls the four months since Eugene tipped Agnes back
  into alcoholism.
- He returns home to find front door open and his mother gone and manages to
  deduce that she might be at a party in Germiston.
- He acquires the money for a taxi there but is sexually assaulted by the driver
  on the way.
- He finds his mother there: unconscious asleep under a pile of jackets at the
  party, her clothes torn.

## Chapter 24

- Shuggie notices a deterioration in his mother's mental state in the months
  after the Hogmanay incident.
- One day, he arrives home to find Leek on top of her, blood coming from her
  wrists and a razor blade on the floor but he is too young and too overwhelmed
  to understand what is going on.
- She wakes in a psychiatric ward with Eugene and Leek with her – her first and
  only question as to the whereabouts of Shuggie.
- Leek tells her, somewhat reticently, that he is with his father – Agnes must
  have called him when she tried to take her own life.

## Chapter 25

- After a few weeks of Shuggie living with Shug and Joanie, Agnes finally snaps
  and calls to retrieve her boy.
- She smashes a window at the place and demands that Shuggie is returned to her.
- The boy, ever loyal to his mother, jumps through the broken window and into
  her arms.
- Shug warns that his mother will never get better but Shuggie only shrugs.

## Chapter 26

- After the suicide attempt, Eugene came to keep an eye on Agnes during the day
  for some time but eventually left, saying he did not like her when she had a
  drink in her.
- After an argument, Leek finally leaves too, giving Shuggie advice on his way
  out that he has to look out for himself and leave when the time is right.
- He assures Shuggie that their mother is not going to get better.

## Chapter 27

- Agnes continues to make plans to leave Pithead and move into the city with
  Shuggie.
- He wants to leave but is more concerned that his mother will just remain an
  alcoholic in the city.
- In a show for him, she chooses to empty the last two cans of lager down the
  drain rather than into herself.

**1989**

**The East End**

## Chapter 28

- Shuggie is in the city dealing with a mother who has returned to drink.
- He is now the only man in her life on whom she can offload her poor me's.
- On his first day at his new school, Shuggie encounters homophobic abuse.
- He realises already that there is no such thing as a fresh start.

## Chapter 29

- A neighbour unexpectedly comes by and drags Shuggie out.
- He wants Shuggie to keep his girlfriend's friend company.
- Shuggie initially thinks this is his opportunity to be a normal boy but after
  a little time with the girl, he realises the main thing they have in common
  is their alcoholic mothers.
- He lets his guard down with her and realises that just as Agnes has lied that
  she will get sober, he has lied to her that he will try to be a normal boy.

## Chapter 30

- Agnes and Shuggie have become more resentful towards one another.
- She kicks him out of the house and he goes to his brother's.
- She sends messages to the house implying that she's going to hurt herself and
  not tell anyone about it.

## Chapter 31

- For some time, Shuggie has been Agnes's carer in all but name.
- One day, he watches her quietly choke on her own secretions and decide not to
  intervene.

**1992**

**The South Side**

## Chapter 32

- Catherine does not return home for Agnes' funeral; Leek attends but keeps his
  distance.
- There is clear love between the brothers but no closeness.
- Shuggie has a friend in Lianne.
- The two seek out Lianne's homeless alcoholic mother and give her some basic
  care.
- Shuggie has found a kindred spirit.

[Home](/blog "ohthreefive")