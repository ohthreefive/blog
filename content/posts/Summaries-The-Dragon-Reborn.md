+++
         
date = "2020-03-02T21:08:35.328Z"
title = "Summaries: The Dragon Reborn"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Dragon Reborn

_Robert Jordan_

## Part One

> *"And his paths shall be many, and who shall know his name, for he shall be*
> *born among us many times, in many guises, as he has been and ever will be,*
> *time without end. His coming shall be like the sharp edge of the plow,*
> *turning our lives in furrows from out of the places where we lie in our*
> *silence. The breaker of bonds; the forger of chains. The maker of futures;*
> *the unshaper of destiny."*
> From Commentaries on the Prophecies of the Dragon, by Jurith Dorine

### Prologue: Fortress of the Light

- Within the fortress of the light, Jaret Byar retells his story for a senior
  commander within the Children of the Light, Pedron Niall.
- He believes that Perrin was the one who betrayed them.
- That commander then has a meeting with an unknown-to-him darkfriend in which
  he says that he will use this boy who has declared himself the Dragon Reborn
  to get more power for the Children of the Light.
- He orders the darkfriend to do what he can to keep Rand alive until he is
  ready to deal with him in full view and in this manner gain the allegiance of
  the various factions in Almoth Plain.
- Chaos has descended there since the battle.
- To actively seek to keep the Dragon Reborn or a False Dragon alive is
  blasphemous in the Children of the Light's eyes.
- When the darkfriend leaves, a strange man who knows much and has become one of
  Niall's counsellor's, called Ordeith, emerges from a secret passage and tells
  Niall about Rand, Perrin and Mat.
- The darkfriend returns to his room where he is confronted by a Myrddraal who
  says he has not done what he has sworn to do.
- The Myrddraal tells him to find and kill Rand and for every month he fails,
  a member of his family will be killed.
- The darkfriend is unsure whose orders to follow.

### Chapter 1: Waiting

- Perrin and a group of Shienaran are keeping watch.
- Perrin is frustrated about how long we have been doing this for.
- A woman approaches, apparently not for the first time, and Perrin goes to meet
  her.
- She is a tinker.
- He asks how she knew to find them there but as usual get no clear reply.
- He and the Shienaran return to Moiraine.
- When they arrive, Perrin sees the Dragon banner and Min.
- "Welcome to the camp of the Dragon Reborn," he tells the women, whose name is
  Leya.

### Chapter 2: _Saidin_

- Perrin, Min and Loial talk about their predicament.
- Min has had a seeing that Leya will probably die a violent death.
- Perrin has some concerns that this might mean the camp is in danger.
- Perrin and Rand talk, with Rand revealing he has been arguing with Moiraine
  about the chaos in Almoth Plain.
- Rand knows that by going there he could help bring some peace but only by
  defeating - killing - one of the warring sides.
- Moiraine advises that he stays put, essentially in hiding, and he knows this
  is the right thing to do but is frustrated.
- In his frustration he pulls upon _saidin_, causing a small earthquake.
- Perrin realises Rand has little control over the One Power.

## Chapter 3: News From the Plain

- Leya received a superficial scalp wound due to the earthquake and Perrin
  wonders whether this is the extent of injury she is going to suffer as a
  result of Min's foreseeing.
- Min does not openly say it but she is convinced Leya will die a bloody death.
- The news from Almoth Plain is that some 5000 whitecloaks are there – maybe
  half their total strength – but interestingly they seem to always find ways to
  avoid those who have sworn themselves to the Dragon.
- This paradoxical behaviour raises suspicion.
- There is also word that three uncommonly tall boys with eyes the colour of
  which is not typical for people from the plain have been inexplicably
  murdered.
- Lan theorises that the Soulless, assassins for the Dark One who are able to
  move unseen, may be responsible.
- Moiraine thinks Rand's act will have alerted them to nearby Fades.
- Perrin retires to his tent, troubled by the news, and dreams.

## Chapter 4: Shadows Sleeping

- In his dream, Perrin conversers first with Ba’alzamon who tries to get him to
  give up his axe in favour of returning to be a blacksmith, then with Lanfear,
  who urges him to seize his destiny.
- He sees himself in gold armour with a gold helmet but tries to reject the
  vision.
- As the world blackens, Perrin hears Lanfear tell him that she will always be
  there for him.
- He is then overlooking a bridge and sees a woman in a white dress – possibly
  Lanfear again – disappear in the distance.
- Subsequently, three ornately dressed men come onto the bridge and argue before
  being consumed in flame.
- Perrin looks up to see a wolf watching over him and rejects the vision.
- He is then in a huge domed room and immediately realises he is in a dream as
  he has been there four times before.
- In the middle of the room is a suspended sword which appears to be made of
  glass or crystal.
- As in the previous times he has dreams this dream, he goes for the sword but
  his hand meeting invisible wall about 1 foot away from it.
- For the first time, he hears a voice and the sword appears to declare itself
  as Callandor and whoever wields it controls destiny.
- He hears a second voice referring to him as brother and telling him the
  twisted ones have come.

## Chapter 5: Nightmares Walking

- Perrin awakes with the warning from his wolf brothers and sees trollocs
  approaching the camp.
- Before he can raise the alarm, Lan appears from his tent and sounds it.
- A battle ensues and Perrin goes in search of Leya.
- He finds her about to be attacked by a trolloc and kills it.
- However, immediately afterwards, a Myrddraal confronts him.
- Perrin is frozen and Leya tries to help but is killed by the Myrddraal.
- Enraged, Perrin embraces the wolf, knowing that a huge pack of his brothers
  and sisters are joining the battle.
- He manages to kill the Myrddraal and then joins the other wolves in taking
  care of the remaining attackers.
- In the aftermath, Min tries to comfort Perrin that there was nothing he could
  have done for Leya.
- Rand is found against a tree, disappointed in himself for having failed to
  use the One Power to help.
- In fact, he had to use all his strength to stop himself using the One Power in
  a way which would have harmed his friends.
- He is disappointed in himself for the lack of control he has over _saidin_.
- In addition, in the battle, his wound from his fight with Ba’alzamon has
  reopened.
- Moiraine, already tired from healing all the rest of the camp, does her best
  to help before being escorted away by Lan for rest herself.

## Chapter 6: The Hunt Begins

- The group awake to find Rand has left in the night.
- They meet to discuss tactics and Perrin displays anger towards Moiraine for
  what he sees as her keeping them in the dark.
- He is mirroring the way Rand and Nynaeve have acted towards her in the past.
- The earthquake created a new way out of their path and this is how Rand left
  unnoticed.
- Rand has left a letter: he does not want anyone else to die fo this and
  thinks his next meeting with Ba'alzamon will be their last.
- Moiraine discovers that many in the camp have been having the same dream that
  Perrin had and is able to theorise that Rand has been projecting his own
  dreams on others without knowing it.
- She is concerned that weakening seals may have allowed the Foresaken to
  escape.
- She asks Uno to run an errand for her.
- She asks Min to go to Tar Valon and deliver a message to the Amyrlin Seat –
  this is the only way she can guarantee the message will be delivered without
  being intercepted.
- She asks Loail and Perrin to accompany her and Lan chasing Rand.
- Afterwards, Min catches up to Perrin to tell him that when he agreed to go
  with Moiraine, she had a new seeing.
- She saw an Aielman in a cage, a Tuatha'an with a sword and a falcon and a
  hawk, perching on his shoulders, both female.
- The two talk and discuss Min's love of Rand.

## Chapter 7: The Way Out of the Mountains

- The group descend from their mountainous camp.
- During the descent, consciously and unconsciously, subtly and obviously,
  Moiraine reasserts her control over Loial and in particular Perrin.
- Lan overhears the two of them talking of this and tells them they should
  never have learned to challenge decisions anyway.
- They enter a village, Jarra, in the country of Ghealdan.

## Chapter 8: Jarra

- The group find an inn and speak to the innkeeper.
- Perrin smells a rank smell and wonders, though hopes he is wrong, whether it
  is Rand he can smell.
- The innkeeper tells them stories of Whitecloaks who have given up their vows
  and of numerous spontaneous marriages happening.
- They ask him directly if they have seen anyone of Rand's description and he
  says that he has and that Rand played flute at many of the weddings before
  heading east in a hurry.
- Moiraine explains that the behaviour of the Whitecloaks and the weddings were
  all examples of Rand's effects on other people.
- Perrin muses that this will make Rand easy to track for the darkfriends.
- The innkeeper asks Moiraine if if she will tend to his brother.
- When taken to him, they discover he has yellow eyes and Perrin realises that
  he has been taken over by the wolf.
- They apologise for being unable to help him.
- Before leaving, the innkeeper tells Perrin that the Whitecloaks were looking
  for a blacksmith with yellow eyes and agrees to bring him his food in his room
  so he will not need to go out and be seen in public.

## Chapter 9: Wolf Dreams

- Perrin and Moiraine speak – she wants to tell him all she knows.
- Unfortunately, not a great deal is known about Perrin's condition.
- She urges him to be particularly careful about his dreams - wolves live partly
  in the world of dreams.
- She also says that perhaps she underestimated how important she and Mat are
  compared to Rand.
- Perrin goes to bed intent on not sleeping but does indeed dream, including
  that of a man dressed in velvet - one of the forsaken? - who appears to be
  killed by the dream.
- He also sees Lanfear.
- Both of these people are able to see him and they're surprised to find him in
  their dreams.
- Lanfear says he will ruin everything by being here.
- He is being warned and guided through these dreams by Hopper, a wolf who died
  protecting him.
- Hopper wakes him up by going for his throat in the dream.
- Rand is cowering, hiding from a wolf.
- When it is within 10 paces, he strikes at it with a beam of the One Power.
- This instantly kills it but leaves Rand with the sickening aftertaste of the
  taint of _saidin_.
- Above all he is keen to reach Tear as he believes that he will learn whether
  he is the Dragon or not when he is there.

## Chapter 10: Secrets

- Verin, Nynaeve, Egwene, Elayne, Hurin and Mat are on their way to try to heal
  the latter in Tar Valon and also deliver the Horn of Valere.
- Mat is unconscious.
- Egwene at Nynaeve have numerous discussions, Egwene reflecting that they all
  have secrets.
- Within sight of the towers of Tar Valon, they come across a group of
  Whitecloaks who, unknown to them, are led by the son of Geofram Bornhald.
- Verin insists to all others that she will deal with this but during the
  exchange of words, Egwene becomes angry and fearful that she will be held
  captive again and she, Nynaeve and Elayne start to use _saidar_ to shake the
  Earth underneath the Whitecloaks' horses.
- Verin is furious and manages to get them to stop.
- While diffusing and chastising them, Egwene let slip that they have been in
  Falme, at which time the young captain reveals his father died there.
- He also reveals that Queen Morgase, Elayne's mother, may no longer have faith
  in Aes Sedai.
- The group continue.

## Chapter 11: Tar Valon

- The group arrive back at Tar Valon.
- Hurin takes his leave to return to Fal Dara and tell the King about Falme.
- As they are being cordially questioned at a bridge to enter the city, Egwene
  notices that someone surreptitiously speaks out against Aes Sedai.
- She remembers that had this happened before she left, that person would have
  been severely punished; things have changed.
- Once inside Tar Valon, Verin arranges for Mat to be taken away for healing and
  for the girls to be confined to their room pending an audience with the
  Amyrlin Seat.
- Egwene again speaks out of turn and Verin admonishes her severely,
  followed by a less severe admonishment by the Mistress of Novices.
- It is clear that the three women underestimated the severity of their action
  in leaving The White Tower.

## Chapter 12: The Amyrlin Seat

- The Amyrlin Seat prepares to receive Verin alone.
- Doing so alone is something of a surprise to her second-in-command, Leane, the
  keeper of the seals.
- Verin confirms it was Rand who announced himself as the Dragon Reborn – this
  is a relief to the Amyrlin Seat.
- In turn, she tells Verin that, presumably at this exact moment, two false
  dragons were unhorsed and subsequently eliminated.
- The conspiracy of the three Aes Sedai remains secret and successful.
- Verin produces the Horn of Valere, much to the Amyrlin's surprise, and
  explains that Mat sounded it, meaning that as long as he lives, they need not
  fear the horn falling into another's hands.
- It is imperative that the taint of the dagger from the Shadar Logoth is
  removed from him.
- Verin confirms rumours that the Seanchean use the One Power in battle –
  rumours of this will not go well for the Aes Sedai.
- Finally, they discuss the three runaways – the Amyrlin Seat plans severe
  punishment.

## Chapter 13: Punishments

- The Amyrlin Seat has made all the punishments she plans for the three
  women public knowledge – this widespread knowledge is part of the punishment.
- However, Egwene and Elayne have displayed such skill that the Amyrlin Seat
  has no option but to also raise them to the level of Accepted.
- There was suspicion that Liandrin was of the Black Ajah and it has now been
  confirmed as she left with a dozen other Aes Sedai, stealing a few
  _ter'angreal_ and killing 21.
- Elayne's mother, Queen Morgase, was so furious at her disappearance that she
  has demanded she leaves the White Tower.
- The Amyrlin Seat has managed to convince the Queen that it is not safe for
  her to leave until she has learned some more control.
- She tells Elayne that her real plan however is to keep her and teacher as much
  as possible as she has the potential to be great.
- Elayne's two brothers were also to be removed from their training as Warders
  but they managed to convince the Queen otherwise.
- The Amyrlin Seat dismisses Elayne as she wishes to talk to Nynaeve and
  Egwene privately.

## Chapter 14: The Bite of the Thorns

- The Amyrlin Seat explains that the rumours are that darkfriends did the
  killing in the white tower and that as of yet, no one knows that Aes Sedai
  were killed and no one knows that the Black Ajah exist and are to blame.
- The Amyrlin Seat explains that Egwene and Nynaeve are the only two in whom
  she can trust in the White Tower; she also trusts Moiraine but Nynaeve is not
  sure.
- Therefore, and due to the lack of suspicion that will surround them given
  their shameful return to the White Tower, the Amyrlin Seat charges them
  with being her Black Ajah hunters.
- She explains that both of them have displayed as much power as anyone in the
  White Tower, particularly Nynaeve though she is unable to do it consciously,
  relying on emotion.
- She wishes she could raise Nynaeve to the shawl.
- She explains that she cannot include Elayne in this plot due to the scrutiny
  her mother the Queen has put her under.
- She gives the two women papers bearing the blessing of the Amyrlin Seat to
  carry out whatever deeds they choose.
- Verin will also tell them all she knows.

## Chapter 15: The Grey Man

- Nynaeve escorts Egwene back to her quarters but just before they enter her
  room, they are nearly killed by an armour-piercing crossbow bolt.
- When they recover their senses, Nynaeve reveals that she has trapped a man
  using the One Power to freeze the air around him.
- When they reach the man, they discover him stabbed and dead and conclude there
  must be a second assassin.
- The Mistress of Novices arrives and confirms the dead assassin to be one of
  the Soulless or a Grey Man.
- The women decide not to tell her about the crossbow (and therefore existence
  of a second assassin) and Egwene quickly excuses herself to go and remove the
  bolt from the wall outside her room.
- However, she discovers the bolt to be missing already.
- When she returns, she notices that the Mistress of Novices seems angry with
  Nynaeve.
- When she leaves, Nynaeve mentions to Egwene that the Mistress of Novices never
  questioned who might have stabbed the dead assassin.

## Chapter 16: Hunters Three

- Elayne and her two brothers are in Nynaeve's room.
- They speak openly about wanting to stay in the White Tower to project not just
  Elayne but also Egwene and Nynaeve.
- Egwene finds herself bashful in front of Galad's beauty.
- When they leave, the women discuss that Gawyn is clearly in love with Egwene
  but as he owes his life to Galad, he is remaining silent.
- In a surprise to Egwene, Nynaeve openly tales Elayne about the Black Ajah
  and the Amyrlin Seat's plan for them.
- She believes the attention the Amyrlin Seat gave to Egwene and Nynaeve means
  that they actually are under suspicion but Elayne is not.
- She also confides in the two women that she has suspicions that the Amyrlin
  Seat does not plan to heal Mat.
- She plans to try for herself, asserting that everyone has told her how
  powerful she should become and she might as well test herself.
- Elayne and Egwene wonder if the three can work together more safely than
  Nynaeve on her own.
- Just before they are about to try channelling together for the first time,
  another Aes Sedai enters the room suddenly.

## Chapter 17: The Red Sister

- Elaida confronts the group, particularly Elayne, in whom she is extremely
  disappointed.
- Elayne is, or perhaps was, going to be the first Aes Sedai who was Queen
  for millennia and Elaida had done much to prepare her.
- She hopes the situation can be salvaged.
- She speaks openly about Black Ajah, saying she can help protect them.
- She questions the group about what they did while away, being particularly
  interested in Rand, and Egwene and Nynaeve tell a half-truth that they went to
  rescue Mat.
- Elaida seems to be accepting of what they have said when the Mistress of
  Novices comes in and she leaves.
- The Mistress of Novices is furious that Nynaeve and Egwene have told Elayne
  about the murder.
- She reiterates that they should only discuss it with her or the Amyrlin
  Seat and that they should leave the investigation to them.
- Finally, she explains the purpose of her visit.
- Healing in the White Tower is always done in the presence of those who have
  brought the victim and so they are to go to Mat, where the Amyrlin Seat
  is waiting.
- Nynaeve is sure that the mistress is in no hurry.

## Chapter 18: Healing

- The Amyrlin Seat and nine other Aes Sedai attempt to heal Mat using
  the aid of a _sa’angreal_.
- Egwene has never seen it before but recognises it from lectures as the most
  powerful _sa’angreal_ the White Tower progresses.
- The healing process seems to be an enormous effort for both Aes Sedai
  and Mat.
- Nynaeve subconsciously says that she is not sure if she could handle half the
  power of that the others are using before catching herself – she has just
  claimed she would be unsure she would able to control the power of 10 Aes
  Sedai including the Amyrlin Seat using the most powerful _sa’angreal_, a
  lofty thought.
- At the end, one of the Aes Sedai puts the dagger in a special box and
  Mat appears to relax.
- The Amyrlin Seat tells the woman that she thinks she has done all that can
  be done for him and the now need to go and do their chores.

## Chapter 19: Awakening

- Mat awakens, with strange memories of the previous months.
- Chief among them is a memory of leading a battle in the Trolloc Wars.
- He wrestles himself into the present, theorising that he must be in Tar Valon
- He remembers being sick.
- There is a vast feast laid on a table in his room.
- He starts to think about his friends and his predicament: surely Rand has gone
  mad by his use of the One Power and Perrin by his strange transformation into
  a wolf?
- He surmises that he is the only hope left.
- Just as he is happily planning to leave, he remembers blowing the Horn of
  Valere.
- He tries to reassure himself that the Aes Sedai will only have interest in the
  horn, not the blower.
- Just as he realises that he has eaten all the food that was lain out and is
  still hungry, there is a knock on the door.

## Chapter 20: Visitations

- Lanfear is Mat's visitor, again calling herself Celine.
- She tells him he is more important then he or the Aes Sedai believe and
  that he is more likely to seek glory than Perrin or Rand.
- She wants him to be vigilant as the Aes Sedai will plan to use him.
- She tells him some things which they will not tell him and says she will be
  there for him.
- When he directly confronts her, she does not deny being darkfriend but
  instead pours scorn on darkfriends and says she will serve no man but there
  is one alongside whom she would stand.
- After he says something which displeases her, he feels discomfort and believes
  she is starting to use the One Power on him but she is distracted by something
  outside the room and quickly leaves.
- The Amyrlin Seat comes in and initially annoys Mat by denying all of his
  desires to leave.
- His recovery is nowhere near complete and she does not want her healing to be
  wasted.
- She has told the guards of the exits of Tar Valon not to let him off the
  island.
- When he directly confronts her, she tells him the truth about some of the
  things Celine said she would not.
- At the last, she tells him that he is linked to the horn and that when the
  last battle comes, she plans for him to sound it.
- Mat tries to claim he is not a hero.
- When she leaves, he considers that both of his visitors have told him half-
  truths and he is not keen to trust either.
- He sets about trying to find a way out of both of their ways.

## Chapter 21: A World of Dreams

- Egwene goes to see Verin as requested.
- Verin explains to her some of the work she has been doing trying to meet
  translations from the Age of Legends.
- She explains that Egwene may still be a dreamer and that if she was, she would
  be the first Aes Sedai dreamer for a very long time.
- She explains that there is only one Creator and one Dark One in all the worlds
  of 'if' which the portal stones around travel allowed travel between.
- The Dark One is either free or imprisoned in all of them.
- She says however that there is a world of dreams which seems to exist within
  and around all of them.
- Dreamers can enter this world and when they are there, they are _truly_ there
  and can be affected by it.
- She gives Egwene a _ter’angreal_ which the last dreamer owned, saying it
  will help her but is dangerous.
- She also gives her all the information she has on Liandrin and the
  _ter'angreal_ they stole.
- Egwene leaves, determined to learn.
- After she has left, Verin considers what she did not tell Egwene – she has
  copious notes from the last dreamer but she feels that it is best to let
  Egwene's experiences with the _ter'angreal_ play out for them self.
- That previous dreamer was so secretive that Verin is convinced that only she
  and Egwene know about the existence of this _ter'angreal_.

## Chapter 22: The Price of the Ring

- Egwene is to pass through the three arches before being raised to Accepted,
  just as Nynaeve did previously.
- Elayne has already passed her test; Elaida was happy for Elayne to be tested
  but does not think Egwene is ready.
- Egwene is worried about leaving the _ter'angreal_ and the papers Verin gave
  her lying on the floor while taking the test, but there is no alternative.
- A strange resonance is noticed in the arches (which are also _ter'angreal_) by
  one of the onlooking Aes Sedai but the test progresses as planned.
- All three (past, present and future) tests feature Rand.
- In the second, he tells Egwene he will be turned to the dark.
- Between her second and third tests, Egwene asks Sheriam if this is possible.
- Sheriam says 13 Dreadlords channelling through 13 Myrddraal can do this.
- Egwene notes that Liandrin left with 12 darkfriends - 13 in total.
- In her final test, Elaida is a Black Ajah and tries to turn Egwene to the
  dark, but she escapes.

## Chapter 23: Sealed

- When Egwene emerges from the _ter’angreal_, the onlooking Aes Sedai are
  both surprised and worried.
- Elaida rushes to get the necessary equipment for the Amyrlin Seat to
  complete the ceremony, raising Egwene to Accepted.
- After, she explains to Egwene that she does not believe she should get this
  honour, not because she does not have the ability but because Elaida believes
  she is a wilder and will always be a wilder.
- The Aes Sedai who noticed the strange reverberation before Egwene entered
  the arches comes to apologise.
- The reverberation returned and strengthened while Egwene was inside and they
  feared for her life.
- She has only heard of this sort of thing once before – when two related
  _ter'angreal_ where are used in close vicinity of each other.
- Egwene realises the problem was the ring which Verin gave her.
- As she saw herself as Green Ajah inside the arches, she asks Alana what it
  means to be Green Ajah.
- Although Alana start by saying that Green Ajah love rather than hate or fear
  men, she emphasises that the Green Ajah are also known as the battle Ajah, and
  are proud to be talked of like this.
- She feels responsible for putting Egwene's life in danger and because of this
  offer is to share her chores but the Amyrlin Seat dismisses this.
- Egwene returns to the living quarters but immediately goes to see Nynaeve.
- She finds Elayne collapsed in Nynaeve's lap, clearly having been crying for
  some time due to her actions in the _ter’angreal_.
- Suddenly, with the danger and intrigued about what happened in the arches
  gone, Egwene also remembers the times she abandoned Rand.
- She also collapses into tears, with both women being comforted by Nynaeve.
- Nynaeve expresses an intention to get revenge on the Aes Sedai for what
  they do to their trainees.

## Chapter 24: Scouting and Discoveries

- Mat goes to look around Tar Valon.
- He finds a crossing guard and tries to subtly question him but the guard
  correctly identifies him at explains that he is not going to be allowed to
  leave.
- The guard asks why Mat is being held and he answers honestly that he has just
  been healed.
- He sees Else, the farm girl they met on the road sometime ago.
- She expresses no interest in him and knows all about his healing.
- He sees men training and realises that Gawyn and Galad are being trained by
  Warders.
- Galad has an audience of Aes Sedai and Accepted.
- The two recognise him and come to speak to him.
- He has been massively impressed by their skill with a sword but, thinking he
  needs the money, he surprises himself by challenging them both at the same
  time to fight him with a quarterstaff.
- An onlooking Warder agrees to take the bet on behalf of the two nobles.
- He asks a strange question, "You speak the old tongue?" of Mat before the
  fight.
- Mat has great skill with the quarterstaff but feels exceptionally weak and
  hungry.
- Nevertheless, he is determined not to lose his bet and his money and he
  quickly incapacitates Gawyn.
- Galad proves more difficult but in truth, the only threat to Mat winning is
  that his strength might give out.
- Just as he is delivering a series of blows to incapacitate the lad, he sees
  his neck and momentarily thinks of striking out at it before realising this
  would be a killing blow, not a blow to win a bet.
- The Warder tells the onlooking trainees the story of the greatest swordmaster
  ever known, who was only ever beaten once, by a farmer with a quarterstaff.
- He asks Mat where he comes from and Mat says Manetheren before correcting
  himself to say the Two Rivers.
- He is later confused by him saying this and by being asked if he speaks the
  old tongue.
- Gawyn congratulates Mat and tells him he has some skill.
- Galad is still being tended to by overprotective and interested women when
  Mat leaves.
- He has so little strength left he now has to use his quarterstaff as a walking
  stick.

## Chapter 25: Questions

- Egwene is unsettled by her most recent dreams, including those about Rand,
  Mat and Perrin.
- Apparently, dreamers dreams about _Ta'veren_ and are most often significant.
- She, Elayne and Nynaeve are looking through the papers given to them by Verin.
- Nynaeve insists that there is no connection between the 13 Black Ajah.
- Elayne sees more in it than this – they are evenly spread in age and in
  originating country as well as in the Ajah from which they come.
- Elayne sees the lack of pattern as deliberate and when she pointed out, the
  others agree.
- The majority of the _ter’angreal_ stolen have unknown purposes, and those
  without a purpose were all last studied by the same dreamer whose ring Egwene
  has, Corianin.
- The woman wonder whether Verin noticed this same Aes Sedai studied all these
  _ter'angreal_.
- They are uncertain whether any of the _ter'angreal_ with known uses are of
  any significance.
- Egwene decides she will try to use her _ter’angreal_ in her sleep tonight.
- As if appearing from nowhere, the farmer's daughter turned novice, Else, is in
  the doorway.
- They chastise her for not knocking and she seems to be looking at all the
  things they have out.
- She calmly tells them that the Amyrlin wants them to know where the
  belongings that the departed Black Ajah left behind are being kept.
- She leaves in a hurry.
- The woman are surprised that the Amyrlin seems to trust that this girl and
  express relatively low opinions of her.
- Egwene suddenly decides to follow her but she is nowhere to be seen, leading
  Egwene to conclude she must have run.
- In her search, Egwene comes across Lanfear who briefly talks to her before
  telling her to leave.
- Egwene finds herself instinctively inferior to this woman and takes the order
  to leave without even thinking about it.

## Chapter 26: Behind a Lock

- Egwene explains to the other two about the woman she saw and Nynaeve says that
  should she see her again, she should let Nynaeve know straight away.
- They go to the location Else said held the left-behind items and find it
  locked.
- However, Egwenes powers with metal allow her to break the chains.
- Inside, they find something left behind by every woman which points to the
  city of Tear.
- They are uncertain what to do with this information.
- It is so obvious, then it must be a trap.
- It may even be hubris, with the Black Ajah taunting the White Tower, daring it
  to follow them to Tear.
- Egwene thinks that her dreaming might provide some answers – dreaming is
  closely linked to foretelling.
- However, Nynaeve reminds her that they have their punishment in the kitchens
  to carry out first.

## Chapter 27: _tel'aran'rhiod_

- While the women are doing their punishment in the kitchens, numerous Aes
  Sedai come to visit them.
- Verin openly asked them if they have made any progress with what she has given
  them.
- The women struggle to believe this apparent mistake in discretion was
  accidental and they now have some suspicion that Verin may be Black Ajah.
- Egwene prepares to enter the dream world.
- Nynaeve and Elayne will keep watch over her while she is asleep.
- She sees many things in there, including Perrin with the wolf Hopper at his
  feet.
- Perrin recognises her and tells the wolf not to attack.
- She sees Rand – she looks exhausted and assumes she is another temptation
  from the Dark One (he has already had to fight away his mother and father.)
- Egwene loses her temper at him for not believing her and tries to fix him
  using the One Power but he resists easily, telling her he has learned.
- Next she finds herself in the vast hall in Tear containing Callandor.
- An old, haggard looking woman who calls herself Silvie addresses her and says
  they know each other.
- She says that Egwene will find answers in this place.
- She references one of the forsaken – Ishamael – by name, which to Egwene is
  surprising.
- She says that to deny or to mock the Forsaken or the Dark One is powerful.
- She tells Egwene that someone is coming and that someone should not see her
  there and tells her to leave but is then surprised to hear that Egwene does
  not know how to do this.
- She advises her to learn before waking her up.
- Nynaeve and Elayne say that Egwene appeared to be sleeping undisturbed.
- She tells the women of what happened, other than about Perrin – that is his
  secret to divulge.
- The women decide they must leave the White Tower again and Elayne must write
  to her mother.
- However, as the Amyrlin Seat does not yet know that Elayne has been
  recruited as one of the darkfriend hunters, they have to try to figure out a
  way of getting a letter and then themselves out.
- Nynaeve thinks she can think of a way.

## Chapter 28: A Way Out

- The woman ask Mat to help them get the letter to Caemlyn.
- He refuses their initial attempt to use charm on him and when they get angry
  with him, he explains that he doesn't even have a choice – he is forbidden to
  leave the city.
- The woman produce the letter from the Amyrlin Seat, giving the bearer the
  right to do what they want with her permission.
- With this, Mat expects to be able to leave Tar Valon and get to Caemlyn.
- He believes he will be free of the Amyrlin Seat and of Celine and that
  nobody will ever catch him.

## Chapter 29: A Trap to Spring

- The Amyrlin Seat asks for an update from Egwene and Nynaeve.
- They explain about the findings based on the tip off from Else.
- The Amyrlin Seat explains that all the belongings of the Black Ajah were
  burnt after being searched and that Else was sent away from the White Tower 10
  days ago.
- Clearly, there is an impostor able to impersonate other people.
- In addition, the Mistress of Novices found a dead Grey Man in her bed.
- Given that all the clues pointed to Tear, the woman conclude that there is a
  trap for them in that city.
- The Amyrlin Seat gives them leave to go and says they should take Elayne.
- The Amyrlin Seat knew they would involve Elayne in their hunt for the
  Black Ajah but was unable to directly request it as she is trying to rebuild
  her relationship with the Queen.
- Nynaeve asks about Callandor and the Amyrlin Seat says that it is
  the third most powerful _sa’angreal_ ever created and must not fall into the
  hands of the Black Ajah.

## Chapter 30: The First Toss

- Mat leaves the white tower.
- On his way out, he passes the last Aes Sedai who visited him – Anaiya –
  who gives him a wry smile as she does not think she will be able to escape and
  thinks this will be pointless.
- He goes gambling to earn money and surprisingly wins every game he plays
  including games he has never played before.
- He feels in a fever when he is playing and things are almost happening
  subconsciously.
- At the end of the night, he is in an alley contemplating whether the dagger,
  the Dark One or the Aes Sedai have given him such luck when he realises
  that some men are following him.
- Again displaying some luck, he manages to evade all of them but ends up on the
  roof tops with a narrow bridge to cross.
- Appearing out of nowhere, there is another man on the bridge attacks in with a
  dagger and the two are caught in a grapple.
- Mat tests his luck again by throwing both of them off the bridge.
- When he awakens, he finds that he landed on top of the other man, driving the
  man's dagger through his heart and again is shocked by his luck.
- He goes to find a quiet inn to sit down as the immensity of the evening's
  events has finally hit home.

## Chapter 31: The Woman of Tanchico

- Mat enters the inn and immediately discovers that the entertainer playing
  there is Thom.
- Thom joins him after the story and Mat finds him to be drunken and distracted,
  something he has never known Thom to be.
- He also seems to be somewhat self pitying.
- Mat explains his mission of going to Caemlyn and, given his mood and the risk
  to his life of going there, Thom decides he will go with Mat.
- Outside, there is no sign of the body.
- Mat asks Thom about thieves in Tar Valon; Thom says that due to the strict
  punishment, there are none.
- Mat wonders if the people chasing him we are not thieves, what were they?

## Chapter 32: The First Ship

- The letter from the Amyrlin Seat successfully gets Mat and Thom passage on
  a ship.
- They still have to pay the captain handsomely but Mat is now able to afford
  this.
- The first night on the ship, it is boarded and Mat kills three assailants and
  Thom another one.
- Mat again thinks about his luck.
- Rand is sitting at his camp fire playing music to keep himself awake.
- In his dreams, everyone is out to get him.
- Even Min attempted to stab him in the back.
- Celine is the only one who wants to stay with him, and she wants him to wield
  Callandor.
- Callandor is ever in his dreams.

## Chapter 33: Within the Weave

- Perrin, Loial, Moiraine and Lan are pushing hard on Rand's trail, following in
  the wake of events that have happened around him.
- Moiraine says they are neither good nor evil - the Pattern is neither.
- Perrin thinks he saw a footprint in stone and smells a strange, evil smell -
  neither trolloc nor Myrddraal- but evil.
- They enter a town and an inn where Moiraine and Lan are known by their
  pseudonyms.
- There is a captured Aielman caged in the town square and another guest in the
  inn tells an embellished story of how they captured him.
- Perrin notices a dark-haired woman looking at him, making him uncomfortable

## Chapter 34: A Different Dance

- Perrin, still trying not to sleep, goes walking.
- Moiraine is still trying to decide which move to make next to chase Rand.
- Perrin finds himself at the Aielman and frees him.
- The Aiel are searching for, "He who comes with the dawn," and Perrin says he
  is going to Tear.
- Some Children of the Light find them and the Aielman goes on the attack,
  taking Perrin with him.
- The two easily deal with many Whitecloaks and the Aielman leaves.
- As Perrin returns to the inn he sees the black-haired girl again.
- Lan finds him and questions him about the dead Whitecloaks; he says they have
  to leave immediately.

## Chapter 35: The Falcon

- The group make a quick escape by sea; Loial thinks he is beginning to enjoy
  adventuring.
- Just before the boat casts off, the woman from the inn jumps aboard.
- Perrin speaks to her: she is a hunter and her fascination with Perrin is that
  she easily figured out what the other three were but was confused by him.
- She plans to follow them, thinking that will lead her to the Horn.
- Her name Zarine, but goes by Mandarb, which makes Perrin laugh as that is
  Lan's horse's name.
- She says instead she could be Faile - or falcon - which is one of the two
  female birds Min saw on Perrin's shoulders during her last viewing of him.

## Chapter 36: Daughter of the Night

- Hopper greets Perrin in the dream world _tel'aran'rhiod_ and takes him to see
  something he needs to see.
- A gathering of darkfriends are being berated for their failures and the one
  who let Mat escape Tar Valon is killed.
- I thought it was Ba'alzamon addressing them but the Wheel of Time wiki
  suggests it was Ishamael.
- Lanfear appears and is addressed as this: Perrin now knows who was urging him
  to greatness.
- Lanfear calls this domain hers and now she is free she will use it.
- Myrddraal and Grey Men do not dream; everyone else does.
- She still claims fealty to Ba'alzamon but mentions the inability to be rid of
  the Dragon.
- Hopper leaves and Perrin sees Rand, who is dispatching multiple Fades and
  darkfriends with a sword of _saidin_.
- When he sees Perrin, he attacks, waking Perrin.
- Perrin tells Moiraine of his dreams and she is frustratingly held back in her
  replies.
- She says she does not think he was or can channel and so he should not worry
  about going mad but Red Sisters might kill him before they had a chance to
  find that out.
- She thought some Forsaken might be free.
- She tells him nothing he wanted to hear.
- Rand is awake now and realises that he attacked the real Perrin, not a decoy.
- He thinks he has to be more careful.
- A woman and 8 guards approach in a friendly manner.
- Rand kills them all.
- When arranging the bodies, he notices a 9th man - presumably a Grey Man.

## Chapter 37: Fires in Cairhien

- The three women are on their way to Tear by boat.
- The towns and villages they pass are burned and they see no one alive.
- There is much talk between them and Egwene has been using the _ter’angreal_.
- Elayne and Nynaeve have also used it but without significant effect.
- Egwene has had numerous visions which she cannot interpret but she figures out
  that in one of them, Mat is being chased by a Grey Man.
- Their boat suddenly runs aground and the captain is frustrated that the
  helmsman has hit a mud flat but then realises they have hit the hull of the
  sunken boat and he suspects brigands.
- The women decide they cannot wait for another boat to rescue them and try
  walking to the nearest town.
- On their way, a woman appears unseen in front of them.

## Chapter 38: Maidens of the Spear

- The woman is an Aiel woman and warrior – known as a Maiden of the Spear.
- She has recognised them as Aes Sedai.
- Only Elayne, thanks to her highborn education, knows anything of depth about
  the Aiel.
- They talk about what it means to be a Maiden of the Spear but quickly come to
  the real purpose – one of their number is severely injured and they hope she
  can be healed with Aes Sedai help.
- On the way to the injured woman, they all talk and Egwene learns that the Aiel
  feel they owe service to the Aes Sedai.
- Nynaeve perform is a healing which is remarkable in the eyes of Egwene.

## Chapter 39: Threads in the Pattern

- The three women leave the maidens of the spear.
- Not long afterwards, they are attacked and captured by the brigands.
- They are knocked unconscious and taken somewhere.
- Egwene is the first to wake, overhearing the men saying that the person to
  whom they are to be delivered to will be here by the morning.
- When she has a chance, she wakes Nynaeve but the two find Elayne severely
  injured and Nynaeve is certain she will not be able to help her without her
  herbs, which have been taken.
- The fury and anguish this causes her allows her to channel and use only the
  One Power in the healing.
- Before the three women attack the Brigands, they see that the person waiting
  for them is a Myrddraal, with two others in tow.
- He tells the brigands that these are the three women he is looking for.
- Strengthening their resolve to attack, Egwene breaks the lock but at this
  moment there is an attack by the Aiel and within an instant, all the
  brigands are dead for the loss of one Aiel.
- The Myrddraal have been penned together but before the Aiel can advance
  on them, the three women burst out and use the One Power to consume all three
  Myrddraal in flame.
- A bolt of dazzling white light then extends from Nynaeve and causes the murder
  all to disappear.
- She does not know what it is but Egwene recognises it as balefire.
- When re-gathering their stolen possessions, an Aiel man recognises the ring
  that Lan gave to Nynaeve and says that seeing it in her possession is the
  strangest thing he has come across in this quest.
- Nynaeve is determined to carry on and so the Aiel escort them in the
  night to the nearest town.
- The woman decide they want to enter Tear without being obviously Aes Sedai
  and Elayne thinks she will have to hide her appearance in case she is
  recognised as the daughter-heir of Andor.

## Chapter 40: A Hero in the Night

- Mat and Thom arrive in a town.
- Mat has spent much of the journey teasing and winning money from the captain
  whom neither of them like.
- For example, they sailed straight past the same boat that Egwene, Nynaeve and
  Elayne were on without helping.
- He has also opened and read Elayne's letter.
- The town is crowded with people fleeing the violence in Cairhein.
- After numerous failed attempts to secure accommodation, Mat uses his newfound
  wealth and luck to both pay for and win a bed for the night and horses.
- As they settle down in the stable, a woman secretly enters, followed by four
  men who have been chasing her.
- Instinctively, Mat attacks and incapacities them.
- The woman is an Illuminator and gives Mat some fireworks for his services
  before they all leave.

## Chapter 41: A Hunter's Oath

- Moiraine is upset at Perrin: she has discovered that Zarine knows she is Aes
  Sedai and that Perrin did not tell her this.
- Perrin has had dreams in which he has seen Hopper, but Hopper has chased him
  away as, "Too young," to see what is ahead.
- Once the ship docks in Illian, and Zarine accidentally learns a little
  more of their plan, Moiraine confronts her.
- She tells her that if she cannot be dissuaded from following them, she must
  join them, extending her Hunter's Oath to vowing to do as Moiraine says, tell
  no one of their motives and asking no questions.
- She tells Perrin that Zarine is his responsibility and she reminds them in
  no uncertain terms that she has no allegiance to them and will do what needs
  to be done if they get in the way of her plans.
- During this discussion, she reveals to Zarine for the first time that
  Perrin is _ta'veren_.

## Chapter 42: Easing the Badger

- Perrin thinks something is wrong in Illian – many people are wearing angry
  expressions but look as if they don't know why they are angry.
- He thinks that Lan and Moiraine have spotted it too.
- Loail suddenly comments that it is a bad idea to have come here as
  other ogier frequently visit Illian to perform maintenance on the buildings,
  many from his stedding, and if they see him they will take him back with him.
- Perrin tries to console Loial that Moiraine will not let them take him away;
  he also lets slip in front of Zarine that they have travelled the Ways.
- The Falcon has noticed that her companions have spotted something unusual but
  she cannot sense it and asks about it but is told, "No questions," by
  Moiraine.
- A boy tells loyal and Perrin that all the ogier in the city left recently.
- This also concerns the group.
- They enter an inn where Moiraine knows the keeper - she is an agent for the
  Blue Ajah - and begin to talk.
- A man called Lord Brend who was previously unknown has been raised to the
  council of nine and the council has stopped all ships sailing to Tear; there
  is even talk of war.
- Perrin smells the vile smell he smelt twice before on the road but notices
  that Lan and Moiraine, both of whom have been able to sense Myrddraal in the
  past, do not appear to have noticed anything.
- He sees six non-descript men moving across the inn and realises they are
  reaching for knives and that what he has been smelling are Grey Men.
- They manage to dispatch their foes though due to the confined space, Moiraine
  was not really able to help.
- With a grim and fearful expression – Moiraine being afraid is terrifying to
  Perrin – she tells Lan that there is evil here that he cannot help her with
  and that he must return to Tar Valon with Perrin.
- Lan says she is speaking as if she is already dead.
- She says she does not know if she will be able to overcome the evil here but
  that she has been a fool for ignoring how important Mat and Perrin where
  beside Rand: the Grey Men showed no interest in anyone other than Perrin.

## Chapter 43: Shadowbrothers

- Lan asks Loail and Perrin to help him have a look around outside
  given that he suspects they both have better eyes than him, and Perrin a
  better nose.
- They discover more wolf tracks in stone and Lan is able to identify these as
  belonging to darkhounds.
- Perrin was able to smell a faint smell of it.
- He asks Lan why he was unable to sense the darkhound or Grey Man and Lan
  does not give him the answer but says that whatever the answer is, it is
  unlikely to be good.
- From the tracks, he thinks the darkhound and has found what it was looking
  for and has returned to report to its master.
- Perrin dreams but cannot find Hopper.
- He sees Mat playing dice with Ba’alzamon and tries to warm him but the dream
  fades.
- He sees the three Accepted springing a trap but becoming entrapped by it, a
  woman with braided hair looking on and laughing and a woman in white laughing
  at that woman.
- The woman in white is presumably Lanfear though Perrin does not confirm this
  even though he now knows who she is.
- Finally Hopper arrives and Perrin demands to be told what Hopper says he is
  too young to know and Hopper gives him some cryptic information about what is
  real and what is not until saying that the last hunt approaches.
- Perrin asked him about darkhounds.
- Hopper is shocked, refers to them as shadow brothers and tells Perrin that
  should he encounter one, he should flee.
- Hopper then wakes him up.
- The Falcon is in his room watching him and she questions him, making him
  uncomfortable.
- Moiraine bursts in, telling Perrin that his dreams are almost as good as a
  dreamers dreams and his vision that the Forsaken are free is true.
- One of them is in power in Illian.

## Chapter 44: Hunted

- Moiraine tells them that Sammael is the first second in charge of alien.
- She sounds almost hurried – they must leave and she tells the innkeeper she
  must leave immediately and also tell anyone she cares about to leave.
- She offers Zarine one last chance to leave but in the confusion and hurry,
  Perrin reveals that Rand is the Dragon Reborn and at this revelation, Moiraine
  tells Zarine she is bound to them.
- Lan has killed the darkhound and Moiraine says that Sammael will hear about
  it soon enough but she is also confident that the Grey Men were not sent by
  him as he would not send both.
- She also believes he does not know that Perrin is in Illian or even perhaps
  who he is.
- What he will know is that there must be a Warder in Illian if one of his
  darkhounds has been killed and that means there is also an Aes Sedai.
- As they flee, Moiraine and Lan prepare a stand against what Lan predicts to
  be 10 chasing darkhounds.
- Perrin prepares his bow after Lan says that Zarine's knives will be useless.
- Despite not being the best bowman, Perrin's first three arrows all hit home on
  the lead darkhound but it takes all three to stop it and in this time, he
  expects the others are too close to deal with and he berates Moiraine in his
  mind for having done nothing.
- As he looses his fourth arrow, Moiraine unleashes balefire, destroying all
  remaining darkhounds.
- She confesses that it is forbidden to even know how to do that and she would
  be stilled if it was known that she had learned, never mind used balefire.
- She hopes that because they have already travelled far from Illian and because
  the hounds were very close when she unleashed it, Sammael will not have seen
  the balefire; Zarine cannot believe anyone as far away as Illian could see
  anything that they have done.
- Lan says they must make their way to Tear as quickly as possible.
- Mat and Thom are camped on the road.
- Mat decides to try his luck again by inspecting the inside of one of the
  fireworks, expressly against all common sense and to the fury of Thom; it does
  not explode in his face.
- A group of women and three men approach and the woman addresses Mat.
- Just as he rises to respond, he sees that one of the men has raised a crossbow
  and the woman shouts, "Kill him now."
- Instinctively, Mat throws a firework on the fire and in the confusion, Thom
  kills two of the men with knives and Mat reaches his quarterstaff,
  subsequently incapacitating the third man.
- He turns to interrogate the woman but before she can even respond, one of
  Thom's knives is coming out of her neck.
- He tries to admonish Thom for killing a woman but Thom reveals the large knife
  she was carrying.
- The two initially think they were robbers but then Mat recognises the
  crossbow man as the same man that escaped the ship after the previous attempt
  on Mat's life.
- He curses Elayne for giving him the letter and the task of delivering it and
  insists there is something important contained within it though Thom says he
  can see nothing of interest in it at all, even a coded message.

## Chapter 45: Caemlyn

- After entering the city, Mat is keen to be rid of the letter and approaches
  some palace guards however they are aggressive towards him and they thought
  that he might have come from Tar Valon.
- He has to flee to avoid being arrested.
- He meets back up with Thom and forms a new plan whereby he will not have to
  get past any palace guards but it will involve him winning a huge game of luck.
- There is talk of a new advisor to the Queen who has turned him against eye in
  the sky – presumably another one of the forsaken - called Gaebril.

## Chapter 46: A Message Out of the Shadow

- Mat has chosen to sneak in to the palace by climbing the same wall the Rand
  did.
- With luck on his side, he manages this and to avoid guards on the inside.
- He overhears a conversation between two people he cannot see with one
  reporting to the other that three women are on their way to Tear from Tar
  Valon and that one of them is Elayne.
- The reporting man is called Comar.
- The man who sounds like he is in charge says that the person who is in Tear is
  acting too impatiently but he does not mean to go to war with him.
- He has grown tired with Elayne and asked for her to be killed then later
  thinks all three of the women should be killed.
- Mat just has a chance to catch a glimpse of these men as they walk away.
- He presents Elayne's letter to the Queen and is just about to tell her of the
  plot he overheard when he notices that one of the men glimpsed is standing at
  her right hand side – this new advisor Gaebril.
- Cautiously, he holds his tongue; the Queen asks him if he ever returns to
  Tar Valon to tell his daughter her daughter that she will not be removed from
  there before time and the adviser rewards him for bringing this message.

## Chapter 47: To Race the Shadow

- Mat returns and tells Thom and master Gil what happened in the palace.
- He asked master Gil all he knows about the Queen's new advisor but this is
  little.
- Remembering that she is quick to anger, Thom suggests they start a rumour that
  the adviser means harm to the Queen's daughter – this might be enough to lose
  him favour with her.
- Thom and Mat plan to race to Tear by boat to try to help the three women and
  stop Comar.

## Chapter 48: Following the Craft

- The women arrive in Tear.
- Egwene sees the Stone of Tear and is awestruck despite having read
  descriptions about it.
- It was the first fortress built after the breaking of the world and was forged
  using the One Power – its vast stone walls are seamless.
- Reasoning that the Black Ajah will know they are coming and likely be watching
  the inns, they think about where they might be able to find lodging.
- Nynaeve sees herbs in window and wonders if the owner of the house might be
  similar to a Wisdom.
- This turns out to be true and the two women share lots of knowledge.
- Eventually, she agrees to house them but only after they tell her what they
  are up to.
- Nynaeve quite skilfully tells half-truths, gaining the women's trust.
- The woman even says she knows an expert thief taker who will help them find
  these women they are after.
- Egwene says that Nynaeve manipulated the woman so well it was as if she was
  Moiraine.
- For this, Elayne slaps her hard and tells her she goes too far and that they
  must try to get along.
- There has been increased tension between the two women of a Emond's Field, who
  are now technically at the same level but previously were master and
  apprentice.

## Chapter 49: A Storm in Tear

- Using the _ter’angreal_, Egwene dreams herself into the Stone of Tear and
  sees the 13 Black Ajah sitting around Callandor.
- Liandrin turns and smiles at her.
- She tells Elayne and Nynaeve that she thinks their enemies know they are in
  the city.
- They meet the thief-taker Juilin who says he has seen men on rooftops and
  thinks thieves have moved in to Tear.
- Mat and Thom arrive in Tear and Mat immediately wants to go out on the hunt.
- Thom is coughing ominously.
- They try methodically searching every inn but then, when getting tired, Mat
  wanders into one at random and finds Comar.
- Mat challenges and kills him but before he dies Comar tells him there are
  more assassins out there.
- He thinks his luck works best for random things - so his methodical searching
  was unlucky but his random choice of inn was successful.
- He spends the next three days looking for the women but without success.

## Chapter 50: The Hammer

- Perrin, Loail, Moiraine and Lan arrive in Tear.
- They notice the faces of the residents carry a defeated look.
- While wandering, Perrin encounters a forge and asks the blacksmith to do some
  work for him.
- At the end of the day, the blacksmith is so impressed and grateful that he
  gives Perrin his hammer to keep as payment.
- Perrin says that he is in the fight for now but he would like to be a
  blacksmith again.
- When he returns, Lan is waiting.
- Moiraine tells the group that the forsaken Be’lal is now a High Lord in Tear.
- She believes Be’lal's plan is to let Rand take the sort Callandor and then
  kill Rand with it.
- She asked Loail for information about him – he says there is little
  but that before he turned to the dark, he was one of the leaders in the fight
  against the Dark One.
- He is known to be envious (of the Dragon and Ishamael in particular) and a,
  "Weaver of nets."
- Moiraine tales the group that Be'lal could probably destroy them all from a
  mile away with barely a thought and so she must not use the One Power and give
  them away.
- However, she believes that balefire can destroy the forsaken and if she gets
  close enough without being noticed, she can do this; she has learned balefire
  in the previous year.
- She and Lan plan to sneak into the stone to attempt this.
- Lan has seen Aiel on the rooftops; Moiraine is concerned as the prophecies of
  the Dragon do not mention a role for the Aiel.

## Chapter 51: Bait for the Net

- The three women are captured by Liandrin and two others of the Black Ajah.
- Liandrin reveals that their master in Tear is Be’lal and that he has sent for
  13 Myrddraal.
- Nynaeve knows this means their plan is to turn them to the Shadow after they
  have been bait for Rand.

## Chapter 52: In Search of a Remedy

- Mat takes Thom to the house of the wise woman, which he saw the night they
  first arrived, as his cough has been getting worse.
- As they talk, he realises that she met Elayne, Egwene and Nynaeve.
- He learns he is three hours too late and that they have been taken to the
  Stone of Tear.
- With Thom too unwell to accompany him, he plans to get them out of there
  himself.

## Chapter 53: A Flow in the Spirit

- Moiraine tells Perrin Rand is in the city and that she and Lan will go into
  the stone tonight to try to deal with Be’lal.
- Perrin, Loail and Zarine must go back to the safety of Tar Valon.
- The Falcon consents to this, thinking it would be foolish to stay, and leaves
  the room.
- Following her, Perrin hears her fall but before entering the room she went in
  to is stopped by Moiraine.
- She has felt a 'flow in the spirit' and thinks there is a trap in the room.
- Perrin can see a hedgehog beside the motionless body of Zarine.
- Moiraine eventually remembers it as a _ter'angreal_ and thinks it has
  something to do with dreams and that Zarine may be wholly trapped within the
  dream world.
- She fears that if she is not rescued, she will be trapped and her physical
  body will die.
- However, her business in the stone is more pressing.
- Perrin says he will stay with her but after Moiraine leaves, he asks the loyal
  Aulgur to stand guard well he enters one of his wolf games and asks Hopper for
  help.
- Hopper says Perrin may be too strongly embracing the wolf but agrees to help
  him search for Zarine.

## Chapter 54: Into the Stone

- Mat is on the rooftops surveying the stone, planning to climb it; he even
  thinks he saw Rand climbing it already.
- He is confronted by Julian and also by a group of Aiel.
- The Aiel state they have their own business with the stone and decide not
  to stop Mat.
- While talking with him, Julian realises Mat seeks the three women he helped
  capture and repents, offering to help him.
- Mat accepts Julian's offer of help and goes to set his diversion – he is
  going to let off all the fireworks after jamming them into an arrow slit in
  the stone.
- He suddenly worries that Julian might betray him and after the fireworks go
  off, increasing the size of the arrow slit, he impulsively decides to go
  through the gap in the wall.
- He is forced to fight his way past numerous guards and is just about to be
  overcome when Julian arrives to help him.
- Rand has indeed scaled the wall and is preparing to enter the heart of the
  stone; he hears the alarms go off after Mat's explosion.
- Perrin and Hopper are searching for Zarine in his wolf dream.
- Perrin finds and frees her but she vanishes into nothingness.
- Hopper tells Perrin that in the wolf dream, sometimes the hunt has to be
  performed many times.
- Still feeling helplessly cut cut off from the One Power in their cell, the
  three women wonder what their next move can be.
- Egwene realises she still has her _ter’angreal_, which does not need the One
  Power to work.
- The black Adjar were so contemptuous of their captives that they didn't even
  bother to search them.
- During their trial to become accepted, Nynaeve has learned that you can use
  the One Power in _tel'aran'rhiod_ and Egwene wonders if this might be a way
  out for them.
- Egwene enters _tel'aran'rhiod_ and manages to shield the Black Ajah Joiya who
  is in the Heart of the Stone.

## Chapter 55: What Is Written in Prophecy

- Perrin has rescued Zarine a number of times but she always disappears.
- Hopper warns him that he may become trapped as he is too strongly in the wolf
  dream.
- He is determined to press on and enters a room where hundreds of Falcons
  protect a chained Zarine.
- As he battles towards her, they rip his flesh but he will not be stopped.
- He finally frees her and she does not disappear.
- When he wakens, the broken hedgehog _ter’angreal_ is beside him and the
  Falcon is looking after him, tending his wounds.
- They are both gravely injured.
- Egwene finds the Black Ajah Amico outside her cell and manages to cut her off
  from the One Power, much to Amico's surprise.
- She cannot think of her next move and awakens to tell Nynaeve and Elayne what
  she has managed.
- Mat comes across one of the Black Ajah outside the women's cell.
- He finds her fixed in place by the One Power Egwene used on her in the dream
  world.
- He opens the cell to release the women but instead of finding the hero's
  welcome he expected, the women are simply determined to go after Liandrin and
  the rest of the Black Ajah.
- Nynaeve heals them all.
- Rand enters the heart of the stone and is confronted by Be’lal, who says he
  will kill Rand and the only chance Rand has is if he takes up the sword.
- Rand will not be tempted – he has used the title of Dragon Reborn but has not
  accepted it for himself and does not believe he will or perhaps even can hold
  the sword.
- Instead, he fights Be’lal, both armed with swords created by manipulating the
  One Power.
- However, Be’lal is a true sword master whereas Rand has merely learned what he
  could from Lan and is completely outmatched.
- Be’lal decides he will just kill Rand but in that moment, Moiraine comes in.
- Be’lal regards her contemptuously but his contempt turns to panic and then
  fear when she makes to turn balefire on him.
- To slow to react to the danger in front of him, Be’lal is destroyed.
- Moiraine is telling Rand that Callandor is his birthright when she is consumed
  by black lightning and cast down.
- A black figure descends from the high places of the heart and takes the form
  Rand recognises as BLZ Bob.
- BLZ Bob decides he will not try to tempt Rand this time but simply kill him.
- Feeling he has no other choice, Rand reaches for the sword while being struck
  by Ba’alzamon.
- As he feels his soul being torn from him, there is a sudden release and Rand
  finds himself holding the hilt of Callandor.
- Rand says he means to kill Ba'alzamon once and for all and Ba’alzamon quickly
  retreats, bending reality and disappearing.
- Acting on instinct alone, Rand uses the power of the _sa’angreal_ in his hand
  to bend reality and follow him.
- Ba’alzamon unleashes furious powers and enemies against the chasing Rand, all
  of whom Rand instinctively dismisses with little effort.
- Though he feels somewhat invincible while holding the _sa’angreal_, he also
  feels like he is balancing on the edge of a knife by wielding this much of the
  one power.
- Eventually, BLZ Bob stops running and turns on Rand with balefire; the sword
  Callandor deflects it.
- Eventually, Rand attacks and stabs BLZ Bob with Callandor.
- He returns to be true heart of the stone to see a man in front of him with a
  hole in his chest and smoke coming from his eye sockets.
- He declares himself the Dragon Reborn.

## Chapter 56: The People of the Dragon

- Mat is happy and thinks their work is done.
- Patiently, Moiraine explains that there are still forsaken out there and that
  there is no way this truly was the Dark One.
- Egwene wonders whether it may actually have been Ishamael and remembers a
  piece of paper that Verin gave her talking about a man behind a man, meaning
  behind meaning.
- Moiraine also produces another one of the seals of the Dark One's prison,
  although this one is intact compared to the others which were broken.
- Egwene speaks again, saying she is not surprised to see it as there has been a
  seal present the previous times Rand has faced off with Ba’alzamon.
- A third of the Aiel force were killed in the fighting but for each of
  their number, 10 Defenders of the Stone have been killed or captured.
- The Dragon banner has been raised above the Stone of Tear.
- Mat ponders the prophecies – who are the people of the Dragon?
- The Aiel leader says that all those who wish to ascend to places of
  importance within the Aiel are Randed and the Randing carries the same
  pattern as the Dragon banner.
- Moiraine declares that the Aiel are the People of the Dragon.
- Berelain enters to deliver a message, talking with surprise as she is
  not used to delivering messages but the person who gave her it was hard to
  resist.
- The message is from Lanfear – she says she will give the Dragon to these
  people to keep him safe until she is ready to have him back again.
- Moiraine asks Mat again if he is sure that they have finished with this and
  though he says he is willing to carry on the fight, in his mind he is hoping
  he and Thom can make an escape.

> *"And it was written that no hand but his should wield the Sword held in the*
> *Stone, but he did draw it out, like fire in his hand, and his glory did burn*
> *the world. Thus did it begin. Thus do we sing his Rebirth. Thus do we sing*
> *the beginning."*
> from Do'in Toldara te, Songs of the Last Age, Quarto Nine: The Legend of the
> Dragon. Composed by Boanne, Songmistress at Taralan, the Fourth Age


[Home](/blog "ohthreefive")