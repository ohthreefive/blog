+++

date = "2019-10-13T20:33:17.552Z"
title = "Summaries: The Eye of the World"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Eye of the World

**Robert Jordan**

## Prologue: Dragonmount

- The Lord of the Morning is challenged by the Betrayer of Hope.
- They both have other names, Lews Therin Telamon and Elan Morin Tedronai.
- The Betrayer of Hope says he is there to kill the Lord of the Morning to prove
  he is superior.
- He refers to them both as servants and says that the Lord of the Morning
  previously humiliated him in the House of Servants and this is his revenge.
- The Betrayer of Hope also calls the Lord of the Morning 'Dragon'.
- He says that all of this is his fault because he tried to fight his master,
  the Lord of the Dark.
- The scene around them was suddenly visible to Lews Therin Telamon, and he sees
  the both his wife and children are dead.
- It appears that Dragon did this.
- The Betrayer of Hope tells him you will be known as a kin slayer.
- The Lord of the Morning teleports away and draws a tremendous amount of
  strength from a source of power in the universe which he calls the Light,
  raising a miles-high mountain before disappearing, presumably dead.
- The Betrayer sees the mountain and says that the Dragon will not escape so
  easily.

> *"And the shadow fell upon the land and the world was riven stone from stone.*
> *The oceans fled and the mountains were swallowed up and the nations were*
> *scattered to the eight corners of the world. The moon was as blood and the*
> *sun was as ashes. The seas boiled and the living envied the dead. All was*
> *shattered and all but memory lost and one memory above all others, of him*
> *who brought the Shadow and the breaking of the world, and him they named
> Dragon."* From Aleth nin Taerin alta Camora, The Breaking of the World, author
> unknown, the Fourth Age

> *"And it came to pass in those days as it had come before and would come*
> *again that the Dark lay heavy on the land and weighed down the hearts of men*
> *and the green things failed and hope died and men cried out to the Creator*
> *saying, 'Oh, Light of the heavens, light of the world, let the promised one*
> *be born of the mountain, according to the prophecies, as he was in ages past*
> *and will be in ages to come. Let the Prince of the Morning sing to the land*
> *that green things will grow and the valleys give forth lambs. Let the arm of*
> *the Lord of the Dawn shelter us from the Dark and the great Sword of Justice*
> *defend us. Let the Dragon ride again on the winds of time.'"* From Charal
> Drianaan to Calamon, The Cycle of the Dragon, author unknown, the Fourth Age

## Chapter 1: An Empty Road

- A boy named Rand and his father travel to a village, Edmond's Field.
- On the way, Rand sees a rider in black and is immediately afraid.
- His father does not see this rider, who has disappeared.
- Rand's fried, Mat, has also seen the rider.

## Chapter 2: Strangers

- Mat tells Rand he saw two strangers in town, a woman and a man with a colour
  shifting cloak.
- Their other friend Perrin identifies the woman as Lady Moiraine.
- As they are talking, the feel they are being watched by a raven; it makes them
  uncomfortable.
- Moiraine comes among them, telling them she is a collector of old stories, and
  while she is in town she may have small jobs for them.
- She gives them money.

## Chapter 3: The Peddler

- A man called the Peddler arrives in town and tells the locals that the
  troubles they are having are happening all over.
- A man claiming to be the Dragon reborn is attracting followers, and enemies.
- This is the first Dragon reborn claimant who can wield 'the One Power.'
- There is debate about the Dragon, with one youngster confusing him and the
  Dark Lord.
- An order of women called the Aes Sedai have allegedly been dispatched to kill
  this False Dragon.
- There is equal doubt and confusion about the Aes Sedai.
- Mat, Rand and Perrin discuss things, interrupted by Rand's love interest
  Egwene.
- Perrin also claims to have seen the black rider.

## Chapter 4: The Gleeman

- The boys encounter the Gleeman, who tells them all about the tales he knows
  and will tell at the festival.
- He comments on how uncommonly large Rand is and how his eyes are a different
  colour to the majority of people from his village.
- Moiraine arrives and there is some tension between them.
- Afterwards, the boys resolve to tell the mayor about the cloaked black rider
  they saw.
- The village council are putting patrols out to both look for people who need
  their help because of the war and people who we seek to profit from it.
- Rand's father reveals that he believes him about the Black Rider, in
  particular because there is a fourth witness.
- All four who have seen the black rider so far are boys.

## Chapter 5: Winternight

- Rand and his father are working on the farm – Rand notices that for the first
  time, his father locks the back door.
- He also produces a rather ornate sword which Rand has never seen before.
- Out of the blue, they are attacked by beast-like creatures called Trollocs.
- Thanks to Tam, they both managed to escape, though Tam is injured.
- Seeking supplies, Rand returns to the farm where he encounters and manages to
  kill a waiting Trolloc with his father's sword.
- The Trolloc told him that he was to be questioned by a Myrddraal, which Rand
  knows as a Fade and thinks is a twenty foot tall monster with burning eyes.
- He returns to find his father profoundly feverish and resolves to make it back
  to the village, where he believes everything will be made right.

## Chapter 6: The Westwood

- Rand struggles towards the village and ducks off the road just-in-time.
- He sees a group of Trollocs being led by the rider in black.
- As always, this rider's cloak is unmoving in the wind.
- Rand manages to hide from the rider.
- In his fever, his father seems to imply that Rand is an orphan, found by his
  adopted father as a newborn on Dragonmount.
- In another of his feverish outbursts, he blames King Lamen for all the
  problems in the world as he cut down Avendesora (the tree of life?) an act
  known as Avendoraldera.

## Chapter 7: Out of the Woods

- Rand returns to Emond's Field to discover it has also been attacked.
- He takes his father to the healer, known as the Wisdom, who says there is
  nothing she can do.
- Next he visits the mayor, and while doing so sees that the local inn has had a
  mark drawn on it known as the Dragon's Fang.
- While speaking to the mayor, he learns that the village was saved by the two
  strangers who had recently arrived.
- Mistress Moiraine is an Aes Sedai, and attacked the Trollocs with ball
  lightning.
- The man named Lan is her Warder and slew many with his sword as well as
  rallying the village to defend itself.
- Rand asks Moiraine for her help and she agrees to try.
- Moiraine and Lan both comment that Moiraine has been interested in Rand and is
  glad he's alive; Rand has no explanation for this.

## Chapter 8: A Place of Safety

- Moiraine sets about healing Tam; Trolloc blades leave a taint.
- Moiraine uses a special kind of totem in her healing called an angreal.
- She and Lan talk to Rand about the attack.
- Lan comments that the sword Rand has belongs to a blade master and he wonders
  how it ended up in this village.
- Moiraine and Lan are surprised and impressed that Rand both spoke to and
  killed a Trolloc on his own.
- Lan confirms that the black rider was a Myrddraal, and Rand’s belief that
  they were 20 foot tall was just a story.
- He says they are the spawn of Trollocs, and act as the brains of groups of
  Trollocs in war etc.
- Despite trying, both he and Moiraine were unable to kill this Myrddraal.
- Moiraine explains that the Trollocs were after boys of a certain age close to
  Rand, including Mat and Perrin.
- She explains that Rand must leave with them for his own safety and go to Tar
  Valon.

## Chapter 9: Tellings of the Wheel

- Rand dreams that he is escaping from Trollocs and in the distance he sees a
  massive black mountain, presumably the Dragonmount.
- He feels inexorably drawn to this Mountain but at the same time wants to
  resist its calls to him.
- He sees a figure wearing a cloak the colour of dried blood.
- He finds himself in an amazing city with a massive white tower to which he
  knows he must go.
- He tries to resist this tower but sees it at the end of every street he turns
  down.
- Finally, he finds himself face-to-face with a Myrddraal who says, "We have
  been waiting for you."
- He awakes to find that his father is also awake.
- He does not confront him about possibly not being his son.
- He is surprised when Tam completely agrees that Rand must leave Emond's field.
- This is despite Tam saying it makes no sense that the Dark One was after these
  three young boys.
- Tam tales Rand to be wary of Lan, as a Warder is only loyal to his Aes Sedai
  and so nothing between Rand and Lan will ever be kept from Moiraine.
- Lan comes in and tells Rand that there is trouble outside and they must
  prepare to leave.
- Outside, a group has gathered and is demanding that Moiraine leave as it must
  have been her who brought the Trollocs.
- Initially, the mayor berates them and reminds them of all the healing she has
  done.
- With a display of magic, Moiraine silences the group and then tells them the
  history of their village: it was the last stand of a King Aemon in a heroic
  defence of his land during the Trolloc War.
- At his death, his queen drew on so much of the One Power that she was able to
  kill all the generals of the Trolloc army, which then disbanded.
- The effort consumed her, however.
- Though the proud city and kingdom was never raised again, its memory lived on.
- Hearing the story and embarrassed that they have let down their ancestors, the
  people of Emond's field leave.
- Lan and Rand prepare to leave as well.

## Chapter 10: Leavetaking

- Mat and Perrin prepare to leave with Moira, Rand and Lando.
- Perrin, the blacksmith's apprentice, takes a huge axe with him.
- Mat carries a bow.
- Their preparations to leave have been discovered by Egwene, who insists on
  joining them.
- Similarly, Thom Merrilin, the gleeman, also insist on going with them.
- We learn something new about Trollocs – wolves hate them and so the presence
  of wolves is almost certainly reassuring.
- Early in their flight, Rand sees a massive bird-like beast, known as a
  Draghkar, and a servant of the Dark One.

## Chapter 11: The Road to Taren Ferry

- On their way to the river crossing, the group stop to rest.
- Rand has been trying to push Egwene's horse, Bela, to keep up.
- Moiraine uses magic to soothe their horses tired legs - she finds Bela to be
  the least tired.
- Lan tells Rand she will do the same for the boys but cannot do this to
  herself.
- Before she has the chance, the Draghkar attacks and the group make a hasty
  escape.
- Moiraine draws a mist around them allowing them to travel to the ferry.
- When they make it there, Lan gives the ferry man gold in order to make the
  journey immediately.

## Chapter 12: Across the Taren

- The group cross the river.
- On the other side, the boat is unexpectedly pulled into a whirlpool,
  Moiraine's doing.
- She has also spread the fog down the river, calculating the coverage to
  maximally confuse their pursuers, something she says most Aes Sedai could not
  do.
- They stop briefly, and Moiraine tells Egwene she has a rare gift, being
  able to channel the One Power instinctively, which may make her an ideal Aes
  Sedai, in fact her survival chances would be improved by training.
- Rand hopes she is wrong but Egwene appears to confirm during a simple test: she
  makes light appear in Moiraine's kesiera.
- Moiraine also explains a little about the One Power.
- It comes from the True Source, which is the driving force of creation, made by
  the Creator to turn the Wheel of Time.
- The True Source has a male half (saidin) and female half (saidar.)
- Saidin is tainted by the Dark One.

## Chapter 13: Choices

- The group continue their travels, untroubled by Trollocs.
  Lan teaches the boys how to use their weapons.
- Moiraine teaches Egwene lessons required to become an Aes Sedai.
- She comments that there was another in the village who wanted to become an Aes
  Sedai, and unusual number of recruits for a village so small and indicating to
  Moiraine that the strength of the old world remains in the two rivers.
- She also tells the three boys that danger is not over, the Dark one still
  wants them and she will kill them herself rather than let him have them.
- When they reach the city of Baerlon, the village people are shocked as they
  have never seen a city before.
- Moiraine tells them that they are not hugely welcome here and so she and Lan
  go by the names of Alys and Andra, respectively.
- She tells the villagers to be on their guard as there may be what she calls
  Darkfriends in the city, presumably followers of the Dark One.
- The gatekeeper knows Alys and Andra well and agrees to let them in in secret,
  just as he had let them out.
- He tells them that a group known as the Children of the Light are in the city,
  zealots who wish to destroy the Dragon Reborn.
- The Dragon Reborn has named his followers the Dragon's People.
- The gatekeeper does not trust the Children of the Light as he says there is no
  trouble in this city so they should be elsewhere where the Dragon reborn is
  reported to be - Ghealdan.
- The gatekeeper has also heard that this Dragon Reborn can wield the One Power,
  making him different from previous ones.
- Thom tells Rand a prophecy that a port city known as Tear, the greatest port
  in the land, has a castle called the Stone of Tear, the greatest castle built
  since the breaking of the world and one which has never fallen.
- One prophecy says that the Stone will never fall other than to the Dragon
  Reborn and the Dragon's People, another that the Stone will not fall until a
  famous sword is wielded.
- Rand thinks these prophecies are strange as this sword is kept within the
  Stone and therefore their requirements to fulfil the prophecy is are
  paradoxical, unless someone from Tear wields the stone.
- Thom says this is unlikely as the people of Tear like the Dragon even less
  than the people of Amador, the city where the Children of the Light come from.
- The group settle in an inn, again, Alys is well known to the owner.

## Chapter 14: The Stag and The Lion

- The group get some rest at the inn.
- Moiraine asks the inn-keeper about a woman called Min.
- We learn that this Dragon Reborn's name is Logain.
- Lan has to remind them to keep quiet about their adventure – in particular Mat
  nearly tells someone about their encounters with Trollocs.
- Rand has another dream: he conversers with the Dark One who identifies
  himself as Ba'alzamon.
- Initially, he asked Rand if he is the one, implying that he is searching for
  the Dragon Reborn or similar.
- Rand is able to resist some of his words and wishes and this convinces
  Ba'alzamon that he may well be the Dragon Reborn.
- He tells Rand that he has no chance against him as it was him who whispered in
  the Dragon's ear, telling him to kill his wife, family and all who loved him
  and also he who gave him the moment of clarity to see what he done, sending
  him mad.
- He has whispered in the ears of many powerful people for millennia to further
  his aims.
- He tells Rand that the Aes Sedai will use him, just as they have used all the
  previous Dragons Reborn including the current one, Logain.
- Mockingly, Ba'alzamon says that maybe Rand should tell the Aes Sedai about his
  dreams as that would mean they are unlikely to try to use him, albeit makes it
  equally likely that they will kill him.

## Chapter 15: Strangers and Friends

- Rand wakes to find everybody gone.
- He goes downstairs and the innkeeper gives him some food and says one of his
  companions stayed behind as they weren't feeling well.
- Guests at the inn have complained about the dead rats, all of which have had
  their back broken.
- In his dream, Ba'alzamon broke the back of a rat in front of Rand.
- Hearing this, he immediately goes to find someone to talk to, finding Perrin
  to be the one who stayed behind.
- Perrin had a similar dream and is convinced Mat had the same one, though he
  did not admit it.
- Rand goes out to see the city, keen to find Thom to speak to him about his
  dreams.
- The first person he speaks to, however, is Min.
- She knows both the name and nature of Moiraine.
- She claims Moiraine has confided in her as she is able to see some things in
  the Pattern.
- She says she knows Rand loves Egwene and Egwene loves Rand but that they will
  not be together in that way.
- She says that when she looks at the group of travellers, she knows that they
  will all play a significant part in something dangerous.
- She then make some specific, essentially prophecies, about them:

- She says she sees the same in Egwene as she sees in 'Mistress Alys'

> "Master Andra has seven ruined towers around his head and a babe in a cradle
> holding a sword"

> "The strongest images around the gleeman are a man, not him, juggling fire,
> and the white tower, and that doesn't make any sense at all for a man."

> "The strongest things I see around the big curly haired fellow are a wolf and
> a broken crown and trees flowering all around him" (Perrin)

> "And the other one: a red eagle, an eye on a balance scale, a dagger with a
> ruby, a horn and a laughing face" (Mat)

> "A sword that isn't a sword.
> A golden crown of laurel leaves.
> A beggars staff.
> You pouring water on sand.
> A bloody hand and a white-hot iron.
> Three women standing over a funeral bier with you on it.
> Black rock wet with blood.
> Most of all, I see lightning around you. Some striking at you, some coming out
> of you. I don't know what any of it means, except for one thing: you and I
> will meet again." (Rand)

- Rand surprisingly bumps into the Peddler in the streets, who was assumed
  killed back at Emond's field.
- He is edgy, particularly when talk turns to Moiraine.
- Rand probably tells him too much, including where they are staying and for how
  long.
- The Peddler makes him promise not to tell Moiraine about their meeting.
- He then bumps into Mat, who confirms he had the same dream, and also wants to
  keep it from Moiraine for fear of what Ba'alzamon said she might do.
- Mat and Rand encounters some Children of the Light and Mat plays a joke on
  them, muddying their white cloaks.
- They confront Rand, who attempts to cover up the hilt of his sword but
  instead, instinctively, throws his cloak over his shoulder and stares the
  three of them down.
- He is filled with a powerful feeling while doing this, though inside does not
  know why he is doing it.
- The Children back away after the Town Watch arrive.
- Mat and Rand speak to Thom, who is troubled by their dreams, noting that many
  of the names the Dark One gave were historical figures of infamy.
- He says they probably should not tell Moiraine for now and should return to
  the inn to ensure Perrin does not tell someone about the dreams.
- They encounter Perrin hurriedly leaving the inn, looking for them.
- The village wisdom, Nynaeve, has arrived looking for them.

## Chapter 16: The Wisdom

- Before Rand goes in to see Nynaeve, he is intercepted by Min, who says that on
  first seeing Nynaeve, she realised she is part of the same trouble which
  encompasses the rest of the group.
- Rand is very surprised by this.
- The whole group meet, and Nynaeve explains that all the Emond's Fielders must
  return with her.
- The village council, primarily because they were mistrustful of Moiraine,
  decided they must have their people back.
- Moiraine and Lan argue against this, as do some of the Emond's Fielders,
  though they are a little stifled in their arguments out of their respect for
  the Wisdom.
- After the meeting, Moiraine and Nynaeve speak privately with Moiraine saying
  she could provide more safety, along with Lan, for the children than anyone
  else.
- Nynaeve lets slip that Moiraine was particularly interested to know if anyone
  was born outside of Emond's Field.
- Rand confesses he heard his father say that he was not his father.
- Nynaeve denies that was true but does tell him that he was born from outside
  Emond's Field.
- Rand asks if Nynaeve is planning to join them; something she finds ridiculous.
- He also correctly guesses that the village council was not a smooth thing –
  Nynaeve confesses that the men were so disorganised that they had still not
  chosen who should go after the children when she left.
- She comments that Rand has grown in stature in the short time since he left
  the Two Rivers.

## Chapter 17: Watchers and Hunters

- The group enjoy a merry evening at the inn, with only one man, with a scarred
  face, giving them any concern at the inn.
- Before going to bed, Rand is confronted by a Myrddraal.
- He says he will kill him if he does not give his friends.
- His face is eyeless which terrifies Rand.
- He raises his sword to kill Rand but then says that Rand belongs to the Dark
  One and hurries away.
- Lan arrived shortly afterwards and appears to be able to sense the Myrddraal's
  retreat, declaring he has probably got too far for him to chase him but now
  they must all leave immediately.
- At the city gate, some Children of the Light confront them.
- Moiraine grows in stature until she is taller than the city walls and easily
  dismisses the five men.
- The group make a hasty escape, and after a certain distance notice that there
  is burning coming from the city of Baerlon.
- Moiraine is certain that what they are seeing is the inn burning.
- Nynaeve scolds her for callously allowing other peoples lives to be ruined on
  her account.
- Lan angrily interject that it is the boys who are the reason for the trouble
  and that anything the Dark Lord wants so much must be kept from him.
- Moiraine explains the various ways in which she could help which may either
  draw attention to her or those she helps, resulting in more trouble.
- She confronts Nynaeve, asking what she would do in her place.
- Nynaeve fumbles her answer.
- Moiraine says she will provide enough gold to help those affected by the fire
  but that even this could have risks.

## Chapter 18: The Caemyln Road

- The group continue their flight but horns herald their pursuers.
- Lan goes back to scout and returns reporting up to 5 groups of 100 Trollocs
  each lead by a Myrddraal.
- Lan takes the boys to launch an attack on one group.
- He successfully beheads the Myrddraal, causing all Trollocs under
  his command to writhe about in agony.
- They retreat and regroup but are soon charged by three more groups of
  Trollocs.
- Moiraine intervenes, firstly starting an earthquake directed at the charging
  Trollocs then raising a wall of fire in between them.
- Again they retreat.
- Moiraine reveals that the boys' instinctive battle cries were actually those
  of their ancestors in Manetheren, in particular that of Mat.
- Shamelessly, both Rand and Perrin secretly hope this means that Mat is the
  true target of the Dark One.
- To aid their escape, Moiraine sends a false trail in a different direction.
- Subsequently, they come across a massive abandoned city, where they intend to
  get some rest.
- Moiraine reveals its name to have been Aridhol, but renamed Shadar Logoth
  after the 'death' of the city.
- She had previously not wanted to take refuge there.

## Chapter 19: Shadow's Waiting

- Nynaeve argues with Lan that Moiraine is too tired: she can be of more help to
  her than him.
- Moiraine interjects to explain that Nynaeve does not realise she only needs
  about an hour of rest but does admit that her herbs and healing will be useful
  to her.
- Mat convinces Rand and Perrin that they should go exploring – this city is on
  a completely different scale to Baerlon and Moiraine said it was safe as
  Trollocs would not come in.
- In the shadows, they encounter a man called Mordeth.
- Mordeth leads them under the city to a cellar of sorts in which he has
  amassed a horde of treasure.
- He swells in size suddenly and attacks them but they managed to escape, with
  Mordeth saying that they are all dead man.
- As they make their way back to the camp, feeling like they are being watched
  from every shadow.
- Moiraine explains that Mordeth is centuries old and originally came to Aridhol
  as an advisor to the King, who he poisoned with his words.
- A great evil rose up within the city, Mashadar, and destroyed it; it was
  renamed Shadar Logoth.
- The evil which consumed the city left only Mordeth alive.
- During the Trolloc Wars, a huge band of Trollocs made camp in the city and
  were never heard of again, with bloody remains being found in the city and the
  day after then disappearing.
- This is why the Trollocs fear to go in there.
- Moiraine says the boys were only safe when they were with her as she has set
  wards around their camp.
- Mordeth yearns to ensnare a victim, capture his body and wear it to escape
  the city.
- Lan returns after scouting for a longer time than expected to tell the group
  that there are Trollocs within the city.
- It has taken four Myrddraals to force them into the city and they are so
  terrified that they are heading straight through without searching thoroughly.
- However, as they are heading directly towards the camp, the travellers will
  have to leave.
- Moiraine's concern is that if the Myrddraals drove the Trollocs into the city,
  who drove the Myrddraals in?
- Nynaeve is exclaims that perhaps it was the Dark One, but this is impossible
  because he is bound in Shayol Ghul by the Creator.
- To this, Moiraine replies, "For now."
- She says instead they must make as quickly as possible for the river crossing,
  as both trollocs and Myrddraals hate water.

## Chapter 20: Dust on the Wind

- Trying hard to stay quiet, the Emond's fielders and Thom fall behind Lan and
  Moiraine.
- During their escape, they come upon the Mashadar, which separates the groups.
- Moiraine explains that the full power of the White Tower probably could not
  destroy it and for her to punch a hole through it, she would draw the
  attention of all their pursuers.
- She tells them to follow a certain star, aiming for the river, but being sure
  never to touch the Mashadar.
- During their flight, Trollocs attack, some of which, including a Myrddraal,
  are taken by the Mashadar.
- When Rand finally escapes the city, he is on the run with Mat and Thom.
- They come across and board a ship, waking the sleeping watchman and telling
  the crew to make sail as quickly as possible.
- When the Trollocs attack, Rand's life is saved when a boom inexplicably comes
  loose and strikes a Trolloc, knocking it overboard.
- By the time the fight is won, the captain informs them they are already three
  or four miles down the river.
- Egwene and Perrin left the city together, but in their haste rode their horses
  off a cliff in to the river.
- Perrin successfully reached the other side.
- The fate of Egwene unknown to them, Thom tries to sure Rand that there was
  nothing more he could have done for her and that she and the others will be
  safe with Moiraine and Lan.
- They head down river on the ship, giving the captain their silver coins as
  payment.

## Chapter 21: Listen to the Wind

- Nynaeve awakes alone, chastising herself for failing to stay awake on her
  first night alone.
- She escaped the Trollocs, partly in her opinion because they did not appear to
  be chasing her but turned after the boys.
- She smells a fire and sneaks up on it, finding Moiraine and Lan.
- She overhears Lan saying there were over 1000 Trollocs on their tail, and he
  and Moiraine are concerned that so many can be afoot without apparently being
  seen.
- Moiraine and Lan are concerned that with this many Trollocs, they may be
  witnessing the beginning of more wars.
- Moiraine casually looks to Nynaeve’s hiding place and beckons her out.
- She explains that although not trained, Nynaeve can wield the One Power.
- Nynaeve tries to argue, trying to separate herself from the Aes Sedai.
- Moiraine explains that it is impressive that Nynaeve has learnt to control the
  power as it may well have killed her.
- She says that it is in Egwene's interests to properly learn to control the
  power, for her own safety.
- Nynaeve remains mistrustful, even when Moiraine explains that she thinks
  Nynaeve may be capable of greater power than Egwene with training and that
  both could be among the most powerful Aes Sedai for generations.
- Moiraine confesses that the coins she gave to the boys allow her to track
  them, which again parents earns her suspicion from Nynaeve.
- One boy is across the river, the other two are fading from her, as if the bond
  between them has been broken.
- She has no way to track Egwene.
- The three head off in pursuit.

## Chapter 22: A Path Chosen

- Perrin decides that, as he is a better swimmer than Egwene, he will be best to
  look for her downriver.
- He assumes she has made it across.
- He finds her quickly, and finds that she made it across in better shape than
  him – with her horse and bags still with her.
- They decide to head straight for Caemlyn.
- They believe that this slightly unusual decision will throw off pursuers
  decision will throw of pursuers and that Moiraine has the skills to find them
  in Caemlyn just as easily as at White Bridge (where they were meant to go.)

## Chapter 23: Wolfbrother

- Perrin and Egwene encounter a man dressed in animal hide who introduces
  himself as Elyas.
- This name kindles a memory of a story in Perrin's mind.
- Elyas tells them they have been very noisy and he and his friends have been
  aware of them for at least the last two days.
- It turns out these friends are wolves, and Elyas and his pack of wolves share
  the same yellow eyes.
- When they finally confess their story to him, Elyas tells them they are way
  off track for Caemlyn.
- He says he has an ability to communicate with wolves which is not linked to or
  given by the One Power.
- In fact, it is much older than the One Power and the Aes Sedais are unhappy
  about it.
- They believe that the return of this ancient power is a sign of the weakening
  of some ancient bonds and that this may be portentous.
- The wolves have told Elyas that Perrin also has the ability to communicate
  with them.
- Despite feeling thoughts consistent with this, Perrin is strong in his
  denials.
- When his offer to look after the two himself is rejected, Elyas concludes that
  he and the pack must help lead them to Caemlyn.
- Only one of the wolves rebels, preferring to go and hunt Trollocs.

## Chapter 24: Flight Down the Arinelle

- The chapter begins in one of Rand's dreams.
- He is navigating a maze, being pursued by Ba'alzamon.
- While hiding, he pricks his finger or a thorn.
- Eventually, he comes face-to-face with the Dark One who tells him that his
  plight is hopeless and never control the Eye of the World.
- Rand is only able to escape by declaring that this is a dream.
- He has one final vision of him and Ba'alzamon sharing the same face.
- When Rand awakes, his fingertips is bleeding.
- Mat, Rand and Thom continue on the boat.
- The man who was on Watch the night they came aboard, named Gelb, is highly
  suspicious of them however he lacks the trust of the crew.
- Mat has been quiet since they left the shadowland.
- His interest is piqued when they sail close to a massive solid looking steel
  tower, which the captain says has been there for longer than anyone can
  remember.
- Thom teaches the boys some gleeman tricks, mostly to continue the ruse that
  they are his trainees.
- Rand subconsciously performs some impressive acrobatics, drawing an audience
  from the crew.
- In this audience is Mat and Rand noticed that he carries an ornate dagger.
- He confronts him, learning that Mat did take this from Mordeth.
- Mat thinks it will be fine as it was not given to him but taken and asks that
  Rand tells no one.
- Rand agrees then confirms that Mat has also had dreams of the Dark One.
- Thom confesses that although their ultimate destination is still planned to be
  Tar Valon, he may take them somewhere else - things change.

## Chapter 25: The Travelling People

- Perrin, Egwene and Elyas continue their travels, eating well thanks to
  Elyas's skill at hunting.
- Perrin has stopped dreaming of Ba'alzamon but now in all of his dreams, there
  is a wolf.
- He still largely denies any ability to communicate with them.
- They come across three large mastiffs.
- Elyas tells them this means there are travelling people - Tuatha'an -
  nearby.
- Egwene believes travelling people, or Tinkers as she and Perrin know them, are
  thieves, best avoided.
- Elyas says that though he would not consider them his friends, they are an
  honest people, or at least no more dishonest than any other people
- The three go to meet the travelling people; the wolves stay behind.
- Elyas explains that the reason for their travels is that the people seek a
  song, a song that was lost in the world was broken.
- None of the travelling people now know/ remember the song, but if they find
  it, paradise will return.
- The grandson of the chief of the travellers, a handsome boy, takes an interest
  in Egwene.
- The boy explains about the Way of the Leaf, a peaceful philosophy followed by
  the travelling people.
- He and Egwene go to eat and dance, leaving Perrin a little bereft.
- Elyas and the travelling chief talk and the chief tells a story of a band
  of Tinkers who were crossing the Aiel Waste - some people can manage to cross
  the waste.
- The Aielmen avoid the Tinkers.
- Two years ago, some Tinkers encountered a group of Aiel women, Maidens of the
  Spear, who were entering the Great Waste.
- Only one returned, and she overcame her aversion to the Tinkers, using her
  last words to tell a band that:

> *"Leafblighter means to blind the Eye of the World, Lost One. He means to*
> *slay the Great Serpent. Warn the People, Lost One. Sightburner comes. Tell*
> *them to stand ready for He Who Comes With the Dawn."*

- The rest of her group were found dead, as were Trollocs, a surprise to Elyas,
  as Trollocs are said to fear the Waste, calling it the Killing Grounds.
- Perrin recognises the Eye of the World as it had been in his dreams.
- The Dark One had told rand that he would strangle him with the corpse of the
  great serpent in his previous dream in the maze.
- None of the people can make sense of this story but note that it is
  foreboding.
- Egwene returns, and tearfully asks Perrin if he thinks the others are alive.

## Chapter 26: Whitebridge

- Rand says that Mat has not been acting himself for days.
- Mat says it is because he has been become fed up of being chased.
- Mat remarks that he is surprised that Rand is not acting the same.
- Thom gently advises the boys that they may not ever find the others again and
  so they may well end up benefiting from the training he has given them.
- Mat and Rand disagree about this statement.
- They reach across the massive ancient structure that gives the town its name.
- It looks like it has been cut from a single piece of glass.
- The captain says it never gets slick, even when wet, and no chisel can mark
  it; it is a remnant from the Age of Legends.
- Thom and the captain reluctantly agree that it may have been built with Aes
  Sedai help.
- The captain of the ship also asks the group if they would like to stay with
  him – the gleeman has kept his crew in order despite the hard work he has put
  them through.
- He orders Gelb off his ship.
- He pays Thom for his services and says there will be more if he stays with
  them.
- Rand is determined to find their friends and so they head into the city, going
  in to an inn at Thom's suggestion to hear if there are stories of foreigners
  or strange travellers recently entering the city.
- The innkeeper tells them a story.
- The False Dragon has been captured, with the significant intervention of the
  Aes Sedai.
- He also tells of people from the city of Illian who are going around
  recruiting people for the Legendary Hunt for the Horn.
- The Horn (of Valere) is said to be able to call back the dead.
- They have been saying that the Final Battle (Tarmon Gai'don) is upon them.
- Thom tries to subtly ask if any of their companions have been seen.
- This startles the innkeeper.
- A man described as weasel-y, recently came in to ask about the same people,
  although he was most interested in the boys.
- This man's behaviour was odd and the people in the inn thought he was crazy.
- He left for Caemlyn.
- The following day and every day thereafter, someone else has come in to ask
  about the same people.
- From the innkeeper's description, this is clearly a Fade.
- On hearing this, Thom is keen to flee by river to Illian.
- However, Rand is determined to either stay in Whitebridge or make his own way
  via Caemlyn to Tar Valon.
- The argument is interrupted when the group hear that Gelb is talking about
  them at the other end of the inn.
- Quickly they decide they must escape.
- They do manage to get out of the inn, but come across a Myrddraal in the town
  square.
- The townspeople quickly disperse as the Fade begins to walk towards them.
- Thom, who has told a story about his nephew whose death he feels responsible
  for, gives Rand his cloak, gives him the name of an inn called the Queen's
  Blessing in Caemlyn, and tells the boys to run.
- In an instant, he turns to attack the Myrddraal, catching it off guard.
  Mat has to convince Rand to keep running; Rand wants to help Thom.
- They see a blue flash and hear Thom screaming, though he manages to shout on
  them to run one more time.

## Chapter 27: Shelter from the Storm

- The group continue to travel with the Tuatha'an.
- They are a pleasant group and good spirited company.
- Perrin wants to leave; Elyas says they should wait.
- Elyas is troubled by something; Perrin perceives this but after challenging
  him Elyas will not divulge any information.
- He simply says something tells him to wait.
- Perrin has a dream that he is back in his master's forge, a wolf with him.
- Ba'alzamon enters and kills the wolf by immolation.
- He tells Perrin that he is his and throws a raven at him, its beak striking
  him in the eye.
- Perrin wakes, unharmed, to find Elyas over him, telling him it is time to
  leave.
- The wolves are howling, sharing his pain.
- The leader of the Tinkers comes out of his cabin, looks to the skies and tells
  him they must go a different direction.
- He asks Elyas if he is coming with them but knows the answer is no.
- Perrin does not understand what the man was looking at the sky for.
- The group leave, but not after obeying the formalities of the Tuatha'an.
- When they rejoin the wolves, they communicate to Perrin that they cannot keep
  him safe from Ba'alzamon until he accepts what he his.
- Perrin forces them out of his head, unaware before that he was even able to do
  this.

## Chapter 28: Footprints in Air

- Nynaeve travels with Lan and Moiraine, continuing her mistrust/dislike of the
  Aes Sedai.
- She knows that she should find more affinity with the Warder but finds that it
  is not so – in fact she finds him even harder to relate to.
- She notices that both appear to be searching the air or even questioning it
  and she does not understand this.
- After a long time but there near silence, Moiraine says that they can both
  feel the presence of the Dark One.
- They believe his power will soon be strong enough to break free.
- The group reach Whitebridge and see several burnt buildings in town.
- The townspeople are dishonest about it: they say that the fires were
  accidental.
- Moiraine tells Nynaeve she may yet still be able to track the two boys they
  have given away their coins and the boy who still has their coin is trackable
  to her anywhere within half a world.
- Walking through the town, Moiraine is certain that the two were there within
  the previous two days and so was a Myrddraal.
- She tells Nynaeve that she refuses to believe that the two have been killed
  and that the Dark One has had such an easy victory.
- She says they will track the boy on his own as this can be done more easily.
- (She is unable to tell which of the boy has the coin and which do not.)
- She believes the boys that she cannot track will be driven by the original
  goal of Caemlyn and she can find them there.

## Chapter 29: Eyes Without Pity

- Elyas, Perrin and Egwene are on the run.
- Elyas presses on at ridiculous speed and so as not to be visible on a
  ridge, they go round rather than over every hill.
- Ahead, they see a flock of ravens, 100 strong, searching.
- Elyas manages to keep them out of sight of the ravens, but knows there is
  every chance there is a flock of ravens behind them as well.
- Their speed has to increase, keeping ahead of any danger behind but not too
  close to the ravens in front.
- They come across eaten animals, torn to pieces by the murder.
- Perrin can sense the wolves getting into a huge fight with another group,
  suffering numerous injuries.
- He knows the wolves and therefore another murder of ravens are behind them.
- He finally admits to Elyas that he can sense and communicate with the
  wolves – Elyas also knows they are surrounded.
- Elyas says ahead there is somewhere safe but Perrin does not think they can
  make it.
- He is contemplating whether he has the bravery to kill Egwene himself before
  the ravens rip her apart.
- All of a sudden, he is filled with the lightness and notices that the ravens
  ahead are no longer there.
- Elyas explains that they have entered a stedding, a place thought only to
  be a legend by Perrin, were the One Power has no place.
- Being cut off from the one power is very uncomfortable for servants of the
  Dark One and Aes Sedai and so they do not come in.
- Elyas says that as the ravens can still see into the stedding, they must
  still head out of sight.
- They make camp, and Elyas points them to an eye that belonged to an
  enormous statue of an ancient high king, Artur Hawkwing.
- This king gave peace to the land but only if he was not challenged by the
  people.
- He also despised Aes Sedai and led a twenty year siege on Tar Valon.
- He planned a massive city in the stedding and this statue was built first.
- On the day the statue was finished, the king died.
- His heirs fought over his kingdom and it was lost.

## Chapter 30: Children of Shadow

- Perrin and Elyas talk about his axe.
- Perrin now hates it as it was the weapon with which she would have killed
  Egwene to spare her from the ravens.
- Elyas says hating it is fine – he should only worry when he loves it.
- Suddenly, the wolves raise an alarm.
- A group of mounted men, who are said to smell wrong, are approaching.
- Perrin and Egwene hide; Elyas goes on the hunt with the wolves.
- These men turn out to be Children of the Light.
- When they discover Perrin and Egwene's hiding place and one of them levels a
  lance at him, a wolf bursts from the undergrowth, unhorses and kills the man
  before being impaled on two lances.
- Perrin screams in response to this and launches a furious attack with his axe
  before being knocked unconscious, awaking tied up with Egwene.
- In the tent with them are a lord captain of the Children of the Light and a
  more junior officer.
- They debate whether Egwene and Perrin are darkfriends and allow them to tell
  their story.
- They do not believe the story and the lord captain says they must come with
  them to Caemlyn.
- He believes there is a chance for salvation for Egwene but tells Perrin that
  he killed two of his men.
- Perrin has noticed that he can no longer feel Elyas or the wolves – something
  which he recently yearned for but now thinks the price of it might be that
  they are dead or they have left him.

## Chapter 31: Play for Your Supper

- Rand and Mat are on the run.
- They are torn between hoping that Thom catches up with them and fearing that
  the Myrddraal will.
- They are starving and try stealing from farms but find that is not productive.
- Instead, they offer their services – usually manual labour – for some food and
  board.
- The success of this is variable.
- The only point of contention between the two are the weapons the carry: Rand
  knows that if they sold the ruby at the hilt of Mat's dagger, they could eat
  and sleep like kings all the way to Caemlyn.
- In turn, Mat tells Rand his father's sword would fit an excellent price.

## Chapter 32: Four Kings in Shadow

- Mat and Rand arrive in the town of Four Kings.
- They determine to earn food and board at an inn with Rand's fluteplaying and
  Mat's juggling.
- They find most inns are full and have entertainment.
- The last inn the go to is a rundown and unwelcoming place but they are
  accepted by the innkeeper.
- As they play, they are convinced the innkeeper plans to steal Rand's sword and
  flute.
- Furthermore, as shady looking character arrives, and Rand managers to identify
  his clothes as being from Whitebridge.
- The overhear serving maids saying this man has been to every inn in town and
  only stopped in this one.
- After they finish, they are shown to their store room by one of the bouncers.
- Immediately, they attempt to escape, first barring the door and then trying to
  force open the window.
- They discover the window to be barred on the outside.
- The shady looking man appears at the door, confirming himself to be a
  Darkfriend and telling Mat and Rand they must come with him.
- He has many men surrounding the inn.
- Rand desperately tries to think of a way to escape and blinding flash of light
  occurs, knocking him against the wall, destroying the window and door.
- When he awakes, he stumbles to his feet and find Mat, who says it was
  lightning that struck the inn.
- Mat has been blinded by the great flash of light.
- Rand leads him out of the end, seeing all the Darkfriends lying unconscious or
  dead on the floor and being uncertain whether it was lightning or him that
  caused this.

## Chapter 33: The Dark Waits

- Mat and Rand are on the run again.
- Mat's sight is slow to return.
- They encounter a Darkfriend in one village, but he admits to being alone and
  they are easily able to get away from him.
- Rand has another dream featuring Ba'alzamon in which the Dark One says he is
  his and marks him as his.
- When he awakens, he notices Mat is also dreaming though he never confirms
  whether Mat is having a similar dream.
- Rand becomes very sick with alternating fever and chills.
- As he begins to recover, they are attacked by another Darkfriend him that
  manages to disarm.
- Her dagger causes the woid in which it is impaled to burn.
- On the run again, they manage to hitch a ride on the wagon with Rand able to
  rest.

## Chapter 34: The Last Village

- Mat and Rand are exhausted and starving on the road.
- Mat is increasingly worried about their plight, almost despairing.
- Everyone they meet is going to Caemlyn to see the False Dragon.
- They think this will aid their attempts to remain unseen by Darkfriends and
  Myrddraals.
- They come across the village, and Mat is keen to find food and rest whereas
  Rand is more wary.
- He notices a dark figure moving in the shadows and, when he sees the sign of
  an inn moving in the wind but the figure's cloak remaining still, realises it
  is a Fade.
- He loses sight of the figure then sees an innkeeper speaking to a man.
- The innkeeper tells of someone from Four Kings who is seeking two boys with a
  heron-marked sword.
- The innkeeper says there will be a great reward for finding them.
- The other man teases the innkeeper a little about the story including implying
  that it reflects poorly on the innkeeper – even that he may be a Darkfriend.
- The man tells the innkeeper of his plan to travel through the night to
  Caemlyn.
- When the two separate, Rand approaches the man and asks to travel in his cart.
- While travelling, the man tells stories about the current queen, Morgase,
  whom he loves, and about Caemlyn's Aes Sedai, Elaida, whom he despises.
- Tradition means the eldest son and daughter of the queen go to Tar Valon, the
  former to be a Warder, the latter to study with the Aes Sedai.
- Morgase's first son died in the Blight; her daughter disappeared, causing
  scheming in Caemlyn.
- The cart-rider hopes her current children, Gawyn and Elayne, go elsewhere.
- His long stories send Rand to sleep, where he dreams of Tam, Myrddraals and
  the Dark One.
- He awakes to find them in Caemlyn, described by the man with the cart as the
  greatest city in the world.

## Chapter 35: Caemlyn

- The boys arrive in the city, finding it to be on another scale again compared
  to Shadar Logoth.
- The cart-rider says it was built by Ogier.
- He advises the boys to get rid of their weapons as they will attract attention
  but they are loathe to do so.
- They seek out the Queen's Blessing, the inn Thom told them to go to.
- Once inside, the innkeeper, Basel Gill confirms the flute Rand carries to be
  Thom's and seems to trust the boys thereafter.
- He says he will only believe Thom is dead when he sees it with his own eyes.
- He tells the boys a little bit about Thom – he was once court-bard and after
  she was widowed, there were rumours that two became more than just friends.
- He fell out of her favour after he left without warning when his nephew got
  into trouble.
- Morgase, described by Gil as having a temper, was enraged; Elaida had no
  love for Thom.
- He was not to return to the city, with the death penalty being his punishment
  if he did.
- Gil tells the boys not to speak of Aes Sedai or Thom in the city.

## Chapter 36: The Web of Pattern

- Rand and Mat have their meal and are shown to their room.
- Mat is sullen so Rand leaves.
- In the inn he meets an ogier called Loial, a ten foot tall creature whom Rand
  initially confuses for a Trolloc.
- The ogier, aged ninety, is considered young by his people, and hot-headed.
- Ogiers live in steddings and rarely leave them; Loial left without permission.
- He left to visit groves, forests planted by ogiers after the breaking of the
  world.
- Men have not looked after them and most are now gone.
- He explains to Rand that stone work was a mere hobby to ogier, an astounding
  fact to Rand as the ogier-built city of Caemlyn in which this encounter is
  taking place is the most incredible place Rand has ever seen.
- Loial thinks Rand is an Aielman and is surprised to hear he is not; when Rand
  explains his people descend from Manetheren, Loial apologises for his people
  not getting there in time.
- Loial talks about the Pattern woven by the Wheel of Time, and says that Rand
  is a Ta'veren, a person whom the Pattern is woven around.
- For this reason, he wishes to travel with Rand, but Rand declines.

> *"'Til shade is gone, 'til water is gone, into the shadow with teeth bared,*
> *screaming defiance with the last breath to spit in Sightblinder's eye on*
> *the last day."* Loial

## Chapter 37: The Long Chase

- Nynaeve, Moiraine and Lan continue their chase for Perrin, although they are
  unaware that he is the last of the villagers to have her coin; they are just
  following the coin.
- They come across a group of Whitecloaks who have captured the coin bearer.
- Moiraine and Lan require help from Nynaeve to carry out the rescue.
- If they successfully scatter the Whitecloak's horses, they will be so busy
  trying to rescue them that they may give them an opportunity to rescue and
  make away with the captives.
- Nynaeve unties the horses when a massive bolt of bolt of lightning causes them
  to panic.
- In the confusion, Nynaeve notices the two wolves are running among the horses,
  making them even more panicked.

## Chapter 38: Rescue

- Perrin and Egwene fear that their captors are about to kill them when very
  unexpectedly hears from the Wolves – Elyas is alive and help is on its way.
- Lan bursts in, dispatch is the guards and tells them to wait.
- When Moiraine's lightning strikes, they make their escape.
- Nynaeve, Moiraine and Lan observe Perrin's injuries under torture.
- They enquire as to what he did to incur such wrath but he cannot bring himself
  to say he killed two of the Whitecloaks.
- They see his yellow eyes and Moiraine and Lan understand what this means.
- Moiraine is surprised, saying that this could not have been predicted and is
  not part of the Pattern.
- Perrin's transformation is further evidence to her of the importance of these
  children.
- Lan speaks to Perrin and tells him that Elyas Machera once taught him many
  things, including about the Blight and swordsmanship.
- He was a Warder, belonging to a Tar Valon faction called the Red Ajah.
- Lan says that there are those in Tar Valon who think differently about how to
  fight the Dark One.
- Moiraine and Lan see the yellow eyes as further evidence that in general,
  things are changing, boundaries are weakening and the Dark One will soon be
  free.
- Moiraine is scared that maybe he can already reach into this world.

## Chapter 39: Weavings of the Web

- Rand decides to go out into the city, leaving Mat who is now behaving as a
  recluse in his room.
- The False Dragon Logaine is being brought into the city.
- Tensions are high – the Queen is popular but some do not like her closeness
  with her Aes Sedai.
- In the crowd, a beggar who Rand has heard is looking for him raises a finger
  towards him.
- Rand thinks this man is a Darkfriend and runs from him.
- He manages to get a precarious view of the procession from on top of a wall.
- Logaine is led in, surrounded by Aes Sedai and Warders and inside a cage.
- Rand cannot shake the feeling that he does not look like a defeated man.
- He is musing on this and why he is surrounded by Aes Sedai when he hears a
  girl's voice saying, "To stop him touching the True Source."
- In his surprise at hearing this, he slips from the wall, the last thing he
  remembers being hit on the head and imagining a laughing Logaine chasing him
  into darkness.

## Chapter 40: The Web Tightens

- Rand finds himself sitting at a table with Moira and Logaine.
- He looks around, and when he looks back, the two are replaced by Ba'alzamon
  and he wakens with a start.
- Unbeknownst to him, Rand has fallen into the prince and princess's private
  garden.
- Elayne tends to his wounds and the pair talk over and at him.
- It is sometime until Rand realises who they are and when he does so, he
  becomes nervous and wants to leave.
- The half brother of the two appears, called Galad.
- When he goes to sound the alarm, Elayne openly tells Rand that she does not
  like him and find him and envious person.
- Her brother reminds her that twice Galad has saved his life when, had he not
  done so, he would have become become first prince in line.
- Galad returns with palace guard, with Elayne insisting that all three are
  taken in front of her mother, Queen Morgase.
- Rand is struck by how beautiful the queen is.
- His questioning is relatively gentle until the nature of his sword is
  discovered, which causes a commotion throughout the gathered court.
- The questioner now becomes the Aes Sedai, Elaida.
- Rand finds her harsher and scarier than Moiraine.
- Rand tells her some truths, including his name, where he is from and the fact
  that the sword came from his father, but becomes annoyed by the questioning
  and begins to lie about when he got to Caemlyn and where he has been staying.
- The sword and the fact that he does not look like someone from the Two Rivers
  are disturbing to the Queen and court.
- She says it is also quite a coincidence him appearing in Caemlyn the same
  day that the False Dragon is paraded before being taken to Tar Valon with
  Elayne for her teaching.
- The Aes Sedai tells a prophecy:

> *"This I foretell, and swear under the Light that I can say no clearer: from*
> *this day Andor marches towards pain and division. The Shadow is yet to
> *darken to its blackest and I cannot see if the Light will come after. Where*
> *the world has wept one tear, it will weep thousands. This I foretell. This*
> *too I foretell: pain and division come to the whole world and this man
> *stands at the heart of it."* Elaida Sedai

- Then says it is the Queen's decision what to do with him.
- She is merciful, saying that in these dark days some mercy is needed and that
  Rand's story is so unbelievable that it is more likely to be the truth than an
  invented lie.
- Rand is escorted out by Gawyn and Elayne.
- After Elayne leaves, Gawyn tells Rand that he looks like an Aielman.
- When leaving the castle, he tries to keep his cool but just the thought of
  Elaida watching him means that soon he is involuntarily running at full pace.

## Chapter 41: Old Friends and New Threats

- Rand returns and tells Master Gil what has happened.
- Loial again says he wants to come with him.
- Whitecloaks enter the inn and question Gil but he angrily dismisses them.
- A serving girl says there is a woman in the kitchen asking for Mat and Rand by
  name.
- Rand impulsively runs in, discovering Moiraine, Lan, Nynaeve, Egwene and
  Perrin.
- He briefly summarises their tale, including the news that Thom is probably
  dead.
- The others ask about Mat and Rand tries to explain what has happened to him –
  not a sickness per se but very strange behaviour.
- The Two Rivers folk go to see him in his room and find him curled up, spitting
  wicked comments at them.
- Nynaeve is determined to help him and just before she touches his face,
  Moiraine comes in and tells her not to do so.
- The Aes Sedai has seen evil in him and knows that it came from the Shadar
  Logoth; Rand confirms about Mat having taken something from there.
- When Moiraine approaches Mat, he lashes out at her with the previously
  concealed dagger.
- As if from nowhere, Lan appears and grabs Mat's wrist.
- She explains that Mat has been fighting evil internally and is nearly lost;
  she will try to help him.
- However, he has brought the evil of Mashadar from Shadar Logoth, putting the
  world at risk.
- Perrin says it would be better if they had all died but Moiraine says the Dark
  One can still use them dead whereas alive they can fight him.
- Lan confirms that there are Trollocs and Myrddraals converging on the city and
  soon they will have the confidence to enter to search for the three.
- He is still troubled by the way they appear to have been able to move unseen.

## Chapter 42: Remembrance of Dreams

- The children of Emond's field meet the ogier Loial.
- Shortly afterwards, Mat comes in followed by Moiraine.
- She has used her angreal to purge him of the evil but says it will return in
  time and she cannot separate him from his dagger without killing him – he
  needs help in Tar Valon.
- The evil of the blade will draw Myrddraals and certain other Darkfriends but
  she has put a ward around him which should minimise the risk.
- Loial talks with Moiraine – he talks of a man who entered the stedding near
  death saying that the Dark One was going to kill the Great Serpent and blind
  the Eye of the World.
- Loial also appears to say that the Eye of the World is the eye of the Great
  Serpent.
- Perrin and Egwene tell Moiraine that the Tinkers also told them a story like
  this.
- Moiraine is interested but troubled by this.
- On hearing about the Eye of the World, Rand, Mat and Perrin confess their
  dreams to Moiraine.
- She is disappointed because she asked them to tell her of strange dreams and
  says that her ability to shield them from the Dark One is weakened by his
  multiple contacts with them.
- Rand remembers the list of people Ba'alzamon gave him who had been
  manipulated by the Eye of the World.
- He wonders how much he can trust Moiraine.
- Moiraine remains the boys that the title of Father of Lies is highly
  appropriate for Ba'alzamon and that they should not listen to anything he has
  told them.
- She declares that it is not safe for them to travel to Tar Valon and indeed
  they no longer have the required time.
- Instead they must go to the Blight to see the Green Man and she hopes that the
  Loial can help them.
- He confirms that he can navigate The Ways but says anyone who enter will be
  killed – swallowed by the Shadow.

## Chapter 43: Decisions and Apparitions

- Loial tells the story of The Ways, gateways created as a gift from the Aes
  Sedai to the ogier for their help with the exiles when the male Aes Sedai were
  going mad and the world was breaking.
- Moiraine confirms she belongs to the Blue Ajah and is sympathetic to the
  ogier, believing their sanctuary for the insane male Aes Sedai made the
  breaking of the world less severe (others believe it made it worse.)
- Loyal explains that 1000 years ago, The Ways became tainted and they are no
  longer used.
- Moiraine says they must go to Fal Dara via The Ways.
- The group decided that they must should follow Moiraine's plan and all will
  go.
- That night, the boys all have the same dream where they are again confronted
  by the Dark One.
- He says he has had this confrontation with them since the beginning of time
  though they wore different faces and that in the end, they all knelt or died.
- He tells Rand he has Black Ajah within the Aes Sedai.
- In Mat's encounter, Mat worries that he has given away his face to the Dark
  One.
- After they have awoken and are discussing this, Moiraine comes in and says
  they must leave immediately.

## Chapter 44: The Dark along the Ways

- The group leave the Queen's Blessing in secret and Loial leads them to the
  first Waygate.
- They enter The Ways with both Loial and Moiraine insisting they must be no
  more time than is absolutely necessary within them.
- Within The Ways are a series of bridges and islands surrounded by darkness.
- Loial navigates using *guidings* - writing on stone made by the ogiers.
- Rand is quickly disorientated.
- After travelling for some time, Loial stops abruptly and Rand notices that the
  bridge in front of them is broken.

## Chapter 45: What Follows in Shadow

- The group retreat to take a new path.
- After a while, Lan says he thinks they are being followed by a person or
  thing.
- He insists that they are being followed from a distance as theIr follower
  could have caught up with them but chose not to.
- Soon afterwards, they discover a disturbing truth – Trollocs have entered The
  Ways.
- This provides an explanation to Lan as to how the Trollocs and Myrddraals were
  able to navigate without being seen.
- Soon after they discover disfigured dead Trollocs in the stones.
- This disturbs Loial as he is now even more convinced they should not have gone
  into The Ways.
- Moira is more circumspect – perhaps these were deliberate defences built by
  those who made The Ways to keep evil out.
- Rand feels a wind and remembers Loial said there was none in The Ways.
- Loial says it is Machin Shin, the Black Wind, and the reason The Ways have
  been abandoned by ogier.
- They have to run for their exit.
- When they get there, Moiraine find the key is missing and has to burn through
  the Waygate using her staff.
- Evil voices can be heard from within the smoke.
- They successfully managed to escape.
- Moiraine tells them she does not know whether the Black Wind is something born
  in The Ways of whether it entered there and cannot leave.
- She says there is worse to come.

## Chapter 46: Fal Dara

- The group receive a rapturous reception by the forces at Fal Dara, because of
  the presence of Lan and Moiraine.
- Lan is addressed by another name, Dai Shan, and hailed as a great captain.
- Moiraine is given reverence as an Aes Sedai.
- The city of Fal Dara is not like the ones they have been to before – it is
  more of a fortress.
- The lord in the city, Agelmar Jagad, says they are preparing for a massive
  battle with the Dark One and his Trollocs at Tarwin's Gap and asks that
  Moiraine and Lan join them.
- He says Moiraine would be worth 1000 lances and that Lan's presence and banner
  would rally would rally thousands to their cause.
- Lan says that his kingdom is gone forever and he is now a Warder, bound to
  Moiraine and bound for the blight.
- Moiraine explains that she has seen the Green Man before, and although no one
  is said to see the Green Man twice, she disagrees saying that it is a matter
  of need.
- Agelmar then says he will send men with them but Moiraine and Lan refuse as
  they want to go as quickly and quietly as possible.
- A guard reports that a man has been captured entering the city.
- He is brought in front of them and found to be none other than the peddler,
  Padan Fain.
- He appears to assume different voices and personalities with one saying he and
  wants to be free of Ba'alzamon.
- Another personality tells Agelmar that he knows how to defeat the Dark One but
  this is probably him trying to sew confusion.
- Moiraine says she must take him immediately to cleanse him.

## Chapter 47: More Tales of the Wheel

- The Lord of Fal Dara explains Lan's history and true heritage, that being that
  he is the heir to the throne of a kingdom Malkieri which was great as recently
  as 50 years ago but is now no more.
- He was spirited away with the greatest fighters of his kingdom and trained by
  them.
- His parents swore an oath for him when he was a baby:

> *"To stand against the Shadow so long as iron is hard and stone abides. To*
> *defend the Malkieri while one drop of blood remains. To avenge what cannot*
> *be defended."*

- Moiraine returns explaining that the Peddler is one of the most evil and vile
  man she has ever met.
- He has been in the service of the Dark One for three years, searching for
  three boys.
- She is determined that the reach their destination without the Dark One having
  another chance to try to kill the boys.

## Chapter 48: The Blight

- The group moving to the Blight, finding it to be chokingly humid and acrid.
- Egwene and Rand exchange some words of love between them, but Rand remembers
  what she was told by Min.
- Nynaeve and Lan confess their love for each other however Lan says his mission
  in life means they will never be together.

## Chapter 49: The Dark One Stirs

- Despite Moiraine's assertion that her presence would keep some of the evil in
  the Blight away from them, the group are attacked by twisted creatures and by
  the foliage of the Blight itself.
- They all fight valiantly, none more so than Lan.
- Every time he reappears, Moiraine quickly heals him and he rejoins the fight.
- The attack stops as quickly as it began in response to cries from the
  distance.
- Lan says that they are Worms and that the things that attacked them fear the
  Worms and that they must push on to the high passes as quickly as possible.
- Nynaeve regrets asking him why when Lan replies that the Worms fear things
  which dwell in the high passes.
- Throughout all of this, Loial has been groaning with disappointment.
- Lan offers to stay behind and help them press on but Moiraine says she needs
  him.
- Despite not even reaching the hills, the enter a place of respite from the
  Blight which Moiraine immediately recognises as the dwelling of the Green Man.
- He appears, a being made of trees, leaves and other greenery, dwarfing Loial
  the way the ogier dwarfs the men and women.
- He is surprised that Moiraine was able to find him again but again she
  says that it is because the need is so great.
- He addresses Perrin as Wolf Brother, commenting that it is an indication of a
  return to the old times.
- He addresses Rand as 'child of the Dragon.'
- The address is Loial as little brother.
- When he hears that the group seeks the Eye of the World, he says saying that
  this means the Dark One stirs and that and notes that his efforts to hold back
  the Blight from entering his haven have been harder than ever this year.

## Chapter 50: Meetings at the Eye

- Moiraine leads the group to the Eye of the World.
- The Green Man says he does not want to go in and is not sure if he will see
  them when they come out; his time is over.
- They go in and discover the Eye is a pool of pure Power.

> *"It might be called the essence of saidin. The essence of the male half of*
> *the True Source, the pure essence of the Power wielded by men before the*
> *Time of Madness. The Power to mend the seal on the Dark One's prison, or*
> *to break it open completely."* Moiraine

- It was made by the combination of male and female Aes Sedai (saidin and saidar
  being the male and female halves of the One Power) and is free of taint.
- No female Aes Sedai can wield its power.
- Moiraine implies that it is the boys, or at least one of them, who must use
  it.
- They leave the cave and are confronted by two of the Forsaken who have become
  free of their prison.
- They claim that Mat has led them there and that they are only interested in
  one of them now; Rand is convinced they mean him.
- A battle ensues in which most are useless, including even Lan.
- The Green Man bursts back in and kills one of them though is killed in return.
- Moiraine tells them to run and battles the other forsaken, Aginor.
- The last thing Rand hears is her screams.

## Chapter 51: Against the Shadow

- Aginor soon catches up with Rand and challenges him.
- He says that he can serve the Dark One dead or alive and so considers killing
  him, even though he was ordered to keep him alive.
- Suddenly Rand perceives a pulsing white rope-like thing tethering Aginor and
  disappearing in to the distance.
- A tendril reaches out and when it touches Rand he feels filled with fire.
- Aginor screams in anguish and tries to deny Rand access to the power and they
  enter a mental battle.
- Rand wins and Aginor is consumed in flame.
- Rand wills himself to get away and next finds him self on the battlefield at
  Tarwin's Gap.
- The hopelessly outnumbered humans are rallying for a final charge against the
  trollocs.
- Rand channels the fire within himself into a wall of fire directed by his
  shout against the army and followed by him banging his fists on the ground,
  sending earthquakes towards them.
- He has reduced the odds against the humans to 2 to 1 and they rally for
  another charge.
- Rand decides he wants to end this and so needs to see Ba'alzamon.
- He hears a voice which addresses him as the chosen one and tells him
  Ba'alzamon is not here.
- Rand starts to claim a staircase which appears suspended in nothingness and
  find himself in Ba'alzamon's room – the one from his dreams.
- He threatens and tries to tempt Rand but Rand tries to resist.
- The tether from Ba'alzamon is immense and black.
- Rand cuts it with a sword of pure light which he has produced, causing
  Ba'alzamon to scream.
- The light and fire within Rand then sets Ba'alzamon alight.
- Rand is determined that this will be the end and soon the entire room is
  filled with blinding white light.
- Suddenly, Rand finds himself falling in utter darkness, before being struck by
  something solid.

## Chapter 52: There is neither Beginning nor End

- Rand awakes back on the hilltop, finding ashes and remnants of the Aginor's
  cloak.
- He is heavily disorientated, not even knowing himself.
- He finds Nynaeve, Egwene and an injured Moiraine.
- Moiraine confirms that he has channelled the male half of the One Power,
  something she had already suspected he could do, and says she has told the
  other woman about this but not his friends Mat or Perrin.
- Nynaeve and Egwene are now frightened of and worried for Rand.
- Lan and loyal return with Mat and Perrin.
- The pool at the Eye of the World has been drained and they have found three
  treasures within.
- The first are a number of stone plates which Moiraine recognises as Heartstone
  and one of the seven Seals of the Dark One's prison.
- Heartstone cannot be broken once made and any strength directed against it
  becomes strength of the stone itself.
- No one has been able to make it since the Age of Legends.
- The second treasure is the Horn of Valere, a horn which is said to be able to
  call back ancient heroes for battle against the Dark One.
- The final treasure is the Banner of the Dragon which was carried in his battle
  against the Dark One.
- Moiraine says that they have found these things for a reason and she wants to
  know what that is.

## Chapter 53: The Eye of the World

- Before leaving, Loial sings around the oak growing over the Green Man's grave.
- This gives new health to the tree, pushing back the Blight.
- As they head south, not only does the Blight leave them alone, but it lessens
  as they reach the border, by which it seems spring has sprung.
- They go straight to Fal Dara and Agelmar, who is troubled by the nature of
  their victory.
- His men say the Creator intervened on their behalf; Agelmar saw a man.
- Moiraine ensures the peddler is still secure, and shows Agelmar the Horn of
  Valere but says it must go to Illian.
- She believes the final battle is still to come.
- Seven days later, Rand has been practicing sword work with Lan, to whom he
  says he will not change his mind.
- He talks to Egwene, explaining that he will be the only Emond's Fielder not to
  go to Tar Valon.
- He does not no where he will go, but he says it will never be home, and that
  he will never channel the One Power again.
- Moiraine has been using her power to eavesdrop.
- She smiles and says to herself:

> *"The Prophecies will be fulfilled. The Dragon is Reborn."*

[Home](/blog "ohthreefive")