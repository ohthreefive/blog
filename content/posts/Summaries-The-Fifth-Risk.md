+++

date = "2019-01-03T11:56:10Z"
title = "Summaries: The Fifth Risk"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# the fifth risk

**michael lewis**

## introduction

- The opening chapter is about the Trump presidential transition plans from the
  inside.
- Chris Christie had to approach the Trump campaign and explain the need for a
  transition team.
- Trump didn't think he needed one.
- Shortly after Trump won the election, Christie was unceremoniously fired by
  Steve Bannon.
- Steve Bannon blamed Jarrod Kushner for the firing – Chris Christie had
  previously undertaken legal action against his father.
- Similarly, the entire transition team which Christie had overseen found
  themselves replaced.
- Effectively, Donald Trump wanted to handle the entire transition process
  himself.
- The chapter concludes with a quote from Steve Bannon, "This guy doesn't know
  anything and he doesn't give a shit."

## the tail risk

- The opening chapter concerns the ambivalence of the Trump administration to
  the department of energy.
- Significant time is given to explaining how vast and complicated the
  department is.
- Among many other things, cleanup of nuclear waste and attempting to recover
  lost nuclear substrate such as plutonium and uranium falls under this
  department's jurisdiction.
- After all, 'nuclear' is a form of energy!
- At least a year was spent by the DoE preparing transition documents (Obama was
  so appreciative of Bush's efforts in this regard and sought to surpass them.)
- On day one, however, no one showed up.
- When members of the Trump administration did show up, they seemed under
  qualified and disinterested. At one point, there even seemed to be the
  beginnings of a McCarthy-esque witch-hunt with employees with pro-renewable,
  anti-carbon links being sought.
- The fifth risk of the novel's title is explained. The head of the DoE listed
  the risks the department had to deal with: the fifth is 'project management.'
- An argument is made that the Trump administration embraces ignorance to
  protect itself from its own responsibilities.
- The DoE oversees some extremely long term projects which are in jeopardy if
  short-term thinking and ideology persists.

## people risk

- This chapter deals with the department of agriculture, another US government
  department with an overly simplistic name.
- Like the department of energy, it has a wide remit, not much of which has to
  do with farming, which is its presumed focus by the majority of people.
- Similar to the DoE, the Trump transition team sent no one to the department
  for some time after he took power.
- A number of public servant heroes are interviewed and profiled.
- The department seems non-partisan and, like the DoE, heavily steeped in
  science and research, funding of which has been cut by Trump.
- The chapter ends with a discussion about a small departments within USDA
  called Rural America.
- There are only ever seven departments allowed.
- This department had a large budget and supplied funding and subsidies for many
  ventures within what became Trump supporting America.
- Despite this, the department was cast aside by the Trump administration in
  favour of splitting another USDA department in two in a move which was
  allegedly to promote foreign trade, despite Trump making anti-trade moves
  elsewhere in government.

## all the presidents data

- A sprawling and epic chapter to conclude the book, occupying nearly half of
  the entire runtime.
- Whereas the Department of Energy and Agriculture respectively were name
  checked early in the previous two chapters, the Department of Commerce is only
  mentioned 25 minutes or so into this chapter, it is another 25 minutes until
  it is mentioned again.
- Unlike the previous chapters, it is less the focus of this chapter.
- Instead, focus is heavily placed on heroes, villains and (above all) the
  weather.
- On the heroic side are the first female American astronaut to have performed a
  space walk and a hacker turned first chief data scientist of the US
  government.
- The Department of Commerce, like the previous departments, is extremely poorly
  named: its chief asset is data; data about the weather more than anything
  else.
- A similarity between Trump's treatment of Energy and Agriculture and commerce
  is that, unsurprisingly, no one from the Trump administration arrived at the
  Department of Commerce for some time.
- The villain of the story is the Trump presidency's choice of head of this
  department: the owner of paid service AccuWeather.
- AccuWeather gets it weather data from the government but interprets the data
  itself and sells rather than gives away its interpretations/forecast.
- After some more brief information about the Department of Commerce, we are
  told how the publicly available data and collect has been disappearing from it
  to websites under the trump administration.
- Thereafter, we return to NOAA (National Oceanic and Atmospheric
  Administration) and the subject of weather data, specifically talking about
tornado prediction and warnings.
- Behavioural models are as important as good prediction for good outcomes after
  tornadoes – the department sought to understand why people ignored tornado
  warnings and how to make sure they did not.
- The book who concludes by cleverly circling back to its a alleged subject
  matter: the election of Donald Trump.
- A woman is interviewed about her experiences after a hurricane destroyed her
  barn and house.
- She said how she had prayed for 10 years for a tornado to destroy the barn.
- When pressed further, this was because her husband had committed suicide in
  that barn and she had to look at it every day.
- However, she never expected to lose the house.
- Lewis draws a clear parallel with the election of Donald Trump.
- He is the tornadoes that voters wanted to destroy their barn but it is what
  they didn't expect to be destroyed which might end up killing them.

_"You might have good reason to pray for a tornado, whether it comes in the
shape of swirling winds or a politician. You imagine the thing doing the damage
that you would like to see done and no more. It's what you fail to imagine that
kills you"_

[Home](/blog "ohthreefive")
