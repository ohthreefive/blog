+++

date = "2019-05-25T22:07:27Z"
title = "Summaries: The Fifth Season"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Fifth Season

**N. K. Jemisin**

## Prologue

- A narrator addresses the reader directly.
- A woman cries over her dead child.
- The narrator shifts from the personal to the more widespread: the story takes
  place in a land of constant geological turmoil, ironically called the,
  "Stillness," by its inhabitants.
- A chief city is Yumenes, where the residents are said to be unique in that
  they build structures not designed to last, as if accepting their fate.
- This story is said to be that of the end of the world.
- A human converses with a woman, called a, "Stone Eater," by the narrator and
  not the same species as the human.
- The human describes himself as a, "Slave."
- The human appears to create a massive earthquake in his mind, partly at the
  behest of the Stone Eater, although this is not explained.
- The story returns to the grieving woman.
- She is in a town built underground called Tirimo and her son was just three
  years old.
- He was killed by his father.
- When the destruction from the earthquake reaches Tirimo, it is said that it
  leaves the town untouched in a perfect circle around it and that this implies
  there is something dangerous there, called a rogga.
- A coffin-like stone structure splits and a small being emerges.
- The small being is described as, "Not a child."
- The being starts to walk towards Tirimo.
- Floating above this world are obelisks built by a previous civilisation (all
  previous civilisations are destroyed by the Stillness.)
- The author says that though these natural disasters are described as the end
  of the world, they are really just the end of the civilisations currently in
  the world and that the world itself survives.
- The prologue concludes by the author stating that this time truly is the end
  of the world.

## Chapter 1

- Essun is the mother of the dead child, she has been with him in her house for
  the two days since he has died.
- She lives in Tirimo and is an Orogen, the proper or polite term for a rogga,
  who has been in hiding there for 10 years.
- Only her son, her daughter (who she describes as being, "Lost," and one other
  person called Lerna know her secret.
- It was by her power that the seismic activity from Yumenes passed around
  Tirimo.
- Essun's husband killed her young son.
- She theorises that her son also had her powers and must have inadvertently
  shown them to her husband.
- She also theorises that her son's death at the hands of his father and the
  magical protection of Tirimo will be linked and therefore TIrimo will discover
  her secret and come for her.
- Lerna seems keen to protect her.
- Essun makes a comment that she thinks the earthshake, as they are known, is
  the start of something big.

## Chapter 2

- A child called Damaya believes her parents are selling her to slave traders
  because they hate her.
- Two weeks previously, she injured (or possibly killed) a child at her school
  who was attacking her.
- Feeling herself to be under threat, she responded instinctively, revealing
  herself to be an Orogen, something she was not even aware of herself.
- In truth, her parents are giving her away to something called a Guardian from
  Yumenes because they fear her.
- The Guardian, called Schaffa, and implies that it is the law that such Orogens
  should be reported to Yumenes as they can be trained to control their powers
  and counteract the effects of the Stillness.
- Damaya feels an instant warmth towards the Guardian.

## Chapter 3

- Essun prepares to leave Tirimo.
- She wishes to find her husband and kill him.
- She assumes her husband has also killed her missing daughter.
- She goes to see the town's mayor, who tells her that her husband was last seen
  leaving the town with her daughter, who was alive at the time.
- In conversation, the mayor reveals that his sister was also an Orogen – he has
  realised that Essun is an Orogen.
- Because he is sympathetic towards roggas, he agrees to escort Essun out of the
  town gates.
- Two guards on the gate do not want to let her go but obey the mayor.
- When she is outside the gate, one of them fires a crossbow boltat her, which
  she destroys with her power.
- In fury, she unleashes the power which she is feared for – an earthquake
  begins and all the air around her has its moisture and water drained from it,
  killing those trapped in an expanding sphere, including the mayor, her
  assailants and anything else.
- Essun comes back to her senses, believing now that she is responsible for her
  son's death by cursing him with these powers rather than other people being
  responsible for their fear of him.

## Chapter 4

- An Orogen still in training (Orogens gain 'rings' as their training
  progresses; this Orogen has four) is described as talented but somewhat
  difficult.
- Her name is Syenite.
- She was born to 'normal' parents and is termed 'feral' rather than a Fulcrum-
  born Orogen born to two Orogen parents.
- 'Feral' is a derogatory term, but not as bad as 'rogga'.
- The fact that Orogens occur randomly as well as through breeding means the
  Fulcrum will never be able to fully control them as they would like.
- The Fulcrum is the place in Yumenes where Orogens are trained.
- Orogens are bred by junior Orogens being forced to reproduce with senior
  Orogens: Syenite is to be coupled with a ten-ring Orogen.
- She is surprised by his youth and also somewhat by his naivety: at first he
  did not realise the women were not coming to him willingly.
- Syenite will remain with this Orogen (his name is not given though Syenite
  says she knows it) for at least a year to allow them to conceive.
- They have a mission to destroy a coral reef which they will also undertake
  during this time.
- Syenite is relieved by the relative freedom this mission will allow.

## Chapter 5

- Essun is wandering around after her attack.
- We learn that Orogens absorb a tremendous amount of energy during this process
  and their training is to allow them to control the release of this energy so
  as not to cause more widespread destruction.
- However, after the process, they have surplus energy which they must disperse.
- She unexpectedly comes across a small boy, without being able to sense or hear
  him approaching.
- I assume he is the boy described as leaving the stone coffin at the end of an
  earlier chapter.
- He asks if he can be allowed to sleep alongside her.
- Although uncertain of him, Essun decides it will be no harm to let him sleep
  there and she will get rid of him the next day.
- The chapter concludes with an extract from a scientific book describing non-
  human sentient beings, commenting that they can change shape but they are most
  common form is similar to that of a statue, but that they could be anything.

## Chapter 6

- Damaya and Schaffa are travelling together when Schaffa delivers his first
  lesson.
- Guardians exist to train Orogens but also to protect the world from them, even
  if that means killing them.
- He lays bare the strict relationship they will have.
- He gives her a first test, deliberately breaking her arm to see if she can
  control the instinct to defend herself and kill him.
- Impressively (to him) she manages to suppress her desire to kill him.
- With the nature of their upcoming relationship revealed to her, Damaya wishes
  she had never gone with him.

## Chapter 7

- Essun is speaking to the young/small person, his name is Hoa.
- She find somewhere for him to wash and remarks on how strange she looks, from
  his skin tone, to his squat stature to his strange hair.
- Hoa does a few things which trouble Essun: he does not seem to eat, he does
  not take her hand when she offered it to him after he falls and he avoids her
  direct, albeit rather rude, question of what he is when she sees his odd
  physical appearance.
- However, she is convinced to stay with him when he speaks of her daughter,
  about whom Essun has told him nothing.
- Hoa says he is able to sense Orogens and has always been able to do so, though
  he does not know.
- Essun asks Hoa to help her find her daughter.

## Chapter 8

- Syenite continues her journey with the ten-ring Orogen, whose name we learn is
  Alabaster.
- She sees him quietly quelling seismic activity almost continuously during
  their journey.
- She asks him why he bothers: the Fulcrum maintains what are called Nodes.
- These are Orogens whose job is to quell seismic activity.
- In Yumenes, a huge network of them keeps the city relatively safe and calm.
- In the more sparse regions, a single Orogen may be responsible for a town.
- Alabaster says he is giving these Nodes a break and that Syenite should try
  visiting a Node.
- They also continue their mission of trying to procreate with Syenite believing
  she may be late with her period.
- Suddenly, Alabaster senses massive seismic activity in the distance.
- He uses all of his power, as well as co-opting hers, something which Syenite
  thought was impossible, to quell the shake.
- Syenite feels incredibly uncomfortable and violated to have been used like
  this.
- During the process, she is briefly exposed to his power which she discovers
  finds to be incalculably immense.
- Alabaster tells her that the only conclusion is that it was the node
  maintainer him or herself who nearly set off this shake and that the resulting
  Torus will have killed everyone within miles.
- They must go to investigate.
- When they get to the node, Syenite discovers a horrible truth.
- Nodes are paralysed and sedated Orogens, often children, who were unable to
  control their powers even with training.
- Their instinctive response to quell any seismic activity is still active,
  giving them a function.
- There are some people who pay to rape these Nodes, and they often can ask for
  them to be woken from their sedation.
- However, this risks the instinctive and defensive response to threat within
  the Node, which has been the cause of the accident here.
- Alabaster refers to Nodes such as this as roggas.
- He tells Syenite that they will not report exactly what happened to the
  Fulcrum, just that there was nearly a significant seismic event but that they
  were able to stop it.

## Interlude

- The narrator draws our attention to many deficiencies in the people of the
  Stillness, for example they do not speak of islands or other continents.
- In the seismic activity, such things are fleeting on a geological scale and as
  the people of the Stillness only pay attention to what is passed down in Stone
  Lore, they have no heed for these intangible things.
- Similarly, they do not talk about the stars or other heavenly bodies.
- The narrator points out that the main thing that they do not notice is what is
  missing from the sky.
- The narrator concludes by saying that it is just as well that there are more
  than just humans on this planet.

## Chapter 9

- Alabaster and Syenite reach a small town, at which they are not particularly
  welcomed by the deputy governor.
- Alabaster and the deputy governor exchange relatively hostile words before the
  two Orogens go to eat and stay at a local inn.
- Overnight, Syenite awakens to find Alabaster paralysed by poison.
- Again, he uses her power along with his own to, seemingly impossibly, siphon
  the poison from his blood.
- He explains to Syenite that this ability is beyond her.
- Before waking and also while being controlled by Alabaster, Syenite has a
  strange sensation of falling upwards helplessly towards the sky.
- After Alabaster has fallen back asleep, Syenite gazes towards the sky and
  thinks that the obelisk floating there is significantly closer than it had
  been the previous day.

## Chapter 10

- On the road with Hoa, Essun stops to spend the night in the communal area.
- They are woken overnight by a mass panic and flee.
- They come across a solitary women to ask for water.
- While there, they are set upon by an animal.
- Essun prepares to kill it with her powers but Hoa steps in front of her,
  allowing the animal to bite his arm.
- The animal is instantly killed and petrified by this.
- Hoa seems somewhat ashamed and says he did not want Essun to see this yet.
- When they leave, the other women goes with them.

## Chapter 11

- Damaya is being trained in the Fulcrum.
- Training is very strict.
- She also finds herself bullied.
- The situation is particularly precarious as any Orogen who does not
  successfully learn to control their power can be killed for the safety of
  others.
- Damaya conspires to expose her bullies but in doing so exposes all of her
  classmates, earning her the enmity of all.

## Chapter 12

- Syenite passively aggressivly tells the deputy governor that she plans to
  continue to work on her own even though Alabaster is sick.
- The women have a debate of wills with Syenite eventually succeeding after
  being introduced to a more senior official.
- Syenite discovers that under the coral that she is meant to destroy is a large
  mass which she cannot get the measure of with her feelings – to her it seems
  like a void.
- Eventually, she tries to destroy it and the coral and this unknown mass, as
  was her task.
- As she begins to raise it out of the harbour water, she feels filled with
  power similar to the way she felt when Alabaster took control of her.
- However, the power slips back into the massive object, which, as it reaches
  the surface, Syenite realises as an obelisk.
- She sees that the obelisk has been damaged at its centre with cracks radiating
  out from there.
- At the centre of the cracks, to Syenite's horror, is a dead Stone Eater.

## Chapter 13

- Essun, Hoa and their new companion, whose name is Tonkee, continue to travel
  the road, following Hoa's directions towards Essun's daughter.
- They encounter numerous other refugees.
- Tonkee appears to be well educated, carries a compass (which is said to be a
  priceless item) and seems to be a man who has chemically castrated himself.
- Essun thinks she is a trained geomest.
- Hoa tells Essun that it is becoming more difficult to sense her daughter as
  there is a large group of Orogens up ahead.
- This is strange information for her as Orogens do not tend to congregate en
  masse.
- Essun is unsure whether Tonkee has figured out she is an Orogen; in turn, she
  is certain that Hoa is not human.

## Chapter 14

- Alabaster told Syenite to report that she removed the coral and the obelisk
  popped out of the water by itself – he says this is believable and will avoid
  people asking too many questions.
- The Fulcrum tell Alabaster and Syenite to stay where they are and await
  further instructions; Syenite is frustrated by this.
- She has realised that the power that coursed through her both while raising
  the obelisk and twice while being controlled by Alabaster came from the
  obelisks.
- She surmises that she has some control over the obelisks.
- Alabaster confirms that no one has raised an obelisk like she did in the
  harbour for thousands of years.
- Eventually, she comes to realise that Alabaster can also control the obelisks.
- He points out to her that the obelisk in the sky, from which came power to
  siphon his poison, is listing.
- Alabaster is keen that they do not discuss things too much: he implies that as
  the place in which they are staying right now is connected to the granite
  bedrock, powerful Orogens would be able to eavesdrop on their conversation
  through the stone.
- Alabaster's fears are confirmed when they encounter a Guardian shortly
  afterwards.
- Syenite accidentally reveals that it was her, not Alabaster, who raise the
  obelisk out of the bay.
- The Guardian also reveals that Alabaster's own Guardian has been found, though
  we are still unaware what Alabaster had done to her.
- The Guardian draws and quickly throws a glass knife at Alabaster, piercing his
  shoulder.
- The knife Alabaster appears to be stopping him from functioning at all.
- Some power of the Guardian appears to be stopping Syenite from using her
  Orogen power.
- He draws and throws another knife at Syenite.
- Instinctively, she goes to the obelisk for power and finds herself drawn
  eventually to the centre of the obelisk, coming face-to-face with the Stone
  Eater.
- For reasons unknown to her, she asks the Stone Eater if he is okay.
- To her surprise, he replies that he is fine and thanks her for asking.
- At this, the obelisk shatters.

## Chapter 15

- Essun, Tonkee and Hoa arrive at the commune which contains many Orogens.
- They are greeted by an Orogen called Ike(?), who is accompanied by a member of
  the same species as Hoa.
- There are apparently 22 Orogens in the commune and many of Hoa's species.
- Ike(?) chastens Hoa, who is initially confrontational towards her and the
  other of his species, into promising good behaviour by threatening to reveal
  what his kind are really up to.
- Essun and her party agree to enter the commune, mostly because Essun still has
  slim hope of finding her daughter within it.

## Chapter 16

- Syenite and Alabaster awake on an island to which they have been brought by a
  Stone Eater who Alabaster knows and calls Antimony, even though she has no
  name.
- He explains to Syenite that Stone Eaters can move through rock as if it was
  air.
- Being on an island is terrifying for Syenite as islands are particularly
  susceptible to the frequent earth shakes.
- Alabaster's acquaintance with this Stone Eater both surprises and does not
  surprise Syenite.
- They start to piece together the events of the last few days.
- Alabaster thinks the Guardian who attacked them was probably also the person
  who poisoned him.
- He explains to Syenite that Guardians have factions just like any other
  collective.
- The Guardians likely knew about the submerged obelisk and, on believing a
  powerful Orogen had raised and activated it, decided to intervene.
- Syenite questions just how powerful Alabaster is.
- He says he is unsure himself as he is learning to do things which she was
  never trained to do in the Fulcrum and in fact, is having to forget his
  training in order to learn what he is capable of.
- Guardians have surgical implants which help them with their Orogen suppressing
  ability.
- Guardians are the children of Orogens but without any of their powers.
- With direct skin to skin contact, a Guardian can force an Orogen to use its to
  power on itself.
- The weapons that Guardian uses, such as the knife, also suppress Orogen's
  power.
- These affects are temporary.
- Syenite and Alabaster visit a local town.
- Syenite learns that Alabaster has been there before.
- He reveals that these people do not kill their roggas but instead, put them in
  charge.
- This is why the island communities have inexplicably survived many seasons.

## Chapter 17

- Damaya's first year at the Fulcrum is coming to a close; she has both been
  lonely and regarded as a loner since the incident.
- However, she is progressing in her control and is told that she may be able to
  set the first ring test soon.
- She spends her free time wandering the ancient building, in particular the
  deserted portions of it.
- Wandering back, she encounters a girl she has never seen before.
- The girl tells her she is of the leadership class and has come here to
  discover a secret at the heart of the Fulcrum and asks for Damaya's help.
- Together, they discover the secret room with a massive depression in it, which
  clearly represents something which used to sit there.
- On the inside of the depression, there are spikes of iron.
- A Guardian catches them but the leadership class girl essentially pulls rank.
- They are taken separately and Damaya is questioned by a Guardian alone.
- This Guardian act strangely and appears to be speaking with someone else's
  voice, asking Damaya if she touched the iron spikes in the depression.
- Schaffa walks in as this Guardian has grasped Damaya's hand.
- He kills her and removes the surgical implant from the base of her skull in
  front of Damaya.
- He explains to Damaya that this sort of malfunction in the Guardian is not
  necessarily an uncommon thing but that he is sorry she had to see it and sorry
  she has been hurt.
- Finally, he tells her that she must set her first right test immediately.
- She has proven herself to be a trouble maker and so must prove herself to be
  useful in order to survive, she realises.
- She despairs of the treatment handed out at the Fulcrum.
- Before going to take the test, she tells Schaffa that she wishes her rogga
  name to be Syenite.

## Chapter 18

- Essun is taken underground: the ruined town above is a deliberate ploy to keep
  the true underground commune secret.
- Underground, the commune is like a vast mine and different to anything Essun
  has seen before.
- It appears to have been sculpted out of stone using Orogen control that Essun
  thought was impossible.
- There are also numerous enormous crystals which have been moulded into
  structures.
- Hoa is disturbed by some of what he saw and Essun is interested to find out
  why.
- The underground mines are centuries-old and civilisation after civilisation
  has built upon them.
- The current community is only about 50 years old, Ike(?) says, though this
  underground dwelling is clearly older.
- She has an ability to attract origins towards herself; she neither understands
  how she is doing this nor what she is doing.
- There are complex mechanisms to navigate the vast underground chasm.
- Essun realises they are powered by Orogeny Ike(?) confirms, although no one
  knows how this is done.
- She asks Ike(?) if she can leave and worryingly, she does not respond.

## Chapter 19

- Alabaster and Syenite consider their predicament.
- Given the destruction of the seaside town, it is likely the Fulcrum thinks
  they have died.
- Alabaster says his Guardian can no longer track him and that Syenite's
  Guardian will also have lost their link with her when her Orogen power was
  suppressed.
- With her power recovered, Syenite can feel better than she did before and
  senses are hexagonally shaft extending miles beneath the surface, the same
  shape as the obelisk at the Fulcrum.
- Syenite and Alabaster attend a village meeting where a non-Fulcrum rogga
  speaks.
- Syenite is attracted to him and realises Alabaster is too.
- Selflessly, she offers to help set him up with Alabaster.
- At end of chapter, Syenite realises she is pregnant.
- This is a little daunting for her as a villager has recently asked her if she
  would give any baby she and Alabaster might have to them: the village needs
  Orogens.

## Interlude

- A biographical or autobiographical entry: the narrator is reminding the
  intended reader that there was some joy in her life.
- She also tells them that there are many factions in war.
- In this case, not just Orogens and Stills.
- Also to be considered are the Stone Eaters, the Guardians and Father Earth
  himself.
- Tells the reader that as they rested, these forces gathered strength.

## Chapter 20

- Two years have passed and Syenite, Alabaster, their child and the community's
  'feral' rogga, called Innon, are living together.
- Alabaster thinks it is unsafe for them to return to the mainland and wishes
  Syenite would be more restful.
- Alabaster reveals that their infant son is able to skilfully use Orogeny even
  at such a young age.
- Syenite wants to go out on the raiding/piracy missions which this town uses to
  gain resources.
- Syenite eventually convinces Innon that she would be useful on a pirate
  mission.
- Indeed, she uses her powers to help them in one of the missions.
- She asks to be taken to see the village she destroyed.
- She recounts the mythology of the Earth in which people killed Father Earth's
  only child and it is as a response to this that the constant shakes happen.
- The reason Syenite is remembering this ancient myth is because of the scene of
  devastation in front of her.
- The seaside town is now unstable volcanic wasteland.
- It is unlikely many survived.
- Syenite is horrified by her actions and by the lack of intervention by the
  Fulcrum to calm the area.
- She channels all the power she can to quell the volcanic activity.
- However, she has forgotten that she is on a ship and her action causes tidal
  waves.
- Weakened, she tries to quell them but cannot.
- However, again displaying power beyond belief to Syenite, Alabaster is able to
  sense and calm the waves from afar.
- As the ship turns to leave, Syenite thinks she see someone and quickly
  realises that the Fulcrum did not abandon this place, but was ordered to do so
  by the Guardians, presumably because the Guardians guessed that the Orogens
  who caused this devastation would return and try to fix the mess.
- They now have evidence that Alabaster and Syenite are still alive.

## Chapter 21

- Syenite has realised and confronts Tonkee that she is in fact the same young
  leadership class girl who snuck around the Fulcrum with her in her youth and
  discovered what they now refer to as the socket.
- This also effectively confirms that Essun is Syenite, something I don't think
  was explicitly defined but which of which I was fairly certain when the young
  girl at the Fulcrum said she was taking the name Syenite: the three stories
  therefore have been three parts of Syenite's life.
- Tonkee has been following Syenite through basic investigative techniques.
- She has also been using her parents' significant resources to investigate
  obelisks.
- She believes the socket is where all obelisks were created.
- She believes only some Orogens can control obelisks & Syenite is one of them.
- She had lost track of Syenite but discovered her again when two obelisks
  started to move towards her.
- Tonkee says that obelisks can form relationships with Orogens.
- One of these obelisks was the amethyst obelisk which Syenite had encountered
  with Alabaster.
- Syenite however does not have any knowledge of the other obelisk that was
  tracking her.
- Later, Syenite discovers that her friend from her original community, Lerna,
  is also here.
- There is a clear attraction between the two.
- He tells her that her daughter is not here but that he is confident she will
  find her.
- Finally, Syenite speak to Hoa, confirming that he is a Stone Eater.
- He tells Syenite, rather strangely, that he likes her and this is why he has
  been kind to her.
- He also tells her that, rather unexpectedly, Alabaster is also here at this
  commune, wishes to speak to Syenite and is dying.

## Chapter 22

- Alabaster recounts some history for Syenite.
- Apparently, during times of need, the Empire (Sanzed) turned to cannibalism
  and developed a taste for Orogens.
- This persisted after the shortage of food was solved.
- The Orogens themselves volunteered to found the Fulcrum to save themselves
  from genocide.
- Alabaster urges Syenite to settle, potentially with Innon, and have another
  child.
- Syenite awakes one day to see Alabaster bristling with fear and anger.
- A fleet of four ships, with Guardians among the crews, is coming for them.
- Furious, Alabaster travels to the top of the island and begins to fight them,
  despite the great suppressing power they are using to fight against him and
  the presence of an Orogen on their side as well.
- Incredibly, Alabaster raises a stone wall encircling the island to protect
  them.
- He rains rocks on the ships as he is unable to directly affect them with his
  power.
- Syenite attempts to help and manages to crush one ship with a splinter of rock
  she peels from a cliff.
- As she goes towards Alabaster, we learn that these ships are equipped with new
  technology – cannons – and they fire at Alabaster, not hitting him but
  knocking him over and breaking his concentration with the explosion.
- A Stone Eater uses the distraction to draw Alabaster into the ground.
- His last words to Syenite are to protect their son from the Fulcrum and the
  Guardians.
- They confront the final ship and Guardians, but one of the Guardians
  breaks through and kills Innon in front of her.
- The other Guardian on the ship is revealed to be Schaffa, who correctly
  identifies Alabaster and Syenite's son and says he looks forward to bringing
  him to the Fulcrum.
- Enraged, Syenite draws all the power she can from the nearest obelisk (as
  Alabaster was doing earlier,) raises a spear of stone which splits the island,
  destroys the Guardians and nearly everything else in a straight line from her.
- Their child does not survive but Syenite does.
- The chapter concludes with some narration which is revealed to be from Hoa.
- The act of power by Syenite when she stopped Alabaster's initial shake alerted
  her to the attention of his kind and he says that he was drawn to her and
  fought off others of his kind who are similarly drawn to her and has been
  protecting her for some time before he introduced himself to her.

## Chapter 23

- Syenite goes to meet Alabaster, who is watched over by the Stone Eater
  Antimony.
- He is turning to stone, has lost an arm and is barely alive.
- He tells Syenite he understands why he killed their child but you will never
  forgive her for it.
- He asked her if she has learnt to control the obelisks yet.
- He confirms that he was the cause of the massive rift that started this
  Season.
- He is happy about what he did and thinks that the capital Yumenes got what it
  deserved.
- He asked Syenite if she can help him by making it even worse than it already
  is.
- A moment of revelation passes over Syenite where she realises Alabaster was
  never mad but was right all along.
- The novel can close with Alabaster asking Syenite if she has ever heard of the
  moon.

[Home](/blog "ohthreefive")
