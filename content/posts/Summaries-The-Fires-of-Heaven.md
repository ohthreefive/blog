+++
         
date = "2020-08-04T21:13:52.579Z"
title = "Summaries: The Fires of Heaven"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Fires of Heaven

_Robert Jordan_

> *"With his coming are the dread fires born again. The hills burn, and the*
> *land turns sere. The tides of men run out, and the hours dwindle. The wall*
> *is pierced, and the veil of parting raised. Storms rumble beyond the*
> *horizon, and the fires of heaven purge the earth. There is no salvation*
> *without destruction, no hope this side of death."*

> Fragment from The Prophecies of the Dragon believed translated by N'Delia
> Basolaine First Maid and Swordfast to Raidhen of Hoi Cuchone (circa 400 AB)

## Prologue

- Elaida is holding council.
- Her followers are leading the conversation, with one in particular showing a
  lack of respect.
- Galad has vanished and Gawyn, leading the Younglings, are barely under their
  control.
- Elaida watches the Aes Sedai, Alviarin in particular, with anger.
- They have lost Mazrim Thine and want to keep this fact from the world
  to protect their reputation.
- As the women prepare to leave, Elaida finally speaks, berating the women and
  telling them the priority is Rand.
- The Tower must control the Dragon Reborn.
- Elaida dismisses all but Alviarin and prepares to receive Padan Fain.
- Fain prepares to use Elaida for his means.
- He has heard she is stronger, but more likely to break, than Siuan Sanche.
- The Forsaken Rahvin is in the Royal Palace of Andor, with an Aes Sedai and
  her Warder under his Compulsion.
- He controls Morgase the same way (as Gaebril).
- Lanfear arrives and tells him to expect more Forsaken, but that it is not an
  attack.
- Sammael and Graendal arrive.
- Lanfear tells them to have some fear of the Dragon Reborn: four of their
  number are dead having underestimated him.
- She urges them to work together; Rahvin agrees, inwardly thinking only of his
  own gains.
- They clearly all hate and mistrust each other.
- Sammael still wears a scar Lews Therin gave him, despite having the power to
  make it vanish.

## Chapter 1: Fanning the Flames

- Siuan, Min and Leane (the Amyrlin's former second-in-command) have had a
  problem in their flight from the White Tower.
- Logain got in a scuffle and caused a fire, fleeing before local law
  enforcement arrived.
- Gareth Bryne, Morgase's former Captain-General, is now back in his home town
  and called upon to judge the three women.
- He binds the women by oath to work for him.
- Siuan tells Min she means to escape as soon as possible, but before they are
  even delivered to Bryne's manor, Logain frees them.
- Min again sees glory in this man's future.
- As Bryne prepares to go after the fugitives, he hears news from afar:
  Callandor was drawn in Tear, Elaida is Amyrlin, Siuan and Logain were executed
  in Tar Valon.
- Alteima visits Queen Morgase, fleeing Tear in search of a rich husband.
- Gaebril (Rahvin) interrupts, using Compulsion to send Morgase away and getting
  all information from Alteima about Rand.

## Chapter 2: Rhuidean

- In Rhuidean, Rand watches Moiraine load the _ter'angreal_ for study, including
  the twisted red door frame.
- He meets with the Aiel who have already come to him.
- The Shaido and Couladin remain a concern; the chiefs of the six clans are
  confident that the others (five more) will join, but not the Shaido.
- Rand meets with Moiraine and Egwene and talks without diffidence to the Aes
  Sedai.
- Moiraine has found what looks like a Shayol Ghul seal which she can easily
  damage with a simple knife.
- She does not know what this means but does not think it is good.
- When Moiraine leaves, Egwene berates Rand for his rudeness to Moiraine.
- Rand thinks Egwene reminds him of Ilyena, then realises he does not know who
  Ilyena is.

## Chapter 3: Pale Shadows

- Rand and Asmodean spar words.
- Asmodean has taught him much already.
- They discuss linking: up to 13 female Aes Sedai can link but men cannot link
  to each other without a woman.
- Strength is likened to physical strength: the strongest female Aes Sedai can
  overpower some male Aes Sedai; linked women can be stronger than any one man.
- Mat is drinking and testing his luck, being more aware of his memories of the
  Old World than ever.
- Melindhra of the Shaido approaches him, and when she does not know what
  Daughter of the Nine Moons means, Mat happily accepts her advances.

## Chapter 4: Twilight

- Rand, who accepted the Maidens of the Spear as his honour guard, enters the
  Roof of the Maidens, the only man allowed to do so except _gai'shain_.
- He has a back-and-forth with Aviendha, again, and it seems more and more
  likely that she is in fact in love with him.
- Isendre, known to Rand as a Darkfriend but framed as a thief by Lanfear,
  enters the room, surprised Aviendha is there.
- She thinks Rand is interested in her as he lessened her punishment for
  thievery, knowing the truth.
- He cannot let anyone know that he knows that there are Darkfriends among the
  travellers with them, or else Asmodean (and Rand) will be exposed.

## Chapter 5: Among the Wise Ones

- Egwene is summoned to the Wise Ones.
- They are hearing Moiraine's opinions.
- Rand's plan is to unite the Aiel before entering the world with them as his
  army, to unite the rest of the nations for the Last Battle.
- Moiraine thinks this will start another Aiel war; better to let the Amyrlin
  unite the nations under Rand and bring them to him.
- They are all unaware that Siuan Sanche has been deposed.
- The Wise Ones dismiss any authority Moiraine's opinions carry.
- More Tinkers have been entering the Waste and some Aiel, having heard their
  true history from Rand, have been joining them.
- They debate whether Aviendha should be asked to sleep in the same tent as
  Rand; Aviendha argues strongly against this.
- Egwene is trying hard to find Elayne and Nynaeve in _Tel'ian'rhiod_ for
  communication.
- Later, the Wise Ones discuss a sister-wife and Aviendha asks Egwene if that
  could be accepted where she is from (yup, definitely in love with Rand).
- The two women realise the Wise Ones are trying to play them off each other as
  motivation for their training.

## Chapter 6: Gateways

- Rand smells foulness and is set upon by three Darkhounds: creatures he has not
  previously encountered.
- His sword work has continued to improve and he beheads all three with his One
  Power sword easily.
- They re-form, however, and he obliterates them with Balefire.
- Moiraine arrives to check he was not touched by them.
- Rand quickly leaves, thinking others may have been targeted.
- He opens a Gateway and uses Skimming, musing briefly that Asmodean thinks
  Travelling is more efficient but Rand does not yet have its mastery.
- He saves Mat from three more, with Mat thinking he remembered the attack
  getting closer to him than it seems to have done.
- Moiraine explains that Balefire causes something to cease to exist both now
  and in the past, the length of time being relative to the power of the
  Balefire.
- It therefore has a profound effect on the Pattern and even the Forsaken are
  wary of using it.
- Moiraine cannot heal Mat while he wears the Foxhead medallion.
- Rand and Moiraine (& Lan) have a stand-off and it appears Moiraine backs down,
  begging Rand for the opportunity to guide him and promising to serve him.
- Lan thinks Rand is being arrogant and ungrateful.
- Rand orders Moiraine to leave Mat and his Foxhead medallion, presumably a
  _ter'angreal_, alone.
- He then checks on Asmodean and the two _ter'angreal_ to the Choedan Kal: they
  are safe.
- Lanfear arrives and sees the _ter'angreal_.
- She asks Rand to join her: together they would be the strongest.
- She informs Rand that Rahvin sent the Darkhounds and that he rules Caemlyn
  with Morgase as a puppet.
- A Maiden arrives: the Shaido are on the move, as are the five other clans that
  have not joined Rand, though not together.
- Rand wants to follow the Shaido at first light.

## Chapter 7: A Departure

- Rand is preparing to leave Rhuidean.
- He suggests the Clan Chiefs could ask Ogier to help rebuild.
- Egwene has managed to find dreams, pleasing Amys.
- Aviendha seems upset but will not tell Egwene why.
- Moiraine reflects on her memories from within the three-ringed _ter'angreal_.
- One in particular taught her not to try to use sex to control Rand.
- She has packed numerous artefacts, including the twisted red doorframe, to
  take back to Tar Valon.
- Lan is not sure she should have demurred to Rand.

## Chapter 8: Over the Border

- Fleeing the Black Sisters in Tanchico, Elayne and Nynaeve are using the Power
  to evade bandits and other miscreants as they make the long way back to Tar
  Valon.
- An encounter with Whitecloaks scares Nynaeve, but they appear to have avoided
  discovery.
- At length, Juilin, Elayne and Thom convince Nynaeve they should stop for a few
  days rest.

## Chapter 9: A Signal

- Elayne, Nynaeve, Thom & Juilin arrive in the town of Mardecin.
- Nynaeve questions Elayne about why she flirts with Thom but asks for messages
  of love to be sent to Rand.
- Nynaeve is worried about facing Moghedien again, especially if she is caught
  unaware or when not angry and therefore able to channel.
- As she walks the town, she points out a secret sign of a Yellow Ajah spy in a
  shop window and goes to investigate.
- The proprietor is nervous and offers to make them tea.
- The message is "All sisters are welcome to return to the White Tower. The
  Tower must be whole and strong."
- As Nynaeve is pondering the strange message, she realises the tea is drugged
  and begins to lose consciousness.

## Chapter 10: Figs and Mice

- After failing to escape the drugged tea, Elayne and Nynaeve are rescued by
  Thom and Juilin.
- They question the spy – she and her assistant both strenuously deny being
  Darkfriends.
- They were alerted by a message from the Amyrlin which came about a month ago
  telling them to look out for someone matching Elayne's description.
- A message has already been sent to the White Tower to report their capture.
- Uncharacteristically, Nynaeve displays gratitude towards Thom and Juilin for
  the rescue.
- They decide to disguise Elayne and leave now under the guise of noblewoman
  rather than spice traders.
- When the spy finally wakes up having come round from being drugged by her own
  tea, she sends a message to the White Tower and another undisclosed location.

## Chapter 11: The Nine Horse Hitch

- Logaine has taken over the group of him, Siuan, Min and Leane.
- Siuan has identified a spy for the Blue Ajah from whom she means to get
  information about where the Blue Ajah fled to from the White Tower.
- As she is no longer Aes Sedai herself, she has to present herself as a
  spy but struggles to do this as she is so used to her authority and being
  granted instant respect.
- She manages the task, however, and the spy gives her a name which Siuan knows
  is the birthplace of the first Blue Ajah.
- Siuan thinks she has somehow gained the attention of the Whitecloaks and that
  is one of the reason she wants to depart quickly.
- She has to use Leane's new-found ability to flirt in order to get Logaine to
  leave the inn in which they were talking and continue with their mission.
- Min wonders whether or not she is jealous of Leane's ability to casually
  flirt and control men - she wonders if it could help help with Rand.

## Chapter 12: An Old Pipe

- Gareth Bryne is leading a small group chasing the group who escaped from him.
- He is strongly telling himself that his motivation is to find out why a woman
  – Siuan – would break her oath to him.
- He reflects on his exile by Queen Morgase after the appearance of Lord Gaebril
  – he still does not understand it fully.
- For 1000 years, his family have been loyal to the Queen of Andor; his tobacco
  case bears his family coat of arms and as he looks at it he thinks he needs a
  new pipe.
- It is not difficult to track the women – talk of three pretty woman is all
  over the town and he knows they went west.
- However, he also hears that for some reason the Whitecloaks may also be after
  them.
- He is determined to get to them first.
- We also learned that he once met the Amyrlin Seat; clearly he no longer
  recognises Siuan.
- She and Elaida had Morgase tell him to pull guards back from the Murandian
  border years ago and he never found out why.

## Chapter 13: A Small Room in Sienda

- The atmosphere in the wagon is fractious.
- This is mostly due to Nynaeve; Elayne tries to settle her.
- They are briefly stopped by a travelling menagerie but move on, not long after
  Nynaeve upsets the proprietor.
- They move on to Sienda where they need to find a room for the night so Nynaeve
  can meet Egwene.
- Whitecloaks are there.
- Elayne tells Nynaeve to pass a message via Egwene that she loves Rand and Rand
  alone.

## Chapter 14: Meetings

- Nynaeve enters _Tel'aran'rhiod_ and meets Birgitte.
- Birgitte is still a little reluctant to divulge information to Nynaeve as it
  is one of the things the reborn heroes are not meant to do while they wait in
  _Tel'aran'rhiod_.
- She has been spying on the Forsaken but has not seen Moghedien.
- Nynaeve next meet Elayne and one of the Wise Ones – Melaine.
- They exchange information, during which Nynaeve has to lie and say it is her
  that has been spying on the Forsaken.
- For this, she is chastised by Melaine who says she should not enter
  _Tel'aran'rhiod_ without a Wise One's permission.
- Nynaeve becomes understandably angry: have these women forgotten that she
  managed to defeat Moghedien.
- That gives her the idea to try to find out what is happening in the White
  Tower herself.
- Elayne and Nynaeve discussed that Moiraine has agreed to take orders from
  Rand, strange as that may seem, that he is heading for the Jangai Pass in
  pursuit of the Shaido and that something must be going on at the White Tower
  given the message Nynaeve received from the spy.

## Chapter 15: What Can Be Learned in Dreams

- Nynaeve visits the White Tower in _Tel'aran'rhiod_.
- She finds Egwene there and is surprised that the Wise Ones have allowed her to
  visit this place alone.
- Egwene dresses her down, highlighting the dangers of this place of
  which Nynaeve is unaware.
- Nynaeve is hugely frustrated by the switching of roles of authority between
  the two women (despite her own frustration at Moiraine's assuming authority
  over them; she seems to miss the irony.)
- She thinks she lost moral authority when she lied to Egwene, something she had
  not done before and something which she would not tolerate when she was wisdom
  of Emond's Field.
- The women discover that Elaida is Amyrlin Seat but do not know what
  happened to Siuan.
- They now know that the Ajah are split and their next move is to find the Blue
  Ajah.
- Between them, they decide to tell Moiraine and Rand but are unsure when would
  be the best time to tell the Aiel.
- The White Tower no longer supporting Rand may affect the Aiel's loyalty
  to him.
- After the woman part, Egwene goes to alert Moiraine, reflecting that she is
  surprised she was able to hide from Nynaeve that she had entered
_Tel'aran'rhiod_
  without the Wise One's permission, something which she does indeed still need.
- Egwene is somewhat shocked by the objective and calm way in which Moiraine
  responds to the news.
- Moiraine says there are still more important things in the world than her
  friend Siuan.
- The two women discuss what to do about the White Tower and the Forsaken
  in _Tel'aran'rhiod_.
- Moiraine thinks Lanfear may not be a problem much longer.
- When Egwene leaves, she reflects that Moiraine spoke to her close to as an
  equal.

## Chapter 16: An Unexpected Offer

- Elayne and Nynaeve run into Galad at breakfast.
- He has joined the Whitecloaks.
- He recognises his half-sister and tells her what truly happened in the White
  Tower, though he believes Elaida's story that Siuan was executed.
- He is happy that Elayne was free of White Tower plotting and offers to take
  her home.
- As soon as she gets away from him, Elayne tells Nynaeve once more that Galad
  will do the right thing at all costs, meaning he will either escort them back
  to her mother or turn them over to the Whitecloaks.
- Therefore, they make a plot to escape, making use of the menagerie they came
  across the previous day.

## Chapter 17: Heading West

- The group sneak out of their lodgings and meet back up with the travelling
  menagerie.
- They are able to secure passage.
- Nynaeve and Elayne identify one of the animal handlers as a Seanchean,
  confront her and ask that she teach them more about the Seanchan.

## Chapter 19: Memories

- Queen Morgase is thinking about her recent behaviour being strange when a
  guard approaches her, bearing news of rebellion in the Two Rivers.
- There is something about the animosity the guard displays towards her which
  galvanises her and she confronts Gaebril.
- When he tells her the problem is already dealt with, and uses Compulsion to
  dismiss her, her anger gives her the strength to break free of his orders once
  she has left his presence.
- She visits a retired servant who is not afraid to tell her the truth.
- She is no more than a lap-dog to Gaebril, who in addition has been polygamous.
- Recognising the guard's earlier animosity towards her as his disappointment in
  her actions, she recognises him as loyal and sends for him in secret.
- They leave the city, collecting other loyal men including Basel Gil.
- Morgase means to approach many of those she exiled for support and it makes
  the most sense to approach Gareth Bryne first.
- The Peddler approaches a locked door in the White Tower, behind which he knows
  lies his dagger.
- He picks the lock with ease and thinks contemptuously about how poorly guarded
  the Aes Sedai have left these things.
- Just after finding the dagger, he hears a noise, intercepts and kills an
  Accepted who was checking on the door.
- Before he can leave, he finds himself fixed in space and Alviarin enters.
- The lock was of course warded but his first conclusion, given that she is here
  alone and has raised no alarm, is that she is Black Ajah.
- She neither confirms nor denies it but ask for answers from him.
- Despite not wishing to do so, he realises the easiest thing to do is lie and
  pretend he is just another Darkfriend taking orders from another of the
  Forsaken.
- Alviarin leaves, and though the Peddler knows she will seek to corroborate his
  story as soon as possible and learn his lie, he decides that leaving Tar Valon
  now, rather than killing her and continuing his search for the Horn of Valere,
  is the better option.
- All his focus is still on Rand and now he feels whole again.

## Chapter 20: Jangai

- Rand and company arrived in Jangai through which they learn Couladin passed
  days prior, massacring the people as he went.
- The Aiel decry his behaviour: he has clearly abandoned their philosophies.
- He left a message with the survivors to tell someone (matching Rand's
  description) that this is but a taste of what he will do to him when the next
  meet.
- In the towering mountains, Rand is sure he can make out what looks like the
  ruin of port city, reminding him of how much the world changed during the
  breaking.

## Chapter 21: The Gift of a Blade

- In Jangai, Rand and the Aiel see to the burial of the dead while he also
  reflects on all he has learnt and the power he has gained with Asmodean's
  teaching.
- At night, Aviendha enters his tent and presents him with a gift.
- It is the blade of King Laman of Cairhien, the king who cut down the sapling
  of the Tree of Life which had been an Aiel gift to Cairhien for their
  hospitality when the Aiel were retreating in times passed.
- This king's act had sparked the last time the Aiel left the Waste: to
  kill him and return.
- Rand immediately recognise the blade as power-forged but the handle and grip
  are more ornamental than functional.
- He tells Aviendha that he will keep the blade but she can keep the rest – for
  one thing he cannot comprehend how she managed to afford such a gift and this
  way she will recoup much of its value with these parts of it.
- This sparks yet another argument between them about whether or not she is in
  his debt.
- They have a brief discussion about all the royal wedding customs; the two are
  clearly in love.

## Chapter 22: Birdcalls by Night

- Mat is spending his nights with Melindhra.
- Suddenly, Rand's wards announce an attack.
- Cursing his luck to be tied up with Rand, Mat fights valiantly, including
  slaying a Myrddraal with his power-wrought, raven-marked spear with which he
  returned from the twisted red doorframe.
- He notes the Trollocs were chanting Sammael's name.
- He also notes that the attack was futile and he does not like this.
- Rand saves Aviendha from a Dragkhar attack outside his tent.
- She turns and hurls fire at him with the power, causing him to react
  furiously.
- When he calms down, he realises there was a Dragkhar behind him and she has
  dealt with it.
- Moiraine arrives, and Rand is not sure why she did not come sooner given
  that she would have been able to sense the Dragkhar.
- The Maidens feel they have failed Rand but he tells them they have not.
- He goes to speak to Asmodean who is not sure whether Sammael is truly behind
  the attack.
- Rand replies that he thinks he is but does so referring to himself as Lews –
  the first time he has done this.
- He is unhappy at himself for doing so; he wants to remain Rand al'Thor.

## Chapter 23: "The Fifth, I Give You"

- Egwene is wandering the camp as the party make their chase of Couladin.
- Moiraine keeps a very close eye on the wagons carrying the _ter'angreal_.
- She overhears Rand tell the Aiel that as they pass through villages and
  towns, they must take no bread or other food and must kill no man who does not
  first attack them.
- He does, however, grant them permission to take their fifth as is their
  custom.
- Some of the Chiefs are a bit disgruntled by this.
- Egwene speaks to Aviendha, who expresses desperation to be freed from sleeping
  in Rand's tent.
- A Wise One comments that as she has not been able to attract Rand's eye,
  perhaps it is time to find her another husband.
- Egwene still feels there is something Aviendha is not telling her.

## Chapter 24: A Message Sent

- Egwene enters _Tel'aran'rhiod_ early, hoping to speak to Nynaeve alone.
- Secretly and ashamedly, she hopes she still has the upper hand with the other
  woman.
- Instead, she sees Elayne and another person who is unmistakably Birgitte.
- Before she can be sure, the Wise Ones join her and chastise her greatly for
  entering the dreamworld early.
- She cuts them off to say that it is not Nynaeve who has come but Elayne.
- The Wise Ones then round on Elayne, saying it is dangerous for her to be here
  without their teaching.
- Elayne cuts them off by reminding them she is not their pupil and instead
  serves the Amyrlin Seat; the Wise Ones counter by questioning why someone
  would serve a dead person.
- Elayne and Egwene exchange words and Egwene tries to convey a secret message
  that she needs to speak to Nynaeve and Elayne alone.
- She is unsure if it was understood.

## Chapter 25: Dreams of Galad

- Elayne did understand Egwene's code and the two meet in the White Tower.
- They exchange some information but Elayne holds to her promise not to divulge
  information given to her by Birgitte.
- After Elayne leaves, Egwene decides to try to find out the name of the
  sanctuary for the Blue Ajah that Nynaeve and Elayne seek.
- While in the Amyrlin Seat's study, Galad walks in and suddenly Egwene
  finds both Leane and Siuan in the room.
- Recognising this as a dream, she flees, finding herself in Emond's Field,
  where she sees a wolf and eagle banner flying.
- Next she is being spoken to by her mother, who presents Gawyn as her husband.
- Egwene realises something is wrong and wakes herself.
- Moghedien steps out from the shadows, surprised, disappointed and impressed
  that this woman was able to escape her dream within a dream.
- Her only interest in Egwene now is that she may help lead her to Elayne and
  Nynaeve.
- She now also has an interest in Birgitte.
- She means to find them all and deal with them from the shadows.

## Chapter 26: Sallie Daera

- Logaine is succumbing to the effects of the stilling, with only Leane's
  flirting being able to motivate him.
- His aura is as bright as ever to Min, however.
- Min is sure that Siuan does not know exactly where she is going, but almost as
  soon as she has that thought, they come across the Blue Ajah.
- Sheriam is there and asks why Min has brought Logaine.
- Min turns their attention to the unrecognised Siuan, who is identified after
  questioning.
- Min wants to return to Rand.

## Chapter 27: The Practice of Diffidence

- Siuan must use her most skilful manipulation in discussion with the Aes Sedai.
- She deliberately launches out on the offensive, instructing them on what they
  must do as if she is still Amyrlin Seat.
- To strengthen her argument, she reveals a trickle of her knowledge: that the
  Red Ajah manipulated Logaine, brought him to the attention of the world so
  that they might be seen to capture and rid the world of him.
- Siuan thinks Logaine will expose Elaida publicly for this in an act of
  revenge.
- She also draws attention to her usefulness in terms of the knowledge she has
  of the Aes Sedai eyes and ears throughout the world.
- In response, the Aes Sedai draw her attention to her new status and
  firmly put her in her place; this seems to have been Siuan's plan but
  pretending diffidence is difficult for her.
- She also tells them she believes Rand went to the Aiel Waste after Tear.
- Siuan's efforts have opened the door for Leane's comments to be more easily
  accepted by the sisters, especially when delivered in a more reasonable
  manner.
- Her suggestion is that the exiled Sisters elect their own Amyrlin Seat to
  be presented to the world when Elaida's treachery is exposed.
- In addition, she says that this Aes Sedai should not have been in the
  Tower at the time Siuan was deposed to minimise claims of collusion.
- Clearly, Siuan has Moiraine in mind.
- Some of the actions of Siuan where against the laws of the Aes Sedai but
  all present agree her punishment was unnecessary and overly harsh.
- They seem to accept the women's suggestions and offer of help.
- A warder arrives talking of an approaching group of riders, one of whom is
  surely Gareth Bryne.
- As a great commander, this makes their options (capture or kill) much more
  dangerous.
- Remembering her part (with Elaida) in convincing Morgase to bend Gareth Bryne
  to their will, and their recent encounter, Siuan loses her calm and blurt
  out instruction.
- She is recognised immediately.
- Sheriam orders Gareth Bryne to be brought to her and Siuan to explain why he
  is chasing her; she can see no option but the truth.

## Chapter 28: Trapped

- As Gareth Bryne is brought before the Aes Sedai, he knows he is trapped but
  uses his considerable expertise to think of a way to get the most out of the
  situation.
- He all but volunteers to be the general for the Aes Sedai army, but
  insists on independence in battle decision-making and on the support of the
  Aes Sedai come what may.
- They grant him this and also that Siuan will fulfil her oath.
- Menu is outside with Logaine, who has been muttering some words.
- When Siuan enters, she intercepts her to tell her that she saw an aura around
  Gareth Bryne which included Siuan.
- If they stay close, they will live; if parted they will both die.
- Min does not like viewings with an 'if', all but one before have
  concerned certainties.
- Siuan says she must speak to Logaine.

## Chapter 29: Memories of Saldaea

- The wagonmaster recalls his visit from Lanfear, instructing him on his mission
  for now, including keeping a close eye on Asmodean (the true identity of
  whom the wagonmaster does not know).
- He is visited by Isendre, who has been shaved all over and beaten with nettles
  for her latest attempt to sneak into Rand's room.
- She has been doing most of the spying on Asmodean: by sharing his bed.
- She reveals to the wagonmaster that she believes Aviendha has been sharing
  Rand's bed and she thinks she has no chance because of this.
- Sensing desperation in the women which may reveal him as a Darkfriend to the
  Aiel, he kills her.
- He recently received a cryptic message from an unknown Darkfriend written in
  what he thinks was Aiel script.
- He thinks he has to discover this other Darkfriend and see if she can do
  better than Isendre.

## Chapter 30: A Wager

- Rand is musing on his realisation that Asmodean does not know everything;
  he has displayed little knowledge about controlling the weather.
- Rand has to shift his thought away from considering the Forsaken to be no
  less than omnipotent.
- He recalls another of their number knows even less about the weather.
- This final thought, however, is one of Lews' memories.
- A party from Cairhien arrives, telling of the city being under siege by
  Couladin.
- They believe they can hold for some time; Rand makes plans to be there in
  seven days.
- One of the Lord's finds this statement incredible and places a large wager
  that Rand and his Aiel cannot make it there so quickly.
- Rand tasks the Lords to return and let the city know of his coming but keep
  it secret from the Shaido.
- He also sends some of his Aiel to scout the Shaido, but not the Maidens.

## Chapter 31: The Far Snows

- Rand is hoping for some privacy when he enters his tent to find Aviendha naked
  within.
- She had assumed he would be gone for longer and was attempting a sweat bath.
- In her flailing desperation, she manages to channel a Gateway and Travel to
  somewhere snowy.
- Rand immediately realises she will not last long naked, blocks the Gateway
  open and follows.
- He rescues her from the cold and when she comes around, the two embrace.
- Before they return, she casually tells him that this changes nothing: he
  belongs to Elayne.
- Rand had been ready to marry Aviendha and feels that his relationship with her
  has not got any easier despite what has just happened.

## Chapter 32: A Short Spear

- Before returning to the Gateway, Rand sees a huge winged beast flying
  overhead (presumably a dragon) though he does not know what it is.
- It has two riders.
- He manages to stay hidden from it.
- Before he and Aviendha can reach the Gateway, he sees a group inspecting it:
  they are unmistakably Seanchean.
- Aviendha binds the two _damame_ and _sul'dam_ while Rand deals with the
  riders.
- Before he can get to the Gateway, he notices a _saidin_-woven cloak across it.
- He thinks Asmodean must have done this.
- Before going through, he exchanges words with one of the Seanchean nobles:
  since Falme, he is known to them and their invasion to reclaim the land they
  believe is theirs has stopped for now.
- The two are closely followed through the Gateway by a thrown Seanchean spear;
  Rand cuts it off mid-shaft.
- He decides to make this a keepsake to remind him of the Seanchean.
- Apparently news of his coupling with Aviendha has already spread.
- Asmodean confesses to cloaking the Gateway but only to hide it from unwanted
  eyes.
- Rand commands Asmodean to tell him two new things, things which Rand has
  not directly ask him about, every time they speak.

## Chapter 33: A Question of Crimson

- Thom is practising a knife-throwing exercise with Nynaeve; Luca watches on,
  impressed.
- Nynaeve absolutely refuses to wear the crimson dress Luca wants her to.
- She and Elayne talk, with Nynaeve explaining how she questioned the Seanchean
  too strongly and ended up with a swollen eye for her trouble.
- Elayne has been studying an _a’dam_ given to her by the Seanchean woman.
- She tells Nynaeve that she thinks she knows how it works – like Linking except
  all the control goes to only one of the channellers – and that she thinks she
  can make one.
- Nynaeve fails to grasp the significance, so angry is she at Elayne's curiosity
  with this hateful object.
- Elayne is more explicit: no one has been able to make a _ter’angreal_ in
  thousands of years and she thinks she might be able to.
- They debate their next move – Nynaeve has still not remembered the name of the
  town to which she thinks they should travel but is convinced she will do so;
  Elayne is worried about her stubbornness.

## Chapter 34: A Silver Arrow

- Elayne and Nynaeve discussed the latter's behaviour around Luca and her
  upcoming visit to _Tel'aran'rhiod_.
- Nynaeve enters and finds Birgitte, asking to be taken to Moghedien.
- She sees a group of Forsaken plotting against Rand.
- They retreat when Birgitte thinks it is prudent to do so but shortly
  afterwards, Moghedien appears and cuts Nynaeve off from _saidar_.
- She also knocks Birgitte unconscious.
- She begins to torture Nynaeve, saying that she is not planning to kill her.
- As she is about to use Compulsion to make Nynaeve bring Elayne to her, a
  silver arrow sprouts from her breast.
- Moghedien envelops Birgitte in darkness before she is able to loose a second
  arrow and retreats to Liandrin to be healed by one of the black sisters.
- Weakened by the healing, Liandrin tries to use compulsion on Moghediena but is
  easily cut off by the Forsaken.
- Moghedien again says she is not going to kill.
- Instead, she teaches Liandrin that compulsion is most effective when forcing
  someone to do something they already want to do.
- She commands Liandrin to "live" before shielding her from the True Source with
  a weave so intricate, it is unlikely anyone will be able to undo it.
- She tells the other Black Ajah that Nynaeve and Elayne are in a menagerie and
  they are to find them.
- In _Tel'aran'rhiod_, Nynaeve is weeping, regretting the death of Birgitte.

## Chapter 35: Ripped Away

- Nynaeve returns and is beginning to tell Elayne what happened when Thom and
  Juilin enter their tent carrying an unconscious Birgitte.
- Elayne shoos them away without much explanation - they do not know who she is
  - and Nynaeve tries to heal her.
- Despite drawing more power than Elayne thought imaginable, healing does not
  work as Birgitte is not really injured.
- Elayne convinces Nynaeve to let her try something and quickly bonds Birgitte
  to her just like Aes Sedai do to their Warders.
- Nynaeve is a little concerned about what the White Tower would think of this
  but it does seem to have helped.
- She is filled with regret – Birgitte was torn out of the dream world and may
  have lost her connection to the Wheel of Time – and anger towards Moghedien.
- She leaves to be alone and think about what they must do now that Moghedien
  knows they are with travelling menagerie.

## Chapter 36: A New Name

- Elayne explains the nature of their situation to Thom and Juilin: namely that
  they can enter dreams and are both hunting and being hunted by Forsaken.
- She keeps her promise not to reveal Birgitte's true identity, but this is just
  a show: Thom recognises her easily from stories, as does Juilin.
- Birgitte recovers to some extent and says she holds no grudge against Nynaeve.
- Her concern is for her partner, who has already been reborn but will not know
  to look for an already grown up Birgitte.
- She consents to be in Elayne's Warder.
- The next day, she shows her archery skills to Luca who declares that she must
  be Birgitte.
- Birgitte agrees to perform archery for his menagerie but gives a different
  name for him to use

## Chapter 37: Performances in Samara

- Birgitte and Nynaeve clash.
- Birgitte is fed up at Nynaeve's guilt; in fact she is insulted.
- She thinks Nynaeve is removing her agency.
- Birgitte knew her actions, knew the risks, and made her choices.
- Juilin performs on the rope, pretending to be drunk.
- Next Elayne performs and does so without channeling in case the Black Ajah are
  near.
- Her performance is daring and accomplished, but terrifying for Nynaeve.
- Nynaeve realises a loud man in the crowd is known to her: Uno the Shienaran.

## Chapter 38: An Old Acquaintance

- Nynaeve and Uno talk with Uno telling her she and some other Shienaran now
  follow on the heels of Masema, who has become Prophet for the Dragon Reborn.
- His ways are sometimes as severe as the Whitecloaks.
- He wants to help Nynaeve and Elayne in their flight.

## Chapter 39: Encounters in Samara

- Nynaeve is taken by Uno to meet Masema, now known as the Prophet.
- He has become a zealot: almost as fervent in his beliefs as the Children of
  the Light.
- During their encounter Nynaeve struggles to maintain affording him the respect
  he now commands.
- In the end, he neither hinders nor particularly chooses to help her and
  Elayne.
- Uno suggests they leave while they can.
- Three rulers of Ghealdan have fallen foul of him in a short period of time and
  the current one appears to be his follower.
- His power is probably not to be underestimated.
- Upon leaving, Nynaeve realises they are being followed by Galad.

## Chapter 40: The Wheel Weaves

- Galad catches up to Nynaeve.
- Surprisingly, he does not mean to detain her but wants to know about the
  safety of his half sister and why they chose the more dangerous option of
  running away from him.
- During his talk, he speaks the name of the town Nynaeve has been trying to
  remember for some time – Salidar.
- He says he will help Elayne and Nynaeve obtain a ship to take them back to
  Caemlyn.
- Nynaeve accepts the offer but intends to take the ship to Salidar instead.
- After leaving Galad, Uno offers the help of around a dozen Shienaran who have
  been following Masema but would happily follow her and protect her.
- The prospect of men offering to try and find her a ship and a guard of
  Shienaran raises Nynaeve's spirits.
- When she returns to the menagerie, Birgitte and Elayne chastise her for
  wandering away from the camp without explanation.
- Elayne is shocked that Galad has offered to help even though he does not
  know his offer is being manipulated.
- Birgitte's archery performance terrifies Nynaeve so much that she feels sick.

## Chapter 41: The Craft of Kin Tovere

- Rand and his collected armies and refugees are overlooking the besieged
  Cairhien.
- Among the refugees, a man called Kin Tovere was a master of working glass.
- Rand ordered the construction of a tower and Kin Tovere built two telescopes
  for him.
- While performing the last survey prior to battle, Rand sees two enormous
  spears thrown at the Shaido around Cairhien, one of them piercing two men.
- He was not able to see who or what fired them, though they came from the city.

## Chapter 42: Before the Arrow

- Mat decides yet again that he must leave Rand.
- This time, he does not feel the immediate pool of _ta'veren_.
- He goes to see Rand in his tent but finds only Asmodean there.
- Pouring over maps and, after Lan has entered, he begins to instinctively
  talk battle strategy.
- He explains how the true priorities are to stop Couladin from entering
  Cairhien and causing further destruction in the city they are here to save
  and also that some of the Aiel clans are still untrustworthy after the
  revelations Rand made about their past.
- It would be a disaster if they were to change sides during the battle and
  therefore some care should be taken to make sure Rand's army could not be
  caught between Couladin and traitors.
- When Rand finally enters, Mat tells him he will leave and Rand grants him
  leave, in fact saying that he does not need permission.
- After he has gone, we learn that Rand deliberately asked Lan to approach
  Mat on his own as he suspected he had learned more within the twisted red
  doorframe than just how to speak the Old Tongue.
- Rand also coldly reveals that he had no problem allowing both Perrin and Mat
  to leave him because he knows both will return, such is the draw of
  _ta'veren_.
- Rand recognises his own inexperience with battle and seeks council from the
  Aiel Chiefs.
- She also still has issues with untrustworthy Tairen Lords.

## Chapter 43: This Place, This Day

- Rand is anxious before battle.
- He is worried about how much death will come from the battle; Moiraine has
  retired from the battlefield to prepare to heal.
- Lan asked him why he has gone back to wearing a sword and Rand confesses
  that he feels using the power against Couladin is unfair.
- Lan suggests that Rand should avoid Couladin at all costs.
- As part of his plan, he has asked Aviendha and Egwene to attack using the one
  power from his tower.
- They are not yet bound by the oaths of the Wise Ones and the Aes Sedai,
  respectively.
- Before battle, the Maidens of the Spear approach and tell Rand that they will
  protect him from the tower.
- If the one power can shorten the battle and reduce the number of dead, he
  must also use it.
- Seeing that they refuse to back down, Rand concedes.
- During his assault, he finds it hard to fight back the memories of Lews.
- Making his way out of the city, Mat sees that a group of Tairens and
  Cairhienin are going to be ambushed by Aiel.
- He sees no option but to intervene and unwittingly finds himself leading a
  party into battle, cursing his luck.

## Chapter 44: The Lesser Sadness

- Rand, Egwene and Aviendha are near-exhausted in their tower, razing the field
  with Power-wrought lightning.
- Rand sees the Shaido have nearly entered Cairhien and knows this would be a
  disaster; he uses his strength to stop them.
- Suddenly the Maidens on the hill around him and his tower are struck by
  lightning; Rand suspects Sammael.
- His Tower crashes to the ground.
- Mat, the Tairens and Cairhienin have fought well but he sees more Aiel ahead,
  thinks he sees Couladin with them and believes they are making to attack
  Rand's position.
- Mat decides to draw them to his forces with cries of, "Protect the Lord
  Dragon," while taking them in the flank with some of his force.
- He sees the lightning and also assumes it was Sammael.
- The generals he is fighting with praise him; he hopes they do not find out he
  is little more than a child and is using broken memory and luck for strategy.
- Rand awakes and finds himself, Egwene and Aviendha badly wounded; many Maidens
  have suffered worse.
- He means to press on but is reminded that in his weakened state, Sammael could
  kill him easily.
- He decides to see what else he can do.
- Memories of Lews Therin and his dead wife flood his memory; he talks of
  wanting to find his people.
- He is told that many Aiel have come over to him and the battle is won.
- Asmodean subtly grants some Healing to Rand, who has collapsed with
  exhaustion.

## Chapter 45: After the Storm

- Mat killed Couladin during the battle.
- Melhindra has returned to him, holding no grudge for his leaving.
- The Tairen and Cairhien forces who were under his command now pledge their
  allegiance to him.
- He thinks there must still be some way he can get out.
- Rand awakes, the effort of healing him having caused Moiraine nearly to faint.
- Many Shaido escaped across the river and destroyed bridges behind them.
- In his slumber, he has been visited by Lords of both Tear and Cairhien,
  seeking his favour and also pledging the city to him.
- Aviendha is adamant that Rand rests but he insists on entering the city as
  soon as possible.

## Chapter 46: Other Battles, Other Weapons

- Despite Aviendha's protestations, Rand insists on entering Cairhein
  immediately to minimise the time the nobles have to plan.
- He is warmly greeted in the city.
- He sees Shaido, soon to become _gai'shain_, and some wearing a scarlet
  headband which he does not fully understand.
- Tairen nobles greet him and he returns the greeting in a deliberately
  incongruous way, to try to put them off balance.
- In the throne room he asks for a chair, saying the throne is for someone else,
  and forces the Tairen and Cairhein lords to mingle by standing them in order
  of rank.
- He declares the city of Cairhein as belonging to Cairhein and not his
  conquered property, earning him rapturous applause from the Cairheinin.
- He hopes his efforts in the Game of Houses have been successful in preventing
  anyone getting a strong hold against him.
- After allegiance is sworn to him, he ponders how to go after Sammael.

## Chapter 47: The Price of a Ship

- Recently, Nynaeve has been arguing with Elayne and Birgitte at increasing
  frequency.
- The pressure of finding a ship is getting to the women.
- They learn that Egwene has visited them both in their dreams.
- Thom and Juilin return from town mildly injured and talk of a riot: the
  Prophet's followers are at fighting with the Whitecloaks.
- The Shienaran Uno returns to say that the rioting was started when the
  Whitecloaks seized a ship which Masema's followers had obtained for Nynaeve.
- The Whitecloaks now hold the docks.
- Nynaeve is confident that Uno and his soldiers will be able to get them to the
  dock but before this can be resolved, Galad approaches.
- It was he who ordered the Whitecloaks to seize the boat as he was also
  trying to obtain it for Nynaeve.
- Frustrated, the group prepare to try to take the boat by some means and leave.
- Luca asks Nynaeve to stay but she refuses.
- Elayne and Birgitte go to the Seanchean woman and Elayne asks her to come with
  them.
- She refuses and pays no heed when Elayne reveals her true self to be the
  daughter heir of Andor.
- In fact, the woman does not believe her.
- Venting her frustration at Birgitte's lack of help, Birgitte casually mentions
  that she does not believe her either, causing Elayne to have a tantrum.

## Chapter 48: Leavetaking

- Nynaeve prepares to leave the menagerie, thinking about Lan and Luca.
- She means to marry Lan whether he wants it or not.
- When the group re-enter the town of Samara, Nynaeve is shocked by the
  destruction around.
- On their way to the port, they are set upon by a mob.
- The Shienaran, Juilin and Thom fight valiantly but Birgitte and Galad are
  flawless.
- Every arrow Birgitte looses finds an eye.
- Galad not only stands in front of the mob but manages to make it retreat
  from him and takes no injury despite facing up to 5 men at a time.
- They reached the dock and Galad dismisses the Whitecloaks defending it.
- The ship's captain demands a high price and Galad can only afford to pay for
  part of the journey to Caemlyn.
- He apologises to Nynaeve for breaking his promise and she finally realises the
  true extent of what Elayne says about him: he will do anything which he thinks
  is right, regardless of the cost to himself or others.
- It is likely this battle between the Whitecloaks and the people of the
  Prophet will become a war – all to find a ship for these women.
- Nynaeve insists that the refugees from Samara are allowed to travel with them.

## Chapter 49: To Boannda

- On the journey down river, tension between the women cools, despite Elayne
  apparently using the One Power to speed their way.
- Nynaeve confirms Elayne's heritage to Birgitte.
- Elayne and Nynaeve make several journeys to _Tel'ian'rhiod_ together, catching
  up with news from Egwene and Amys and spying in the White Tower.
- They reach Salidar after five days.

## Chapter 50: To Teach, and to Learn

- Nynaeve and Elayne find their status among the Aes Sedai is unchanged in spite
  of their accomplishments.
- They are forgiven for their running away and ordered to teach about
  _Tel'ian'rhiod_.
- They are to report all their activities since leaving the Tower to Siuan and
  Leane.
- Nynaeve notices the deference displayed by these women and the rift between
  them to be false.
- Much concern is caused by the damaged Seal.
- The men agree to spirit Elayne and Nynaeve away, but they do not want it.
- Gareth Bryne recruits them to his army instead.
- Min and Elayne discuss Rand.
- Siuan asks Nynaeve for particular help with _Tel'ian'rhiod_.
- Nynaeve consents in return for teaching about healing and the opportunity to
  study the stilled women - she is seeking a way to help Rand.

## Chapter 51: News Comes to Cairhein

- Rand is plotting, with much to be done on many fronts.
- Moiraine brings him a letter from Elaida asking him to come to the White Tower
  in order that they might prepare him for the Last Battle.
- She also gives him a letter from Alviarin, who merely says that he has allies.
- Mat is now a great captain.
- He enters with ill news that Gaebril has killed Morgase.
- Rand reveals to Mat that Gaebril is truly Rahvin.
- Rand is immediately filled with guilt - the only reason he did not attack
  Caemlyn is that he thought that is what the Forsaken expected of him.
- He now means to go on an all out personal assault on Rahvin.
- He charges Mat with taking on Illian and Sammael, making it clear he means to
  use whatever knowledge Mat learned in the twisted doorframe _ter'angreal_.
- Mat is recounting the events to Melindhra.
- When he mentions Caemlyn she attacks and nearly kills him.
- Instinctively, he kills her with a throwing knife; her dying words reveal she
  was a Darkfriend.

## Chapter 52: Choices

- Rand is confident today is the day he will kill Rahvin.
- Mat arrives to explain what happened with Melindhra.
- They suddenly realise Rand's Maiden bodyguard are not there but he has no time
  to worry about if and how he might have offended them.
- Moiraine gives him two sealed letters: one for him and one for Thom.
- She bids him to the docks before they leave for Caemlyn.
- Kadere is at the docks and is unexpectedly confronted by Lanfear in his wagon.
- Rand has grown impatient and is about to demand the point of Moiraine's detour
  when Kadere's wagon explodes and Lanfear emerges with Kadere's skin.
- Moiraine, and thus Lan, charge quickly but are casually knocked aside.
- Rand tries to take on Lanfear but she is furious having learned of his time
  with Aviendha.
- When Rand swears he will never be hers, she swears to kill him.
- She finds an ivory _angreal_ in front of the twisted redstone doorframe which
  Moiraine had asked Kadere to put on display the previous day.
- With this, she is overpowering Rand, who is still struggling to commit to
  harming a woman.
- Moiraine awakes and sees Lanfear in front of the doorway: this was her plan
  after her own viewing in the doorway.
- Rand was her distraction.
- She charges, catching Lanfear off guard, and both women go through the
  doorway.

## Chapter 53: Fading Words

- Rand sees Moiraine and Lanfear disappear into the _ter'angreal_ and
  immediately binds Lan with air to stop him following.
- He feels his bond with her severed and the special bond she placed on him -
  one that would transfer to another Aes Sedai on her death - pull him to the
  west.
- He makes to leave immediately, asking Rand to lie to Nynaeve.
- Egwene is seriously injured; Aviendha less so.
- Rand blames his inability to kill a woman - even Lanfear - for Moiraine's
  death.
- Amys tells him he is a fool.
- He remembers the letters Moiraine gave him.
- She explains that she knew that this day would come and either she and Lanfear
  would die, Lanfear would kill Rand or Rand would side with Lanfear.
- She thinks the best outcome has happened; she was sure he could not kill
  Lanfear.
- She tells Rand to trust no Aes Sedai, Black Ajah or no, whether they be Verin
  or Alviarin (Rand thinks there may be significance to her naming them.)
- She casually mentions to be careful of Natael as he is still the man he always
  was.
- Rand is aghast at how calm she has been, knowing she was in the presence of a
  Forsaken.
- Weeping, he is approached by the Maiden Sulin and finally figures out why they
  abandoned him.
- Rand hates even to see a woman die and chose no Maidens to accompany his party
  attacking Rahvin.
- Sulin explains that her choice as a Maiden is the spear over everything: if
  denied the spear, she would take her life.
- The Maidens abandoned Rand because he robbed them of their choice to fight
  this enemy, even knowing they were not even an inconvenience to him.
- Realising he either choses to have the Maidens risk their lives for his and
  their honour or take their lives in dishonour, he gives them their choice.
- When he leaves his tent, he finds a huge number have already gathered to fight
  for him.

## Chapter 54: To Caemlyn

- Rand Skims his Aiel army to Caemlyn.
- Not long after their arrival, he realises he has walked into a trap.
- Trollocs appear in their thousands and engage them in battle.
- A vicious lightning strike scours Rand's position.
- When he comes around, he sees Aviendha stone cold beside him, Asmodean burnt
  to a crisp and only smoking boots and a spear where Mat stood.
- Enraged, he opens a Gateway to launch an assault on Rahvin.
- Nynaeve and Siuan (using the _ter’angreal_ and a sleeping potion to enter) are
  in _Tel'aran'rhiod_ with Siuan trying to learn the rules of the place.
- She teases Nynaeve about her own attempts to remove her block from using the
  one power without being angry.
- The two women descend into a fist fight.
- From the corner of her eye, Nynaeve thinks she sees someone watching them and
  immediately concludes it must be Moghedien.
- She forces Siuan out of the dream world, not knowing what effect that might
  have, and transports herself somewhere instinctively.
- However, she is still bested by the Forsaken who prepares to torture her.
- From nowhere, Birgitte appears and looses an arrow at Moghedien, which is
  easily dealt with.
- Moghedien uses her knowledge of _Tel'aran'rhiod_ to reduce Birgitte to a baby.
- Nynaeve crawls towards her, begging for mercy.
- At the last, Moghedien realises her mistake in letting Nynaeve too close.
- She create an _a’dam_ in the dream world, giving her control over Moghedien.
- She interrogates Moghedien who confesses that the Forsaken working together to
  try to get Rand to attack Sammael and ambush him there.
- However, no one among them trusted Lanfear and Rahvin trusted no one but
  himself and has set his own trap for Rand, into which Moghedien believes he
  has fallen.
- Nynaeve can only travel to Caemlyn in the dream world while still holding onto
  Moghedian, but hopes there might be something she can do there.

## Chapter 55: The Threads Burn

- Rand knows he has been in this place before but does not know the rules of
  _Tel'aran'rhiod_ and so is somewhat cautious despite his furious pursuit of
  Rahvin.
- The Forsaken continues hiding and attacking; Rand is losing the battle.
- Nynaeve has arrived in Caemlyn to see the destruction caused by the men and
  shafts of balefire the two men are firing at each other.
- She rushes head long to chase them despite Moghedien being almost paralysed by
  fear of death at the hands of these stronger men.
- Rand and Rahvin have been in _Tel'aran'rhiod_ with their physical bodies,
  making them much more powerful but much more vulnerable.
- Finally, Nynaeve catches sight of Rahvin who is currently attacking Rand.
- She surrounds him with as strong a fire as she, with the power of Moghedien
  linked, can create and, for an instant, the man is burned but he quickly
  channels a block of air around him and turns to the woman.
- From below in a courtyard, Rand sees the fire and hears the shrieks and
  realises somehow Rahvin has been attacked and given away his location.
- Rand fires the strongest beam of balefire he can and Rahvin vanishes.
- He barely has time to stop and speak to Nynaeve before saying he has to return
  to the battle.
- Moghedien seeks some sort of compromise with Nynaeve.
- Nynaeve forces her to drink a very strong sleeping portion.
- Before she falls asleep, Nynaeve tells her that Moghedien has given away
  snippets of information revealing that in the real world, she is in Salidar.
- Nynaeve says she will see here there.
- Rand returns to see a vastly outnumbered Aiel force making a last stand.
- He begins to destroy shadowspawn in their hundreds.
- Through the battle, he sees Aviendha channelling and realises that his
  balefire has undone some of Rahvin's last acts.
- He is unrepentant, even if he has vastly changed the Thread of the Pattern.
- Asmodean and Mat are also both alive again – in effect they never died.
- With the battle won, Rand counts the cost of his dead followers.

## Chapter 56: Glowing Embers

- Rand is approached by Bashere from Saldaea.
- To Rand's surprise, though he is just a general, Bashere says he is confident
  that his queen Tenobia will join Rand without having to be conquered.
- Asmodean is thinking about what happened, and has decided he would rather not
  know.
- He goes to enter the kitchens but as he opens the door, sees someone he
  recognises and is surprised to see and is killed.
- Morgase remains on the run with her supporters.
- She means to retake her throne no Mater what.

> _"And the Glory of the Light did shine upon him._
> _And the Peace of the Light did he give men._
> _Binding nations to him. Making one of many._
> _Yet the shards of hearts did give wounds._
> _And what was once did come again _
> _— in fire and in storm_
> _splitting all in twain._
> _For his peace..._
> _— for his peace..._
> _...was the peace..._
> _...was the peace..._
> _...of the sword._
> _And the Glory of the Light did shine upon him."_
> From Glory of the Dragon composed by Meane sol Ahelle, the Fourth Age

[Home](/blog "ohthreefive")