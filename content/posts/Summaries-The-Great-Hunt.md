+++
         
date = "2019-12-13T10:32:58.951Z"
title = "Summaries: The Great Hunt"
description = "A summary to help me remember what I read better"
tags = [ "audible", "summaries" ]

+++

# The Great Hunt

**Robert Jordan**

## Prologue: In the Shadow

> "And it shall come to pass that what men made shall be shattered, and the
> Shadow shall lie across the Pattern of the Age, and the Dark One shall once
> more lay his hand upon the world of man. Women shall weep and men quail as the
> nations of the earth are rent like rotting cloth. Neither shall anything stand
> nor abide...
> Yet one shall be born to face the Shadow, born once more as he was born before
> and shall be born again, time without end. The Dragon shall be Reborn, and
> there shall be wailing and gnashing of teeth at his rebirth. In sackcloth and
> ashes shall he clothe the people, and he shall break the world again by his
> coming, tearing apart all ties that bind. Like the unfettered dawn shall he
> blind us, and burn us, yet shall the Dragon Reborn confront the Shadow at the
> Last Battle, and his blood shall give us the Light. Let tears flow, O ye
> people of the world. Weep for your salvation."

> From the Karaethon Cycle, The Prophecies of the Dragon, as translated by
> Ellaine Marise'idin Alshinn, Chief Librarian at the Court of Arafel, in the
> Year of Grace 231 of the New Era, the Third Age

- Through the eyes and ears of a man, who calls himself Bors, the prologue
  details a large gathering of important darkfriends.
- Ba’alzamon appears in front of them, telling them his time is near and showing
  them the appearances of Mat, Perrin and Rand.
- He says one of these will be the Dragon and will be best used to serve him,
  not to be killed.
- After this, each of the followers hears their task in their own heads.
- Bors is to return home to Tarabon and keep quiet about those who landed at
  Toman Head.
- Ba’alzamon present his task as a series of images which he says he will not
  explain.

> An impossible sky of striated clouds: red and yellow and black, racing as if
> driven by the mightiest wind the world had ever seen

> A woman, a girl?, dressed in white, ceded into blackness and vanished as soon
> as she appeared

> A raven stared him in the eye, knowing him, and was gone

> An armoured man in a brutal helm shaped and painted and gelded like some
> monstrous poisonous insect raised a sword and plunged to one side beyond his
> view

> A horn, curled and golden, came hurtling out of the far distance; one piercing
> note it sounded as it flashed toward him, split open and fountained molten
> rock

- Bors appears to be a Whitecloak.
- During the prologue we learned that it was the Dragon who sealed Ba’alzamon
  and the Forsaken (the 13 most powerful users of the One Power) in Shayol Ghul.
- Doing so broke the world, tainted the male half of the One Power and drove
  most male Aes Sedai mad.

## Chapter 1: The Flame of Tar Valon

- Rand and Lan are practising swordsmanship in Fal Dara.
- Rand feels as if he is hit by a solid wall of wind, interrupting their
  practice.
- It felt otherworldly, deliberate, and disturbs Rand.
- Lan asks him why he has not left yet and although he tries to explain that
  it is due to his companions from home, Lan gets him to admit that the real
  reason is Moiraine.
- Rand is angry that she has not told him anything or interacted much with him
  since his channeling.
- Lan says it is not required of the Aes Sedai to explain herself.
- Lan tells Rand a little about his sword, including that it was made by Aes
  Sedai and is exceedingly rare and powerful.
- The pair hear trumpets and see the banner of the Aes Sedai in the
  distance.
- The Amyrlin Seat has come to Fal Dara, and Rand realises she is
  probably there for him.
- Lan muses that it would probably have been better had Rand left a week ago.

## Chapter 2: The Welcome

- Rand finds numerous Aes Sedai in his living quarters, replacing his old
  clothes with new, fine ones, on the order of Moiraine.
- Rand notices a sigil stitched into some of the clothes – this might be the
  sigil of the Dragon, I'm not sure.
- He decides that he would like to see the Amyrlin Seat before he leaves and
  goes to try to take a look at her from afar.
- He then tries to take a horse from the stable to leave but is told that no
  horse is allowed to leave, that the welcome is over and that the Amyrlin
  Seat will send for Rand and his two companions soon.

## Chapter 3: Friends and Enemies

- Desperate to leave, Rand becomes paranoid, thinking that his use of the one
  power might be sending him mad, that the strange wind he felt was sent by the
  Amyrlin Seat to capture him and that someone is watching him.
- He comes across Loail and asks for help to escape the castle/city
  but instead Loail bids his friends to come and look after him
  because he thinks he is sick.
- Rand deliberately insults all of them as he wants to leave on his own and not
  endanger any of them and his ploy seems to work.
- He tries the same on Egwene but she is wise to him.
- As they argue she reveals she has been questioning Padan Fain without Moiraine
  knowing, which Rand thinks is dangerous.
- Egwene says that he is becoming more like his old self and interestingly that
  the guards are becoming more aggressive - I think it is likely he/some evil is
  manipulating her.
- Egwene's plan had been for Rand to be able to hide from the Aes Sedai in
  the dungeon.
- She was persuaded to help him when he revealed that Lan had said it was
  probably better if they did not get their hands on him.
- When Rand gets to Padan Fain's cell, Padan Fain tells him that he has not
  won, it is not over and that Moredeth knows more than you could know, Moredeth
  knows everything.
- Taken aback by Padan Fain's behaviour, Egwene decides it is better they do
  not stay there and decide instead they should hide Rand in the women's
  quarters.

## Chapter 4: Summoned

- Moiraine meets with some of her fellow Aes Sedai and the discuss matters and
  goings on in the world.
- She then goes to meet the Amyrlin Seat and they have a formal discussion.
- Among the discussion is the suggestion that Moiraine is hiding a man or boy
  who can channel the One Power.
- When the other Aes Sedai are dismissed and the Amyrlin Seat and Moiraine
  are alone together, the formality drops and the two speak as confidantes.
- The Amyrlin Seat says that if anyone knew what they were planning, they
  would likely be stilled and that she is unable to decide whether this would be
  right or wrong thing to happen.

## Chapter 5: The Shadow in Shienar

- Moiraine and the Amyrlin Seat continue their discussion.
- To be stilled is the same as for a man to be gentled, and means removing their
  ability to use the One Power, something which the Amyrlin Seat says has
  been done to Logaine.
- Moiraine reasserts her belief that Rand is the Dragon Reborn, and admits that
  Mat took a dagger from Shadar Logoth.
- Though concerned, the Amyrlin Seat thinks there are enough Aes Sedai
  with her to cure him, and she is sure none are of the Black Ajah.
- They discuss Horn of Valere, with a new plan of the Emond's Fielders
  presenting it to the Council of Nine in Illian.
- Moiraine also shows the broken seal: an ominous sign of the coming last
  battle.

- A commander of the Children of the Light arrives with a legion of troops in
  Tarabon.
- He speaks with a Questioner, who assumes control of the men due to orders from
  above - Bors?.
- The commander is troubled by both the size of the force, who might be giving
  these orders and to what end.

- A member of the Amyrlin Seat, Liandrin, speaks with Lord Agelmar's wife in
  private.
- She declares herself of the Red Ajah, has concerns about Agelmar and tells
  stories of a boy who can wield the One Power.
- She also has doubts about Moira, because she travelled with Padan Fain,
  though Agelmar's wife corrects her on these facts.
- Liandrin leaves feeling she has recruited the woman as her spy.
- She seems to be a Black Ajah.
- She gives her the task of making sure the boys are taken to Tar Valon.
- Padan Fain receives a visitor to his cell; not who he was expecting though he
  has been waiting for someone.

## Chapter 6: The Dark Prophecy

- Rand has another nightmare about the Dark One, and when he awakes finds
  Nynaeve knitting beside him.
- She says Egwene has gone to see Padan Fain, with Mat or Perrin if she can
  find them.
- Rand is worried, not trusting Padan Fain though asserting that he has killed
  the Dark One, even naming him Shai'tan, for which Nynaeve chastises them,
  believing it to bring evil to name him.
- Seeming to prove her correct, a Trolloc attack is sounded by alarms.
- Rand heads for the dungeons.
- He finds Padan Fain gone and Mat and Egwene unconscious in the cell.
- On the walls, written in blood, is, "We will meet again on Toman Head. It is
  never over, al'Thor."
- Liandrin arrives first, and tries to force some truth out of Rand by
  Compulsion but Moiraine arrives, stops her and sees to Mat and Egwene.
- Egwene has taken a blow to the head but Mat is worse off in that the dagger
  from Shadar Logoth is gone.
- Rand later learns the Horn of Valere was also taken.
- Later, speaking to Lan, he learns the orders he was told that no one was to
  leave the city were false.
- Lan confirms he has permission to leave and seems to suggest he should do so.

## Chapter 7: Blood Calls Blood

- Moira, the Amyrlin Seat and a few select Aes Sedai are healing Mat.
- They note that the healing will not be complete until they have recovered the
  dagger.
- Moiraine comments that Mat himself may be the best person to retrieve it.
- They also believe the dagger and the Horn of Valere will be together.
- One of the other Aes Sedai, Verin, from the Brown Ajah, a largely
  academic sect, wonders idly how many could be infected by the dagger in a
  year.
- She also produces a translation of something written on the walls during the
  attack which may or may not be a dark prophecy.
- It talks about the significance of Tarabon and Almoth Plain.
- It names one of the Foresaken a woman who used to be the Dragon's lover:
  Lanfear, the Daughter of the Night.
- She was said to be one of the most powerful of the Foresaken.
- It also makes mention of a man who can channel.
- During discussions about whether this could or could not be a true prophecy,
  Verin casually mentions that presumably this refers to one of the boys Moira
  has with her.
- By reacting instinctively in defence to this comment, Moiraine confirms
  Verin's suspicion and the Amyrlin Seat confirms that she knew about this as
  well by her own reaction.
- While they consider action against her, they noticed that Verin genuinely
  seems to be only academically interested, and has not even gone to the Red
  Ajah.
- She even says she wants to document the natural history of the madness men
  encounter after they channel.
- The situation seems to be diffusing when Verin casually mentions that she
  first had her suspicions 20 years ago.

- Rand and Perrin have another argument, made worse by the fact that Rand still
  thinks he cannot tell Perrin the truth.
- Lan arrives to tell Rand that it is too late for him to leave because the
  Amyrlin Seat has requested an audience.
- Lan tries to prepare Rand for this audience.

## Chapter 8: The Dragon Reborn

- Rand has his meeting with the Amyrlin Seat, Moiraine and Verin.
- The Amyrlin Seat tells him he is the Dragon Reborn and that Tam was not
  his father, explaining the prophecies.
- He tries to resist but his attempts are weak.
- She explains that because of the prophecies, she must let him go to do as he
  wants.
- The Aes Sedai are returning to Tar Valon with Egwene and Nynaeve.
- He chooses to go after the dagger and the Horn of Valere with his friends and
  soldiers from Fal Dara.
- After he leaves, the Amyrlin Seat ponders whether they have done the right
  thing.
- Nynaeve and Lan speak and he essentially confesses his love for her, giving
  her a royal ring from his lost kingdom.
- She then has a confrontation with Moira, whom she blames for much of the
  trouble that has affected her and her friends.
- Finally, Egwene and Rand part ways, with both of them being poor at
  restraining their emotions.
- They are uncertain if they will ever see each other again.

## Chapter 9: Leavetakings

- The group prepare to leave Fal Dara.
- Rand and Loail manage to bury the hatchet; Perrin and Mat are
  still cold towards Rand.
- Lan gives one final lesson called Sheathe the Sword where one must accept
  taking a sword blow to himself in order to achieve a greater
  goal/accomplishment.
- Rand is uncertain why Lan has told him about this sacrificial move.
- The Amyrlin Seat comes to see them away and Rand is suddenly aware of
  watching eyes again.
- An arrow glances the Amyrlin Seat and kills someone standing nearby.
- Her calm words imply that she knows the arrow was meant for Rand and not her.
- When the Aes Sedai have left, Ingtar reveals that they have a sniffer
  called Hurin to help them with their hunt.
- He has been hidden from the Aes Sedai as his strange sense of smell is
  not something they understand and the Aes Sedai do not like things they
  cannot understand.
- This reminds Rand of Moiraine telling him that old things are come again and
  new things are occurring perhaps because the world is changing.
- The sniffer says that the thieves have gone south, much to Ingtar's
  surprise.

- The action switches to the city of Illian where a feast to mark the beginning
  of the Great Hunt has occurred.
- We follow a captain called Bayle Domon who has felt like he has been hunted
  recently and has had many attempts to acquire his smuggling services.
- When he has refused them, his men have been killed.
- He is on his way to a new meeting where he knows he will be asked again to
  smuggle.
- This time, he is offered so much money that he cannot refuse.
- He is told to go to Mayene and bring someone back with him, asking no
  questions along the way.
- He hurriedly boards his ship to leave, musing that the real reason for
  interest in him might be because of one of the things he has collected – he is
  a collector of old things and has acquired a disc of Heartstone.
- This may be another seal from Shayol Ghul.
- When his ship is on the open water, he tells his second mate to head west –
  directly opposite from Mayene.

## Chapter 10: The Hunt Begins

- The group set off in pursuit.
- Along the way, Rand becomes frustrated when one of the party again mistakes
  him from as someone from Aiel.
- The Aiel are described as incredibly hardy.
- He is getting fed up that people are calling him things he does not think he
  is – he still appears to deny that he is the Dragon reborn.
- The sniffer notes that the trail is a little odd, changing direction
  unexpectedly.
- He says he can smell violence: trollocs, a Fade and something worse.
- They come across a village which appears deserted and search some of the
  houses.
- Ingtar's original second in command twice thinks he sees a woman in robes
  but then can never find her.
- Rand has a strange vision of what happened in that house which repeats itself
  in his mind and deeply disturbs him.
- Mat had no such vision when searching the houses.
- A shout comes up and the whole party converge upon the crucified body of a
  Fade.
- Ingtar orders that they leave and continue to follow the trail as soon as
  possible.

## Chapter 11: Glimmers of the Pattern

- Rand asks Ingtar about what they saw in the village but he is
  reluctant to talk about it.
- He does however reveal two things to Rand.
- Firstly, it was decided that Rand is second in command of the party – a huge
  surprise.
- Secondly, Moiraine has sent the banner of the Dragon for Rand to take forward.
- Rand finally reconciles with Mat and Perrin when he confides that he thinks
  he is being used as an Aes Sedai puppet.
- They both feel sorry for him and to an extent fear him.

- Padan Fain thinks about recent events.
- He notes that Rand was able to disappear from his senses while back at Fal
  Dara.
- It was temporary, but Rand's ability to do it puzzles Padan Fain in that he
  does not know how it was achieved.
- Padan Fain is thinking with satisfaction about killing the Fade and therefore
  guaranteeing obedience from the trollocs.
- He says he is no longer just Padan Fain but is something more and thanks the
  dagger for its part in this.
- He is currently unable to open the chest containing the Horn of Valere.

## Chapter 12: Woven in the Pattern

- Egwene sees Nynaeve and Lan apparently having another argument.
- Rather unexpectedly, Verin comes to Egwene and Nynaeve and tells them she will
  teach them.
- Nynaeve is more sceptical; Egwene more enthusiastic.
- Verin tells Nynaeve that she has great potential.
- Nynaeve does indeed manage to channel crudely.
- Thereafter, numerous other Aes Sedai come to give them lessons, with most
  not seeming to be too interested in it and Egwene noticing that Liandrin was
  the least interested in them and the most interested in asking questions about
  the boys.
- Egwene has a dream in which she appears to see Ba’alzamon as well as a woman
  standing over Rand - instinctively evil - and she is left with a feeling that
  he is in great danger.
- She goes to tell Moiraine but hears that she left with Lan, closely followed
  by Liandrin then both by Verin.
- Egwene explains her dream to another Aes Sedai and learns that she may be
  what is called a dreamer and more importantly, many dreamers are able to
  perform foretelling.
- The next leg of their journey to Tar Valon will be by boat and Egwene and
  Nynaeve are to travel with the Amyrlin Seat.

## Chapter 13: From Stone to Stone

- Rand awakes to find himself nearly alone – only Loail and the
  sniffer Hurin are with him.
- They are asleep on the same stone from the night before but the world around
  them looks different.
- Loail says that he once read a scrap of book which talked about
  stones which came from before the age of legends.
- During the age of legends, high in the sky were apparently able to travel from
  stone to stone, between worlds of 'if'.
- Hurin becomes a little panicked and begs Rand to get them home.
- Rand thinks that perhaps his technique of finding the void, which he has
  always used to remain calm, might actually be a method of accessing the one
  power.
- He wonders if he accidentally transported them the previous night when he felt
  the void on going to sleep.
- He reaches for it again but it shattered into shards of glass, physically
  causing him pain.
- This is a new sensation for him – usually when he loses the void it just
  bursts like a bubble in his mind.
- Hurin says he can still smell the darkfriends and Rand decides they should
  go after them, hopefully figuring out how to get back to their world.

## Chapter 14: Wolfbrother

- Perrin decides he must use his ability to communicate with the Wolves to find
  Rand.
- They are unable to find him so he uses them to hunt the trollocs.
- He confesses his ability to be able to communicate with the Wolves to
  Ingtar who surprisingly accepts this, having heard about Elyas, but
  suggest they pretend he is a sniffer to the rest of the group.
- They track the trollocs and Perrin senses evil up ahead.
- Ingtar declares that they are being followed and shortly afterwards, Verin
  appears saying she has been sent by Moira.
- She is interested to see Rand and even more interested about the manner of
  his disappearance and Perrin's apparent new-found ability.
- She and the other Aes Sedai already knew about Hurin, the group's first
  sniffer.
- She says she will go on travelling with them.
- Mat and Perrin think she is more interested in Rand than an accompanying the
  group.

## Chapter 15: Kinslayer

- Hurin continues to lead them after the darkfriends, but is confused by the
  place they are in.
- He feels like he is both following their trail but also following a memory.
- No marks are seen on the ground.
- Loail declares that this place is angry and wanted him to build a
  weapon out of a tree.
- Rand in counters Ba’alzamon again in what he thinks is a dream.
- The Dark One addresses him as Lews Therin and again states that they have
  fought many times before but that the last battle is coming.
- Ba’alzamon above knows about Rand's discussions with Moira, the Hunt for the
  Horn etc and says that he has followers everywhere.
- He tries to tempt him by offering to teach him to channel.
- Rand denies him and Ba’alzamon threatens him, with Rand feeling consumed by
  flame arising within his Heron marked sword.
- He awakes to find himself unburned other than on his hand where there is the
  branding of a heron from his sword.

## Chapter 16: In the Mirror of Darkness

- The group travel towards what appears to be the spire of Artur Hawkwing.
- When they reach it, they discover it is actually a monument to a trolloc
  victory over that king.
- Loail thinks that in this world, the trollocs won.
- Unexpectedly, the hear the screams of a woman and Rand gallops to save her
  from attack from an unnatural beast called a Grolm.
- She is dressed in white and exquisitely beautiful.
- All three of the travellers are captivated by her and her knowledge matches
  her beauty.
- She calls herself Selene.
- She addresses Rand as a Lord and urges him to seize greatness.
- When he persists in his desire to hunt for the horn and the dagger, she
  appears to anger and chastises Rand for his stubbornness, something she says
  he has always had.
- Rand asks her to travel with them until the horn is found; before she can
  answer, five more of the Grolm are seen in the distance coming towards them.

## Chapter 17: Choices

- Rand is determined not to try to use the stones again and with unique skill,
  kills all five of the onrushing beasts with five arrows.
- Selene encourages his use of the void – something she calls the oneness – to
  help him achieve such great acts.
- Soon, the noise of many other Grolm is heard and Rand concedes they must try
  to use the Portal Stone.
- He embraces the One Power at the stone and successfully transports all of
  them, horses included, back to their own world.
- He is not sure exactly how he did it but felt drawn to the One Power and happy
  in its warmth and use.
- They appear to be in a place called Kinslayer's Dagger, a mountain range, and
  Selene explains that like the Ways, time and space are different where they
  were.
- They may even be ahead of Fain.
- Selene congratulates and compliments Rand greatly and says she would like to
  stay with them to help their hunt for the horn.
- Rand reluctantly agrees but notes that Egwene would likely have not
  complimented him so warmly.
- Hurin goes to find a trail and somewhere to sleep.

## Chapter 18: To the White Tower

- Egwene tells Nynaeve that her worries about Rand are intensifying and her
  worst dreams feature the man in the mask.
- The Amyrlin Seat arrives to give them their final lesson.
- She provokes Nynaeve, having heard this is the only way to make her use the
  power, and successfully gets an example of the amount of power Nynaeve has.
- Egwene and Nynaeve both find this the most difficult of the lessons they
  received.
- Nynaeve remains angry and even resentful towards Aes Sedai.
- They arrive in Tar Valon, which is more beautiful than any city they have
  seen, with the White Tower being the most beautiful and the presence of the
  Dragonmount in the distance to the west the only ominous sign.
- They are greeted by the Mistress of Novices, who confirms that Egwene will
  start as a Novice but Nynaeve has an Accepted - highly unusual.

## Chapter 19: Beneath the Dagger

- Rand awakes to see Selene going through his bag; she tells him she is looking
  for clothes as hers are dirty.
- Selene continues to try to tempt Rand: physically with her beauty and
  psychologically with the promise of greatness.
- Suddenly Hurin announces he sees a camp.
- Rand and Loail sneak in, discover the horn and the dagger left on
  top of it.
- The alarm is raised when they are leaving and Rand embraces the void, using
  what he has learnt with the sword to kill multiple trollocs, all the while
  resisting the call of _saidin_, with which he knows he could burn all his
  enemies.
- After fighting to escape, he feels a sudden urge to go back and face all the
  other trollocs but Loail convinces him they must flee.
- Selene praises his efforts but is again frustrated by his stubbornness when he
  insists not to carry the horn himself but to take it back to Lord Agelmar.
- She is able to open the chest, something no one else has been able to do.
- She casually points out that the darkfriends will now be hunting them and they
  cannot go back but must go on, suggesting her city as their destination.

- Padan Fain, obviously furious, plans his revenge on Rand and the world.

## Chapter 20: _Saidin_

- Selene asks to see the horn again; Rand denies it to her.
- Rand asks Selene if she is Aes Sedai; she seems insulted.
- Rand sees a ruined statue, the hand of which clutches a crystal sphere, and
  feels drawn to it.
- Selene bids him stay, especially as there is a cliff between them and it, but
  Rand thinks he can get there safely.
- He goes to the sphere and embraces and is embraced by _saidin_.
- He starts to recite prophecies of the Dragon but then loses the connection as
  quickly as he found it, with little recollection of the events.
- Selene chastises him, worried that he may have died on the cliff.
- She beckons him to elope with her and the horn; again he refuses as the horn
  is not his to keep.
- She stiffens at this.
- The group head for an inn called the Nine Rings, named after Rand's favourite
  childhood adventure story.

> The sphere is a Choedan Kal. [The Choedan Kal are two giant sa'angreal that
were created during the War of Power, one for a man and one for a woman. They
are the two most powerful sa'angreal ever
created.](https://wot.fandom.com/wiki/Choedan_Kal)
> Sa'angreal are super-charged angreal, like the one we have seen Moiraine use.

## Chapter 21: The Nine Rings

- The inn keep assumes Rand and company are hunting the horn, similar to other
  recent patrons.
- Rand is again addressed as a lord, but acts contrary to this by playing his
  flute to entertain the inn.
- Soldiers in the inn sing along, and afterwards their captain comes to
  apologise if they caused any offence.
- He asks Rand some very specific questions, making Rand mistrustful.
- Selene is worse, excusing herself then leaving the group in the night.
- In the morning, this fact annoys the captain, betraying the fact that he had
  the inn watched.
- He gives Rand 50 soldiers as an escort.
- Rand is happy for the safety this will give them against Padan Fain, but
  worried that his escort could take the horn from him.

## Chapter 22: Watchers

- Moiraine and Lan have gone to a small village where two Aes Sedai have
  long spent time in retreat preparing to write a history of the world.
- For this, they have amassed a great deal of knowledge.
- Moiraine challenges Lan, firstly by saying that she has bound him to another
  in the event of her death but that her chosen other was chosen because she
  would make a good decision about binding him to yet another.
- It appears this revelation is designed to unbalance Lan so that Moiraine can
  challenge him properly, mainly about his preparation of Rand for meeting the
  Amyrlin Seat.
- Moiraine says that this did not upset her plans but it was not explicitly
  discussed between the two beforehand.
- She believes that because of Nynaeve, Lan has for the first time altered his
  allegiance, even if barely perceptibly.
- Moiraine asks the other Aes Sedai for any connection between the horn and
  the Dragon, she says the only connection is that the horn is said to be found
  before the Last Battle and the Dragon reborn will fight in the Last Battle.
- Asking for connection between Toman Head and the Dragon, she says that she
  and her partner differ in their interpretation of one translation but she
  believes the Dragon reborn will be announced to the world on Toman's Head.
- She asks about Shadar Logoth and hears that there is no connection between it
  and the Dragon.
- She asks about Lanfear, and hears that she is an enigma.
- What is known of course is the connection between Lanfear and the original
  Dragon.
- Moiraine is wandering the garden, considering what she has heard in thinking
  that there was an answer in there which she did not expect but cannot quite
  remember it.
- In this daze, she is caught unaware by the attack of a Draghkar.
- The Draghkar fixes her with its song but Lan and the other warder of the
  other Aes Sedai arrive and kill it before it can deliver its deadly kiss.
- Moiraine confirms that the Black Ajah must have returned because the Draghkar
  was warded, allowing it to sneak up on them.
- She says she now has one answer and must leave immediately, asking for pen and
  paper before she does.

## Chapter 23: The Testing

- Nynaeve is taken to a ter'angreal for testing as all Aes Sedai must be
  before becoming Accepted.
- She is given the chance to refuse but presses on, with vengeance against
  Moiraine being one motivator.
- There are three tests in the ter'angreal.
- First is a challenge from the past - in this case Aginor.
- She defeats him using the One Power.
- The Mistress of Novices says she should not have been able to channel in the
  ter'angreal or remember what happened (which she can) and that it would be
  wise if she did not channel again (the first two Aes Sedai who channelled in
  it lost their ability to channel forever once they left it.)
- The second challenge is in her present - she is shown Emond's Field in
  disarray and given the chance to help; she chooses to leave.
- The third challenge is a future - marriage to and children with Lan.
- She eventually finds herself unable to resist this mirage and only pulls out
  at the last minute with the aid of channelling.
- On return, she is noted to have a single thorn through the centre of each
  palm, which is odd to the watching Aes Sedai.
- One of them is the Amyrlin Seat, who confirms her as Accepted.

> Angreal are objects allowing the bearer to channel more of the One Power than
> they usually could, though they differ in their strengths and in response to
> the channeler's strength.

> Sa'angreal are much more powerful

> Ter'angreal are less powerful than sa'angreal; they can be used to create
> angreal.
> Not all ter'angreal require a person to use the One Power to 'activate' them.

## Chapter 24: New Friends and Old Enemies

- Egwene meets Elayne and discovers who she really is by their mutual
  association with Rand.
- They are friendly towards one another.
- She meets her brother Galad and half brother Gawyn, finding the half brother
  uncommonly beautiful, despite Elayne's open dislike of him.
- Elayne introduces Egwene to 2 other girls who have met Rand – firstly, Else,
  the daughter of one of the farmers he stayed with en route to Caemlyn.
- Elayne says she is lazy and unlikely to succeed in becoming an Aes Sedai.
- The other is Min, who is not there to study to become an Aes Sedai but
  at the request of Moiraine who wishes to study her innate ability to see
  things in people.
- Elayne's mother's counsellor, Elaida, is also there and Elayne introduces her
  as of the Red Ajah.
- Elaida speaks down to Elayne saying that their previous relationship holds no
  sway at the white tower.
- She dismisses the other girls and questions Min alone; Min is exhausted
  after the questioning.
- She also admits to herself she is falling in love with Rand.

## Chapter 25: Cairhein

- The group arrive in Cairhein, which Rand finds to be a mistrustful and
  somewhat unhappy city.
- It was partly destroyed during the Aiel war and is still in the process
  of being rebuilt.
- They settle in and in and Loail agrees to watch over the horn while
  Rand goes to investigate the city.
- Due to his clothing and everyone is insistent that he is the Lord, Rand finds
  he is treated well and surprisingly quickly is invited to numerous social
  events in the city – social climbing and something called the great game is
  prevalent in Cairhein.
- He decides to visit a part of the city which is less likely to be involved in
  this game.
- Amazingly, he comes across Thom, alive and well.
- He explains that the Fade was only interested in the boys and so left him
  rather than killing him.
- He is a little disappointed that Moiraine is no longer with them but delighted
  to hear that Rand has kept his flute and harp; he demands them back.

## Chapter 26: Discord

- Rand is jubilant and goes to collect Loail to visit Thom.
- He needs to be convinced but Rand does not even let him get his reasoning
  out.
- He explains what has happened to Thom, other than the fact he can channel, and
  asks him to accompany them with the horn.
- Thom declares that he is too old and is happy he got out of trouble when he
  did.
- His final thoughts, however, are of his young girlfriend, and he muses that it
  would be unfair for her to be stuck with an old husband.

## Chapter 27: The Shadow in the Night

- Returning home from Thom's, Rand and Loial are attacked by Trollocs.
- At one point Rand tries to channel but cannot.
- During their escape, they unexpectedly come across Selene, who helps them, all
  the while trying to urge Rand to embrace _saidin_.
- During their escape, they lose Selene again.
- When they get back to their inn, they are happy to find Hurin unhurt.
- There is already a note from Selene waiting for Rand, saying he is hers, he
  should embrace greatness and that they will meet again.

## Chapter 28: A New Thread in the Pattern

- Mat, Perrin, Ingtar and the others are on the heels of Padan Fain.
- Perrin tells Ingtar that the wolves have told him that someone or something
  called Shadowkiller has killed many trollocs but that they did not perceive
  him as an enemy.
- One of the crew spot an Aiel man and there is a minor confrontation.
- However, the Aes Sedai Verin is able to defuse the situation and get some
  information.
- The Aiel man is seeking someone who is said to come from prophecy.
- He is called He Who Comes With the Dawn.
- It is said there are signs but the signs are not described.
- He is said to fight under a banner, described unknowingly by the Aiel man
  as displaying the ancient sigil of the Aes Sedai.
- The group are now entering Cairhein, with Verin keen that they press on.
- What the Aiel has told her is not part of the pattern as she knows it.
- Perrin and Mat are left wondering whether the Aiel are seeking Rand.

## Chapter 29: Seanchean

- The white cloak Geofram Bornhald surveys the destruction of the village,
  including the hanging of children, carried out by the Questioners disguised.
- He dislikes their methods.
- Geofram Bornhald receives further orders from the Questioners but decides on
  his own path.

- At sea, Bayle Domon's ship is caught by one of the Seanchean.
- He is boarded and discovers that they are led by a woman.
- They wear ornate grotesque helmets, accounting for their reputation as
  monsters.
- They have casual disregard for the rights of others – saying they have all
  forgotten their oaths and should re-swear them.
- They talk as if these lands were theirs and they will reclaim them.
- Among their crew is a leashed woman who Bayle Domon assumes is Aes Sedai
  owing to her abilities.
- For saying this name, he is hit.
- The Seanchean have another name – _damane_.
- Bayle Domon is guided back to the Lord of these people, who has settled in
  another coastal town they invaded, by the other ship.
- He sees more ships than he has ever seen.
- He sees strange beasts, apparently tamed by the Seanchean - these include
  Grolm.
- The heartstone (Cuendillar) seal is what they were after – the Lord has the
  exact twin of it as well as multiple other artefacts made of heartstone.
- He is interested in heartstone and wants to hear stories from Bayle Domon,
  specifically how he came by the seal.
- Terrified for his life, Bayle Domon prepares to lie.

## Chapter 30: Daes Dae'mar

- Rand finds that even though he has tried steadfastly not to play it, he is
  becoming deeper enmeshed in Cairhein's Great Game (Daes Dae'mar).
- On returning from a walk with Loail, he finds the inn in which they
  were staying burning and Hurin severely injured.
- The horn is gone, and the dagger.
- He rescues Hurin and the Dragon banner.
- Out on the street, he sees Mat, Perrin and Ingtar.
- He laughs ironically that they have come too late.

## Chapter 31: On the Scent

- With the group reunited, their aim is to recapture the horn.
- Perrin realises that Rand is who the wolves call Shadowkiller.
- Verin is keen to know how Rand got ahead of them and he tells them about
  the portal stones and Selene.
- Verin is most interested in Selene.
- Hurin has picked up the scent of the darkfriends, locating them in the manner
  of the Lord of the second most powerful house in Cairhein.
- As part of the Great Game, Rand already has an invitation to this house.
- The group prepare to steal it again.
- During their discussions, Verin explains the significance of the statue with
  the crystal sphere – it was the male of a pair of the most powerful sa'angreal
  in the Age of Legends.
- Used together, as they must be, they might allow the users to break the world.
- Verin knows only a handful of Aes Sedai who could use the female statue –
  the Amyrlin Seat, Moira, Elaida and three in training, she says.
- She has no idea why any of them would aid a male channeller.

## Chapter 32: Dangerous Words

- Rand enters Barthanes's party and cautiously plays the great game with him.
- Meanwhile, Mat and Hurin are searching for the darkfriends.
- Loail remembers that a grove and therefore Waygate used to be in
  this place.
- Among the entertainers at the party are Thom Merrilin.
- He warns Rand that while Cairhein enjoys playing the great game, it was
  invented by the White Tower and Rand should be careful that he is not playing
  the Aes Sedai's game.
- Rand insists he will be free of Aes Sedai once the horn as found.
- Hurin returns with a coded message to suggest they have found something –
  Rand leaves with him.

## Chapter 33: A Message From the Dark

- Rand sends Mat to tell Ingtar and Verin that the horn has left by the
  Ways.
- He goes to the Waygate and removes the leaf/key to open it.
- Immediately, the Dark Wind Machin Shin begins to pour out of the Waygate;
  within it, he hears his name being called.
- He feels forced to embrace _saidin_and channels it, pushing the Dark Wind
  back.
- He feels both exhilarated but scared of the One Power, wondering if he can
  control it.
- Loail manages to find the leaf, helping the Waygate close once the
  wind has been pushed back.
- The group make plans to leave the manor immediately.
- Before they do so, Barthanes intercepts Rand to say that Padan Fain has
  left him a message that Rand can find what he wants on Toman Head and that
  Padan Fain will be waiting for him there.
- The group reassemble to plan their next move.
- In the discussion, Rand states clearly that he will ride as hard as he can to
  Toman Head to meet the darkfriends.
- The distance means it could take months.
- His friends agree to go with him.
- Verin interrupts and says more haste is needed.
- She declares that the group will go to a nearby stedding (Tsofu) which will
  have a Waygate and that they will use the Ways.
- Before the group go to bed, Rand asks Verin about Mat.
- She thinks that their healing has failed and although his strength remains,
  his body is weathering away and he will be dead within weeks.

## Chapter 34: The Wheel Weaves

- Thom returns home to find his girlfriend dead in bed and two attackers in his
  room.
- He kills one instantly and disables the other, interrogating then killing him.
- They claim to have been sent by Barthanes but Thom has his doubts.
- His friend the innkeeper comes into the room and agrees to hide the bodies.
- She identifies them as actually belonging to the house of the king of
  Cairhein and wonders what Thom has done to bring himself back into the Great
  Game.
- News reaches them that both the King and Barthanes have been assassinated,
  Barthanes brutally, and Thom makes preparations to leave quickly.

- Padan Fain goes to present himself to the high Lord of Seanchean.
- He gives him the Horn of Valere and pledges allegiance, internally being torn
  apart by the struggle not to reach for the dagger (the high Lord unexpectedly
  was able to open the chest easily to reveal its contents.)
- He addresses the Seanchean as if they were the descendants of high King Artur
  Hawkwing and the High Lord Turak appears to confirm this.
- The High Lord says he will keep but not use the horn and presented to his
  Empress – this is useful new information for Padan Fain as he did not
  previously know the Seanchean had an Empress.
- Before leaving, Padan Fain tells the High Lord that he should watch out for a
  darkfriend called Rand who is always followed by trollocs.
- The High Lord responds to this by wondering whether a Grolm could kill a
  trolloc.
- It appears the beasts the Seanchean have are the same Grolm that Rand
  encountered in the 'if' world.
- Padan Fain leaves, content in his plan to entrap Rand.

## Chapter 35: Stedding Tsofu

- The group enter this stedding and Rand experiences being cut off from the one
  power.
- They meet female Ogier from the stedding.
- There are three Aiel women deep in the stedding and on seeing the
  Shienaran, they grab their spears and prepare to attack.
- This only stops when an Ogier reminds them there is pact of not taking weapon
  against anyone in the stedding.
- During the attack, Rand embraces the void but found it strangely empty as
  _saidin_ was not there.
- He realised he did not like this emptiness.
- Mat and Perrin tell Rand about the Aiel man they met, and the search for
  He Who Comes With the Dawn, who they think is Rand.

## Chapter 36: Among the Elders

- The group are brought among the elder Ogier.
- The elders decree that although going through the Ways is incredibly dangerous
  (they even produce the last Ogier from there standing who went through and he
  is now a blundering shell) they will not stop them trying.
- As Verin opens it, Rand sees the Dark Wind and tells her to close it
  immediately.
- Verin has her suspicions about why Machin Shin was waiting for them.
- After some debate, Hurin suggests the use of portal stone.
- The group converse and agree that Rand will lead them back to the portal
  stone in Kingslayer's Dagger.
- One of the elders is surprised at hearing that the portal stones can be used
  and tells them that there is a much closer portal stone nearby.

## Chapter 37: What Might Be

- Having been led to the portal stone, Rand tries to use it with Verin trying to
  guide him.
- While channelling, he lives hundreds if not thousands of different versions of
  his own life, at the end of each he hears Ba'alzamon saying that he has won
  again.
- When he comes out of this sequence, Verin admonishes him for trying too much
  too quickly and not using her guidance.
- He has taken them directly to Toman Head in their world but the journey has
  taken about four months by Verin's estimation.
- He prays he is not too late to save his friends and that Padan Fain is still
  waiting.

## Chapter 38: Practice

- Egwene, Nynaeve, Min and Elayne practice talk among themselves while the two
  novices practice using the One Power.
- Elayne and Egwene talk about quite how hard the Aes Sedai are pushing
  them.
- Nynaeve remains full of anger and contempt towards Aes Sedai.
- Min remarks that she can see something new in the girls – namely that they
  will soon be in great danger.
- Unexpectedly (particularly as usually as novices they would be sent for rather
  than visited) the Black Ajah Liandrin comes to visit them.
- She tells them they must come with her to Toman Head in order to keep Rand
  safe.
- The idea of keeping him safe is enough for Egwene but Nynaeve wants more
  details.
- After Liandrin leaves, Min, who is not bound to the White Tower, announces she
  will go with them.
- Elayne decides to join them as well.
- Egwene hopes she can get to Toman Head in time.

## Chapter 39: Flight From the White Tower

- The group meet Liandrin as planned, with her expressing disappointment that
  two others are with him.
- Their path is through the Ways – something Egwene and Nynaeve are only willing
  to do because of their love of their friends.
- When challenged about Machin Shin, Liandrin suggests she will be able to deal
  with it and perhaps she knows more about it than Moiraine.
- In the Ways, Egwene has her first prophetic dreams in sometime.
- She does not see Rand but Ba’alzamon, this time unmasked and with healing
  burns on his face.
- She is awoken by Liandrin to tell them they have reached where they need to
  be.

## Chapter 40: _Damame_

- Liandrin presents the four to a Seanchean woman who appears acting against the
  orders of her High Lord.
- Nynaeve and Egwene were the two of interest.
- When Nynaeve realises what is going on, she channels to attack.
- In the confusion, Nynaeve escapes, Mins stabs the hand of the man who was
  holding Elayne, allowing her to escape but leaving Min captured, and Egwene
  is leashed.
- Egwene learns some of the consequences of being leashed, namely that the woman
  she is attached to has abilities to harm her and control her use of the one
  power.
- In addition, any injury inflicted on her will be felt double by Egwene.
- She learns that it was an Aes Sedai who first conceived of the leash.
- The only positive in the encounter is that Egwene is able to plead
  successfully for Min's survival.
- Liandrin and the Seanchean women have a disagreement about whose fault this
  set back was and part ways.
- Nynaeve finds Elayne in the forest and the two decide to go to Falme, as that
  is the only place mentioned by the Seanchean woman.

## Chapter 41: Disagreements

- Rand, Ingtar and Verin discuss what their next move should be and there is
  some disagreement, with Ingtar particularly being interested only in the
  horn and Rand wanting to go to Falme and to find Padan Fain.
- Verin is confident that whatever decision Rand makes will be the correct one
  for him.
- Rand has another encounter with Ba’alzamon in his bedroom.
- The encounter is familiar with Ba’alzamon imploring Rand to join him and to
  learn from him and again dismissing his chances to defeat him as he has lost
  to him a thousand times a thousand times.
- Rand rejects being addressed as Lews Therin and manages to banish Ba’alzamon
  from his mind.

## Chapter 42: Falme

- Nynaeve and Elayne have reached Falme and are sneaking about, trying to get
  information.
- In particular, Nynaeve is keen to know how the bracelets work.
- Both have used the One Power despite admitting that they should not and that
  it is dangerous.
- Egwene has been performing well at her training, including finding out she can
  detect ore in the ground.
- She has a visit from Min, something which is allowed once per week.
- She describes to Min what she has learnt about the connection between her –
  the _damame_ – and the woman to whom she is leashed – the _sul’dam_.
- There are numerous protections against the _damame_ either physically hurting
  herself or using the One Power.
- Egwene describes the situation – i.e. the prospect of resistance being
  successful – as hopeless.
- Min does not want to give up hope and in fact has found them a captain who
  wishes to leave Falme even without the permission of the Seanchean in the near
  future: this is Bayle Domon.
- Egwene agrees that Min should leave but, due to her good performance in
  training, Egwene has been told she is being taken back to Seanchean.
- Min still tries to convince her to continue her resistance – the things she
  has seen suggest that Egwene will be reunited with Mat, Perrin and Rand and
  Min does not understand how this could happen if she is taken back to
  Seanchean.
- Egwene's _sul’dam_ returns and is displeased to detect that she has used the
  one power when they were not leashed.
- She punishes her by giving her a new name – Tuli – the name of her kitten as a
  child.

## Chapter 43: A Plan

- Nynaeve and Elayne catch up to Min and hear the update from her.
- She takes them to Bayle Domon and Nynaeve says that with her and Elayne's
  help, as well as Egwene who they plan to rescue, they should be able to fight
  off the _damame_ and escape Falme by sea.

## Chapter 44: Five Will Ride Forth

- Perrin, Mat and Hurin scout Falme.
- Padan Fain has been there but is there no longer.
- Unbeknownst to Perrin, Geofram Bornhald and his rebel white clocks are in the
  town and Geofram Bornhald catches sight of him but cannot immediately remember
  who he is.
- He is preparing for another battle with the Seanchean.
- He tells his second-in-command to witness the battle and to take news of it
  back to his son and to a senior within the whitecloaks children of the light.
- He wants them to know that they were betrayed by the Questioners.
- They are unaware of a huge winged shape circling above.

- Hearing Hurin has detected the Peddler's trail but is sure he went to Falme
  alone, the group form a plan, manipulated by Verin, that Rand, Mat, Perrin,
  Hurin and Englishmen will go into Falme in search of the horn and the
  dagger.
- Rand is aware of the prophecy Verin is trying to enact: five will ride fourth
  and four return.

## Chapter 45: Blademaster

- Nynaeve uses _saidar_ to remove the collar from a _damame_ and the women
  capture her _sul’dam_.
- Nynaeve wears the bracelet and puts the _a'dam_ on the _sul’dam_, though Min
  thinks it will only work on someone who can channel.
- Seeing the control it gives her over the _sul’dam_, she changes her mind about
  disguising Elayne as a _damame_ and instead decides to use the _sul’dam_
  instead.

- Bayle Domon waits for the women on his ship, ready to leave in an instant.

- Mat identifies the location of the dagger in Falme.
- The group go there and after Ingtar kills one Seanchean guard, they are able
  to enter the room in which the Horn of Valere and ruby-hilted dagger are kept.
- They recover the items and are about to leave when Rand sees Egwene from a
  window, leashed.
- He hesitates and in this time, Turak comes in to the room.
- His men found the dead guard.
- Turak casually explains how he will recover the horn kill most of them and get
  information from any survivor.
- A guard reaches for the horn and Mat slashes his hand with the dagger.
- Within moments, the wound then the man blacken, bloat and die.
- Mat and Perrin attack some guards, Ingtar and Hurin the others.
- Turak turns to face Rand; his curved blade is also Herron-marked.
- Rand does not embrace the void as he wants to avoid _saidin_ revealing him to
  the _damame_.
- Without the void, he is nearly helpless in defence, disappointing Turak.
- He risks embracing the void to save Egwene and initially manages a competent
  defence but realises he cannot keep it up.
- Purposefully avoiding _saidin_, he goes all out in attack and kills Turak.
- The group decide to flee; Rand knows he must come back for Egwene.
- Mat keeps hold of the horn, though Ingtar is keen to have it.

## Chapter 46: To Come out of the Shadow

- Nynaeve rescues Egwene, with Elayne and Min helping.
- As they attempt to escape, to Seanchean patrols approach from either side, not
  particularly paying them attention.
- Egwene feels uncontrollable anger at the prospect of being need to wear the
  collar again and lashes out at them with the One Power.
- Nynaeve has to continue to draw on _saidar_ to help them all escape towards
  the docks, especially as they appear to be under retaliatory attack by _damame_.

- At the docks, Bayle Domon sees the commotion, explosions and lightning in the
  town but tells his crew that they are not to leave until the women arrive.

- Geofram Bornhald is overlooking the town with 1000 mounted Whitecloaks.
- He also sees the commotion and urges the start of his attack on the town.
- He sees a large winged creature overhead and assumes it is one of the
  Seanchean beasts.

- Rand and company are trying to get back to their horses when the explosions
  distract Seanchean patrols in their way.
- As they hide, Ingtar confesses to Rand that he has been a darkfriend,
  including letting in the archer who fired on the Amyrlin Seat or Rand
  earlier.
- His desire for the horn was a form of retribution.
- His motivation for becoming a darkfriend was the lack of appreciation of the
  rest of the world for the Shienaran work near the blight.
- He says to Rand that he wants to stay behind and assure the escape of the
  other men, the horn and the dagger.
- Rand absolves him and leaves with Hurin, Mat and Perrin.

## Chapter 47: The Grave Is No Bar to My Call

- Rand tells the group of the fate of Ingtar.
- They see ranks of Seanchean build up in Falme and the oncoming Whiteclocks.
- Rand tells the others to escape to Verin with the horn while he goes back for
  Egwene.
- While they argue about this, Mat blows the horn.
- A great white mist arises and Geofram Bornhald sounds of the charge from
  within it but the Seanchean attack the horses with their _damame_.
- A group about 100 strong advance across the mist towards Rand, led by Arthur
  hawk wing.
- They address Rand as Lews Therin and as Dragon.
- Perrin chooses to be the banner man and Mat the trumpeter and they prepared to
  charge the Seanchean.
- Charging through the mist, Rand comes face to face with Ba’alzamon.
- He is armed with a staff and they battle; the fate of the battle between the
  Seanchean and the heroes of the horn appears to be linked to their own battle.
- Rand notices two weaknesses: firstly Ba'alzamon is not all knowing because he
  believes Rand blew the horn.
- Secondly, that Ba'alzamon appears to be wary of the Heron-marked blade.
- Seeing this, he adopts Sheathe the Sword allowing Ba’alzamon to stab him with
  the staff like a spear.
- In turn, Rand stabs him where his heart should be with his sword.
- Ba'alzamon screams and the world explodes.

## Chapter 48: First Claiming

- Min finds Rand under a tree, unconscious, heavily bruised and with multiple
  wounds, all cauterised.
- She has seen Bayle Domon leaving in his ship and does not blame him.
- He is deadly cold and she gets under a blanket with him to try to keep him
  warm.
- This is how Egwene finds them and, without meaning, the two women have a fight
  about him with Min saying Egwene rejected him for the weight tower.
- Egwene says she and Elayne were drawn here to find Rand, though Nynaeve was
  not.
- Egwene leaves and Selene enters, identifying herself for the first time as
  Lanfear.
- She asks Min to take care of Rand until she is ready to come back for him.

- Outside Falme, Geofram Bornhald's second in command, Jaret Byar, prepares to
  leave with word that darkfriends sabotaged their mission.
- He thinks Perrin was involved.
- What he wants to report more than anything, and what troubles him, is the dark
  shape he saw flying above Falme.

## Chapter 49: What Was Meant to Be

- Rand awakes.
- Min informs him that Verin, Aileen, Egwene, Nynaeve and Mat have all gone to
  the White Tower.
- To his surprise, Moiraine is there.
- She tells him that she has been in Falme, gathering information and trying
  to free her sisters from the Seanchean.
- He is cold towards her.
- She produces his sword, burnt down to the hilt.
- He is now Randed on both palms with the herron.
- She explains that his battle with Ba’alzamon was seen for miles around in the
  sky and that there is now artwork of Rand battling Ba’alzamon backed by the
  Dragon banner.
- She asks about Padan Fain – she does not know where he is but now knows quite
  how dangerous he is.
- He is some sort of evil amalgamation of Ba’alzamon and Moredeth.
- Rand is keen to go after him but still refuses his identity as the Dragon.
- Perrin, Lan, Loail and the Shienarans and are waiting with him,
  the latter of which pledging themselves to him forever.
- Moiraine also produces two more broken seals of the dark one's prison.
- Min tales Rand that the Aes Sedai were unable to heal his wound.

## Chapter 50: After

- The story of what happened on Toman Head spreads throughout the world, with
  multiple inaccuracies as stories always have.
- The presence of the Dragon is the only consistency.


> *"And men cried out to the Creator, saying, O Light of the Heavens, Light of*
> *the World, let the Promised One be born of the mountain, according to the*
> *Prophecies, as he was in Ages past and will be in Ages to come. Let the*
> *Prince of the Morning sing to the land that green things will grow and the*
> *valleys give forth lambs. Let the arm of the Lord of the Dawn shelter us*
> *from the Dark, and the great sword of justice defend us. Let the Dragon*
> *ride again on the winds of time."*

> *from Charal Drianaan te Calamon, The Cycle of the Dragon, Author unknown,*
> *the Fourth Age*

[Home](/blog "ohthreefive")