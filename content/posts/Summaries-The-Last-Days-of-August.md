+++
         
date = "2020-02-05T21:01:17.942Z"
title = "Summaries: The Last Days of August"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Last Days of August

_John Ronson_

## Episode one

- Due to his previous work within the porn industry, Ronson responded to the
  news of a well-known porn star called August Ames committing suicide by
  deciding to investigate the story.
- Ames had declined a role in a film with a gay man as co-star and had been
  dropped by the film company because of this.
- She posted her predicament on Twitter and received abuse for homophobia.
- Among the tweets were one from a gay male porn star advising her to take a
  cyanide pill and a few from a woman described as, "Porn royalty."
- Tweets from the latter were widely seen due to her large following and
  something of a pile on happened.
- She took her life the next day.
- Her husband wrote a blog post and used the platform of an award ceremony to
  attack this porn queen (and bullying in general) as the cause of his wife's
  death and said that all porn stars must have full freedom of choice with their
  bodies.
- Ronson's initial investigation reveals some incongruities.
- The cyanide tweet was sent _after_ the suicide.
- When Ronson read the "porn queen's" tweets, he noted them to be mild and also
  that they never mentioned August Ames by name.
- This is in direct contradiction to Ronson's advice to Kevin that _his_ blog
  post should not directly name anyone.
- The first chapter ends with Ronson speaking to August's best friend, who says
  she wants to let him know some truths about Kevin.

## Episode two

- Ronson introduces the chapter by openly telling the listener that although the
  story is amazing, it does not turn into a murder mystery – he wishes to avoid
  any false tension in this regard.
- In the months after starting his investigation into August Ames' death, Ronson
  is approached by numerous people who cast doubt on the character of her
  husband.
- He has been married before and the previous wife or girlfriend has committed
  suicide, people say.
- A porn director who was named in Kevin's blog post after his wife's death
  reveals that he asked the coroner for their full report and that inexplicably,
  Kevin was checked into the Sheraton hotel on the night of her death.
- The main takeaway from the chapter is that Kevin seems to have a very
  controlling personality and that his relationship with Alex was not as good as
  he portrayed it publicly.

## Episode three

- The episode begins with an interview with August's favourite brother.
- We learn that she had a difficult relationship with her father, including him
  ignoring her accusations of sexual abuse by a member of his family.
- This moves on to a story of possible physical abuse on the set of a film by a
  Russian porn star.
- With high-end porn shoots, actors must sign in and sign out, on camera, to say
  whether they have been abused or made to do anything they did not consent to.
- It is implied that payment for the shoot can be withheld pending a positive
  sign out.
- The episode concludes with an interview with the photographer from the set of
  that film.
- Although not present for about 30 minutes of filming, he did not see anything
  that accounts for August's claims of serious physical abuse by her co-star.
- We are being painted a picture of vulnerable women being taken advantage of
  physically and emotionally within a somewhat cutthroat industry.
- Mental health issues among porn stars is implied to be common and support for
  them may be lacking.

## Episode four

- Ronson makes further attempts to find or at least find out about Kevin's first
  wife.
- They are largely unsuccessful but lead him to a porn star who once did a scene
  with her.
- When interviewing him, he says that he is a good friend of Kevin's and without
  prompting vouches for his character.
- Late one night, Ronson is phoned by another porn star who goes on the
  offensive, saying that his investigation will only result in negative
  consequences as many people in the industry are close to suicide anyway and
  his revelations will tip them over the edge.
- He ends the conversation and speaks with her a few weeks later at a more
  sociable hour.
- She again goes on the offensive, saying that she cannot bring back any of the
  dead girls and can only do harm.
- He also receives similar frustrated calls from Kevin but finally manages to
  agree to meet and interview Kevin once more.

## Episode five

- In another interview with August's husband Kevin, he acts defensively, stating
  that his attacks on the porn queen in the days after his wife's suicide were
  not to blame her for his wife's death but simply to out her behaviour as
  unacceptable and needing to change.
- He paints August as the controlling one in the relationship and mounts
  defences to many of the accusations against him.
- The answers he gives about all the incongruities from his initial story are
  discovered to be factually correct.
- Six months after first hearing that Kevin had a former wife who was
  institutionalised, that woman, Shazia, reaches out to them.
- She says that she also contacted Kevin with a burner email account to say that
  she was going to speak to Ronson and tell the truth about him.
- Before their interview, Kevin contacts Ronson's team calling Shazia's mental
  state into question and saying they will not get the truth from her.

## Episode six

- In his interview with Shazia, Ronson learns that her mental health problems
  were partly short lived and partly tied up with alcoholism around her time in
  the porn industry.
- She speaks of Kevin as very controlling but never physically abusive.
- Ronson circles back to another interview with Kevin.
- He says he refuses to say a bad word about Shazia but that she was severely
  unwell.
- Ronson is getting and portraying the picture of a man who is trying to hide
  his worst personality traits through insecurity rather than of a monster.
- When talking about the night of August's death, Kevin describes sitting in his
  apartment in darkness for hours.
- The electrical company confirms that Kevins address only was without power for
  21 minutes.

## Episode seven

- Ronson and his producer are invited to a ceremony for August.
- Ronson cannot attend it but she can, however after Kevin hears that she is
  going, he tells August's family that he is considering not going because of
  that.
- This is essentially blackmail as he has her ashes and so the ceremony could
  not take place without him.
- He also contacts the producer telling her that their relationship is over.
- An anonymous source shows the uncut footage of August's scene where she
  claims to have been physically abused.
- The producer does say that the scene is physical as August described it and
  gets the impression that she was unhappy but notices that she said nothing to
  anyone about this.
- Ronson has the coroner's report about August's death checked by another who
  concurs that there is nothing to suggest it was _not_ suicide and the fact
  that she did it in a public place – unusual for female suicides – is likely
  because she did not want someone close to her or her family finding her.
- Ronson concludes the episode by circling back to earlier interviews with Kevin
  which she is now able to listen to with more context.
- He notes that Kevin, by his own admission and by the observation of others
  within the porn industry is emotionally distant and not the sort of comfort
  blanket that August was looking for.
- Several people described their relationship is more like roommates than
  soulmates.
- He draws attention to Kevin's effort to start a project to help porn stars who
  are feeling suicidal.
- He also notes that in there earliest interview, Kevin drew a diagram of
  everyone complicit in August death, conspicuously leaving himself out.
- However, the words of this interview more than implied that Kevin took some of
  the blame himself.

[Home](/blog "ohthreefive")