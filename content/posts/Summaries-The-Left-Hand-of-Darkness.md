+++

date = "2018-09-23T22:51:50Z"
title = "Summaries: the Left Hand of Darkness"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# left hand of darkness

**ursula k. le guin**

## 0 introduction

- Ursula Le Guin talks about the science science fiction and that she feels
  it is often misinterpreted as being a predictive (often negatively so,
  eg dystopias) medium.
- Her belief is that science fiction is more a thought experiment or a
  descriptive medium in which the author tells lies deliberately but interweaves
  their lies with truths.
- Science fiction allows metaphor, she says, but playfully notes that exactly
  what the stories are a metaphor for is not always clear because if she knew that
  she would not require the metaphor.
- She notes that the reader of fiction (specifically fiction and not just
  science fiction) must be to an extent, "Insane," because they are aware that
  what they are reading is a collection of lies however, in order to read and
  immerse themselves in the story, the reader willingly believes them.

## 01 "a parade in erhenrang"

- The narrative is introduced in the past tense by a human narrator, Mr. Ai.
- We later discover he is an emissary of mankind’s Empire-of-sorts, Ekumen, sent
  to try to bring this alien planet in to Ekumen.
- The alien planet is "Winter" to humans (due to its weather) and Gethen to the
  natives.
- Mr. Ai is in the city/province/kingdom of Karhide. He feels out of place,
  both physically and culturally.
- At first, Mr. Ai attends a royal parade to celebrate the completion of an
  arch. There, he converses with Estraven, a high-ranking local of sorts.
- We later learn Estraven has been looking after and supporting Mr. Ai since
  his arrival a few months ago. However, at chapter’s end, he withdraws this
  support as it conflicts with his personal and political interests. Karhide is
  conservative where the Ekumen are liberal.
- When introducing himself as narrator, Mr. Ai comments on the subjectivity of
  truth.
- Mr. Ai often mentions his inability to trust the native, partly due their
  ambiguous gender.
- Mr Ai, upon learning that Estraven will no longer support him, realises that
  his message of an empire with many co-operative planets and civilisations
  comes with no evidence other than his word and he realises it must sound like
  a fanciful story. Again, raising the theme of truth, facts and their
  subjectivity.
- The fluidity of fact and truth are likely important themes, based on the
  content of the introduction and this first chapter.
- Note early in the chapter the narrator claims he was nearly killed on the day
  he is recounting, though this is not expanded upon in this chapter.

_"If this is the royal music, no wonder all the kings of Karhide are mad." Genly
Ai_

_"Karhide is not a nation but a family quarrel."_

## 02 "the place inside the blizzard"

- A fable or Karhide, "Hearth tales," about two, "Brothers," who mate/marry and
  have a child.
- We learn that the custom (or even law?) is that they should separate after this.
- Unwilling to do so, one brother commits suicide due to the emotional trauma.
- Suicide is a greater crime than murder in this society.
- The other brother exiles himself, denouncing his name and nation (which isn't
  Karhide; 'Shath'?).
- He wanders north, encountering a vast ice plane, with no hope of survival.
- He has a vision of a land of pure white and sees his brother who says this is
  the place for those who commit suicide.
- He rejects the opportunity to stay somehow find himself alive in another nation,
  where he lives under a false name for nearly the rest of his life.
- As an old man, he enquires as to the fortunes of 'Shath', which are said to have
  been poor for many years.
- On hearing this, he reclaims his name and citizenship and shortly thereafter
  dies.
- After this, the nation of 'Shath' is said to prosper again.

_"All went as it should go in field and house and hearth."_

## 03 "the mad king"

- Mr. Ai, whose first name we learn is Genly, has his audience with the king of
  Karhide (Argaven).
- Just before the meeting, he learns on our radio broadcast that Estraven is exiled
  for treason, on penalty of death.
- As Estraven's treason was to support him, Mr. Ai is shaken and fears the same
  fate for himself.
- Genly's aim is to convince the king to join the Ekumen.
- However he finds this difficult as standard diplomacy is ineffective on Gethen.
- Gethenian diplomacy is a kind of conversational duel where prestige is sought.
- The diplomacy is a failure, Argaven disputes all of Genly's claims about the
  Ekumen, says Estraven was using him in a power play and grants him the freedom
  of the planet.
- Again, this confuses Genly (why is his fate so different from Estraven's for
  seemingly the same treason?) and he elects to go the the neighbouring nation,
  Orgoyen.
- Of note, the king refers to his people as humans and his planet as Earth.
- There is no word for war in Karhideish.
- We learn several things about the Ekumen:
    - They have a mode of travel called time jumping. Vast distances of space (for
      example the 17 light years from Gethen to the nearest Ekumen planet) can be
      travelled near instantaneously to the traveller but the time will pass for
      everyone else.
    - They have the ansible communicator, a device on which anything typed is
      immediately displayed on a paired device anywhere in the universe, regardless of
      distance. This sounds like quantum entanglement, used explicitly by the sophons 
      in the Three Body Problem.
    - The Ekumenical, "Scope," contains 83 habitable planets and 3000 nations.
    - The, "Prime," world is called, "Hain."(sp?)
    - No other people experienced by the Ekumen can change gender like the Gethenians
      (fixed female gender appals King Argaven, who calls it obscene.)
    - All the people's in the Ekumen are said to be descended from a single species.
    - Ekumenical tradition is to send a single envoy to these negotiations, though a
      previous scientific party has been on Gethin for research only.

_"It doesn't take a thousand men to open a door." Genly Ai_

_"Shifgrethor — prestige, face, place, the pride-relationship, the
untranslatable and all-important principle of social authority in Karhide and
all civilizations of Gethen" Genly Ai_

_"Trust no-one." King Argaven_

_"When action grows unprofitable, gather information, when information grows
unprofitable, sleep." Teaching from Ekumenical school._

## 04 "The 19th Day"

- An East Karhideish story or parable is told of two men who seek their fortune
  from prophets.
- The first, a master of sorts, asks on which day he will die and is told the
  day but neither the month nor year. This maddens him.
- The second, a servant, seeks to clarify for his master and asks the seers how
  long his master will live. They reply that the master will live longer than the
  servant.
- The servant returns home and tells his master what he has learned. The master
  is consumed by anger due to the lack of specificity in his servant's question.
  Enraged, he kills him and descends further into madness.
- One month later, he commits suicide on the 19th day of the month as
  prophesied.
- We are being told again that a truth is not the same as an answer and truths
  are open to interpretation.

## 05 "the domestification of hunch"

- The chapter tells the story of Genly's journey out of Karhide.
- He is seeking seeking the foretellers of the story in the previous chapter.
- This is at the request of the scientific investigators of the Ekumen who have
  previously visited Winter.
- We learned that the process of foretelling is done by nine individuals, led by
  the, "Weaver."
- Mr. Ai meets one of the foretellers and befriends him. He turns out to be the
  Weaver. His name is Faxa (sp?)
- He spends some time with the group before paying for and asking his question.
- In this time, he comes to believe the ceremony is not a sham.
- During the question foretelling ceremony, Mr. Ai realises the Weaver has some
  telepathic ability and this is what allows him to lead the ceremony.
- The Ekumen are capable of communicating telepathically ("Mind speech") and he
  thinks that this Weaver might be the first person on the planet of Winter to
  whom he will disclose this.
- The chapter closes with the Weaver explaining to Mr. Ai that the foretelling,
  and knowing the truth, is a fruitless exercise, because it is uncertainty which
  drives people.

_"You don't see yet, Genry, why we perfected and practice foretelling? To
exhibit the perfect uselessness of knowing the answer to the wrong question."
Faxa_

_"Ignorance is the ground of thought. Unproof is the ground of action." Faxa_

_"The only thing that makes life possible is permanent, intolerable uncertainty.
Not knowing what comes next." Faxa_


## 06 "one way in to olgoreyn"

- We follow Estraven, departing Karhide after receiving his sentence and
  journeying to Olgoreyn.
- He has been given three days to leave until his death penalty comes in to law.
- This is revealed to be a ruse: agents of the new prime minister (Tibe) attempt
  to stop him leaving so they can legally kill him.
- The grace period allows Tibe to appear publically merciful.
- During his escape, he is attacked and paralysed. However, the captain of the
  attackers declares that as he is still within the period he has been allowed
  to leave Karhide, they will not kill him and instead deliver him to Olgoreyn.
- Olgoreyn is a rigidly officious society.
- Lord Estraven slowly works his way up to a meeting with two of the thirty three
  leaders of Olgoreyn whom he knows from his previous role as prime minister of
  Karhide.
- He tells them that Tibe's plan is to strengthen Karhide in order to challenge
  Olgoreyn.
- He tells them he sees the union with Ekumen as inevitable and that whoever
  from Gethen aids that union will be the dominant ruling power on the planet
  thereafter.
- Interestingly, one of the two then categorically states he believes Estraven
  (and Genly's) tale of the Ekumen on faith and belief in Estraven's character
  alone. This may be the first example in the novel of such a strong belief
  without apparent evidence of truth.
- Estraven advises bringing Mr. Ai to Olgoreyn.

_"The admirable is inexplicable." Lord Estraven_

_"Behind every man in Olgoreyn comes the inspector." Lord Estraven_

_"I had my eyes on the stars and did not watch the ground I walked in." Lord
Estraven_

## 07 "the question of sex"

- Taken from the writings of one of the members of the scientific exploration to
  Gethen many years before Mr. Ai's mission.
- It details the sexual cycle of Gethenians and postulates on its impact
  culture.
- They spend approximately 21 days in somer, in which they are androgynous. The
  remaining roughly 6 days are in kemmer in which they are sexually driven and,
  with a partner, will adopt male and female sex, respectively.
- If conception does not occur, they return to the somer state; if it does, the
  female maintains kemmer until the end of lactation.
- The scientist notes that the people of Olgoreyn are able to medicinally
  manipulate the gender to which they become in kemmer.
- Although there is no legal marriage, monogamous relationships are common and
  culturally important.
- Incest is not frowned upon, but a comment is made that if two brothers have
  another a child, they are expected to separate. This recalls the story earlier
  in the novel in which two brothers refused to separate.
- The scientist has theorised that the Gethenian population were a genetic
  experiment of the early human colonisers.
- She notes a number of advantages that the androgynous population have.
- As there is no sexual drive for as much as 80% of their lives, they are highly
  industrious. She also theorises that this somer state, the lack of masculinity,
  is the reason for the lack of war on Gethen.
- She notes however, that the extreme winter and effort required to battle this
  "adversary" may be in a stronger reason for the lack of war.
- She also notes that as the sexual drive is only active during kemmer but is
  **always** active during kemmer, sexual intercourse is always consensual and so
  there is no rape.

## 08 "another way in to olgoreyn"

- We are taken back to learn about Mr. Ai's journey to Olgoreyn.
- Initially, he aims to continue his diplomacy with Tibe in Karhide but quickly
  sees him for a war-monger, seeking to use war to motivate the people of Karhide
  into becoming a more advanced civilisation like Olgoreyn.
- Mr. Ai applies to enter Olgoreyn and is surprised to find his application
  successful much quicker than expected.
- Before he leaves he is given money from an old kemmeri of Lord Estraven, who
  asked him to pass it on and to try to return information about Estraven's
  wellbeing.
- On his way to Olgoreyn, Mr. Ai is attacked as part of a skirmish on a village
  in the border between the two nations.
- Again, he is surprised to be picked out from the refugees to be taken to the
  main city of Olgoreyn.
- When there, he sees how it is an advanced nation compared to Karhide, and
  feels comfortable and more like he is at home in the Ekumen.
- Mr. Ai is warmly welcome and by one of the commensals and invited to a dinner.
- There he discovers that most of the people at the dinner wish to use Mr. Ai,
  who already has some notoriety within Olgoreyn, for their own advancement.
- Laterally, he discovers that Lord Estraven is also attending the dinner.
- He surmises that it is Estraven's doing that he has been summoned to this
  dinner.

## 09 "Estraven the traitor"

- The story is told of a historical dispute between Estre and Stok.
- It seems anyone from Estre can be known as Estraven (and Stok, Stokven.)
- When a leader from Estre and a leader from Stok meet in the wilderness, they
  fall in love and conceive a child.
- They are discovered by others from Stok who kill Estraven (real name Arek.)
- Later, Therem (the Stokven) delivers their child to a Estre and he is brought
  up as heir to the throne, causing dispute.
- He is ambushed in the wilderness, wounded but manages to kill his attackers.
- He is discovered by his mother/father Therem.
- They vow peace between Stok and Etre.
- As part of the peace, Estraven gives up some land to Stok.
- For this, and for killing his attackers earlier, he is known as Estraven the
  Traitor.

## 10 "conversations in mishnory"

- Mr. Ai is to present himself to the commensals of Olgoreyn.
- Immediately before the meeting, Lord Estraven presents himself and warns him
  not to allow himself to be used as a pawn or a tool by any of the commensals.
- His discussions with the commensals are significantly more productive than
  that with King Argaven.
- He discovers among the 33 commensals, there are numerous factions of varying
  power.
- He discovers spies both within the commensals and for Tibe in Karhide.
- We also learned that King Argaven has had a child but it died shortly
  thereafter.
- Mr. Ai tells the commensals that there is another Ekumen ship with 11
  hibernating people on it orbiting the nearest star.
- Overall, the commensals see no negative impact to joining the Ekumen.
- It may force or convince Karhide to join, in which case, it would always be
  seen as the second nation to have joined.
- Otherwise, Olgoreyn will gain all the free trade of knowledge from the union
  and will dominate over Karhide anyway.
- Mr. Ai also mentioned that a couple of centuries ago, there was a catastrophe
  at the centre of the Ekumen, from which they are still rebuilding.
- He details for the commensals the structure and purpose of the Ekumen,
  emphasising that it is more a knowledge union than political, military or legal
  union.
- At the conclusion of the ceremony, Mr. Ai speculates that there is an
  intangible quality to the Olgoreyn which he mistrusts. He says this form of
  speculation is something that emissaries such as himself are trained to
  practice, and it is called far fetching. He is unable to be more precise as to
  what has disturbed him, but he is deeply unsettled and concludes that his
  negotiations with Olgoreyn have been as frustrating as those with King Argaven.

## 11 "soliloquy in mishnory"

- Mostly internal monologue from Estraven.
- He reflects upon Mr. Ai's performance in Olgoreyn and on the infighting and
  faction forming within the commensals seeking to control him.
- He also reflects on his dealings with Mr. Ai in Karhide, realising apparently
  for the first time both that his attempts to advise Mr. Ai have been
  misunderstood and reciprocally realising that Mr. Ai was actively seeking his
  advice.
- Towards the end of the chapter, we learned that Mr. Ai has again presented to
  the commensals and again has been requested to bring the orbiting spacecraft to
  Olgoreyn.
- However, he has again been refused in his request that the public are made known
  that this will happen.
- The chapter ends with Lord Estraven confronting Mr. Ai and again warning him,
  although exactly of what we are unsure. He tells him to bring the craft to
  Gethen before it is too late.

## 12 "of time and darkness"

- A historical account of a prophet called Meshe who is able to see all things
  which have and which will happen.
- I'm sure Faxa referred to him when talking to to Mr. Ai about foretelling,
  earlier.
- The chapter muses on the nature of time and says that all things happened at
  the centre of time with both things to come and things passed spread equally.
- It is rather densely written and hard to follow.
- If it has a point, I think the chapter is trying to describe how meaningless
  and brief life is across the span of time. Which is cheery.

## 13 "on the farm"

- Mr. Ai goes to visit the commensal Upsle to discuss the recent re-encounter with
  Lord Estraven.
- Overnight, he is arrested and taken to a manual labour farm.
- He discovers inmates there are all chemically castrated.
- This has a different effect on him compared to the Gethenians.
- Other than some rather depressing insight into the nature of incarceration in
  Olgoreyn, I am unsure about the point of this chapter was.
- At least it does confirm Estraven's warning that Mr. Ai was in danger.

## 14 "the escape"

- Lord Estraven breaks into the farm to rescue Mr. Ai.
- He is able to do this with some ease for two reasons.
- Firstly, part of the defence of the farm is that, due to the relatively
  starved conditions in which the prisoners are camped, the harshness of the
  winter wilderness around the farm and the fact that the prisoners have no
  official documentation, escape is essentially impossible. However, a rescuer is
  able to help overcome these issues.
- Secondly, Estraven is able to enter something called dothe(sp?), which allows
  him to make use of all his strength for a short period of time, and therefore
  carry Mr. Ai to safety.
- After this period of Dothe, he must rest for a similar period of time.
- During this rest, Mr. Ai gradually regains consciousness.
- Estraven confesses that he has always believed entirely in Mr. Ai's story and
  everything he has done since, no matter how underhanded or indecipherable it may
  have seemed, was an attempt to bring forward the alliance between the a Ekumen
  and Gethen.
- Estraven notes that he is the only passing on Gethen who has trusted 100% in
  Mr. Ai's story and yet he is the only person on Gethen to Mr. Ai has never
  trusted.
- He concedes that all of his attempts, both in Karhide and Olgoreyn, have
  failed due to factors he was on able to either foresee or account for. He
  therefore feels strong responsibility for Mr. Ai's current plight.
- In order to fully convince Mr. Ai, he requests that he teach him mind speak,
  in which there are no lies between conversants. Mr. Ai agrees.

## 15 "to the ice"

- Lord Estraven and Mr. Ai travel back to Karhide across a great expanse of ice
  and a glacier.
- This main narrative recalls one of the historical asides/narratives from a
  previous chapters: not for the first time.
- Ai & Estraven gain some understanding and respect for each other while dealing
  with the extreme conditions.
- In the evenings they talk at length, though mind speech, teased at the end of
  the previous chapter, is not explored.
- Eventually they come to a spectacular region where a glacier passes through
  some volcanoes.
- It is described as looking like death.

_"It is good to have an end to journey towards. But it is the journey that
matters in the end." Genly Ai_

## 16 "between drumner and dremegole"

- A chapter from Estraven's point of view.
- The two are now calling each other by their first names, "Harth" & "Ai", which
  seems significant.
- The first half to two thirds of the chapter recounts an awful struggle to
  cross fissured glacier between the two volcanoes of the chapter title. This
  causes significant delay in their journey.
- Estraven notes that Mr. Ai was impatient, quick to anger and to action. He sees
  previously unknown or on unappreciated bravery in Mr. Ai.
- He also notes some new peculiarities, for example that Mr. Ai seems unwilling
  to cry even though he clearly wants to at some points, due to despair about the
  journey.
- As expected, he begins to enter kemmer.
- He realises this will put extra strain on the journey and relationship but due
  to fatigue is unable to resist the natural changes that occur in kemmer and
  therefore has strong attraction towards Mr. Ai.
- The chapter concludes with an interesting discussion between the two about the
  differences between the male and female gender of Mr. Ai's species.
- Mr. Ai realises he has been on Gethen so long that the Gethenian species is
  more known to him now than women of his species.
- Lord Estraven observes that as the travellers are of two separate species and
  completely alone, they are now **both** the alien rather than when in Karhide
  or Olgoreyn where Mr. Ai was clearly the outsider.

_"Light is the left hand of darkness and darkness the right hand of light. Two
are one: life and death, lying together like lovers in kemmer, like hands joined
together, like the end and the way."_

## 17 "an orgota creation myth"

- Creation story from Olgoreyn history.
- It details what I think is a comparison to original sin, in that one of the
  thirty nine first men killed thirty seven of the others and, when in kemmer,
  mated with the last.
- All the nations off Gethen were born of the coupling and carried the original
  sin.

## 18 "on the ice"

- The time on the ice recalled from Mr. Ai's point of view having previously been
  described from Estraven's journal.
- The most important part is Mr. Ai's realisation that he is in love with Lord
  Estraven and his able to see him for the first time for what he is – man and
  woman at the same time.
- In addition, he teaches Estraven mind speech, which is terrifying to Estraven as
  he hears Ai speak in his dead brother's voice.
- The main plot of the novel is recalled for what seems like the first time in a
  while, with Estraven saying he has sent a message to King Argaven and that Ai
  still needs to alert his ship as soon as possible.

## 19 "homecoming"

- Lord Estraven and Mr. Ai finish their journey across the ice and come cross a
  small village in the mountains.
- There, Mr. Ai finally agrees to signal his ship but discovers they must travel
  another hundred miles or so to reach a suitable radio.
- They do this but are betrayed (Estraven is still exiled.)
- Estraven tells Mr. Ai that he must go back to Olgoreyn, leaving Mr. Ai in
  Karhide.
- However, the border guards on the Karhide side of the border shoot and kill
  Estraven.

## 20 "a fool's errand"

- After the death of Lord Estraven, Mr. Ai returns to Karhide where he gets another
  audience with King Argaven.
- As expected by Estraven, the commensals of Olgoreyn are in disgrace due to their
  lying, and King Argaven is happy to take advantage and welcome Mr. Ai's ship.
- Still sorrowful, Mr. Ai arranges first contact, and ensures all goes well.
- He is unable to keep his promise to Lord Estraven that he will insist that his
  exile is revoked before bringing his ship down to Karhide.
- King Argaven wishes to assess whether the meeting is a success before lifting
  the exile.
- Mr. Ai is disappointed in himself at reneging on his final promised to Estraven.
- The meeting is a success and the other members of the Ekumen begin to explore
  and integrate themselves within the Gethenians.
- At the close of novel, Mr. Ai seeks some solace, redemption or forgiveness for
  his part in Estraven's death.
- This is the fool's errand of the chapter title.
- After speaking to Estraven's father, he meets his son, who is inquisitive and
  energetic.
- The novel concludes with his son asking Mr. Ai to tell him all about the
  different worlds out there among the stars.

[Home](/blog "ohthreefive")
