+++
         
date = "2020-12-29T07:39:25.739Z"
title = "Summaries: The Lord of Chaos"
description = "A summary to help me remember what I read better"
tags = [ "summaries", "Audible" ]

+++

# The Lord of Chaos

_Robert Jordan_

> _"The lions sing and the hills take flight. The Moon by day and the Sun by_
> _night. Blind woman, deaf man, jackdaw fool; let the Lord of Chaos rule."_
> -Chant from a children's game, heard in Great Aravalon, the Fourth Age

## Prologue: The First Message

- One of the forsaken called Demandred is visiting Shayol Ghul to see the Dark
  Lord.
- He is taken to him by an enormous Myrddraal.
- The Dark Lord offers an opportunity to be his second in command and tells him
  the name of those who must live in those who must die – causing Demandred to
  cry with happiness.
- The Dark Lord asks Demandred would use balefire for him – balefire was
  only used for one year during the War of Power by either side due to it
  negative consequences.

- Nynaeve, Birgitte and Elayne have discovered Moghedien in Salidar – a woman
  named Marigan – and have told Siuan and Leane about her.
- They are the only five who know her true identity.
- Nynaeve is using the bracelet to interrogate her but is finding it difficult
  to pin down the Forsaken.
- They all agree that if they let more Aes Sedai know, Moghedien would be
  executed before her knowledge could be learned.
- Elayne is desperate to go back to Caemlyn – partly to see Rand and partly to
  restore calm now that her mother is supposedly dead, something Elayne does not
  believe yet.
- Nynaeve thinks she can 'heal' the Stilling process – Siuan thinks it is
  nonsense but Leane is more hopeful.
- The weather is unseasonably hot and the only conclusion is that it is the work
  of the Dark One.
- Elayne goes to see Min - she is jealous that the other woman is accompanying
  nine Aes Sedai to Camelyn to see the Dragon Reborn.
- Their friendship is too strong to let their love of Rand come between them.
- Min does not know that the Birgitte in their camp is truly the legend reborn.
- Elayne has respect from many Aes Sedai: firstly for the knowledge she and
  Nynaeve have allegedly discovered through study (though actually just been
  told by Moghedien) and secondly for making a _ter'angreal_ (which was entirely
  her own work.)
- Moiraine has already become a legend for killing two Forsaken: no Aes Sedai
  had ever killed one.
- Moiraine and Lanfear are both assumed dead.

- Perrin is now Lord of the Two Rivers, but he asks Faile to see the various
  villagers who present with their gripes and grumbles.
- He is too aware of his past as a boy in the village to act as dispute-settler
  between his elders.
- Noble-born, she is more than up to the task: in fact it bores her.
- Tam has been teaching A'ram and he has been progressing well, though now an
  outcast of the Tuatha'an.
- Perrin tells Faile that he can feel the pull of Rand and wants to go to
  him.
- As always, he thinks he should go alone as he is concerned for Faile's
  safety.

- Gawyn and his younglings are escorting some Aes Sedai to Cairnhein to
  bring the Dragon Reborn back to the Tower.
- He thinks this must mean that Elaida plans to align herself with Rand,
  something which is a surprise to him.
- Three Aiel Wise Ones enter the camp – Gawyn is suspicious as he is
  expecting an attack from the Aiel.
- A peddler brings news that Queen Morgase is dead and Elayne presumed dead.
- Gawyn realises that now he is more likely to kill the Dragon then bring him
  back to the Tower.

- Katerine is the name of the Aes Sedai who is leading the party Gawyn is
  protecting.
- Two of the sisters with her are secretly Black Ajah.
- The Aiel Wise Ones who approached were of the Shaido – this is
  with whom Elaida plans to ally.
- The black sisters have received secret orders at the Dragon Reborn is not to
  be killed.
- One of them debates why, the other reminds her that she has sworn to serve
  without question.
- They regard the Aiel with contempt – as savages.
- The leader of the Shaido is Sevanna, widow of Couladin.
- She has secretly been visited by a wetlander who gave her an ornate cube and
  said that with the use of a Wise One who could channel, she should use it
  against the Dragon Reborn.
- She has told no one about this so far.
- Shaido display the same amount of contempt towards the Aes Sedai as they
  received.

- Queen Morgase is still on the run, looking for support to reclaim her throne
  from Gaebril.
- She is finding support hard to come by and one of her guardians – Tallanvor
  – is displaying more and more subordination.
- She receives a surprise visit from the Lord commander of the Whitecloaks,
  Pedron Niall.
- Much to her surprise, Niall offers Morgase help.
- He informs her that Gaebril is dead by Rand's hand and he declares Rand a
  false Dragon: a puppet of the Aes Sedai.
- Shortly after he leaves, a man of Caemlyn sneaks in having identified the
  Queen and wishing to offer his help.
- Morgase sets about trying to find information from him.
- Niall hears news of a split group of Aes Sedai in Salidar.
- He believes it to be another plot – a way for the Tower to side with the
  Dragon but seem as if they are not, as if it is just rebels who stayed with
  him.
- Any case, he means not to assault this splinter group in Salidar.
- Instead, he tasks one of his inquisitors to make raids on nearby regions in
  the guise of Dragonsworn.

- Semirhage and Mesaana are waiting for Demandred to return.
- He does so, shortly followed by Graendal.
- Sammael will not be joining them, being ever suspicious.
- Eventually, Demandred begins to reveal the Dark One's plan: firstly by
  quoting him directly, "Let the Lord of Chaos rule"
- When he reveals the rest of his plan, Mesaana cannot decide whether she is
  fearful or excited.

- Two previously dead Forsaken are awoken in new bodies and are greeted by the
  same massive Myrddraal Demandred encountered earlier.
- Its name is Shaidar Haran.
- The are informed Rahvin is dead from Balefire and cannot be resurrected as
  they have been.
- Both are cut off from the One Power, something apparently necessary for their
  task.
- Should they complete it, they will be raised highest of the Forsaken.

## Chapter 1: Lion on the Hill

- Rand practices sword in front of the Caemlyn nobles who he despises for their
  duplicity and fawning behaviour towards him.
- Only the Saldaea general, Bashere, who is not liked by the Caemlyn, gives him
  any reason not to think that practising sword is a good idea.
- His mind is still filled with Lews' thoughts, which he tries to suppress.
- He tells Bashere that the White Tower is split and that one of the groups
  would support him.
- In his own thoughts, he knows he will never trust another Aes Sedai,
  especially after his last advice from Moiraine, but he will use them.
- Mazrim Taim comes to see him - a huge surprise.

## Chapter 2: A New Arrival

- Mazrim Taim wants to form a compact with Rand; Rand makes him kneel.
- Mazrim Taim submits, seeing his only safety as being with Rand and also
  wanting to share in the glory of the Dragon Reborn's victory.
- There is significant tension between Mazrim Taim and Bashere due to the things
  Mazrim Taim has done in Saldaea.
- Mazrim Taim presents Rand with one of the Dark One's prison's seals.
- Rand now possesses three seals and knows another three to be broken – neither
  he nor Mazrim Taim knows anything about the seventh.
- Throughout, the memory of Lews is telling him to kill Mazrim Taim and break
  the seal.
- He plans to take Mazrim Taim to the farm, where he can serve him.

## Chapter 3: A Women's Eyes

- Rand channels a Gateway to take Mazrim Taim to the 'farm'.
- It is a sanctuary where Rand has gathered twenty seven men who may be able to
  channel: Rand does not know how to test them but thinks Mazrim Taim will know.
- He is correct and able to quickly learn how to do it when Mazrim Taim
  demonstrates.
- Mazrim Taim thinks the mission is somewhat beneath him; he thinks Rand means
  to counter the Aes Sedai with his own channellers.
- Rand asks Mazrim Taim to demonstrate how much of _saidin_ he can hold and
  finds that it is not much less than he can.
- Throughout, the thoughts and voice of Lews are in his mind, telling him to
  kill Mazrim Taim.
- He plans to make a visit to Bashere's camp with a hidden agenda.

## Chapter 4: A Sense of Humour

- Rand goes to see the High Lord Weiramon, who is with many other Lords
  of Tear and of Cairnhein.
- There is little love between Tear and Cairnhein, but they are both relieved
  that the other is at least not Aiel.
- Many Aiel wear a red headband with the Aes Sedai symbol, but none of the
  Maidens.
- They wish an all out and immediate assault on Sammael in Illian but Rand
  orders them to wait for Mat and, even once Mat has arrived, they are likely
  to wait further and instead follow Rand's plan, which he has already told
  them.
- The Aiel Wise Ones are keen that Rand pursue Aviendha.

## Chapter 5: A Different Dance

- A long, introspective Mat chapter.
- He is leading a 6000 strong band of soldiers from Tear and Cairnhein known as
  the Band of the Red Hand.
- His relentless march on Illian via Tear is a diversion, part of a plan he,
  Rand and Bashere concocted.
- They are meant to be showing Sammael that they mean to attack him.
- They are currently in the town of Maerone, where Mat reflects on his last
  year, his connection to Rand and things that are yet to come.
- He recalls a visit from Rand via Gateway, where Rand excitedly talked about
  the chance to unite all nations for the Last Battle.
- In an unexpected turn, a Sea Folk ship has arrived up river, but Mat does not
  have time to find out the meaning of this.

## Chapter 6: Threads Woven of Shadow

- Sammael goes to meet Graendal: he does not trust her but will at least speak
  with her.
- She talks up the danger of Rand and asks if Sammael has an exit strategy from
  Illian.
- She is secretly baiting Sammael: she wishes Rand dead but the Dark Lord has
  forbidden it; if Sammael does it in self-defence her purpose will have been
  fulfilled without breaking loyalty.
- She has also been to Shayol Ghul and harbours desires to be Nae'blis.
- Semirhage is torturing an Aes Sedai under the watch of Shaidar Haran.
- She does not understand Myrddraal and is confused but the prominence of this
  one.
- She, and the other Forsaken, do not understand the bonding process between Aes
  Sedai and Warders.
- Her renown as a Healer, but also torturer, is why she was given this task.
- She unwittingly kills the Warder, but thinks this has had the fortunate effect
  of making the Aes Sedai (Cabriana) more pliable.

## Chapter 7: A Matter of Thought

- Elayne has managed to make a number of _ter'angreal_, though is much more
  successful when copying than creating.
- She has made a number that allow entry to _Tel'aran'rhiod_, into which she,
  Nynaeve, Siuan and Leane will meet 6 Aes Sedai.
- The group plan to visit the White Tower looking for information.
- Elayne notes the inexperience of the Aes Sedai with _Tel'aran'rhiod_.
- They learn little, but note that Elayne appears not to know of Moiraine's
  death.
- Three Aes Sedai are caught in a nightmare and the other three makes things
  worse by trying to fight the nightmare rather than denying its existence.
- They only just mange to escape, with Elayne and Siuan's help.
- Before waking, Elayne visits Caemlyn, sees and disapproves of Rand's Dragon
  throne.
- She wants to help him.
- She leaves, and shortly afterwards Demandred appears; he had been watching
  her.

## Chapter 8: The Storm Gathers

- Talking, Elayne and Nynaeve discuss how they hope now the Aes Sedai will be
  more humble in their dealings with _Tel'aran'rhiod_.
- Elayne is teaching Novices that day; Nynaeve has no purpose.
- She firsts sets about finding Siuan to insist she lets her study her Stilling.
- On the way she sees Logaine, who has been tasked with telling the story of how
  the Red Ajah assisted him to certain Altaran nobles.
- She is also confronted by Theodrin, who makes the same arguments to Nynaeve
  about working on her block as Nynaeve does to Siuan about working on Stilling.
- Angrily, Nynaeve settles on trying to learn from Moghedien how to sense a man
  channeling.
- Another thing that has been bothering her all day is that her weather sense
  has felt a storm come in and wash over them, but the weather has been
  relentlessly hot and dry.
- They are interrupted by Elayne: Elaida has sent an emissary to Salidar.

## Chapter 9: Plans

- Pedron Niall has two major spymasters – one is a public front, the other the
  real spy.
- He hears from his spy that Logaine may well have been a tool of the Red Ajah.
- He means to use this to his advantage: to say publicly that the Black Ajah
  have taken over the White Tower and are supporting Rand.
- He means to unite the world for the last battle but this will be a world
  without the Dragon Reborn.
- The longer Queen Morgase remains under his protection, the more it will seem
  to outsiders that she has sided with him and so he is not pressing her to
  accept his offer of escorting her back to her throne.
- Morgase has recently learned from Niall that her son Galad has joined to White
  Claoks.
- She thinks Pedron Niall is using this as some sort of bargaining with her – he
  could harm her son if she does not do what he wants.
- However, she considers Gawyn to be her true heir.
- Morgase wants to make the Dragon Reborn pay for the poor state she has seen
  refugees from Caemlyn in.

## Chapter 10: A Saying in the Borderlands

- Rand continues his plotting in Caemlyn.
- He learns that two Aes Sedai have apparently entered the city.
- After much debate, he decides he must go to see them.
- At the inn, he is surprised to see a large group of Emond's Field girls,
  including Mat's sister, are accompanying the Aes Sedai to learn.
- Rand hears that they have recently changed their plans to go to the Tower –
  this must mean they have recently discovered about Siuan's deposition.
- He is wary of the two, who are Verin and Alanna.
- Alanna approaches him and subtly convinces him that her healing would do him
  good.
- Rand agrees before realising that he has let his guard down and without a
  chance to stop it, Alanna bonds him as a warder.
- Furious, Rand hits out with a show of his power until Verin is able to calm
  him down.

## Chapter 10: Lessons and Teachers

- Verin is shocked by what she has just seen: Rand has indeed come far in the
  year since she first met him.
- She feels vindicated to have warned Siuan and Moiraine about their plans for
  him.
- She even considers whether he might have had a part in Moiraine's death.
- For her own part, she regains her composure, noting that she has had a plan
  for 70 years and that this is just another hurdle.
- Verin wonders what they might do with this tenuous thread which Alanna now has
  to Rand.
- She wonders if the rumours of Rand recruiting men who could channel where
  true and what to do about it.
- She wonders about the rumours of a band of rebel Aes Sedai and what to do
  about that.
- She hopes that Alanna is receptive to her guidance about all these matters.
- Rand rushes back to the castle, regretting the fact that he easily let these
  Aes Sedai perform the warding and he now did not know what to do about.
- He channels a gateway to the farm and finds that Mazrim Taim has made some
  progress with the recruits.
- He has however dismissed the sword master as he thinks that men who can use
  the Power do not need the training; Rand insists that he call him back.
- Mazrim Taim wants to use gateways to actively recruit rather than waiting for
  volunteers to show up in Caemlyn.
- He thinks that if he does this, Rand's male channellers could rival the Tower
  in one year rather than the six so far predicted.
- Rand has two reasons for caution: Mazrim Taim is too aggressive for his
  liking and himself could be captured during one of his missions.
- However, the accelerated pace at which he could gather channelling men means
  that he grants his request.

## Chapter 12: Questions and Answers

- Nynaeve and Elayne have been unsuccessfully trying to eavesdrop on the Red
  Ajah embassy with the One Power, despite Moghedien telling them it should be
  possible even with the Wards.
- Nynaeve is worried that they have let Moghedien hear too much.
- Nynaeve is unsettled and visits Birgitte, asking her to acquire horses in case
  they need to leave quickly, but not to ask Uno.
- She asks Birgitte to physically eavesdrop but is refused.
- Worried for Rand, she eventually eavesdrops herself but catches only the tail
  end of a conversation.
- Apparently those in Salidar want more time to consider Elaida's terms.
- When the Red Ajah leave, she hears the others say they are fed up waiting for
  an unnamed, "Biddable child," to decide something.
- Theodrin catches Nynaeve eavesdropping but does not punish her, instead they
  make their way to practice getting rid of the block.
- The two are being watched by an unknown woman.

## Chapter 13: Under the Dust

- Theodrin remains convinced she can remove Nynaeve's block but the two make no
  progress at their lesson.
- Nynaeve and Elayne agree that what Nynaeve overheard more likely means that
  Salidar will seek to re-unite the Tower before backing Rand; they want to
  change this.
- Elaida's ambassador interrupts and firstly asks Elayne to return then asks
  Nynaeve about Rand.
- Elayne decides to try to find something in _Tel'aran'rhiod_ to help convince
  the Salidar Aes Sedai to join Rand.
- They will blindly use Need, and be taking where they must be.
- After being taking to a room of _ter'angreal_ in the Tower, they re-focus and
  find _ter'angreal_ somewhere in a nearby city Elayne recognises as Ebou Dar.
- One _ter'angreal_ can control the weather, probably.
- Elayne is sure she briefly saw Egwene in _Tel'aran'rhiod_.
- The two are now convinced they must find _angreal_, _sa'angreal_ or
  _ter'angreal_ to help Rand, and will start by searching Ebou Dar for this room
  they found in their dream.

## Chapter 14: Dreams and Nightmares

- Egwene had been in Tel Aviv Riyadh.
- She fled to the Tower immediately on seeing Elayne and Nynaeve.
- She is not meant to be in the dream world while recuperating in Cairnhein.
- She is floating in a place where all dreams can be seen and entered if you
  know what you are looking for.
- She finds herself in Gawyn's dream, where he confesses his love for her.
- A bubble of evil attacks the Aes Sedai in Salidar.
- Unable to embrace the true source, Nynaeve finds herself somewhat useless.
- Anaya arrives, certain that the attack came from Sammael or another of the
  Forsaken and orders a group of accepted and novices to link with her for
  a counter attack.
- During this, a novice called Nicola appears to have a foretelling.
- Afterwards, Nynaeve is surprised that Anaya seems to have been disappointed
  that it was not the Forsaken.
- Exhausted by the exertion, she goes to bed.

## Chapter 15: A Pile of Sand

- Egwene tries to convince the Wise Ones that she is healed.
- The Salidar Aes Sedai want her at their next meeting.
- Egwene tries to learn more about _Tel'aran'rhoid_ as she has seen people there
  whom she does not think belong.

## Chapter 16: Tellings of Wheel

- Rand entertains some nobles from Caemlyn, discussing succession.
- He openly wants Elayne put on the throne (as well as that of Cairnhein).
- After the meeting, one of the nobles remarks that she sees a resemblance in
  Rand of daughter-heir Tigraine, who left Caemlyn unexpectedly one day.
- She was mother to Galad and first wife of Queen Morgase's second husband.
- Tigraine and her brother Luc were allegedly spirited away by an Aes Sedai,
  causing King Laman to cut down Avendoraldera, causing the Aiel war, which led
  to Rand being born to Tigraine, now Shaeil, on Dragonmount.
- Rand is frustrated that the Caemlyn nobles do not have the desire for power
  that allowed him to manipulate those in Cairnhein and Tear.
- Unexpectedly, an emissary from Sammael arrives but turns out to be something
  of a literal puppet, speaking in the voice of the forsaken and asking for a
  truce.
- Both Rand and the voice of Lews in his mind reject him outright.
- Rand determines that he must go to Cairnhein that night, partly to see
  Egwene.
- Aviendha appears and says she must do the same, partly to speak with Amys and
  the Wise Ones.
- Bashere wants Rand to visit the farm but Brand says it must wait.

## Chapter 17: The Wheel of a Life

- Rand receives his update about Cairnhein from Berelain and Ruaridh.
- He also hears about regions and kingdoms he has yet to visit but have already
  begun to echo with the effects of him.
- Some women in Cairnhein have taken up swords in an impression of the Maidens.
- Berelain supports them but Ruairidh is disgusted by their behaviour.
- Rand is asked to rule on the fate of a clan chief who committed an offence
  six days ago.
- He was one of the Aiel who helped take the Stone of Tear with Rand.
- He killed a man who had a dragon tattoo like the markings of Rand and the
  Aiel Chiefs.
- Remorsefully, Rand sentences him to hang for murder.
- He berates Berelain and Ruairidh for not being able to make this decision in
  his absence.
- Rand wants to visit the school he set up, and to see Egwene.

## Chapter 18: A Taste of Solitude

- Rand goes to visit his school: a place he hopes will keep knowledge alive if
  he breaks the world.
- He speaks to a philosopher, who muses on the circular nature of the Wheel of
  Time, for example saying that the Dark One must be defeated and his prison re-
  sealed because it needs to be this way for the cycle to restart.
- He says in this way, it will not be the Last Battle in truth.
- Egwene goes to see Rand.
- Mostly she wants his help to convince the Wise Ones to let her back in to
  _Tel'aran'rhiod_.
- Rand wants to know where Elayne is: he means to put her on her throne.
- Egwene keeps to her promise and tells Rand nothing; Rand responds in kind.
- Rand wishes Egwene did not behave so much like an Aes Sedai.
- Sulin is sneaking around to keep an eye on Rand - she disapproves of the small
  guard he is keeping.

## Chapter 19: Matters of _Toh_

- Rand dreams Lews Therin's dreams for the first time.
- He travels back to Caemlyn and talks with Aviendha, who tells him about some
  Wise One dreams about him.
- He thinks their meanings are simple and trivial; she is more concerned.
- The _ter'angreal_ weather bowl is in one of the dreams.
- Aviendha muses about her _toh_: to Rand for saving her life; to fulfil this
  she must kill herself.
- Her _toh_ to Elayne for sleeping with Rand means that she must kill Rand.
- These are incompatible.

## Chapter 20: From the Stedding

- As predicted by Loial, three other Ogier come to bring him back home:
  his elder, his mother and his betrothed.
- He tells them that Loial is in the two rivers with Perrin.
- Rand asks if they will help him to locate Waygates on a map.
- They agree.
- Rand asks the location of the Waygate within the Shadar Logoth.

## Chapter 21: To Shadar Logoth

- The Ogier agree when Rand tells them he can easily make their journey to
  Emond's field with a Gateway.
- They are also accompanied by some Maidens and other Aiel, Rand tells to
  be very careful.
- At the Waygate, Rand places a ward which will only affect shadowspawn all
  around it.
- Before leaving, they realise that one of the maidens is missing and despite
  their efforts, cannot find her.
- Rand channels a gateway to the Two Rivers to drop off the Ogier before
  opening a gateway to Caemlyn – his heart is torn by leaving his home behind.

## Chapter 22: Heading South

- Mat and the Band of the Red Hand are making good time towards Tear.
- One of his scouts alerts him to a destroyed Tinker caravan.
- Scrawled on one wagon is the message, "Tell the Dragon Reborn."
- That night, a group of Aiel sneak into the camp and attempt to kill Mat.
- After fighting them off, Mat is certain he has found evidence of what can
  only be a gateway.
- He now thinks he is under direct threat from one of the Forsaken.

## Chapter 23: To Understand a Message

- Sammael has summoned Graendal to him and to talk.
- He has found items from the Age of Legends - possibly even _angreal_ or
  _sa'angreal_.
- He displays confidence and calm, unbalancing Graendal who is used to him being
  quick to anger.
- She concludes that he has gained an upper hand, especially when he claims that
  Rand has agreed to a truce with him.
- He says Asmodean and Lanfear are dead and Moghedien likely is too.
- She gives him information – that Mesaana is in the White Tower – and promises
  that she will look for Demandred and Semirhage.
- After she has gone, Sammael is proud of his manipulation: he means to be
  Nae'blis.

## Chapter 24: An Embassy

- Egwene is walking the city of Cairnhein, enjoying the freedom she now has due
  to the fact that the Wise Ones think she has recuperated from her injuries.
- She will soon be allowed to enter Tel aviv Riyadh with their permission and
  attend meetings.
- She sees the embassy from the White Tower and rushes back to Amys' tent.
- Inside is Berelain, again enjoying a friendly relationship with the Wise Ones,
  much to Egwene's surprise.
- Berelain heads back to the palace to greet the embassy; Egwene asks to keep
  her presence secret.
- Egwene warns that Elaida can have no good intentions for Rand.

## Chapter 25: Like Lightning and Rain

- The Wise Ones decide to investigate the Seals as they have learned that is
  what the Aes Sedai plan to do in the city's great library.
- Egwene tries to spy on the embassy from the White Tower with little success.
- One Aes Sedai is channeling continuously.
- She bumps into Gawyn while leaving the scene and the two confess their love.
- Egwene also tells Gawyn that Elayne is alive and that Rand did not kill his
  mother.
- The embassy discuss their mission.
- Two Aes Sedai talk to two separate nobles saying that if Rand leaves with
  them, there will be a power vacuum that they might fill.
- They mention a plan for if Rand will not go with them, one that is related to
  the Aes Sedai channeling continuosly.

## Chapter 26: Connecting Lines

- Rand can still feel the presence of Alanna while he inspects the Saldaea
  soldiers on horse.
- He is also debating whether it is just Lews Therin's memories in his head or
  whether the man himself is there and able to converse with him.
- A Whitecloak climbs a rooftop and aims a crossbow at him.
- He is able to deflect the bolt with the power and within minutes, several
  Whitecloaks and Aiel are dead and four Whitecloaks are captured.
- Rand vows revenge on Pedron Nialll for the death of some of the maidens.
- He takes a history lesson from Elenia in which he learns that the noble houses
  of Andor are only related due to their nobility – the connections are so
  tenuous that were they commoners, they would not be considered related at all.
- Rand is relieved that he is not related to Elayne.
- He means to return to Cairnhein, without Aviendha.
- He forgets that he was due to meet a Wavemistress of the Sea Folk.

## Chapter 27: Gifts

- Egwene and Gawyn have numerous meetings.
- Egwene goes to see Rand in the palace before he meets the Aes Sedai.
- She tells him to be wary of the embassy from the Tower but to show some
  respect.
- The embassy arrives early, trapping Egwene.
- Rand uses _saidin_ to shield her from view during his meeting.
- He agrees to return to the White Tower with them but only when he chooses to
  do so.
- He advises them to stay at least a mile away from him.
- He makes a demonstration that he can sense when they have embraced _saidar_.
- He gladly accepts their gift of gold to him.
- After they have left, Egwene is confused as to why he has agreed to go with
  them and what his plan is.
- Rand is sure that he was able to determine that one of them was an ally of
  Alviarin.
- The Aes Sedai debate whether Rand truly could detect whether they were
  embracing _saidar_.
- Nesune his shocked that the other two sisters were not able to detect the
  presence of a woman who can channel just because they could not see her.
- They muse over whether this may be Moiraine or the mysterious green who has
  been rumoured.

## Chapter 28: Letters

- Rand wants to stop running from problems.
- Aviendha wants to go with him whenever he Travels.
- He receives letters from Sea Folk both in Caemlyn and in Cairnhein, both upset
  at his failure to meet them.
- The Queen of Ghealdan sends a letter that appears to secretly pledge support.
- A Gray Man enters but is incapacitated by Rand before Taim, who has
  coincidentally just Travelled there, kills him.
- Taim has found a channeler he wants Rand to meet.
- Lews Therin urges Rand to kill Taim.
- Padan Fain can sense Rand, and sense the bond with Alanna, though he does not
  understand it.
- He muses over how he has successfully poisoned Pedron Niall and Elaida against
  Rand though insists he must be the one to kill him.

## Chapter 29: Fire and Spirit

- Nynaeve is embarrassed as Theodrin tried to help loosen her block with wine
  and Nynaeve ended up hopelessly drunk.
- The story has spread throughout the town.
- Thom and Juilin return much to the pleasure of Elayne but bring no
  particularly good news.
- Elayne has accepted that her mother is dead although Thom tells of a rumour
  that she has joined with the Whitecloaks.
- Nynaeve decides to probe at Logaine again.
- As she is doing this, she is also thinking about Egwene and Rand.
- Without realising, she uses what she has learned by studying Siuan and Leane
  to heal Logaine and quickly sends for help while shielding him.

## Chapter 30: To Heal Again

- Six sisters are left to shield Logaine - a nod to Nynaeve's power.
- Nynaeve is dragged before every Yellow Ajah in Salidar to perform healing on
  Siuan and Leane, which she does with some ease.
- For this, she earns praise but also the inquisitive mind of every Yellow Ajah:
  they want to test her and improve the Healing weave.
- Siuan and Leane discuss that they are weaker, but resolve to ask the council
  to be restored as Amyrlin and Keeper again.
- Later they come to see Nynaeve to ask if she can Heal them to full power as
  she did Logaine.
- Aes Sedai hierarchy is largely based on power and Siuan does not think she
  can gain the answers she wants in this current state.
- The four agree to go forward as friends.
- An old friend of Siuan's, Deidre, has noticed that Siuan is weaker since being
  healed and is interested by this.
- Unexpectedly, she receives a visitor who displays signs of the Black Ajah.
- The visitor reveals herself to be the Forsaken whose powers were stripped from
  her after being resurrected earlier in the novel, now called Aran'gar but
  going by Halima in Salidar.

## Chapter 31: Red Wax

- Valda arrives and meets High Inquisitor Rhadam.
- Both are unsatisfied with Pedron Niall and wonder if he might be removed as
  Lord Captain.
- Niall sees Valda arrive and knows it means trouble.
- He spymaster reports Aes Sedai on leashes in Tarabon.
- Morgase is summoned to Niall; on the way she witnesses those who plotted to
  liberate her being hanged.
- She signs Niall's treaty.
- Watching, Rhadam wondered if Morgase knew those who were hanged.
- His plan is for a show trial followed by hanging the Queen.

## Chapter 32: Summoned in Haste

- There has been turmoil in Cairnhein since the Aes Sedai arrived.
- The Wise Ones have raised Sevanna to that rank.
- They are frustrated with Rand.
- Egwene goes to visit Sea Folk, wanting to learn about weather, but they
  recognise her ability to channel and reject her.
- She meets with the Salidar Aes Sedai in _Tel'aran'rhiod_.
- They formally summon her to them and ask her to use _Tel'aran'rhiod_ to get
  there sooner, though the Wise Ones do not condone doing so.
- One of them, whom Egwene is shocked to discover is Siuan, shows her a map.
- Rand happened to be watching the meeting - he now knows where to find Salidar
  and Elayne.

## Chapter 33: Courage to Strengthen

- The Wise Ones confront Egwene as she prepares to physically enter
   _Tel Aviv Riyadh_.
- She confesses all of her lies, including that she is not fully Aes Sedai.
- Rand pays an unexpected visit to Mat and asks him to Travel with the band
  via Gateway to Salidar, collect Elayne and escort her to the Lion Throne in
  Caemlyn.
- He also has a letter for Thom written by Moiraine.
- Should the Salidar Aes Sedai declare support for Rand, Mat is to offer
  them his protection.
- He is also to collect any Dragonsworn on his journey.
- He is also to take Aviendha, and to help Egwene if he can.
- Mat is concerned that this may distract the plan for Sammael but Rand is
  confident it will not.
- Egwene meet hers _toh_ towards the Wise Ones, who then helped prepare her for
  her journey.
- One of them comments that should her misdemeanours mean she can never be
  Aes Sedai, she should return to the Wise Ones as apprentice.

## Chapter 34: Journey to Salidar

- Egwene is able to make the journey physically in _Tel Aviv Riyadh_ skilfully.
- When she arrives, she is quickly caught up on a few things, most notably
  Nynaeve's discovery of how to heal Stilling.
- She realises that the Aes Sedai do not realise she has been masquerading
  as a full sister.
- Shortly afterwards they reveal their true purpose in summoning her: she is to
  be named Amyrlin Seat.

## Chapter 35: In the Hall of the Sitters

- Despite not being Aes Sedai, that is apparently not an impediment to making
  Egwene Amyrlin.
- Rituals for selecting Egwene as the Amyrlin Seat are followed.
- Once enough votes are cast, it is decided and Egwene will be raised.
- She asks what would have happened had she not been raised and is told she
  would likely have been exiled as the Tower seeks always for harmony and a
  rejected Amyrlin Seat candidate would be a constant source of disharmony.

## Chapter 36: The Amyrlin Is Raised

- Egwene receives separate advice from separate Aes Sedai, all seeking to
  influence her.
- In her maiden speech, she makes Sheriam Keeper of the Chronicles.
- She also raises Theodrin, Faolain, Nynaeve, and Elayne to Aes Sedai by decree,
  causing immediate objections.
- She summons Elayne and Nynaeve to ask about their discoveries and learns about
  Ebou Dar.
- With Egwene as Amyrlin, they hope they can go in search of the _ter'angreal_.

## Chapter 37: When Battle Begins

- Egwene continues to catch up with Nynaeve and Elayne.
- They reveal their capture of Moghedien.
- Egwene immediately confronts the woman to learn of Travelling – her suspicions
  about how it could be achieved are correct and she is quickly able to do it
  the female way.
- Elayne thinks she will be able to replicate it but Nynaeve is unable to
  observe it due to her block.
- Siuan arrives and quickly realises that Egwene will not be the puppet
  Amyrlin Seat that Sheriam hoped she would be.
- They agree to work together.
- Separately, two of Egwene's competitors meet to discuss what has happened,
  both seeing Egwene as a puppet of Sheriam particularly for raising four
  accepted to the shawl.
- Siuan, Elayne and Nynaeve set about manipulating these two groups as well as
  Sheriam herself to further Egwene's plan which includes uniting all women who
  can channel, regardless of their race and training.
- Talking with Deirdre, Halima reveals that one of her main aims will be to
  kill or gentle Logaine again.

## Chapter 38: A Sudden Chill

- Mat arrives at Salidar; his spies report Gareth's army.
- As it outnumbers his, he tells his troops to put on a defensive display so as
  not to look like they are attacking and goes into the village with Aviendha.
- When the Aes Sedai sense Aviendha's ability to channel, they swarm around
  her and batter her with questions.
- Mat is taken to the Amyrlin Seat, but when the room contains only Egwene,
  Elayne and Nynaeve, he berates all of them and tells them of Rand's plan and
  that they need to leave soon.
- Before any of the women are able to correct him, a Novice arrives and
  addresses Egwene as Amyrlin Seat.

## Chapter 39: Possibilities

- There is a stand-off between Mat and the women.
- Both think the other is in more danger than they believe and both mean to help
  the other get out of it.
- Egwene discusses with Elayne and Nynaeve what they must do about getting to
  the _ter'angreal_ in Ebou Dar.
- They decide Mat should accompany them.
- Elayne wants to study the _ter'angreal_ Mat wears which protects him from
  _saidar_.
- Egwene entertains Sheriam, again having to manipulate the woman but managing
  to make it clear that the Salidar Aes Sedai must be on the move toward
  Tar Valon.

## Chapter 40: Unexpected Laughter

- Thom and Mat converse; the older man tries to explain to the younger that he
  would be better going along with the women's plans than trying to change their
  mind.
- Thom does not divulge the contents of Moira's letter.
- Birgitte confesses to Elayne that she is struggling to live up to her
  legendary status and so would rather people did not know who she truly was.
- Aviendha confesses to Elayne but Elayne says she has no _toh_ towards her,
  especially as she knew three women would love Rand.
- She tells Aviendha about Min.

## Chapter 41: A Threat

- Min arrives in Caemlyn and goes to see Rand.
- She sees an aura around him that she does not like - he will be hurt.
- First off, she insists that he sees her as a woman and he agrees, noticing
  some feelings towards her.
- She tells him of the embassy from Salidar and two, as well as Melanie, discuss
  tactics for dealing with the Aes Sedai.

## Chapter 42: The Black Tower

- Min returns to report to the Salidar Aes Sedai.
- With Verin and Alanna, that makes 11 Aes Sedai.
- There are also Elaida's Aes Sedai in the city.
- Rand goes to the farm and sees it transformed.
- The people there call it the Black Tower, to balance the Aes Sedai's White
  Tower.
- Rand does not like the name.
- He announces that they will be called Asha'man, which is Guardian in the Old
  Tongue, and must advance through Soldier and Protected to reach the rank.
- He gives Taim two pins to mark the ranks.
- Taim seems furious with him.
- He does not think Rand should tolerate so many Aes Sedai so close to him, for
  his own safety.
- Rand fights to keep Lews Therin from his mind, though at least the man does
  not reach for _Saidin_.
- Taim sees Rand's turmoil and begs him to hold on to his sanity.

## Chapter 43: The Crown of Roses

- Alanna's bonding of Rand is not viewed positively by the Salidar Aes Sedai.
- They will seek to influence him through Caemlyn's nobles.
- The Salidar Aes Sedai are not sure what to do with Alanna and Verin.

## Chapter 44: The Colour of Trust

- Mat feels like he is being shut out and goes dancing to take his mind of it.
- He dances with a woman called Halima but afterwards, feels his medallion go
  cold and when he shoots her a look, she looks surprised.
- Mat is asked by many Aes Sedai to be their warder.
- The next day he is told to go with Elayne and Nynaeve to Ebou Dar or go home
  with his army.
- When Mat is gone, Egwene moves to her next plan: how to manipulate his second
  in command so that the Band accompany her to Tar Valon.

## Chapter 45: A Bitter Thought

- Perrin arrives in Caemlyn.
- Loial has gone to rest in steading and will join them in 10 days.
- Rand and Min greet them warmly; Faile quickly realises Min's feelings
  for Rand.
- She herself is apprehensive as her father is one of Rand's generals and she
  ran away to become a hunter for the horn.
- Perrin express some concern about the hardening he sees in Rand.
- Rand asks Perrin to go to Tear, saying he will confide in him his plan, a
  plan only three men know.
- Rand updates Perrin about Mat and about the death of Moiraine.
- He explains how he is playing the two Aes Sedai factions against each other to
  keep their attention off him and there is a third secret faction which
  supports him.

## Chapter 46: Beyond the Gate

- Perrin and Davram Bashere talk: Bashere is originally concerned Perrin just
  wants the Broken Crown.
- After much back and forth, Bashere accepts Perrin as son-in-law.
- Faile and her mother have also been at each other's throats.
- Her mother thinks Perrin is not strong enough to control her.
- Eventually, cordiality wins out.
- Min advises Rand by explaining some of her Viewings.
- She says Perrin will have to 'be there' for Rand twice but might not even be
  able to help.
- She tells him the Salidar Aes Sedai are meeting nobles from Caemlyn.
- Demira (a Salidar Aes Sedai) is heading to the library to research the Seals.
- A group of Aiel pass and she finds herself stabbed with spears.
- She recognises one man: squat and with black, villainous eyes.
- Faile offers to help spy on Rand for Perrin; she wants him to be wary of his
  friend.
- Demira survived her attack and was given a message.
- The Aes Sedai in the Crown of Roses were tipped off about her attack.
- Putting these together, the Salidar Aes Sedai decide the attack was just a
  warning, and that only Rand commands Aiel.
- They decide not to meet Rand for a few days.

## Chapter 47: The Wandering Woman

- Mat finds the journey to Ebou Dar to be an ordeal.
- Nynaeve will not speak to him and appears angry with him.
- Elayne practically demands she let her study his _ter'angreal_ and when he
  refuses, she and the other Aes Sedai do so anyway without his permission.
- At first, he is happy as all their efforts have no effect on him but then he
  is hit by an object thrown with the Power and he realises the limits of his
  protection.
- He learns that Birgitte is Warder to Elayne.
- After they arrive in Ebou Dar, they settle in an inn of the chapter title and
  Mat feels much better.

## Chapter 48: Leaning on the Knife

- Elayne and Nynaeve speak to the Queen of Ebou Dar.
- Whitecloaks are in the city, including Jaichim Carridin, as are Tower Aes
  Sedai.
- There are apparently Aes Sedai runaways in the city.
- After diplomacy on Elayne's part the Queen hears their story and thinks the
  _ter'angreal_ is in a very dangerous region called Beslan.

## Chapter 49: The Mirror of Mists

- Rand wants Perrin to help with the plan in Illian but Perrin wants to stay
  with Rand because of Min's viewing - Rand will be hurt without Perrin.
- Rand wants Loial to help him organise defending all Waygates.
- Min arrives saying seven Aes Sedai are on their way.
- They try to intimidate Rand but he shatters their illusion and says they
  should respect him instead.
- Min later learns the Aes Sedai know something important that Rand does not but
  before she can learn what it is, two Aes Sedai who had previously been tasked
  with finding Rand in the Waste unexpectedly arrive.
- With Alanna and Verin, the Salidar Aes Sedai number 13.
- Min gets word to Rand who immediately makes plans to retreat to Cairhein.
- Rand sends a letter to Taim telling him to be wary and one to Merana saying
  five Aes Sedai may accompany him.
- Kiruna, one of the Aes Sedai returned from the Waste, seizes control of the
  embassy and insists eight will accompany her to Cairhein; the others will take
  the Two Rivers girls to Salidar.

## Chapter 50: Thorns

- Rand visits his School in Cairhein and discusses Maters with the philosopher-
  historian who advises him to, "Clear rubble before you can build."
- He meets with various nobles; Min can see that most will die and tells him so.
- Min gains an interest in philosophy and practices fighting with Maidens.
- Perrin is hounded by Berelain and also by nobles who want to get closer to
  Rand.
- Rand plans to even out his meetings with the Tower and Salidar Aes Sedai
  before both groups are together in the city.

## Chapter 51: The Taking

- Rand entertains three of the Tower Aes Sedai, led by Coiren.
- Before he has time to realise, he finds himself shielded.
- The servants who accompanied the Aes Sedai were also Aes Sedai: 13
  have linked to shield him.
- They take him from the palace and hide him in the room in which some of their
  number have been channelling constantly since their arrival.
- Because they have been channelling for so long, other women able to sense
  channelling have begun to ignore them, allowing them to conceal Rand.
- The Aes Sedai spread the rumour that Rand has left the city.
- Perrin is annoyed at him for this, as are the Wise Ones.
- Faile still ignores Perrin.
- One of the Aiel finds it suspicious that Rand has left the city on the
  same day that Min cannot be found.

## Chapter 52: Weaves of the Power

- Mat is in Ebou Dar, unable to keep tabs on the women.
- He notices a man turn his head at mention of the Daughter of the Nine Moons,
  but does not realise the man is Seanchean and does not press the matter.
- Elayne has been channeling a disguise to evade Mat and search Rahad; after ten
  days there has been no success with the latter.
- The two Aes Sedai with them, hunting without disguise, also have no success.
- Egwene meets an angry Logaine and promises no harm to him.
- She meets Elayne in _tel'aran'rhiod_ though there is little to discuss.
- Siuan is there when she wakes: preparations have been made for Logaine's
  'escape'.
- Myrelle is the Aes Sedai to whom Moiraine passed Lan's bond.
- He has finally reached her, and she begins to heal his wounds.

## Chapter 53: The Feast of Lights

- The city of Cairhein is in full celebration.
- Perrin, however, is still concerned as Rand has been gone for 6 days and the
  Aes Sedai 3.
- Lord Dobraine brings news of the murder of two nobles and the plotting of
  another.
- Berelain arrives having found Rand's sword.
- Sulin immediately concludes that he has been taken by the Aes Sedai.
- Not all Aiel can accompany the party: for one many would not fight Aes Sedai
  and for another there is still a city to protect from Shaido.
- The Wise Ones will come, to help battle with the Power.
- Dobraine will bring Cairheinin lances.
- Galina is trying to get information out of Min and is pressing for Rand to be
  gentled.
- At his next beating, Rand is able to see that Sevanna is in the camp.
- He and Lews Therin agree to work together: the Aes Sedai will need to tie off
  his shield for him to break it and he must be patient.
- Galina is contemptuous towards the Wise Ones and plans to use the Aiel to kill
  the Younglings.
- Sevanna has learned the weaves to shield.
- She plans to take Rand from the Aes Sedai.
- She decides she will not use the _ter'angreal_ box she was given by the
  mysterious Wetlander and instead will marry Rand.
- She is contemptuous towards the Aes Sedai.
- They kill a Wise One with the Power, planning to blame the Aes Sedai and
  justify an attack.

## Chapter 54: The Sending

- Perrin sets out from Cairnhein to Hunt Rand.
- On the way, he communicates with the wolves, who inform him of the youngling
  protection of the Salidar Aes Sedai.
- When they hear that they have caged Shadowkiller, they vow to help Perrin
  release him.
- Loial surmises that Rand would be too strong to be captured by
  just six Aes Sedai and therefore there must be more in the party they hunt.
- They meet up with the Caemlyn Aes Sedai and some Two Rivers men, who are
  following Rand using Alanna, expecting Rand to be with Perrin.
- Perrin informs them what has happened and they decide to join up in a cautious
  alliance to continue the hunt.
- One night, the wolves reach out to Perrin telling them that there are many two
  legs and he must come now.

## Chapter 55: Dumai's Wells

- One of Gawyn's scouts reports that a large group of Shaido are
  approaching.
- Despite the Aes Sedai not recognising his authority, he quickly arranges
  for the wagons to be put in a circular defensive formation and readies his
  Younglings.
- By the time Perrin and his followers approach, tens of thousands of Shaido
  are assaulting the wagons.
- He means to make an all or nothing charge to rescue Rand and, just before
  hitting the Shaido in the flank, he calls upon the wolves to assist,
  who do so in their thousands.
- In his crate, Rand feels some of the Aes Sedai shielding him tie off
  their weaves.
- He is able to break these weaves and once the shield is reduced, breaks the
  shield.
- Gawyn and his Younglings cut their way out of battle but not before Gawyn has
  time to tell Rand he will see him die one day.
- Mazrim Taim arrives with an army of Guardians who fight their way to Rand.
- Once the embassy Aes Sedai are shielded, Rand asks Mazrim Taim to focus
  on the Shaido.
- The Guardians begin to kill the Shaido in their thousands until
  Rand commands that they stop.
- The Salidar Aes Sedai approach, saying that their intention was to help Rand
  but noticing that it did not look like he needed it.
- As Rand had commanded that only six may follow, he tells them that they must
  kneel just like the embassy Aes Sedai.
- Mazrim Taim adds that they should swear fealty and in doing so, they become
  the first Aes Sedai to swear allegiance to the Dragon Reborn.

## Epilogue: The Answer

- A Darkfriend servant of Moghedien is still carrying out her last command,
  unaware of her capture.
- The command was to find a cache of _angreal_ in Ebou Dar.
- She has been unsuccessful, but thinks it is important that Nynaeve and Elayne
  are also there.

- A _gholam_ assassin kills the philosopher Hedrim Fel in his room.

- The Seanchean man in Ebou Dar prepares to leave and report Mat's comment.
- He hopes the city will remain ripe for looting.

- Aran'gar, as Halima, frees Moghedien and tells her she is summoned to Shayol
  Ghul.
- The channeling reveals Halima to be a man.

- Egwene also feels the male channeling and wonders if it was Logaine.

- Demandred reports to the Dark Lord in front of Shaidar Haran; he believes he
  has done well.

> _"The unstained tower breaks and bends knee to the forgotten sign._
> _The seas rage, and stormclouds gather unseen._
> _Beyond the horizon, hidden fires swell, and serpents nestle in the bosom._
> _What was exalted is cast down; what was cast down is raised up._
> _Order burns to clear his path."_
> —The Prophecies of the Dragon

[Home](/blog "ohthreefive")