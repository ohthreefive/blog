+++

date = "2019-09-03T12:32:05.752Z"
title = "Summaries: The Mirror of Yoga"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Mirror of Yoga

**Richard Freeman**

## Chapter one: Phases of the Practice and Classical Forms of Yoga

- The introductory chapter initially goes over some different forms of yoga
  practice then switches to a lengthy discussion about the philosophies of yoga
  practice.
- Emphasis is put upon the impermanence of everything, a notion which is
  anathema to the ego, and the interconnected nature of the universe.

> We learn to become aware of our deep perceptions as both vital and real but
> more important we recognise that our own forms of perception are the gateway
> into the matrix that intimately connects us to everything else. Matrix means
> the womb. It comes from the word, "Mother," and it implies that there is a
> nests that interconnects and sustains everything. Whatever your practice is,
> no matter what you think or experience, all of this is cradled within the
> matrix called yoga. The matrix itself has no motivation or desire, but it
> allows each and every thing within it to evolve fully so that everything
> finds it mate and its complement in order to become actualised. Just as a
> mother, with unconditional love, supports her child, the matrix allows
> everything to grow, flourish and flower, and it also allows everything to
> die or to disappear. In this way, all things discover themselves and they
> also ascertain their relationship to and their interconnectedness with,
> everything else. From whatever point we initiate our own yoga practice, and
> incidentally we must begin from where we actually are, this matrix starts
> to open for us, and we find that we can go ever deeper into our own
> immediate experience, just as when digging a well. We see that each
> philosophical stance and every practice is a composite of all other
> philosophical perspectives and of all the other types of practice. We
> experience for ourselves that each is nested within a more complex
> interwoven pattern of the yoga matrix, where no type of practice or theory
> is dominant and where the pure radiant presence, the underlying nature of
> the matrix itself, is revealed.

## Chapter two: The Body and Mind as Fields of Experience

- The chapter opens with a discussion about the purpose and nature of the
  breathing and chanting exercises which often precede a yoga practice.
- Focusing the mind on the here and now is a method of emphasising the
  impermanence of everything.
- The practice begins from where you are now, but where that might be is not
  important because, in relation to the matrix of yoga, every place in the
  universe reflects the entirety of the universe.
- A huge emphasis is put upon observation.
- You should not seek to know and to conquer the yoga practice, just as the ego
  would have it, instead observe your practice.
- Natural, subconscious reactions to stimuli can be misleading or misguided.
- They are however natural and so part of the yoga practice should be to observe
  these natural reactions so that in future you might avoid them and avoid
  misleading yourself.
- This can refer to the natural feelings within the body when adopting, for
  example a difficult twisting pose, or more broadly thoughts and feelings about
  the world at large which may arise during practice.

> The mind and the ego are both so eager and endlessly willing to jump in,
> organise, categorise and to know in order to move on. For example, the body
> is much much more than the theories and maps that the mind and ego are prone
> to make about it. Our theories - the patterns we know as 'the body' - are
> helpful to a point, but they must be released, lest they turn into knots and
> we become stuck in the ways that we move, think or interact with the world. It
> _is_ important to understand and categorise but it is equally important to let
> go of these organisational tools at the right moment. Just as we know when we
> look at a map that it is not actually the territory it represents, so too we
> know the work of our mind and ego is not the whole picture. Maps are extremely
> helpful: without them, you could be lost. But no map can describe the entire
> territory. Imagine that you are able to create the perfect map. If you had
> such a map, it would contain everything: all the roads, the streets, the hills
> and the valleys. In fact, the perfect map would not only be a street map, but
> it would also be topographical, and would eventually be as detailed and as
> mysterious as the configurations of the grains of sands within the territory
> of the map itself. You would have the world's perfect map, but you would not
> be able to fold it up and put it in your glove box, so it would be very
> difficult to use. That is the inherent problems with maps, they are wonderful
> and useful, but no map is the territory it represents. By the same token, yoga
> is not a quest for omnipotence, as the ego would have us believe. Rather, it is
> freedom from this never ending forever incomplete mission of the ego in search
> of omnipotence.

## Chapter three: The Process of Hatha Yoga - Union of Sun and Moon

- The chapter opens with a long discussion both about the connection of the breath
  and the mind and of the inhale with the exhale.
- The left and the right nostrils are discreet channels for the breath and a
  balance must be sorted between them, like the balance between the Sun and the
  Moon, from which 'tha' of hatha yoga gets its name.

> The foundation of hatha yoga practice becomes the act of observing your
> breath flow, shifting and balancing so that you can track its effect on
> changes in the patterns of your awareness and in how you imagine yourself
> and your world. Notice any Sun-like temptation to reduce your observations
> to just a theory. Instead, stay with the open, vibrating quality of the
> breath and everything within it.

## Chapter four: The Roots of the Practice

- In yoga, we must form relationships with others.
- We must hold all sentient beings in our hearts all the time.
- The truth of impermanence is heavily emphasised again.
- It can be used to create or enhance connections with others – when you look
  into someone's eyes, anyone's eyes, there is a connection in that you are
  both dying, impermanent.
- Embracing the impermanence and abandoning the concept of meaning allows the
  mind to be free.

> The Buddha taught that there are four noble truths: first, the truth of
> suffering; second, that there is a cause for suffering; third, that there
> is or can be a cessation of suffering and fourth, that there is a path to
> that cessation. These truths apply directly to the path of yoga.

## Chapter five: Buti and Context

- Buti is the 'context maker.'
- It lies at the centre and therefore contextualises other meanings.

> It is extra-ordinarily difficult for any of us to be able to maintain an
> unobstructed awareness of any experience, because when an observation
> first registers, our own mind covers it and the true unobstructed nature
> of that which is being observed instantly disappears. However, as we
> deepen our yoga practice, we find moments in which our instinct to
> understand, label, define, categorise and judge that which we are
> witnessing can be suspended so that we simply see what we are seeing.
> This intensity of openness and awareness, the need for constantly
> re-folding our awareness stack onto itself as we continuously re-awaken
> our senses summarises the entire process of yoga.

## Chapter six: Bhagavad Gita and the Unfolding of Love

- The chapter begins with a discussion about the imperfection of everything.
- All pursuits have a degree of imperfection, to which the author often refers
  as a residue.
- There is a discussion about karma yoga, including an exploration that the most
  rewarding reason to pursue any task, including a yoga practice, is for the
  enjoyment of the task rather than for material or physical gain.
- The author discusses how the perfectionist can seek to improve every pose in
  their practice, for example by focusing their perfectionism on the residue of
  imperfection within a certain pose.
- This is similar to how perfectionism works in any other aspects of life.
- However, this concept, that only by perfecting the practice will the benefit
  be achieved, or alternatively that in order to achieve the benefit, you must
  simply perfect the practice, is flawed.
- Instead, the residue should be given to the practice.
- Focus on what you are experiencing now: not what you want to happen, not what
  you expect to happen and not what has previously happened under similar
  circumstances.

> A major theme in the Bhagavad Gita is that all of our actions have
> some element of imperfection to them. The outcome of any given action might
> be good, but it will not be absolutely perfect.

## Chapter seven: Tantra and the Radiant Earth

- I picked up almost nothing that I can summarise in words from this chapter.
- Don't know why!

> Tantra means extending a thread or a weaving of threads. It implies the
> stretching out on a loom of connections in order to form and
> inter-penetrating network or a matrix. Tantra forms a vast complex of
> specific patterns and rituals done in endless detail to sanctify every
> particular of our experience.

## Chapter eight: Yoga Sutra

- Among the teachings of yoga sutra are nine possible distractions from the
  practice, with which the practitioner must engage.
- The first is disease in any form.
- The practitioner must engage with ways to rid themselves of this affliction.
- The second is to practice despite illness, even if it means modifying the
  practice.
- This principle is a recognition that at some point, we will all get sick and
  never recover – we will all die.
- Continuing to practice while unwell as a preparation for dying.
- The next obstacle is dullness.
- The yoga practice must be approached with vitality.
- Another obstacle is doubt.
- Doubt in and of itself is not negative – at the most reductive level, it simply
  means accepting there is more than one way to approach a problem.
- What must be avoided is the paralysis this doubt can bring.
- Accept the paradox that both solutions are linked rather than lingering in the
  doubt.
- Doubt can be a powerful obstacle from many practitioners as they believe it is
  the ego forcing its way into their practice.
- This is not true.
- Another obstacle is delusion, carelessness or not seeing things for what they
  are.
- Among other things, it is resolved by paying attention to feedback from the
  body.
- Another obstacle is laziness.
- Laziness refers not only to the physical hindrance, but also to the mental
  laziness of wishing to stay in a pleasant state.
- Laziness fights against the embracing of impermanence.
- Hankering is the next obstacle.
- It is the mind searching for the peaceful happiness that many practitioners
  experience once they have settled into their practice.
- To avoid it, understand that all sensations, pleasant or otherwise, are just
  sensations and are there to be experienced.
- The next obstacle is bad philosophy, and refers to not just incorrect ideas, but
  also holding fixed good philosophies in a dogmatic and unwavering way.
- Flexibility is key.
- The counter extreme, whereby all philosophies are equal and none are wrong, is
  also an obstacle as this loose, somewhat amorality, blunts the sharpness of the
  mind.
- The next obstacle is a failure to obtain grounding in any state and can arise
  from a lack of tuition and guidance.
- The final obstacle is instability, in which the mind can very quickly wobble
  from concentration.
- One of the simplest ways to combat these obstacles is a practice known as
  oneness.
- It is a practice in which a single focus can be held as the basis of the
  practice.
- By focusing on this single issue, all of the ways of yoga can be explored.

> Whatever your unique life circumstances are, if you find any kind of
> pleasing content of the mind that inspires, anything that can actually
> be a seed for your contemplation, then contemplate it. This teaching from
> the yoga sutra demonstrates a remarkably open minded approach, composed
> with the intention of truly teaching people to find whatever method
> possible to liberate the mind. The yoga sutra could even be considered a
> non-sectarian description of the generic process of cultivating genuine
> mystical experience.

- Ashtanga yoga, by its very name, means eight limbs.

> Yama, ethical practices

> Niyama, observances

> Asana, postures

> Pranayama, extension of the internal breath

> Pratyahara, abstraction of the senses

> Dharana, concentration

> Dhyana, meditation

> Samadhi, deep meditation in which all sense of a subject and an object
> disappear

- For most westerners, the most relatable limb is the poses.
- The poses are one of the four outer limbs, and the experienced practitioner
  understands that none of the inner limbs can be attained without balance in the
  outer limits.
- The limbs are not sequential.

## Chapter nine: Breaking Down the Fundamentalism

- The finishing chapter opens with a long discussion about gurus, their origins
  and importance, and numerous pitfalls in to which an experienced guru can fall
  and take with them their students.
- The next section of the chapter talks about the practice itself and give some
  tips on how to improve and maintain your practice.
- The end of the chapter heavily emphasises relationships, interconnectedness and
  impermanence as fundamental to yoga.

[Home](/blog "ohthreefive")
