+++
         
date = "2019-11-19T23:29:03.311Z"
title = "Summaries: The Secret Barrister"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Secret Barrister

**The Secret Barrister**

## Introduction - My opening speech

- The novel is introduced by the author, who will remain anonymous, and his
  intention is made clear.
- Namely, he wishes to highlight the inadequacies in the current criminal
  justice system, how they arose and possibly how to improve things.
- The book will be structured like a criminal case.

## Chapter 1 - Welcome to the Criminal Courtroom

- A brief history followed by discussion of the skeleton of the criminal court
  and criminal trial process.
- Also mentioned are the differences between solicitors, advocates and
  barristers, which the author admits remain difficult for the average person to
  retain.
- *This holds true for me after listening!*
- Some persistent antiquities within training of barristers and the criminal
  court are mentioned.
- The author concludes by commenting that the British criminal justice system,
  which is quite different to those elsewhere, is upheld as the gold standard
  but that, in the author's opinion, this is only true when it works as it
  should.

## Chapter 2 - The Wild West: The Magistrate's Court

- This chapter is an all-out assault on the magistrate's court, for which the
  author has little regard.
- In his introduction to the court, he details the rational and well-intentioned
  reasons for its existence and methods of operation, but he subsequently
  deconstructs its multiple failures, particularly the way it fails those on
  trial.
- Of particular concern to the author is that the magistrate's system is
  sufficiently efficient that versions of it look like being the future of
  dealing with low-level cases.
- For example, uncontested cases cases being dealt with by a magistrate or
  similar in the absence of even a hearing or even defendants entering their
  plea and receiving their judgement entirely online.

## Chapter 3 - Imprisoning the Innocent: Remand and Bail

- The author sets out a defence of the concept of bail: the justice system
  assumes that all accused are innocent until proven guilty and so when awaiting
  trial, the defendant should not have their liberty removed without reason.
- Removing liberty by not granting bail for up to 60 days in the magistrate's
  court and 182 days in the Crown Court can have life changing consequences.
- There is even a paradox of consequence for those eventually found innocent
  compared to those found guilty.
- The guilty have their eventual sentence essentially reduced by counting the
  number of days without bail as days of their sentence served.
- The innocent do not even receive the most meaningless, automated apology for
  the inconvenience.
- The author again points out points out his opinions as to _why_ the bail
  system is a failure.
- Complex bureaucracy and underfunding are chief.

## Chapter 4 – Watching the Guilty Walk Free: Prosecuting on the Cheap

- This chapter is largely as diatribe of the pre-trial process, more
  specifically the areas in which the victim can be failed.
- These are largely administrative failures but can have many causes, for
  example underfunded and overworked lawyers and police officers.
- To some extent, the author seems to believe that process is so inefficient and
  full of failure is that it leads to a certain apathy which then makes the
  problems worse.

## Chapter 5 – The Devil's Greatest Trick: Putting the Witness First

- A modern change in court proceedings, public awareness campaigns and
  perception around criminal and other trials is that the victim should come
  first.
- The author points out many ways in which this has failed and even some ways in
  which this is too reductive a goal.
- Again, chronic underfunding of court and other services is a major issue with
  complainants, defendants and witnesses attending for trials which never take
  place due to inadequate court time.
- The author also draws attention to cases being brought to trial which probably
  never should be due to someone, for example in the Crown Prosecution Service,
  being worried about the public image of a case being thrown out without a
  trial (for example historical child sex abuse allegations.)
- Lastly, the restrictions put on the questioning victims can undergo can
  conversely negatively affect the defendant, who in a criminal trial is
  innocent until proven guilty.
- If restrictions on questions, ostensibly to protect the victim, lead to a
  wrongful conviction, then in the authors opinion this putting the victim first
  has gone too far.

## Chapter 6 - Defenceless and Indefensible

- The author focuses on two subjects for criticism in this chapter.
- Firstly is the paltry fees that solicitors and barristers are paid for
  criminal cases.
- In his opinion, this has forced them to take on more cases and pay less
  attention or give less time to them.
- Secondly, he decries failed regulation of solicitors though admits that the
  blame also lies within his profession as the regulatory authorities rely upon
  reports to investigate.

## Chapter 7 – Legal Aid Myths and the Innocence Tax

- The author sets out an argument that cuts to legal aid and the way public
  cases are financed punishes the innocent.
- Cuts to legal aid and the introduction of means testing means that legal aid
  is not available to those whose household income – not simply the income of
  the accused – are greater than £37,500 are not eligible for legal aid.
- In addition, legal aid rates paid are at a lower level than what private
  representation would cost.
- The author makes the following comparison – it is as if you break your leg and
  the government tells you you are not eligible for treatment on the NHS but any
  subsidy it will give you will only be at NHS rates.
- He highlights a case in which a successful defendant ended up paying more in
  legal fees than the fine he would have received if he had pled guilty to the
  offence he was innocent of.

## Chapter 8 – Trial on Trial: Part I – The Case Against

- The author of tries to pick out the flaws in the adversarial system.
- While barristers are not allowed to hide the truth – if their client admits
  guilt, they cannot argue their innocence – they are allowed to pick holes in
  evidence of vulnerable people whose previously given statements and current
  memories may be distorted.
- In Europe, an inquisitorial service exists with a judge leading a case and the
  search for the truth behind the case being the ideal outcome.
- (Although it does not seem to find address nor to explain the event.)
- The adversarial system seeks to prove guilt beyond reasonable doubt, with the
  true narrative of the case not being relevant or even known by its conclusion.
- The authors point out that he does not believe inquisitorial trials should
  replace adversarialism and will explain why in the next chapter.

## Chapter 9 – Trial on Trial: Part II – The Case for the Defence

- The other points out two flaws in the inquisitorial system.
- The first is incompetence.
- He can remember many situations in which evidence requested by the defence,
  has been either wilfully or accidentally withheld by the prosecution up to and
  even beyond the 11th hour, potentially leading to wrongful convictions or even
  wrongful trials.
- Bearing in mind that the falsely accused may have been remanded in custody
  awaiting this trial and have had their liberty withheld.
- The second point is politics.
- The government should not be in charge of the justice system or rather
  determining a persons guilt because of political influences.
- For example, after the failure to positively respond to allegations against
  Jimmy Savile, there was a wrongly believed allegation against a group of
  politicians and generals which, despite little evidence, was taken forward
  because it was politically important not to make a mistake like Savile again.
- The impartiality of an independent adjudication system is essential.
- The author also believes that the search for truth, the goal of the
  inquisitorial system, is less important than the question asked of the
  adversarial system: "Based on the evidence, can we find this person guilty
  beyond reasonable doubt?"
- The adversarial system protects the falsely accused but may fail to convict
  some truly guilty people.
- Based on his limited experience, the author believes that this is the correct
  way round – no one innocent should have their liberty removed.

## Chapter 10 - The Big Sentencing Con

- The author has major issues with not only sentencing but how it is represented
  by the media.
- Sentencing laws and guidelines, which are imposed on judges, are exceptionally
  complicated and seemingly unbalanced with maximum prison times not seeming to
  represent the true gravity of the crimes committed.
- These factors are outside of the judge's control.
- Briefly touched upon point is the fact that sentencing can be affected by the
  state of mind – or indeed hunger - of the judge, or more seriously the
  ethnicity and even gender of the accused.
- The author passes over this last point quickly though I believe there is some
  compelling and disturbing evidence supporting it.
- In the second half of the chapter discusses how United Kingdom has a very high
  rate of imprisonment and very low rate of prison standards.
- He concludes with the paradox that official statistics suggest present
  sentences are becoming more common and more lengthy but the public believe
  that the criminal justice system is becoming more lenient.

## Chapter 11 – The Courage of Our Convictions: Appeal

- The author's major complaint about the appeal process and being acquitted of a
  crime as a miscarriage of justice is that compensation is unfairly difficult
  to attain.
- In fact, it has become _more_ difficult as legislation now means that the
  wrongly accused must prove beyond reasonable doubt that they did not commit
  the crime of which they were wrongly found guilty.
- This of course is the opposite of the proving beyond reasonable doubt that
  someone did commit the crime before they can be convicted of it.
- The author lays the blame for the recent increased strictness on former
  Justice Secretary Chris Grayling, before closing out the chapter by mentioning
  that when he moved on to become the transport minister, he increased the
  compensation to those who trains were delayed by more than 15 minutes.

## Chapter 12 - My Closing Speech

- An excellent concluding chapter drawing together the arguments of the previous
  dozen or so.
- The author is fairly clear that the government are largely responsible for
  failures in the current criminal justice system.
- He means this in both an educational sense – few lay people are even remotely
  aware what a barrister does – and in the lack of funding for the criminal
  justice system.
- He lays out his rather strong argument that a functioning and independent
  criminal justice system is essential for the good, the bad, the innocent and
  the guilty alike.
- The other party which receives his scorn is the media.
- Simplistic and sensational reporting both skews the opinion of criminal
  justice in the mind of the public and exerts political pressure which is
  unwarranted and sometimes even actively harmful.

[Home](/blog "ohthreefive")