+++
         
date = "2019-11-02T08:15:55.268Z"
title = "Summaries: The Secret Commonwealth"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Secret Commonwealth

**Philip Pullman**

## Chapter 1: Moonlight and Bloodshed

- Pan and Lyra have been growing apart.
- Pan goes to explore Oxford one evening without Lyra.
- He witnesses two men meet at the Royal Mail depot, apparently waiting for
  someone.
- Earlier that day, in Geneva, two other men meet.
- One, who works there, says a scientist has disappeared.
- The other, the visitor, who works in discipline and security within the
  Magisterium, comments that he was likely researching 'rose' or similar.
- The first says that he will soon be apprehended in England.
- The Magisterium man asks if there is any more information about the girl
  (presumably Lyra.)
- The first (Delamaire) says that she has many protections, including legal,
  and that he would like to break these defences down.
- Pan sees the two man attack and kill a newcomer.
- One of them is injured and so they leave, apparently without something they
  had come for.
- Unexpectedly, the 'dead' man's daemon flies to Pan and beckons him down.
- The dying man tells Pan to take his wallet and stresses its importance.
- Pan returns to find Lyra asleep, and takes comfort in her warmth.

## Chapter 2: Their Clothes Smelled of Roses

- Lyra awakes and muses that she does not even know why she and Pan have become
  estranged.
- As they make their way to breakfast, Pan begins to tell her about the events
  of the previous evening but Lyra cuts him off.
- At breakfast, she sees a friend called Miriam reading a letter, looking upset.
- She says her father has been bankrupted.
- To cheer her up, Lyra suggests they go to another college than theirs (St.
  Sophia's) where breakfast is better.
- Miriam says her father makes perfumes and can no longer get the supply of a
  specific rose (from ?Leticia?) because medical companies are buying them all
  up.
- The specific rose is important - English Roses would not be an alternative.
- Lyra manages to cheer her up somewhat.
- Afterwards, she speaks to Pan who says Miriam's daemon thinks the real issue
  is that Miriam is wooriried what her boyfriend and family will think of her
  when she is poor.
- Lyra apologises to Pan for interrupting him earlier and lets him finish.
- Pan says he witnessed a murder.

## Chapter 3: Left Luggage

- Pan and Lyra look through the dead man's wallet.
- He has a note from the home office requesting free passage and assistance
  through the Ottoman Empire for the bearer.
- He was a botanist.
- They also discover a meeting date, time and place, for the following year, and
  a key to a left luggage locker.
- Lyra thinks they should take this to the police; Pan comments that the man
  thought they were the only hope now.
- Pan mentions that the man and his daemon could separate like him and Lyra.
- They decide to got to the police.
- At the police station, Pan sees one of the men involved in the attack: he is a
  police officer.
- They leave quickly, first to inspect the scene of the crime then to retrieve
  the lost luggage.
- Pan comments that Lyra used to be optimistic, now she is pessimistic; Lyra
  replies that she used to be young.
- The retrieve a rucksack.
- Pan sees someone apparently watching them so Lyra runs all the way back to her
  room in her college.

## Chapter 4: The College Silver

- Delamaire speaks to someone about the operation, asking why it went wrong and
  asking that they check again to see if the dead man was carrying anything.
- The man, Olivier appears tired and stressed and says this is because of the
  'new method.'
- Delamaire says he does not trust the new method and it shouldn't be used.
- Olivier says he thinks someone saw the operation, and that it might have been
  'the girl.'
- Lyra goes to meet one of her professors, and asks about trade in roses.
- He tells her about the history of rose trade around the Byzantine and Ottoman
  Empires and about attar of roses (rose essence) and says he will look in to
  any current problems.
- Lyra goes to Jordan college to earn money by polishing silver before a
  celebration.
- She finds herself polishing a rose water bowl and the master tells her this
  might be the last time it is used as they can no longer source good rose
  water.
- He says the best comes from Levante or further afield but they cannot get it
  anymore; he gives Lyra a tiny sample of his.
- She enquires and he says if she wants to know more she should speak to a
  Jordan professor (who used to work at St. Sophia's.)
- Lyra decides against this as she finds him boring.
- She stashes the rucksack in Jordan despite Pan wanting to see what is inside,
  then works on an essay.
- She goes to see an old boyfriend in a bar whom she has correctly remembered
  now works in the mail depot.
- He confirms that a worker has hurt his leg - this is the other attacker.
- Though enjoying his company, she realises she can never share everything about
  her with him and makes her way back to Jordan.
- Inside the rucksack are bottles and toiletries, two books, some scientific
  papers and a notebook.
- Most of it is in foreign languages Lyra does not understand but some of the
  notebook is in English.
- Lyra wants to go back to St. Sophia's to read it due to the bad light.
- Pan upsets her by asking sarcastically if he will be allowed to read it too.

## Chapter 5: Dr. Strauss' Journal

- Pan and Lyra read the journal.
- In it there is a man called 'Hassle' who journeys in to a desert in search of
  a red building which is heavily guarded.
- They grow roses in this building that cannot be grown elsewhere.
- In order to enter this desert, Hassle must separate from his daemon, which
  brings back painful memories for Pan and shame from Lyra.
- If you lose your daemon, you can find him in a place translated as 'blue
  hotel.'
- The roses tell something about the nature of Dust.
- For this reason, The Magesterium also wants to find the red building.
- When Lyra finishes reading, she tries to engage Pan by saying there might be
  other separated like them out there in the world.
- She then notices that a novel which she likes but Pan hates is lying on her
  floor: she thought she had put it away.
- Pan wants her to get rid of it and calls it poison; Lyra thinks Pan is being
  ridiculous.
- The story about the red building tickles a memory in Lyra, but she cannot
  fully bring it back.

## Chapter 6: Mrs. Lonsdale

- Lyra asks Pan what has happened between them the previous year and gets a
  surprisingly forceful response.
- The first regards the novel Hyperchorasmions, By a German called Branda.
- In it people have no daemons and a man decides to kill god and succeeds.
- It contains a now widely popular phrase, "It is nothing more than what it is."
- It is a commercial success but looked down upon critically.
- The Constant Deceiver, by Simon Talbert, an Oxford professor, is critically
  praised but is similarly absolutist.
- Talbert argues truth and even reality have no real long-term meaning.
- Pan finds these thoughts, ideas and themes contemptible, and says Lyra has
  been influenced by them and they are making her forget him.
- Lyra tries to explain that she is just exploring other philosophies.
- After the argument, Pan decides not to tell Lyra something about the notebook
  they read the previous night.
- Lyra goes to see Mrs. Lonsdale, a housekeeper of sorts who has been keeping an
  eye on Lyra.
- The talk about Dr. Polestead, around whom Lyra feels awkward and thinks he
  feels the same.
- This is of course Malcolm Polestead, the protagonist of *La Belle Sauvage*.
- Unexpectedly, Polestead arrives but then quickly leaves when he sees Lyra.
- He calls Mrs. Lonsdale 'Alice', revealing her to have also been part of *La
  belle Sauvage*.
- Mrs. Lonsdale says Polestead is responsible for her being there and they will
  tell her story soon.
- Later, Pan confides that he thinks Polestead's daemon, a cat, saw him the
  night of the murder returning with the wallet.
- This implies Polestead and his daemon are also separated.
- Lyra thinks Polestead will assume they are thieves.
- Lyra voices her disappointment at Pan.
- The new master of Jordan college is a pharmaceutical businessman called Dr.
  Verner Hammond.
- He was intrigued by Lyra's position in the college and invites her for dinner.
- Dr. Hammond tells her the money left by Dr. Khan for her tuition,
  accommodation and meals has run out.
- Jordan college will loan her to finish her studies but she will have to vacate
  her rooms and move in to essentially a store cupboard.
- He also says she will not be allowed to socialise with the academics etc. as
  she has been used to.

## Chapter 7: Hannah Relph

- Dr. Relph, who has been teaching the alethiometer to Lyra, summons her.
- When Lyra arrives, Alice and Malcolm are also there.
- The tell the story of *La Belle Sauvage*, including Bonneville's obsession
  with Lyra (which they have not yet explained) his rape of Alice and murder by
  Malcolm.
- Lyra is understandably shocked.
- She sees some parallels between Malcolm and Will.
- They explain that she is in danger: a Geneva-based organisation called La
  Maison Juste has lobbied a bill through parliament which includes removing
  scholastic sanctuary, under which Lyra currently is kept safe.
- They explain that Dr. Khan was convinced to make bad investments, losing some
  of Lyra's money.
- The also explain that Dr. Hammond's recent treatment of Lyra may be because he
  is also part of the plan to weaken her protections.

## Chapter 8: Little Clarendon Street

- Malcolm and Lyra talk about separation, and Lyra mentions that the dead man
  also appeared able to do it.
- Lyra confides in him the details of the murder Pan witnessed and the rucksack.
- They quickly return to her room and find it ransacked.
- The rucksack is gone but Lyra reveals she has stashed its contents elsewhere.
- She shows them to Malcolm.
- Malcolm takes Lyra to stay with his parents; his mother tells her tales about
  the flood and Malcolm and Alice's adventure.
- That night Lyra dreams of Malcolm and Will's daemons, and of the red building,
  and knows she must go there.
- Malcolm goes to Hannah to show her the contents of Dr. Strauss' bag: rose oil,
  rose seeds, books and the journal.
- They draw some conclusions and remember a story about a scientific outpost in
  China whose scientists have been going missing.
- Given how quickly Lyra was found, Malcolm assumes the 'other side' have an
  alethiometer.
- They agree to tell Lyra about Oakley Street, for her safety.
- Hannah talks about the new method for reading the alethiometer, and remembers
  that a talented reader with this method is called Olivier Bonneville.

## Chapter 9: The Alchemist

- Malcolm goes to the botanical society to enquire about the man (Hassle) who
  has been killed.
- The director of the society says he was a researcher and was missing, presumed
  dead.
- On his way back from there, Malcolm realises he is in love with Lyra.
- At the trout, Malcolm's parents discuss how melancholy Lyra is and think about
  ways to help.
- Upstairs, Lyra muses about how she and Pan appear to hate each other and have
  done for a while.
- They have an argument in which Pan accuses her of having no imagination.
- Lyra has secretly been experimenting with the new technique to read the
  alethiometer.
- She does it again and in her reverie, a cat (or Will's cat daemon) leads her
  to a man she initially mistakes for Will.
- He is however Olivier Bonneville and his daemon is a sparrowhawk.
- He seems to recognise Lyra.
- When she comes back from this, she is tired and goes to sleep.
- Pan takes out a notebook from the rucksack he has been hiding and takes it to
  an alchemist called Makepeace.
- Lyra and Pan met him sometime ago and he knows about their separation through
  an acquaintance with a witch.
- He says he knows some information about the journal but will only tell Pan if
  he comes with Lyra.
- He writes something in the journal.
- They discuss their estrangement.
- They discuss his current research and he says he has discovered a field and is
  now trying to discover whether the field is the same or different everywhere.

## Chapter 10: The Lineas Room

- Malcolm is invited to a meeting by the director of the botanic society.
- Lyra discusses Malcolm with Pauline, one of the kitchen girls.
- Pan thinks Pauline is in love with Malcolm; Lyra cannot see Malcolm as an
  person who could be loved.
- Despite the recent revelations, he is still a teacher to her.
- Hannah goes to see her boss at Oakley Street who agrees to provide protection
  for Lyra and agrees that all this is something of interest to them.
- At the meeting, the botanical society director has assembled numerous people
  with knowledge of Hassle and of his areas of interest.
- One tells them about how rose oil can aid microscopy and enhance visibility of
  the Rusakoff field.
- He believes there is a link to dust and this is why the magisterium are
  involved.
- A captain from the CCD interrupts and questions to group but thanks to
  Malcolm's expertise from within Oakley Street, he manages to divert the man.
- Pan confesses to Lyra about her meeting with Makepeace.
- This begins a passionate argument between them with Pan throwing insults her
  away for her abandonment of him on the shores of the land of the dead.
- Lyra confesses she is so depressed to the extent that she would consider
  suicide if she thought Pan would survive.
- Pan continues to press Lyra by saying that her current attitude to the world,
  influenced by the books she is reading, is part of the reason she is so
  unhappy.
- They both end sobbing and unable to speak to each other.
- When Lyra wakes, Pan is gone.

## Chapter 11: The Knot

- Lyra finds a note from Pan saying that he has gone to look for her
  imagination.
- She panics, not knowing when or even if he will return.
- Gathering her thoughts, she realises that her old Gyptian mentor, Fada
  Coram, might be worth asking for advice.
- She then remembers her old boyfriend Dick mentioning he had an Gyptian
  grandfather.
- She goes to see Dick who advises her how and where to find her grandfather,
  which she subsequently does and he agrees to quietly escort her on his boat to
  Fada Coram.
- In Geneva, at La Maison Juste, Marcel Delamaire has acquired a small sample of
  rose oil from an operative.
- He questions the operative though he already knows the answer to most of the
  questions: he just wants to see how much other people know.
- He is planning the greatest ever meeting of the Magisterium, where oil will be
  the major topic, though he notes that many of the attenders will not know this
  in advance.
- Malcolm realises Lyra is missing.
- His daemon Asta correctly theorises that her disappearance has something to do
  with the terrible relationship between Lyra and her daemon.
- He first visits Alice who has no extra information for him but does comment
  that she thought there was something between Lyra and Pan and wondered if they
  could separate.
- He next visits Hannah who agrees to use the alethiometer to help, though she
  comments that it may be more useful to ask why rather than where Lyra has
  gone.
- She then confronts Malcolm as she believes he is in love with Lyra and advises
  him not to let his feelings cloud her judgement his judgement.
- Malcolm is taken aback by the sudden, confident and correct assertion about
  him but thinks that the advice was, if anything, counter-productive.

## Chapter 12: The Dead Moon

- Lyra and Dick's grandfather, Giorgio, pass the journey exchanging stories.
- Giorgio tells her numerous fables which she claims to him to believe but to
  herself thinks they are just stories.
- Giorgio says that most people do not believe them and mentions the influence
  of the popular book Lyra has been reading reading in changing peoples
  attitudes towards these fables.
- Lyra remembers some details from Hassle's journal including the Blue Hotel.
- Giorgio says that he thinks a story does mention that place or something like
  it but he does not really know what it is.
- He theorises that maybe it is a land of the dead for daemons.
- Olivier Bonneville is frustrated by his inability to spy on Lyra using the new
  method for reading the alethiometer – it has recently stopped working.
- I wonder if this is due to the separation of Lyra and Pan.
- He tries holding a photograph of Lyra and when he does this, finds himself in
  Marcel Delamaire's room and Delamaire has a collection of photographs of Lyra.
- A drawback of the new method is that it only works in the present tense, that
  is it can show you events but neither their cause nor consequence.
- Bonneville decides that he will have to make traditional enquiries about
  Delamaire as nobody knows about this obsession he has been hiding.
- Malcolm decides to confront the injured attacker and tried to press
  information out of him.
- He manages to get him to confess that the bent copper he worked with has a
  contact within the college is called Simon Talbert.

## Chapter 13: The Zepplin

- Pan is in search for Lyra's imagination.
- Though he could navigate Oxford well himself, he now misses Lyra's ability to
  ask questions of people to navigate.
- He manages to stow away on a boat.
- Malcolm and Hannah visit the Oakley Street director, Glenys Godwin, in the
  presence of another operative, Charles Capes.
- Godwin's plan is that Hannah continues to use the alethiometer to search for
  Lyra.
- Capes is to infiltrate the meeting of the Magisterium.
- Malcolm is to head east in search of the red building.
- He is the only one who can do this as he can already separate from his daemon.
- He is also to investigate the unrest in the Levante.
- At the meeting in Geneva, Delamaire passes a motion to reduce the size of the
  ruling council of the Magisterium.
- Increased unity among the Magisterium was something Godwin feared – Oakley
  Street is under resourced and understaffed to fight them.
- Lyra and Giorgio are just entering the Fens in the middle of the night when he
  hears a noise and spies a zepplin overhead, searching.
- In the distance, they see some marsh lights which Lyra attributes to methane
  whereas Giorgio says they are jack-o'-lanterns.
- The zepplin fires on these lights which enrages Giorgio as he says that is
  illegal.
- In response, the lights intensify and in doing so, threaten to light up their
  boat in the dark.
- The zepplin is also turning its searchlight towards them.
- In desperation, Lyra wishes Pan was there and, although her rational mind
  fights against it, she wants to delve into the Secret Commonwealth, herd the
  jack-o'-lanterns and use them to help her.
- The jack-o'-lanterns converge on part of the marsh and a white bird, which
  Lyra thinks is a heron, flies up away from them directly into the engine of
  the zepplin, causing it to crash and burn.
- Georgia is amazed though he says the bird was actually probably a boggart,
  another creature of the Secret Commonwealth.
- Lyra is initially sickened by the inevitable deaths of the crew of the
  zepplin, with Giorgio telling her the jack-o'-lanterns will drown any
  survivors, however she soon regains her appetite and enjoys a meal with him.

## Chapter 14: The Café Cosmopolitan

- Dick goes to see Malcolm and tells him that Lyra visited him and has gone off
  with his Gyptian grandfather because Pan has left her.
- Malcolm thanks Dick and tells him to confide in Alice and Hannah but no one
  else.
- At the Magisterium's conference, Delamaire continues to bargain and manipulate
  to form a small council which can take action quicker.
- Among those talking in his support is Simon Talbert, who introduces himself
  not as a member of the Magisterium but as someone reporting on the conference.
- Nearby is the café of the chapter title and it is said to be a place for
  information to be exchanged.
- Olivier Bonneville has found someone who may know Delamaire.
- He does not get much information himself but his daemon learns that
  Delamaire's older sister was once high up in the Magisterium, caused a scandal
  when she had a child out of wedlock and caused heartbreak in her brother when
  she disappeared about 10 years ago.
- That child is Lyra.

## Chapter 15: Letters

- Lyra reaches the Fens and is warmly embraced by Ma Costa.
- She goes to meet Fada Coram and had a long chat with the now elderly Gyptian.
- He tells her a little about Malcolm and Oakley Street.
- She tells him about her dream in which she thinks she was with Will's daemon.
- She says she awoke with a certainty that she had to go to the desert to the
  red building, but first she had to find Pan as she needed her daemon to get
  into the building.
- She asks Fada Coram about the blue hotel but he has never heard of it.
- She thinks she might have to go there first to find Pan.
- He is, however, well versed in the Secret Commonwealth.
- A letter from Malcolm arrives in which he apologises for not having let her
  know about Oakley Street and advises her to ask Coram about it.
- He says he is going to the east to seek the red building.
- Lyra sends a letter in reply.
- She is full of gratitude for Malcolm.
- She has been missing Pan greatly and missing the confidence he used to give
  her.
- Pan, stowing away on a boat, thinks he sees ghosts, perhaps ghosts of daemons,
  trying to huddle up to him for warmth.
- He does not know if he is awake or asleep until he overhears two workers on
  the boat and suddenly cannot see the ghosts any more.
- One of the voices mentions to the other that the cargo they are picking up is
  people.
- He says these people have no papers and no passports and are being brought in
  greater and greater numbers, possibly to work on the farms.
- They are essentially slaves.

## Chapter 16: Lignum Vitae

- Lyra returns to Fada Coram.
- They have both come to the conclusion that Lyra should pretend to be a witch
  given that witches can separate from their daemons.
- Coram gives her gold, which she tries to refuse, and a fighting stick made of
  the hardest wood in the world, its name being chapter title.
- They talk briefly about Will and he says at some point she will have to move
  on.
- Lyra visits Giorgio for the last time and they discuss the Secret Commonwealth
  again.
- Giorgio advises is her only to talk about it in select company.
- Before falling asleep, Lyra wonders why she likes the company of much older
  men and realises it is because she does not worry about being attracted to
  them and therefore being unfaithful to the memory of Will.
- She starts to read the Constant Deceiver again.
- She notices how most arguments by Talbert can be summarised as, "X is no more
  than Y."
- She reads his chapter explaining how daemons are not real and muses on how
  well this educated person is been able to argue again something which is
  clearly real.
- She wants to discuss this with Pan.
- She then thinks about Branda's novel and the final line, "It is nothing more
  than what it is," and sees for the first time how similar the style is to
  Talbert's.
- She again notices that Pan had tried to point this out to her.
- She tries to summarise her thoughts in a letter to Malcolm but finds her
  attempts to be clumsy.
- She begins to fall asleep, feeling more alone and depressed than ever.
- Delamaire speaks to Bonneville, complaining that he is not making progress in
  finding Lyra and that he looks like he has been drinking to excess.
- He gives him an ultimatum that if he does not make progress within the week,
  the alethiometer will be taken from him.
- More out of petulance than skilful argument, Bonneville correctly asserts that
  no one is half as good with the alethiometer as him and that if that is what
  Delamaire wants, she can happily have it back.
- After the meeting, Delamaire quietly muses that the old methods of espionage
  might be just as effective.
- Delamaire is a private person and his mundane-seeming private life means that
  the media at-large have displayed very little interest in him.
- He goes to visit his mother who is looked after by nuns in a convent.
- She clearly has low regard for him, especially in comparison to his sister,
  and both chides and taunts him for his inability to find Lyra.
- They discuss roses and how they reveal something about the universe which is
  clearly antithetical to the beliefs of the magisterium.
- Delamaire explains his mother his many ideas of how to deal with them, one of
  them being to accept the truth they reveal and go against millennia of
  teaching, another being attacking the concept of truth itself.
- He enquired what his mother means to do with Lyra and she says that she will
  get the truth from her, break her spirit and turn her into the woman her
  mother would have been.
- Delamaire voices concern that one of his mother's lovers might be too
  influential and may have other plans for Lyra.
- They leave on negative terms, something which seems to be the norm between the
  two of them.
- Bonneville has been completely unable to get information about Lyra using the
  new method.
- The less physically arduous but slower old message has revealed to him that
  Lyra is travelling east but no more than that.
- He has been keeping the alethiometer in a safety deposit box, fearful that la
  Maison Juste may try to steal it from his apartment.
- Lyra is suddenly woken by Ma Costa and Fada Coram saying that the CCD have
  broken the treaty, enter the Fens and are searching for her.
- She is quickly spirited away on another large boat, grateful for the
  assistance but sad that she is leaving these people she loves.

## Chapter 17: The Miners

- Pan manages to escape from the boat on which he has stowed away without anyone
  seeing him doing so.
- His next task is to find the river and head north towards ?Whittengate?.
- Lyra's lack of confidence and perhaps imagination means that on her boat, she
  fails to enact her cover story at first challenge.
- She can come up with no good story when a man challenges her for not having a
  daemon.
- She is saved by the interjection of two Welsh miners who take her out of the
  ships saloon and tell her that they have had an eye on her since they saw
  someone take something from her rucksack when she entered.
- They return her alethiometer and she is incredibly grateful.
- They are extremely interested by its construction, noting that an alloy of
  gold and titanium (as they believe alethiometer to be) is very difficult and
  that the age of the alethiometer predates the the discovery of titanium by
  about a century, as far as they know at least.
- These two men met a witch recently and thinks that is what Lyra is.
- This gives her the confidence to follow through with her cover story.
- Two uniformed men, claiming to come from a new custodial arm of the League of
  Saint Alexander, stop the three of them and demand to see identification.
- This time, Lyra successfully blags by mentioning that the three of them are
  operatives working for la Maison Juste.
- This successfully deflects the men.
- Bonneville decides to try the new method with the alethiometer one more time.
- Firstly, he had realised that part of the reason for the disorientation and
  nausea the new method causes is that the user is looking through no-one's eyes
  in particular and therefore is not grounded.
- He therefore tries to listen and smell and keep his eyes closed.
- Given his lack of ability to see Lyra, he decides to concentrate on her
  daemon.
- When he is sure he can smell and hear a Pan, he briefly opens eyes seeing him
  silhouetted alone against a great river.
- Bonneville now knows that Lyra and Pan have separated and now thinks if he can
  find this river, he can find Pan.
- He also realises that the new method focuses on the daemon, not the human, and
  that is why he could no longer see Lyra.

## Chapter 18: Malcolm in Geneva

- Malcolm's spangled ring leads him to a boat on the lake.
- He and Asta briefly investigate.
- At the Congress, the new High Council elects its first leader, Saint Simeon,
  which surprises some people due to the man's age.
- His piety is unquestionable.
- Malcolm goes to the Café Cosmopolitan and finds Simon Talbert.
- Posing as a journalist, he interviews him though both men are aware of each
  other and it becomes a simple game of words and wills.
- Malcolm thinks Talbert got the better of him but Asta is not so sure, noting
  how uncomfortable his daemon became when he mentioned the corrupt policeman
  George Pastor.
- They follow Talbert afterwards and, as expected, he goes to la Maison Juste.
- Inside, he and Delamaire talk about Malcolm, whom Talbert has incorrectly
  remembered as being called Matthew.
- Delamaire casually dismisses him, leaving Talbert feeling a little put out.
- Malcolm goes to the train station, realising that now he is exposed to
  Talbert, he should leave quickly.
- The train he plans to take has an entire carriage devoted to the new High
  Council ruler, much to the embarrassment of Saint Simeon.
- He notices a huge team of unfamiliar faces have been assembled around him.
- Malcolm and many other passengers are not able to get on the train.
- He rents a room in an inconspicuous hotel and over dinner, he and Asta discuss
  Pan and Lyra, noticing how diminished in particular Lyra appears.
- In the corner of the dining room, there is a shifty-looking man.
- Later, in the middle of the night, that man knocks on Malcolm's door,
  revealing himself to be the man who had brought rose oil to Delamaire.
- He warns Malcolm that Delamaire is looking for him and all roads in Geneva are
  watched.
- Malcolm decides to escape by boat, presumably the boat to which his spangled
  ring led him earlier, and asks this man to come with him.

## Chapter 19: The Professor of Certainty

- Pan has managed to make his way to Whittenberg, and announces that his goal
  there is to speak to Professor Gustav Branda.
- He gets some directions from a blind girl and her blind daemon.
- Using the alethiometer, Bonneville is able to locate Pan in that town due to
  its well-known church.
- In Branda's garden, there is an old girl/young woman wearing a dress too young
  for her and a hairstyle too old for her playing a childish game.
- Pan briefly speaks to her and she says she is doing this because Branda pays
  her to do it and that Branda will not speak to Pan and that many other ghosts
  have come before.
- Branda initially tries to ignore Pan but then the girl bursts in, revealing
  her name to be Sabina, and the two argue.
- She says she is tired of living like this and had come here hoping that he
  would love her.
- Pan thinks they may be father and daughter but his thoughts are interrupted
  when he notices Branda can separate.
- He pursues Branda into a study where Branda denies fear of Pan and the
  concept of fear itself.
- Pan questions Branda about dust, the existence of which he strongly denies.
- Pan goes on a somewhat furious verbal assault, accusing Branda of being the
  cause of Lyra's depression and asking him why he makes his daughter wear and
  do such odd things.
- Branda has been largely unable to meet his eye, in part because he does not
  believe in daemons but now more through embarrassment and defeat.
- Pan feels compassion and notes to himself that despite her leaning towards
  rationality over all else, Lyra would also relent in her attack due to her
  feeling the same compassion compassion.
- Pan leaves and at the same time Bonneville disembarks from a train in
  Whittenberg.

Pullman is using the phrase, "At the same time," very frequently to shift
between characters in the story.
I am uncertain whether this is relevant or just a simple phrase but it's
appearances are noticeable.
I wonder if I would have noticed it so much if I was reading rather than
listening to the book.

## Chapter 20: The Furnace Man

- Lyra has made it as far as Prague.
- Unexpectedly, she is stopped by a man who has no daemon.
- He says that she must come with him to help another man who asked for her
  specifically.
- On the way, Kubicek explains to Lyra that many people can separate from their
  daemons and that they have their own secret society of sorts – he wonders if
  the daemons also have this.
- When Lyra meets the other man, she is shocked as he radiates heat and with his
  words and his tears are flames, literally.
- This man explains that his father was an experimental philosopher who was
  interested in base elements and the connections between them.
- He was also interested in the fact that some states are fixed and others are
  changeable, for example the changing appearance of a young person's daemon.
- An experiment on his son left his son assimilated with fire and his daemon
  with water.
- When his father ran out of money to conceal his son, he sold his daemon to a
  magician, leading to their separation.
- The man wants Lyra to help him find his daemon.
- Lyra assumes he wants her to use the alethiometer but he denies knowledge of
  the device.
- Suddenly Lyra remembers where she heard the name Kubicek before – it was
  written in Dr Strauss's journal which she found in Hassle's rucksack.
- She consults it and its list of addresses finds a few names based in Prague.
- She sees one name written perpendicular to the others – we saw Makepeace write
  this name when Pan presented the journal to him.
- The man's name is Agrippa and Lyra takes the other two men to see him.
- Agrippa is working in his basement.
- Lyra first goes to see him by herself and sees the burning man's daemon in a
  tank suspended above mechanical apparatus.
- The burning man, unable to resist any longer, bursts into the cellar and he
  and his daemon joyfully unite.
- However, this union causes them both to disappear in a huge cloud of steam
  which starts the apparatus moving.
- Agrippa reveals himself to be the burning man's father and the one who sent
  Kubicek to pick up Lyra when she arrived in Prague.
- It has been his plan all along to get Lyra to bring his son back to him to
  complete his experiment.
- Lyra is utterly confused.
- She is also angry at his treatment of his son and asks why this was necessary
  as steam is just steam.
- Agrippa replies that nothing is just itself and that everything is connected,
  a contradiction to the doctrine of Branda which Lyra has adopted.
- He chastises her for her adoption of this belief system.
- She is still struggling for explanations when Agrippa announces that it was
  very important that she came to see him.
- He tells her that he is the only man who could tell her whether to go north or
  east next.
- Still confused, he tells Lyra to look again at the journal and underneath
  Agrippa's name and address, in Sebastian Makepeace's handwriting, is the
  sentence, "Tell her to go east."
- Given the answers this man has been able to provide, Lyra presses on with
  questions.
- She asks if she will find her daemon and he responds that she will but not in
  the way she thinks.
- She asks about dust and he says that it is real and that the alethiometer is
  not the only nor the most efficient way of reading it.
- He tells her of one way in particular – a set of playing cards – that she will
  recognise when she sees it.
- She asks about the blue hotel and he says that it is real, gives her an
  alternative name for it and says that in order to leave it with her daemon,
  she will have to make a great sacrifice.
- She asks about the Secret Commonwealth and she says that that is the realm in
  which he is interested.
- She asks about a word she saw in the journal and Agrippa explains that it is a
  Latin portmanteau of 'by water' and 'by sea.'
- The implication is that to get to the red building, you must go by both water
  and sea and in order to do that, you must be separated from your daemon.
- Lyra again feels a tickle in the corner of her mind – she knows what is in the
  red building but she cannot get to the memory or knowledge as it just flickers
  away from her.
- She leaves with Kubicek, who agrees to help her get a ticket and shows
  her a name in the journal with him she will be safe on her travels.

## Chapter 21: Capture and Flight

- Delamaire is furious at Bonneville's actions and sends the CCD after him.
- Pan is considering his next moves and his interrogation of Branda when he is
  captured by Bonneville who is then almost immediately and unexpectedly
  captured by the CCD.
- Malcolm is trying to escape across lake Geneva when a massive storm erupts.
- Before this, his accomplice reveals that he sensed Delamaire knew the answers
  to the questions he was asking and so he with held one truth.
- Though the scientific research station was attacked, it was not completely
  destroyed as the attacking 'men from the mountains' were driven back by a
  giant apparently-not-mythical bird which Malcolm recognises from both Dr.
  Strauss' journal and from the epic historical poem he has been reading.
- The two manage to escape the storm on to the shores of Lake Geneva, guided by
  Malcolm's spangled ring.
- Bonneville undergoes heavy-handed interrogation and is struggling to keep his
  wits and dignity but eventually gains the mental upper hand on the two
  interrogators.
- This is prompted by them mentioning Malcolm's name and description – though
  they think his name is Matthew – and that he is his father's killer.
- Bonneville has almost succeeded in convincing them Malcolm is their true
  target when he manages to convince them to have a word with Pan.
- When they enter Pan's cell, they find he has escaped and Bonneville is
  furious.
- Thanks to Bonneville's smart wit and the officers' embarrassment at losing
  Pan, he manages to convince his interrogators that he is their superior.
- He has lost interest in Pan now that he has a new target: Malcolm Polestead.

## Chapter 22: The Assassination of the Patriarch

- Lyra has arrived in Constantinople and has been practising her technique of
  moving around without drawing attention.
- She learns about unrest around the region with numerous villages being
  attacked by the so-called men from the mountains.
- As she is in Constantinople, she decides to go to witness the coronation of
  the new president of the new high council.
- In his council chambers, Saint Simeon as well as numerous other priests are
  killed by men dressed in white.
- In the confused crowd, Lyra hears some people speaking in English and begins
  to talk to a woman.
- She notices but is not appalled by Lyra's lack of daemon; in fact she
  sympathises.
- She takes Lyra for a coffee to discuss things and tells her about something
  called the rose panic, caused by these men from the mountains attacking rose
  farmers.
- Members of the remaining high council meet and vote in Marcel Delamaire as the
  new president.
- His first act is to fire Binot – the CCD head of security.
- He muses about his sister and how much he is looking forward to telling his
  mother about his new position.

## Chapter 23: The Smyrna Ferry

- Lyra is on a boat to Smyrna.
- She spends the first part of the journey contemplating her recent actions and
  beliefs and reaching somewhat of a conclusion/epiphany that she has been in
  the wrong and driven Pan away from her.
- She is convinced now that the secret Commonwealth is real but rational thought
  and measurement can no more explain or observe it than can a microscope weigh
  something.
- Her thoughts are interrupted by a crash – their large boat has transected
  another, filled with refugees.
- In the panic, Lyra notices Alison ?Wetherfield? is on board and is partly
  coordinating rescue.
- She puts a young girl puts a young girl whose family have been drowned in
  Lyra's care.
- In the night time, the girl's daemon huddles into Lyra.
- In the morning, Lyra realises that rather than the daemon seeking comfort from
  her, it may well have been seeking to comfort Lyra, recognising her sorrows.
- Lyra is extremely grateful.

## Chapter 24: The Bazaar

- Malcolm arrives in Constantinople two days after the assassination.
- He quickly realises he is being followed and recognises the man as Bonneville.
- He confronts and easily overpowers Bonneville and the two talk.
- Malcolm appears to have the upper hand and direct the conversation but I am
  uncertain whether Bonneville is just acting the victim and may indeed have
  been deliberately clumsy in his pursuit of Malcolm so that he would be caught.
- Malcolm learns a little about rose oil, namely that it lessens the side-
  effects of the new method of alethiometer reading as well as revealing dust
  and other connections between parts of the world.
- Bonneville tells him that Delamaire's plan all along was to become the sole
  ruler of the magisterium, a position no one has held for centuries.
- The one piece of information which takes Malcolm by surprise is that Delamaire
  is Lyra's uncle.
- Malcolm refuses to admit to murdering Bonneville's father, knowing the
  location of Lyra (which he actually doesn't) or how Lyra came by her
  alethiometer.
- Malcolm agrees to let Bonneville go with the threat that should he tried to
  attack Malcolm again, Malcolm will either inform Delamaire's operatives where
  he is or simply kill him himself.

## Chapter 25: The Princess Cantacuzino

- Lyra arrives in Smyrna and goes to visit the name that Agrippa wrote in her
  clavicular.
- She grants Lyra an audience.
- She seems a sympathetic woman and is, like Lyra, without a daemon.
- They exchange their stories with the princess mentioning the blue hotel, to
  which she refers as the city of the moon, and saying she wonders if her daemon
  is there, giving Lyra his appearance and name so she will know him if she
  meets him.
- Before she leaves for Aleppo, the Princess gives her names of people there who
  may be sympathetic to her.
- Lyra notices a picture on the princesses table which the Princess confirms as
  her great nephew, Olivier Bonneville.
- While the princess says she does not like Bonneville and would not give Lyra
  away to him, Lyra takes the unexpected appearance of this photograph as
  something of a warning sign from the Secret Commonwealth to be on her guard
  always.

## Chapter 26: The Brotherhood of This Holy Purpose

- Malcolm continues his investigations into rose oil.
- A contact of his agrees to take him to a meeting of rose oil farmers.
- There is a discussion, including about these 'men from the mountains' who are
  destroying rose oil farms.
- Malcolm notices a number of armed men quietly entering the hall.
- One of them interrupts a farmer on stage, denouncing rose oil, evangelising
  their new doctrine and saying that it is the word of God and so the only
  doctrine.
- When the farmer argues vehemently with this man, he is shot.
- In the brief chaos that follows, Malcolm realises this was not the plan of
  these intruders and covertly manages to kill their leader and escape.
- He meets back up with his contact later.
- On discovering the death of their leader, the remainder of the intruders were
  convinced that divine retribution had been the cause of his death and left the
  farmers be.

## Chapter 27: The Cafe Antalia

- Lyra decides that before she makes her way to Aleppo, she will go to the Cafe
  mentioned as a future meeting place in Dr Strauss' journal.
- Before doing this, she is overcome by a feeling of warmth and comfort and
  believe something has happened to Pan.
- Pan has come across a girl whom he initially mistook for Lyra.
- This girl is a survivor of the boat crash Lyra was involved in and lost her
  daemon in that crash.
- The two agree to pretend to be to belong to each other for both their safety.
- In the café, Lyra is surprised when she is approached by a man called
  Schlesinger who correctly identifies himself as being part of Oakley Street.
- He tells her she is in great danger and that the new High Council of the
  Magisterium has a warrant out for her arrest.
- He takes her to his wife who helps disguise her by cutting and dying her hair
  and giving her advice on how to hold herself.
- Schlesinger has a letter from Malcolm which Lyra opens to find it full of
  unclear warnings – deliberately so - to avoid giving away details to other
  people who may have opened a letter before her.
- Both Schlesinger and his wife give constant warning to Lyra but she still
  leaves there feeling somewhat content.
- This feeling is rapidly taken from her when she see smoke coming from the
  Schlesinger's building just shortly after she had left it.

## Chapter 28: The Mirriorama

- Lyra is on the train, trying to remain inconspicuous.
- Lyra muses about the number of refugees on the move and wonders how this
  cannot be common knowledge in England.
- Her attempt to remain hidden fails again but the man who notices her is
  sympathetic towards her, mostly because he believes she is a witch.
- He says he has had a relationship with a witch in the past.
- He has a pack of playing cards, the name of which is the title of the chapter.
- He shows Lyra various pictures and tells a story using the cards and she
  appears to understand exactly what he is telling her despite the apparently
  non-specific nature of the pictures on the card.
- She falls asleep.
- The first thing she does when she wakes up is look for her alethiometer,
  assuming it has been stolen.
- When she finds it in her bag, she thinks about using it to look from Malcolm
  and then realises that it was really she is looking for a Pan and chastises
  herself for forgetting this.
- The kind man has gone but has left her his Mirriorama.
- Lyra shuffles and accidentally drops the cards, but again instinctively sees
  patterns when she picks them up.
- In particular, she sees a woman in a shawl carrying a basket and knows this is
  her and a man walking towards her – Malcolm.
- This takes place in a farm with an arch of roses and she also sees a camel
  train.
- All the actions Lyra has performed with the cards seem to be instinctive, as
  if her hands were moving by themselves.
- Lyra's thoughts then go to a negative place where she mistakes this automation
  for a lack of her own existence or meaning and devolves into a depression
  about it not really mattering whether she is alive or dead.
- The man also left her a note on the pack of cards telling her to be careful in
  Salukia as he feels it will be a dangerous place for her.
- He says she should try not to even cast a shadow.
- At the next station the train calls into, Lyra notices there are armed guards
  everywhere.

## Chapter 29: News from ?Tashbilak?

- Glenys Godwin is summoned to a senior minister's office and immediately knows
  that Oakley Street is to be shut down.
- Before she leaves, she enacts a long running plan which will ensure the safety
  of the information Oakley Street holds; this will be carried out before she
  even enters the other man's office.
- At the meeting, she tries to bring doubt into the mind of the minister
  shutting her down by saying that the act of closing down her department, which
  has most closely monitored the Magisterium in Geneva, at the same time as the
  new High Council leader is visiting England will make it look like it was the
  High Council that shut down Oakley Street, not the British government.
- Immediately after the meeting, she meets another government contact and has
  some of her suspicions about the Magisterium confirmed – they are summoning an
  army to invade and control the rose oil.
- She says that whether sanctioned or not, Oakley Street will continue.
- She puts her hope in Malcolm.
- Malcolm visit Schlesinger, who survived the fire bombing on his apartment.
- He has been looking after a survivor of one of the attacks on the research
  station and Malcolm goes to speak to this man.
- The man tries to describe the branding of the pharmaceutical vans which seem
  to support the attackers of the research station.
- Suddenly, Malcolm notices that Schlesinger is lying on the floor unconscious
  and realises the nurse has poisoned him.
- She chases her and she shoots him in the hip before killing herself.
- Malcolm returns to the man, who says the nurse has been poisoning him to get
  information from him.
- The man dies as Schlesinger regains consciousness, taking Malcolm to get some
  help.
- Malcolm is patched up and continues his conversation with Schlesinger,
  learning about Lyra's plans.
- In the discussion, Schlesinger's wife realises Malcolm is in love with Lyra.
- Schlesinger helps Malcolm decipher the now dead man's description of the
  pharmaceutical company, "TP".
- He says it was originally a big petroleum-chemical company but moved into
  pharmaceuticals and since then has had to be more public.
- They are a ferociously research driven company.
- Malcolm realises that the painkillers he has been given for his hip are made
  by this company.
- Their interest in rose oil is clearly to further their commercial gains.
- He gets on a train to Aleppo and debates with Asta about the poem they have
  been reading.
- Looking deeply into it, the hero could be Malcolm and heroine, Lyra.
- The heroine loses something, goes on a quest to reclaim it but can only do so
  great sacrifice; This could be Lyra's search for Pan.
- At one point, the heroine is captured by a dark princess and freed by the hero
  after playing a trick; this could be the encounter with the water fairy during
  the events of la Belle Sauvage.
- Malcolm is sceptical; Asta says that there is no way to know the truth but she
  believes they should accept the connections hidden within the stories.
- The train makes an unexpected stop where a very important person appears to be
  boarding.
- Malcolm is struggling with his pain and passes out, with a pool of blood
  collecting beneath his leg.

## Chapter 30: Norman and Barry

- Alice is summoned to meet with the master of Jordan.
- There she finds two men, introducing themselves as a sergeant and captain in
  the regular army but denying they belong to the CCD when directly questioned
  by Alice.
- They ask her about Lyra and she truthfully tells them she does not know where
  she intended to go or where she is but no more than that.
- When the men threaten her that she might be arrested, she bursts out of the
  office to try to explain to the onlooking bursar and his secretary what has
  happened and have them as witnesses.
- The men arrest her.
- The secretary was tipped off to go to Malcolm's parents by Alice mentioning
  Norman and Barry, the name of the peacocks at their pub.
- Malcolm's mother immediately thinks they should go to see Hannah Relph.
- When they get to Hannah's apartment, they find the same men already there,
  emptying boxes from the apartment and holding Hannah hostage.
- The sergeant seems to be about to again deny that they are not in the CCD when
  the captain cut him off.
- Remarkably, Malcolm's mother manages to fight them off, mostly with verbs;
  rather than physical assault.
- They threaten her that they will return.
- The secretary goes to work the next day to find herself being fired by the
  bursar.
- He has a rather pathetic air and denies responsibility for the firing.

## Chapter 31: Little Stick

- The soldiers board Lyra's drain and several fill her compartment.
- It is not long until her lack of a daemon is noticed and the soldiers react
  with panic, fear and anger.
- Lyra correctly guesses none of them will understand French and starts to
  recite French poetry meekly, holding up her hands in submission.
- The juxtaposition of this small helpless girl and the large group of soldiers
  highlights the absurdity of their fears and they all begin to laugh.
- An officer briefly enters the cabin, correctly identifies the language Lyra is
  speaking as French, ask her about her daemon but in friendly terms and leaves.
- Things are going well until the soldiers start to become drunk.
- Suddenly, Lyra is aware they are about to attack her and she lashes out with
  her fighting stick, teeth, nails, and anything else she can.
- She severely injures a couple of them but is overpowered and only saved when
  the officer returns.
- She spends the rest of the journey in his compartment, with him giving her
  some rose oil to wash with and saying it is not difficult for him to come by
  it as a senior officer.
- He escorts her off the train in Salukia, and she is left not knowing what
  she's going to do next.

## Chapter 32: Hospitality

- In Salukia, Lyra is briefly looked after by a couple without daemons, who wrap
  her in a headscarf.
- Lyra finds that this completes her transition into a ghost – walking unnoticed
  by almost everyone.
- She happens upon a priest who reveals to her many secrets about daemons and
  the blue hotel.
- He says that this part of the world is quite advanced medically despite its
  poverty elsewhere.
- People can sell their daemons and be separated from them – a process that not
  everyone survives.
- In return, people without daemons can approach a so-called dealers to get a
  new daemon attached to them.
- The priest is very scared and worried by Lyra's mention of the blue hotel.
- He says it is an evil place and something or someone there draws daemons
  towards it.
- He has had one parishioner go there and return, changed for the worse.
- Lyra persists and the priest tells her where she can find a dealer who might
  be able to help her find the blue hotel.
- Lyra goes to the hotel and as predicted, dealers start to approach her door
  quickly.
- The most interesting thing she learns from the first dealer is that he sold a
  daemon to Professor Branda.
- However, the first three dealers wish no more to do with her when she says she
  wants to go to the blue hotel, rather than buy a daemon from them.
- Lyra goes out to get something to eat and when she comes back a fourth dealer
  has arrived and she haggles with him to be her guide to the blue hotel.
- He says he has been there twice and his daemon says she had hoped they would
  never return.

## Chapter 33: The Dead Town

- True to his word, the dealer takes her to the blue hotel.
- They arrive at night but Lyra says she wants to go in straightaway.
- Olivier Bonneville is overlooking the ruined city, rifle in hand.
- As he prepares to shoot Lyra, he notices a man sitting beside him – Lyra's
  dealer.
- He was so preoccupied with Lyra he did not see the man sneaking up on him.
- The tells Bonneville that Lyra is the only person you can retrieve a treasure
  situated 3000 miles away.
- He says he should do no harm to her until this treasure is retrieved.
- Bonneville is not sure what to do - he does not care about this man or the
  treasure - but in the time they have been talking, he has lost sight of Lyra.
- Lyra has turned a corner in the city to encounter a young woman who knows Lyra
  by name and beckons Lyra to follow, saying, "We have been waiting for you."
- Lyra hopes the 'we' she means Pan and her.

[Home](/blog "ohthreefive")