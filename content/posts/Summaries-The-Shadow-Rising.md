+++
         
date = "2020-04-28T20:27:32.840Z"
title = "Summaries: The Shadow Rising"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Shadow Rising

Robert Jordan

# Part One

## Chapter 1: Seeds of Shadow

> *"The Shadow shall rise across the world, and darken every land, even to the*
> *smallest corner, and there shall be neither Light nor safety. And he who*
> *shall be born of the Dawn, born of the Maiden, according to Prophecy, he*
> *shall stretch forth his hands to catch the Shadow, and the world shall*
> *scream in the pain of salvation. All Glory be to the Creator, and to the*
> *Light, and to he who shall be born again. May the Light save us from him."*
> — from Commentaries on the Karaethon Cycle
> Sereine dar Shamelle Motara, Counsel-Sister to Comaelle, High Queen of
> Jaramide
> (circa 325 AB, the Third Age)

- Min has returned to Tar Valon on Moiraine’s mission to let the Amyrlin know
  about Rand's intention to go to Tear.
- Before she reaches the Amyrlin Seat, she notices that every Warder and
  Aes Sedai she sees looks dead or wounded and she surmises she is seeing a
  coming battle.
- In one of the visions an Aes Sedai is wearing a silver collar and she is
  terrified that it is the Seanchean who will attack.
- She tells the Amyrlin Seat this as well as Moiraine’s message and much to her
  surprise, find herself being recruited by the Amyrlin Seat as her new
  Black Ajah hunter.
- Elaida has been trying to listen in.
- She does not trust the Amyrlin Seat and think she may be the only one who
  remembers how close she was to Moiraine before they were both Aes Sedai.
- Elaida has a mild version of the ability to foretell and has seen two things.
- First is that the lineage of Andor will be essential in the fight against the
  Dark One.
- This has been the motivation for her life's work.
- Her second foretelling has been about Rand and all the chaos that will follow
  him.
- For this reason, she means to stop him.
- While thinking about her plans, and she theorises that perhaps Rand can
  channel but that not even the Amyrlin Seat would be so bold as to hide a
  man who could channel.
- She recruits an ally, confiding her theories in this other Aes Sedai,
  Alviarin.
- She wonders whether she will have to deal with the Amyrlin Seat as well as
  Rand.
- Dain Bornhold leads a half-legion of Childern of the Light under the command
  of Ordeith.
- Dain is unhappy about Ordeith, a man he does not know, but as he values
  revenge on Perrin and their mission takes them close to the Two Rivers, he
  readily accepted it.
- Ordeith says he plans to make Rand come to him by punishing those he
  loves.
- A High Lady of the Seanchean, Suroth, has managed to establish control of a
  small area in the wake of the defeat in Falme.
- She has sent some information back to the Empress and is seeking more
  information, this time about Rand and the control the Aes Sedai may or
  may not have over him; she asks a captive.
- She talks about a secret that only she and three others know and that she is
  keen to keep it safe.
- She means to capture and control Rand and his power.

## Chapter 2: Whirlpools in the Pattern

- Perrin is having an argument with Zarine/Faile.
- She wants to leave; he wants to stay to support Rand.
- He accepts Rand's position as the Dragon Reborn but still thinks of him as
  Rand his friend.
- Out of nowhere, his axe attacks and nearly kills him.
- Once he has nullified the threat by burying the axe in a thick wood door, he
  and Zarine conclude only Rand could have caused this to happen.
- Mat is playing cards with High Lords of Tear who are already manoeuvring for
  favour with the Lord Dragon and his friend.
- Some of the figures in the cards take form and attack Mat.
- No one else admits what they saw; Mat thinks Rand must be responsible.
- Someone says they heard Mat speak in the Old Tongue.
- Rand is propositioned in his room by Berelain.
- He is attacked by multiple copies of his own reflection.
- He is severely wounded but manages to defeat them.
- He assumes it must be an attack by one of the Forsaken - the act seems beneath
  Ba'alzamon.
- He clutches Callandor, keen to feel invincible for a time before going to
  Moiraine for healing.
- On the occasion of each attack, a cock has crowed.

## Chapter 3: Reflections

- Rand is visited by Perrin, Moiraine and numerous others, and she takes the
  time to perform a healing.
- The wound in his side cannot be fully healed and Rand tells Moiraine he
  believes this is the wound from which he will bleed and die in his battle with
  the Dark One, according to a prophecy.
- Moiraine chastises him for reading too much and for drawing too firm
  conclusions from prophecy.
- Rand re-asserts that he will not be chased again; Moiraine explains her
  beliefs that this was neither the Dark One nor the Forsaken.
- She thinks that as the seals on Shayol Ghul weakening, evil is leaching out
  and that it is naturally attracted to _ta'veren_.
- She says that she can try to help, though is unable to teach, Rand to control
  his power if he will let her.
- Perrin speaks to the Aiel commander Rhuarc when everyone has gone.
- Rhuarc says that the Aiel do not have a prophecy of the Dragon, and that Rand
  has merely fulfilled one prophecy meaning he might be He Who Comes With the
  Dawn.
- Perrin worries what this will mean if Rand does not fulfil all of their
  prophecies.
- He believes he must convince Faile to leave and to leave without him.

## Chapter 4: Strings

- Thom is in his room writing forged letters in what seems to be an attempt to
  sew some discord within the High Lords of Tear not absolutely loyal to Rand.
- He sees his role as helping Rand not fall victim to their politicking.
- Mat comes to visit him, determined to leave after the events of the evening.
- Thom says he should at least stay the night before leaving in daylight.
- Thom notices how easily Mat can be persuaded not to leave.

## Chapter 5: Questioners

- Nynaeve and Egwene are questioning the captured Black Ajah, one of whom has
  been stilled.
- This one tells them and overheard story about a weapon or something dangerous
  in a place called Tanchico which will be used to control Rand, though at some
  risk to the user.
- Another tells a much more fully fleshed out story, that Liandrin means to free
  one of the False Dragons.
- This man can channel very strongly and she means to have him reach destruction
  on the world under the name Rand.
- This will cause the world to unite against the Dragon Reborn.
- She tells them that Moiraine is not as clever as she thinks.
- Moiraine subsequently burst into the room with an angry look on her face.

## Chapter 6: Doorways

- Moiraine’s anger is directed towards Rand's stubbornness and in fact she has
  missed the fact she has stumbled into an interrogation.
- She manages to re-compose herself.
- There is a stand-off and eventually the three women demand some answers of
  Moiraine.
- She tells them that as the Forsaken Sammael is in charge of Illian, she wishes
  Rand and Tear to attack and defeat Illian.
- Not only will that rally the city of Illian under their banner but it should
  also convince other nations to unite under the Dragon Banner.
- Moiraine is convinced that more Forsaken are free and probably in other
  nations waiting to strike at Rand; she wants him to make the first move.
- She explains to the woman that Rand is reading the Prophecies of the Dragon
  for himself and that she does not think this is a good idea.
- Egwene and Nynaeve are furious with Moiraine for planning to use Rand to start
  a war.
- However, Elayne explains that in their current situation, war is inevitable.
- Moiraine tells them about a _ter’angreal_ within the collection of Tear: a
  doorway through which three questions will be answered about past, present and
  future.
- Unfortunately, a person can only use this _ter’angreal_ once and it is not
  without risk.
- In particular, any question which concerns the shadow carries a risk to the
  well-being of the questioner.
- Moiraine wants this _ter’angreal_ for her plans.
- She advises the women to go to Tanchico - she has already sent word to the
  Amyrlin to warn of the plot to free the False Dragon.
- Afterwards, the women talk and Egwene realises she is no longer in love with
  Rand and Elayne realises that she is.
- Nynaeve asked them what they mean to do about this.

## Chapter 7: Playing with Fire

- Egwene and Elayne visit Rand with two aims.
- Before they can speak, he asks for what they might do for him, one example
  being helping him translate the Old Tongue.
- Elayne decides not to tell Rand that she could help with this.
- Firstly, they want to help him control _saidin_.
- He initially resists but Egwene manages to prove to him he can sense when they
  embrace _saidar_.
- During her embrace, she feels his wound and the evil within, and cannot
  believe he can stand it.
- She asks him to embrace _saidin_ and is disappointed to feel nothing.
- She tells him to trust her as she thinks she now has more strength than
  Moiraine, though much less experience.
- She asks for a bigger example of his power, confident she can cut him off if
  need be, but is overwhelmed by him, both her and Elayne being cut off and Rand
  weaving so many threads that she can hardly believe it.
- They chose another tack: talking about how they use the Power.
- At the discussion of fire, Egwene concedes that using _saidin_ and _saidar_ is
  fundamentally different, but she will not give up trying to help.
- Their second aim was to tell Rand that Egwene no longer wishes to marry him as
  she no longer loves him like that.
- Rand is relieved: he tells Egwene his feelings had also changed in that way.
- Egwene leaves, impressed that Rand managed to make up the lie so easily and
  quickly.
- She leaves him with Elayne.

## Chapter 8: Hard Heads

- Elayne and Rand speak to each other, talking of their fondness for each other
  but not being entirely clear, at least in Rand's mind.
- He subsequently has a meeting with the High Lords of Tear but is somewhat
  distracted.
- Egwene and Mat speak, with Egwene telling him he must not try to leave.
- He wants her help with what he should do and why he has such gaps in his
  memory.
- She tells him about the _ter'angreal_ but advises that he does not use it or
  is at least careful.

## Chapter 9: Decisions

- Mat remains within the city, uncertain of his next move.
- He has a hard time finding a moment to speak with Thom and, despite wishing he
  didn't, he seems to run into Moiraine often.
- Perrin is learning about the strife happening elsewhere in the world and
  struggling to think of a safe place to send Faile.
- Elayne is using her last few days in Tear to continue what she thinks are vain
  interrogations of the Black Ajah and catching brief moments to spend with
  Rand.
- Rand is also aware that soon the Black Ajah will be sent to Tar Valon and
  Elayne, Egwene and Nynaeve will also leave.
- He cannot think of a good excuse to keep her here.
- His meetings with the High Lords are frustrating him as they sometimes
  wilfully ignore him.
- This raises him to anger and he uses the One Power in a meeting with him,
  terrifying them into obedience.
- With the High Lord's dismissed, Rand suddenly finds himself in the presence
  of Selene.
- For the first time, she reveals herself to him to be Lanfear.
- Again, she repeats that he is hers.
- She explains that some of the other Forsaken fear that he will be given a
  higher place at the Dark One's side and that they will try to kill him to stop
  this from happening.
- She does mention one, Asmodean, who could teach him how to use _saidin_.
- She tries to tempt Rand by saying that if they were to use the two
  _sa’angreal_ in the world more powerful than the sword Callandor, they could
  kill all of the Forsaken, the Dark Lord and maybe even the Creator.
- Only once during their exchange does Rand speak as if he is the Dragon,
  instinctively chastising her for only ever wanting power; both are surprised.
- In the end, she decides that he is not ready and that she would rather he came
  to her and so she will leave him.
- Out of nowhere, a Grey Man attacks and Lanfear releases the block on Rand,
  allowing him to defend himself.
- She says such an attack is beneath her and warns him not to go outside.
- When he opens the door, he sees a scene of madness.

## Chapter 10: The Stone Stands

- Outside his chambers, there is a battle between Trollocs, Myrddraals, the
  Aiel and defenders of the stone.
- Rand forms his sword of the power and joins the battle, echoing the cry he
  has heard before before: "The Stone stands!"
- In this manner, he fights his way and leads allies through the Stone, battling
  all Shadowspawn he encounters.
- As is their way, the Trollocs do not spare women and children.
- He even sees Trollocs and Fades attacking and killing each other.
- He encounters Lanfear again who chastises him for using a sword when he
  could use more and then reminds him that this attack probably originates from
  another Forsaken and that Forsaken may be on his way to steal the sword
  Callandor now.
- Rand battles his way back to the Callandor, and when he reaches it
  unleashes power beyond comprehension, with lightning striking all Shadowspawn
  throughout the Stone and killing them.
- Moiraine approaches him shortly afterwards but stays clear of him, seemingly
  afraid.
- Rand feels he has failed by acting too late, both in not attacking the
  Forsaken days ago but also by fighting with a sword for so long while his
  allies died.
- He tells Moiraine that he will tell them all his next plan tomorrow; he thinks
  Lanfear may have accidentally divulged some useful information while talking
  to him.

## Chapter 11: What Lies Hidden

- Egwene decides to enter _Tel'aran'rhiod_ in order to investigate Tanchico.
- She decides to do this in the first instance without using the _ter’angreal_,
  partly as a test of her power.
- While walking around the artefact collection in the palace, the most
  interesting things she encounters is a statue of a woman holding a sphere.
- She feels drawn towards it but when she picks it up feels stabbing pains and
  flows of the Power between her and it.
- She thinks it is probably another _ter’angreal_.
- During her exploration, she is distracted by thoughts of the Aiel Waste and
  journeys there, seeing an Aiel woman hunting what looks like some sort of
  boar.
- In the Palace of Tanchico, she saw the skeleton of something that was also not
  quite a boar.
- Rand has also been wondering about what a dragon is – as well as being marked
  twice with a heron he is meant to be marked twice with a dragon.
- Much to Egwene's surprise, the Aiel woman appears to her in Tanchico but
  with a different body as well as different clothes.
- She is one of the Aiel wise woman and knows how to navigate _Tel'aran'rhiod_.
- She is troubled by Egwene telling her that the Black Ajah are real and says
  she should come to see her in the Waste and learn what she can from her.
- The Aiel wise woman, called Amys, tells Egwene that there is an evil in
  Tanchico worse than man could make.
- No Aes Sedai can teach her as much about _Tel'aran'rhiod_.

## Chapter 12: Tanchico or the Tower

- Egwene tells Nynaeve, Elayne and their Aiel guardian what happened.
- She determines to go to see Amys in the Waste.
- To their surprise, the Aiel woman says she would rather accompany Elayne
  and Nynaeve to Tanchico.
- Egwene is entirely understanding – any other Aiel can guide her.
- Moiraine arrives, bearing a letter written a month ago but addressed to her in
  the Stone.
- She also has news: the two captive Black Ajah are dead.
- The letter is from Amys and asks that their Aiel guardian returns to her.
- Moiraine manages to convince them all that this is the best course of action.
- Egwene and the Aiel will go to the Waste; Nynaeve and Elayne to Tanchico.

## Chapter 13: Rumours

- Mat is thinking about the events of the evening.
- He managed to kill a Grey Man assassin before being confronted by a Myrddraal.
- Preparing for a last stand, a group of Trollocs appeared out of nowhere,
  killed the Fade and moved on, leaving him alone.
- He is very confused by this.
- He overhears a group of men talking about the Two Rivers and goes to them,
  listening to rumours.
- Apparently Whitecloaks have laid siege to the region, looking for the False
  Dragon and a yellow-eyed man.
- When directly questioned, Mat hears that they are not looking for anyone
  else.
- He goes to see Perrin, who is already packing as he has also heard the rumour.
- Mat wants to go with him but cannot even say these words out loud.
- Perrin thinks that because he is _ta'veren_ but does not say this aloud.
- Mat considers himself the biggest fool in the world.

## Chapter 14: Customs of Mayene

- Berelain enters Perrin's chambers and propositions him, suggesting that he
  come back to Mayene with her and design ironwork for her bedroom.
- Faile enters and Perrin is surprised that she is relatively calm.
- She has also heard the rumours and sees Perrin's packed bags as a sign that
  they are going to the Two Rivers.
- Perrin however realises that the Whitecloaks know him and unless he gives
  himself up for killing some of them, they will never stop.
- He means to hand himself in, knowing that means he will be hanged.
- In order to spare Faile seeing this, he tells her he is interested in
  Berelain.
- Faile leaves, catches up to Berelain and confronts her, but is surprised
  to be thrown from her feet by the noblewoman.
- The two have a confrontation interrupted by the Aiel general, who easily
  separates them.
- Faile concludes that she will have Perrin for her own.

## Chapter 15: Into the Doorway

- Mat has decided to enter the _ter’angreal_ doorway.
- Inside, he is greeted by an odd being who guides him to three similar beings.
- Despite believing he had carefully thought out his questions, they answer his
  first by telling him he has to go to the same place in the Aiel Waste to where
  Egwene is to go.
- They say his life depends on it and he will marry what sounds like a noble of
  the Aiel there.
- Mat is extremely frustrated by the answers and demands more but is fought
  away by previously unseen attackers.
- Shortly after he leaves the _ter’angreal_, Rand steps out.
- Shortly after that, Moiraine steps out.
- She immediately rounds on them, asking where they heard the existence of the
  doorway.
- Rand says he read it in a book; she challenges him about what went on inside
  but he does not answer.
- He says it was difficult to comprehend anyway as we were speaking the Old
  Tongue through a translator – something Mat had not even realised.
- He uses this excuse not to tell them what he encountered in the doorway.
- The two boys casually assume Moiraine will not tell them what she asked and
  was told and she confirms this.

## Chapter 16: Leavetakings

- Perrin is able to capitalise on Loial's fear that his mother will
  find out he is in the Stone of Tear and summon him back home by asking him to
  help him navigate the Ways in order to get to Emond's Field and confront the
  Whitecloaks.
- The plan appears to have worked until Faile interjects that Loial promised to
  show her the Ways and Ogier are known for keeping their oaths above all else.
- Loial is uncomfortable to be caught between them and they refuse to back down
  to each other until finally Loail agrees to show Faile through the Ways
  and Perrin says he will just follow their path.
- Perrin leaves in a bad mood but encounters one of the Aiel outside.
- Rand already knows about Perrin's plan and sent this Aiel man to him.
- He says more Aiel may follow as they have been stuck inside the Stone for
  so long that they are becoming restless.
- Perrin wonders is if his plan surrender to the Whitecloaks might not be
  required now.
- The women are also making their plans to leave.
- Elayne has sent a letter to Rand which she hopes will suffice to keep him
  thinking of her.
- Lan bursts in and chastises Nynaeve for leading him to believe they were going
  to the safety of Tar Valon when really they are going to Tanchico.
- The two argue but when Nynaeve produces the written instructions from the
  Amyrlin Seat, Lan backs down.

## Chapter 17: Deceptions

- Thom continues in his mission to destabilise the High Lords of Tear who oppose
  Rand and to rally the common people of Tear to him.
- Exhausted, he is relaxing in his room after this when Moiraine appears.
- She makes it fairly plain that she knows exactly what he has done and offers
  him chance to go to Tanchico with Nynaeve and Elayne, most importantly to look
  after Elayne.
- Thom reluctantly accepts this fate, especially as Moiraine says she will give
  him the names of the Red Ajah involved in his nephew's death.
- Min speaks to Gawyn and Galad.
- They are keen to know where Elayne and Egwene are and want to know if Min can
  tell them anything.
- When she has Gawyn alone, she chastises him for this as her cover in the White
  Tower does not include her knowing these women.
- Gawyn mentions with trepidation that the Whitecloaks appear to be courting
  Galad.
- Min sees the False Dragon Logain with a splendid halo above his head.
- She goes to tell the Amyrlin Seat and to ask if she can go to Tear to help
  Rand.
- Her request is denied.
- Two important letters arrive.
- Firstly, the False Dragon Mazrim Taim has escaped and it is the Amyrlin's
  decree that should he be recaptured, he should be gentled on the spot.
- Secondly, a cryptic letter from Moiraine confirms that Rand has taken
  Callandor, the Stone of Tear and announced himself the Dragon Reborn.
- The Amyrlin Seat is happy about this at least as she can now openly say
  that the White Tower should aid and guide Rand.
- A Black Ajah approaches the Aes Sedai novice who first showed Min to the
  Amyrlin Seat.
- She tortures her for information about who she spoke to and what she said but
  the girl can only remember Min speaking to Gawyn.
- The Black Ajah kills her.

## Chapter 18: Into the Ways

- The group enter through the Waygate.
- Only Gaul, the Aiel man sent by Rand to assist Perrin, has joined him.
- However, some Aiel women have chosen to assist Faile.
- Perrin and Faile are still not talking to each other.
- He is determined to lead the party for as long as possible.
- As they leave, there is an earthquake, which Perrin assumes is caused by Rand
  and possibly due to the two _ta'veren_ separating.

## Chapter 19: The Wavedancer

- Nynaeve and Elayne are finding passage to Tanchico with the Seafolk.
- The captain of the ship is not particularly receptive towards them until they
  have a frank discussion below deck.
- The Seafolk also have prophecies someone similar to the Dragon reborn and
  known as Coramoor, spoken about in the Jendai Prophecies.
- When they hear that Nynaeve and Elayne are hunting Black Ajah they agree to
  their passage.
- Their ship, the Wavedancer, survived an attack by the Seanchean the previous
  year.
- Before they leave, they feel the same earthquake.

## Chapter 20: Winds Rising

- Elayne and Nynaeve discover two new companions: both Juilin and Thom are
  waiting to board the vessel.
- Thom has been sent by Moira; Lan sent Juilin and told him to keep Nynaeve
  safe.
- Elayne is struck by a familiarity with Thom and goes to speak to him but
  cannot quite figure out how she knows him.
- Elayne discovers that the Windfinder aboard the Seafolk ship can channel and
  uses this to speed their vessel.
- The women are scared that the White Tower will interfere and keep their power
  secret.
- Elayne asks if the woman will teach her and the woman said they should teach
  each other.

## Chapter 21: Into the Stone

- Rand has gathered everyone in the Heart of the Stone to announce his plans.
- Before he arrives, Moiraine is thinking over all that is occurring and in her
  thoughts she reveals she already suspects that the Windfinders of the Seafolk
  can channel.
- She wanted rid of Thom as she wanted to be Rand's only guide at this time.
- Rand arrives and tells the assembled High Lords and Ladies that they will not
  march on Illian but on Cairnhein.
- Amid the battle cries, he says they do not go there to conquer but to feed the
  peasants and restore peace to the region which is in the throes of Civil War.
- He chooses all the nobles who have been plotting against him to lead this
  mission.
- Egwene is happy that he is not starting a war; Moiraine is unsure if this is a
  good plan.
- Next, he thrusts Callandor into the stone floor of the Heart, saying
  it will stay there until he returns, also announcing his plans to leave.
- This act causes the earthquake.
- Moiraine and Egwene catch up to him and Moiraine confronts him for leaving
  this powerful artefact open to the Forsaken.
- He says he has laid a trap around it and only he can draw it.
- Much to their surprise, he announces that he is going to the Aiel Waste,
  to the very place that Egwene needs to go and that Mat was told to go.
- Moiraine wonders whether these decisions Rand has made are based on his
  answers when he went through the _ter'angreal_ doorway.

## Chapter 22: Out of the Stone

- Shortly after the group leave, Moiraine asks Rand to reveal his next secret.
- He means to have them travel to the Waste using a portal stone.
- She and Egwene think this is a bad idea, having heard from Vera what happened
  the last time he tried.
- When they find the portal stone, Rand also reveals that he found an _angreal_
  which works for Male channellers.
- He means to use this to augment his power so that he can transport the
  sizeable group.

## Chapter 23: Beyond the Stone

- Rand has successfully transported the party using the portal stone, though
  even with the help of the _angreal_, it has taken a significant physical toll
  on him.
- Moiraine heals him but Rand rebukes her for it, saying in future she should
  ask before she does it.
- They are met by a group of Aiel, including Amys.
- The city they have sought, Rhuidean, is a sacred place and Rand asks leave to
  enter.
- The Wise Ones, Aiel women apparently similar to a Wisdom like Nynaeve, are the
  ones who decide and despite protests from a particular Aiel man, they grant
  Rand passage on account of his Aiel father.
- This parentage is news to Egwene, but also to Rand who had assumed it was his
  mother who was Aiel.
- Mat suddenly jumps up and joins Rand in asking to into the city; this is
  only granted when Rand demands that he join him.
- Rand is told to enter the heart of Rhuidean.
- This angers that Couladin so much that he nearly attacks Mat but is thrown
  aside by the Wise One, revealing that they can channel.
- Next, Egwene's Aiel companion, who is called Aviendha, is told to enter the
  city and face a trial by three rings.
- Egwene recognises these as the same as the _ter’angreal_ an Aes Sedai
  novice must pass through to become accepted.
- It seems Aviendha is to become a Wise One.
- There is some discussion between the Wise Ones, Moiraine and Egwene, taking
  into account Aiel politics and customs, during which one of the Wise Ones
  lets slip that they think Moiraine has to go into the city and face the rings
  as well.
- Moiraine accept this fate immediately and leaves quickly.
- Egwene has been desperate to learn from Amys about _Tel'aran'rhiod_ and her
  lessons begin.
- She learns that it is not connected to the One Power as Aes Sedai believe
  and she is told that she should not try to re-enter it now until given
  permission.
- In the past, some people have been able to enter it with their physical bodies
  but this almost always was done for evil purposes and lead to their madness or
  death.

## Chapter 24: Rhuidean

- Mat and Rand enter Rhuidean and Mat spots Aviendha running ahead of them: she
  has already overtaken them.
- Near parched, Rand channels water from deep underground to fill a basin.
- The deserted city is impressive.
- The enter a courtyard, in the centre of which is what Rand believes to be
  Avendesora, the tree of life, and beneath which are two foot high statues of a
  man and woman each holding a crystal sphere.
- There are _ter'angreal_ littered around the courtyard, including the rings.
- Rand tells Mat to stay behind; to go in means returning an Aiel chief, mad or
  dead and Rand thinks Mat cannot be an Aiel chief.
- Mat's impatience leads him to wander and stumble across a similar twisted
  doorway _ter'angreal_ which he enters, still angry at the answers he
  previously received.
- Again he is asked if he bears a source of light, music or anything made of
  iron.
- He is lead to a room where eight figures refuse to answer his question of
  which Aiel he is to marry and why.
- The silence continues as he asks about the two other answers he previously
  received, namely that he will die and be reborn and that he will take half the
  light out of the world.
- Furious, he shouts that he wishes he did not have blanks in his memory, was
  free of Aes Sedai and the Power and was back in Rhuidean.
- He hears, "Done," three times, implying this doorway grants three wishes, and
  finds himself in darkness, unable to breathe.

## Chapter 25: The Road to the Spear

- Rand finds himself a step behind the Aiel man who entered Rhuidean before him.
- He enters the _ter’angreal_ and finds himself going back in time through the
  history of the Aiel.
- Firstly, an Aiel is told by his wife to do what the Jenn Aiel ask of him, and
  he agrees to go unarmed to Rhuidean.
- Secondly, another Aiel tells a companion they must go across the Spine of the
  World.
- He knows they guard the Jenn Aiel and that they are secretly called the People
  of the Dragon, but he is not sure why.
- Thirdly, a young Aiel is approached by some Jenn Aiel, who far outnumber them,
  to ask for help in retrieving stolen goods.
- The Jenn are offered spears, which will leave them exiled, and one of them, a
  woman, becomes the first maiden of the spear, and modifies it to the standard
  Aiel short spear.
- Fourthly, an Aiel party sneak up on some captured Aiel but are not skilled and
  wake the guards.
- Though they win the scuffle, one of their number is killed and on their
  return, they are outcast from the Da'shain Aiel for the killing.

## Chapter 26: The Dedicated

- Another Aiel is mourning the capture and murder of his relatives; he follows
  the Way of the Leaf.
- One of his group wants to leave behind the Aes Sedai artefacts they are meant
  to be guarding in search of a safe place and a song.
- It sounds like the Tuathua'an are these "Lost" Aiel.
- Next, a dwindling group of Aiel hear news of an Ogier group beginning to
  suffer from the Longing and that Ishamael was only partly trapped.
- As the group's leader dies, he tells his son to go south.
- Next, that same leader enters the Hall of Servants in which female Aes Sedai
  are having a discussion.
- He speaks to a Nym, one of which I think the Green Man was, before an Aes
  Sedai gives him instructions and says she has a plan for the Nym.
- There are wagons filled with _angeal_, _ter'angreal_ and _sa'angreal_ for them
  to take safely away from the male Aes Sedai.
- The task for the Nym was a seed singing ceremony, at which an Aiel is
  reflecting that his grandfather claims to have served Lanfear before she was
  Forsaken.
- The ceremony is protected by soldiers, described as wearing helmets almost
  like the Seanchean.
- There is news that Lews Therin has attacked Shayol Ghul.
- The Aiel is attacked and his grandfather hung.
- This grandfather is a young man, excitedly waiting to find out the result of
  an Aes Sedai experiment to allow men and women to channel using the same
  source.
- The experiment leads to an earthquake and something called the Sharom falls to
  the ground.
- Rand reflects that he saw the hole being drilled in to the Dark One's prison.
- Rand finally leaves the _ter’angreal_ and the first thing he notices is a
  figure hanging from the tree of life: it is Mat.
- He is just-in-time to save him.
- Mat tells him of the other twisted doorway _ter’angreal_ and Rand wonders
  whether it could be useful as he could do with more answers.
- Mat has a Foxhead medallion and the Ashandarei but does not realise that is
  what he asked for.
- A bubble of evil begins to attack the pair; Rand is able to destroy it by
  channeling.
- Rand and Mat escape and make their way up the hill as the sun begins to rise,
  making Rand think of 'He Who Comes With the Dawn.'
- These visions have been of Rand's ancestors, going back in time.

## Chapter 27: Within the Ways

- The group navigate the Ways.
- Just as Loial confirms they are at their destination, they are
  attacked first by some Trollocs, then a Fade, then the Machin Shin.
- They manage to escape them all.
- In the mountains above Emond's field, Perrin sees one hawk being brought down
  by an arrow and another by a group of ravens originating from where the arrow
  came.
- No one else has seen this and Perrin would rather confirm his fears before
  reporting to the group.
- He reaches out but cannot find any wolves.

## Chapter 28: The Tower of Ghenjai

- Perrin enters a wolf dream, but finds no wolves.
- He searches and comes across a man though he does not look human, who appears
  to have been skinning a wolf, so he chases him.
- The man enters a tower but Perrin cannot enter as he cannot see any entrance.
- Hopper finally appears and calls the man Slayer and saying the tower is evil.
- A woman suddenly appears (Lanfear?) and tells Perrin the tower's name and that
  is is a doorway to the realms of the Aelfinn and the Eelfinn.
- She tells Perrin she should not really be talking to him, he sees the shadow
  of someone else, then both are gone.
- Hopper did not see the woman or hear her speak.
- He tells Perrin the wolves have either fled or been killed by Slayer; he
  should not return home.

## Chapter 29: Homecoming

- Perrin enters Emond's field and goes to Egwene's parents' house.
- He learns there that Trollocs have been attacking the outer farms, including
  his family's.
- His entire family, from parents and siblings to cousins and other more distant
  relations, have been killed.
- The Whitecloaks have actually been fighting off the Trollocs, though in
  addition they have burned Mat and Rand's dads' farms, the latter burning led
  by Padan Fain.
- Their parents managed to escape.
- Perrin asks if there has been any resistance and is told that a hunter for the
  horn called Lord Luc has given up his hunt in order to help the people of
  Edmond's field but there has been no real resistance.
- Faile interrupts Perrin at this time, seeing his plan to give himself up
  will result only on his death and then no longer help his family.
- She tells him to let himself grieve and he breaks down in tears, blaming
  himself for not being able to get there sooner.

## Chapter 30: Beyond the Oak

- Perrin agrees that it is no longer in his interest to hand himself in to the
  Whitecloaks but thinks he should now try to meet up with Mat and Rand's
  fathers and help form a resistance, freeing prisoners and killing all
  Darkfriends.
- Egwene's mother says she will lead him to them but actually leads them to 2
  Warders.
- She tells them that there are Aes Sedai in the village but only the
  Women's Circle know.
- She is hopeful that the Women's Circle and the Aes Sedai will agree with
  her decision to let Perrin and his companions meet with them.

# Part Two

## Chapter 31: Assurances

- Perrin and the Aes Sedai question each other.
- One of them is Verin.
- The Aes Sedai say they are in Emond's field and the Two Rivers to
  recruit new novices – the rate of ability to channel is very high in this
  region.
- There are too many Trollocs for them to deal with them on their own and as the
  Whitecloaks are technically not Darkfriends, they are forbidden to use the one
  power against them.
- This is even though the Whitecloaks have killed one of the warders.
- Perrin is mistrustful as he recognises the name of the other Aes Sedai as
  one who Egwene said asked too many questions about him, Rand and Mat.
- Though they accept it will be difficult, the group plan their attack on the
  Whitecloaks to rescue prisoners.
- Perrin is fairly sure that the peddler is actually the one wolves knows as
  Slayer.
- Dain Bornhald is increasingly frustrated about the authority given to Ordeith.
- All of his patrols are being attacked by Trollocs.
- Ordeith is said to have hunted any kind of animals, and even people, for
  sport.
- It was Ordeith's men who killed Perrin's family.
- Bornhald has sent men with the peddler on missions but normally finds that
  whenever he does, the peddler reports a skirmish with Trollocs in which
  Bornhald's men have died: he thinks he is hiding something from him.
- He directly confronts the peddler including with physical violence but finds
  the man appears dismissive, unworried and even contemptuous of him.
- The peddler has been hiding a captured Myrddraal.
- He has been torturing the Shadowspawn and trying to get it to do something for
  him.
- The Myrddraal agrees.

## Chapter 32: Questions to Be Asked

- Perrin, Faile, the Aiel, the Warders and Aes Sedai head to the
  Whitecloaks camp, passing through villages on the way.
- Perrin muses out loud that even with the Whitecloaks protecting it, the
  Trollocs surely should have attacked Emond's Field.
- Vera wonders if it is some sort of lure, probably for Rand.
- When they reach a village, there is some suspicion among the villagers about
  why the Whitecloaks are interested in Perrin, Rand and Mat and so Perrin
  tells them he has killed Whitecloaks.
- Vera urges calm: the Whitecloaks are experts at turning neighbours on each
  other.
- Perrin tells the people that it was the peddler who let the Trollocs in to
  Emond's Field the previous year.
- The Hunter for the Horn, Lord Luc, is said to be approaching approaching.

## Chapter 33: A New Weave in the Pattern

- Lord Luc announces loudly about all the things he has done to protect these
  people and questions Perrin.
- Perrin is unimpressed with the man and his haughty demeanour but also notices
  that he appears to recognise Vera as Aes Sedai and also appears to
  recognise and perhaps even be scared of Perrin, or perhaps surprised to see
  him.
- He tells the people that the methods they have been practising to keep them
  safe will not last and that they should band together, perhaps by going to
  Edmond's field or another town, where they have a better chance of defending
  themselves from Trollocs.
- The people agree and Lord Luc leaves without much of a fuss; later Verin says
  she thinks the ease with which Perrin converted them to his cause is
  evidence of his _ta'veren_ nature.
- The rescue is executed with some skill, partly due to the expertise of the
  Aiel woman.
- The alarm is only raised when they are part way out of the camp.
- Perrin asks some of the young men to accompany him in drawing the Whitecloaks
  away from the rescued prisoners.
- It is only when they have got some distance away when he reveals he means to
  hunt Trollocs, presumably bringing the Whitecloaks into battle with Trollocs
  at the same time.
- He senses that Vera disapproves of this decision, though she says that there
  might be some rain coming, which will help their escape.

## Chapter 34: He Who Comes With the Dawn

- Mat and Rand return from Rhuidean to be confronted by the father of the
  other Aiel Rand saw enduring and failing to cope with the
  _ter'angreal_.
- He accuses Rand of murder and he and some of his clan throw spears at them,
  which they manage to avoid.
- A Wise One interjects and urges Rand to show that he has been marked twice,
  like in the prophecy of he who comes with the dawn.
- At this, most gathered Aiel leave without ceremony, though Rhuarc stays.
- They have a discussion about what happened in Rhuidean and Rhuarc says he
  believes that a person sees the past through their ancestor's eyes.
- Egwene informs him that they spent seven days inside, that Moiraine is still
  in there and no one has ever come out after 10 days.
- Rand thinks he must wait for Moiraine but is disappointed that he has lost
  seven days.
- Aiel cannot heal with the One Power like Aes Sedai, but both Mat and Rand
  are taken to their healers.
- Rand asks Rhuarc how he might declare himself He Who Comes With the Dawn and
  Rhuarc tells him there is a place of peace among the Aiel that would
  suffice - Alcair Dal.
- He then asks the Wise Ones if they would ask the clan chiefs to gather but
  they say it will be a hard task.
- He asks about his mother and hears a little of her story and how an Aes
  Sedai had a Foretelling about her: this is what led her to abandon him and
  travel to the Waste.
- He has a half-brother somewhere.
- He asks about the prophecies and the Wise One tells him that only the wise
  ones and some clan chiefs know the full extent of the prophecies.
- Without he who comes with the dawn, no Aiel will survive beyond the last
  battle but with him, though they will suffer, a remnant of a remnant will
  survive.
- The Wise One asks Rand what his plan is but he will not say.
- Aviendha and Moiraine return from Rhuidean later that night and much to his
  shame, Rand's main relief is that he has not lost another three days.
- His plan involves breaking the rules in a way no one has done before.
- He wants to be in charge of where it will happen.

## Chapter 35: Sharp Lessons

- Egwene meets Elayne in _Tel'aran'rhiod_.
- They exchange news, including that the Aiel Wise Ones and the sea folk
  wind weavers can channel.
- The Sea Folk believe Rand is Caramoor; the Aiel will take convincing that he
  is He Who Comes With the Dawn.
- Egwene is suddenly pulled from the dream by a furious Amys who chastises her
  for failing to keep her promise not to enter _Tel'aran'rhiod_ again without
  her guidance.
- Grudgingly, Egwene agrees to the restrictions put upon her but feels her life
  may be more difficult than Rand or Mat's.
- Amys takes her to a meeting of the Wise Ones where they discuss Rand.
- While they are confident that Rhuarc will follow and protect him, they believe
  he will offer no guidance.
- They task Aviendha with staying close to him, speaking with and most
  importantly listening to him; saying her good looks will make this easier.
- She resists, admitting she does not like Rand but Egwene says she should do
  this for Elayne.
- The Wise Ones are somewhat appalled by this justification, saying that she
  will do what she is told because she has been told to do it, not because of
  Elayne.
- After the meeting, Egwene asks Moiraine how she is after her experience in
  Rhuidean.
- Moiraine says she already knew some of the memories and that some of the
  others are fading, but no more.

## Chapter 36: Misdirections

- Travelling to the place of safety, Rand notices Avienda stays by his side,
  tells him the virtues of Elayne and asks him to talk to her so she might
  listen.
- Her manner is so blunt that Rand is convinced she is doing this by command
  rather than by her own choice.
- The group see a peddler approach without a guide, something which is not
  allowed in the Threefold Land.
- The man tells them that he did not know there were guides this far south and
  Rhuarc agrees that he can travel with them.
- A woman who was one of the peddler's companions offers to sell Mat a hat to
  shield him from the Sun.
- When they are on their way again, Rand half speaks to himself, half to Mat,
  saying that this peddler is not what he seems, that they ride with evil now
  and that he must not get caught in snares before he can lay his own traps.
- Mat wishes he had away out of the Waste.

## Chapter 37: Imre Stand

- The group stop at a dwelling place known as Imre Stand.
- Worryingly, they find signs of a massacre there.
- The attack looks to Mat like it was carried out by Trollocs but Rhuarc is
  appalled by this as no Trollocs enter this far south in the Three Fold Land
  for fear of the Aiel.
- The peddler who Rand does not trust leads his female companion to see the
  massacre before speaking to Rand; she is beautiful but Rand does not trust her
  either.
- Aviendha is ever at his side and he and Mat finally realises she was in
  Rhuidean to give up her spear and become a Wise One in training.
- This partly explains her foul mood.
- Rand practices swordplay with Lan; as the Aiel have rejected the sword this is
  Rand's way of trying to distract attention away from him.
- Rhuarc offers to teach Rand with a spear.
- There is conversation in the evening, with Mat telling a gleeman some of his
  stories of Rhuidean.
- Mat realises he is now fluent in the Old Tongue and that some of his memory
  holes are filled with ancient memories, surely not his.
- There is a sudden Trolloc attack, during which Mat fights with the spear he
  returned from the _ter'angreal_ with and fails to notice the silver foxhead
  medallion from the same place periodically turns cold.
- Moiraine protected the Wise Ones during the attack.
- Rand felt tired and weak but managed to fight as usual; Aviendha remains by
  his side and is outside his tent in the morning.

## Chapter 38: Hidden Faces

- A woman called Egeanin, who later is revealed to be a Seanchean leader who
  stayed after Falme, is questioning a man called Florian who we have previously
  met in another book, looking for particular _sul'dam_.
- She is unsure why the High Lady/Empress of the Seanchean has not launched an
  attack yet.
- Jaichim Carridin, a Children of the Light inquisitor, is having discussions
  with nobles and the king of Tarabon about the Children of the Light restoring
  order after the assassination of the Panarch.
- When alone, he finds him self in the presence of a woman who identifies him as
  Bors, the Darkfriend previously identified in a prologue.
- She identifies herself as Liandrin and threatens his exposure and tasks him
  with protecting the Panarch's palace.
- His hatred for Aes Sedai is such that although she has pretty much
  guaranteed his survival for the meantime, immediately plots his way
  immediately begins to plot how to be free of her and use the children of the
  light to kill or capture her.
- Liandrin updates herself with her Black Ajah followers.
- They are concerned that even though they have been promised to rise in
  prominence when the Dark One returns, they may find themselves trampled
  beneath the feet of warring Forsaken before that happens.
- One of them suggests balefire as a weapon against the Forsaken but Liandrin
  remains her that their first test of trying to control this nearly sunk the
  ship they were on.
- They were only able to test balefire because one of the _ter’angreal_s they
  were told to steal from the White Tower produces it.
- The group do not know exactly what it is they are in Tanchico to steal, but
  are sure it is in the Panarch's palace.
- One of their number, presumably a previous Brown Ajah, has been reading and is
  sure it must be another _ter’angreal_, this one with the ability to control
  someone else.
- They mean to use it to control Rand.
- Egeanin is in her house when she realises a man has snuck in.
- He identifies himself as a Seeker for the Truth and servant of High Lady
  Suroth, says he has been transported by the captain Bayle Domon and discusses
  the empress' plans.
- When he leaves, Egeanin is relieved and checks that he has not been in her
  basement.
- She has a captive _sul’dam_ down there.
- When she tried to use an _a’dam_ as a physical restraint she found it had the
  same effect on the _sul’dam_ as it does on _damame_.
- In Seanchean, girls are tested to see if they have the potential to channel –
  in which case they become _damame_ – or whether they are able to control an
  _a’dam_ and therefore a _damame_ – in which case they become sultan.
- The only way Egeanin can see that this _sul’dam_ can be affected by _a’dam_
  like a _damame_ is if _sul’dam_ can also channel.
- She thinks she may have discovered one of the empress' secrets and does not
  wish to die for it.
- The Seanchean believe it was a female Aes Sedai who caused the breaking of
  the world and that is why they treat channellers as they do.

## Chapter 39: A Cup of Wine

- On arriving in Tanchico, Bayle Domon sees Nynaeve and goes to apologise for
  being unable to wait in Falme.
- He takes them to an inn, saying it is not safe in Tanchico.
- Nynaeve openly explains their hunt for Black Ajah and recruits Domon.
- Thom plays for the crowd and Elayne unwittingly becomes drunk before
  confronting him: he admits to her that he played as bard in her mother's court
  but not that they were lovers.
- Nynaeve visits _Tel'aran'rhiod_ to search for Egwene.
- She sees a man, clearly not Aiel, in the Waste but is then warned by a woman
  to leave before he kills her.
- The woman appears to be the famous warrior Birgitte.
- Next she is in Emond's Field and a man, like Lan but not him, fires an arrow
  which barely misses her.
- The next morning, Thom and Juilin argue about the best way to hunt Liandrin
  but Nynaeve has made her own plans and has disguises for her and Elayne.

## Chapter 40: Hunter of Trollocs

- Perrin and his band of Two Rivers folk have had some success in raiding the
  Trollocs, thinning their numbers.
- He decides upon another attack, though at the meeting Lord Luc argues against
  the idea.
- Later, he tells Faile that he has some suspicions about Lord Luc, given
  that a number of the attacked farms and villages were visited by the man
  either the day of or before the attack.
- He also smells wrong: cold and inhuman. (Slayer?)
- Faile tries to chasten him for believing Luc to be a Darkfriend.
- As Perrin and the Two Rivers folk wait in ambush for the Aiel to send Trollocs
  their way, Perrin smells Trollocs behind them – they have been ambushed
  themselves.
- Perrin takes an arrow in the side but fights bravely until meeting his match,
  injured as he is, with a Myrddraal.
- At the last moment, one of the warders appears from nowhere and beheads the
  Shadowspawn.
- Alanna sent him after Perrin.
- Nearly half of the Two Rivers folk were killed in the battle and they retreat,
  eventually losing the smell of following Trollocs and hearing the sound of the
  Tuatha'an in the forest ahead.

## Chapter 41: Among the Tuatha'an

- Perrin and the survivors meet up with the Tuatha'an.
- They are the same band that Perrin met before – he is a little disconcerted
  about this coincidence.
- They feel unable to help with his arrow wound and instead will keep him
  comfortable overnight until he can be to the Aes Sedai in the morning.
- Perrin and Faile talk – Faile finally admits her parentage and also
  learns a bit more about Perrin's connection to Wolves.

## Chapter 42: A Missing Leaf

- In the wolf dream, Perrin discovers that the Waygate is open again.
- He survives an attempt by Slayer to kill him and manages to pursue and pin
  down the unknown man.
- Perrin describes his smell as like an unemotional human.
- In appearance, he is like Lan's cousin.
- Having survived an attempted arrow, Perrin manages to pin him down in a dense
  wood.
- Slayer unexpectedly engages in conversation, praising Perrin for the trick of
  closing the Waygate, but telling him that it is open again and that now
  Perrin's presence in the Two Rivers is known, it will be razed to the ground
  to get him, given the number of enemies he now has.
- Perrin realises Slayer is probably talking to buy time for another attack and
  wakes him self with effort.
- He tells Faile that he needs to go to the Waygate but she says he needs
  healing in Emond's Field.
- When they get there, he discovers the villagers have fortified their home
  under the guidance of the now-revealed Aes Sedai and their Warders.
- They have raised a wolf banner, confirming Perrin as their leader.
- Verin tells Perrin he needs Alanna's help with his wound.

## Chapter 43: Care for the Living

- Perrin is feeling guilty for the dead of Emond's Field; Faile continues to try
  to console him.
- Loial takes blame for the Waygate: he knew it was only locked from one side.
- Alanna arrives, assesses Perrin and says the wound is so great he may not
  survive the Healing.

## Chapter 44: The Breaking Storm

- When Perrin wakes, Faile reluctantly tells him that Gaul and Loial have gone
  to the Waygate.
- Perrin is determined to go and help but is in no shape.
- Suddenly there is a shout: Trollocs are on their way.
- With their new defences, they throw back the attack but just as Perrin
  realises it was just a test, a shout comes from the other side of the village.

## Chapter 45: The Tinker's Sword

- The noises from the back of the village were not Trollocs but Tuathu'an who
  had been attacked by the Trollocs.
- The villagers take them in.
- Perrin is unable to leave to go after Loial and Gaul as people keep asking his
  advice.
- The tortoise's numbers are severely decreased.
- One of the Tuatha'an children picks up a sword, forbidden by the Way of the
  Leaf, and argues strongly with his grandmother that had he known how to use
  one, he could have saved his mother and her daughter.
- The grandmother blames Perrin for allowing this aggressive thinking into
  the Tuathu'an's mind but Perrin says every man has a right to defend himself.
- He suggest to the boy that he learns how to use this sword from Tam.
- The Children of the Light and Lord Luc arrive, the former confronting the
  villagers.
- The Children suffered heavy losses in a fight at Taren Ferry; Luc asks if
  Ordeith was there (Perrin did not know Luc had knowledge of Ordeith).
- Dain Bornhald is incandescent when he sees Perrin and demands he be handed
  over.
- Beside him, Jaret Byar is uncertain that this is a wise idea.
- Perrin eventually suggests that the Children and the villagers join forces
  against the Trollocs; after the threat is dealt with, the issue of Perrin's
  arrest can be dealt with.

## Chapter 46: Veils

- Nynaeve and Elayne go out in disguise.
- Egeanin, who was out following Floran Gelb, sees two women clearly disguised
  and follows them instead.
- Elayne has been in _tel'aran'rhiod_ and learnt from Amys that Perrin did not
  go to the Waste but no more.
- They are confronted by robbers and begin to fight them off.
- Egeanin decides to help them.
- Despite efforts to avoid it, Elayne instinctively uses the One Power briefly
  and subtly in her defence; Egeanin clearly notices.
- After the fight, they invite her back for tea.
- Juilin reports that he found where the Black Ajah *were*, but it looks like
  they left there yesterday.
- Thom has discovered that a new Panarch (Amathera) will be invested the next
  day and Whiteloacks are moving into the palace; he thinks this is an unusual
  turn of events.
- She asks some questions, and they invite her to ask more when she wants and
  not to be afraid of Aes Sedai.
- Bayle Domon's name is mentioned; Egeanin leaves quickly after this as she
  knows Domon and worries her identity as a Seanchean might be revealed to these
  people.
- She is confused that Aes Sedai do not appear to be what she expected; she
  wants to learn more.
- Unknown to her, she is followed - it is Mor, the Seeker for the Truth.
- A woman who claims to know Nynaeve and Elayne arrives and the inn-keeper lets
  her in.
- She is not identified, but might be Liandrin, another Black Ajah or one of the
  Forsaken.
- She found them because of Elayne's use of the One Power.
- Before the women even see this enemy, she is using Compulsion on them and she
  questions them.
- She asks about what they are doing and what happened in Tear.
- She asks if they have _ter'angreal_ but does so in a way that means Elayne
  does not reveal it.
- She leaves, commanding them to forget the encounter.

## Chapter 47: The Truth of a Viewing

- The Amyrlin Seat is busying herself with some Maters when Elaida bursts
  in, saying she is deposed.
- We learn that Elaida believes the Amyrlin Seat aided in the release of
  Mazrim Taim; her reasons are not given.
- There is battle within the White Tower, Greens and Blues flee or are killed,
  Reds stay with Elaida and other Ajah are split.
- The Amyrlin Seat awakes to find herself stilled.
- Elaida is also looking for Min, but she is helped by one of the kitchen staff
  and rescues the Amyrlin Seat.
- During the escape, they bump into Gawyn.
- Gawyn has led a band of trainee Warders and is tired and bloodstained.
- His appearance is identical to that of the original viewing Min had of him.
- As she has lost her powers, most people do not recognise the Amyrlin Seat
  but Gawyn does and he demands to know where his sister and Egwene are.
- The Amyrlin Seat uses her new ability and lies immediately.
- Reluctantly, he agrees to let the women go but Min is confused as her final
  viewing flicks between him breaking Egwene's neck and laying his own head on
  her lap.
- This uncertain viewing never happened before.
- Before the women finally escape, the Amyrlin Seat, who has regained her
  composure, meets and recruit the False Dragon Logain to assist them.
- Min sees the aura of glory around Logain again.

## Chapter 48: An Offer Refused

- Rand travels the Waste.
- Aviendha tries to educate him in more Aiel ways.
- Moiraine only seems interested in his plan, which he will not divulge.
- Egwene spends most of her time with the Wise Ones.
- Aviendha is learning to channel; Rand is jealous as he has no one to teach
  him.
- The Peddler Kadere offers Rand information in return for safety (Rand refuses)
  and the gleeman Natael asks information of Rand to compose a song.
- Mat is little company.

## Chapter 49: Cold Rocks Hold

- Approaching Rhuarc's home, clan chiefs, Rand, Amys and Couladin move ahead.
- The clan chiefs asks for passage appropriately, but Couladin asks as if he was
  a clan chief, for which he is given leave to enter but only for water and
  shelter.
- Rand is welcomed as _Car'a'can_.
- Rhuarc informs Rand that some chiefs have already arrived at Alcair Dal.
- Rand is torn between going their now to reduce the chance of infighting or
  waiting as Rhuarc suggests: anyone not present when the _Car'a'can_ arrives
  will be dishonoured.

## Chapter 50: Traps

- Rand is trying to make amends with Aviendha, even though he does not know the
  source of her quarrel.
- While talking the Wise Ones, one of them is about to say something but Amys
  interrupts.
- Fed up of the Aiel spartan life, Rand asks for water to wash and falls asleep,
  dreaming of swimming.
- In his dreams, Rand is swimming with Elayne and Min when a woman confronts
  them and joins him, probably Lanfear.
- This woman then talks to another insubstantial man who might be another
  Forsaken.
- When he awakes, he senses someone in the room but soon finds it to be
  Aviendha, watching over him; she also divulges that the Wise Ones watch him in
  his dreams.
- Rand still senses something is wrong and is right: Draghkar attack but he
  manages to kill them.
- There is also a Trolloc attack in Cold Rocks hold; Mat realises it is a
  diversion for the Draghkar attack on Rand.
- After, Moiraine tries to get Rand to confide in her and so he challenges her,
  using her vow to tell the truth, and she cannot agree to his terms.
- Rand learns from Rhuarc that Couladin and the Shaido have already left for
  Alcair Dal.
- Rand determines to leave for Alcair Dal immediately, accompanied by the
  Maidens and Mat.

## Chapter 51: Revelations in Tanchico

- Since the new Panarch was invested, there have been riots in Tanchico, with
  the Children violently keeping the peace.
- This has kept Elayne and Nynaeve indoors, with frequent visits from Egeanin
  the only relief from boredom.
- During a visit, Bayle Domon enters, recognises and starts a fight with
  Egeanin.
- Elayne binds them and Egeanin is forced to explain; she confesses everything
  but explains she has learned so much since Falme.
- Elayne finds it hard to hate Egeanin; Nynaeve does not.
- Domon's news was that he has found at least two of the women they are after in
  the Panarch's palace.
- Juilin and Thom also arrive separately, also having learned the same but by
  different means; all three men are surprised by the others' successes.
- Nynaeve prepares to enter _Tel'aran'rhiod_ using the _ter'angreal_ to keep
  their meeting with Egwene, even though Egwene has not been there for some
  time.
- Elayne watches over her, thinking about how to discover if the Panarch is
  unaware of the Black Ajah, being controlled by them or a Darkfriend.

## Chapter 52: Need

- In _Tel'aran'rhiod_, Nynaeve is surprised to come across Egwene, flanked by
  Amys and Bair.
- They exchange news before Nynaeve eventually begs for help.
- Amys hints that need is the key to navigating _Tel'aran'rhiod_; she also
  explains why entering another's dream or bringing that person into your dream
  are not good ideas.
- With this information, Nynaeve enters the Panarch's palace.
- She sees the new Panarch being toyed with by one of the Black Ajah, Temaile
  Kinderode.
- She then sees a woman who she for some reason cannot remember and cannot bring
  herself to think about: she realises this woman has used Compulsion on her in
  Tanchico and is quick to anger.
- As if from nowhere, Birgitte appears to tell her that this woman is not Black
  Ajah but the Forsaken, Moghedien.
- She says the man she encountered in the Aiel Waste in _Tel'aran'rhiod_ was
  another Forsaken, Asmodean (Lanfear previously told Rand that Asmodean could
  teach him about _saidin_).
- Birgitte tells her a little about her life cycle and the rules: even by
  speaking to Nynaeve she has broken a rule.
- Again as if from nowhere, another legendary warrior - Gaidal Cain - arrives
  and chastises Birgitte before both leave.
- Nynaeve goes to look at what Moghedien was looking at: she finds a seal to
  Shayol Ghul.
- She also finds what appears to be a male _a'dam_ in the palace: surely what
  the Black Sisters are looking for; she thinks Moghedien has seen it in
  _Tel'aran'rhiod_ too.
- She awakes and updates Elayne; Egeanin says the male _a'dam_ is not a
  Seanchean creation.
- Thom is attacked outside but Juilin incapacitates the man before recognising
  him.
- He is the Seeker keeping tabs on Egeanin.
- Egeanin confesses an attraction to Bayle Domon to the other women.

## Chapter 53: The Price of a Departure

- During his ongoing mission in the Two Rivers, Perrin stumbles upon an injured
  Loial carrying a severely injured Gaul.
- They successfully closed the Waygate but think they saw thousands of Trollocs
  who had already entered.
- Lord Luc is also there and disputes this claim: one thousand at most.
- Perrin has a wolf dream and sees as many as ten thousand Trollocs; defeat is
  inevitable.
- He decides to hunt Slayer and manages to loose an arrow at him.
- The man turns in time to be struck in the chest, before fading.
- Perrin awakes to be told Lord Luc just took off, looking gravely unwell and
  clutching his chest.
- Perrin finds Faile and makes her promise to carry out a pointless errand: one
  which Perrin knows will take her out of the doomed Two Rivers.
- She agrees, as long as Perrin marries her.
- They wed that night.

## Chapter 54: Into the Palace

- The women enter the Palace with the men having started a riot to cause a
  distraction.
– Elayne and Egeanin are able to rescue the Panarch after Elayne successfully
  controls the Black Sister looking over her.
– Nynaeve goes in search of the artefacts and stumbles upon the Forsaken
Mogadishu
  by chance.
– However, it is Nynaeve who saw her first and she uses this to her advantage by
  launching a furious attack in an attempt to cut the woman off from the one
  power.
- The woman resists and initially appears to toy with Nynaeve, telling her all
  about the artefacts and that even with the collar, they will find controlling
  the Dragon Reborn has negative consequences for those who seek the control.
– Just as Nynaeve is becoming desperate holding this woman at bay, she realises
  that in truth, the woman is not toying with her but trying to distract her as
  she is struggling to contain Nynaeve's power.
- The realisation is all Nynaeve needs to take control and cut the women off.
– She takes numerous _ter’angreal_, an intact seal of the Dark One's prison
  and the collar and two bracelets which can control Rand.
– When leaving, she sees one of the Black Sisters carrying a rod which she
  recognises as a _ter’angreal_ which can produce balefire but is notoriously
  erratic.
- She is just in time to hide as balefire erupts, destroying part of the palace
  around her.
– Mysteriously, despite being cut off from the One Power, Moghedien appears to
  have disappeared.
– Nynaeve meets back up with Elayne, Egeanin and the Panarch and the woman find
  their male companions and leave the palace.

## Chapter 55: Into the Deep

- Nynaeve decides the collar and bracelets, described by Moghedien as
  indestructable, are too dangerous even for the White Tower.
- She asks Bayle Domon to take them to the deepest sea he knows and drop them
  overboard.
- Nynaeve wants to go to Lan in Tear.
- Elayne and Egeanin will go to the White Tower with the seal.

## Chapter 56: Goldeneyes

- Perrin prepares a desperate stand-off against the Trollocs.
- There is still time for as confrontation with Dain Bornhald; Perrin
  manages to convince them to stay and help in the fight.
- The fight is one-sided with the Two Rivers folk only holding their retreat
  thanks to the women joining the fight.
- Perrin notices he has not seen the Whitecloaks fighting but then a shout comes
  up and fighting begins elsewhere.
- Perrin sees Faile and Bain leading other Two Rivers folk into battle.
- The Trollocs are defeated.
- Bornhald arrives asking for Perrin to hand himself over.
- He says he only promised that if the Whitecloaks fought.
- The women saw the Whitecloaks not joining the fight and stand before Perrin.
- Perrin tells them to leave the Two Rivers.
- Ordeith is watching, infuriated that his plan has failed.
- He blames 'Isam' for stopping bringing in the Trollocs; Isam appears to be
  Luc/Slayer.
- He sees the Manetheren banner and has another idea for the Two Rivers' and
  Rand's downfall.
- He plans to go to Tar Valon and reclaim his dagger, but first it Caemlyn.

## Chapter 57: A Breaking in the Three-Fold Land

- Arriving at Alcair Dal flanked by Maidens and Rhuarc's clan (the Taardad),
  Rand finds Couladin and the Shaido Aiel already there in number.
- Moiraine was not allowed to accompany him.
- Including Rhuarc, five chiefs are there, but so is Couladin, who is not a
  chief.
- Couladin bears the two Dragon marks, and declares himself He Who Comes With
  the Dawn.
- Rand counters by showing his marks and challenging Couladin to say what he saw
  in Rhuidean.
- When Couladin lies, Rand goes against Rhuarc's advice and announces to the
  tens of thousands of Aiel that they once followed the Way of the Leaf, and so
  the Tinkers are closer than them to being true Aiel.
- The Shaido react by, though forbidden in this place, attacking.
- In the midst of the attack, Rand channels, bringing rain to the Three-Fold
  Land.
- During this distraction, he turns to look find Lanfear and says he knew she
  was present and this has been a trap for her.
- He asks where 'he' (Asmodean) is.
- Lanfear counters: Couladin's dragon marks and the Draghkar attack were
  Asmodean's diversion for Rand, to speed him to Alcair Dal while Asmodean
  enters Rhuidean; Lanfear does not know why.
- Rand uses the One Power to transport himself using a Gateway and desperately
  chases, knowing what Asmodean seeks.

## Chapter 58: The Traps of Rhuidean

- He catches up just in time for both men to place their hands on the miniature
  statue of a man holding a sphere.
- It is a _ter'angreal_, connecting the user to the most powerful male
  _sa'angreal_, the large original version of this statue which Rand saw being
  unearthed in Cairnhein.
- With so much power, the two men destroy Rhuidean around them but are matched.
- Rand remembers the small _angreal_ he bears and uses it to cut Asmodean off
  from the Dark One.
- Asmodean is bereft and Lanfear arrives, saying she will not aid him.
- In fact, she will tell the other Forsaken he has betrayed them and shields him
  to minimise his use of the Power.
- She did not know it was possible to be cut from the Dark One.
- Rand tells Asmodean he will teach him, keeping his disguise as a Gleeman.
- Asmodean talks angrily to Lanfear, calling her Mierin, a name Rand heard while
  in the glass columns.
- She was the female Aes Sedai who opened the Bore to the Dark One's prison.
- Asmodean says he is no longer protected from going mad: that will be both
  their fate.
- After Lanfear leaves, Rand finds the female _ter'angreal_ equivalent and
  Asmodean says she will be upset she did not find it herself.
- Rand tells Asmodean that he will first teach him to shield his dreams.
- They re-enter Alcair Dal, the Dragon Reborn and his gleeman.
- Moiraine notices the statues.
- The Shaido Aiel have left.
- Rand reminds himself the Aiel are a means to an end, and he must be harder
  than even them.

> *"And when the blood was sprinkled on ground where nothing could grow, the*
> *Children of the Dragon did spring up, the People of the Dragon, armed to*
> *dance with death. And he did call them forth from the Wasted lands, and they*
> *did shake the world with battle."*

> From the Wind of Time by Sualmein so Bhagad Chief Historian at the Court of
> the Sun, the Fourth Age


[Home](/blog "ohthreefive")