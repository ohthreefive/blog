+++

date = "2018-12-29T23:33:32Z"
title = "Summaries: The Silmarillion"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Silmarillion

**j r r tolkein**

## Ainulindalë (Music of the Ainur)

- The god figure is Eru Ilúvatar
- He created the Ainur from his mind, each representing an individual part of
  him, using the Secret Fire/Flame unperishable
- They sang separately at first; Ilúvatar brings them together in a chorus
- Melkor, the strongest and wisest, seeks to dominate the chorus
- He also sought the Secret Flame
- Ilúvatar shot him down; he got up again
- Ilúvatar shot him down again, apparently aided by Manwë, described as Melkor's
  brother and near equal; he got up again
- Ilúvatar then re-asserted dominance a third time and they sang together
- From their song, the universe was created, Eä to the Ainur
- Elves and Men/Edain are called the firstborn
- Ilúvatar created them with knowledge unknown to the Ainur, and they were
  therefore captivated by them
- The Ainur are also particularly taken with water
- Ilúvatar then told the Ainur that they could go to Eä but would have to remain
  there
- Many did, and became known as the Valar
- Ulmo is the Ainu associated with water, and most musical
- Manwë (wind & air; greatest in authority) and Aulë (the smith/maker) are also
  named
- When they arrive, they discover Eä is formless, and realise Ilúvatar had only
  shown them a vision of what was possible; they must work to form it, including
  earth (Arda to the Elves)
- The Secret Flame was set at the heart of Arda (or Eä; I'm not sure)
- Melkor sought to shape the world to his dominance and was cast out
- The other Valar got on with the job of creating the world they had been shown
- Melkor returned, jealous, and waged war with the other Valar
- This great war is not recorded in histories, but sounds very apocalyptic,
  which is ironic as they were fighting about and at the beginning of the world,
  not the end
- Despite Melkor destroying mountains etc., the Valar were able to progress
  creation
- Of note, the Valar can enter Arda seen or unseen to the Children

## Valaquenta

- Includes a summary of Ainulindalë.
- Clarified is that the Secret Flame lives at the heart of Eä.
- However, Eä is described as 'the world', which is ambiguous.
- It is also described as vast and I believe it more refers to the universe
  rather than to the earth, which is referred to by its Elvish name 'Arda' elsewhere.

### of the Valar

- Valar is the Elvish name for the Ainur who came in Arda.
- There are seven of each and Melkor is considered separately, although also one
  of the Ainur.
- It is stated that the names provided here are the names that the Elves of
  Valinor gave to the Valar, though the place Valinor has not been defined yet.
- It is also said that the Elves of Middle Earth and that Men had many different
  names for these guards.
- The seven of each are presented in order of magnitude, though their individual
  skills differ.
- Melkor and Manwë are first in might of the Ainur and are brethren.
- Melkor is described as, in the beginning, the greatest of the Ainur.
- Manwë is described as the dearest to Ilúvatar and the one who best understands
  the mind of Ilúvatar.
- Manwë was meant to be first among equals and lord of all the world.
- His delight is in wind, the air and all swift birds.
- He is giving the surname Súlimo.
- His spouse is Varda.
- Her delight is in the stars and she best has knowledge of all the regions of
  Eä. She is also associated with light, and the light of Ilúvatar is said to
  still dwell in her face.
- She is described as too beautiful to be described in the words of Elves or
  Men.
- Varda is said to have known Melkor prior to the creation of Eä and to have
  rejected him in this time.
- For this rejection, Melkor hates her.
- It is also stated that Melkor fears Varda most of all, though the reason for
  this is not given.
- Varda was the first to come to the aid of Manwë in year in his fight against
  Melkor.
- Manwë and Varda are said to remain in Valinor, be seldom parted and to dwell
  in the highest tower atop the tallest mountain, Taniquetil.
- When seated together on their thrones, it is said Manwë can see further than
  anyone and Varda can hear all things.
- The Elves revere Varda greatest of all, they name her Elbereth.
- Ulmo is Lord of water and second in might to Manwë.
- Before the creation of Valinor, Ulmo and Manwë were great friends.
- However, since then, Ulmo dwells alone, seldom leaving water or attending
  councils in Valinor, except in times of great need.
- When he does appear to Elves or men, he is terrifying to behold.
- His voice is unfathomably deep.
- He is said to have never abandoned the Elves or men, even when they had
  invoked wrath of the other Valar.
- Without leaving the water, he is still able to travel deep within Middle Earth
  due to streams and tributaries.
- He has great horns called the Ulumúri, and when he plays these, any who hear
  them are left with a longing for the sea for the rest of their life.
- Due to the ubiquitous presence of water, Ulmo is said to hear more of the
  happenings of Arda than even Manwë.
- Aulë is Lord of all the substances of Arda, next in might to Ulmo, and with
  him and Manwë wrought much of the world in the beginning.
- The Noldor learned most of & from him and he was always their friend.
- In thought and powers, Melkor was most like Aulë.
- Melkor was jealous of him and continuously destroyed that which Aulë created,
  the repair of which grew wearisome for Aulë.
- Both delighted in creation, especially of things which where unique or
  absolutely new.
- Both appreciated the praise received for their creations.
- However, Aulë really did not envy the works of others and dedicated all he
  created to Ilúvatar.
- Yavanna, the giver of fruits, is spouse to Aulë and lover of all things on the
  Earth which grow.
- Yavanna is second to Varda.
- She is surnamed Queen of the Earth by the Elves.
- Mandos and Lórien, masters of spirits, are brothers, though these names are
  the names of their dwellings. Their true names are Námo and Irmo.
- Námo is the elder and is the keeper of the houses of the dead.
- He forgets nothing and knows all things that will come to pass, other than
  those things which have not been revealed by Ilúvatar to the Ainur.
- He is the doomsman of the Valar but only pronounces his doom at the behest of
  Manwë.
- His spouse is Vairë the weaver. She weaves all things that have ever been in
  to webs which cover the halls of Mandos.
- Irmo the younger brother is the master of visions and dreams.
- Estë the healer is his spouse and her gift is rest.
- From the fountains of Irmo and Estë the dwellers of Valinor draw their
  refreshment.
- Nienna, sister to Námo and Irmo, is mightier than Estë and grief is her
  domain, though she does not weep for herself.
- Her gift is pity and hope; she gives strength to the spirit.
- She lives very remotely and most often visits the halls of Mandos.
- Tulkas is greatest in strength and feats of prowess.
- He came last of the Valar to aid their first fight against Melkor.
- He is tireless, has no steed as he can outrun any, and wields no weapons.
- He cares little for past or future and so can offer little counsel.
- Nessa is his spouse, sister of Oromë.
- She loves dancing and deer.
- Oromë is described as a little less powerful than Tulkas but more dreadful in
  anger.
- Oromë was last of the Valar to depart Middle Earth and return to Valinor.
- He is the hunter of monsters, loving trees, hounds and steeds.
- Naha is his white (day) or silver (night) horse, and he has a great horn.
- Nienna is his spouse, younger sister of Yavanna.
- Birds sing at her passing; flowers open at her glance.
- It is said that history (of the children of Ilúvatar) recalls only some of the
  depths of the Valar, and that what was ever unknown to the Elves is vast,
  implying any stories about them only scratch the surface of their whole being.
- 9 Valar are greatest in power and reverence but 1 is removed of their number -
  cryptic! These 8 are the Aratar:
    - Manwë & Varda
    - Ulmo
    - Yavanna & Aulë
    - Mandos/Námo
    - Nienna
    - Oromë
- Manwë is king but they are equals in majesty, greater than all sent by
  Ilúvatar to Eä.

### of the Maiar

- The Maiar are of the same order but lesser in standing than the Valar; they
  are servants and helpers of the Valar.
- Their number is unknown and few are named, partly because they did not often
  reveal themselves to the children of Ilúvatar in Middle Earth.
- It is said that this is different to in "Aman," though we are not told
  what/where Aman is yet.
- The greatest two serve Manwë (Eönwë the banner bearer and herald) and Varda
  (Ilmare the handmaid.)
- Eönwë is the mightiest in arms of those in Arda.
- The best known are Ossë & Uinen.
- Ossë is vassal to Ulmo, loving the coast and islands (rather than deep water)
  and loving the winds of Manwë as he delights in storms and violence.
- Uinen is his spouse and opposite in some ways, loving the sea and being able
  to calm Ossë.
- Mariners love but do not trust Ossë; the Númenorians revered Uinen as highly as
  the Valar.
- Melkor hates the sea (he could not subdue it) and bent Ossë to his will to try
  to control it during the creation of Arda, but with the help of Uinen (at Aulë's
  bidding,) Ossë was restrained, repented and was forgiven by Ulmo.
- He is said to have thereafter been _mostly_ faithful - occasionally his love
  of violence sees him rage without the command of Ulmo.
- Olórin (Gandalf!) is said to be the wisest Maiar.
- He spent a lot of time with Nienna, learning pity and patience, and hence in
  the Second Age had pity and love for the children of Ilúvatar.
- In the First Age, he either stayed hidden from the Elves or appeared as one of
  them, so the Elves did not perceive from where his counsel or visions had come.
  He was able to quell fear and despair in the children.
- Melian served Vairë and Estë and dwelled long in Lórien, tending Irmo's
  gardens, before coming to Middle Earth.
- Melian is a major figure in the upcoming Quenta Silmarillion.

### of the Enemies

- Melkor ('he who comes with might') was the most powerful of the Valar.
- He is said to descended from splendour to arrogance to contempt for all but
  himself.
- He was able to contend with all other Valar and for a time held greater dominion
  over Arda than them.
- He first desired light, but failing to achieve it, turned to darkness, using it as
  his servant.
- The Elves of the Noldor suffered worst from him and do not speak his name, calling
  him Morgoth.
- He attracted many Maiar to himself in the beginning, and later recruited other servants.
- From Maiar he created the Balrogs (Valaraukar.)
- He greatest servant once served Aulë and is named Sauron by the Eldar, or Gorthaur the
- Cruel. He was a part of all Melkor's evils.
- Sauron's evil is only less than that of Morgoth in that he was once a servant, something
  Melkor never was.

## Quenta Silmarillion: a History of the Silmarils

### of the Beginning of Time

- The first war came before the creation of Arda. Melkor had the advantage but
  as the echoes of war passed across the universe, Tulkas perceived and came to
  the aid of the Valar.
- Melkor departed in fear of Tulkas and developed a strong hatred of him.
- Tulkas chose to remain in Arda.
- The Valar used this time of peace to mould and shape Arda.
- To bring light to the world, Aulë created two lamps at Yavanna's request and
  Varda filled them. Manwë raised them atop two great pillars.
- By the light of the lamps, the seeds of Yavanna were able to grow. It is said
  that animals came forth to fill the forests but it is not said whether they were
  created by the Valar, sent by Ilúvatar or something else.
- The Valar chose to live on an island called Almaren which was situated at a
  point equidistant from the lamps, where the light of both met.
- The Valar rested to watch their world grow but, because of the light of the
  lamps, did not perceive the return of Melkor. He was now shrouded in darkness
  and his shadow and the light of the lamps helped hide him from the Valar
- His spies among the Maiar had informed him of the feast that Manwë had
  declared. He gathered allies.
- Aulë and Tulkas were most tired of the Valar due to the efforts they had put
  in to the creation of Arda.
- At the great feast, Tulkas married Nessa and thereafter rested.
- Melkor saw this as has time to return and passed into the north of middle
  earth.
- He was still hidden by the light of the northern lamp, being north of it.
- He began creating his massive and deep fortress of Utumno.
- His return was finally realised when disease and malice spread from here, for
  example with forest dying and beasts turning to evil creatures.
- Melkor launched an assault on the two lamps with his followers, successfully
  destroying their pillars.
- The effort of this assault also tore the earth apart and the spilling lamps
  caused massive fires.
- It is said that the first creation of Arda by the Valar was in perfect
  symmetry but after this destruction by Melkor, it was never recreated in the
  same way.
- Melkor retreated to Utumno (his stronghold) in fear of Manwë and Tulkas and managed
  to reach it before he could be overtaken.
- The Valar were unable to overthrow Melkor in this time as they needed the
  greater part of their strengths to restrain and save Arda: this is the end of
  the Spring of Arda.
- The dwellings of the Valar on Almaren were utterly destroyed and they chose to
  leave Middle Earth, heading to the westernmost island on Arda called Aman. The
  western shore of Aman looked out to the Outer Sea.
- To the east of Aman was the great sea of Belegaer.
- To protect themselves from Melkor, the Valar raised the tallest mountain range
  on the eastern shore. These mountains were called the Pelóri and the highest is
  Taniquetil, upon which Manwë has his throne.
- West of these mountains is the region of Valinor, in which the Valar chose to
  dwell.
- The Valar had collected all they had saved from Middle Earth and, along with
  new and even more beautiful things they created, they made Valinor into an even
  more splendid place than Middle Earth in its spring.
- There was no imperfection or entropy in Valinor.
- The city of the Valar was known as Valma.
- West of its walls was a great mound on which Yavanna sat and sang a great song
  of all living things of her thought.
- With her was Nienna, but she did not externally display her thoughts in song,
  but rather watered the mound with her tears.
- The Valar gathered to listen to the song.
- They were said to be sat upon their thrones near the west of Velma, called
  Máhanaxar or the Ring of Doom.
- From the song, the two trees were created, the most important creations of
  Yavanna and the most historically important.
- One tree bore silver fruit and shone silver light into the world, the other
  gold.
- They waxed and waned over seven hours, overlapping by one hour, giving 12
  hours of continuous light.
- The silver tree awoke first and from that first hour of its light shining on
  its own are all the ages of Valinor counted.
- Varda collected all the water and dew from the trees into great vats.
- This time is known as the time of bliss in Valinor.
- Though the Valar loved Middle Earth, they rarely left Valinor in this time.
- In the dim light, Melkor was able to move around Middle Earth in various guises.
  All evils and sickness are attributed to him in this time.
- Aulë was the most industrious of the Valar in this time, not only in fits of
  creation but in the gathering of knowledge of the world, which he wished to pass
  on.
- He was known as friend to the Noldor, as this race of Elves were the most
  skilled craftsmen.
- So gifted were the Noldor that they were able to add to the knowledge of
  Aulë. They were the first beings to create gemstones and the greatest of these
  were the Silmarils.
- From his throne, Manwë like to keep an eye on the so-called outer lands, and
  he was aided by the counsel of great hawks and eagles who would report to him.
  However, as Melkor had surround themselves with impenetrable shadow, some things
  were even hidden to Manwë.
- Manwë's most favourite Elves were the Vanya and his most favourite art was
  poetry; the Vanya are Masters of such.
- He held a sceptre which was crafted for him by the Noldor.
- His wife Varda was known as Elbereth in the Sindarin tongue, the Maker of the
  Stars.
- Ulmo never dwelt in Valma but lived in the outer ocean.
- His great horns were forged by Salmar, who it is said came with him to Arda.
  I think therefore that Salmar must be one of the Maiar.
- The Teleri Elves knew Ulmo best.
- Due to the ubiquity of water, it is said that even those who wander deep
  within the darkness of Melkor, a darkness which cuts them off from the view of
  Manwë, can still be heard and aided by Ulmo.
- Yavanna would often return to Middle Earth to try to heal the hurts of Melkor.
- On returning to Valinor, she always counselled the Valar that they would, at
  some stage, have to wage a great war with Melkor.
- Oromë also visited Middle Earth regularly, on his white horse Naha, to hunt
  the evil beasts of Melkor in the dark forests.
- The Elves were created alike in stature and mind but significantly less in
  might than the Valar.
- As Elves and Men were created separately from the Valar, any interference by
  the Valar, for example when, even beneficently, they try to force their will on
  the children of Ilúvatar, the results are seldom good.
- Ilúvatar's gift to Men was that their hearts should seek beyond the confines
  of Arda rather than to be contained within it.
- Men should strive to shape their own destinies, rather than those within the
  music of the Ainur.
- Ilúvatar knew that this would result in Men being liable to disharmony and to
  straying from a righteous path but knew that in the end, all these things would
  be reflected in his glory.
- Along with the free will to pursue their own fate, death is part of the gift
  of mankind.
- Elves do not die until the world dies unless slain or dying of grief.
- Elves do not know where Men go when they die.
- If Elves do die, they go to the halls of Mandos and it is said that they are
  able to return from there.
- Although death was intended by Ilúvatar as a gift, Melkor found a way to
  pervert it by inducing fear of death in man.
- The Valar have declared that Men will take part in the second music of the
  Ainur after the end of the world. The Elves do not know what role they will play
  at this time.

### Aulë and Yavanna

- Aulë is impatient for the arrival of Elves and Men and so creates Dwarves. His
  impatience is because he wants someone to teach.
- He creates them by his own design and devises a language for them. He clearly
  knows this is not necessarily allowed and he keeps it secret from the other
  Valar.
- At this time, the world was under the dominion of Melkor and so he makes the
  Dwarves resilient, strong and unyielding.
- The first Dwarves created are the seven fathers.
- Obviously, Aulë cannot keep them hidden from Ilúvatar, who challenges him
  saying that his gift was his own life and not the ability to create life.
- Aulë's creation can therefore only be a reflection of his thoughts and deeds.
- Aulë gives a compassionate speech saying he did not want dominion over them
  but rather to teach them. He offers his work as a gift to Ilúvatar then says
  perhaps he should destroy them.
- He raises his hammer and the Dwarves flinch away from him.
- Ilúvatar sees this as kind-of free will and accepts them as a gift from Aulë
  instead.
- I wonder whether in truth Ilúvatar granted them this free will but kept that
  hidden from Aulë, perhaps as he was impressed by the speech.
- Ilúvatar promises not to alter their design but will not allow them to come to
  Arda before his children, saying they will sleep under the mountains until then.
- Ilúvatar said there would often be strife between his created and adopted
  children.
- The Dwarves are said to endure toil better than Elves or men. Their lifespan
  is much greater than that of men.
- The Elves believe that Dwarves return to stone after their death.
- That Dwarves themselves believe that Aulë will gather them in the halls of
  Mandos after their death and that in time, Ilúvatar will count them among his
  true children. Their ultimate purpose will be to help Aulë re-build Arda after
  the 'final battle.'
- They also believe that the seven fathers of the Dwarves are reborn and keep the
  same name. The mightiest of the Dwarves was Durin, whose home was the enormous
  cavern of Khazad-dûm.
- Of all dwarves, the children of Durin are most friendly with the elves.
- Yavanna was the first of the Valar to which Aulë revealed the Dwarves. She
  worries that as they are builders, they would not have a love for the living
  things of the world but rather the things of their own creation.
- Aulë tries to assuage Yavanna's fears by reminding her that the children of
  Ilúvatar will also have to kill living things in their endeavours, for example
  chop down trees.
- Unsatisfied by this, Yavanna speaks to Manwë but does not betray Aulë's
  creations.
- Manwë asks Yavanna what of her creations she holds the most dear and what she
  would like to be set apart from any interference by man or elf. Trees are her
  choice.
- Upon hearing Yavanna's truest desires, Manwë recalls the great chorus and
  realises that within it were thoughts and desires of all his brethren which he
  previously had not recognised. He gains a greater understanding of the song.
- Manwë realises that when the children of Ilúvatar arrive, spirits will also
  enter within the plants and animals of Arda as per the song of Yavanna.
- He also realises that from when he and Yavanna sang together during the chorus
  the Eagles will be created.
- Manwë also declares that shepherds will walk among the trees (Ents.)
- Yavanna returns to Aulë with a kind-of threat, telling him that the dwarves
  will have to take care not to anger the forests in their labours. Aulë's rather
  blunt response is that, nevertheless, they will need wood.

### of the Coming of the Elves and the Captivity of Melkor

- The growth of living things in Middle Earth stalled after the destruction of
  the lamps: Middle Earth was lit only by the stars whereas Aman was lit by the
  light of the two trees.
- Yavanna and Oromë were the only Valar to visit Middle Earth in this time,
  Yavanna to grieve over destruction of growing things and Oromë to hunt the
  creatures of Melkor.
- Yavanna even caused some still awake things to go to sleep, so that they might
  next wake in a brighter age.
- Melkor was very active in this period, drawing many evil spirits towards
  himself, including his Balrogs. He also created new terrors. His domain spread
  southward.
- He also created a fortress and armoury on the north west shore to defend
  himself from any assault from Aman – this was Angband and was given to Sauron.
- Yavanna and Oromë spoke at a council of the Valar with Yavanna urging the
  Valar to get rid of Melkor. She felt that the awakening of the children of
  Ilúvatar was near and did not want them to awake in a land of darkness, ruled by
  Melkor.
- Unsurprisingly, Tulkas was keen to go to war.
- Mandos spoke and declared that it was the doom of the firstborn to awake under
  starlight and therefore to revere Yavanna greatest of all.
- Varda therefore took the waters she had saved from the silver tree and created
  many new stars and constellations – said to be the greatest work of the Valar
  since their arrival.
- In the north, above Melkor's stronghold, she created a constellation of seven
  stars known as Valacirca or the Sickle of Doom, to remind Melkor of the presence
  of the Valar.
- The Elves awoke as Varda was finishing her labours, observing the stars first
  of all.
- The Elves awakened in a land to the east which is now lost. The first sounds
  they heard were of running water down the hills of this land.
- Oromë was the first of the Valar to find the Elves on one of his expeditions
  hunting the monsters of Melkor.
- However, Melkor had been aware of the Elves before the Valar and had sought,
  by capturing are killing some of them, to make them fear Oromë when he finally
  revealed himself to them: he was 'the hunter' who they feared was capturing
  Elves who wandered afar.
- The Elves called themselves the Quendi, "Those who speak with voices," as they
  had not yet met any other creatures who spoke.
- The first Elves are described as stronger and greater than contemporary elves
  however not more fair as the sorrow and wisdom learnt by the Elves over the ages
  has enriched their beauty.
- It was Oromë who granted the Elves the name Eldar which means people of the
  stars, however this subsequently referred only to the Elves who followed him
  west to Valinor.
- Because of the deceit of Melkor, some Elves fled at the arrival of Oromë and
  are said to have been lost.
- Melkor is said to have tortured and experimented on captured elves, creating,
  out of envy and mockery, they Orcs.
- Oromë returned to Valinor and told of the coming of the Elves and of the
  darkness of Melkor before returning to Middle Earth.
- Manwë thought long and communed with Ilúvatar before calling a council to
  which even Ulmo attended.
- There he declared that it was the will of Ilúvatar that they should go to war
  with Melkor for the control of Arda.
- The Valar had a swift victory, forcing Melkor and his forces to retreat to
  Utumno.
- While the battle continued, the Valar set protection around the Elves so the
  knew no more about this war.
- So severe were the battles that the shape of the Earth was changed by them.
- Eventually, Utumno was breached and Tulkas bested Melkor and bound him with a
  chain built by Aulë.
- Due to the vast depths of Utumno and Angband, some of the allies of Melkor
  were able to hide or escape, including Sauron.
- Melkor was imprisoned in Mandos, from where it is said not even the Valar can
  escape.
- Another council decided that the Elves should be brought to the safety of
  Aman. The chief opponent of this was Ulmo, who thought they should be allowed to
  remain and heal middle area.
- This interference in the fate of the children of Ilúvatar by the Valar was the
  source of many dooms.
- As the Elves had only seen most of the Valar in war, Oromë was again sent to
  bring them to the west. He first brought three Elves as representatives, who
  returned and urged their kin to come with them back into the west.
- The Elves were split, with those who went with Oromë retaining the name the
  eldar and those who chose to remain being called the Avari or unwilling.
- The people of Ingwë, known as the Vanya, where the smallest host and first
  host to travel to Valinor.
- Ingwë is described as the greatest ever of the elves, though he never returned
  to Middle Earth after leaving it this first time.
- The Noldor, who are the people of Finwë, were next to come. They are expert
  builders and craftsman, beloved of Aulë.
- The last host of Elves to come was also the largest and known as the Teleri.
  They had a love of water. Due to their size, their journey was slow and they had
  two leaders, Elwë and Olwë.
- The Elves who travelled west and saw the light of the trees are known as the
  Calaquendi or Elves of the light.
- Together, the Elves unwilling to travel to Aman and the Elves who were willing
  but never made it there are known as the Moriquendi or Elves of the dark.
- During the great march west, one stop was on the eastern bank of the great
  river Anduin, beneath the misty mountains. At this stop, Lenwë of the Teleri
  decided not to continue further and to remain in Middle Earth. His host are
  known as the Nandor and gained greater knowledge of the living things of middle
  earth than any other elves.

### of Thingol and Melian

- Melian was the most beautiful and the most wise of the Maiar. She loved to
  sing and her songs were a form of sorcery.
- She often spent time in Middle Earth.
- Elwë of the Teleri often travelled to meet his friend Finwë of the Noldor.
- On one occasion, during his travels to see Finwë, he heard the song of Melian
  and was captivated.
- He met her and immediately they bound themselves to each other and he never
  again returned to Valinor.
- His people were thereafter known as the Sindar.
- The Sindar are also known as the grey Elves and amongst the most beautiful of
  any of the children of Ilúvatar.
- In the language of his own people Elwë was known as Thingol.
- He was an extremely powerful Elf but had greater power given to him by Melian.

### Of Eldamar and the Princes of the Eldalië

- The Elves of the Vanya and the Noldor travelled to the western land of
  Beleriand before their voyage across the sea to Aman.
- Ulmo spoke to the elves, turning fear of the sea into desire and moved an
  island from the sea to the shore for them.
- The Elves were pulled across the sea on this island.
- The easternmost portion of the island broke off and remained, being called the
  Isle of Balar and often visited by Ossë.
- The Teleri, who made Olwë their king after the disappearance of Elwë,
  travelled later to the western shore and remained there, often being visited by
  Ossë and Uinen.
- Ossë taught the Teleri much sea craft.
- After many years, at the bidding of the Noldor and particularly Finwë their
  king, Ulmo agreed to assist the Teleri to come to Aman.
- This caused Ossë grief.
- Ulmo drew them across and what remained of the island.
- Ossë persuaded some to remain, and of that race, their king was Círdan the
  shipwright.
- Those closest to Elwë also remained, and hoped to find him, though in their
  hearts they also desired to travel to Aman.
- These Elves dwelt in the forests, rather than by the sea.
- Elwë finally returned but satisfied his desire to see the trees by seeing the
  light of a man in Melian's face.
- He was a magnificent sight to behold, his powers enhanced by Melian.
- When on the island, Ossë spoke to the Teleri and they urged Ulmo to hold their
  voyage. Ulmo agreed, in part because at the Council of the Valar, he was most
  keen for the Elves to remain in the Middle Earth rather than to be summoned to
  Valinor.
- The Valar and the Elves already in Aman where are grieved to hear their kin
  would not be joining them.
- There island was therefore named Tol Eressëa or the Lonely Isle.
- The Elves on Aman missed the stars most of all and so the Valar made a cleft
  (Calacirya) in the mountains of the Pelóri through which the Elves could see the
  stars. In addition, the light of the trees could also pass through this gap and
  it first reached the western shores of the lonely island. This resulted in the
  first flowers in Middle Earth growing on that western shore.
- Situated in an ideal position to see you through the gap in the mountains, the
  Elves raised a great mound (Tuna) and build their city (Tirion) upon it, with the
  highest tower belonging to Ingwë.
- As the Elves loved her silver tree, Yavanna made for them a lesser
  impersonation of it and the Sindar in named this Galathilion.
- A seedling of this tree was planted on Tol Eressëa (Celeborn) and a seeding of
  this she became the white tree of Númenor (Nimloth.)
- In this time, the Noldor discovered gems and how to shape them. They gave
  these freely to their kin.
- The king of the Noldor, Finwë, had three sons. The first, Fëanor, was of a
  separate mother to the other two.
- Fëanor was the greatest in skill and desire. Fingolfin was the most mighty and
  the youngest, Finarfin, was wise. He later married Olwë's daughter.
- Fëanor has seven sons. These included Celegorm, who was a hunter and so a
  friend to Oromë.
- Fingolfin had two sons and a daughter.
- Finarfin had four sons and a daughter, Galadriel, the most beautiful of the
  house of Finwë.
- Eventually, and especially because they could now see the light of the trees
  through the gap in the Pelóri, the Teleri decided they wished to come to
  Valinor.
- At the bidding of the Valar, Ulmo sent Ossë who taught the Teleri how to build
  ships.
- Once built, Ossë sent great swans to pull the ships across the windless sea.
  They dwelt on the shores of Aman.
- In time, the Vanya left the Elven city Tirion and moved to either within
  Valinor or to the mountain of Manwë.

### of Fëanor and the Unchaining of Melkor

- Fëanor was the name given to him by his mother but Curufinwë was his true
  name.
- Such was his spirit, it is said that his birth took all strength from his
  mother. She went to rest in the forest of Lórien however her spirit departed to
  Mandos, never to return.
- Before dying, she told her husband Finwë that the strength of many had gone
  into Fëanor. She could therefore bear no more children.
- Finwë was grieved.
- Fëanor was the first person to discover how to create gems more beautiful than
  those discovered in the earth.
- Fëanor's wife was similar in skill but more cool and temperament than him. His
  future deeds would cause them to grow apart. They had seven sons.
- There is great tragedy in the house of Finwë. Though he re-married, his love
  for his first wife was so persistent that he showed favour to Fëanor. In turn,
  Fëanor showed no particular love for his two half-brothers.
- Had Finwë managed his grief and been content with his one son, much strife would
  have been avoided. However his other two sons and their children also performed
  great and significant acts, so had he not had them, the deeds and history of the
  Elves would also be lesser.
- The time came for Melkor's retrial.
- Through deceit, he convinced the Valar of his repentance and vowed to aid them
  and the children in all their pursuits, including undoing the damage he had
  caused in the past.
- In truth, he had nothing but envy for the splendour of Valinor and the gems
  created, as well as bitterness towards the Elves (as they were the reason the
  Valar went to war with him.)
- It was Manwë's purity of spirit that prevented him seeing Melkor's deceit.
  Ulmo was not fooled and Tulkas was angered by his presence.
- However, by being nothing but helpful, Melkor was able to secure more and more
  freedom around Aman.
- Among the elves, Melkor thought the Teleri were too weak to be worth turning
  his attention toward and the Vanya were not interested in him as they were content
  living under the light of the trees. The Noldor however were delighted with all
  the new knowledge they could learn from him.
- Of all of the elves, Fëanor hated Melkor the most and first named him Morgoth.

### of the Silmarils and the Unrest of the Noldor

- Fëanor's greatest creation where are the Silmarils, three gemstones which
  contained the mingled light of the two trees. No one but Fëanor knows what the
  outer 'case' is made of.
- Despite the fact that the Silmarils' light came not from Fëanor, he coveted
  them and held them in his heart above all other things.
- Any evil-doer who touched the Silmarils would be burned.
- Melkor had such envy of the Silmarils that he went about making strife among
  the Noldor.
- He tried to convince them that they were being held captive by the Valar and
  to convince Fëanor that his brother Fingolfin was trying to usurp his claim to
  the throne.
- With the skills he had learned, Fëanor created weapons for his followers, as
  well as armour and shields.
- As his half brother was discussing with his father all of the tidings, Fëanor
  burst in and threatened Fingolfin at sword point. Fingolfin willingly departed
  but Fëanor caught up with him and threatened him once more. Again, Fingolfin
  departed peacefully.
- The Valar then called Fëanor before them.
- By interrogation, the Valar were able to learn that Melkor was at the heart of
  this disturbance. However, Fëanor could not be held blameless and was banished
  for 12 years to consider his actions and to return in peace. Fingolfin agreed to
  make peace upon Fëanor's return but Fëanor said nothing. His sons were banished
  with him.
- Mandos pointed out to Fëanor that Manwë was the king of all of Arda so even if
  he felt captive to him in Valinor, he would yet be subject to him in middle
  earth anyway.
- Because of his love for him, Fëanor's father went into exile with him but
  found his son's heart could not be changed.
- The Valar searched in vain for Melkor.
- Melkor came eventually in disguise to Fëanor and, with skill, rekindled all
  the bitterness he had towards both the Valar and his half brother.
- However, he overstepped his mark by stating that the Valar desired the
  Silmarils.
- On hearing this, Fëanor was able to see through Melkor's lies and realised it
  was him who desired the Silmarils more than anyone. Such was the anger and
  strength within Fëanor that even though he was an Elf standing before a Valar,
  he was able to send Melkor away. Melkor left in hatred and fear.
- Finwë reported the goings-on back to the Valar who again sought Melkor but again
  in vain.
- The Elves reported seeing Melkor flee north. At his departure, the light of
  the two trees, which had been diminished during this strife, was restored to its
  full brightness.

### of the Darkening of Valinor

- Despite seeming to head north, Melkor turned south as he thought the Valar
  would not seek him there.
- There, within shadows, he came across a great spider Ungoliant.
- Ungoliant did not come from Arda – I had previously thought she was one of the
  Maiar but it is not explicitly stated here.
- Ungoliant weaves webs of darkness around herself and Melkor and they travel
  north.
- During Manwë's harvest feast, to which Fëanor has been invited and has come
  alone and received the hand of friendship from Fingolfin, Melkor and Ungoliant
  attacked the two trees.
- Melkor stabs them with a spear and Ungoliant drinks their sap until they are
  dead.
- Not sated, the spider also drinks all the collections of water by Varda and
  she swells enormously.
- They flee north, intent upon Finwë and his seven grand sons in exile. Finwë
  does not flee and is slain by Melkor.
- All the precious gems, including the Silmarils, of the Noldor are stolen.

### of the Flight of the Noldor

- Yavanna knew that she could never create the trees again but, given that their
  light was captured within the Silmarils, she could rejuvenate them using the
  Silmarils.
- Aulë was most sympathetic towards the hard decision faced by Fëanor, who also
  knew he could not recreate the Silmarils.
- Fëanor refused to willingly hand over the Silmarils to the Valar, in part due
  to the distrust of them sown by the lies of Melkor.
- Shortly after this refusal, news of Finwë's death at the hands of Melkor and
  the taking of the Silmarils reached Valinor.
- At this news, Fëanor named Melkor 'Morgoth', the name by which he was known ever
  after by the elves, cursed Manwë for his summons and fled to his home in exile,
  realising that his father was more dear to him even than the Silmarils.
- At this time, Morgoth was fleeing north with Ungoliant and crossed via the
  narrow, icy straight connecting Aman to Middle Earth. Morgoth was unable to flee
  from Ungoliant at this time.
- Ungoliant realised that Morgoth was trying to reach his fortress of Angband
  and, before reaching it, stopped him and demanded all the jewels they had stolen
  from the Noldor.
- Morgoth grudgingly gave up all but the Silmarils, which he declared to be his
  for ever after, though they burned his evil hand even as he held them.
- Swollen in size and power with all she had eaten, Ungoliant attacked Morgoth.
  He let out a great cry which awoke sleeping Balrogs beneath Angband.
- They came to his aid and Ungoliant fled before them, dwelling for ever after
  beneath Ered Gorgoroth.
- The valley in this region was already inhabited by great spiders created
  during Morgoth's first reign in Middle Earth.
- Ungoliant mated with them and her offspring abode ever after in this region,
  though she later departed even further south.
- Her fate is unknown, though she may have devoured herself in her hunger.
- Morgoth then returned and rebuilt Angband, drawing and multiplying all evil
  towards himself. With the Silmarils, he made for himself a crown, the act
  causing his hands to be burned black and the crown to forever cause him pain and
  hatred. He never took it off.
- Only once thereafter did Morgoth leave Angband and only once did he again
  wield a weapon.
- Despite his banishment, Fëanor returned to the city of the Elves and gave an
  almighty speech, mainly against Morgoth.
- He also spoke out against the failure of the Valar to protect them and
  reminded those people that the Valar and Morgoth were of the same kind.
- Fëanor unwittingly repeated the lies of Morgoth, telling his people that the
  Valar were holding them captive in order to supplant them with men, who many of
  the Eldar had never heard of before. He declared that he would return to middle
  Earth to wage and unending war with Morgoth or anyone else who would take the
  Silmarils.
- Fëanor and his sons swore a dreadful oath to this effect. Fingolfin and one of
  his sons spoke against him.
- Finarfin and one of his sons spoke calmly and advised caution on all sides.
- Galadriel, daughter of Finarfin, would not swear the oath of Fëanor but did
  wish to travel and to rule her own kingdom in Middle Earth.
- Another of Fingolfin's sons wanted to see Middle Earth and Fëanor's cause won
  the day, and he planned his journey to Middle Earth immediately.
- Manwë, in part upset at the perceived hurt the Valar had done to the elves,
  did not hinder Fëanor, but also in part this was because he did not believe
  Fëanor could hold the Elves to his will.
- The departing Elves were not entirely harmonious, with many refusing to take
  Fëanor as their king.
- In particular, the house of Fingolfin were more keen to follow him and Fëanor.
- This was the greater host of the Noldor, and Fingolfin only agreed to follow
  because he would not be parted from his people or his son who was keen to leave.
- Finarfin was the most loath to leave, but did so with his people.
- Only a small portion of the Noldor chose to remain.
- At the last, Manwë spoke, saying he would neither help nor hinder the Noldor
  but strongly advised against their leaving due to the evils that had befallen
  and bad spirit in which the journey was being made.
- Manwë delivered a personal attack on Fëanor, saying he was exiled for his oath
  and that as Melkor was one of the Valar, his oath was in vain because Fëanor
  could never overcome him.
- Fëanor threw back in Manwë's face, essentially accusing the Valar of idleness
  and cowardice for refusing to challenge Morgoth, when he, who Manwë had named as
  inferior, would do so willingly. He also said that perhaps he underestimated him
  and that Ilúvatar had placed a secret flame within him that would allow him to
  defeat Morgoth. At the least, he would wound him so greatly that the Valar would
  be in awe of his deed.
- This is at least the third mention of the fire within Fëanor.
- Given that earlier in the creation story, Ilúvatar is said to have placed the
  Secret Flame from which he created all things within Arda, I wonder whether it
  actually resides within Fëanor, or whether I am just reading too much into this.
- Fëanor quickly realise his first mistake – his mighty host would need a mighty
  fleet to travel to Middle Earth and the Noldor had neither a fleet nor the
  skills to make one, certainly not quickly.
- Therefore, he decided to turn to the Teleri, who had always been friends to
  the Noldor.
- The Teleri, however, were happy with their home. They also did not want to aid
  the Noldor against the counsel of the Valar.
- Olwë spoke coolly after Fëanor denounced this as betrayal, saying that when
  the Noldor helped the Teleri when they first arrived, it was with a promise that
  they would remain in Aman forever.
- He also said that it was the duty of a friend to rebuke when appropriate,
  rather than to follow blindly, his friends.
- He declared that the ships of the Teleri were something they would not be able
  to create again, a third echo of this sentiment, and therefore they would not
  give them up. Led by Fëanor, the Noldor took the ships by force.
- When they had travelled very far north, they saw a great dark figure on the
  mountains, which may or may not have been Mandos. He spoke of their doom.
- He said any who would leave were lost to the Valar and that the oath would
  consume the Noldor utterly as ever they would lose that which they sought.
- He said that many Noldor would be slain, and those who were not, would become
  diminished in Middle Earth by their regrets.
- Fëanor spoke out once more against cowardice. Finarfin, filled with regret,
  chose to remain. He became king of the Noldor who remained, but his sons went
  with Fëanor.
- After travelling north, Fëanor decided he did not want to cross the
  treacherous icy plane of the Helcaraxë which only Ungoliant and the Valar
  had ever before successfully crossed.
- However, they did not have enough ships to carry the call of the Noldor.
- As his host had control of the ships, they stole away in secret and left the
  host of Fingolfin behind.
- The eldest son asked for the arrangements to send the ships back but instead
  Fëanor burned them.
- Fingolfin saw the ships aflame in the distance and was enraged. He and his
  followers, as well as Galadriel, risked the crossing of the Helcaraxë and
  successfully landed in Middle Earth, though many died in the effort.
- This crossing was one of the greatest deeds ever of the Noldor.

### of the Sindar

- Although they had not seen the trees, the grey Elves became the fairest and
  wisest of all Elves of Middle Earth due to the teachings of Thingol and Melian.
- Their only daughter was named Lúthien.
- In this time, Dwarves came over the Blue Mountains of Ered Luin into
  Beleriand.
- The Dwarves had created multiple underground cities for themselves, the
  greatest be in Khazad-dûm, which was later known as Moria.
- The Elves struggled to understand the dwarfish language and the Dwarves were
  not forthcoming in teaching it; conversely, Dwarves were able to learn Elvish.
- Dwarves felt closer to the Noldor and, of course, to Aulë, due to their great
  works of craftsmanship.
- Melian knew that the peace in Middle Earth would not last and she counselled
  Thingol of this who sought advice, including from the dwarves, about creating a
  stronghold.
- The dwelling was called Menegroth or the Thousand Caves, and only a single
  bridge lead to its entrance.
- It was the Dwarves who discovered that the evils of Melkor prevailed in his
  absence and had, in fact, multiplied and come forth.
- King Thingol began to arm his people with the help again of the dwarves, of
  whom Telchar from Nogrod was the greatest smith.
- The Dwarves were the greatest Masters of steel, including the Noldor, and also
  unsurpassed at making ringmail.
- Lenwë, one of Olwë's Teleri who had never returned to Aman, led a force of
  lightly armed woodland Elves who were terrorised by the evil creatures.
- His son Denethor therefore lead a host of his people to bring them in to the
  safety of King Thingol's realm.
- Peace prospered for some time, during which writing was conceived and this form
  of recording language spread.
- When Morgoth returned to Middle Earth, his scream during his fight with
  Ungoliant was heard by the Sindar.
- Ungoliant fled south but by the strength of Melian, was not able to enter the
  cities of the Sindar.
- She dwelt beneath mountains thereafter named Ered Gorgoroth or the mountains
  of terror.
- The now multiplied Orcs attacked Beleriand, and with the aid of Denethor, King
  Thingol won the first great war with them.
- Dwarves also aided in the first victory, though it was not without a price:
  Denethor fell in the fighting.
- When what remained of his people returned home crestfallen at the loss, some
  among them became a secretive people who evaded conflict and detection and did
  not wage war.
- They were the green elves.
- In the west, Círdan had lost his battle with the Orcs. In response, Thingol
  drew his allies close and Melian made a protective spell around their land,
  impassable by anyone less powerful than Melian. This was Doriath, the guarded
  land.
- Outside of this land, there was war and strife. Into this came Fëanor.

### of the Sun and Moon and Hiding of Valinor

- The Valar were as upset about the corruption of Fëanor as they aware of the
  destruction of the trees. Fëanor was the greatest of all the Elves and again,
  the novel talks about a flame within him.
- Mandos declared that Fëanor would soon be with him.
- Manwë asked Yavanna and Nienna for their help and through prayer and tears,
  they were able to make the trees bear one more fruit. Aulë and his helpers made
  vessels for them and Manwë raised them to the heavens and Varda gave them
  movement. Two of the Maiar were chosen to haul them around the world.
- One of the purposes of this was to give light to Middle Earth in order to
  hinder the deeds of Melkor.
- Morgoth hated the new light and attacked the moon but was beaten by its Maiar.
- In the creation of his new evil creatures, some of Morgoth's power had gone
  into them and he was therefore diminished in strength himself.
- He was too afraid to attack the Maiar who drew the Sun, such was her power.
- The Valar saw Morgoth's attack on the moon and recalled his destruction of
  their original home in Middle Earth. To defend themselves, they raised their
  Pélori mountains higher and with more sheer outer walls. The pass of the
  Calicirya was left open but guarded by towers and sentinels and an army on its
  western side.
- A series of islands were raised like a net from north to south, confounding
  any mariners who tried to reach Aman.
- Only one messenger ever made it west to Valinor again.

### of Men

- Men awoke to the east, in the land of Hildórien, at the first rising of the
  Sun.
- The Valar never came to greet or guide men, who then feared rather than loved
  them.
- The Elves do not know what happened to the spirits of Men after they died.
  Only one man ever returned after death and he never spoke to another man after
  his return.
- The Elves who never left Middle Earth were lesser than the Elves who saw the
  trees, except perhaps the Sindar in under the guidance of Melian.
- There were even important unions between man and elf, eg. Elwing and Eärendil,
  whose son was Elrond.

### of the Return of the Noldor

- Even before Fëanor and his followers could make their first camp in middle
  earth, Morgoth assaulted them.
- Despite the advantage of numbers and surprise, the Elves were victorious and
  drove the Orcs back towards Angband.
- This was the second Battle of Beleriand.
- Fëanor in his fury passed ahead of his host in pursuit and was cut off.
- He and a very small number of his followers where surrounded by Balrogs and
  eventually, Gothmog, Lord of the Balrogs, smote Fëanor.
- His sons arrived, drove back in the Balrogs and carried his body away.
- Before they could reach the camp, Fëanor stopped them, knowing he was dying.
  His last sight was of Thangorodrim in Angband and he realise he could never, nor
  could the Noldor, overthrow Morgoth. However, his last words were spoken to hold
  his sons to the oath.
- Morgoth feigned surrender and suggested a meeting with the Noldor. The eldest
  son of Fëanor, Maedhros, decided to meet the emissaries but to do so with a
  great host, suspecting betrayal.
- While Maedhros was correct, he underestimated Morgoth, who sent an even
  greater host. He was captured and all his followers killed. Morgoth hung him by
  his wrist from a precipice of Thangorodrim.
- At this time, Fingolfin arrived with his host and at the first rising of the
  Sun. Morgoth fled from this light and Fingolfin marched to Angband, sounding
  horns.
- Seeing the strength of Angband, Fingolfin chose to retreat and regroup,
  meeting with the rest of the host of Fëanor.
- Maedhros heard the horns of Fingolfin.
- Unsurprisingly, there was strife between the combined hosts of Fingolfin and
  Finarfin's son Finrod with the smaller host of Fëanor, which therefore withdrew
  to the southern shore of the lake at which they were in camp.
- Morgoth used the time given to him by this dissent among the Noldor to create
  vast clouds of smoke and ash to hide his allies from the new light of the sun. A
  son of Fingolfin, Fingon, realised it was time to heal the wounds of the Noldor
  in order that Morgoth might not take advantage of the Noldor's hesitation.
- Fingon had been friend to Maedhros before the troubles arose in the Noldor and
  he set out alone to find him, coming to the face of Thangorodrim hidden under
  Morgoth's own manufactured darkness.
- In despair, he sang an ancient song which Maedhros took up, revealing him to
  Fingon. He asked Fingon to shoot him with an arrow.
- Fingon was about to do so but Thorondor, greatest of the eagles, arrived and
  bore him up to Maedhros. Unable to undo Morgoth's chain, he cut his hand off to
  free him.
- This is remembered as one of the greatest deeds of the Noldor.
- This act also helps to unite the Noldor, with Maedhros declaring Fingolfin
  as their lord. However, not all of his brothers agreed with this in their
  hearts.
- King Thingol was wary about the arrival of so many new Elven princes and only
  permitted the children of Finarfin to enter his guarded realm.
- This was in part because their mother was the daughter of Olwë, brother of
  King Thingol.
- King Thingol declared that the Noldor could live where they wanted as long as
  it was not on his lands and it did not displace any of his people.
- Many of the Noldor declared this as a rather cool reception however Maedhros
  pointed out that in order to be a king, he must show strength and protect his
  people.
- Another son of Fëanor was more upset, and spoke out against this
  decree of King Thingol. Many of the Noldor were dismayed to hear more unrest
  coming from the house of Fëanor.
- The people of this brother, Caranthir, dwelt far to the east and they were the
  first to encounter that dwarves, who had retreated at the first onslaught of
  Morgoth.
- They made an alliance out of a combined hatred of Morgoth and shared knowledge
  and industry, though there was little love between them.
- Despite years of peace, doubts and fears were stirred within Finrod, son of
  Finarfin, by Ulmo.
- After a visit to Menegroth, Finrod decided he would like to build a similar
  safe place and, with the help of the Dwarves and of King Thingol, he did this.
- The place was called Nargothrond.
- Finrod paid the Dwarves well and they made for him a spectacular necklace, one
  of their great works.
- His sister Galadriel fell in love with Celeborn of the Sindar and remained in
  Menegroth, learning much from Melian.
- Seeing that the Elves were settled and had begun to wander, Morgoth quickly
  launched another assault, the third great battle of Beleriand. Again, his forces
  were defeated, this time utterly, mainly by the Noldor.
- Chastened by the attack, the Elves closed ranks and essentially laid a 400
  year siege on Angband.
- During this time, Morgoth sent many new evil creations to test themselves
  against the elves.
- He also use the opportunity to increase his spy network by capturing and
  torturing Elves to his will.
- 100 years later Morgoth attacked Fingolfin with a small force but was again
  beaten. Morgoth realised his Orcs alone could not match the elves.
- 100 years later again, Glaurung, his first dragon, issued forth and caused
  much destruction but was beaten back by Fingon. Morgoth was angry that Glaurung
  had attacked while still young and not at full power, giving the Elves an early
  glimpse at his machinations.
- Thereafter, there was another period of peace.

### of Beleriand and its Realms

- A geography heavy chapter.
- Highlights include the building of Minas Tirith by Finrod, although as he
  dwelt in his stronghold of Nargothrond, he gave it to his brother Orodreth.
- It was called Minas Anor at this time.
- Morgorth never built ships nor attacked Middle Earth from the sea; all his
  followers being distrustful and fearful of water.
- In Ossiriand, a great river split and then rejoined, leaving an islands in the
  middle called Tol Galen. Beren and Lúthien lived on this island after their
  return.

### of the Noldor in Beleriand

- Turgon was second son of Fingolfin.
- Turgon was led to the secret, mountain-encircled land of Tumladen by Ulmo.
- Ulmo showed Turgon the secret way, carved by underground waters, underneath
  the mountains into Tumladen. In the centre of the great plain of Tumladen stood
  a solitary hill.
- On this hill, over 52 years, the city of Gondolin and was built in memory of
  the city of Tirion.
- Ulmo told Turgon that the city of Gondolin would be the longest lasting of any
  Elven city, however that the doom of the Noldor would also befall him.
- Ulmo told Turgon that he should leave behind armour and weapons for a
  messenger to come to Gondolin in future to warn Turgon of the doom of the
  Noldor. By this, he meant treachery.
- It was more than 350 years until the people of Turgon left that land. In this
  time, they build Gondolin to be a most beautiful city and in it were two
  imitations of the great trees. Most beautiful of all, however, was Turgon's
  daughter.
- At this time, Galadriel and Melian often spoke and Melian perceived that there
  was much Galadriel had not told her.
- Galadriel disclosed some of the events in the west, including the history of
  the Silmarils, but did not talk of the treason of the Noldor.
- Melian foretold that the Silmarils would be the most significant objects in
  history and that Middle Earth would be brought to near ruin over their fate.
  This troubled her and of course she counselled king Thingol.
- Thingol confronted the sons of Finarfin. Finrod was unrepentant, saying that
  his people had done no wrong to the Sindar.
- His brother however spoke more angrily saying that the house of Finarfin was
  not responsible for the evil deeds of the house of Fëanor.
- Thingol was angry but did not lose his temper, instead declaring that the
  tongue of the Noldor should never be heard or spoken in his land but that his
  kin may yet be welcome there.
- Melian also foresaw that none of the Elves would ever recover of the
  Silmarils.
- Galadriel asked Finrod seemingly innocently why he had not married. Finrod had
  loved a Vanya who remained in Aman. However the question caused an unexpected
  vision in Finrod, who saw nothing would endure of his to pass on to a son.

### of Maeglin

- The sister of Turgon, Aradhel, was keen to leave Gondolin.
- On her travels south, she was denied entry through the girdle of Melian.
- While travelling around this area, she became cut-off from her party who
  presumed her dead.
- She had survived, however, but was in trapped within the enchanted forest of
  Nan Elmoth.
- In that forest lived a solitary and dark Elf called Eöl.
- He saw and fell in love with Aradhel and used his magic and that of the forest
  to draw her towards him.
- They had a son together called Maeglin. He was a keen smith and ever friendly
  with the Dwarves.
- After many years, Aradhel and Maeglin escaped and headed back to Gondolin.
  However, Eöl followed them and found his way in to Gondolin.
- He demanded his son at least but preferably also his wife be returned to him.
- He was refused and then his anger threw a spear at his son which his wife
  stood in front of.
- Although not a deep wound, the spear was poisoned and Aradhel died, for which
  the Elves of Gondolin killed Eöl.
- They saw this as justice but Idril, the daughter of Turgon, thought it was a
  troubling act.
- Maeglin was in love with Idril, even though they were cousins and this was
  frowned upon by else at that time.
- Idril had no love for Maeglin.
- Due to his great knowledge, Maeglin was a popular Elf in Gondolin although his
  desire for Idril drove everything he did, particular in gaining power.
- Therefore, the seed of evil was sown within Gondolin.

### of the Coming of Men to the West

- Some Men had travelled west from there original home and Finrod, while out
  hunting, encountered them and befriended one called Bëor.
- Thereafter, Men came in greater numbers to the west.
- They were never welcomed by the green Elves of Ossiriand but found some
  friends within the Noldor.
- Three great houses of Men are described, as well as their children who will
  have great impact in future events.
- In his initial discussions with Bëor, Finrod senses a darkness within men
  which they will not speak of – this has been caused by Morgoth who encountered
  them before the elves.
- The Elves call Men the Edain.
- Melian told Galadriel that even though King Thingol forbad any man to come
  into their hidden kingdom, one man would one day come, and this doing was
  greater than her power to prevent his entry, and songs will be sung of him which
  out last her.

### of the Ruin of Beleriand and the Fall of Fingolfin

- Fingolfin was concerned about Morgoth and wanted to attack. However, the
  relative peace in Beleriand meant he could not convince his allies.
- Morgoth then launched a massive surprise attack.
- He sent forth rivers of fire followed by Glaurung Lord of the Dragons followed
  by his Balrogs followed by his Orcs.
- They destroyed the siege of Angband.
- In the fighting, the house of Finarfin was worst affected and Finrod was
  nearly killed but saved by son of his man friend, Barahir.
- For this act, he gave Barahir his own ring.
- Only a handful of the followers of Barahir survived this battle, and by the
  end of the fourth great battle of Beleriand, only one, Beren, survived. He would
  subsequently become very famous.
- When Fingolfin heard these tales, in a rage went to Angband himself, sounded
  his horn and challenged Morgoth the single combat.
- This is said to be the last time Morgoth left Angband, being taunted by the
  Fingolfin as a coward and unable to refuse.
- Despite fighting bravely against Morgoth who was armed with a great hammer
  Grond, Fingolfin did not possess the power to beat him and Morgoth knocked him
  down and stood on his neck to kill him.
- With his last breath, Fingolfin stabbed Morgoth's foot.
- The Lord of the Eagles then arrived, attacking Morgoth, scarring his face and
  carrying Fingolfin's body away, laying it to rest high on a hill above Gondolin,
  where Turgon could see it.
- Also in this battle, Sauron displayed his great power and took the citadel of
  Minas Ithil.
- Two other Men played important roles, Húrin and Huor, sons of one of the lords
  of the great houses of men.
- When in their greatest peril, they were also saved by the emissaries of the
  Lord of the Eagles who took them all the way into Gondolin, where they spent one
  year in friendship with Turgon, before being allowed to leave having sworn an
  oath not to reveal the secrets of Gondolin, many of which they did not even
  know.
- Morgoth was very interested to learn about these Men as he had yet to discover
  the whereabouts of Gondolin, nor of Nargothrond.
- Being uncertain of these things, Morgoth retreated to regroup his strength and
  although he launched another assault on Beleriand seven years later, and was
  unable to utterly destroy the Noldor.
- For his part, Turgon judged that it was not yet time to unveil Gondolin.

### of Beren and Lúthien

- Beren was son of Barahir, and last surviving a small band of Men Barahir led
  in a guerrilla campaign against Morgoth after the ruin of Beleriand.
- He re-captured his father's ring after he was killed by Orcs.
- He wandered in to Doriath and saw Lúthien: they fell in love.
- Thingol disapproved of this man so gave him an impossible task: rescue a
  Silmaril for Lúthien's hand in marriage.
- Despite being forbidden, Lúthien followed, using magic to cloak herself as she
  left.
- They encountered a band of elves, including two sons of Fëanor (Celegorm and
  Curufin) and Finrod, son of Finarfin.
- Finrod and a band agreed to journey with them, with Finrod disguising them as
  Orcs. The sons of Fëanor warned against the effort to take a Silmaril.
- Beren and Finrod's group were unmasked by Sauron and captured, being killed
  one by one by a werewolf until it came for Beren, at which time Finrod broke his
  chains and fought the wolf, with both dying.
- In this time, Lúthien, trying to pursue, had been brought before Celegorm and
  Curufin and imprisoned.
- Celegorm's hunting partner was Huan, wolf of Valinor, and he helped Lúthien
  escape.
- Lúthien and Huan stood at the bridge in front of Sauron's fortress and issued
  a challenge. One by one Huan slew all of Sauron's wolves, including their sire
  Draugluin.
- Sauron came forth as a wolf, causing Huan to falter, but Lúthien bound Sauron
  with a spell, Huan re-grouped and seized him by the throat. Sauron fled, giving
  up his tower and prisoners, including Beren.
- The three carried on their quest but were ambushed by Celegorm and Curufin.
  Beren was wounded, Huan abandoned his former master and helped them escaped,
  with Lúthien healing Beren and disguising them as a bat and wolf, allowing them
  to enter Angband unseen.
- Lúthien sang an enchantment making all fall asleep and Beren cut a Simlmaril
  from his crown. In greed he tried to cut another but his knife broke and Morgoth
  awoke.
- At the gates of Angband they encountered Carcharoth, a wolf bred to defeat
  Huan. Carcharoth bit off Beren's hand which clutched a Silmaril. The jewel
  burned him and he fled in madness.
- The eagles arrived to save Beren and Lúthien.
- Thingol assented to Beren's marrying Lúthien.
- Carcharoth's madness brought him in to Doriath and Beren and Huan hunted and
  killed him, though both were killed in return and Lúthien's grief also caused
  her to return to the halls of Mandos.
- Mandos in his pity allowed them both to return to Middle Earth, with Lúthien
  now mortal.
- They lived alone thereafter until their deaths.

### of the Fifth Battle, Nírnaeth Arnoediad

- Beren and Lúthien return to Middle Earth and dwelt together on an island,
  bearing a son but having no other significant part in Middle Earth at this time.
- Before this, Lúthien went back to her father Thingol to let him know she had
  returned and although he was happy, she could see a doom upon him and chose to
  leave.
- Melian's grief was great.
- Realising both that Morgoth was not entirely unassailable and that he would not
  stop until all was destroyed, Maedhros decided to bring together the Elves and
  Men in a great assault against him.
- In part due to the doom of the Noldor, Maedhros was unable to convince all of
  the elves, such as the sons of Finarfin, to follow him.
- Thingol did not come. The sons of Féanor had demanded he return the Silmaril
  Beren had won and had done so in threatening terms. Melian counselled that he
  should return it but due to the nature of the summons and the power of the
  Silmaril over Thingol, he chose to keep it. For this, the sons of Féanor swore
  they would kill him if they ever saw him.
- Maedhros did receive help from both Dwarves and men.
- Tidings of the summit also reached Turgon in Gondolin.
- The first attacks of Maedhros were successful in driving the Orcs from nearly
  the entire north of Beleriand however, they were mainly a negative thing as the
  forewarned Morgoth of the alliance coming against him.
- Maedhros' next plan was for a direct assault on Angband.
- The plan was for Maedhros to march directly on Angband over the great desert
  plain Anfauglith, drawing out the forces of Morgoth which would subsequently be
  attacked in the flank by Fingon, current High King of the Noldor.
- On the morning of battle, unlooked for, Turgon arrived with a force from
  Gondolin which gladdened the hearts of the elves.
- Morgoth however had his own plan. While his servants within the host of Maedhros
  delayed their march, he sent forward a large army (but in truth small fraction
  of his full strength) to taunt Fingon and bring him into the battle early. Húrin
  counselled strongly against this.
- The Elves held until a previously captured Elf was brought out by the Orcs: his
  hands, feet then head where chopped off.
- This elf's brother (Gwindor) then led an attack and in response to this, the
  remainder of the Noldor could no longer contain themselves and also attacked.
- They destroyed Morgoth's army utterly and Gwindor even advanced through the
  gates of Angband but when there, was cut off and captured.
- During Gwindor's assault, Morgoth had unleashed a large force against the
  trailing army of Fingon on the plains of Anfauglith.
- So began the battle of Unnumbered Tears.
- The next day, with defeat close for Fingon, Turgon joined the fray, having
  spared his host from the rash assault on Angband. Turgon battled his way to his
  brother's side to rescue him. In that hour, the host of Maedhros also arrived.
- Despite this briefly turning the tide, Morgoth unleashed his final terrors, the
  Balrogs and the numerous dragons, led by Glaurung.
- With one final treachery, Morgoth was successful. Men who had turned to his
  cause changed sides and assaulted Maedhros. Other treacherous Men joined the
  battle, surrounding his forces. Fate saved the sons of Féanor that day, however.
- With the aid of dwarves, the Noldor cut their way out of the battle and fled to
  Ossiriand.
- The Dwarves fought particularly bravely, being resistant to fire to an extent,
  and before he died a Dwarf Lord stabbed Glaurung in the belly, forcing the
  Dragon's retreat.
- Fingon and Turgon were at that time being assaulted by the Balrogs and Fingon
  fought Gothmog, Lord of the Balrogs, in single combat. He was defeated when
  assaulted by a second Balrog.
- The man Húrin, who had previously spent time in Gondolin, fought bravely and
  insisted that Turgon escape the battle as Gondolin was now the last hope for
  elves.
- Turgon accepted the counsel and withdrew, with his flanks being guarded by two
  of his captains, Ecthelion and Glorfindel.
- As an attempt to redress the treachery of the other men, Húrin and followers
  formed the final rearguard, protecting Turgon's retreat. This is said to be one
  of the greatest acts ever performed by men.
- Eventually, all but Húrin where killed, Morgoth wanting to take him prisoner as
  he had previously been in the hidden kingdom of Gondolin.
- The scale of defeat for the Elves was huge: no followers of Fingon ever returned
  home.
- Morgoth reasserted control over the north, now with Men at his disposal as well.
- Some Elves fled to Círdan the shipwright, but within the year, Morgoth launched
  a huge assault on them, destroying the havens.
- Some escaped by sea, including one of Fingon's sons called Gil-galad.
- Morgoth's victory over the Noldor was incomplete as he had been unable to
  capture or kill Turgon, who was now High King.
- Morgoth had feared Turgon since Valinor, where he had sensed his ruin every time
  their eyes met.
- Morgoth tried to use his prisoner Húrin to provoke Turgon, but Húrin never once
  yielded. He was set upon a stone chair at the top of the Thangorodrim and given
  the power of Morgoth's hearing and vision to watch doom befall Beleriand.
- Despite this, Húrin never once asked for mercy or death.
- Morgoth commanded his Orcs to collect all the dead Elves and their armour and
  weaponry and pile them in a mound on Anfauglith.
- However, grass grew over this mound and forever after, no creature of evil was
  able to walk on it.

### Of Túrin Turambar

- Túrin was eight when his father Húrin was captured in the battle of Unnumbered
  Tears.
- He was sent by his mother to live in secret with the Elves in Doriath.
- They only just made it, being pursued, thanks to Beleg, and Elf who became his
  friend and teacher.
- Túrin took the sword as his weapon and a dragon helm.
- After accidentally causing the death of an Elf of Doriath who scorned him for
  his mortal lineage, Túrin fled. He joined with a band of criminal outlaws.
- Beleg found them and managed to get Túrin to reject the outlaws but could not
  convince him to return to Doriath.
- The band encountered a band of outlaw dwarves, including one called Mîm, who
  offered them lodging in return for his life. Unbeknownst to Túrin, Mîm's son had
  been killed in the encounter.
- Beleg and Túrin led a guerrilla campaign against Morgoth's allies, but by
  wearing his dragon helm, Túrin was revealed to Morgoth who sent Orcs against
  them.
- These Orcs encountered Mîm first and he betrayed Túrin, leading to the deaths
  of all the other outlaws save Beleg and one other.
- Beleg pursued the Dwarves who captured Túrin, met another Elf, Gwindor and
  freed Túrin. Gwindor had been captive in Angband since Unnumbered Tears.
- On freeing him from his bonds, Beleg cut Túrin's foot with the great sword
  Anglachel. This woke the unconscious Túrin who slew Beleg in his confusion.
- Gwindor managed to calm Túrin, gave him the black sword and led him to
  Nargothrond where Gwindor he had been a lord.
- The sword was re-named Gurthang and Túrin hid his identity, becoming Mormegil
  the Blacksword of Nargothrond.
- Túrin became great and influential, convincing the Elves to reduce their
  secrecy and build a bridge out of Nargothrond, but also arrogant, ignoring
  Ulmo's advice to destroy the bridge.
- Morgoth assailed Nargothrond with Glaurung the dragon. King Orodreth was
  killed and Gwindor mortally wounded. Túrin fled, carrying Gwindor away.
- Gwindor told Túrin to save Orodreth's daughter Finduilas, who was the only one
  who could Túrin from his doom.
- Glaurung however cast a spell on him and captured Finduilas. She was killed
  before Túrin ever reached her.
- He again became a guerrilla fighter, called himself Turambar (master of doom)
  and using bow and spear rather than Gurthang.
- Túrin's mother Morwen and sister Niënor sought him (against advice from
  Doriath) but encountered Glaurung. Morwen was lost and Niënor was enchanted to
  forget her past.
- She wandered and was found by Túrin and Finduilas' grave.
- They fell in love and Niënor conceived.
- Glaurung continued to attack Túrin, now in Brethil, so he sought to ambush the
  dragon.
- He managed to find him asleep and stab him with Gurthang. The dragon's spilled
  blood caused Túrin to fall into unconsciousness and with his last words,
  Glaurung lifted his spell on Niënor.
- Realising she had married her brother, Niënor committed suicide.
- When he awoke and learned the truth, Túrin was grief-stricken and also
  committed suicide a short while later.
- He was buried on a mound and the gravestone also named his sister, though her
  body was not found.

### of the Ruin of Doriath

- Morgoth devised a scheme to discover the whereabouts of Turgon and Gondolin:
  he released Húrin.
- In Middle Earth, Húrin's reappearance was treated with suspicion and so he was
  largely shunned.
- Disconsolate, he decided to return to his old friend Turgon via the secret
  route into Gondolin.
- However, when he arrived, he found the way blocked.
- Thorondor, Lord of the Eagles, saw him and told Turgon.
- Turgon was distrustful that this may be a scheme of Morgoth.
- Later, he change his mind but Thorondor and his Eagles could no longer find
  Húrin.
- From outside Gondolin, Húrin cried a curse on Turgon for foresaking him.
- This was heard by spies of Morgoth who had been following him and so the
  location of Turgon was revealed to Morgoth.
- In his sleep, Húrin had the voice of Morwen and so he travelled to
  Brethil to find her. He found her by the graves of his children.
- They conversed through the night, with her being glad he had returned, but in
  the morning she died.
- Next, Húrin travelled to Nargothrond, which he found destroyed. The Dwarf Mîm
  stood at its gates.
- Húrin revealed his name to Mîm and said he knew who had betrayed his son
  Turin, at which Mîm repented and asked for mercy but was slain by Húrin
  nonetheless.
- Húrin took only one treasure from Nargothrond, the Nauglamír necklace.
- Húrin next journeyed towards Doriath, and was taken captive by the elves
  before he reached Thingol.
- When taken before Thingol, he was seen for who he was.
- He presented the necklace of the Dwarves with some scorn.
- Melian chastened him saying that his words were poisoned by the power of
  Morgoth and his son Túrin had been well looked after and Morwen had been told
  not to go to Nargothrond.
- On hearing this, the spell was lifted and Húrin sorrowfully gave the necklace
  of the Dwarves to Thingol.
- It is said he later committed suicide.
- Dwarves frequently visited Doriath as their skills were of use and Thingol
  asked them to combine the necklace and the Silmaril, which they did
  successfully.
- However, thy did not want to give up the necklace, saying Thingol had no claim
  to it and indeed, the Silmaril was not even his by rights either.
- Thingol responded with equal spite and bitterness.
- Enraged, the Dwarves killed him, fled but were caught and killed, other than
  two, and the Silmaril was returned to Melian.
- At the death of Thingol, the girdle of Melian was broken.
- She told Mablung to get word to Beren that the doom of Doriath was near and
  departed back to Valinor.
- Without Melian's protection, the Dwarf army which returned seeking vengeance
  was victorious and Mablung was killed.
- Beren gathered a host, including his son Dior, and ambushed the returning
  dwarves, reclaiming the Nauglamír which he subsequently gave to Lúthien.
- Dior, with his wife, two sons and daughter Elwing, went to live with the
  Sindar and struggled to reproduce the glory of Doriath.
- Later, Dior received the Nauglamír from an envoy, and knew this meant his
  father and mother were dead.
- The seven sons of Fëanor heard that a Silmaril was again out with their hands
  and led an assault in which three of their number fell but so did Dior and his
  wife. His two sons were left to starve in the forest. His daughter survived.

### of Tuor and the Fall of Gondolin

- The son of Huor was named Tuor and was born after his father's death in the
  battle of Unnumbered Tears.
- He lived with the Elves then, aged 16, was captured by Orcs and evil men.
- After three years, he escaped and launched a one-man campaign against his
  captors which lasted for years, until Ulmo decided to guide him.
- Tuor was led and cloaked on his travels by Ulmo and found the armour and
  weapons that Turgon had left behind. He took those and was subsequently spoken
  to by Ulmo.
- Ulmo told Tuor he must travel to Gondolin. Tuor also came across an elf,
  Voronwë, who was the only survivor of the various ships Turgon had sent to try
  to find Valinor in the past.
- Voronwë agreed to guide Tuor to the hidden door of Gondolin. Of note, on their
  travels they caught a sight of Túrin, Tuor's cousin.
- Because of the armour and weapons he bore, the Elves in Gondolin knew he was
  sent by Ulmo and he was brought before the king, his nephew and his daughter
  Idril, with whom Tuor immediately fell in love.
- Tuor delivered the counsel of Ulmo which was that the doom of the Noldor was
  approaching Gondolin and Turgon should abandon the city.
- For various reasons, Turgon did not want to leave Gondolin.
- One of the chief opponents of the words of Tuor was Maeglin, nephew of Turgon,
  who was in love with his cousin (Idril) and did not like Tuor and the favour he
  had gained in Gondolin.
- Having rejected the counsel of Ulmo, Turgon caused the secret way into
  Gondolin to be destroyed, which explains why Húrin found it that when he
  travelled back there having been released from Angband.
- In this way, the discovery of Gondolin was in a sense Turgon's fault.
- Idril had a great foreboding within her, however, so she created the secret
  exit from Gondolin but hid it from almost everyone.
- Idril and Tuor fell in love and, after seven years, their union was blessed by
  Turgon. This caused great envy in Maeglin, who desired and his cousin, not least
  as she was the heir to Gondolin.
- This was the second union between Elves and men.
- Their son Eärendil was born a year later.
- Despite the location of Gondolin being revealed to Morgoth by the cries of
  Húrin at the closed gate, none of his servants or spies ever successfully
  entered the realm due to the vigilance of the Eagles who watched its mountainous
  borders.
- While on a mining expedition, Maeglin was captured by Orcs and tortured by
  Morgoth until he revealed the secrets of Gondolin in exchange for his life.
- Morgoth released Maeglin so that he might help in his later assault, which
  came many years later, led by dragons, Orcs, wolves and Balrogs.
- During the battle, Ecthelion fought and killed Gothmog, lord of the Balrogs,
  but was himself slain in the process.
- Maeglin took Eärendil and Idril prisoner but Tuor fought and killed him for
  their freedom.
- They escaped via the secret passageway Idril had made. This escape was aided
  by the smoke caused by the dragons.
- The escaping party stumbled across an orc patrol, accompanied by another
  Balrog.
- Glorfindel and Thorondor fought and killed the Balrog (though Glorfindel was
  also killed.) Thanks to the remainder of the Eagles, all of the Orcs were
  killed and word of the escape did not reach Morgoth.
- Tuor and Idril led their forces southward, meeting Elwing son of Dior.
- When the fate of Gondolin became known, the son of Fingon, Gil-galad, became
  High King of the Elves of Middle Earth.
- This band of Elves were joined by those of Círdan and lived under the
  protection of Ulmo.
- Ulmo returned west and pleaded with the Valar to forgive the Noldor and aid
  them in their fight against Morgoth.
- In his old-age, Tuor built a great ship (Eärrámë) and travelled with Idril
  into the west. His fate is unknown.

### of the Voyage of Eärendil and the War of Wrath

- Of the Elves that had come together, Eärendil was master and he had great
  friendship with Círdan the shipwright.
- His wife was Elwing and they had two sons, Elrond and Elros.
- Eärendil wanted to deliver a message of apology and seek aid from the Valar in
  the west and his heart was given to the sea.
- He sought after Tuor and Idril, his parents.
- He built a great ship called Vingilótë but on all his travels, never landed in
  the west.
- Elwing did not accompany him on these voyages.
- It was his longing for her which led him always to turn back from his voyages.
- Mablung, one of the remaining sons of Féanor, learned that a Silmaril was free
  in the world and, with the oath upon him, sent word of both friendship but also
  demands for the return of the jewel.
- The demands were not met.
- The sons of Fëanor attacked and vanquished the remnants of Doriath and
  Gondolin, but two more of their number died leaving just Mablung and Maedhros
  alive of the seven sons.
- Círdan and Gil-galad were unable to arrive in time to prevent the slaughter.
- In sorrow, Elwing threw herself into the sea. Her two sons were taken captive.
  The survivors went back with Gil-galad to the island of Balar.
- Ulmo raised Elwing up from the sea and gave her the form of a giant white
  bird.
- She flew to Eärendil on his ship and regained her Elvish form.
- Maedhros actually found great love for the sons of Eärendil who he had
  captured and felt a great weight of burden from the oath and all the deeds that
  had befallen.
- Eärendil and Elwing had assumed their sons to be killed and in despair,
  Eärendil turned again to Valinor, seeking aid.
- Possibly because of the power of the Silmaril, Eärendil was finally able to
  cross the great sea.
- The Teleri gazed in wonder at the arrival of this ship and the Silmaril it
  carried.
- Eärendil forbid his fellow travellers (Elwing and three other men) from
  setting foot on that sacred place. Elwing refused, as their fates were joined.
- On most of his journey to Valinor, he found a land empty as everyone was
  gathered at a great feast. This caused him despair, thinking the Valar merely a
  myth.
- Also he thought they may have been destroyed or some other evil befallen them.
- As he was about to turn back for Ewling, he heard the voice of Eönwë, the
  Herald of Manwë, who welcomed him and asked him to come before the Valar. He
  agreed.
- He sought pardon for the Noldor, pity, mercy and help. His requests were
  granted.
- Manwë also decreed that Eärendil and Elwing, and all of their children, would
  be given leave to choose either life as an Elf or as a man/Woman.
- Elwing chose to live as an Elf and Eärendil agreed through love of her, though
  in truth his heart was that of a man.
- The great ship Vingilótë was taken by the Valar and lifted up into the
  heavens.
- Eärendil remained its captain, wearing the Silmaril on his brow.
- Elwing stayed on Aman, and befriended all seabirds and learnt flight from
  them.
- In Middle Earth, the rising of Vingilótë was seen as a new star, and Mablung
  and Maedhros were happy as they knew a Silmaril had been rescued by the Valar
  from all evils.
- Morgoth was filled with doubt at its sight.
- However, such was his pride that he did not foresee any attack on him by the
  Valar.
- The Valar prepared their attack with the Vanya and the remnants of the Noldor,
  led by Finarfin.
- The Teleri sent few, still hurt from the earlier wrongs done to them and
  seeking to avoid war.
- They did however provide ships and mariners to transport the armies across the
  sea.
- The army of Morgoth had grown so large that even the great plain of Anfauglith
  could not contain it.
- However, this did not help him and near all of his Balrogs were destroyed,
  other than those who fled. Similarly, has Orcs were nearly utterly destroyed.
- Morgoth's final device was winged dragons, never before seen.
- Such was the fury of their assault, the host of the Valar and was driven back
  for the only time.
- However, Eärendil aboard his ship Vingilótë led a massive assault with all the
  great birds of heaven, led by Thorondor.
- Eärendil himself slew the largest of these dragons, which fell upon the towers
  of Thangorodrim and destroyed them.
- Again, nearly every winged Dragon was destroyed.
- The Valar unroofed every pit of Morgoth and delved deep into Angband to
  confront him.
- Morgoth fled and sued for peace.
- He was not granted it and instead bound by the same chain which had previously
  held him and his crown was made into a collar for his neck and the Silmarils
  were taken from him.
- The assault changed the geography of the north, such was its fury.
- The Elves of Beleriand were summoned to return to Valinor.
- Despairing of the constraints of the oath, Mablung and Maedhros declared that
  the Silmarils must be given back to them.
- Manwë replied that any claim they had had gone, in part because of the evils
  they had done, none less than the slaying of Dior.
- Maedhros wished to submit and debated long with Mablung, but eventually
  submitted and they took the Silmarils by force, killing those who guided them.
- Eönwë did not permit any further slaying of Elf by Elf and the remaining sons
  of Féanor where allowed to flee, each bearing a Silmaril.
- The holy jewels however burned them.
- Mablung threw himself into chasm to rid himself of the pain, taking the
  Silmaril with him down into the depths of the earth.
- Maedhros threw his Silmaril into the sea to rid himself of the pain and
  thereafter wandered the shores, singing in lament.
- The final resting place of the Silmarils was therefore set, one in heaven with
  Eärendil, one in the deep water and one in the earth.
- The Elves who returned from Beleriand settled on Tol Eressëa, the Lonely Isle.
- The Teleri forgave the wrongs done to them in the past and the oath of Fëanor
  was laid to rest.
- Círdan and Gil-galad choose to return remain for a time in Middle Earth, as
  did Celeborn and Galadriel.
- Galadriel was the only Elf remaining of the Noldor who had fled Valinor under
  the constraints of the oath (though she herself never swore the oath.)
- Elrond and Elros also remained in Middle Earth, with one choosing to live as
  an Elf and the other as a man. From this line are the only Men with the power of
  the old world in their blood.
- Morgoth was imprisoned in the void outside the world.
- This is the end of the story of the Silmarils.

## Akallabêth - of the Fall of Númenor

- With his lies and deceit, Morgoth left seeds of his evil in Middle Earth which
  would, at certain times, grow despite his absence.
- The Valar raised an island called Númenor for Men to dwell upon, neither
  within Middle Earth or Valinor.
- Men were taught much by Eönwë and had very long lives.
- The Valar led Men to this island, which they subsequently named Númenor.
- The Men who lived there were known as the Dúnedain or Kings among men.
- The first king of the Dúnedain was Elros. He lived for 500 years, 410 years as
  king.
- The remaining Men on Middle Earth were largely those who had fought with
  Morgoth.
- Although the Men of Númenor could have overcome them in war, they had become
  peaceful with their main love being that of seafaring.
- The Men of Númenor were forbidden to sail so far west than they could not see
  the shores of their land behind them, as they were not allowed to set foot on
  the undying lands.
- The closest of these to them was the Lonely Isle, and at times Elves from here
  came to visit them, bringing them gifts.
- One of these gifts was a seedling from the white tree whose lineage went back
  to the original silver tree in Valinor.
- As they were forbidden to travel far west, they sailed to the east, and sat
  foot again on Middle Earth, where they sought to teach the Men who had been left
  behind.
- These visits were short at first as the mean of Númenor did not have a
  permanent dwelling in Middle Earth.
- Eventually, the desire to see the undying lands and to share the freedom from
  death that the Elves had proved too strong within the Númenorians.
- It is unknown whether this represented the treachery of Morgoth or not.
- The disquiet was reported back to the Valar and Manwë sent a message to men
  reminding them that their doom was granted by Ilúvatar and that just by visiting
  the undying lands, they could not hope to gain immortality.
- Manwë debated with the men, concluding that the purpose of Men has not been
  revealed even to them and that they should content themselves with the gifts
  that Ilúvatar has given them.
- Eventually, the people of Númenor split, with the majority being loyal to
  their king and their race and a minority, while remaining loyal, heeding the
  words of the Valar and remaining friendly with the elves.
- The people of Númenor feared death and their wisest were tasked with finding a
  way to return after death. Great houses were built for the dead.
- Around this time, Men first made dwellings on the westernmost shores of middle
  earth.
- These Númenorians lived like kings rather than teachers among the Men of
  Middle Earth.
- Those among them who still befriended the elves, would travel to the lands of
  Gil-galad and help in their fight against Sauron.
- Sauron grew in power at this time, building a mighty Fortress of Barad-dur in
  the land of Mordor. He hated the people of Númenor, not least for their aid to
  the Elves during the battles around the creation of the one ring.
- Of the nine rings of power that Sauron gave to men, three were given to kings
  from Númenor.
- Sauron remained hidden, fearful of the strength of Númenor, until he felt his
  time was upon him.
- Gradually, those Men who kept faith in the Valar and friendship with the elves
  moved to Middle Earth, having been moved from the west to the east coast of
  Númenor so that their interactions with the Elves of the Lonely Isle was
  decreased.
- The 24th king of Númenor was brought tidings that Sauron was asserting
  domination over Middle Earth and aimed to destroy the Númenors.
- He called himself king of Men and it is this fact that particularly grated
  with the 24th King.
- He prepared an army and marched through Middle Earth, calling upon Sauron to
  kneel before him.
- Seeing the strength of men, Sauron chose to acquiesce, sensing that victory
  through might may not be achievable but that he may yet be able to corrupt the
  men.
- It was agreed that Sauron would be held captive in Númenor, but within three
  years he was deep within the Kings secret council due to his deceptions and
  craft.
- Sauron was able to convince the king that Melkor was the true lord of the
  world and giver of power, not Ilúvatar.
- Of the small amount of remaining faithful, chief among them was Amandil, whose
  son was Elendil, whose sons were Isildur and Anárion.
- Amandil descended directly from Elros.
- Amandil was suspicious and began to grow a resistance, sensing doomed to come.
- Sauron counselled the king to destroy the white tree but he was reticent to do
  so. At great risk, Isildur snuck in and took a branch from the tree for
  safekeeping.
- Shortly after this, the king agreed to destroy the original tree.
- In its place, Sauron built a great temple. This temple was dedicated to Melkor
  and many of the faithful were sacrificed here.
- While the strength and knowledge of Men continue to increase under the
  tutelage of Sauron, their lifespan actually decreased with sickness and madness
  affecting them.
- They became more war-like.
- Ar-Pharazôn became a mighty tyrant in the Middle Earth though in truth Sauron
  was the ruler.
- As old age overtook him, Sauron realised his time had come.
- He convinced the king that he should take the undying lands from the Valar.
- Amandil learned of the plan and told his son he would act as Eärendil had done
  in the past and travel to the west to deliver a message and asked for
  forgiveness from the Valar.
- He counselled his son to prepare in secret to transport the faithful Men to
  safety.
- Elendil prepared ships, including taking the tree that had been stolen, and
  waited for a sign from his father.
- As the king prepared his mighty war fleet, numerous natural occurrences
  started to devastate Númenor, including lightning storms and earthquakes.
- Finally, he set sail into the west. He reached and set foot on the shores of
  Aman.
- He found them to be empty but Manwë had taken counsel from and lain down
  governance of this matter to Ilúvatar, who created a vast chasm in the sea which
  swallowed the fleet. The king and all his Men and you had set foot up on the
  undying lands were crushed by falling earth.
- Númenor was pulled into the chasm and Aman and the Lonely Isle were separated
  forever from Middle Earth.
- The fleet of the faithful was carried to Middle Earth by a devastating wind
  and scattered.
- The coast of Middle Earth was also changed by the great waves and earthquakes.
- Sauron was also swept into the great chasm, but he was happy as he believed
  the power of Númenor had been utterly destroyed by the Valar.
- His spirit returned to his stronghold in Mordor and in times to come, it took
  the form of a great eye.
- The Men of the Dúnedain still had their hearts set on the west and would often
  sail the seas but never again came upon the undying lands.

## of the Rings of Power and the Third Age

- After Morgoth was overthrown, Sauron repented, possibly truthfully, and Eönwë
  sentenced him to return and face judgement from the Valar.
- Sauron was reticent to face the judgement of the Valar and instead, hid in
  Middle Earth.
- At this time, the Elves of Gil-galad, including Elrond, were settled in the grey
  havens, from where they often set sail into into the west.
- The Elvish land was called Eregion and had a direct pass to Khazad-dûm; there
  was a great and profitable friendship between the Elves and Dwarves at this
  time.
- Among the Elves was a grandson of Fëanor called Celebrimbor, who was the
  greatest worker of jewels since his grandfather.
- While there was peace in this land, most of Middle Earth had descended into
  savagery since the war of wrath.
- Men multiplied in this time, many of them evil, and Sauron found these the
  easiest people is to bend to his will. He believed the Valar had again foresaken
  Middle Earth and sought to increase his power.
- He travelled the lands in a fair form but Gil-galad and Elrond did not trust his
  words even though they did not at that time realise who he was.
- Sauron played upon the desire to improve Middle Earth, claiming that with his
  help, it could be made as beautiful as the undying lands. His words were most
  gladly received in Eregion.
- At this time, smiths created the rings of power but did so under the guidance of
  Sauron, who saw an opportunity to bend Elves to his will. He had found elves
  particularly resistant to just his words.
- Sauron therefore made one ring, into which much of his power was placed as this
  power was required to gain mastery over the similarly powerful Elvish rings.
- When Sauron wore his one ring, he was able to see the minds and deeds of those
  who wore the other rings.
- However, the Elves realise this quickly and therefore removed their rings.
- This made Sauron angry and he made war upon the Elves to recover the rings; they
  fled from him and were able to save three rings, all created by Celebrimbor
  alone without any influence from Sauron.
- These three rings were given to the very wise and never used while Sauron had
  the one ring. Although the most powerful of the Elven rings, they were still
  subject to the one.
- Sauron desired them more than any other rings.
- There was constant war, Eregion on was destroyed, Celebrimbor was slain and the
  door to Moria was closed.
- In response, Elrond created the refuge of Rivendell.
- Sauron gave seven rings to Dwarves but found them hard to dominate as they
  suffer the wills of others poorly.
- In the end, an extreme desire for gold and wealth was stoked within the Dwarves,
  which in itself did enough benefit to Sauron by its evil.
- Of the seven Dwarf rings, some were destroyed and some were recovered by
  Sauron.
- He gave nine rings to men.
- Men gained great long life through the rings, however they became thralls to
  them and eventually became invisible to all except Sauron.
- Eventually, Sauron so strongly desired dominion over all of Middle Earth and
  drew all evil towards himself and allowed his Orcs to multiply.
- Many Elves fled to the west via the grey havens however Gil-galad retained great
  power in that place and, aided by Númenor, did not allow Sauron to overcome him.
- Men in the east were under the dominion of Sauron, who they viewed as both king
  and god.
- The only break in the hostilities is when Sauron willingly went to Númenor as
  hostage, seeing an opportunity to destroy that powerful civilisation from
  within.
- In his absence, the power of Gil-galad and the lands of the Elves in middle
  earth had grown.
- On return, Sauron therefore retreated to plan his next move.
- Survivors of Númenor, most importantly Elendil and his two sons, settled in
  Middle Earth.
- Anárion and Isildur were separated from their father and settled in a region
  which was subsequently called Gondor.
- The main city in the south of this region was called Osgiliath.
- They built a great tower to the east, in the mountains overlooking Mordor,
  called Minas Ithil.
- To the west, a similar tower Minas Anor was built. Isildur dwelt in Minas Ithil,
  Anárion in Minas Anor.
- They shared a throne in Osgiliath.
- Among their many creations in this time was the tower of Orthanc in the region
  of Isengard.
- The greatest of the treasures that these people had rescued from Númenor where
  the seven seeing stones and the white tree, which was placed in Minas Ithil
  given that it was Isildur who had rescued it from Númenor.
- The stones showed things distant in place and time. To most, they could only
  show things close to other stones but for the very powerful, their gaze could be
  turned to anywhere.
- It is said that Elendil could see as far as the Lonely Isle, upon which the
  master seeing stone was set.
- When Sauron went to war against Gondor, he was able to take Minas Ithil. Isildur
  was able to escape with a seedling of the white tree.
- Anárion was able to hold Osgiliath.
- Elendil and Gil-galad, who lived out with Gondor, realised Sauron would
  overthrow them all and so formed the last alliance, gathered all good people and
  things to themselves and marched upon Mordor.
- All living things gathered before the gate of Mordor for battle, being split
  between the two forces.
- The Elves were not split, fighting entirely against Sauron.
- Very few of the Dwarves came to fight on either side but the house of Durin
  provided the greatest force and fought against Sauron.
- Gil-galad and Elendil were victorious and marched into Mordor where they laid a
  seven years each upon Sauron.
- Anárion was killed during the siege.
- Eventually, Sauron himself came forth and fought Gil-galad and Elendil, killing
  them both.
- Elendil shattered his great sword underneath him when he fell.
- Sauron was also cast down in that fight and Isildur cut the one ring from his
  hand with shards of his father's sword.
- Isildur kept the ring for himself, against the council of Elrond.
- Sauron forsook his physical form again.
- Thereafter began the third age.
- Isildur planted the seedling of the white tree in Minas Anor in memory of his
  brother, where it thrived.
- Isildur then departed.
- Sauron's stronghold was destroyed but its foundations were left in place.
- No Men would dwell on that land due to the memory.
- Elves and Men became estranged after this alliance.
- Knowledge of the one ring was lost even to the very wise.
- Isildur had planned to return to his father's land in Eriador but was ambushed
  by Orcs.
- Three of his sons were killed, leaving his only heir being his youngest son who
  was with his wife in Rivendell.
- Isildur escaped using the invisibility power of the ring. He was hunted by scent
  by Orcs but while trying to escape by river the ring slipped from his finger and
  the Orcs shot him with arrows. The ring was lost, near to the misty mountains.
- Three of his party survived and eventually the shards of Narsil were delivered
  back to Imladris.
- The people of Númenor dwindled in number and stature, their realms also
  diminished and eventually they became a wandering people and their origins were
  unknown to other men.
- The Elves in Rivendell remembered however and the line of descent from Elendil
  and Isildur remained unbroken.
- For some time, the realm of Gondor prevailed but it too eventually diminished
  and the line dating back to Anárion was broken.
- Eventually, the watchtowers over Mordor were abandoned and the ringwraiths
  returned to Minas Ithil.
- It was renamed Minas Morgul, the tower of sorcery.
- Osgiliath was abandoned and fell into ruin.
- Minas Anor persisted however and was renamed Minas Tirith, the tower of guard. A
  white tower was built atop to keep watch over many lands.
- The last king was summoned to a challenge by the Lord of the Nazgûl but was
  betrayed and captured.
- Thereafter, the stewards took over the leadership of Minas Tirith. They kept the
  lands west of it safe.
- The heirs of Isildur dwelt at that time under Elrond's guidance in Rivendell.
- Círdan remained in the grey havens, shepherding any Elves who wished to return
  west.
- The Elves were able to use their rings while Sauron did not have his.
- Elrond had one and Galadriel the other but at that time, only those two and
  Círdan knew who held the third.
- The wise knew that whether the ring of Sauron was discovered by him or
  destroyed, the power of the three Elven rings and the power of the elves
  themselves would diminish and the time of Men would have to begin.
- When Sauron did eventually return, he did so in the in the forest of Mirkwood
  where Thranduil was king.
- Evil settled in the south of Mirkwood, with Thranduil keeping it at bay in the
  north.
- It was a long time before anyone discovered that the evil was the return of
  Sauron.
- At the return of Sauron, the wizards entered the world but only Círdan knew
  their true nature.
- Mithrandir/Gandalf was able to both discover Sauron and chase him from Mirkwood
  for a time, but he subsequently returned.
- Gandalf returned and rediscovered Sauron and tried to convince his peers to go
  to war against him.
- However, Saruman, head of the white council, said watchful waiting would be
  adequate. In truth, Saruman desired the ring for himself.
- Finally, Saruman agreed to attack Sauron, fearing that Sauron would find the
  ring first. The white council successfully drove Sauron from Mirkwood but he had
  foreseen their attack and prepared a retreat.
- He departed swiftly to his previous stronghold of Mordor and quickly re-
  fortified it.
- Long before they began searching for it, the one ring had actually been found.
- In the year of the white council's assault upon Sauron, it passed into the hands
  of the hobbits.
- Gandalf discovered the ring but was unsure what to do with it or from whom to
  seek help.
- He asked that the Dúnedain help him keep watch on the borders of the lands of
  the hobbits.
- Eventually, rumours of the ring reached Sauron and he began his assault on
  Middle Earth to recover it.
- After the defeat of Sauron as detailed in the Lord of the Rings, it was revealed
  that Círdan had given the third Elven ring to Gandalf to help him.
- With Sauron defeated, the power of the Elven rings also diminished and the elves
  left Middle Earth behind, with Gandalf, Elrond and Galadriel taking their place
  on the last ship which Círdan had prepared for them.

[Home](/blog "ohthreefive")
