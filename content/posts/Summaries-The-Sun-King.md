+++
         
date = "2020-01-09T09:24:03.426Z"
title = "Summaries: The Sun King"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Sun King

_David Dimbleby_

## Episode 1: The Rise of the Sun King

- An early history of Rupert Murdoch after his arrival in London in the late
  1960s.
- His main anti-establishment motivation is presented as the British
  establishment's rejection of his father when he wrote a piece blaming British
  leadership for the Gallipoli disaster in the First World War.
- His initial newspaper was the News of the World but shortly thereafter he
  bought the daily broadsheet The Sun, which was failing, and converted it into
  a tabloid to take on the Daily Mirror.
- With salacious gossip and the like, he was able to increase sales but was
  nowhere near catching the Daily Mirror until the arrival of the page 3 girl.
- Within eight years, the Sun overtook the Daily Mirror; the positions never to
  change again.
- Murdoch was always looking ahead and his next move would be the United States.

## Episode 2: The Prince of Darkness

- In 1977, having purchased the New York Post, Rupert Murdoch was battling
  against the liberal and allegedly highbrow New York audience.
- However, he perceived that the same methods which brought him success in the
  UK would work on the New York audience and, due to a serial killer who called
  himself the Son of Sam, he found his way in.
- One of his reporters, an Australian often referred to as his attack dog or The
  Prince of Darkness, found out about another shooting by this Son of Sam
  through a contact he admitted with the police.
- This man, called Steve Dunleavy, spent hours with the families of the two
  victims as they died in hospital.
- He was guilty of a lie of ommission: his green T-shirt was similar to that of
  the scrubs worn by doctors and he did not introduce himself as a reporter.
- The story was a sensation and the Post had gained Murdoch his foothold in New
  York.

## Chapter 3: The Wapping Cough

- Fast forward to 1985 and The Sun and the News of the World are the two highest
  sailing papers in Britain; Rupert Murdoch has also added the Times and Sunday
  Times to his UK portfolio.
- However, he is about to make his most radical break from the establishment.
- Not only in terms of moving out of Fleet Street but also detaching himself
  from the powerful printing trade unions.
- Printing presses were an old fashioned industrial enterprise and the trade
  unions held the power in negotiations.
- Murdoch wanted to move into computerised printing but in order to do so, had
  to undertake a massive, covert operation under the noses of the trade unions.
- He created an office in Wapping allegedly to produce a new paper called the
  London Evening Post.
- He engaged a single employee first to go to America and learn about producing
  a paper on computer and then return to the UK and put as much in place as
  possible for producing his main papers at Wapping.
- As the enterprise grew, more employees were needed and they were gradually
  siphoned out of the main offices of the Sun _et al_.
- The reason for their sudden absence was usually attributed to sick leave and
  saw the term 'the Waping cough' was born.
- Both his own employees and the trade unions were aware that these people were
  moving to Wapping but they had all been duped into believing it was to produce
  this new small evening paper and no one interjected.
- In January 1986, after a small dispute with the trade unions in which they
  refused to negotiate, Murdoch sacked all the printers in the traditional
  printing press and started production of his four papers in Wapping.
- Violent industrial action lasted a year as 5000 employees lost their jobs and
  Murdoch saved about £12 million and also established an independent power base
  for himself.
- Within a year, other major newspapers moved out of The Street.
- This new position of security allowed Muerdoch to turn his focus elsewhere,
  safe in the knowledge that his paper was under his control.

## Episode 4: Politics and Power

- The chapter starts by flashing back to 1981 and an alleged-but-never-admitted
  meeting between Rupert Murdoch and Margaret Thatcher in which he asked her to
  wave through his attempt to buy the Times and Sunday Times, giving him 37% of
  the British newspaper market.
- The competition commission had never allowed any such dominance.
- Murdoch still denies this meeting and as part of the deal to acquire The
  Times, he agreed to play no role in its running from an editorial point of
  view.
- However, just one year later, after a strong difference of opinions, the first
  editor of his Times (Harold Evans) was effectively forced to resign.
- Harry remains bitter and resentful towards Rupert Murdoch to this day: he sees
  him as a deplorable human.
- In the late 1990s, Murdoch saw a shift in Britain towards the left and
  appointed a new Sun editor who had previously been a Labour voter, David
  Yelland.
- Among the first people he met where the Blairs.
- This editor describes frequent and candid phone calls from Tony Blair during
  his time in office which were designed to test what the Sun's response to the
  governments actions would be before they carried them out.
- This editor was eventually replaced by Rebecca Brooks, who practised rather
  adversarial journalism and was present when the paper shifted back away from
  Labour.
- One of Brooks' Sun's most famous campaigns was to effectively campaign for and
  remove the head of a local children's authority over the baby P scandal.
- At the Levinson enquiry, Rebecca Brooks denied ever phoning then children
  secretary Ed Balls to tell him to remove the departmental head.

## Episode 5: Hacked

- The chief characters in this episode are Mark Lewis, a lawyer one of whose
  clients was named as a victim of the first hacks in 2006, Dan Evans, a private
  investigator who hacked phones initially for the Daily Mirror but was then
  courted over to the news of the world, he repented and gave evidence in
  response for lenient sentencing, and Nick Davies, a journalist who had been
  inspired by the Watergate scandal and who was involved in the hacking stories
  from 2006 up to the closure of the News of the World in 2011.
- After the publication of the Guardian's main 2011 story about the hacking of
  Milly Dowler's phone, he received a phone call from Carl Bernstein telling him
  he had done good work.
- In 2006, Prince William left a voicemail saying he had hurt his knee and might
  have to see a doctor.
- A week later, the News of the World's royal correspondent Clive Goodman wrote
  that Prince William had seen a doctor – in fact the knee got better by itself
  and he never sought medical attention.
- Buckingham Palace security figured out that the voicemail might be the source
  of the leak, informed the Metropolitan Police and after an investigation in
  which the private investigator Glenn Mulcair was identified as the hacker,
  Mulcair and Goodman were imprisoned, the News of the World apologised for the
  behaviour of this, "One rogue," reporter and apologised to the eight victims.
- Both Mark Lewis and Nick Davies saw peculiarity – at least three of the eight
  victims had nothing to do with the royal family, one being a client of Lewis.
- He applied to the courts to receive the evidence against Clive Goodman,
  pursued and won a huge – £1 million when the usual settlement was £20-£30,000
  – settlement for his client.
- He did not perceive the significance of this unusually large sum.
- In the next couple of years, Davies published a book about the methods used by
  tabloid newspapers and came under attack from tabloid editors.
- In 2009, Dan Evans, who had stopped hacking after the 2006 incident, was down
  on his luck, hungover and so tried to hack a phone.
- His attempt failed - unusually for him - and a while thereafter Vodafone
  alerted the News of the World of that they had identified suspicious behaviour
  and the News of the World quietly removed Evans.
- Eventually, Nick Davies managed to find and pick up the story and contacted
  Evans.
- After some more time, the hacking of Milly Dowler's mobile phone was
  discovered.
- This gave the story enormous weight and it was published by the Guardian in
  2011, leading to Bernstein's phone call to congratulate Davies on his work.
- The Levinson enquiry was set up to investigate the press/journalism practises
  and Rupert Murdoch voluntarily shut down the News of the World.
- There was also a Parliamentary select committee set up and, when Murdoch was
  giving evidence, he was attacked by a member of the gallery with a paper plate
  of shaving foam.
- Despite all the good that had been done, Nick Davies thought this was a
  devastating act, allowing Murdoch to play the victim.

## Episode 6: The Alternative News Network

- Rupert Murdoch set up Fox News, headed by Roger Ailes, a previous Republican
  political aide, as he saw the same gap in serving American culture that he saw
  in Britain.
- He wanted a news network that went against the liberal elite, against CBS and
  CNN.
- Like his newspapers, Fox News would prioritise opinion and gossip over truth
  however, Ailes realised, they would have to publicly present themselves as the
  truth that the liberal news was not telling America - the _real_ news.
- Fox News overtook CNN in 2003 but its most influential period is now.
- The level of influence Rupert Murdoch, who has taken over the running of Fox
  News after Roger Ailes resigned amid sexual misconduct allegations, has on the
  Donald Trump presidency is probably greater than any he ever had over a
  British prime minister.

[Home](/blog "ohthreefive")