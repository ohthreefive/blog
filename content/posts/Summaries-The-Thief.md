+++

date = "2019-04-07T20:36:12Z"
title = "Summaries: The Thief"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summaries" ]

+++

# The Thief

**Megan Whalen Turner**

## Chapter 1

- A thief who has boasted widely about his abilities is briefly released from
  his prison cell for a discussion with the King and one of his chief advisors,
  the Magus.
- They want him to steal something for them.
- The thief previous bragged about stealing the King's seal openly - hence his
  imprisonment.

## Chapter 2

- The next morning, the thief is awoken, rather sooner than expected, to be
  taken on this journey to steal something.
- His party includes the Magus, a skilled soldier (of sorts) called Pol and two
  men who the thief refers to as useless the elder and useless the younger.
- On the journey, Pol cleans and dresses the wounds picked up by the thief
  during his time in prison.

## Chapter 3

- The journey continues, venturing further out of civilisation towards the
  wilderness.
- We learn that the two uselesses are apprentices of the Magus's.
- The elder is called Ambiades and appears to be a social snob.
- The younger is called Sophos and is apparently the son of a Duke.
- He is more studious and diligent and appears to take more of a liking to the
  thief.
- The thief displays a natural rapport with the common people that they meet,
  which his companions do not share.

## Chapter 4

- An exposition heavy chapter.
- The thief is named Gen.
- Three kingdoms are described.
- Sounis is where our protagonists are from.
- Attolia are their enemies of sorts.
- Both of these kingdoms have previously been conquered by invaders.
- Eddis is a much poorer kingdom which lies in between these two. It relies
  on trade.
- It has strategic importance in that for one of the other kingdoms to invade
  the other, they would have to pass through Eddis via a narrow passage.
- Therefore, Eddis is important for peace between the other kingdoms.
- In the mythology of Eddis, there was a mythical stone which granted its
  bearer immortality but only if it was given freely (rather than taken.)
- This stone (Hamaithes's Gift) was lost at the time of the invasion.
- The Magus believes he has found the stone but thinks he needs the help of a
  master thief to recover it as he believes there have been previous failed
  attempts to recover it.
- With the stone, the king of Sounis could make an un-refusable marriage
  offer to the Queen of Eddis.
- The thief correctly theorises that in truth, the king of Sounis is
  seeking a way to attack Attolia and needs Eddis to do this.
- At end of chapter, Sophos reveals his ignorance of these old mythologies as
  his father thought they were not appropriate stories to learn.
- The Magus decides he will teach Sophos these stories.
- Something about Sophos's father's assertion that these stories are not for him
  to learn disturbs Ambiades and Pol – the thief sees it but does not
  understand why they are put out.

## Chapter 5

- The Magus tells a creation myth but Gen says he has told it wrong.
- The Magus counters that he has studied the myth and is confident his version
  is more accurate.
- Gen's version, told to him by his mother, is more likely an inaccurate version
  of the story that has gradually changed with time.
- The Magus reminds Gen that he knows about his mother and knows that his own
  name is probably short for Eugenides, which is also the name of the god of
  thieves.
- Gen's mother was a thief and died after a fall; his father is a soldier and
  alive.
- Gen is annoyed that the Magus got the better of him.
- Gen tells the story of Eugenides and how a war between the Sky and Earth, two
  of the first gods, led people to worship both of them.
- The Magus tries to prevent Gen and Sophos talking together much.
- Gen's strength is coming back.
- He realises his companions are watching him in shifts as he sleeps.

## Chapter 6

- The group continue their journey to Attolia via Eddis.
- Gen continues to antagonise Ambiades and successfully gets him to attack him.
- He later learns is that although Ambiades has a well-known grandfather, his
  family was disgraced and he is essentially as poor as the thief.
- At the end of the chapter, and Magus continues telling mythology.
- After their battle, Earth and Sky agreed to give up some of their powers –
  Earth give up her ability to shake the ground and the Sky was to give up his
  thunderbolts; but he disappeared without doing so.
- The Earth therefore tasked Eugenides with stealing them.
- He managed this and gave them to his sister: she was to be the keeper of both
  the Earth and Sky's gifts.
- The Sky confronted Eugenides and made him disclose where the thunderbolts were
  in return for granting Eugenides immortality.
- The Sky did this but could not re-claim his thunderbolts as he was held to his
  promise to give them up.
- Eugenides's sister was thereafter the most powerful of the gods other than the
  first gods, being now the keeper of one each of Earth's and Sky's powers.

## Chapter 7

- The chapter begins with the Magus accusing and then beating Gen far
  apparently stealing some food.
- Gen viciously protest his innocence.
- Not long afterwards, Ambiades ties Gen's hands while he is left alone,
  however the restraints are too tight and Gen suffers severe pain and the
  bitterness he feels towards Ambiades is amplified by this.
- We learn that Pol is the captain of Sophos's father's personal guard and is
  on the mission to protect him.
- The band enter a region known as the Dystopia - a volcanic waste land which is
  woven into the myth of Eugenides.
- The Magus tells Gen that he will have to perform his talents here.

## Chapter 8

- Gen has had a couple of dreams in which he speaks to a woman in a white room
  about stealing something.
- Gen tells more of the mythology: the mortal brother of Eugenides, jealous of
  him, conspires with the Sky to trick Eugenides into using a thunderbolt.
- This starts a fire which ultimately kills Eugenides's brother and burns him
  black.
- Seeing this, Hephestia goes to search of the thunderbolts but is deliberately
  misled.
- A river God, son of the Sky, carries Eugenides and the thunderbolts away.
- When this river God encounters a river goddess, daughter of the Earth, they
  fight over the thunderbolts.
- The target of Gen's robbery is a temple which is usually submerged by the
  river.
- The Magus has led them to its entrance, which is only usable a few nights of
  the year and only for a few hours at a time.
- Gen enters and finds a deadly maze which he tries to navigate.
- During this, we learn he has stolen multiple items from his companions and
  other passers-by on the journey so far which use this to help him.
- Ultimately, he is unsuccessful as the temple does not have the traditional
  rooms in which hidden treasure would be kept.
- He manages to get out of the maze before it becomes flooded again.
- In the morning and find the Magus is relatively welcoming towards him despite
  failure: he can try again the next night.

## Chapter 9

- Gen goes into the maze for a second night but has no further success.
- Before entering a third time, he and the Magus discuss the map he has drawn of
  the maze.
- On his third night in the maze, again finding no door, Gen stares at a huge
  slab of obsidian and partly in frustration and partly in curiosity, hits it
  with his pry-bar.
- It begins to crack and eventually Gen is able to break the whole slab,
  revealing a staircase.
- At the top of the staircase is a room containing the gods; the actual gods.
- Hephestia sits with Hamaithes' Gift on a tray in her lap.
- All of the gords are motionless other than Eugenides, who tells Gen to take
  the stone.
- He does so and starts to leave in a hurry as the river starts to flood the
  maze again.
- Before he can reach the end of the maze, he slips and is pulled underwater.

## Chapter 10

- The Magus is very pleased with Gen and the group head home.
- They are set upon by Queen's guards of Attolia and in the fight, Hemiathes'
  Gift is lost in a stream.
- In pursuit, the Magus sends Gen to steal food and then horses for them.
- Gen is surprised when, while stealing the horses, his silent prayer to the
  gods that the horses make no noise is answered and the horses are truly
  silent.
- Gen is unsurprisingly shaken by this divine intervention.
- The group head for the mountain pass with more of the Queen's guard in
  pursuit.
- When they reach it, Gen argues to be left behind as he wishes to hide rather
  than run.
- However, when the Queen's guard overtake him, he attacks them and is
  surrounded.

## Chapter 11

- Gen, the Magus and Sophos are prisoners.
- Ambiades was a spy for Attolia and had to sold them out.
- On hearing this, Pol killed Ambiades and was killed in turn for this.
- The Magus apologises to Gen and says it will be difficult for him to convince
  the Queen that Hamaithes' Gift has been lost – Ambiades was the only
  reliable source for them.
- The Magus also tells Gen that the temple in the Dystopia has been destroyed
  by the river since they recovered the gift.
- In the fight before being captured, Gen learns he instinctively killed one of
  his attackers, an act by which he is horrified in retrospect.
- He was also seriously wounded himself.
- The Queen of Attolia offers Gen to work for her.
- He declines and instead breaks out, also breaking Sophos and the Magus out.
- During their flight, the Magus explains that Attolia, Eddis and Sounia are
  only free because a civilisation known as the Mede are busy fighting the
  merchant navy but when this finishes, they will come to conquer them.
- The only hope is for the three kingdoms to unite.
- The three fugitives make a brave escape, but suddenly come upon soldiers
  waiting for them.

## Chapter 12

- The group are now in Eddis.
- They are brought in front of the Queen who reveals Gen to be *her* thief and
  his real name is truly Eugenides.
- He deliberately entered Sounis as he was seeking Hamaithes' Gift
  for Eddis.
- When he found no evidence of it after breaking into the Magus's chambers, he
  talked up his reputation, stole the king's seal ring then openly bragged about
  it in order to be imprisoned but also to bring himself and his abilities to
  the attention of the Magus.
- His plan had been to follow the Magus and steal the Gift from him but in the
  end he was chosen to accompany the Magus, which was another less likely plan
  of his.
- Eugenides is the son of the War Minister in Eddis and had actually previously
  met the Magus.
- The Magus did not realise Eugenides was from Eddis until after they had met
  the guards and saw their reaction to him but had some suspicions earlier,
  particularly when the makeshift bridge across the river *appeared* at their
  moment of greatest need.
- Sophos was completely unaware.
- Pol had realised he was from Eddis when he saw him fighting with a sword
  in the style of that place.
- Eugenides was also convinced that Ambiades knew who you was: they were both
  deceiving others.
- The Queen of Attolia knew who he was when they captured him and she had sent
  a message to the Queen of Eddis saying that she hoped she could entertain
  herself for a much longer time in future.
- The Magus resigns himself to accepting that he has been outplayed by Eugenides
  and appears to harbour him no malign intent.
- Sophos is revealed to be the king's nephew and heir.
- He is returned to Sounis with some concessions being gained for
  Eddis.
- The Queen of Eddis (and cousin of Eugenides) thanks him for his work and asks
  him to write down his story.

[Home](/blog "ohthreefive")
