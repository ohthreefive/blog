+++
date = "2020-10-30T17:52:13.325Z"
title = "Summaries: Why I’m No Longer Talking To White People About Race"
description = "A summary to help me remember what I read better"
tags = [ "Audible", "summary" ]

+++

# Why I'm No Longer Talking to White People About Race

_Reni Eddo-Lodge_

## Preface

- The author also narrates the book.
- She traces its origin back to a synonymous blog post in 2014.
- She explains her reasoning as being the fact that whether she was speaking to
  white racists or liberals, she felt like she was talking to someone who still
  had an inherent racism.
- She has somewhat ironically spent the last several years talking to white
  people about race as the publishing industry is white-dominated and she spent
  years working on the book.
- Overall, she feels that every piece of anti racist work has merit.

## Chapter 1: Histories

- Various stories about racism in Britain are recounted.
- Emphasis is put on the fact that British participation in the slave trade
  lasted longer than the time that has passed since abolition.
- Most of the chapter is dedicated to 20th Century British racism, particularly
  post-World War II.

## Chapter 2: The System

- The chapter deals with institutional racism or, as the author prefers,
  structural racism.
- She prefers the latter term as it does not limit racism to only established
  institutions.
- The chapter ends with attempts to counteract structural racism and the racist
  responses to them – namely complaints about positive discrimination, tokenism
  and minimum numbers.
- The author is keen to point out that these responses are just another form of
  racism: white people who believe in meritocracy usually fail to grasp the fact
  that the white-dominated world has not come about because of the meritocracy
  they uphold but instead because of the effects of longstanding structural
  racism.

## Chapter 3: What Is White Privilege?

- The author is keen to point out that she does not equate white privilege with
  wealth or even power.
- It is more endemic and more difficult to root out.
- She draws a very useful comparison when drawing attention to white people's
  blindness to white privilege.
- On a frequent commute where she had to take a bicycle, she suddenly became
  aware of the complete lack of accessibility around the London transport
  network.
- As she says, until she had to move her wheels around, she had no idea how much
  the world was set up against those had no choice but to do so.

## Chapter 4: Fear of a Black Planet

- The chapter begins with Enoch Powell's 1968 speech and moves on to an
  interview between the author and Nick Griffin to avoid any claim of defamation
  after publishing the book.
- The interview is published as a transcript and Griffin repeatedly expresses
  racist and fearful language and opinions.
- The chapter moves on to a discussion about the lack of black role models in
  media and culture, highlighting controversy about Idris Elba being considered
  as the new James Bond and whether or not Hermione Granger could be a black
  character.
- The author is keen to point out simple contradictions: if there is no
  disadvantage in being anything other than white, why should there be any fear
  or mistrust of black role models?
- The author widely discusses her opinions on free speech.
- Free speech is not the right to openly express bigoted views.
- It is the right of everyone to open openly argue about such views.
- Racists who use free speech to spread their opinions should expect their
  opinions to be openly and strongly challenged.
- The author also expresses the simple and sensible suggestion that liberal
  defence of immigration and multiculturalism should not focus on the positive
  impact immigrants bring but rather should directly contradict the aggressive
  anti-immigration stances of racists with aggressive pro-immigration positions.

## Chapter 5: The Question of Feminism

- This long chapter starts by discussing the failure of the feminist movement to
  recognise that racism cannot be a separate issue from feminism, continues
  through discussing the backlash black feminists, the author included, have
  faced by forcing race into feminist discussion and concludes with a powerful
  feminist and anti-racist message.
- It is something of a tour de force and the most impactful chapter yet.

## Chapter 6: Race and Class

- The author is keen to point out that racism transcends class structures.
- There is a big difference between the so-called white working class, a phrase
  which the author believes to be racist, pointing to its use by Nick Griffin,
  and a black working class.
- More detailed class research has shown that many white people who identify as
  working-class have significantly higher incomes than black people.
- The author highlights a single example of harm: a housing scheme in London
  which aimed to provide 'affordable housing' to the working class actually
  provided houses to buy in the price range of white people and a very small
  number of houses to rent in the price range of black people.
- There is again a misplaced fear that immigrants or non-white British people
  may use up the resources which lower class white people need to climb the
  ladder.
- There is no evidence for this.

## Chapter 7: There Is No Justice, There Is Just Us

- The book ends with a neat summation.
- The author describes racism as a white problem.
- She describes how non-white people rarely ask for advice on how to fight
  racism in the way that white people do.
- Commenting on the title of the book, she says that many white people wallow in
  guilt and self pity when they realise the benefits of their skin colour;
  instead they should get angry as that anger will be required to fight the
  embedded prejudices.

[Home](/blog "ohthreefive")