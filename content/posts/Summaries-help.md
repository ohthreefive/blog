+++

date = "2019-02-17T20:45:06Z"
title = "Summaries help"
description = "I think writing summaries is really helping my recall of novels"
tags = [ "random", "summaries" ]

+++

I decided to start writing summaries in late September. Some early observations:

- It can be easy to misjudge the amount of detail to record, especially for non-
  fiction.
- For fiction, summaries get easier to write as the story progresses, probably
  because I get a better sense of what is important to remember as the plot
  develops.
- These summaries have been really beneficial, but more so for fiction than non-
  fiction so far. I can recall more of the plot of the fiction I’ve read but the
  non-fiction remains somewhat elusive. This is probably unsurprising as it’s a
  little harder to grasp and hold facts when listening compared to reading when
  you can stop, look and absorb.
- My reading has been *really* slowed down, and while I appreciate this, I have
  just finished a 29 hour listen and it was a slog at times!

[Home](/blog "ohthreefive")
