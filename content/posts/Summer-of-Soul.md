+++
         
date = "2022-01-10T10:09:42.245Z"
title = "Summer of Soul"
description = "Brief movie reviews #58"
tags = [ "movie" ]

+++

Fantastic documentary which just made me wish I was there.
Unforgettable.

4/5

[Home](/blog "ohthreefive")