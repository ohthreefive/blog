+++
date = "2023-10-12T12:26:34"
title = "Teenage Mutant Ninja Turtles: Mutant Mayhem"
description = "Brief movie reviews #81"
tags = ["movie"]

+++

Incredible visuals: among this, Puss in Boots, Elemental and, of course, the Spider-Verse movies, the bar for animation is just so high. The trailer for the Paw Patrol movie at the start looked old-fashioned in comparison.

This was more enjoyable than it had any right to be, with its rather generic and basic plot only becoming a hindrance towards the very end. Good stuff.

3/5

[Home](/blog "ohthreefive")