+++
date = "2020-09-05T09:51:22.215Z"
title = "Tenet"
description = "Brief movie reviews #46"
tags = [ "movie" ]

+++

Taking a high concept premise like that Inception but raising it ever higher and
making it even less easy to understand, Tenet still succeeds because it
encourages the viewer to 'just go with it' and enjoy it.

Which is just as well, because come the final act, I had no idea exactly why or
what was happening on screen was happening, but I definitely knew that the good
guys had to beat the bad guys and that I really wanted them to!

5/5

[Home](/blog "ohthreefive")