+++

date = "2017-03-20T12:16:30Z"
title = "The Accountant"
description = "Brief movie reviews #1"
tags = [ "movie" ]

+++

I rented The Accountant over the weekend. It seemed to have the same positive
feelings around it that John Wick had and I loved John Wick!

Sadly, it's average. Affleck is great and the support players (Kendrick, Lithgow
etc) make it look easy. But the plot is too easily telegraphed (e.g. where is
brother!?)

The action/violence aims for the same realistic tone as John Wick but completely
falls short of both the style and brutality of it.

Before this, John Wick was the last movie which I saw because everyone said it
was great. And they were right. This time though, they were wrong.

2/5
