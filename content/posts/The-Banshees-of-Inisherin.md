+++
         
date = "2023-01-21T20:51:03.235Z"
title = "The Banshees of Inisherin"
description = "Brief movie reviews #73"
tags = [ "movie" ]

+++

St⭐️r really is the star of Disney+, with excellent movies throughout.

Banshees was such a bizarre, confounding and gripping movie. Colin Farrell
deserves everything he gets, and the rest of the cast are equally as good.
I just loved it.

5/5

[Home](/blog "ohthreefive")