+++

title= "The Big Short"
date= "2018-09-23T23:27:24Z"
tags= ["movie"]
description= "Brief movie reviews #25"

+++

Fascinating look in to banking complacency and shamelessness in the lead up to
the 2008 crash.

Undercuts itself with its show-y asides (eg Margot Robbie in a bubble bath.)
These were presumably meant to be ironic but they fell flat.

4/5

[Home](/blog "ohthreefive")
