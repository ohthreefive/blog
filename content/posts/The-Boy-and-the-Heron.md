+++
date = "2024-03-05T14:43:12Z"
title = "The Boy and the Heron"
description = "Brief movie reviews #84"
tags = ["movie"]

+++

Finally, I see a Miyazaki in the cinema! And it was no let down. Gloriously
beautiful, strange, magical, unique. I feel lucky.

5/5

[Home](/blog "ohthreefive")