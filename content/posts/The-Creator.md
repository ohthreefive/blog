+++
date = "2023-10-12T12:29:41"
title = "The Creator"
description = "Brief movie reviews #82"
tags = ["movie"]

+++

Sci-fi visuals that put more expensive experiences to shame - $80 vs $212 million for Marvel’s Secret Invasion, according to reports. There are also some great ideas.

Sadly, it’s all put together in a rather incomprehensible manner. More time on story and script and this could have been a great.

2/5

[Home](/blog "ohthreefive")