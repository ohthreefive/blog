+++
date = "2022-01-10T10:05:44.652Z"
title = "The Green Knight"
description = "Brief movement reviews #55"
tags = [ "movie" ]

+++

I think I could watch Dev Patel do anything for 2 hours. Delightfully surreal.

4/5

[Home](/blog "ohthreefive")