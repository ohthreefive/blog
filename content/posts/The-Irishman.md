+++
date = "2020-02-23T09:48:15.896Z"
title = "The Irishman"
description = "Brief movie reviews #42"
tags = [ "movie" ]

+++

Confidently slow-paced and three-and-a-half hours long - so how did this movie
seem to disappear? Pesci & De Niro at their best.

5/5

[Home](/blog "ohthreefive")