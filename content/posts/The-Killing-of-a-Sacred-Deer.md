+++

date = "2018-09-25T20:28:01Z"
title = "The Killing of a Sacred Deer"
description = "Brief movie reviews #26"
tags = [ "movie" ]

+++

Deeply, deeply unsettling from start to finish, the main issue I have with the
movie was the utter lack of any sense of meaning. One of those movies that’s
worth seeing, but certainly not for the feel-good factor or the personal growth.

4/5

[Home](/blog "ohthreefive")
