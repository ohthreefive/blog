+++
date = "2022-05-04T07:11:35.383Z"
title = "The Northman"
description = "Brief movie reviews #62"
tags = [ "movie" ]

+++

While it doesn't deserve the banner, "This generation's Gladiator," on the
poster, this was an excellent and brutal piece of film-making. Skarsgård Did not
skip traps day, but definitely lacks Crowe's brooding charisma. A powerful,
near-wordless finale left the audience silent as the credits rolled. It's been a
while since I've experienced a movie which has achieved that.

4/5

[Home](/blog "ohthreefive")