+++
date = "2019-12-28T08:26:40.628Z"
title = "Star Wars: The Rise of Skywalker"
description = "brief movie reviews #39"
tags = [ "movie" ]

+++

The first two acts were fast-paced enough to make up for the lack of real
character interaction, but the final act was just a sprint through bad choice
after bad choice. A real disappointment.

2/5

[Home](/blog "ohthreefive")