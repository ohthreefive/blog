+++
date = "2022-11-19T07:49:58.731Z"
title = "The Unbearable Weight of Massive Talent"
description = "Brief movie reviews #71"
tags = [ "Movie" ]

+++

Hats of to Nicholas Cage for an outstandingly humble and self-effacing turn.
Despite foreshadowing the turn from character-driven to action-led movie, it was
disappointing when the plot turned away from the Cage-Pascal bromance.

4/5

[Home](/blog "ohthreefive")