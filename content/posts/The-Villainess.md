+++

date = "2017-09-24T08:42:45Z"
title = "The Villainess"
description = "Brief movie reviews #11"
tags = [ "movie" ]

+++

I am unashamedly an action fan, and moreover of the martial arts flick. The Raid and its sequel are my recent high. So that’s six years without my socks being truly blown off by action!

Reviews for the Villainess seemed to agree that the action was breath-taking and the plotting rather thick. I can happily forgive the latter if the former is up to scratch (in fact, given I tend to re-watch action pics, I like the reward of an overly complicated plot getting less opaque on repeat viewings.)

There’s no doubt that the stitching of frames to make it look like the action scenes are cut-free lends them immediacy and stamina, but the action itself lacks the choreography of the Raid. And, to an extent, the confidence: the camera shakes all over the place as if the try to disorientate you, or as if to distract from what’s on screen. Much in the Raid (or the John Wicks) just points a camera and lets the action unfold from a nice clear vantage point.

Which is a shame, because the rest of the film (confusing plot and timeline included) is excellent! Don’t get me wrong, the action in the Villainess puts most Hollywood fare to shame, but it falls short of my modern benchmark.

3/5

[Home](/blog "ohthreefive")
