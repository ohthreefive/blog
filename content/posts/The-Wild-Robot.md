+++
date = "2025-01-14T18:19:21Z"
title = "The Wild Robot"
description = "Brief movie reviews #92"
tags = ["movie"]

+++

My local small cinema often gets reels after they've left multiplexes, so I got a second chance with this one. Like Inside Out, parents and children are watching a different movie play out. it really is fantastic, especially for the first almost hour, then just dips in quality a little. But still, excellent stuff.

4/5

[Home](/blog "ohthreefive")