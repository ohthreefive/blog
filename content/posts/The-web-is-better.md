+++
         
date = "2019-06-12T04:50:10.334Z"
title = "The web is better when half your phone screen isn’t taken up by a manipulative request to give up personal information for no obvious gain"
description = "why do I need to sign in to read the news?"
tags = [ "random" ]

+++

![The BBC homepage now has a huge banner requesting login](/images/bbc.png "Screenshot of bbc website")

[Home](/blog "ohthreefive")