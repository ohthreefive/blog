+++
         
date = "2022-09-17T19:48:17.117Z"
title = "Thor: Love and Thunder"
description = "Brief movie reviews #68"
tags = [ "movie" ]

+++

It's nearly five years since Ragnarok, which started the decline in the MCU in
my eyes, despite the highs of Infinity War/Endgame.

Love and Thunder leans heavily on that. Taika's irreverence is allowed to run
far too free. The re-introduction to Thor and the Guardians is, frankly, stupid,
and could have been handled better. And quicker. And with more emotional beats.

As a father, I found Gorr's introduction hard to watch, and immediately
sympathised with him as a character. In fact, he barely puts a foot wrong in the
whole movie and, wisely, never cracks wise.

Hemsworth also puts everything into his Thor. He has to swing wildly from comedy
to emotion and he is more up to the task. An unkillable man-child who has no
parents to guide him and who seems to lose everyone around him - no wonder he is
such a mess.

From Gorr's attack on New Asgard up until the climax, I think this has the
makings of an excellent Marvel movie. If most of the stupidity had been omitted,
perhaps allowing Zeus to be the only representation of the unlovable god, then
this would have been leaner in run time and heavier in substance.

Another MCU disappointment.

2/5

[Home](/blog "ohthreefive")