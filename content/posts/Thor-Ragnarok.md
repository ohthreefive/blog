+++

date = "2017-11-04T19:34:51Z"
title = "Thor: Ragnarok"
description = "Brief movie reviews #14"
tags = [ "movie" ]

+++

Often hilarious and always deliberately, Thor 3 is one of the most fun trips
I’ve had to the cinema in memory. I was also impressed at the permanent changes
to the Marvel universe the movie has made.

Disappointments include the villain, Hela. Introduced as supremely powerful and
(unsurprisingly) dispatched by the end of the movie. She felt rather like a
brief but necessary evil only, all too shallow. Shallow could probably describe
the movie overall, actually.

Spider-Man homecoming is the only MCU movie I haven’t seen yet, but they’re
beginning to feel a bit safe and serialised, and I’m saying that having just
seen a genuine mould-breaking movie! Infinity War had better be something
special!

As a final aside, how poorly defined are the powers of our heroes? Is Dr Strange
really able to deal with both Thor & Loki so easily? Is Hulk ever going to beat
another Avenger in a fight? Is Thor always going to thunder-up in fights from
now on? And if not, why not? It’s all a bit confusing!

3/5 

[Home](/blog "ohthreefive")
