+++
date = "2022-05-31T08:25:29.078Z"
title = "Top Gun: Maverick"
description = "Brief movie reviews #64"
tags = [ "movie" ]

+++

A little bit of re-make, a lot of sequel. Confident and over-competent, like its
title character. With this and Fallout, Cruise and McQuarrie are teaching
studios how to make big movies well. Credit to Kosinski as well. Everyone in the
screening seemed to love it.

5/5

[Home](/blog "ohthreefive")