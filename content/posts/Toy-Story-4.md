+++
date = "2019-07-07T06:53:57.025Z"
title = "Toy Story 4"
description = "Brief movie reviews #32"
tags = [ "movie" ]

+++

The Toy Story trilogy - as it will always be known - will always be Andy's
story. Toy Story 4 is Woody's story which, as well as being detrimental to Buzz
Sidequest, makes the emotional punch of this fourth edition lesser. Woody is
just a character, albeit the main character. Andy was more than a character.
Andy was us. However, this remains better than most animations or other movies
out there and had plenty to entertain the little ones.

4/5

[Home](/blog "ohthreefive")