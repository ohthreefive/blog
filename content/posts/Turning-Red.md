+++
date = "2022-03-19T08:07:44.210Z"
title = "Turning Red"
description = "Brief movie reviews #59"
tags = [ "movie" ]

+++

Pixar's least adherent movie to the highly successful, "Let's go on a quest,"
style that has served them so well, and it's all the better for it. Tremendous.

4/5

[Home](/blog "ohthreefive")