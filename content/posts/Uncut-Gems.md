+++
date = "2020-02-22T12:19:45.610Z"
title = "Uncut Gems"
description = "Brief movie reviews #40"
tags = [ "movie" ]

+++

A boiler room of tension; less depraved and physically intimidating than Bad
Lieutenant but no less excruciating.

4/5

[Home](/blog "ohthreefive")