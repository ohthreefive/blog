+++

date = "2018-03-10T09:58:36Z"
title = "Updating Hugo GitLab ci"
description = "I've always struggled with the .gitlab-ci.yml file. Hopefully no more!"
tags = [ "meta", "linux" ]

+++

Skip this first section to get to the useful information!

### BACKGROUND

I started [my website][1] about a year ago, using the Hugo static site
generator and GitLab Pages.

I had abandoned my self-hosting efforts due to (lack-of-knowledge-based) issues
with dynamic DNS and my desire to use https. It was beyond me.

The Hugo and GitLab Pages documentation are really very good. Good enough for a
novice to follow along, but not quite good enough for me to be able to
troubleshoot things that went wrong. Most often, if something went wrong, I
didn’t understand well enough what had gone wrong and why, so finding a fix was
(still is) ... troublesome!

I still haven’t got my head around the `.gitlab-ci.yml` file. I copied the one
from the example provided by the GitLab Pages example site, but whenever I
wanted to update the Hugo version, 48 hours of `git commit`s, `git revert`s and
general fury usually occurred. Sometimes I worked it out, sometimes I rolled
back to the last working version.

It had been about three months, and I decided it was time to take on the
challenge again!

### COPY AND PASTE

I headed straight for a search on updating the file and found [this really
useful post][2].

Rather than set the Hugo version and the `sha256sum` every time, set them as
variables which can be called later in the file. Clever! It means all I *should*
have to change in future are those variables. Thanks, [Achilleas][3]!

I’ve reproduced my file below. It’s also available on the link above.

{{< highlight yaml >}}
image: alpine

variables:
        HUGO_VERSION: '0.37'
        HUGO_SHA: 'ce89665e85a5fdc7cdcf34529f6e5d45e22e3bbce93b2107ba015cde3bdfaf06'

before_script:
  - apk update && apk add openssl ca-certificates
  - wget -O ${HUGO_VERSION}.tar.gz https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
  - echo "${HUGO_SHA}  ${HUGO_VERSION}.tar.gz" | sha256sum -c
  - tar xf ${HUGO_VERSION}.tar.gz && mv hugo* /usr/bin/hugo
  - hugo version

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
{{< /highlight >}}

[Home](/blog "ohthreefive")

[1]: https://ohthreefive.gitlab.io/ "my blog!"

[2]: https://gitlab.com/pages/hugo/issues/12 "from Hugo's discussions"

[3]: https://gitlab.com/axil "Achilleas - a very helpful dev!"
