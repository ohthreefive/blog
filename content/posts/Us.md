+++
date = "2019-08-11T13:59:46.262Z"
title = "Us"
description = "Brief movie reviews #33"
tags = [ "movie" ]

+++

Without reducing this review to a direct comparison between this and Get Out,
which I only caught when it appeared on Netflix earlier in the year, two
similarities felt prominent. Firstly, from the opening credits, a simple zoom
out on a wall of caged rabbits, this film oozed quality. I felt the same about
the quality of film-making in Get Out. Regardless of what I was about to see, it
would be *well made*. Secondly, these are my kind of horror films. By that I
mean they're not that *scary*, but there is a constant sense of unease
punctuated with brief releases. Brilliant film.

5/5

[Home](/blog "ohthreefive")