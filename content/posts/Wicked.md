+++
date = "2025-01-01T20:54:06Z"
title = "Wicked"
description = "Brief movie reviews #91"
tags = ["movie"]

+++

Unacceptably long and, is it just me, or is Glinda pretty much irredeemable? Even when she's being nice, it's for the wrong reason! I can't deny it wasn't beautiful and that the signature songs were brilliant. But wow. Long.

3/5

[Home](/blog "ohthreefive")