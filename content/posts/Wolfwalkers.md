+++
         
date = "2021-01-03T09:25:58.098Z"
title = "Wolfwalkers"
description = "Brief movie reviews #48"
tags = [ "movie" ]

+++

An immediate family favourite, an absolute joy, a beauty.

5/5

[Home](/blog "ohthreefive")