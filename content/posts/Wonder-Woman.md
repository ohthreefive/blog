+++

date = "2017-10-14T20:05:10Z"
title = "Wonder Woman"
description = "Brief movie reviews #12"
tags = [ "movie" ]

+++

A very well reviewed DCEU movie? So good that it might have ended the DCEU in favour of more stand alones?

The movie itself was decidedly... average. Average CGI with some ropey weightlessness. Average third act with a drop-in über villain. So why was it so well received?

The freshness of a movie hero who both wants to save the world, feels a duty to save the world and sees the world as worth saving is the highlight. Optimism without cynicism. All DC superhero movies have absolutely lacked this and Marvel heroes tend to drip with cynicism (Vision excepted.) They’re also so deep in to their universe that they’ve started fighting each other.

So bravo for the tone, at least.

3/5

(PS After watching the last act, one question: why didn’t [Diana spear Doomsday][1] in BvS DoJ? Supes definitely could have sat that one out!)

[Home](/blog "ohthreefive")

[1]: https://mobile.twitter.com/ohthreefive/status/918965770251579392?p=v "linking to my own tweet...classy"
