+++
date = "2023-12-21T20:14:54Z"
title = "Wonka"
description = "Brief movie reviews #83"
tags = ["movie"]

+++

Some more charming Paul King and Simon Day fare, but without the frequent
laughs of the Paddington movies. Timothy Chalamet is an excellent lead and
gives Wonka just the right amount of personality - coming across more as
charming compared to Depp’s excessive quirkiness. Passed the time nicely,
but unlikely to be revisited frequently.

3/5

[Home](/blog "ohthreefive")