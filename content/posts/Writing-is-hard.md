+++

tags = ["random"]
date = "2017-04-19T08:07:24+01:00"
description = "My last post sucked. Much to learn, I still have."
title = "Writing is hard"

+++

### It's just words, right?

My [last post][1] talked about something I've noticed in (usually) superhero
movies, where as a franchise develops, something from an earlier movie
originally presented as all-powerful is sent down the pecking order, usually by
an even more powerful something-or-other.

I used Marvel's movies as an example. Another I thought of is Transformers,
where in the first movie Megatron is an unstoppable one-robot army. By the third
film in the franchise, Optimus Prime has him for breakfast. I probably
shouldn't look to that particular franchise for logic and continuity, but it's a
good example!

Anyway, re-reading the piece... it's awful. I have the idea clear in my mind,
but it hasn't come out anything like as coherently as I'd have liked.

Hopefully, a bit of writing practice will help!

[Home](/blog "ohthreefive")

[1]: /content/post/Boy-that-escalated-quickly.md "Horribly written!"
