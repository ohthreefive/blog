+++

date        = "2017-02-26T20:28:27Z"
description = "How to connect to a bluetooth device from the command line on Ubuntu Mate Raspberry Pi edition"
title       = "bluetoothctl basics"
tags  = [ "linux", "raspberrypi", "commandline" ]

+++

### BLUETOOTH AND LINUX

I'm not a Linux veteran, but I've been around long enough to know the
combination of Linux and bluetooth is not talked about fondly.

Too bad, as I'm currently using a Raspberry Pi 3 with [Ubuntu Mate for
Raspberry Pi][1] and a [nexdock][2](NexDock? They don't seem to be able to
decide on the capitalisation) as my daily driver.

Bluetooth is therefore essential.

### FIRST IMPRESSIONS

When I first go the nexdock, maybe six months ago, the GUI tool `blueman` was
really frustrating and frankly unreliable. I investigated `bluetoothctl`, got
things working and wrote a back-up shell script which sits on my desktop.
Whenever something goes wrong with bluetooth, I switch it off and on again using
the USB mouse I've got (the nexdock trackpad is terrible) then double click the
script which re-connects the keyboard.

### HICCUP

Except after a recent Mate `apt upgrade`, the script stopped working. I had no
backup USB keyboard at the time, so jumped in to the `blueman` GUI, which I'm
pleased to say worked perfectly. While it's great to have the GUI, I want my
script to work again! I grabbed a USB keyboard, disconnected, un-trusted and
un-paired the nexdock keyboard and launched `bluetooothctl` from the terminal.

### bluetoothctl

Rather than talk through the steps, I'll just put the output from my terminal
here. It's short and fairly self-explanatory:

```bash
ohthreefive@ohthreefive:~$ bluetoothctl
[NEW] Controller B8:27:EB:88:09:26 ohthreefive [default]
[bluetooth]# devices
[bluetooth]# scan
Missing on/off argument
[bluetooth]# scan on
Discovery started
[CHG] Controller B8:27:EB:88:09:26 Discovering: yes
[NEW] Device A4:77:33:CC:01:23 A4-77-33-CC-01-23
[NEW] Device 20:16:07:05:93:90 NexDock Keyboard
[CHG] Device A4:77:33:CC:01:23 RSSI: -78
[bluetooth]# pair 20:16:07:05:93:90
Attempting to pair with 20:16:07:05:93:90
[CHG] Device 20:16:07:05:93:90 Connected: yes
[CHG] Device 20:16:07:05:93:90 Modalias: usb:v04E8p7021d0001
[CHG] Device 20:16:07:05:93:90 UUIDs: 00001124-0000-1000-8000-00805f9b34fb
[CHG] Device 20:16:07:05:93:90 UUIDs: 00001200-0000-1000-8000-00805f9b34fb
[CHG] Device 20:16:07:05:93:90 Paired: yes
Pairing successful
[CHG] Device 20:16:07:05:93:90 Connected: no
[CHG] Device A4:77:33:CC:01:23 RSSI: -68
[bluetooth]# trust 20:16:07:05:93:90
[CHG] Device 20:16:07:05:93:90 Trusted: yes
Changing 20:16:07:05:93:90 trust succeeded
[CHG] Device A4:77:33:CC:01:23 RSSI: -79
[bluetooth]# connect 20:16:07:05:93:90
Attempting to connect to 20:16:07:05:93:90
[CHG] Device 20:16:07:05:93:90 Connected: yes
Connection successful
[NexDock Keyboard]# exit
[DEL] Controller B8:27:EB:88:09:26 ohthreefive [default]
ohthreefive@ohthreefive:~$ SUCCESS! =D
```

As you can see it took a while to find the keyboard but after that every command
(pair, trust then connect) worked first time.

I'd include a link to the `man` page for `bluetoothctl` but, well, look it up
for yourself! (*hint* `bluetoothctl --help` is more worth your while!)

### THE SCRIPT

Below is the one line `bash` script I can now use (again) if I need to
re-connect the keyboard. It's working again since I manually forgot and re-added
the nexdock keyboard connection. It's a bit slower than I remember, but it's
only a backup after all, and now `blueman` is another reliable backup anyway.

{{< highlight bash >}}
#/bin/bash
echo "connect 20:16:07:05:93:90" | bluetoothctl
{{< /highlight >}}

### WRAP

Huge thanks to [Martin Wimpress][3] and the Mate team for the Pi image and
improvements to `blueman`. Linux and bluetooth: keep on keeping on!

[1]: https://ubuntu-mate.org/raspberry-pi/

[2]: http://nexdock.com/support-raspberry-pi/

[3]: https://plus.google.com/+MartinWimpress

[Home](/blog "ohthreefive")
