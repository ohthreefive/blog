+++

date = "2017-04-24T18:05:22+01:00"
title = "kmz to gpx"
description = "Finding my way around 'competing' file standards for mapping"
tags = ["linux", "commandline"]

+++

I very recently wanted to get some waypoints onto a mapping app on my phone for
a walk I was about to do. As always, I turned to the excellent [Munro Magic][1]
website. However it offered me a `.kmz` for the route I was planning and nothing
else.

I think of `.kmz` simply as a zipped `.kml` file, `.kml` being 'key markup
language' used by Google Earth. There's probably more to `.kmz` than that but
once I'd figured that out, it was easy to do the conversion to `.gpx`.

Numerous websites advised 7zip to unzip the `.kmz` but I wonder if that's a
Windows limitation. On my Linux machine, `unzip file.kmz` gave me `file.kml`.

Next came a little Unix command line tool called [GPS Babel][2]. Version 1.5.4
is available on the website but version 1.5.2 is available in the Ubuntu
repositories so for ease of install I did a simple `apt install gpsbabel`.

GPSBabel can't convert directly from `.kmz` to `.gpx` hence the `unzip` step
above. But it can do `.kml` so final step was:

    gpsbabel -i kml -f file.kml -o gpx -F file.gpx

Worked perfectly and not a GUI in sight!

[Home](/blog "ohthreefive")

[1]: http://www.munromagic.com "invaluable website"

[2]: https://www.gpsbabel.org/ "neat tool"
