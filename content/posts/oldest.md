+++

title=       "First post to Gitlab Pages with Hugo... kind of"
description= "First post on my GitLab Pages site. Except really it's a re-post after switching theme from Lanyon to After Dark."
date=        "2017-01-28"
tags=  [ "meta" ]

+++

## A WEBSITE NO ONE WILL VISIT!

I've been wanting my own website for some time. While I managed to get an nginx
web server running at home, I couldn't sort out dynamic DNS despite many
attempts. I discovered GitHub pages but they weren't served over https and I
didn't fancy designing the page myself. **(I have subsequently come to realise I
could have just as easily used Hugo with GitHub. Always learning.)**

## GITLAB PAGES

After signing up for GitLab I noticed the pages functionality (or at least
advertised functionality) was different from GitHub in that it mentioned using
a static site generator. I'd never heard of one of those before but after a
quick read, I picked Hugo from the list and, after only a few hair tearing
moments, had a https website.

## BONUS CONTENT

There's a guide to using your own domain name..! I might try this. Later. Once
I've got a hang of the basics!

## CONTENT

So is suspect I'll mainly randomly muse about tech here, but it'll also be a
dumping ground for anything I want to put out there.

Until next time.

[Home][1]

[1]: / "go home"
