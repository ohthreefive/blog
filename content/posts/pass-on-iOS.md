+++

date = "2018-03-25T19:28:56Z"
title = "pass on iOS"
description = "I've finally integrated pass into iOS in a reasonably automated way"
tags = [ "security", "programming", "ios"]

+++

Way back in May 2017 I decided I needed to get access to my favourite password manager, [`pass`][1](which I use on my desktop Linux machines) from my mobile operating system of choice, iOS.

With `pass` using `git` repositories, I needed an iOS `git`. Enter [Working Copy][2]. And I needed a way to work with `gpg` generated keys. Enter [PGPEverywhere][3].

As documented in [my blog post last May][4], I managed to get a functional `pass` password reader with this approach, but not a generator.

### PYTHONISTA

I’ve made a few projects in Python. My Python learning is mostly held back by a lack of a project to practice on. I decided to write a password generator in Python. It resides in a [public GitLab repo][5]. Why Python? Because of the awesome app [Pythonista][6], which is not only a full Python IDE but also plays very well with iOS automation.

The code for the password generator I use in my iOS workflow is a little different to that on GitLab but it contains private information so I can’t share it verbatim.

### EXTRA SAUCE

Unsurprisingly, the now-Apple-acquired [Workflow][7] also plays a part. Four apps to replicate a command line utility. LOL. Linux for the win! However, three of the four apps are must have, surely?

### THE WORKFLOW

Here’s the assumed situation: I’m at a site I wish to sign up for.

1.  Go to the "login" page of the website (not the "sign up" page, the reason why will become clear.)
2.  Access the share sheet with the share icon and select "Run Worklow."
3.  Chose my "New password"  workflow.
4.  This workflow takes the URL passed to it, stores it as text and runs my password-generating Pythonista script. The workflow passes the URL to the script and Pythonista can ‘collect’ this URL with `sys.argv`.[^1],[^2]
5.  The script interactively generates a random password and asks me for a login I want to use with the site.
6.  It then combines the new password, login and the URL above into a variable which is sets to the clipboard.
7.  Finally, it opens a second Workflow workflow.
8.  The second workflow opens PGPEverywhere, where I paste in the variable Pythonista had set to clipboard, encrypt (but don’t sign) the text and re-copy it.
9.  Then I hit the back button, which takes me back to Workflow where my workflow resumes by asking me for the name of this password. Typically, that’s the website.
10.  The password name (plus suffix ‘.gpg’) is attached to an x-callback-url which will write a file to a repository (also specified in the x-callback-url) in Working Copy.
11.  If no text is passed in the URL, Working Copy writes the clipboard to the new file in the chosen repository, in this case that means an encrypted block of text.
12.  After the write, Workflow re-opens PGPEverywhere, where I re-paste the encrypted block, decrypt it and copy the unencrypted text to the clipboard. Hit back to return to Workflow.
13.  The workflow continues by grabbing the clipboard and separating it, so that just the new password goes back to the clipboard and the URL [^3] is sent to Safari.
14.  Safari re-opens with a password in the clipboard and an encrypted password stored in my password repository on Working Copy.[^4]


Assuming all goes well with the new password, I probably want to `commit` the new password file and maybe even `push` these changes back to GitLab. Using Working Copy’s x-callback-url support, I can do both of these without leaving Safari using Pythonista.[^5]

### WRAP

The above workflow is complete in about a minute. While clumsy in appearance, I can’t generate and test new passwords much quicker on a Linux box. It’s the most complex workflow I’ve created so far. And I love it!

#### POSTSCRIPT

I’ve made a shorter Workflow workflow [^6] which, when I encounter a page to sign in to, opens my password repository (via x-callback-url) where I copy the encrypted block, then opens PGPEverywhere where I decrypt and copy the password, then re-opens Safari. It’s much simpler and is used more often (I’ve already got logins for a **lot** of websites!)

### THE PYTHONISTA SCRIPT

I’ve re-checked and the Python code in step 4 above and actually it doesn’t contain the ‘key’ to my Working Copy repos so I can post it here:

``` Python
from random import randint as r, choice as c, shuffle
from clipboard import set, get
from webbrowser import open
import sys

url = sys.argv[1]

def getNumber():
	
	while True:
		try:
			length = int(input("What length of password would you like?" + "\n"
			+ "(8-64 characters please)" + "\n" + "\n"))
			break
		except ValueError:
			print("\n" + "That's not a number. You'll need to start again." + "\n")
			
	if length < 8:
		print("\n" + "Too short, your password length has been increased to 8"
		+ "\n")
		length = 8
	elif length > 64:
		print("\n" + "Too long, your password length has been decreased to 64"
		+ "\n")
		length = 64
		
	return length

def getComponents():
	
	components = [0]
	
	while True:
		upper = input("\n" + "Include upper case/capital letters?" + "\n" + "(y/n)"
		+ "\n" + "\n").lower()
		if upper in ["y", "yes"]:
			components.append(1)
			break
		elif upper in ["n", "no"]:
			break

	while True:
		numerals = input("\n" + "Include numbers?" + "\n" + "(y/n)" + "\n"
		+ "\n").lower()
		if numerals in ["y", "yes"]:
			components.append(2)
			break
		elif numerals in ["n", "no"]:
			break

	while True:
		symbols = input("\n" + "Include symbols?" + "\n" + "(y/n)" + "\n"
		+ "\n").lower()
		if symbols in ["y", "yes"]:
			components.append(3)
			break
		elif symbols in ["n", "no"]:
			break

	return components

def makePass(userLength, pasComponents):
	password = ""
	chars = ["abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	"0123456789", "-/:;()£&@.,?![]{}#%^*+=_\|~<>€$"]
	for i in pasComponents:
		password += c(chars[i])
	userLength -= len(pasComponents)
	for i in range(0,(userLength)):
		j = c(pasComponents)
		password += c(chars[j])
	for i in range(0,10):
		password = list(password)
		shuffle(password)
		password = "".join(password)
	return password

userLength = getNumber()
pasComponents = getComponents()
yourPassword = makePass(userLength,pasComponents)

login = input("\n" + "Login for this site? " + "\n")

set(yourPassword + "\n" + "login: " + login + "\n" + "URL: " + url + "\n")

open("workflow://run-workflow?name=Encrypt%20and%20Save%20passwd")
```

The code is similar to that on my GitLab page. I’ve added importing a few modules to open URLs (x-callback-url in this case), access `sys.argv` (to get the URL passed from Workflow) and work with the clipboard.

[Home](/blog "ohthreefive")

[1]: https://www.passwordstore.org/ "The Standard Unix Password Manager"

[2]: https://workingcopyapp.com/ "Working Copy - git on ios "

[3]: http://pgpeverywhere.com/ "PGPEverywhere app homepage"

[4]: https://ohthreefive.gitlab.io/post/rolling-my-own-pass-ios-client/ "the seeds of a pass ios client were sown"

[5]: https://gitlab.com/ohthreefive/PyPasswd/blob/master/pypasswd.py "Python3 password generator"

[6]: http://omz-software.com/pythonista/ "Pythonista home page"

[7]: http://workflow.is/ "Please comtinue development, Apple!"

[^1]: Note Workflow seems to only be able to call truly *local* Pythonista scripts, ie those on "This iPad/iPhone" not "iCloud" or "External Files".

[^2]: You may wonder why I don’t just start this by sharing to the Pythonista script. Running Pythonista scripts as an action extension from the share sheet doesn’t let you ‘break out’ of Safari to call Workflow. At least, I don’t think it does.

[^3]: The *original* URL from which this complex workflow all began, remember!

[^4]: Usually opening Safari from Workflow opens the URL in a new tab, however for some (welcome) reason sending the same URL as the currently active tab only seems to refresh the tab. It would be even nicer if I could just re-open Safari in place without the re-load, but I can’t win them all! (Or remotely figure this out, to be honest.)

[^5]: I could use a Workflow workflow to do this, but find a a Pythonista script a little quicker.

[^6]: Ok I admit it: I get a kick out of writing ‘Workflow workflow.’
