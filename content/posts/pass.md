+++

title= "Pass"
date= 2018-03-20
tags= ["commandline", "linux", "security"]

+++

**This is a re-blog of an old piece**

# Setting up `pass` - kind of

I've recently blogged about getting Password Store (an Android app available
in the f-droid store and compatible with the `pass` password manager.) `pass`
essentially uses your private `gpg` key to encrypt small text files containing
your passwords. You can stick extra information like username, URL etc and you
can easily organise your passwords in a folder-based hierarchy.

It's a nice application and is being actively developed. See [the website][1]
for more information.

I decided to move to `pass` from 1Password. I first used 1Password on an iPhone,
then used it on my iMac as well (with Dropbox syncing) and then my iPad. Since
then, my iMac died, I moved to Android on mobile and Ubuntu (+Raspbian!) on
desktop and upgraded to an iPad Air. The Android 1Password app is (now) great so
I was relatively happy, but not having a password manager on my desktop irked
me.

I heard about `pass` either on the [Linux Voice][2] podcast, the [Ubuntu UK][3]
or quite possibly both. I set it up on an old Lenovo laptop (of my wife's) that
I've got kind of acting as a server. I should have set it up on my main desktop
(an HP tower from my dad's office that was heading for the scrap-heap) but alas!

I've mentioned before having no knowledge of `gpg`, poor knowledge of SSH keys
and a little of using `git` remotes. But I got it all set up with [Gitlab][4],
got it working on my Nexus 6, then my new Nextbit Robin (and since my
Cyanogenmod-enhanced Nexus 6!) Feeling confident, I thought I'd try to set up my
HP desktop to access the encrypted password store without instructions.

## The plan

I figured I needed:

1. the same private (`gpg`)  key that encrypts the passwords, originally created
   on my Lenovo machine
2. an SSH key-pair to talk to gitlab.com (I could use password but apparently
   key-pairs are much safer... and you can password-protect your key for double
   security)
3. both the correct `pass` and `pass git` commands to ensure I accessed my
   existing password store rather than creating a new one (or worse, doing
   badness to my existing one!)

Having managed all this from the comfort of a lovely Android app with decent
documentation, I figured I could do it with minimal griding of teeth and pulling
of hair!

## Identify the key

First, the private key. The setup instructions on the Password Store homepage
(re-printed by me [here][5]) are simple. `gpg -k` (or `gpg --list-keys`) will
spit out something like this: (I used `gpg --list-keys` if you care!)

    gpg: directory `/home/user_name/.gnupg' created
    gpg: new configuration file `/home/user_name/.gnupg/gpg.conf' created
    gpg: WARNING: options in `/home/user_name/.gnupg/gpg.conf' are not yet
    active during this run
    gpg: keyring `/home/user_name/.gnupg/pubring.gpg' created
    gpg: /home/user_name/.gnupg/trustdb.gpg: trustdb created

That's the output you'll get the first time you use a `gpg` command, not the
output I was looking for! Why was I getting that? Well, I ran the command on my
HP machine, the one I wanted to import the key on to. Not the machine I'd
created the key on. I've never run a `gpg` command on my HP machine! So I logged
on to the Lenovo server machine instead and ran the same command:

    /home/user_name/.gnupg/pubring.gpg
    ------------------------------
    pub   4096R/ABCD1234 2015-12-13
    uid                  Your Name <your_email@address.com>
    sub   4096R/1234ABCD 2015-12-13

The key-id is the on the 'pub' line, after the slash (`ADCD1234` in this case.)
The fact that the line starts 'pub' makes me think it's a public rather than
private key and I know I actually want the private key ... but as I know too
little about this kind of thing I'll stop thinking! [^1]

## Export then import the key

Exporting it can be done with the command `gpg --export-secret-keys
[key_id] > keys.asc`. This file (`keys.asc`) now resides in whichever directory
you ran the command in. I needed this file on my HP machine. As I use Syncthing
this was easy. The other thing worth saying is that when I created the key I
gave it a password, so it wouldn't be a disaster to copy it over using 3rd party
cloud storage (other people may know better than me about this!) Of course, you
could use a USB stick or something simple like that!

On my HP machine I ran the command:

    gpg --import path/to/file/keys.asc

And got the output:

    gpg: keyring `/home/user_name/.gnupg/secring.gpg' created
    gpg: key ABCD1234: secret key imported
    gpg: key ABCD1234: public key "Your Name <your_email@address.com>" imported
    gpg: Total number processed: 1
    gpg:               imported: 1  (RSA: 1)
    gpg:       secret keys read: 1
    gpg:   secret keys imported: 1

## Install `pass`

Everything seemed to be going fine! I then remembered I didn't have `pass`
installed yet:

    sudo aptitude install pass

## SSH shenanigans [^2]

With `pass` installed and my `gpg` key imported/copied, I now needed to sort out
SSHing to gitlab. Thankfully, gitlab has [excellent documentation][6]. I
confirmed I hadn't previously created a key-pair in the past with the command:

    cat ~/.ssh/id_rsa.pub

And when that threw up nothing I used the command:

    ssh-keygen -t rsa -C "$your_email"

(By the way, the bracketed value is just a comment to attach to the key. Enter
what you want, be that an e-mail address or not!) Output:

    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/user_name/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/user_name/.ssh/id_rsa.
    Your public key has been saved in /home/user_name/.ssh/id_rsa.pub.
    The key fingerprint is:

I just hit enter to accept the default for the 'enter file' question. I set a
password. Below the 'fingerprint' line is a long fingerprint (duh) and a
'randomart image' of the key. I have no idea what used the randomart serves! So
much still to learn... Anyway back to the task at hand.

## Get SSH key to Gitlab

Re-running:

    cat ~/.ssh/id_rsa.pub

Outputs my key to the command line. I selected it and copied it to the clipboard
before heading to the [gitlab page][7] that deals with your SSH keys. Paste
and save the key. Gitlab should now trust incoming requests from my HP computer.
Remember at this point I *already had* a password store set up on a different
computer and saved on gitlab. If you don't have a password store  or gitlab
account then you'll need to do those things first.

## What's next?

Here I paused. Mostly because I didn't know what to do next. Eventually I
figured I needed to create a *new* password store on my HP computer, encrypted
with the same key, then 'sync' (I don't know what the correct term is in this
regard) with the one I already had on gitlab. This is the sort of assumption I
wouldn't have had the confidence to make without the help of a 'how to' about
six months ago. Perhaps the confidence is mis-placed! But it worked this time.

So, following the top three commands from the [EXTENDED GIT EXAMPLE][8] here, I
entered the following commands:

    pass init ABCD1234

In my terms this means: 'initialise a `pass` password store encrypted with the
`gpg` key with the ID I've just typed.' Output:

    mkdir: created directory ‘/home/user_name/.password-store’
    Password store initialized for ADBCD1234.

Then:

    pass git init

Output:

    Initialised empty Git repository in /home/user_name/.password-store/.git/
    [master (root-commit) 80bd4d2] Added current contents of password store.
     1 file changed, 1 insertion(+)
      create mode 100644 .gpg-id

I was very hopeful that this 1 file insertion wasn't going to ruin everything.
By very hopeful, I mean I assumed it was going to ruin everything. For sure.

Then:

    pass git remote add origin git@gitlab.com:my_gitlab_username/my_gitlab_password_store_repository_name.git

That would have seriously vexed me in the recent past. I definitely would have
got that ... address (?) ... wrong. Gitlab is awesome again here. On the front
of my project page for my password store is a box containing the correct SSH
(and HTTPS) address for this purpose. Thanks gitlab!

## Get the password store from Gitlab

I then ran one of the few `git` commands I know off the top of my head, prefixed
with `pass` (it's awesome how well `pass` and `git` work together!)

    pass git pull origin master

As I'd never SSH'd to gitlab before I was asked to trust its fingerprint:

    The authenticity of host 'gitlab.com (104.210.2.228)' can't be established.
    ECDSA key fingerprint is f1:d0:fb:46:73:7a:70:92:5a:ab:5d:ef:43:e2:1c:35.
    Are you sure you want to continue connecting (yes/no)?

Anyone have any idea what to do with this? Like an idiot I just typed 'yes' and
trusted in gitlab. Seems about as sensible as using Password1234 for all your
passwords and is pretty ironic given I'm doing all this to create an encrpyted
password store but let's not be too masochistic!

I was asked for the password of the SSH key (as I set a password for it.) Once
entered, I got this output:

    From gitlab.com:my_gitlab_username/my_gitlab_password_store_repository_name
     * branch            master     -> FETCH_HEAD
     Merge made by the 'recursive' strategy.

After being asked to enter a 'merge' comment. I was still anxious when being
asked about the merge comment. Presumably I was merging the one file created by
my `pass git init` command with my already-created password store. I was still
convinced everything was about to be ruined...

But the output below the 'Merge made by' line (above) was a list of all my data
from my password store, now happily residing on my HP computer! My only quibble
is I don't seem to have tab completion. Ho-hum!

## Wrap

This has been a horribly long-winded account, and maybe I'll re-post just the
instructions. But by typing my stream of consciousness, maybe other Linux
beginners like me will rest happily, knowing that there are plenty of other
people out there just as clueless as them, but persevering nonetheless!

[1]: https://www.passwordstore.org
[2]: https://www.linuxvoice.com
[3]: http://ubuntupodcast.org/
[4]: https://www.gitlab.com
[5]: http://www.googlenowoncrap.tumblr.com/post/140855681463/setting-up-password-store-for-android
[6]: http://doc.gitlab.com/ce/ssh/README.html
[7]: https://gitlab.com/profile/keys
[8]: https://git.zx2c4.com/password-store/about/

[^1]: I have, to an extent, enlightened myself in this regard! I was correct,
      the 'pub' does mean it's the public key's id. But as the public and
      private keys are a pair, the private key's id is the same. I found this
      out by typing `gpg --list-secret-keys` which gives the same(ish) output as
      `--list-keys` but with 'sec' for secret instead of 'pub' for public.
[^2]: I've definitely read a blog post / tutorial called this. I'm such a
      plagiariser!
